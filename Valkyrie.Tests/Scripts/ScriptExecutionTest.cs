using System;
using System.Linq;
using Valkyrie.Data;
using Valkyrie.Scripts;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.Scripts
{
    public class ScriptExecutionTest : GenericUnitTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public ScriptExecutionTest(ITestOutputHelper testOutputHelper) : base()
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public void StringConcatTest()
        {
            var i = new Interpreter();

            var res = i.Eval("'ASD' + 'FGH'");
            _testOutputHelper.WriteLine(res.ToString());

            Assert.Equal("ASDFGH", res.ToString());
        }

        [Theory]
        [InlineData(3,5)]
        [InlineData(15,5)]
        [InlineData(83,5)]
        [InlineData(32,5)]
        [InlineData(3,56)]
        public void FloatTest(float a, float b)
        {
            var i = new Interpreter();

            Assert.Equal(a + b, i.Eval($"{a} + {b}"));
            Assert.Equal(a - b, i.Eval($"{a} - {b}"));
            Assert.Equal(a * b, i.Eval($"{a} * {b}"));
            Assert.Equal(a / b, i.Eval($"{a} / {b}"));

            Assert.Equal(a + b, i.Eval($"Add({a} , {b})"));
            Assert.Equal(a - b, i.Eval($"Rem({a} , {b})"));
            Assert.Equal(a * b, i.Eval($"Mul({a}, {b})"));
            Assert.Equal(a / b, i.Eval($"Del({a} ,{b})"));

            Assert.Equal(a + b - 2 * a / b, i.Eval($"{a} + {b} - 2 * {a} / {b}"));
            
            Assert.Equal(a < b, i.Eval($"{a} < {b}"));
            Assert.Equal(a > b, i.Eval($"{a} > {b}"));
            Assert.Equal(a <= b, i.Eval($"{a} <= {b}"));
            Assert.Equal(a >= b, i.Eval($"{a} >= {b}"));
            Assert.Equal(a == b, i.Eval($"{a} == {b}"));
            Assert.Equal(a != b, i.Eval($"{a} != {b}"));


            Assert.Equal((a + b) + ((a + b) + b), i.Eval($"Add({a}, {b}) + Add({a}+{b},{b})"));
        }

        [Fact]
        public void OperatorsTest()
        {
            var i = new Interpreter();

            Assert.Equal((float)(2f * 4f / 6f * 5f), i.Eval("2 * 4 / 6 * 5"));
            Assert.Equal((float)(2f + 2f * 4f + 3f), i.Eval("2 + 2 * 4 + 3"));
        }

        [Theory]
        [InlineData("void A() { }")]
        [InlineData("void A ( float arg ) { }")]
        [InlineData("void A(float arg0, float arg1) { }")]
        [InlineData("float A() { return 1; }")]
        [InlineData("float A(float a, float b) { return a - b; }")]
        public void MethodsDefine(string code)
        {
            using (var test = CreateTest("MethodsDefine"))
            {
                test.Container
                    .RegisterLibrary(Libraries.Scripts)
                    .Build();

                var s = test.Container.Resolve<IScriptEngine>();

                var result = s.Compile(code.ToStream());
                var ss = code.Split(' ');
                Assert.Equal(1, result.Methods.Count());
                Assert.Equal(ss[0], result.Methods.First().ResultType.GetFullName());
            }
        }

        [Theory]
        [InlineData("void B() {}void A() { B(); }", "A", 2)]
        [InlineData(" float B() {return 1;}float A ( float arg ) { c = arg + B(); return c; }", "B", 2)]
        [InlineData("float B(float a, float b) {return a*b;}float A(float a, float b) { return a - b * B(a,b); } void C() {} ", "C", 3)]
        public void TwoMethodsDefine(string code, string run, int metCount)
        {
            using (var test = CreateTest("MethodsDefine"))
            {
                test.Container
                    .RegisterLibrary(Libraries.Scripts)
                    .Build();

                var s = test.Container.Resolve<IScriptEngine>();

                var result = s.Compile(code.ToStream());
                var ss = code.Split(new []{' '}, StringSplitOptions.RemoveEmptyEntries);
                Assert.Equal(metCount, result.Methods.Count());
                Assert.Equal(ss[0], result.Methods.First().ResultType.GetFullName());

                s.Run(result, run);
            }
        }

        [Theory]
        [InlineData("float Factorial(float value) { if(value > 1) return value* Factorial(value - 1); else return 1;}", 0, 1)]
        [InlineData("float Factorial(float value) { if(value > 1) return value*Factorial(value - 1); return 1;}", 1, 1)]
        [InlineData("float Factorial(float value) { if(value > 1) return value*Factorial(value - 1); return 1;}", 2, 2)]
        [InlineData("float Factorial(float value) { if(value > 1) return value*Factorial(value - 1); return 1;}", 3, 6)]
        [InlineData("float Factorial(float value) { if(value > 1) return value*Factorial(value - 1); return 1;}", 4, 24)]
        [InlineData("float Factorial(float value) { if(value > 1) return value*Factorial(value - 1); return 1;}", 5, 120)]
        public void TestRecursion(string code, float value, float r)
        {
            using (var test = CreateTest("TestRecursion"))
            {
                test.Container
                    .RegisterLibrary(Libraries.Scripts)
                    .Build();

                var s = test.Container.Resolve<IScriptEngine>();

                var script = s.Compile(code.ToStream());

                var actual = s.Run(script, "Factorial", value);
                Assert.Equal(r, actual);
            }
        }

        [Theory]
        [InlineData("float ReturnValue(float value) { return value; }", 1)]
        public void TestMethodCallInScript(string code, float r)
        {
            using (var test = CreateTest("TestMethodCallInScript"))
            {
                test.Container
                    .RegisterLibrary(Libraries.Scripts)
                    .Build();

                var s = test.Container.Resolve<IScriptEngine>();

                var script = s.Compile(code.ToStream());

                var actual = s.Run(script, "ReturnValue", r);
                Assert.Equal(r, actual);
            }
        }

        class TestExportedClass
        {
            public string Value { get; set; }
            public TestExportedClass Self => this;

            public string GetText()
            {
                return Value;
            }
            public string GetText(string value)
            {
                return Value + value;
            }
        }

        [Fact]
        public void TestClassUsageCall()
        {
            var codeSelf = "TestClass ReturnSelf(TestClass self) { return self; }";
            var codePropertyGet = "string ReturnSelf(TestClass self) { return self.Value; }";
            var codePropertyGetSelf = "string ReturnSelf(TestClass self) { return self.Self.Value; }";
            var codePropSet = "TestClass ReturnSelf(TestClass self, string newValue) { return self.Value = newValue; return self; }";
            var codePropSetSelf = "TestClass ReturnSelf(TestClass self, string newValue) { return self.Self.Value = newValue; return self; }";
            var codeMethodCall = "string ReturnSelf(TestClass self) { return self.GetText(); }";
            var codeMethodCallSelf = "string ReturnSelf(TestClass self) { return self.Self.GetText(); }";
            var codeMethodCallArg = "string ReturnSelf(TestClass self, string newValue) { return self.GetText(newValue); }";
            var codeMethodCallSelfArg = "string ReturnSelf(TestClass self, string newValue) { return self.Self.GetText(newValue); }";
            
            using (var test = CreateTest("TestObjectPropertiesCall"))
            {
                test.Container
                    .RegisterLibrary(Libraries.Scripts)
                    .Build();

                var s = test.Container.Resolve<IScriptEngine>();

                s.ExportTypeWithMethods<TestExportedClass>("TestClass");

                var r = new TestExportedClass() {Value = "ASD"};

                var script = s.Compile(codeSelf.ToStream());
                var actual = s.Run(script, "ReturnSelf", r);
                Assert.Same(r, actual);

                script = s.Compile(codePropertyGet.ToStream());
                actual = s.Run(script, "ReturnSelf", r);
                Assert.Equal(r.Value, actual);

                script = s.Compile(codePropertyGetSelf.ToStream());
                actual = s.Run(script, "ReturnSelf", r);
                Assert.Equal(r.Value, actual);

                script = s.Compile(codePropSet.ToStream());
                actual = s.Run(script, "ReturnSelf", r, "AAA");
                Assert.Equal("AAA", actual);

                script = s.Compile(codePropSetSelf.ToStream());
                actual = s.Run(script, "ReturnSelf", r, "AAA");
                Assert.Equal("AAA", actual);

                script = s.Compile(codeMethodCall.ToStream());
                actual = s.Run(script, "ReturnSelf", r);
                Assert.Equal("AAA", actual);

                script = s.Compile(codeMethodCallSelf.ToStream());
                actual = s.Run(script, "ReturnSelf", r);
                Assert.Equal("AAA", actual);

                script = s.Compile(codeMethodCallArg.ToStream());
                actual = s.Run(script, "ReturnSelf", r, "AAA");
                Assert.Equal("AAAAAA", actual);

                script = s.Compile(codeMethodCallSelfArg.ToStream());
                actual = s.Run(script, "ReturnSelf", r, "AAA");
                Assert.Equal("AAAAAA", actual);
                
                
            }
        }
    }
}