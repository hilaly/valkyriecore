using System;
using System.IO;
using System.Text;
using Xunit.Abstractions;

namespace Valkyrie.Tests
{
    public class GenericUnitTests
    {
        #region Utils

        protected ITest CreateTest(string name)
        {
            return new TestInstance(name);
        }

        #endregion

        public GenericUnitTests(ITestOutputHelper output)
        {
            var converter = new Converter(output);
            Console.SetOut(converter);
        }

        public GenericUnitTests()
        {
        }
    }

    class Converter : TextWriter
    {
        ITestOutputHelper _output;

        public Converter(ITestOutputHelper output)
        {
            _output = output;
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }

        public override void WriteLine(string message)
        {
            _output.WriteLine(message);
        }

        public override void WriteLine(string format, params object[] args)
        {
            _output.WriteLine(format, args);
        }
    }
}