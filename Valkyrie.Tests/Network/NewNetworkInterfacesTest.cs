using System;
using Valkyrie.Communication.Network;
using Valkyrie.Data.Config;
using Valkyrie.Data.Serialization;
using Valkyrie.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.Network
{
    public class NewNetworkInterfacesTest : GenericUnitTests
    {
        #region Help class defines

        [Contract]
        class TestRequest
        {
            [ContractMember(0)] public int Value;
        }

        [Contract]
        class TestResponse
        {
            [ContractMember(0)] public int Value;
        }

        class TestClient : ClientBase
        {
            public TestClient(INetworkChannel networkChannel) : base(networkChannel)
            {
            }
            public TestClient(IClient networkChannel) : base(networkChannel)
            {
            }

            public IPromise<TestResponse> Unary1(TestRequest request)
            {
                return SingleToSingle<TestRequest, TestResponse>("Unary1", request, new ClientCallOptions());
            }
            public IPromise<TestResponse> Unary2(TestRequest request)
            {
                return SingleToSingle<TestRequest, TestResponse>("Unary2", request, new ClientCallOptions());
            }

            public IAsyncClientStreamingCall<TestRequest, TestResponse> Client1()
            {
                return ManyToSingle<TestRequest, TestResponse>("Client1", new ClientCallOptions());
            }
            public IAsyncClientStreamingCall<TestRequest, TestResponse> Client2()
            {
                return ManyToSingle<TestRequest, TestResponse>("Client2", new ClientCallOptions());
            }

            public IAsyncServerStreamingCall<TestResponse> Server1(TestRequest request)
            {
                return SingleToMany<TestRequest, TestResponse>(
                    "Server1", request,
                    new ClientCallOptions());
            }
            public IAsyncServerStreamingCall<TestResponse> Server2(TestRequest request)
            {
                return SingleToMany<TestRequest, TestResponse>(
                    "Server2", request,
                    new ClientCallOptions());
            }

            public IAsyncDuplexStreamingCall<TestRequest, TestResponse> Duplex1()
            {
                return ManyToMany<TestRequest, TestResponse>(
                    "Duplex1",
                    new ClientCallOptions());
            }
            public IAsyncDuplexStreamingCall<TestRequest, TestResponse> Duplex2()
            {
                return ManyToMany<TestRequest, TestResponse>(
                    "Duplex2",
                    new ClientCallOptions());
            }
        }

        class TestService
        {
            IPromise<TestResponse> Unary1(TestRequest request, ServerCallContext context)
            {
                return Promise<TestResponse>.Resolved(new TestResponse() {Value = request.Value + 1});
            }

            IPromise<TestResponse> Unary2(TestRequest request, ServerCallContext context)
            {
                return Promise<TestResponse>.Resolved(new TestResponse() {Value = request.Value + 2});
            }

            void Server1(TestRequest request, IObserver<TestResponse> responsesStream,
                ServerCallContext context)
            {
                var response = new TestResponse() {Value = request.Value + 1};
                for (int i = 0; i < request.Value; i++)
                    responsesStream.OnNext(response);
                responsesStream.OnCompleted();
            }
            
            void Server2(TestRequest request, IObserver<TestResponse> responsesStream, ServerCallContext context)
            {
                var response = new TestResponse() {Value = request.Value + 2};
                for (int i = 0; i < request.Value; i++)
                    responsesStream.OnNext(response);
                responsesStream.OnCompleted();
            }

            IPromise<TestResponse> Client1(IObservable<TestRequest> requestsStream,
                ServerCallContext context)
            {
                var response = new TestResponse();
                var result = new Promise<TestResponse>();
                var d = requestsStream.Subscribe(request => { response.Value += request.Value + 1; },
                    exception => result.Reject(exception), () => result.Resolve(response));
                return result.Then(r =>
                {
                    d.Dispose();
                    return r;
                });
            }

            IPromise<TestResponse> Client2(IObservable<TestRequest> requestsStream,
                ServerCallContext context)
            {
                var response = new TestResponse();
                var result = new Promise<TestResponse>();
                var d = requestsStream.Subscribe(request => { response.Value += request.Value + 2; },
                    exception => result.Reject(exception), () => result.Resolve(response));
                return result.Then(r =>
                {
                    d.Dispose();
                    return r;
                });
            }

            void Duplex1(IObservable<TestRequest> requestsStream,
                IObserver<TestResponse> responsesStream, ServerCallContext context)
            {
                IDisposable d = null;
                d = requestsStream.Subscribe(request =>
                    {
                        var response = new TestResponse() {Value = request.Value + 1};
                        for (int i = 0; i < request.Value; i++)
                            responsesStream.OnNext(response);
                    },
                    exception =>
                    {
                        responsesStream.OnError(exception);
                        d.Dispose();
                    }, () =>
                    {
                        responsesStream.OnCompleted();
                        d.Dispose();
                    });
            }

            void Duplex2(IObservable<TestRequest> requestsStream,
                IObserver<TestResponse> responsesStream, ServerCallContext context)
            {
                IDisposable d = null;
                d = requestsStream.Subscribe(request =>
                    {
                        var response = new TestResponse() {Value = request.Value + 2};
                        for (int i = 0; i < request.Value; i++)
                            responsesStream.OnNext(response);
                    },
                    exception =>
                    {
                        responsesStream.OnError(exception);
                        d.Dispose();
                    }, () =>
                    {
                        responsesStream.OnCompleted();
                        d.Dispose();
                    });
            }
            

            public ServiceDefinition BindService()
            {
                return new ServiceDefinition()
                    .BindMethod("Unary1", (Func<TestRequest, ServerCallContext, IPromise<TestResponse>>) Unary1)
                    .BindMethod("Unary2", (Func<TestRequest, ServerCallContext, IPromise<TestResponse>>) Unary2)
                    .BindMethod("Server1", (Action<TestRequest, IObserver<TestResponse>, ServerCallContext>) Server1)
                    .BindMethod("Server2", (Action<TestRequest, IObserver<TestResponse>, ServerCallContext>) Server2)
                    .BindMethod("Client1", (Func<IObservable<TestRequest>, ServerCallContext, IPromise<TestResponse>>) Client1)
                    .BindMethod("Client2", (Func<IObservable<TestRequest>, ServerCallContext, IPromise<TestResponse>>) Client2)
                    .BindMethod("Duplex1", (Action<IObservable<TestRequest>, IObserver<TestResponse>, ServerCallContext>) Duplex1)
                    .BindMethod("Duplex2", (Action<IObservable<TestRequest>, IObserver<TestResponse>, ServerCallContext>) Duplex2);
            }
        }

        #endregion

        public NewNetworkInterfacesTest(ITestOutputHelper output) : base(output)
        {
        }

        [Theory]
        [InlineData(19876)]
        [InlineData(19877)]
        [InlineData(19878)]
        public async void TestSendReceiveData(int port)
        {
            var test = CreateTest($"TestNetwork_{port}");

            var serverLog = test.Container.Resolve<ILogService>().GetLog("Network.Server");
            var clientLog = test.Container.Resolve<ILogService>().GetLog("Network.Client");
            
            var portDefine = new ServerPort() {Address = "127.0.0.1", AppId = "Test", Port = port};

            var server = await Valkyrie.Network.Utils.StartServerAsync(portDefine, serverLog);
            var client = await Valkyrie.Network.Utils.CreateClientAsync(portDefine, clientLog);

            var buffer = new byte[] {1, 2, 3, 4};
            var sbuffer = new byte[] {5, 6, 7, 8};
            
            client.Send(buffer, NetDeliveryMethod.ReliableOrdered, 1);
            client.TrafficCompression = 3;
            client.Send(buffer, NetDeliveryMethod.ReliableOrdered, 1);
            client.TrafficCompression = 5;
            client.Send(buffer, NetDeliveryMethod.ReliableOrdered, 1);
            client.TrafficCompression = 9;
            client.Send(buffer, NetDeliveryMethod.ReliableOrdered, 1);
            
            test.Update(5f);
            var receivedMessages = server.PopMessages();

            Assert.Equal(4, receivedMessages.Count);
            foreach (var pair in receivedMessages)
            {
                Assert.Equal(buffer, pair.Value);
                pair.Key.Send(sbuffer, NetDeliveryMethod.ReliableOrdered, 1);
            }

            test.Update(5f);
            receivedMessages.Clear();
            byte[] x;
            while ((x = client.PopMessage()) != null)
                receivedMessages.Add(new SerializedKeyValuePair<IClientView, byte[]>() {Value = x});
            Assert.Equal(4, receivedMessages.Count);
            foreach (var pair in receivedMessages)
            {
                Assert.Equal(sbuffer, pair.Value);
            }
        }

        [Theory]
        [InlineData(19879)]
        [InlineData(19880)]
        public async void TestRpc(int port)
        {
            var test = CreateTest($"TestRpc_{port}");

            var clientLog = test.Container.Resolve<ILogService>().GetLog("Network.Client");
            
            var portDefine = new ServerPort() {Address = "127.0.0.1", AppId = "Test", Port = port};

            var server = new Rpc.Server(test.Container.Resolve<IDispatcher>(), test.Container.Resolve<ILogService>())
            {
                Ports = new[] {portDefine},
                Services = new[] {new TestService().BindService()}
            };

            await server.StartAsync();

            var nc = await Valkyrie.Network.Utils.CreateClientAsync(portDefine, clientLog);
            var client = new TestClient(nc);
            
            test.Update(3f);

            int value = -1;
            client.Unary1(new TestRequest {Value = 5}).Done((response => value = response.Value));
            
            test.Update(5f);

            Assert.Equal(5 + 1, value);
        }
    }
}