using System.Collections.Generic;
using Valkyrie.Ai;
using Valkyrie.Ai.FSM;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.Ai.Fsm
{
    public class FsmUnitTest : GenericUnitTests
    {
        #region FSM Related

        #region Simple Fsm

        class FsmUpdateEvent
        {
            public static FsmUpdateEvent Instance = new FsmUpdateEvent();
        }

        class FsmSwitchState
        {
            public string ns;
        }

        [Fact]
        public void SimpleFsm()
        {
            var fsm = Valkyrie.Ai.Utils.CreateFsm();

            var enterStates = new List<string>();
            var exitStates = new List<string>();
            var updateStates = new List<string>();

            void AddState(string state)
            {
                fsm.In(state)
                    .OnEnter(() => enterStates.Add(state))
                    .OnExit(() => exitStates.Add(state))
                    .On<FsmUpdateEvent>(x => updateStates.Add(state))
                    .On<FsmSwitchState>((tr, switchState) => tr.Switch(switchState.ns));
            }

            AddState("0");
            AddState("1");
            AddState("2");
            AddState("3");

            fsm.Start("0");

            fsm.Raise(FsmUpdateEvent.Instance);
            fsm.Raise(new FsmSwitchState() {ns = "1"});
            fsm.Raise(FsmUpdateEvent.Instance);
            fsm.Raise(new FsmSwitchState() {ns = "2"});
            fsm.Raise(FsmUpdateEvent.Instance);
            fsm.Raise(new FsmSwitchState() {ns = "3"});
            fsm.Raise(FsmUpdateEvent.Instance);
            fsm.Raise(new FsmSwitchState() {ns = "2"});
            fsm.Raise(FsmUpdateEvent.Instance);
            fsm.Raise(new FsmSwitchState() {ns = "1"});
            fsm.Raise(FsmUpdateEvent.Instance);
            fsm.Raise(new FsmSwitchState() {ns = "0"});
            fsm.Raise(FsmUpdateEvent.Instance);

            Assert.Equal("0-1-2-3-2-1-0", string.Join("-", enterStates));
            Assert.Equal("0-1-2-3-2-1", string.Join("-", exitStates));
            Assert.Equal("0-1-2-3-2-1-0", string.Join("-", updateStates));
        }

        #endregion

        #region HFSM

        class HFSMEnemyVisible
        {
            public bool HealthLow;
        }

        class HFSMEnemyNotVisible
        {
            public static HFSMEnemyNotVisible Instance = new HFSMEnemyNotVisible();
        }

        class HFSMTimeLeftEvent
        {
            public static HFSMTimeLeftEvent Instance = new HFSMTimeLeftEvent();
        }

        class HFSMFinishPatrol
        {
            public static HFSMFinishPatrol Instance = new HFSMFinishPatrol();
        }

        [Fact]
        public void HierarhicalFsm()
        {
            var enterStates = new List<object>();
            var exitStates = new List<object>();


            IStateDefinition AddState(IFsm f, object state)
            {
                return f.In(state)
                    .OnEnter(() => enterStates.Add(state))
                    .OnExit(() => exitStates.Add(state));
            }

            IStateMachineDefinition AddInnerState(IFsm f, IFsm state)
            {
                return f.In(state);
            }

            var ncFsm = Valkyrie.Ai.Utils.CreateFsm();
            AddState(ncFsm, "idle")
                .On<HFSMTimeLeftEvent>((tr, ev) => tr.Switch("patrol"));
            AddState(ncFsm, "patrol")
                .On<HFSMFinishPatrol>((tr, ev) => tr.Switch("idle"));
            ncFsm.Start("idle");

            var fsm = Valkyrie.Ai.Utils.CreateFsm();

            AddState(fsm, "search")
                .On<HFSMEnemyVisible>((tr, ev) =>
                {
                    if (ev.HealthLow)
                        tr.Switch("flee");
                    else
                        tr.Switch("attack");
                })
                .On<HFSMTimeLeftEvent>((tr, ev) => { tr.Switch(ncFsm); });
            AddState(fsm, "attack")
                .On<HFSMEnemyNotVisible>((tr, ev) => tr.Switch(ncFsm))
                .On<HFSMEnemyVisible>((tr, ev) =>
                {
                    if (ev.HealthLow)
                        tr.Switch("flee");
                });
            AddState(fsm, "flee")
                .On<HFSMEnemyNotVisible>((tr, ev) => tr.Switch(ncFsm));
            AddInnerState(fsm, ncFsm)
                .ResetOnEnter()
                .On<HFSMEnemyVisible>((tr, ev) =>
                {
                    if (ev.HealthLow)
                        tr.Switch("flee");
                    else
                        tr.Switch("attack");
                });

            fsm.Start(ncFsm);

            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMFinishPatrol.Instance);
            fsm.Raise(HFSMEnemyNotVisible.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMFinishPatrol.Instance);
            fsm.Raise(HFSMEnemyNotVisible.Instance);
            fsm.Raise(new HFSMEnemyVisible {HealthLow = false});
            fsm.Raise(new HFSMEnemyVisible {HealthLow = true});
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMFinishPatrol.Instance);
            fsm.Raise(HFSMEnemyNotVisible.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMFinishPatrol.Instance);
            fsm.Raise(HFSMEnemyNotVisible.Instance);
            fsm.Raise(new HFSMEnemyVisible {HealthLow = true});
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMTimeLeftEvent.Instance);
            fsm.Raise(HFSMFinishPatrol.Instance);
            fsm.Raise(HFSMEnemyNotVisible.Instance);

            Assert.Equal("idle-patrol-idle-patrol-idle-attack-flee-idle-patrol-idle-flee-idle",
                string.Join("-", enterStates));
            Assert.Equal("idle-patrol-idle-patrol-idle-attack-flee-idle-patrol-idle-flee",
                string.Join("-", exitStates));
        }

        #endregion

        #region Fsm From Object

        [FsmStartState("idle")]
        class TestSimpleFsm
        {
            public List<string> EnterStates = new List<string>();
            public List<string> ExitStates = new List<string>();

            [FsmEnterState("idle")]
            [FsmEnterState("patrol")]
            [FsmEnterState("search")]
            [FsmEnterState("flee")]
            [FsmEnterState("attack")]
            void EnterState(IFsm f)
            {
                EnterStates.Add(f.State.ToString());
            }

            [FsmExitState("idle")]
            [FsmExitState("patrol")]
            [FsmExitState("search")]
            [FsmExitState("flee")]
            [FsmExitState("attack")]
            void ExitState(IFsm f)
            {
                ExitStates.Add(f.State.ToString());
            }

            [FsmEventState("idle", EventType = typeof(HFSMTimeLeftEvent))]
            void IdleReceiveTime(IFsmTransitor fsm)
            {
                fsm.Switch("patrol");
            }

            [FsmEventState("patrol", EventType = typeof(HFSMFinishPatrol))]
            void OnPatrolFinish(IFsmTransitor fsm)
            {
                fsm.Switch("idle");
            }

            [FsmEventState("search", EventType = typeof(HFSMEnemyVisible))]
            [FsmEventState("idle", EventType = typeof(HFSMEnemyVisible))]
            [FsmEventState("patrol", EventType = typeof(HFSMEnemyVisible))]
            void OnEnemyVisible(IFsmTransitor tr, HFSMEnemyVisible ev)
            {
                tr.Switch(ev.HealthLow ? "flee" : "attack");
            }

            [FsmEventState("search", EventType = typeof(HFSMTimeLeftEvent))]
            [FsmEventState("attack", EventType = typeof(HFSMEnemyNotVisible))]
            [FsmEventState("flee", EventType = typeof(HFSMEnemyNotVisible))]
            void OnTimeSearchLeft(IFsmTransitor tr)
            {
                tr.Switch("idle");
            }

            [FsmEventState("attack", EventType = typeof(HFSMEnemyVisible))]
            void OnAttackLostHp(IFsmTransitor tr, HFSMEnemyVisible ev)
            {
                if (ev.HealthLow)
                    tr.Switch("flee");
            }
        }

        [Fact]
        public void ObjectToFsm()
        {
            using (var test = CreateTest("ObjectToFsm"))
            {
                var o = new TestSimpleFsm();
                var fsm = test.Container.ToFsm(o);

                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMFinishPatrol.Instance);
                fsm.Raise(HFSMEnemyNotVisible.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMFinishPatrol.Instance);
                fsm.Raise(HFSMEnemyNotVisible.Instance);
                fsm.Raise(new HFSMEnemyVisible {HealthLow = false});
                fsm.Raise(new HFSMEnemyVisible {HealthLow = true});
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMFinishPatrol.Instance);
                fsm.Raise(HFSMEnemyNotVisible.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMFinishPatrol.Instance);
                fsm.Raise(HFSMEnemyNotVisible.Instance);
                fsm.Raise(new HFSMEnemyVisible {HealthLow = true});
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMTimeLeftEvent.Instance);
                fsm.Raise(HFSMFinishPatrol.Instance);
                fsm.Raise(HFSMEnemyNotVisible.Instance);

                Assert.Equal("idle-patrol-idle-patrol-idle-attack-flee-idle-patrol-idle-flee-idle",
                    string.Join("-", o.EnterStates));
                Assert.Equal("idle-patrol-idle-patrol-idle-attack-flee-idle-patrol-idle-flee",
                    string.Join("-", o.ExitStates));
            }
        }

        #endregion

        #endregion
    }
}