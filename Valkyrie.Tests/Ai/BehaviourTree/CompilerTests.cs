using Valkyrie.Ai.Behaviour;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.Ai.BehaviourTree
{
    public class CompilerTests : GenericUnitTests
    {
        private readonly ITestOutputHelper _output;

        public CompilerTests(ITestOutputHelper output)
        {
            _output = output;
        }

        BehaviourTreeCompiler Init(string name)
        {
            var test = CreateTest(name);
            test.Container
                .RegisterLibrary(Libraries.Ai())
                .RegisterLibrary(Libraries.Scripts)
                .Build();
            var compiler = test.Container.Resolve<BehaviourTreeCompiler>();
            return compiler;
        }

        [Theory]
        [InlineData("[ al.fa() ]")]
        [InlineData("select { [ al.fa() ] [alfa()] }")]
        [InlineData("select { select { [al.fa()] [alfa()] } [alfa()] }")]
        [InlineData("select { sequence { [al.fa()] [alfa()] } [alfa()] }")]
        [InlineData("select { if [ab()] [true()] sequence { [idle() patrol()] } }")]
        public void ConstructFromSource(string define)
        {
            var compiler = Init("ConstructFromSource");

                var tree = compiler.Compile(define);

                _output.WriteLine(tree.ToString());
        }

        [Theory]
        [InlineData("[ new BehaviourTreeStatus.Running ]")]
        public void CanRunAction(string define)
        {
            var compiler = Init("CanRunAction");
            var time = new NodeCheckArgs();
            var invokeCount = 0;
            var testObject = compiler.Compile(define);
            Assert.Equal(BehaviourTreeStatus.Running, testObject.Tick(time));
            Assert.Equal(1, invokeCount);     
        }

        [Theory]
        [InlineData("if [ false ] [false aadf]")]
        [InlineData("if [ true ] [false aadf]")]
        public void CheckConditionAction(string define)
        {
            var compiler = Init("CheckConditionAction");
            var time = new NodeCheckArgs();
            var testObject = compiler.Compile(define);
            Assert.Equal(BehaviourTreeStatus.Failure, testObject.Tick(time));
        }
    }
}