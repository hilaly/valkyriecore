using Valkyrie.Ai.Behaviour;
using Xunit;

namespace Valkyrie.Tests.Ai.BehaviourTree
{
    public class ActionNodeTests
    {
        [Fact]
        public void can_run_action()
        {
            var time = new NodeCheckArgs();

            var invokeCount = 0;
            var testObject = 
                new ActionNode(
                    "some-action", 
                    t =>
                    {
                        Assert.Equal(time, t);

                        ++invokeCount;
                        return BehaviourTreeStatus.Running;
                    }
                );

            Assert.Equal(BehaviourTreeStatus.Running, testObject.Tick(time));
            Assert.Equal(1, invokeCount);            
        }
    }
}