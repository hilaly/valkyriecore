using System.IO;
using System.Reflection;
using System.Text;
using Valkyrie.Data;
using Valkyrie.Ecs;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.Ecs
{
    public class EcsParserTests : GenericUnitTests
    {
        private readonly ITestOutputHelper _output;

        private const string ecsText = "syntax = \"ecs1\";" +
                                       "alias string = \"System.String\";" +
                                       "include System;" +
                                       "context core(default), coreA;" +
                                       "alias String = \"System.String\", Int64 = \"System.Int64\";" +
                                       "    namespace Sea.Shared.Game;" +
                                       "context game(namePrefix:superPrefix, mustExist), temp;" +
                                       "component Comp2;" +
                                       "component Comp3{}" +
                                       "component Comp4 as int;" +
                                       "  component Comp1 in game, temp" +
                                       " {" +
                                       "    val1 as bool;" +
                                       "}" +
                                       "component TempArgs(key:value, key1:value), TempWithoutArgs in game" +
                                       "{" +
                                       "    value as int;" +
                                       "    value1, value2 as string;" +
                                       "" +
                                       "           method TestAbstractMethod1(p1 as string, p2 as int) as void;" +
                                       "         method TestAbstractMethod2(p1 as string) as void;" +
                                       "        method TestAbstractMethod3() as void;" +
                                       "   }";

        private const string ecsTestText = "syntax = \"ecs1\";" +
                                           "context Test(default);" +
                                           "component EntityComponent0, EntityComponent1, EntityComponent2, EntityComponent3;";

        public EcsParserTests(ITestOutputHelper output) : base(output)
        {
            _output = output;
        }

        [Fact]
        public void TestLexer()
        {
            {
                var outStream = new MemoryStream();
                using (var fs = new MemoryStream(Encoding.UTF8.GetBytes(ecsText)))
                {
                    using (var sw = new StreamWriter(outStream))
                    {
                        EcsFileParser.GetParser().GetLexer().Parse(fs).ForEach(u => _output.WriteLine(u.ToString()));
                    }
                }
            }
        }
        [Fact]
        public void TestParser()
        {
            {
                var outStream = new MemoryStream();
                using (var fs = new MemoryStream(Encoding.UTF8.GetBytes(ecsText)))
                {
                    using (var sw = new StreamWriter(outStream))
                    {
                        var ast = EcsFileParser.GetParser().Parse(fs);
                        _output.WriteLine(ast.ToString());
                    }
                }
            }
        }
        [Fact]
        public void TestCompiler()
        {
            {
                var outStream = new MemoryStream();
                using (var fs = new MemoryStream(Encoding.UTF8.GetBytes(ecsText)))
                {
                    using (var sw = new StreamWriter(outStream) { AutoFlush = true })
                    {
                        EcsFileParser.Parse(fs, sw);
                        outStream.Seek(0, SeekOrigin.Begin);
                        _output.WriteLine(outStream.ReadAllText());
                    }
                }
            }
        }
        [Fact]
        public void TestCompilerTest()
        {
            {
                var outStream = new MemoryStream();
                using (var fs = new MemoryStream(Encoding.UTF8.GetBytes(ecsTestText)))
                {
                    using (var sw = new StreamWriter(outStream) { AutoFlush = true })
                    {
                        EcsFileParser.Parse(fs, sw);
                        outStream.Seek(0, SeekOrigin.Begin);
                        _output.WriteLine(outStream.ReadAllText());
                    }
                }
            }
        }
    }
}