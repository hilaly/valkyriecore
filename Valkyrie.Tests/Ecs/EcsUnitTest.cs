﻿using System.Diagnostics;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Engines;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.Ecs
{
    public class EcsUnitTest : GenericUnitTests
    {
        private readonly ITestOutputHelper _output;

        public EcsUnitTest(ITestOutputHelper output) : base(output)
        {
            _output = output;
        }

        #region Generted

        public partial class Contexts : EcsRoot
        {
            public TestContext Test { get; } = new TestContext();
        }

// Args: default:
        public sealed class TestContext : Context<TestEntity>
        {
            //List of components names
            internal static string[] ComponentNames =
            {
                "EntityComponent0Component",
                "EntityComponent1Component",
                "EntityComponent2Component",
                "EntityComponent3Component",
            };

            //List of components types
            internal static System.Type[] ComponentTypes =
            {
                typeof(EntityComponent0Component),
                typeof(EntityComponent1Component),
                typeof(EntityComponent2Component),
                typeof(EntityComponent3Component),
            };

            //List of components indices
            internal static int EntityComponent0ComponentIndex = 0;
            internal static int EntityComponent1ComponentIndex = 1;
            internal static int EntityComponent2ComponentIndex = 2;
            internal static int EntityComponent3ComponentIndex = 3;

            public TestContext()
                : base(4)
            {
            }

            protected override TestEntity CreateEntityImpl()
            {
                return new TestEntity();
            }
        }

        public sealed class TestEntity : Entity
        {
            public TestEntity() : base(4)
            {
            }

            public bool EntityComponent0
            {
                get => HasComponent(TestContext.EntityComponent0ComponentIndex);
                set
                {
                    if (value == EntityComponent0) return;
                    if (value)
                        AddComponent(new EntityComponent0Component(), TestContext.EntityComponent0ComponentIndex);
                    else
                        RemoveComponent(TestContext.EntityComponent0ComponentIndex);
                }
            }

            public bool EntityComponent1
            {
                get => HasComponent(TestContext.EntityComponent1ComponentIndex);
                set
                {
                    if (value == EntityComponent1) return;
                    if (value)
                        AddComponent(new EntityComponent1Component(), TestContext.EntityComponent1ComponentIndex);
                    else
                        RemoveComponent(TestContext.EntityComponent1ComponentIndex);
                }
            }

            public bool EntityComponent2
            {
                get => HasComponent(TestContext.EntityComponent2ComponentIndex);
                set
                {
                    if (value == EntityComponent2) return;
                    if (value)
                        AddComponent(new EntityComponent2Component(), TestContext.EntityComponent2ComponentIndex);
                    else
                        RemoveComponent(TestContext.EntityComponent2ComponentIndex);
                }
            }

            public bool EntityComponent3
            {
                get => HasComponent(TestContext.EntityComponent3ComponentIndex);
                set
                {
                    if (value == EntityComponent3) return;
                    if (value)
                        AddComponent(new EntityComponent3Component(), TestContext.EntityComponent3ComponentIndex);
                    else
                        RemoveComponent(TestContext.EntityComponent3ComponentIndex);
                }
            }
        }

        public sealed partial class TestMatcher
        {
            static IMatcher<TestEntity> _EntityComponent0ComponentInstance;

            public static IMatcher<TestEntity> EntityComponent0 => _EntityComponent0ComponentInstance ??
                (_EntityComponent0ComponentInstance =
                    Matcher<TestEntity>.ComponentMatcher(TestContext.EntityComponent0ComponentIndex));

            static IMatcher<TestEntity> _EntityComponent1ComponentInstance;

            public static IMatcher<TestEntity> EntityComponent1 => _EntityComponent1ComponentInstance ??
                (_EntityComponent1ComponentInstance =
                    Matcher<TestEntity>.ComponentMatcher(TestContext.EntityComponent1ComponentIndex));

            static IMatcher<TestEntity> _EntityComponent2ComponentInstance;

            public static IMatcher<TestEntity> EntityComponent2 => _EntityComponent2ComponentInstance ??
                (_EntityComponent2ComponentInstance =
                    Matcher<TestEntity>.ComponentMatcher(TestContext.EntityComponent2ComponentIndex));

            static IMatcher<TestEntity> _EntityComponent3ComponentInstance;

            public static IMatcher<TestEntity> EntityComponent3 => _EntityComponent3ComponentInstance ??
                (_EntityComponent3ComponentInstance =
                    Matcher<TestEntity>.ComponentMatcher(TestContext.EntityComponent3ComponentIndex));

            public static IAllOfMatcher<TestEntity> AllOf(
                params IMatcher<TestEntity>[] matchers)
            {
                return Matcher<TestEntity>.AllOf(matchers);
            }

            public static IAnyOfMatcher<TestEntity> AnyOf(
                params IMatcher<TestEntity>[] matchers)
            {
                return Matcher<TestEntity>.AnyOf(matchers);
            }

            public static IMatcher<TestEntity> NoneOf(params IMatcher<TestEntity>[] matchers)
            {
                return Matcher<TestEntity>.NoneOf(matchers);
            }
        }

// Args: 
        public sealed partial class
            EntityComponent0Component : Valkyrie.Ecs.Entities.IEcsComponent //TODO: generate base class
        {
        }

// Args: 
        public sealed partial class
            EntityComponent1Component : Valkyrie.Ecs.Entities.IEcsComponent //TODO: generate base class
        {
        }

// Args: 
        public sealed partial class
            EntityComponent2Component : Valkyrie.Ecs.Entities.IEcsComponent //TODO: generate base class
        {
        }

// Args: 
        public sealed partial class
            EntityComponent3Component : Valkyrie.Ecs.Entities.IEcsComponent //TODO: generate base class
        {
        }

        #endregion

        public class InitEngine : IEcsInitializeEngine, IInitializeSystem
        {
            private int _counter;

            public void Initialize()
            {
                _counter++;
            }

            public int Counter => _counter;
        }

        public class ExecEngine : IEcsExecuteEngine, IExecuteSystem
        {
            private int _counter;

            public void Execute(Iteration iterationInfo)
            {
                Execute();
            }

            public int Counter => _counter;

            public void Execute()
            {
                _counter++;
            }
        }

        public class CleanEngine : IEcsCleanupEngine, ICleanupSystem
        {
            private int _counter;

            public void Cleanup()
            {
                _counter++;
            }

            public int Counter
            {
                get => _counter;
            }
        }

        [Fact]
        public void TestMatchers()
        {
            int totalComponentsCount = 28;

            var context = new TestContext();
            for (var allComp = 0; allComp < totalComponentsCount; ++allComp)
            {
                var allIndices = new int[allComp + 1];
                for (var i = 0; i <= allComp; ++i)
                    allIndices[i] = i;

                for (var anyComp = 0; anyComp < 4; ++anyComp)
                {
                    var anIndices = new int[anyComp + 1];
                    for (var i = 0; i <= anyComp; ++i)
                        anIndices[i] = i;

                    for (var noneComp = 5; noneComp < totalComponentsCount; ++noneComp)
                    {
                        var noneIndices = new int[noneComp];
                        for (var i = 0; i < noneComp; ++i)
                            noneIndices[i] = i + 1;

                        var group1 = context.GetGroup(Matcher<TestEntity>.AllOf(allIndices).AnyOf(anIndices)
                            .NoneOf(noneIndices));
                        var group2 = context.GetGroup(Matcher<TestEntity>.AllOf(allIndices).AnyOf(anIndices)
                            .NoneOf(noneIndices));
                        Assert.Same(group1, group2);

                        var group3 = context.GetGroup(Matcher<TestEntity>.AllOf(noneIndices).AnyOf(anIndices)
                            .NoneOf(allIndices));
                        var group4 = context.GetGroup(Matcher<TestEntity>.AllOf(noneIndices).AnyOf(anIndices)
                            .NoneOf(allIndices));
                        Assert.Same(group3, group4);

                        var group5 = context.GetGroup(Matcher<TestEntity>.AllOf(allIndices).AnyOf(noneIndices)
                            .NoneOf(anIndices));
                        var group6 = context.GetGroup(Matcher<TestEntity>.AllOf(allIndices).AnyOf(noneIndices)
                            .NoneOf(anIndices));
                        Assert.Same(group5, group6);

                        _output.WriteLine(
                            $"all=[{string.Join(",", allComp)}] any=[{string.Join(",", anIndices)}] non=[{string.Join(",", noneComp)}]");

                        Assert.NotSame(group1, group3);
                        Assert.NotSame(group1, group5);
                        Assert.NotSame(group3, group5);
                    }
                }
            }
        }

        [Fact]
        public void EcsGroup1Test()
        {
            {
                var ecsContext = new TestContext();
                var genericGroup4 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1));
                Assert.Equal(0, genericGroup4.Count);
                TestEntity e;

                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;
                Assert.Equal(1, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);

                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;
                Assert.Equal(0, genericGroup4.Count);

                e.EntityComponent1 = true;
                Assert.Equal(1, genericGroup4.Count);

                e.EntityComponent1 = false;
                Assert.Equal(0, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);
            }
        }

        [Fact]
        public void EcsGroup2Test()
        {
            {
                var ecsContext = new TestContext();
                var genericGroup4 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1,
                        TestMatcher.EntityComponent2, TestMatcher.EntityComponent3));
                Assert.Equal(0, genericGroup4.Count);
                TestEntity e;

                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;
                Assert.Equal(1, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);

                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                Assert.Equal(0, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);
            }
        }

        [Fact]
        public void EcsGroup3Test()
        {
            {
                var ecsContext = new TestContext();
                var genericGroup4 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1,
                        TestMatcher.EntityComponent2, TestMatcher.EntityComponent2));
                Assert.Equal(0, genericGroup4.Count);
                TestEntity e;
                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;
                Assert.Equal(1, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);

                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                Assert.Equal(1, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);
            }
        }

        [Fact]
        public void EcsGroup4Test()
        {
            {
                var ecsContext = new TestContext();
                var genericGroup4 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1,
                        TestMatcher.EntityComponent2, TestMatcher.EntityComponent3));
                Assert.Equal(0, genericGroup4.Count);
                TestEntity e;
                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;
                Assert.Equal(1, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);

                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                Assert.Equal(0, genericGroup4.Count);

                e.Destroy();
                Assert.Equal(0, genericGroup4.Count);
            }
        }

        [Fact]
        public void EcsSameGroup4Test()
        {
            {
                var ecsContext = new TestContext();
                var genericGroup0 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1,
                        TestMatcher.EntityComponent2, TestMatcher.EntityComponent3));
                var genericGroup4 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1,
                        TestMatcher.EntityComponent2, TestMatcher.EntityComponent3));

                Assert.Same(genericGroup0, genericGroup4);
            }
        }

        [Fact]
        public void EcsEntityComponentsWorkingTest()
        {
            {
                var ecsContext = new TestContext();
                var genericGroup4 =
                    ecsContext.GetGroup(TestMatcher.AllOf(TestMatcher.EntityComponent0, TestMatcher.EntityComponent1,
                        TestMatcher.EntityComponent2, TestMatcher.EntityComponent3));

                Assert.Equal(0, genericGroup4.Count);

                var entity = ecsContext.CreateEntity();
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent0 = true;
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent1 = true;
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent2 = true;
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent3 = true;
                Assert.Equal(1, genericGroup4.Count);

                entity.EntityComponent0 = false;
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent1 = false;
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent2 = false;
                Assert.Equal(0, genericGroup4.Count);

                entity.EntityComponent3 = false;
                Assert.Equal(0, genericGroup4.Count);

                entity.Destroy();
            }
        }

        [Fact]
        public void EcsEntityComponentsGetComponentGeneric()
        {
            {
                var ecsContext = new TestContext();
                TestEntity e;
                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;

                var s = Stopwatch.StartNew();
                s.Start();
                var start = s.ElapsedTicks;
                for (int i = 0; i < 12345; ++i)
                {
                    var temp = e.EntityComponent3;
                }

                var end = s.ElapsedTicks;
                _output.WriteLine("123456789 iterations in {0} sec",
                    (float) (((double) (end - start)) / Stopwatch.Frequency));
            }
        }

        [Fact]
        public void EcsEntityComponentsGetComponent()
        {
            {
                var ecsContext = new TestContext();
                TestEntity e;
                e = ecsContext.CreateEntity();
                e.EntityComponent0 = true;
                e.EntityComponent1 = true;
                e.EntityComponent2 = true;
                e.EntityComponent3 = true;

                var s = Stopwatch.StartNew();
                s.Start();
                var start = s.ElapsedTicks;
                for (int i = 0; i < 12345; ++i)
                {
                    var temp = e.HasComponent(TestContext.EntityComponent3ComponentIndex);
                }

                var end = s.ElapsedTicks;
                _output.WriteLine("123456789 iterations in {0} sec",
                    (float) (((double) (end - start)) / Stopwatch.Frequency));
            }
        }

        [Fact]
        public void EcsSystemWorking()
        {
            using (var test = CreateTest("ECS Systems test"))
            {
                var ctx = new Contexts();
                var systems = ctx.Systems;
                var ieng = new InitEngine();
                var eeng = new ExecEngine();
                var ceng = new CleanEngine();
                systems.Add(ieng);
                systems.Add(eeng);
                systems.Add(ceng);

                var iterationCount = 5;
                for (int i = 0; i < iterationCount; ++i)
                {
                    ctx.Execute();
                }
                
                ctx.Shutdown();

                Assert.Equal(1, ieng.Counter);
                Assert.Equal(iterationCount, eeng.Counter);
                Assert.Equal(iterationCount, ceng.Counter);
            }
        }
    }
}