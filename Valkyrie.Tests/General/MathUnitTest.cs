using System.Numerics;
using Valkyrie.Data;
using Valkyrie.Math;
using Xunit;

namespace Valkyrie.Tests.General
{
    public class MathUnitTest : GenericUnitTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(30)]
        [InlineData(60)]
        [InlineData(90)]
        [InlineData(-30)]
        [InlineData(-180)]
        [InlineData(180)]
        [InlineData(360)]
        [InlineData(120)]
        public void TestRotate(float angle)
        {
            Assert.Equal(angle.NormalizeSigned(), MathExtensions.SignedAngle(Vector2.UnitY, Vector2.UnitY.Rotate(angle)), 2);
        }

        [Fact]
        public void FloatStringConverting()
        {
            Assert.Equal("1.1", 1.1f.WriteAsString());
            Assert.Equal("0.1", 0.1f.WriteAsString());
            Assert.Equal("-1.1", (-1.1f).WriteAsString());
            
            Assert.Equal(1.1f, "1.1".ReadFloat());
            Assert.Equal(0.1f, "0.1".ReadFloat());
            Assert.Equal(-1.1f, "-1.1".ReadFloat());
        }
    }
}