using System.Collections.Specialized;
using Valkyrie.Data;
using Valkyrie.Math;
using Xunit;
using Xunit.Abstractions;

namespace Valkyrie.Tests.General
{
    public class BigNumbersUnitTest : GenericUnitTests
    {
        [Fact]
        public void LongToString()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                var temp = 1L;
                for (var i = 0; i < 20; ++i)
                {
                    temp *= 10;
                    test.Log(temp.ToBigNumberString());
                }
            }
        }
        
        
        [Fact]
        public void BigNumberToString()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int value)
                {
                    var bn = new BigNumber(value);
                    test.Log($"{value} => {bn}");
                    Assert.True(value.ToString() == bn.ToString());
                }

                Compare(1);
                Compare(513);
                Compare(986333315);
                Compare(-561);
                Compare(0);
                Compare(-1);
                Compare(-26111668);
                Compare(-135466);
            }
        }
        
        [Fact]
        public void BigNumberSignInverseToString()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int value)
                {
                    var bn = - (new BigNumber(value));
                    value = -value;
                    test.Log($"{value} => {bn}");
                    Assert.True(value.ToString() == bn.ToString());
                }

                Compare(1);
                Compare(513);
                Compare(986333315);
                Compare(-561);
                Compare(0);
                Compare(-1);
                Compare(-26111668);
                Compare(-135466);
            }
        }
        
        [Fact]
        public void BigNumberAdd()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int v0, int v1)
                {
                    var bn0 = new BigNumber(v0);
                    var bn1 = new BigNumber(v1);
                    
                    var result = v0 + v1;
                    var bnResult = bn0 + bn1;

                    test.Log($"{v0}+{v1}={result} => {bn0}+{bn1}={bnResult}");
                    Assert.True(result.ToString() == bnResult.ToString());
                }

                Compare(0,0);
                Compare(0,534);
                Compare(0,-534);
                Compare(13,-534);
                Compare(-13,-534);
                Compare(13,534);
                Compare(-13,534);
                
                Compare(256,256);
                Compare(256,-256);
                Compare(-256,256);
                Compare(-256,-256);
            }
        }
        
        [Fact]
        public void BigNumberRem()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int v0, int v1)
                {
                    var bn0 = new BigNumber(v0);
                    var bn1 = new BigNumber(v1);
                    
                    var result = v0 - v1;
                    var bnResult = bn0 - bn1;

                    test.Log($"{v0}-{v1}={result} => {bn0}-{bn1}={bnResult}");
                    Assert.True(result.ToString() == bnResult.ToString());
                }

                Compare(0,0);
                Compare(0,534);
                Compare(0,-534);
                Compare(13,-534);
                Compare(-13,-534);
                Compare(13,534);
                Compare(-13,534);
                
                Compare(256,256);
                Compare(256,-256);
                Compare(-256,256);
                Compare(-256,-256);
            }
        }
        
        [Fact]
        public void BigNumberMore()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int v0, int v1)
                {
                    var bn0 = new BigNumber(v0);
                    var bn1 = new BigNumber(v1);
                    
                    var result = v0 > v1;
                    var bnResult = bn0 > bn1;

                    test.Log($"{v0}>{v1}={result} => {bn0}>{bn1}={bnResult}");
                    Assert.True(result.ToString() == bnResult.ToString());
                }

                Compare(0,0);
                Compare(0,534);
                Compare(0,-534);
                Compare(13,-534);
                Compare(-13,-534);
                Compare(13,534);
                Compare(-13,534);
                
                Compare(256,256);
                Compare(256,-256);
                Compare(-256,256);
                Compare(-256,-256);
            }
        }
        
        [Fact]
        public void BigNumberLess()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int v0, int v1)
                {
                    var bn0 = new BigNumber(v0);
                    var bn1 = new BigNumber(v1);
                    
                    var result = v0 < v1;
                    var bnResult = bn0 < bn1;

                    test.Log($"{v0}<{v1}={result} => {bn0}<{bn1}={bnResult}");
                    Assert.True(result.ToString() == bnResult.ToString());
                }

                Compare(0,0);
                Compare(0,534);
                Compare(0,-534);
                Compare(13,-534);
                Compare(-13,-534);
                Compare(13,534);
                Compare(-13,534);
                
                Compare(256,256);
                Compare(256,-256);
                Compare(-256,256);
                Compare(-256,-256);
            }
        }
        
        [Fact]
        public void BigNumberEquals()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int v0, int v1)
                {
                    var bn0 = new BigNumber(v0);
                    var bn1 = new BigNumber(v1);
                    
                    var result = v0 == v1;
                    var bnResult = bn0 == bn1;

                    test.Log($"{v0}=={v1}={result} => {bn0}=={bn1}={bnResult}");
                    Assert.True(result.ToString() == bnResult.ToString());
                }

                Compare(0,0);
                Compare(0,534);
                Compare(0,-534);
                Compare(13,-534);
                Compare(-13,-534);
                Compare(13,534);
                Compare(-13,534);
                
                Compare(256,256);
                Compare(256,-256);
                Compare(-256,256);
                Compare(-256,-256);
            }
        }
        
        [Fact]
        public void BigNumberNotEquals()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int v0, int v1)
                {
                    var bn0 = new BigNumber(v0);
                    var bn1 = new BigNumber(v1);
                    
                    var result = v0 != v1;
                    var bnResult = bn0 != bn1;

                    test.Log($"{v0}!={v1}={result} => {bn0}!={bn1}={bnResult}");
                    Assert.True(result.ToString() == bnResult.ToString());
                }

                Compare(0,0);
                Compare(0,534);
                Compare(0,-534);
                Compare(13,-534);
                Compare(-13,-534);
                Compare(13,534);
                Compare(-13,534);
                
                Compare(256,256);
                Compare(256,-256);
                Compare(-256,256);
                Compare(-256,-256);
            }
        }
        
        [Fact]
        public void BigNumberFromString()
        {
            using (var test = CreateTest(System.Reflection.MethodBase.GetCurrentMethod().Name))
            {
                void Compare(int value)
                {
                    var bn = new BigNumber(value);
                    var result = BigNumber.Parse(bn.ToString());
                    test.Log($"{value} => {bn} => {result}");
                    Assert.True(result == bn);
                }

                Compare(1);
                Compare(513);
                Compare(986333315);
                Compare(-561);
                Compare(0);
                Compare(-1);
                Compare(-26111668);
                Compare(-135466);
            }
        }
    }
}