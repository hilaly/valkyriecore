using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Valkyrie.Ecs;
using Valkyrie.Rpc;

namespace Valkyrie
{
    public class ValkyriePreferences : AssetPostprocessor
    {
        private const string PreferencesEcsCompilerEnabled = "ValkyrieUnity_EcsCompiler_Enabled";
        private const string PreferencesProtoCompilerEnabled = "ValkyrieUnity_ProtoCompiler_Enabled";

        private static bool EcsCompilationEnabled
        {
            get => EditorPrefs.GetBool(PreferencesEcsCompilerEnabled, true);
            set => EditorPrefs.SetBool(PreferencesEcsCompilerEnabled, value);
        }

        private static bool ProtoCompilationEnabled
        {
            get => EditorPrefs.GetBool(PreferencesProtoCompilerEnabled, true);
            set => EditorPrefs.SetBool(PreferencesProtoCompilerEnabled, value);
        }

        static string[] GetAllFiles(string searchPattern)
        {
            return Directory.GetFiles(Application.dataPath, searchPattern, SearchOption.AllDirectories);
        }

        private static string[] AllProtoFiles => GetAllFiles("*.proto");
        private static string[] AllEcsFiles => GetAllFiles("*.ecs");

        [PreferenceItem("Valkyrie")]
        static void PreferencesItem()
        {
            EditorGUI.BeginChangeCheck();

            EcsCompilationEnabled =
                EditorGUILayout.Toggle(new GUIContent("Enable *.ecs files process", ""), EcsCompilationEnabled);
            EditorGUI.BeginDisabledGroup(!EcsCompilationEnabled);
            if (GUILayout.Button(new GUIContent("Parse all ecs files")))
                CompileAllInProject("ecs", AllEcsFiles, CompileEcs);
            EditorGUI.EndDisabledGroup();

            GUILayout.Space(10);

            ProtoCompilationEnabled = EditorGUILayout.Toggle(new GUIContent("Enable *.proto files process", ""),
                ProtoCompilationEnabled);
            EditorGUI.BeginDisabledGroup(!ProtoCompilationEnabled);
            if (GUILayout.Button(new GUIContent("Parse all proto files")))
                CompileAllInProject("proto", AllProtoFiles, CompileProto);
            EditorGUI.EndDisabledGroup();
        }

        #region Compilation impl

        private static void CompileAllInProject(string ext, string[] files, Func<string, bool> compileCall)
        {
            try
            {
                for (int i = 0; i < files.Length; i++)
                {
                    var fn = files[i];
                    EditorUtility.DisplayProgressBar($"Compile *.{ext} files", fn, (float) i / files.Length);
                    compileCall(fn);
                }

                AssetDatabase.Refresh();
                Debug.Log($"Valkyrie.{ext}: SUCCESS");
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Debug.Log($"Valkyrie.{ext}: FAILED");
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

        private static bool CompileEcs(string fn)
        {
            if (Path.GetExtension(fn) == ".ecs")
            {
                Debug.Log($"Valkyrie.Ecs: compiling {fn}");
                var directory = Path.GetDirectoryName(fn);
                var filename = Path.GetFileNameWithoutExtension(fn);
                var targetFn = Path.Combine(directory, $"{filename}.cs");
                using (var os = File.CreateText(targetFn))
                using (var fs = File.OpenRead(fn))
                {
                    try
                    {
                        EcsFileParser.Parse(fs, os);
                        Debug.Log($"Valkyrie.Ecs: compiled {targetFn}");
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"Valkyrie.Ecs: parse of {targetFn} FAILED, {e}");
                        return false;
                    }
                }
                return true;
            }

            return false;
        }

        private static bool CompileProto(string fn)
        {
            if (Path.GetExtension(fn) == ".proto")
            {
                Debug.Log($"Valkyrie.proto: compiling {fn}");
                var directory = Path.GetDirectoryName(fn);
                var filename = Path.GetFileNameWithoutExtension(fn);
                var targetFn = Path.Combine(directory, $"{filename}.cs");
                using (var os = File.CreateText(targetFn))
                using (var fs = File.OpenRead(fn))
                {
                    try
                    {
                        ProtoParser.Parse(fs, os);
                        Debug.Log($"Valkyrie.proto: compiled {targetFn}");
                    }
                    catch (Exception e)
                    {
                        Debug.LogError($"Valkyrie.proto: parse of {targetFn} FAILED, {e}");
                        return false;
                    }
                }
                return true;
            }

            return false;
        }

        #endregion

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            if (!EcsCompilationEnabled && !ProtoCompilationEnabled)
                return;

            var anyChanges = false;
            foreach (var assetPath in importedAssets)
            {
                var filePath = Path.Combine(Directory.GetParent(Application.dataPath).FullName, assetPath);
                if (CompileEcs(filePath) || CompileProto(filePath))
                    anyChanges = true;
            }

            if (anyChanges)
                AssetDatabase.Refresh();
        }
    }
}