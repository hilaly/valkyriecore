using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using Valkyrie.Data.Config;
using Valkyrie.Data.Localization;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.Scripts.Tools;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;
using Valkyrie.UnityExtensions;
using Object = UnityEngine.Object;

namespace Valkyrie
{
    public static class EditorUtils
    {
        [MenuItem("Valkyrie/Tools/Delete local save")]
        public static void DeleteSave()
        {
            PlayerPrefs.DeleteAll();
        }

        internal static string GetCoolName(string original)
        {
            var temp = original.Replace("ConfigData", "");
            var sb = new StringBuilder();
            foreach (var ch in temp)
            {
                if (char.IsUpper(ch))
                    sb.Append(' ');
                sb.Append(ch);
            }

            return sb.ToString();
        }

        #region Unity working

        static void CreateScript(string path, Action<StreamWriter> writeCall)
        {
            if (File.Exists(path))
                return;

            using (var stream = File.CreateText(path))
            {
                stream.AutoFlush = true;
                writeCall(stream);
            }

            Debug.LogFormat("Script '{0}' created", path);
        }

        [MenuItem("Valkyrie/Init/Template")]
        static void InitValkyrieFolderStructure()
        {
            var foldersList = new[]
            {
                "Content",
                "Content/Locations",
                "Content/Prefabs",
                "Resources",
                "Resources/Ui",
                "Scripts",
                "Scenes"
            };
            foreach (var path in foldersList)
                CreateFolder(path);

            Debug.LogFormat("Valkyrie folders structure initialized");

            CreateScript("Assets/Scripts/Constants.cs", (stream) =>
            {
                stream.WriteLine("public static class Constants");
                stream.WriteLine("{");
                stream.WriteLine("\t#region Constants");
                stream.WriteLine();
                stream.WriteLine("\t\tpublic const string ApplicationId = \"ValkyrieProjectTemplate\";");
                stream.WriteLine("\t\tpublic const string UiRootPrefab = \"Ui/UiRoot\";");
                stream.WriteLine();
                stream.WriteLine("\t#endregion");
                stream.WriteLine("}");
            });

            CreateScript("Assets/Scripts/Startup.cs", (stream) =>
            {
                stream.WriteLine("using Valkyrie;");
                stream.WriteLine("using Valkyrie.Di;");
                stream.WriteLine();
                stream.WriteLine("public class Startup");
                stream.WriteLine("{");
                stream.WriteLine("\t#region Singleton");
                stream.WriteLine();
                stream.WriteLine("\tprivate static Startup _instance;");
                stream.WriteLine();
                stream.WriteLine(
                    "\tpublic static Startup Instance => _instance ?? (_instance = BuildContainer().Resolve<Startup>());");
                stream.WriteLine();
                stream.WriteLine("\t#endregion");
                stream.WriteLine();
                stream.WriteLine("\t#region properties");
                stream.WriteLine();
                stream.WriteLine("\tpublic IContainer RootContainer { get; }");
                stream.WriteLine();
                stream.WriteLine("\t#endregion");
                stream.WriteLine();
                stream.WriteLine("\t#region Ctor");
                stream.WriteLine();
                stream.WriteLine("\tpublic Startup(IContainer container)");
                stream.WriteLine("\t{");
                stream.WriteLine("\t\tRootContainer = container;");
                stream.WriteLine("\t}");
                stream.WriteLine();
                stream.WriteLine("\t#endregion");
                stream.WriteLine();
                stream.WriteLine("\tstatic IContainer BuildContainer()");
                stream.WriteLine("\t{");
                stream.WriteLine("\t\treturn Factory.Start(Constants.ApplicationId)");
                stream.WriteLine("\t\t\t.UseUnity(30)");
                stream.WriteLine("\t\t\t.UseMvvm(Constants.UiRootPrefab)");
                stream.WriteLine("\t\t\t.UseLibrary(new ProjectTemplateLibrary())");
                stream.WriteLine("\t\t\t.Build();");
                stream.WriteLine("\t}");
                stream.WriteLine("}");
            });

            CreateScript("Assets/Scripts/ProjectTemplateLibrary.cs", stream =>
            {
                stream.WriteLine("using Valkyrie.Di;");
                stream.WriteLine();
                stream.WriteLine("class ProjectTemplateLibrary : ILibrary");
                stream.WriteLine("{");
                stream.WriteLine("\tpublic void Register(IContainer container)");
                stream.WriteLine("\t{");
                stream.WriteLine("\t\tcontainer.Register<Startup>().AsSelf().SingleInstance();");
                stream.WriteLine("\t}");
                stream.WriteLine("}");
            });

            CreateScript("Assets/Scripts/DiComponent.cs", stream =>
            {
                stream.WriteLine("using System;");
                stream.WriteLine("using Valkyrie.UnityExtensions.Components;");
                stream.WriteLine();
                stream.WriteLine("public class DiComponent : DiUnityComponent");
                stream.WriteLine("{");
                stream.WriteLine("\tprotected override void SelfInit()");
                stream.WriteLine("\t{");
                stream.WriteLine("\t\tif(Startup.Instance == null)");
                stream.WriteLine("\t\t\tthrow new Exception(\"Startup is not created\");");
                stream.WriteLine("\t\tbase.SelfInit();");
                stream.WriteLine("\t}");
                stream.WriteLine("}");
            });

            CreateScript("Assets/Resources/Ui/Styles.xml",
                stream => { stream.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?><root/>"); });
            CreateScript("Assets/Resources/Ui/Views.xml",
                stream => { stream.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?><root/>"); });

            AssetDatabase.Refresh();
        }

        public static bool KeysPressed(string controlName, params KeyCode[] keys)
        {
            return GUI.GetNameOfFocusedControl() == controlName
                   && Event.current.type == EventType.KeyUp
                   && keys.Any(u => u == Event.current.keyCode);
        }

        public static void DrawWithColor(Color color, Action drawAction)
        {
            var temp = GUI.color;
            GUI.color = color;
            drawAction();
            GUI.color = temp;
        }

        public static void CreateFolder(string path)
        {
            if (!path.StartsWith("Assets"))
                path = $"Assets/{path}";
            //path = Path.GetDirectoryName(path);
            if (Directory.Exists(path))
                return;
            if (AssetDatabase.IsValidFolder(path))
                return;
            var parts = path.Split(new[] {'/', '\\'});
            var parentFolder = parts[0];

            for (int i = 1; i < parts.Length - 1; ++i)
            {
                parentFolder = $"{parentFolder}/{parts[i]}";
            }

            CreateFolder(parentFolder);
            AssetDatabase.CreateFolder(parentFolder, parts[parts.Length - 1]);
        }

        static string PrepareFolder(string path)
        {
            var subPath = Path.GetDirectoryName(path);
            if (!string.IsNullOrEmpty(subPath) && !Directory.Exists(subPath))
                Directory.CreateDirectory(subPath);
            return path;
        }

        public static void SaveToPrefab(this GameObject o, string path)
        {
            path = path.Replace("\\", "/");
            Object prefab = AssetDatabase.LoadAssetAtPath<Object>(path);
            if (prefab == null)
            {
                var folderPath = Path.GetDirectoryName(path);
                CreateFolder(folderPath);
                prefab = PrefabUtility.CreateEmptyPrefab(path);
            }

            PrefabUtility.ReplacePrefab(o, prefab, ReplacePrefabOptions.ConnectToPrefab);
        }

        public static bool DrawToggleRegion(ref bool toggle, Color guiColor, string text, Action drawAction)
        {
            var tempColor = GUI.color;
            GUI.color = guiColor;
            toggle = EditorGUILayout.BeginToggleGroup(text, toggle);
            if (toggle)
            {
                EditorGUI.indentLevel++;
                drawAction();
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.EndToggleGroup();
            GUI.color = tempColor;
            return toggle;
        }

        public static Vector2 DrawScrollRegion(ref Vector2 position, Action drawAction)
        {
            position = EditorGUILayout.BeginScrollView(position);
            drawAction();
            EditorGUILayout.EndScrollView();
            return position;
        }

        public static void DrawObjectInspector(object data, IConfiguration config, params string[] excludeFields)
        {
            var dataType = data.GetType();
            if (typeof(Object).IsAssignableFrom(dataType))
            {
                var editor = Editor.CreateEditor((Object) data, dataType);
                editor.DrawDefaultInspector();
            }
            else
            {
                var fields = dataType.GetFields(BindingFlags.Public | BindingFlags.Instance);
                var properties = dataType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (var info in fields.Where(u => !excludeFields.Contains(u.Name) && !u.IsInitOnly))
                {
                    if (info.IsInitOnly)
                        DrawWithColor(Color.black,
                            () => EditorGUILayout.LabelField(info.Name, info.GetValue(data).ToString()));
                    else
                        Draw(() => info.GetValue(data), o => info.SetValue(data, o), info.FieldType, info.Name, config);
                }

                foreach (var info in properties.Where(u => !excludeFields.Contains(u.Name) && u.CanRead))
                {
                    if (info.CanWrite)
                        Draw(() => info.GetValue(data, null), o => info.SetValue(data, o, null), info.PropertyType,
                            info.Name, config);
                    else
                        DrawWithColor(Color.black,
                            () => EditorGUILayout.LabelField(info.Name, info.GetValue(data, null).ToString()));
                }
            }
        }

        public static void DrawObjectInspector(Rect rect, object data, IConfiguration config,
            params string[] excludeFields)
        {
            var dataType = data.GetType();
            if (typeof(Object).IsAssignableFrom(dataType))
            {
                EditorGUI.ObjectField(rect, dataType.Name, (Object) data, dataType, true);
            }
            else
            {
                var fields = dataType.GetFields(BindingFlags.Public | BindingFlags.Instance);
                var properties = dataType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                foreach (var info in fields.Where(u => !excludeFields.Contains(u.Name)))
                {
                    Draw(rect, () => info.GetValue(data), o => info.SetValue(data, o), info.FieldType, info.Name,
                        config);
                    rect.x += 20;
                }

                foreach (var info in properties.Where(u => !excludeFields.Contains(u.Name)))
                {
                    Draw(rect, () => info.GetValue(data, null), o => info.SetValue(data, o, null), info.PropertyType,
                        info.Name, config);
                    rect.x += 20;
                }
            }
        }

        static void Draw(Func<object> getFunc, Action<object> setAction, Type dataType, string label,
            IConfiguration config)
        {
            var data = getFunc();

            if (typeof(Object).IsAssignableFrom(dataType))
            {
                var editor = Editor.CreateEditor((Object) data, dataType);
                editor.DrawDefaultInspector();
            }
            else
            {
                if (dataType == typeof(string))
                {
                    setAction(EditorGUILayout.TextField(label, (string) data));
                }
                else if (dataType == typeof(int))
                {
                    setAction(EditorGUILayout.IntField(label, (int) data));
                }
                else if (dataType == typeof(float))
                {
                    setAction(EditorGUILayout.FloatField(label, (float) data));
                }
                else if (dataType == typeof(bool))
                {
                    setAction(EditorGUILayout.Toggle(label, (bool) data));
                }
                else if (dataType.IsEnum)
                {
                    setAction(EditorGUILayout.EnumPopup(label, (Enum) data));
                }
                else if (config != null && IsType<BaseConfigData>(dataType, false))
                {
                    var allCfgDatas = config.Get<BaseConfigData>().Where(u => dataType.IsAssignableFrom(u.GetType()))
                        .ToList();
                    var names = allCfgDatas.Select(u => $"{u.Id}({u.GetType().Name})").ToArray();
                    var index = EditorGUILayout.Popup(label, allCfgDatas.IndexOf((BaseConfigData) data), names);
                    if (index >= 0 && index <= allCfgDatas.Count)
                    {
                        setAction(allCfgDatas[index]);
                    }
                }
                else if (typeof(IList).IsAssignableFrom(dataType))
                {
                    var innerList = (IList) data ?? (IList) Activator.CreateInstance(dataType);
                    DrawList(GetList(innerList, dataType, config, label), dataType, innerList);
                    setAction(innerList);
                }
                //else if (typeof(ICollection).IsAssignableFrom(dataType) && dataType.GetMethod("Add") != null && dataType.GetMethod("Clear") != null)
                //{
                //    var collection = (ICollection)data;
                //    var innerList = new ArrayList(collection);
                //    DrawList(new ReorderableList(innerList, GetElementType(dataType)), dataType, innerList);
                //    var add = dataType.GetMethod("Add");
                //    var clear = dataType.GetMethod("Clear");
                //    clear.Invoke(data, null);
                //    foreach (var o in innerList)
                //        add.Invoke(data, new[] { o });
                //    setAction(innerList);
                //}
                else
                {
                    DrawWithColor(Color.red,
                        () =>
                            EditorGUILayout.LabelField(label,
                                $"Not supported Type: {dataType.Name}"));
                }
            }
        }

        static void Draw(Rect rect, Func<object> getFunc, Action<object> setAction, Type dataType, string label,
            IConfiguration config)
        {
            var data = getFunc();

            if (typeof(Object).IsAssignableFrom(dataType))
            {
                EditorGUI.ObjectField(rect, label, (Object) data, dataType, true);
            }
            else
            {
                if (dataType == typeof(string))
                {
                    setAction(EditorGUI.TextField(rect, label, (string) data));
                }
                else if (dataType == typeof(int))
                {
                    setAction(EditorGUI.IntField(rect, label, (int) data));
                }
                else if (dataType == typeof(float))
                {
                    setAction(EditorGUI.FloatField(rect, label, (float) data));
                }
                else if (dataType == typeof(bool))
                {
                    setAction(EditorGUI.Toggle(rect, label, (bool) data));
                }
                else if (dataType.IsEnum)
                {
                    setAction(EditorGUI.EnumPopup(rect, label, (Enum) data));
                }
                else if (config != null && IsType<BaseConfigData>(dataType, false))
                {
                    var allCfgDatas = config.Get<BaseConfigData>().Where(u => dataType.IsAssignableFrom(u.GetType()))
                        .ToList();
                    var names = allCfgDatas.Select(u => $"{u.Id}({u.GetType().Name})").ToArray();
                    var index = EditorGUI.Popup(rect, label, allCfgDatas.IndexOf((BaseConfigData) data), names);
                    if (index >= 0 && index <= allCfgDatas.Count)
                    {
                        setAction(allCfgDatas[index]);
                    }
                }
                else if (typeof(IList).IsAssignableFrom(dataType))
                {
                    var innerList = (IList) data ?? (IList) Activator.CreateInstance(dataType);
                    DrawList(GetList(innerList, dataType, config, label), dataType, innerList);
                    setAction(innerList);
                }
                else
                {
                    DrawWithColor(Color.red,
                        () =>
                            EditorGUI.LabelField(rect, label,
                                $"Not supported Type: {dataType.Name}"));
                }
            }
        }

        static readonly Dictionary<IList, ReorderableList> _reorderableLists = new Dictionary<IList, ReorderableList>();

        static ReorderableList GetList(IList collection, Type dataType, IConfiguration config, string label)
        {
            ReorderableList result;
            if (!_reorderableLists.TryGetValue(collection, out result))
            {
                _reorderableLists.Add(collection, result = new ReorderableList(collection, GetElementType(dataType)));
                result.drawHeaderCallback = rect => EditorGUI.LabelField(rect, label);
                result.drawElementCallback = (rect, index, active, focused) =>
                {
                    var element = result.list[index];
                    var elementType = element.GetType();
                    if (elementType == typeof(string) || elementType == typeof(int) || elementType == typeof(float) ||
                        elementType == typeof(bool) || elementType.IsEnum ||
                        typeof(Object).IsAssignableFrom(elementType) ||
                        IsType<BaseConfigData>(elementType, false))
                        Draw(rect, () => element, (value) => result.list[index] = value, elementType, "", config);
                    else
                        DrawObjectInspector(rect, element, config);
                };
            }

            return result;
        }

        public static void DrawList(IList collection, Type dataType, string label, bool editable,
            ReorderableList.ElementCallbackDelegate drawElementCallback,
            ReorderableList.ElementHeightCallbackDelegate heightDelegate)
        {
            ReorderableList result;
            if (!_reorderableLists.TryGetValue(collection, out result))
            {
                bool drawHeader = !string.IsNullOrEmpty(label);
                _reorderableLists.Add(collection,
                    result =
                        new ReorderableList(collection, GetElementType(dataType), editable, drawHeader, editable,
                            editable));
                if (drawHeader)
                    result.drawHeaderCallback = rect => EditorGUI.LabelField(rect, label);
                if (heightDelegate != null)
                    result.elementHeightCallback = heightDelegate;
                result.drawElementCallback = drawElementCallback;
            }

            DrawList(result, dataType, collection);
        }

        static Type GetElementType(Type dataType)
        {
            if (dataType.IsGenericType)
                return dataType.GetGenericArguments()[0];
            return null;
        }

        static void DrawList(ReorderableList list, Type dataType, IList innerList)
        {
            try
            {
                list.DoLayoutList();
            }
            catch (MissingMethodException)
            {
                if (dataType.IsGenericType && dataType.GetGenericArguments()[0] == typeof(string))
                    innerList.Add("");
            }
        }

        public static string DrawPopup(string label, string currentValue, IList<string> values)
        {
            if (values.Count == 0)
                values.Add(string.Empty);
            var index = EditorGUILayout.Popup(new GUIContent(label), values.IndexOf(currentValue), values.ToArray());
            if (index < 0 || index >= values.Count)
                return string.Empty;
            return values[index];
        }

        #endregion

        #region Valkyrie

        #region configs

        private static IContainer _editorScope;

        public static ILocalizationEditor LoadLocales()
        {
            if (Application.isPlaying)
            {
                return Core.GetInstance().RootScope.Resolve<ILocalizationEditor>();
            }

            if (_editorScope == null)
            {
                var container = Configurator.Start("EDITOR")
                    .RegisterLibrary(Libraries.Data).Build();
                _editorScope = container.CreateChild();
            }

            _editorScope.LoadAllLocales(GoogleLocaleSettings.GetDefaultPath).Done();
            return _editorScope.Resolve<ILocalizationEditor>();
        }

        public static IEditableConfiguration LoadConfiguration()
        {
            if (Application.isPlaying)
            {
                return Core.GetInstance().RootScope.Resolve<IConfiguration>().StartEdit();
            }

            if (_editorScope == null)
            {
                var container = Configurator.Start("EDITOR")
                    .RegisterLibrary(Libraries.Data).Build();
                _editorScope = container.CreateChild();

                var logService = container.Resolve<ILogService>();
                logService.Observe().Subscribe(lm =>
                {
                    switch (lm.Level)
                    {
                        case MessageLevel.Error:
                            Debug.LogError($"{lm.Channel}:{lm.Message}");
                            break;
                        case MessageLevel.Warning:
                            Debug.LogWarning($"{lm.Channel}:{lm.Message}");
                            break;
                        case MessageLevel.Info:
                        case MessageLevel.Debug:
                            Debug.Log($"{lm.Channel}:{lm.Message}");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
            }

            var result = _editorScope.Resolve<IConfiguration>();
            var edit = result.StartEdit();
            edit.Clear();
            var streams = FindAllConfigFiles().Select(cfgFilePath => (Stream) File.OpenRead(cfgFilePath)).ToArray();
            //result.Load(streams, ConfigType.Xml);
            foreach (var stream in streams)
            {
                try
                {
                    edit.Load(stream, ConfigType.Xml);
                    edit.Fetch();
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogException(e);
                }

                stream.Dispose();
            }

            return result.StartEdit();
        }

        static IEnumerable<string> FindAllConfigFiles()
        {
            return AssetDatabase.GetAllAssetPaths().Where(path => path.EndsWith(".xml"));
        }

        #endregion

        #endregion

        public static bool IsType<T>(Type type, bool notAbstract)
        {
            return (notAbstract)
                ? !type.IsAbstract && (typeof(T).IsAssignableFrom(type) || type.IsSubclassOf(typeof(T)))
                : typeof(T).IsAssignableFrom(type) || type.IsSubclassOf(typeof(T));
        }

        #region Nodes

        public static void DrawComplexGrid(Rect rect, Vector2 offset, float gridSpacing, float gridOpacity,
            Color gridColor, int bold = 5)
        {
            var tempRect = rect;
            DrawGrid(tempRect, offset, gridSpacing, gridOpacity * .25f, gridColor);
            DrawGrid(tempRect, offset, gridSpacing * bold, gridOpacity * .5f, gridColor);
            DrawGrid(tempRect, offset, gridSpacing * bold * 2, gridOpacity, gridColor);
        }

        static void DrawGrid(Rect rect, Vector2 offset, float gridSpacing, float gridOpacity, Color gridColor)
        {
            int widthDivs = Mathf.CeilToInt(rect.width / gridSpacing) + 1;
            int heightDivs = Mathf.CeilToInt(rect.height / gridSpacing) + 1;

            Handles.BeginGUI();
            Handles.color = new Color(gridColor.r, gridColor.g, gridColor.b, gridOpacity);

            Vector3 newOffset = new Vector3((rect.position.x + offset.x) % gridSpacing,
                (rect.position.y + offset.y) % gridSpacing, 0);

            for (int i = 0; i <= widthDivs; i++)
            {
                Handles.DrawLine(new Vector3(gridSpacing * i, -gridSpacing, 0) + newOffset,
                    new Vector3(gridSpacing * i, rect.height + gridSpacing, 0f) + newOffset);
            }

            for (int j = 0; j <= heightDivs; j++)
            {
                Handles.DrawLine(new Vector3(-gridSpacing, gridSpacing * j, 0) + newOffset,
                    new Vector3(rect.width + gridSpacing, gridSpacing * j, 0f) + newOffset);
            }

            Handles.color = Color.white;
            Handles.EndGUI();
        }

        #endregion
    }
}