using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Valkyrie.Math;
using Valkyrie.Procedural.Triangulation;
using Valkyrie.Ui.Window;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace Valkyrie.Procedural
{
    public class ProceduralMeshEditorWindow : ValkyrieEditorWindow
    {
        [MenuItem("Valkyrie/Tools/Procedural Mesh Editor")]
        static void OpenWindow()
        {
            var window = GetWindow<ProceduralMeshEditorWindow>();
            window.MenuHeight = 0;
            window.titleContent = new GUIContent("Procedural Mesh Editor, Valkyrie(c)");
            
            window.Show();
        }

        private PreviewRenderUtility _renderUtility;
        private Material _coolMaterial;
        private Material _errorMaterial;
        
        private Material _meshMaterial;
        private string _text = "main = { Sphere(1) }";
        private List<Mesh> _meshes = new List<Mesh>();
        private GUIStyle _previewBackgroundStyle;

        #region Init/Destroy
        
        protected override void OnEnable()
        {
            base.OnEnable();

            _previewBackgroundStyle =
                new GUIStyle(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).FindStyle("GameViewBackground"));
            
            _renderUtility = new PreviewRenderUtility();
            
            var tempMat = AssetDatabase.GetBuiltinExtraResource<Material>("Default-Diffuse.mat");
            
            _coolMaterial = new Material(tempMat);
            _errorMaterial = new Material(tempMat) {color = Color.red};
            
            UpdateMesh();
        }

        private void OnDisable()
        {
            _renderUtility.Cleanup();
            
            DestroyImmediate(_coolMaterial);
            DestroyImmediate(_errorMaterial);
            _coolMaterial = null;
            _errorMaterial = null;
        }
        
        #endregion

        private void UpdateMesh()
        {
            try
            {
                EditorUtility.DisplayProgressBar("Valkyrie Mesh Editor", "Generating mesh", 0f);
                var source = Fields.Factory.Parse(_text).Construct("main");
                if (source != null)
                {
                    _meshMaterial = _coolMaterial;

                    foreach (var mesh in _meshes)
                    {
                        DestroyImmediate(mesh);
                    }
                    _meshes.Clear();
                    var tr = source
                        .TriangulateCubes(BoundingBox3.CreateFromPoints(System.Numerics.Vector3.One, System.Numerics.Vector3.One * -1), 0f);
                    tr.Width = tr.Height = tr.Length = 32;
                    _meshes.AddRange(tr.Triangulate());
                }
                else
                    _meshMaterial = _errorMaterial;
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogException(e);
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

        #region Drawing

        protected override void BuildLayout(EditorWindowFactory factory)
        {
            factory.BeginHorizontal();
            {
                factory.Panel(DrawCode, false);
                factory.Panel(DrawView, false);
            }
            factory.EndHorizontal();
        }
        
        private void DrawView(Rect rect)
        {
            _renderUtility.camera.transform.SetPositionAndRotation((Vector3) (-Vector3.forward * 8f),
                Quaternion.identity);
            _renderUtility.camera.farClipPlane = 30;
 
            _renderUtility.lights[0].intensity = 0.5f;
            _renderUtility.lights[0].transform.rotation = Quaternion.Euler(30f, 30f, 0f);
            _renderUtility.lights[1].intensity = 0.5f;
 
            _renderUtility.BeginPreview(rect, _previewBackgroundStyle);
            
            if(_meshMaterial != null)
                foreach (var mesh in _meshes)
                    DrawMesh(rect, mesh, _meshMaterial);
            
            bool fog = RenderSettings.fog;
            Unsupported.SetRenderSettingsUseFogNoDirty(false);
            _renderUtility.camera.Render();
            Unsupported.SetRenderSettingsUseFogNoDirty(fog);
            Texture texture = _renderUtility.EndPreview();
 
            GUI.DrawTexture(rect, texture);
        }
        
        void DrawMesh(Rect rect, Mesh mesh, Material material)
        {
            _renderUtility.DrawMesh(mesh, Vector3.zero, Quaternion.identity, material,0);
            return;
        }

        private void DrawCode()
        {
            var nt = GUILayout.TextArea(_text);
            if (nt != _text)
            {
                _text = nt;
                UpdateMesh();
            }
        }
        
        #endregion
    }
}