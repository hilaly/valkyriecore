using System;
using UnityEngine;

namespace Valkyrie.Ui.Window
{
    class Panel : IPanel
    {
        private readonly Action<Rect> _draw;
        private Vector2 _gridOffset;

        public bool DrawGrid { get; set; }
        public bool UseLayout { get; set; }

        public Panel(Action<Rect> draw)
        {
            _draw = draw;
        }

        public void Draw(Rect rect)
        {
            if(DrawGrid)
                EditorUtils.DrawComplexGrid(new Rect(rect.position, rect.size), _gridOffset, 20, 0.4f, Color.gray, 5);
            
            if (UseLayout)
            {
                GUILayout.BeginArea(rect);
                _draw(new Rect(Vector2.zero, rect.size));
                GUILayout.EndArea();
            }
            else
                _draw(rect);
        }

        public void ProcessEvents(Event e, Rect position)
        {
            switch (e.type)
            {
                case EventType.MouseDrag:
                if (e.button == 0)
                {
                    OnDrag(e.delta);
                }
                break;
            }
        }
        
        private void OnDrag(Vector2 delta)
        {
            _gridOffset += delta;
            GUI.changed = true;
        }

    }
}