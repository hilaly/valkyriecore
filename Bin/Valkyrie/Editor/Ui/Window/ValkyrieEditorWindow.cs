using System;
using UnityEditor;
using UnityEngine;

namespace Valkyrie.Ui.Window
{
    public abstract class ValkyrieEditorWindow : EditorWindow
    {
        private const float PanelBorders = 5f;

        private IPanel _resizablePanel;

        protected virtual void OnEnable()
        {
            var layoutFactory = EditorWindowFactory.Create();
            BuildLayout(layoutFactory);
            _resizablePanel = layoutFactory.Build();
        }
        
        public float MenuHeight { get; set; }

        public void OnGUI()
        {
            var windowContentRect = new Rect(PanelBorders, MenuHeight + PanelBorders, position.width - PanelBorders * 2,
                position.height - MenuHeight - PanelBorders * 2);

            if (MenuHeight > 0f)
                DrawMenu();
            Debug.Assert(_resizablePanel != null);
            _resizablePanel.Draw(windowContentRect);
            
            ProcessEvents(Event.current, windowContentRect);
            
            if(GUI.changed)
                Repaint();
        }
        
        private void DrawMenu()
        {
            var menuBar = new Rect(0, 0, position.width, MenuHeight);

            GUILayout.BeginArea(menuBar, EditorStyles.toolbar);
            GUILayout.BeginHorizontal();

            DrawMenuImpl();

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        void ProcessEvents(Event e, Rect rect)
        {
            _resizablePanel.ProcessEvents(e, rect);
            ProcessEventsImpl(e);
        }

        #region Abstract

        protected abstract void BuildLayout(EditorWindowFactory factory);
        
        protected virtual void ProcessEventsImpl(Event e)
        {}

        protected virtual void DrawMenuImpl()
        {
            //GridEnabled = GUILayout.Toggle(GridEnabled, new GUIContent("Show grid"), EditorStyles.toolbarButton, GUILayout.Width(70));
        }
        
        #endregion
    }
}