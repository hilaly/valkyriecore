using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Valkyrie.Ui.Window
{
    public class EditorWindowFactory
    {
        private List<IPanel> _panels = new List<IPanel>();
        private IPanel _finished;

        private EditorWindowFactory()
        {}
        
        internal static EditorWindowFactory Create()
        {
            return new EditorWindowFactory();
        }

        internal IPanel Build()
        {
            if(_finished == null)
                throw new Exception("Invalid open/close calls count");
            return _finished;
        }

        public EditorWindowFactory BeginVertical()
        {
            var panel = new ResizablePanel() {IsVertical = true};
            if (_panels.Count > 0)
                ((ResizablePanel) _panels.Last()).AddPanel(panel);
            _panels.Add(panel);
            return this;
        }

        public EditorWindowFactory BeginHorizontal()
        {
            var panel = new ResizablePanel() {IsVertical = false};
            if (_panels.Count > 0)
                ((ResizablePanel) _panels.Last()).AddPanel(panel);
            _panels.Add(panel);
            return this;
        }

        public EditorWindowFactory EndVertical()
        {
            return Close(true);
        }

        public EditorWindowFactory EndHorizontal()
        {
            return Close(false);
        }

        EditorWindowFactory Close(bool isVertical)
        {
            var panel = _panels[_panels.Count - 1];
            if((panel as ResizablePanel).IsVertical != isVertical)
                throw new Exception("Invalid close call order");
            if (_panels.Count == 1)
                _finished = panel;
            _panels.RemoveAt(_panels.Count - 1);
            return this;
        }

        public EditorWindowFactory Panel(Action<Rect> draw, bool drawGrid)
        {
            var panel = new Panel(draw) {UseLayout = false, DrawGrid = drawGrid};
            if (_panels.Count > 0)
                ((ResizablePanel) _panels.Last()).AddPanel(panel);
            else
                _finished = panel;
            return this;
        }

        public EditorWindowFactory Panel(Action drawLayout, bool drawGrid)
        {
            var panel = new Panel((rect) => drawLayout()) {UseLayout = true, DrawGrid = drawGrid};
            if (_panels.Count > 0)
                ((ResizablePanel) _panels.Last()).AddPanel(panel);
            else
                _finished = panel;
            return this;
        }
    }
}