using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Valkyrie.Ui.Window
{
    class ResizablePanel : IPanel
    {
        private const float ResizerSize = 5f;
 
        private List<IPanel> _panels;
        private List<float> _sizeRatios = new List<float>();
        private List<Rect> _resizers = new List<Rect>();
 
        private int _isResizing = -1;

        private readonly GUIStyle _resizerStyle;
        
        public bool IsVertical { get; set; }

        public void AddPanel(IPanel panel)
        {
            _panels.Add(panel);
            if (_panels.Count == 1)
                return;
            
            var value = _sizeRatios.Count > 0 ? _sizeRatios[_sizeRatios.Count - 1] : 0;
            _sizeRatios.Add(0.5f + value * 0.5f);
            _resizers.Add(new Rect());
        }

        public ResizablePanel()
        {
            _panels = new List<IPanel>();
            _resizerStyle = new GUIStyle
            {
                normal =
                {
                    background = EditorGUIUtility.Load("icons/d_AvatarBlendBackground.png") as Texture2D
                }
            };
        }

        public void Draw(Rect position)
        {
            for(var i = 0; i < _panels.Count; ++i)
                DrawPanel(position, i);
            for(var i = 0; i < _resizers.Count; ++i)
                DrawResizer(position, i);
        }
        
        Rect GetPanelRect(Rect position, int index)
        {
            var minRatio = 0f;
            var maxRatio = 1f;
            
            var sizeRatioIndexMin = index - 1;
            var sizeRatioIndexMax = index;
            if (sizeRatioIndexMin >= 0)
                minRatio = _sizeRatios[sizeRatioIndexMin];
            if (sizeRatioIndexMax < _sizeRatios.Count)
                maxRatio = _sizeRatios[sizeRatioIndexMax];

            var p = position.position;
            var s = position.size;
            if (IsVertical)
            {
                p.y = p.y + (position.height * minRatio) + ResizerSize;
                s.y = position.height * (maxRatio - minRatio) - ResizerSize;
            }
            else
            {
                p.x = p.x + (position.width * minRatio) + ResizerSize;
                s.x = position.width * (maxRatio - minRatio) - ResizerSize;
            }
            var panelRect = new Rect(p, s);
            return panelRect;
        }

        void DrawPanel(Rect position, int index)
        {
            var panelRect = GetPanelRect(position, index);
            _panels[index].Draw(panelRect);
        }
        void DrawResizer(Rect position, int index)
        {
            var sizeRatio = _sizeRatios[index];
            var resizerRect = _resizers[index] = IsVertical
                ? new Rect(position.x, position.y + (position.height * sizeRatio) - ResizerSize,
                    position.width, ResizerSize * 2f)
                : new Rect(position.x + (position.width * sizeRatio) - ResizerSize, position.y,
                    ResizerSize * 2, position.height);

            var areaRect = IsVertical
                ? new Rect(resizerRect.position + (Vector2.up * ResizerSize), new Vector2(position.width, 2))
                : new Rect(resizerRect.position + (Vector2.right * ResizerSize), new Vector2(2, position.height));
            GUILayout.BeginArea(areaRect, _resizerStyle);
            GUILayout.EndArea();

            EditorGUIUtility.AddCursorRect(resizerRect,
                IsVertical ? MouseCursor.ResizeVertical : MouseCursor.ResizeHorizontal);
        }
        
        public void ProcessEvents(Event e, Rect position)
        {
            for (var index = 0; index < _panels.Count; index++)
            {
                var rect = GetPanelRect(position, index);
                _panels[index].ProcessEvents(e, rect);
            }

            switch (e.type)
            {
                case EventType.MouseDown:
                    if (e.button == 0)
                    {
                        var index = _resizers.FindIndex(u => u.Contains(e.mousePosition));
                        if (index >= 0)
                            _isResizing = index;
                    }
                    break;
 
                case EventType.MouseUp:
                    _isResizing = -1;
                    break;
            }
 
            Resize(e, position);
        }
 
        private void Resize(Event e, Rect position)
        {
            if(_isResizing < 0)
                return;

            var threshold = ResizerSize * 3 / (IsVertical ? position.height  : position.width);
            var min = 0f;
            var max = 1f;
            if (_isResizing > 0)
                min = _sizeRatios[_isResizing - 1];
            if (_isResizing < _sizeRatios.Count - 1)
                max = _sizeRatios[_isResizing + 1];
            min += threshold;
            max -= threshold;
            
            if (IsVertical)
                _sizeRatios[_isResizing] = Mathf.Clamp((e.mousePosition.y - position.y) / position.height, min, max);
            else
                _sizeRatios[_isResizing] = Mathf.Clamp((e.mousePosition.x - position.x) / position.width, min, max);

            GUI.changed = true;
        }
    }
}