using UnityEngine;

namespace Valkyrie.Ui.Window
{
    interface IPanel
    {
        void Draw(Rect rect);
        void ProcessEvents(Event e, Rect position);
    }
}