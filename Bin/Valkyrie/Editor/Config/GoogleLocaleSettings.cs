using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using Valkyrie.Config;
using Valkyrie.Data;
using Valkyrie.Data.Localization;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Scripts.Tools
{
    [CreateAssetMenu(fileName = "GoogleLocalizationSettings", menuName = "Valkyrie/Google Localization Settings",
        order = 1)]
    public class GoogleLocaleSettings : ScriptableObject
    {
        public string GoogleReference;
        public string TargetFolder = "Assets/Resources/Localization";
        public List<string> Sheets = new List<string>();

        internal static string GetDefaultPath
        {
            get
            {
                var assets = AssetDatabase.FindAssets("t:GoogleLocaleSettings");
                foreach (var assetPath in assets.Select(AssetDatabase.GUIDToAssetPath))
                {
                    var path = AssetDatabase.LoadAssetAtPath<GoogleLocaleSettings>(assetPath).TargetFolder;
                    var resIndex = path.LastIndexOf("Resources");
                    if (resIndex >= 0)
                        return path.Substring(resIndex + "Resources".Length + 1);
                    return path;
                }

                return "Localization";
            }
        }

        [MenuItem("Valkyrie/Localization/Download Google docs")]
        static void DownloadAll()
        {
            var log = new ConfigEditorWindow.UnityLog();
            var assets = AssetDatabase.FindAssets("t:GoogleLocaleSettings");
            foreach (var assetPath in assets.Select(AssetDatabase.GUIDToAssetPath))
            {
                Debug.Log($"Start downloading {assetPath}");
                AssetDatabase.LoadAssetAtPath<GoogleLocaleSettings>(assetPath).Download(log);
                Debug.Log($"End downloading {assetPath}");
            }
            
            AssetDatabase.Refresh();
        }

        private void Download(ILogInstance log)
        {
            Dictionary<string, LocalizationSource> localizationSources = new Dictionary<string, LocalizationSource>();

            void ParseRow(List<KeyValuePair<string, object>> list)
            {
                var key = list.Find(u => u.Key == "Key").Value.ToString();
                foreach (var pair in list)
                {
                    var lang = pair.Key;
                    if (lang == "Key" || lang.StartsWith("*"))
                        continue;
                    var value = pair.Value.ToString();

                    if (!localizationSources.TryGetValue(lang, out var source))
                    {
                        source = new LocalizationSource {Language = lang, Keys = new Dictionary<string, string>()};
                        localizationSources.Add(lang, source);
                    }

                    source.Keys[key] = value;
                }
            }

            Debug.Log($"Start importing locales from {GoogleReference} to {TargetFolder}");
            GoogleSheets.ReadGoogleSheet(GoogleReference, Sheets, ParseRow, log, "Valkyrie application",
                "Assets/Valkyrie/ThirdParty/credentials.json");

            Directory.CreateDirectory(TargetFolder);
            foreach (var localizationSource in localizationSources.Values)
            {
                var path = Path.Combine(TargetFolder, $"{localizationSource.Language}.json");
                File.WriteAllText(path, localizationSource.ToJson());
                log.Info($"Save localization for {localizationSource.Language}");
            }

            Debug.Log($"Locales from {GoogleReference} imported to {TargetFolder}");
        }
    }
}