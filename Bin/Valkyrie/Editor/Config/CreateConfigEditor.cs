using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Data.Config;

namespace Valkyrie.Config
{
    class CreateConfigEditor
    {
        private IEditableConfiguration _config;

        private readonly List<Type> _availableTypes;
        private readonly List<string> _names;
        private readonly Dictionary<Type, BaseConfigData> _instances = new Dictionary<Type, BaseConfigData>();
        private int _index = -1;

        public IEditableConfiguration Config { set { _config = value; } }

        public CreateConfigEditor(IEditableConfiguration config)
        {
            _config = config;

            _availableTypes =
                AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(assembly => assembly.GetTypes())
                    .Where(type => EditorUtils.IsType<BaseConfigData>(type, true))
                    .ToList();
            _names = _availableTypes.Select(u => EditorUtils.GetCoolName(u.Name)).ToList();
        }

        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        public bool OnGUI()
        {
            _index = EditorGUILayout.Popup("Type", _index, _names.ToArray());

            EditorGUILayout.Separator();

            if (_index >= 0)
            {
                var data = GetData(_index);
                var log = new ConfigEditorWindow.UnityLog();
                EditorUtils.DrawWithColor(data.Validate(log) ? GUI.color : Color.red, () =>
                {
                    EditorUtils.DrawObjectInspector(data, _config, "RefClass");
                    foreach (var logMessage in log.Messages)
                        EditorGUILayout.LabelField(logMessage);
                });
                EditorGUILayout.Separator();
            }

            if (!IsAvailable())
                GUI.enabled = false;
            
            if (GUILayout.Button("Create"))
            {
                _config.Add(GetData(_index).MakeCopy());
                return false;
            }
            GUI.enabled = true;
            if (GUILayout.Button("Cancel"))
                return false;

            return true;
        }

        private bool IsAvailable()
        {
            if (_index < 0)
                return false;
            var data = GetData(_index);
            if (string.IsNullOrEmpty(data.Id))
                return false;
            return _config.Get<BaseConfigData>().FirstOrDefault(instance => instance.Id == data.Id) == null;
        }

        BaseConfigData GetData(int index)
        {
            var type = _availableTypes[index];
            BaseConfigData result;
            if (!_instances.TryGetValue(type, out result))
            {
                result = (BaseConfigData) Activator.CreateInstance(type);
                _instances.Add(type, result);
            }
            return result;
        }
    }
}