using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Scripts.Tools
{
    public class GoogleSheets
    {
        public static void ReadGoogleSheet(string googleReference, List<string> sheets,
            Action<List<KeyValuePair<string, object>>> rawDataCall, ILogInstance log,
            string appName = "Valkyrie application",
            string credentialsFilePath = "Assets/Valkyrie/ThirdParty/credentials.json")
        {
            var service = CreateSpreadSheetService(log, appName, credentialsFilePath);
            foreach (var sheet in sheets)
            {
                ReadSheet(googleReference, service, sheet, rawDataCall, log);
            }
        }
        
        static SheetsService CreateSpreadSheetService(ILogInstance log, string appName, string credentialsFilePath)
        {
            string[] scopes = { SheetsService.Scope.SpreadsheetsReadonly };
            UserCredential credential;

            using (var stream =
                new FileStream(credentialsFilePath, FileMode.Open, FileAccess.Read))
            {
                string credPath = "token";
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                log.Info("Credential file saved to: " + credPath);
            }

            return new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = appName
            });
        }
        
        static void ReadSheet(string spreadsheetId, SheetsService service, string spreadsheetName, Action<List<KeyValuePair<string, object>>> parseRow, ILogInstance log)
        {
            try
            {
                var request = service.Spreadsheets.Values.Get(spreadsheetId, spreadsheetName);
                request.MajorDimension = SpreadsheetsResource.ValuesResource.GetRequest.MajorDimensionEnum.ROWS;
                request.ValueRenderOption = SpreadsheetsResource.ValuesResource.GetRequest.ValueRenderOptionEnum
                    .UNFORMATTEDVALUE;
                var response = request.Execute();
                var rows = response.Values;

                var names = rows[0];

                if (rows.Count < 2)
                    return;
                if (names.Count < 3)
                    return;

                for (var index = 1; index < rows.Count; index++)
                {
                    var resList = new List<KeyValuePair<string, object>>();
                    var r = rows[index];
                    var cCount = System.Math.Min(r.Count, names.Count);
                    for (var i = 0; i < cCount; ++i)
                        resList.Add(new KeyValuePair<string, object>(names[i].ToString(), r[i]));
                    parseRow(resList);
                }
            }
            catch (Exception e)
            {
                log.Error($"Error in sheet {spreadsheetName}: {e}");
                throw;
            }
        }
    }
}