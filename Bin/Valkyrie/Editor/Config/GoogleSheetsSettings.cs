using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Valkyrie.Config;
using Valkyrie.Data;
using Valkyrie.Data.Config;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Scripts.Tools
{
    [CreateAssetMenu(fileName = "GoogleCfgSettings", menuName = "Valkyrie/GoogleCfgSettings", order = 1)]
    public class GoogleSheetsSettings : ScriptableObject
    {
        public string GoogleReference;
        public string TargetFile;
        public List<string> Sheets = new List<string>();

        [MenuItem("Valkyrie/Config/Download Google docs")]
        static void DownloadAll()
        {
            var log = new ConfigEditorWindow.UnityLog();
            var assets = AssetDatabase.FindAssets("t:GoogleSheetsSettings");
            foreach (var assetPath in assets.Select(AssetDatabase.GUIDToAssetPath))
            {
                Debug.Log($"Start downloading {assetPath}");
                AssetDatabase.LoadAssetAtPath<GoogleSheetsSettings>(assetPath).Download(log);
                Debug.Log($"End downloading {assetPath}");
            }
            
            AssetDatabase.Refresh();
        }

        static void Remove(IEditableConfiguration config, string id)
        {
            var old = config.Get<BaseConfigData>(id);
            if (old != null)
                config.Remove(old);
        }

        private void Download(ILogInstance log)
        {
            var types = typeof(BaseConfigData).GetAllSubTypes(x => !x.IsAbstract);
            var config = EditorUtils.LoadConfiguration();
            
            config.Clear();
            var ta = AssetDatabase.LoadAssetAtPath<TextAsset>(TargetFile);
            if (ta != null)
                config.Load(new MemoryStream(ta.bytes), ConfigType.Xml);

            void SetValue(BaseConfigData o, KeyValuePair<string, object> valuePair)
            {
                try
                {
                    var propInfo = o.GetType().GetProperty(valuePair.Key);
                    if (propInfo != null)
                        propInfo.SetValue(o, ParseValue(propInfo.PropertyType, valuePair.Value.ToString(), config),
                            null);
                    else
                    {
                        var fieldInfo = o.GetType().GetField(valuePair.Key);
                        if (fieldInfo != null)
                            fieldInfo.SetValue(o, ParseValue(fieldInfo.FieldType, valuePair.Value.ToString(), config));
                        else
                            log.Warn($"Param {valuePair.Key} not found at {o.GetType().Name}");
                    }
                }
                catch (Exception e)
                {
                    log.Error($"{o.Id} error on {valuePair.Key}: {e}");
                }
            }
            void ParseRow(List<KeyValuePair<string, object>> obj)
            {
                var id = obj[0].Value.ToString();
                if (id.StartsWith("*"))
                {
                    Remove(config, id.Trim('*'));
                    return;
                }

                var typeName = obj[1].Value.ToString();
                try
                {
                    var type = types.Find(u => u.Name == typeName);
                    if (type == null)
                    {
                        log.Warn($"Can not find type {typeName} for Entity={id}");
                        return;
                    }

                    Remove(config, id);
                    var instance = (BaseConfigData) Activator.CreateInstance(type);
                    instance.Id = id;
                    config.Add(instance);
                    for (var i = 2; i < obj.Count; ++i)
                    {
                        if (obj[i].Key.StartsWith("*"))
                            continue;
                        SetValue(instance, obj[i]);
                    }
                }
                catch (Exception e)
                {
                    log.Error(e.ToString());
                }
            }
            
            Debug.Log($"Start importing {GoogleReference} to {TargetFile}");
            GoogleSheets.ReadGoogleSheet(GoogleReference, Sheets, ParseRow, log, "Valkyrie application",
                "Assets/Valkyrie/ThirdParty/credentials.json");
            
            config.Fetch();
            ConfigEditorWindow.Save(config, TargetFile);
            
            Debug.Log($"{GoogleReference} imported to {TargetFile}");
        }

        class TempConfig : BaseConfigData
        {
            public TempConfig(string id)
            {
                base.Id = id;
            }
        }

        private static object ParseValue(Type type, string value, IConfiguration configuration)
        {
            if (type == typeof(string)) return value;
            if (type == typeof(int)) return int.Parse(value);
            if (type == typeof(long)) return long.Parse(value);
            if (type == typeof(float)) return float.Parse(value);
            if (type.IsEnum) return Enum.Parse(type, value);
            if (type == typeof(bool)) return bool.Parse(value);
            if (typeof(BaseConfigData).IsAssignableFrom(type))
            {
                return value.NotNullOrEmpty()
                    ? configuration.Get<BaseConfigData>(value) ?? new TempConfig(value)
                    : null;
            }

            if (type.IsValueType || type == typeof(System.Numerics.Vector2) || type == typeof(System.Numerics.Vector3))
                return value.ToObject(type);
            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                var values = (value.IsNullOrEmpty())
                    ? new string[0]
                    : value.Split(new[] {"; ", ";"}, StringSplitOptions.RemoveEmptyEntries);

                var t = type.GetGenericArguments()[0];

                var os = values.Select(s => ParseValue(t, s, configuration)).ToList();

                var result = Activator.CreateInstance(type);
                var add = type.GetMethod("Add");
                foreach (var o in os) add.Invoke(result, new[] {o});
                return result;
            }

            throw new Exception($"{type.Name} is not supported data type");
        }



    }
}