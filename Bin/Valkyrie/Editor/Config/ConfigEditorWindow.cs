﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Valkyrie.Data.Config;
using Valkyrie.Tools.Logs;
using Valkyrie.Ui.Window;

namespace Valkyrie.Config
{
    public class ConfigEditorWindow : ValkyrieEditorWindow
    {
        #region ValkyrieEditorWindow
        
        protected override void BuildLayout(EditorWindowFactory factory)
        {
            factory.BeginHorizontal()
                .Panel(DrawListPanel, false)
                .Panel(DrawInfoPanel, false)
                .EndHorizontal();
        }

        protected override void DrawMenuImpl()
        {
            GUILayout.BeginVertical();
            GUILayout.BeginHorizontal();
            if(GUILayout.Button("Save", EditorStyles.toolbarButton, GUILayout.Width(50)))
                Save();
            if (GUILayout.Button("Load", EditorStyles.toolbarButton, GUILayout.Width(50)) || Application.isPlaying)
                _cfg = null;
            if (GUILayout.Button("Create", EditorStyles.toolbarButton, GUILayout.Width(50)))
            {
                _showCreation = true;
                _selectedData = null;
            }
            
            GUILayout.EndHorizontal();
            DrawFilter();
            GUILayout.EndVertical();
        }
        
        #endregion

        #region Fields

        const string CfgFilePath = "Assets/Resources/config.xml";

        private IEditableConfiguration _cfg;

        private bool _showCreation;
        private bool _settingsVisible;
        private bool _configsVisible = true;
        private string _filterRegExpString;
        private Regex _filteRegex;
        private Vector2 _configsScroll = Vector2.zero;
        private readonly Dictionary<string, bool> _datasVisibility = new Dictionary<string, bool>();
        private CreateConfigEditor _creationEditor;
        private string _selectedData;

        IEditableConfiguration Config
        {
            get { return _cfg ?? (_cfg = ReloadConfig()); }
        }

        Regex Filter
        {
            get
            {
                if (_filteRegex == null)
                {
                    _filteRegex = new Regex(string.IsNullOrEmpty(_filterRegExpString) ? "" : _filterRegExpString,
                        RegexOptions.IgnoreCase);
                }

                return _filteRegex;
            }
        }

        #endregion

        #region Utils

        IEditableConfiguration ReloadConfig()
        {
            _cfg = EditorUtils.LoadConfiguration();
            if (_creationEditor != null)
                _creationEditor.Config = _cfg;
            //_datasVisibility.Clear();
            return _cfg;
        }

        public static void Save(IEditableConfiguration cfg, string filePath = CfgFilePath)
        {
            EditorUtils.CreateFolder(Path.GetDirectoryName(filePath));
            using (var stream = File.Create(filePath))
                cfg.Save(stream, ConfigType.Xml);
            AssetDatabase.ImportAsset(filePath, ImportAssetOptions.ForceUpdate);
        }

        private void Save()
        {
            Save(Config);
        }

        #region showing

        [MenuItem("Valkyrie/Config/Editor")]
        static void OpenWindow()
        {
            var window = GetWindow<ConfigEditorWindow>();
            window.Show();
            window.MenuHeight = 45f;
            window.titleContent = new GUIContent("Config Editor, Valkyrie(c)");
        }

        #endregion

        #endregion

        #region drawing

        void DrawFilter()
        {
            var tempFilter = EditorGUILayout.TextField("Filter:", _filterRegExpString, EditorStyles.toolbarTextField, GUILayout.Width(300));
            if (tempFilter != _filterRegExpString)
            {
                _filterRegExpString = tempFilter;
                _filteRegex = null;
            }
        }

        void DrawListPanel()
        {
            EditorUtils.DrawScrollRegion(ref _configsScroll, () =>
            {
                var regex = Filter;
                foreach (var data in Config.Get<BaseConfigData>()
                    .Where(u => regex.IsMatch(u.Id) || regex.IsMatch(u.GetType().Name)))
                    if(GUILayout.Toggle(_selectedData == data.Id, $"{data.Id} ({EditorUtils.GetCoolName(data.GetType().Name)})"))
                    {
                        _showCreation = false;
                        _selectedData = data.Id;
                    }
            });
        }

        void DrawInfoPanel()
        {
            if(_showCreation)
                DrawCreation();
            else
                DrawSelected();
        }

        void DrawSelected()
        {
            var data = Config.Get<BaseConfigData>().FirstOrDefault(u => u.Id == _selectedData);
            if (data != null && DrawData(data))
                    Config.Remove(data);
        }

        // ReSharper disable once InconsistentNaming
        // ReSharper disable once UnusedMember.Local
        private void OldOnGui()
        {
            if (_showCreation)
                DrawCreation();
            else
                DrawConfigs();

            EditorGUILayout.Separator();

            DrawSettings();

            EditorGUILayout.Separator();


            if (!Application.isPlaying && GUILayout.Button("Reload cfg"))
                _cfg = null;
            if (GUILayout.Button("Save config"))
                Save();
            if (Application.isPlaying)
                _cfg = null;
        }

        private void DrawCreation()
        {
            if (_creationEditor == null)
                _creationEditor = new CreateConfigEditor(Config);

            _showCreation = _creationEditor.OnGUI();
            if (!_showCreation)
                Save();
        }

        private void DrawSettings()
        {
            EditorUtils.DrawToggleRegion(ref _settingsVisible, GUI.color, "Settings",
                () => { EditorGUILayout.LabelField("Not implemented"); });
        }

        private void DrawConfigs()
        {
            EditorUtils.DrawToggleRegion(ref _configsVisible, GUI.color, "Data", () =>
            {
                var tempFilter = EditorGUILayout.TextField("Filter:", _filterRegExpString);
                if (tempFilter != _filterRegExpString)
                {
                    _filterRegExpString = tempFilter;
                    _filteRegex = null;
                }

                EditorUtils.DrawScrollRegion(ref _configsScroll, () =>
                {
                    var datasToRemove = new List<BaseConfigData>();
                    var regex = Filter;
                    foreach (
                        var data in
                        Config.Get<BaseConfigData>()
                            .Where(u => regex.IsMatch(u.Id) || regex.IsMatch(u.GetType().Name)))
                        if (DrawData(data))
                            datasToRemove.Add(data);
                    foreach (var data in datasToRemove)
                        Config.Remove(data);
                });
            });
            if (GUILayout.Button("Create"))
            {
                _showCreation = true;
            }
        }

        private bool DrawData(BaseConfigData data)
        {
            bool value;
            if (!_datasVisibility.TryGetValue(data.Id, out value))
                value = true;
            
            var result = false;
            var log = new UnityLog();
            EditorUtils.DrawToggleRegion(ref value, data.Validate(log) ? GUI.color : Color.red,
                $"{data.Id} ({EditorUtils.GetCoolName(data.GetType().Name)})",
                () =>
                {
                    EditorUtils.DrawObjectInspector(data, Config, "Id", "RefClass");

                    foreach (var logMessage in log.Messages)
                        EditorGUILayout.LabelField(logMessage);

                    if (Config.GetType().GetMethod("Remove") != null &&
                        GUILayout.Button($"Delete {data.Id}"))
                        result = true;
                });

            _datasVisibility[data.Id] = value;
            return result;
        }

        public class UnityLog : ILogInstance
        {
            public readonly List<string> Messages = new List<string>();

            public void Info(string format)
            {
                Messages.Add(format);
                UnityEngine.Debug.Log(format);
            }

            public void Debug(string format)
            {
                Messages.Add(format);
                UnityEngine.Debug.Log(format);
            }

            public void Warn(string format)
            {
                Messages.Add(format);
                UnityEngine.Debug.LogWarning(format);
            }

            public void Error(string format)
            {
                Messages.Add(format);
                UnityEngine.Debug.LogError(format);
            }
        }

        #endregion
    }
}
