﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using Valkyrie;
using Valkyrie.Data;
using Valkyrie.Runtime.Console;
using Valkyrie.Tools.Logs;
using Valkyrie.Ui.Window;
using Valkyrie.UnityExtensions;

namespace Assets.Valkyrie.Editor.Console
{
    public class EditorDeveloperConsoleWindow : ValkyrieEditorWindow
    {
        #region Menu

        [MenuItem("Valkyrie/Tools/Console")]
        static void OpenWindow()
        {
            var window = GetWindow<EditorDeveloperConsoleWindow>();
            window.MenuHeight = 40;
            window.titleContent.text = "Developer Console, Valkyrie(c)";
            window.Show();
        }

        #endregion

        #region Classes

        class RegexListMatcher
        {
            public RegexListMatcher()
            {
                InfoShow = WarnShow = ErrorShow = true;
            }

            private string _filterText = ".*";
            private Regex _matcher;

            public string Filter
            {
                get => _filterText;
                set
                {
                    if (_filterText == value) return;
                    _filterText = value;
                    _matcher = null;
                }
            }

            public bool DebugShow { get; set; }
            public bool InfoShow { get; set; }
            public bool WarnShow { get; set; }
            public bool ErrorShow { get; set; }

            Regex GetMatcher()
            {
                if (_matcher == null)
                    _matcher = string.IsNullOrEmpty(_filterText) ? new Regex(".*") : new Regex(_filterText);
                return _matcher;
            }

            public List<LogRecord> Match(List<LogRecord> list, IEnumerable<LogRecord> source)
            {
                var matcher = GetMatcher();

                var messages = source.Where(u =>
                {
                    switch (u.Level)
                    {
                        case MessageLevel.Error:
                            if (!ErrorShow)
                                return false;
                            break;
                        case MessageLevel.Warning:
                            if (!WarnShow)
                                return false;
                            break;
                        case MessageLevel.Info:
                            if (!InfoShow)
                                return false;
                            break;
                        case MessageLevel.Debug:
                            if (!DebugShow)
                                return false;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                    if (!matcher.IsMatch(u.Message))
                        return false;
                    if (!matcher.IsMatch(u.Channel))
                        return false;
                    return true;
                }).ToArray();

                list.Clear();
                list.AddRange(messages);

                return list;
            }
        }

        #endregion

        #region Fields

        private Vector2 _logScrollPosition = Vector2.zero;
        private object _selected;
        private Vector2 _infoScrollPosition = Vector2.zero;
        readonly RegexListMatcher _matcher = new RegexListMatcher();

        private readonly List<LogRecord> _messages = new List<LogRecord>();
        private string _commandText;

        #endregion


        #region Menu panel

        private bool collapse;
        private bool clearOnPlay;
        private bool errorPause;

        private Texture2D errorIcon;
        private Texture2D errorIconSmall;
        private Texture2D warningIcon;
        private Texture2D warningIconSmall;
        private Texture2D infoIcon;
        private Texture2D infoIconSmall;
        private GUIStyle boxStyle;
        private Texture2D boxBgOdd;
        private Texture2D boxBgEven;
        private Texture2D boxBgSelected;
        private GUIStyle textAreaStyle;

        protected override void DrawMenuImpl()
        {
            var console = Console;

            GUILayout.BeginVertical();
            
                GUILayout.BeginHorizontal();
    
                    GUILayout.Button(new GUIContent("Clear"), EditorStyles.toolbarButton, GUILayout.Width(40));
                    GUILayout.Space(5);
        
                    collapse = GUILayout.Toggle(collapse, new GUIContent("Collapse"), EditorStyles.toolbarButton,
                        GUILayout.Width(60));
                    clearOnPlay = GUILayout.Toggle(clearOnPlay, new GUIContent("Clear On Play"), EditorStyles.toolbarButton,
                        GUILayout.Width(80));
                    errorPause = GUILayout.Toggle(errorPause, new GUIContent("Error Pause"), EditorStyles.toolbarButton,
                        GUILayout.Width(70));
        
                    GUILayout.FlexibleSpace();
        
                    _matcher.DebugShow = _matcher.InfoShow = GUILayout.Toggle(_matcher.InfoShow,
                        new GUIContent("L", infoIconSmall), EditorStyles.toolbarButton, GUILayout.Width(30));
                    _matcher.WarnShow = GUILayout.Toggle(_matcher.WarnShow, new GUIContent("W", warningIconSmall),
                        EditorStyles.toolbarButton, GUILayout.Width(30));
                    _matcher.ErrorShow = GUILayout.Toggle(_matcher.ErrorShow, new GUIContent("E", errorIconSmall),
                        EditorStyles.toolbarButton, GUILayout.Width(30));
    
                GUILayout.EndHorizontal();
            
                _matcher.Filter = EditorGUILayout.TextField("Filter:", _matcher.Filter, EditorStyles.toolbarTextField,
                    GUILayout.Width(300));
                _matcher.Match(_messages, console != null ? console.Messages : _messages);
                
            GUILayout.EndVertical();
        }

        #endregion

        private IDeveloperConsole Console =>
            Application.isPlaying ? FindObjectOfType<Core>().RootScope.Resolve<IDeveloperConsole>() : null;

        protected override void OnEnable()
        {
            base.OnEnable();

            errorIcon = EditorGUIUtility.Load("icons/console.erroricon.png") as Texture2D;
            warningIcon = EditorGUIUtility.Load("icons/console.warnicon.png") as Texture2D;
            infoIcon = EditorGUIUtility.Load("icons/console.infoicon.png") as Texture2D;

            errorIconSmall = EditorGUIUtility.Load("icons/console.erroricon.sml.png") as Texture2D;
            warningIconSmall = EditorGUIUtility.Load("icons/console.warnicon.sml.png") as Texture2D;
            infoIconSmall = EditorGUIUtility.Load("icons/console.infoicon.sml.png") as Texture2D;

            boxStyle = new GUIStyle {normal = {textColor = new Color(0.7f, 0.7f, 0.7f)}};
            boxBgOdd = EditorGUIUtility.Load("builtin skins/darkskin/images/cn entrybackodd.png") as Texture2D;
            boxBgEven = EditorGUIUtility.Load("builtin skins/darkskin/images/cnentrybackeven.png") as Texture2D;
            boxBgSelected = EditorGUIUtility.Load("builtin skins/darkskin/images/menuitemhover.png") as Texture2D;


            textAreaStyle = new GUIStyle
            {
                normal =
                {
                    textColor = new Color(0.9f, 0.9f, 0.9f),
                    background =
                        EditorGUIUtility.Load("builtin skins/darkskin/images/projectbrowsericonareabg.png") as Texture2D
                }
            };
        }

        protected override void BuildLayout(EditorWindowFactory factory)
        {
            factory.BeginVertical();
            factory.Panel(DrawLogsPanel, false);
            factory.Panel(DrawDetailInfoPanel, false);
            factory.EndVertical();
        }
        
        private void DrawLogsPanel(Rect upperPanel)
        {
            GUILayout.BeginArea(upperPanel);
            _logScrollPosition = EditorUtils.DrawScrollRegion(ref _logScrollPosition, () =>
            {
                var logs = _messages;
                for (int i = 0; i < logs.Count; i++)
                {
                    var c = logs[i];
                    if (DrawBox(c.Message, c.Level, i % 2 == 0, _selected == (object) c))
                    {
                        _selected = c;
                        GUI.changed = true;
                    }
                }
            });
            var nameInput = "INPUT_CONSOLE_FIELD";
            GUI.SetNextControlName(nameInput);
            _commandText = EditorGUILayout.DelayedTextField(_commandText);
            if (EditorUtils.KeysPressed(nameInput, KeyCode.Return, KeyCode.KeypadEnter))
            {
                var temp = _commandText;
                _commandText = string.Empty;
                if (temp.NotNullOrEmpty())
                    Console?.Execute(temp);

                Repaint();
            }
            GUILayout.EndArea();
        }

        private void DrawDetailInfoPanel(Rect lowerPanel)
        {
            GUILayout.BeginArea(lowerPanel);
            _infoScrollPosition = GUILayout.BeginScrollView(_infoScrollPosition);
 
            if (_selected != null && _selected is LogRecord log)
            {
                GUILayout.TextArea(log.Message, textAreaStyle);
                GUILayout.Space(5);
                var sb = new StringBuilder();
                for (int i = 0; i < log.StackTrace.FrameCount; ++i)
                {
                    var frame = log.StackTrace.GetFrame(i);

                    var method = frame.GetMethod();
                    var dt = method.DeclaringType;
                    var shortInfo =
                        $"{method.Name}({string.Join(",", method.GetParameters().Select(u => u.ParameterType.Name))})";
                    var methodInfo = dt != null ? $"{dt.FullName}.{shortInfo}" : shortInfo;
                    var txt = $"{methodInfo} in {frame.GetFileName()}:{frame.GetFileLineNumber()}:{frame.GetFileColumnNumber()}";
                    sb.AppendLine(txt);
                    if (GUILayout.Button(new GUIContent(txt), textAreaStyle, GUILayout.ExpandWidth(true), GUILayout.Height(30)))
                        EditorUtility.OpenWithDefaultApp(frame.GetFileName());
                }
                //GUILayout.TextArea(sb.ToString(), textAreaStyle);
            }
 
            GUILayout.EndScrollView();
            GUILayout.EndArea();
        }

        private bool DrawBox(string content, MessageLevel boxType, bool isOdd, bool isSelected)
        {
            boxStyle.normal.background = isSelected
                ? boxBgSelected
                : (isOdd ? boxBgOdd : boxBgEven);

            Texture2D icon;

            switch (boxType)
            {
                case MessageLevel.Error:
                    icon = errorIcon;
                    break;
                case MessageLevel.Warning:
                    icon = warningIcon;
                    break;
                case MessageLevel.Info:
                    icon = infoIcon;
                    break;
                case MessageLevel.Debug:
                    icon = infoIcon;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(boxType), boxType, null);
            }

            return GUILayout.Button(new GUIContent(content, icon), boxStyle, GUILayout.ExpandWidth(true),
                GUILayout.Height(30));
        }
    }
}
