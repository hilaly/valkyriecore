using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Misc
{
    public interface IAnalytics
    {
        void ReportEvent(string eventName, Dictionary<string, object> parameters);
    }

    public class DummyAnalytics : IAnalytics
    {
        public void ReportEvent(string eventName, Dictionary<string, object> parameters)
        {
            UnityEngine.Debug.Log(
                $"[analytics] {eventName}: {string.Join(",", parameters.Select(x => $"{x.Key}={x.Value}"))}");
        }
    }
}