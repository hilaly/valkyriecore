using UnityEngine;
using Valkyrie.Misc;
using Volatile;

namespace Valkyrie.Scripts.VolatilePhysics
{
    public static
        class VolatileExt
    {
        public static VoltBody CreateStaticBody(this VoltWorld world, UnityEngine.Vector2 position, float radians,
            VoltShape[] shapes)
        {
            return world.CreateStaticBody(position.ToValkyrie(), radians, shapes);
        }

        public static VoltBody CreateDynamicBody(this VoltWorld world, UnityEngine.Vector2 position, float radians,
            VoltShape[] shapes)
        {
            return world.CreateDynamicBody(position.ToValkyrie(), radians, shapes);
        }

        public static void Set(this VoltBody body, Vector2 position, float radians)
        {
            body.Set(position.ToValkyrie(), radians);
        }

        public static void AddForce(this VoltBody body, Vector2 force)
        {
            body.AddForce(force.ToValkyrie());
        }
    }
}