using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Monetization
{
    public interface IAdsManager
    {
        IPromise Init(string appId, bool isDebug);
        
        IPromise<bool> Show(string adId);
        IPromise<bool> ShowBanner(string adId);
        bool IsReady(string adId);
    }
}