using Valkyrie.Di;

namespace Valkyrie.Monetization
{
    class AdsLibrary : ILibrary
    {
        public void Register(IContainer container)
        {
#if UNITY_UNITYADS_API && USE_ADS
            container.Register<UnityAdsService>().AsInterfacesAndSelf().SingleInstance();
#endif
        }
    }
}