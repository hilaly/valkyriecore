#if UNITY_UNITYADS_API && USE_ADS
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.UnityExtensions;

namespace Valkyrie.Monetization
{
    /// <summary>
    /// https://unityads.unity3d.com/help/unity/integration-guide-unity#basic-implementation
    /// </summary>
    class UnityAdsService : IAdsManager
    {
        public IPromise Init(string appId, bool isDebug)
        {
            UnityEngine.Monetization.Monetization.Initialize(appId, isDebug);
            return Promise.Resolved();
        }

        public IPromise<bool> Show(string adPlacement)
        {
            try
            {
                var promise = new Promise<bool>();
                Core.GetInstance().StartCoroutine(ShowAdWhenReady(adPlacement, promise));
                return promise;
            }
            catch (Exception e)
            {
                return Promise<bool>.Rejected(e);
            }
        }

        public IPromise<bool> ShowBanner(string adPlacement)
        {
            try
            {
                var promise = new Promise<bool>();
                Core.GetInstance().StartCoroutine(ShowBannerWhenReady(adPlacement, promise));
                return promise;
            }
            catch (Exception e)
            {
                return Promise<bool>.Rejected(e);
            }
        }

        public bool IsReady(string adId)
        {
            return UnityEngine.Monetization.Monetization.IsReady(adId);
        }

        IEnumerator WaitForPlacement(string adPlacement, Action callback)
        {
            while (!IsReady(adPlacement))
                yield return new WaitForSeconds(0.1f);

            callback();
        }

        IEnumerator ShowAdWhenReady(string adPlacement, Promise<bool> promise)
        {
            yield return WaitForPlacement(adPlacement, () => {});

            if (UnityEngine.Monetization.Monetization.GetPlacementContent(adPlacement) is ShowAdPlacementContent ad)
                yield return ad.Show(result => promise.Resolve(result != UnityEngine.Monetization.ShowResult.Failed));
            else
                promise.Resolve(false);
        }

        IEnumerator ShowBannerWhenReady(string adPlacement, Promise<bool> promise)
        {
            yield return WaitForPlacement(adPlacement, () => {});
            try
            {

                Advertisement.Banner.Show(adPlacement);
                promise.Resolve(true);
            }
            catch (Exception e)
            {
                promise.Reject(e);
            }
        }
    }
}
    
#endif