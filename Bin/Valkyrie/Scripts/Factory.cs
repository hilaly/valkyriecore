﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.Monetization;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;
using Valkyrie.Ui.Adapters;
using Valkyrie.UnityExtensions;
using Valkyrie.UnityExtensions.Components;
using Valkyrie.UserInput.UnitySpecific;

// ReSharper disable UnusedMember.Global

namespace Valkyrie
{
    /// <summary>
    /// Loading flow:
    /// 1. download obb
    /// 2. load environment
    /// 3. connect file server
    /// 4. load config
    /// 5. load save data
    /// </summary>
    public class Factory
    {
        private readonly string _appId;
        private int _targetFrameRate = -1;
        private readonly List<ILibrary> _libraries = new List<ILibrary>();
        private string _deviceId;

        private Factory(string appId)
        {
            _appId = appId;
        }

        public static Factory Start(string appId, string deviceIdOverride = null)
        {
            return new Factory(appId) {_deviceId = deviceIdOverride};
        }

#if UNITY_UNITYADS_API
        public Factory UseUnityAds()
        {
            return UseLibrary(new AdsLibrary());
        }
#endif

        public Factory UseUnity(int targetFrameRate)
        {
            _targetFrameRate = targetFrameRate;
            return UseLibrary(new UnityExtensionLibrary());
        }

        public Factory UseMvvm(string pathToUiRootPrefab)
        {
            UiRoot.PathToPrefab = pathToUiRootPrefab;
            return UseLibrary(new AdaptersLibrary());
        }
        
        public Factory UseLibrary(ILibrary library)
        {
            _libraries.Add(library);
            return this;
        }

        public Factory UseData(bool isOnline)
        {
            if (isOnline)
                UseLibrary(Libraries.Network);
            return UseLibrary(Libraries.Data);
        }

        public IContainer Build()
        {
            var deviceId = _deviceId ?? SystemInfo.deviceUniqueIdentifier;
            var container = Configurator.Start(deviceId);
            foreach (var library in _libraries)
                container.RegisterLibrary(library);

            container = container.Build();
            //Use unity integration
            if (_targetFrameRate > 0)
            {
                var logService = container.Resolve<ILogService>();
                logService.Observe().Subscribe(lm =>
                {
                    switch(lm.Level)
                    {
                        case MessageLevel.Error:
                            Debug.LogError($"{lm.Channel}:{lm.Message}");
                            break;
                        case MessageLevel.Warning:
                            Debug.LogWarning($"{lm.Channel}:{lm.Message}");
                            break;
                        case MessageLevel.Info:
                        case MessageLevel.Debug:
                            Debug.Log($"{lm.Channel}:{lm.Message}");
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                });
//                var loginstance = logService.GetLog("Unity");
//                Application.logMessageReceived +=
//                    (condition, trace, type) => OnUnityLog(loginstance, condition, trace, type);
                CreateGameObject(container);
            }
            return container;
        }

        Core CreateGameObject(IContainer container)
        {
            Application.targetFrameRate = _targetFrameRate;
            var core = container.Inject(new GameObject("Valkyrie.Core").AddComponent<Core>());
            return core;
        }

        private void OnUnityLog(ILogInstance logInstance, string condition, string stacktrace, LogType type)
        {
            switch (type)
            {
                case LogType.Error:
                case LogType.Assert:
                case LogType.Exception:
                    logInstance.Error(condition);
                    break;
                case LogType.Warning:
                    logInstance.Warn(condition);
                    break;
                case LogType.Log:
                    logInstance.Info(condition);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}
