﻿using System;
using UnityEngine;
using UnityEngine.Profiling;
using Valkyrie.Di;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.UnityExtensions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class Core : MonoBehaviour
    {
        #region Singleton

        private static Core _instance;

        public static Core GetInstance()
        {
            return _instance;
        }

        #endregion

        #region Properties

        private IExecutor _executor;

        public IContainer RootScope { get; private set; }

        #endregion

        #region Overrides of MonoBehaviour

        // ReSharper disable once UnusedMember.Local
        void Awake()
        {
            if (_instance != null)
                throw new Exception("Try to create second instance of singleton");
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            Profiler.BeginSample("Valkyrie.Core.Iterate");
            try
            {
                _executor.Iterate();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                throw;
            }
            finally
            {
                Profiler.EndSample();
            }
        }

        // ReSharper disable once UnusedMember.Local
        private void LateUpdate()
        {
            Profiler.BeginSample("Valkyrie.Core.PostIterate");
            try
            {
                _executor.PostIterate();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                throw;
            }
            finally
            {
                Profiler.EndSample();
            }
        }

        // ReSharper disable once UnusedMember.Local
        void OnDestroy()
        {
            _instance = null;
        }

        #endregion

        [Inject] internal void Init(IContainer rootScope, IExecutor executor)
        {
            RootScope = rootScope;
            
            _executor = executor;
        }
        
        private void OnGUI()
        {
            var temp = GUI.color;
            GUI.color = new Color(0, 0.3f, 0, 1);
            GUILayout.Label($"Valkyrie v{typeof(IContainer).Assembly.GetName().Version}");
            GUI.color = temp;
        }
    }
}
