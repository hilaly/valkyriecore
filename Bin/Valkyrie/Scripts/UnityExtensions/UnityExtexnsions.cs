﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Data.Localization;
using Valkyrie.Di;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Entities;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;
using Valkyrie.UnityExtensions;
using Valkyrie.UnityExtensions.Components;
using Quaternion = System.Numerics.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace Valkyrie.Misc
{
    public static class UnityExtexnsions
    {
        #region Transforms

        public static IEnumerable<Transform> FindChildrensByRegex(this Transform parentTransform, string regexPattern)
        {
            var regex = new Regex(regexPattern);
            return
                parentTransform.GetComponentsInChildren<Transform>(true)
                    .Where(transform => regex.IsMatch(transform.name));
        }

        public static Transform FindChildrenByRegex(this Transform parentTransform, string regexPattern)
        {
            var regex = new Regex(regexPattern);
            return
                parentTransform.GetComponentsInChildren<Transform>(true)
                    .FirstOrDefault(transform => regex.IsMatch(transform.name));
        }

        public static GameObject ResetTransform(this GameObject o)
        {
            return Reset(o.transform).gameObject;
        }

        public static Transform Reset(this Transform tr)
        {
            tr.localPosition = Vector3.zero;
            tr.localRotation = UnityEngine.Quaternion.identity;
            tr.localScale = Vector3.one;
            return tr;
        }

        public static bool IsBehind(this Camera cameraProps, Vector3 point)
        {
            return cameraProps.WorldToViewportPoint(point).z <= 0;
        }

        public static Vector2 WorldToCanvas(this Camera cameraProps, Vector3 point, Component canvas)
        {
            var screenSpacePoint = WorldToNormalizedCanvas(cameraProps, point);
            return NormalizedCanvasToCanvas(screenSpacePoint, canvas);
        }

        public static bool IsOnScreen(this Camera cameraProps, Vector3 point)
        {
            var nc = cameraProps.WorldToViewportPoint(point);
            return nc.x > 0 && nc.x < 1 && nc.y > 0 && nc.y < 1 && nc.z > 0;
        }

        public static Vector2 WorldToNormalizedCanvas(this Camera cameraProps, Vector3 point)
        {
            var screenSpacePoint = cameraProps.WorldToViewportPoint(point) - new Vector3(0.5f, 0.5f, 0f);
            return new Vector2(screenSpacePoint.x, screenSpacePoint.y);
        }

        public static Vector2 NormalizedCanvasToCanvas(this Vector2 normalizedCanvasPoint, Component canvas)
        {
            var canvasSize = canvas.GetComponent<RectTransform>().sizeDelta;
            return new Vector2(normalizedCanvasPoint.x * canvasSize.x, normalizedCanvasPoint.y * canvasSize.y);
        }

        public static Bounds GetRendererBounds(this GameObject gameObject)
        {
            var bounds = new Bounds();
            var firstBounds = true;
            foreach (var r in gameObject.GetComponentsInChildren<Renderer>())
            {
                if (firstBounds)
                {
                    bounds = r.bounds;
                    firstBounds = false;
                }

                bounds.Encapsulate(r.bounds);
            }

            return bounds;
        }

        public static Bounds GetMeshBounds(this GameObject gameObject)
        {
            var bounds = new Bounds();
            var firstBounds = true;
            foreach (var r in gameObject.GetComponentsInChildren<MeshFilter>())
            {
                if (firstBounds)
                {
                    bounds = r.sharedMesh.bounds;
                    firstBounds = false;
                }

                bounds.Encapsulate(r.sharedMesh.bounds);
            }

            return bounds;
        }

        public static Bounds GetLocalBounds(this GameObject gameObject)
        {
            var rotation = gameObject.transform.rotation;
            gameObject.transform.rotation = UnityEngine.Quaternion.Euler(0f, 0f, 0f);
            var bounds = new Bounds(gameObject.transform.position, Vector3.zero);
            foreach (var renderer in gameObject.GetComponentsInChildren<Renderer>())
                bounds.Encapsulate(renderer.bounds);
            bounds.center = bounds.center - gameObject.transform.position;
            gameObject.transform.rotation = rotation;
            return bounds;
        }

        #endregion

        #region Input

        public static Ray ToWorldRay(this Touch touch)
        {
            return Camera.main.ScreenPointToRay(touch.position);
        }

        public static Ray ScreenPointToWorldRay(this Vector2 point)
        {
            return Camera.main.ScreenPointToRay(point);
        }

        #endregion

        #region Rx

        static IEnumerator AsyncOperationCoroutine(AsyncOperation asyncOperation, ISubject<float> subject)
        {
            while (!asyncOperation.isDone)
            {
                subject.OnNext(asyncOperation.progress);
                yield return null;
            }

            subject.OnNext(asyncOperation.progress);
            yield return null;
            subject.OnNext(asyncOperation.progress);
            yield return null;
            subject.OnNext(asyncOperation.progress);
            subject.OnCompleted();
        }

        public static T AttachTo<T>(this T disposableInstance, Component component) where T : IDisposable
        {
            return disposableInstance.AttachTo(component.gameObject);
        }

        public static T AttachTo<T>(this T disposableInstance, GameObject gameObject) where T : IDisposable
        {
            var disposable = gameObject.GetComponent<DisposableUnityComponent>();
            if (disposable == null)
                disposable = gameObject.gameObject.AddComponent<DisposableUnityComponent>();
            disposable.Add(disposableInstance);
            return disposableInstance;
        }

        public static IObservable<float> ToObservable(this AsyncOperation asyncOperation)
        {
            var result = new Subject<float>();
            Core.GetInstance().StartCoroutine(AsyncOperationCoroutine(asyncOperation, result));
            return result;
        }

        #endregion

        #region Math

        public static Vector2 GetXZ(this Vector3 v)
        {
            return new Vector2(v.x, v.z);
        }
        
        public static Vector3 ComponentMultiplier(Vector3 a, Vector3 b)
        {
            var x = a.x * b.x;
            var y = a.y * b.y;
            var z = a.z * b.z;
            return new Vector3(x, y, z);
        }

        public static Vector3 ComponentDivided(Vector3 a, Vector3 b)
        {
            var x = a.x / b.x;
            var y = a.y / b.y;
            var z = a.z / b.z;
            return new Vector3(x, y, z);
        }

        public static Vector3 ToUnity(this System.Numerics.Vector3 o)
        {
            return new Vector3(o.X, o.Y, o.Z);
        }

        public static System.Numerics.Vector3 ToValkyrie(this Vector3 o)
        {
            return new System.Numerics.Vector3(o.x, o.y, o.z);
        }

        public static Vector2 ToUnity(this System.Numerics.Vector2 o)
        {
            return new Vector2(o.X, o.Y);
        }

        public static System.Numerics.Vector2 ToValkyrie(this Vector2 o)
        {
            return new System.Numerics.Vector2(o.x, o.y);
        }

        public static Quaternion ToValkyrie(this UnityEngine.Quaternion q)
        {
            return new Quaternion(q.x, q.y, q.z, q.w);
        }

        public static UnityEngine.Quaternion ToUnity(this Quaternion q)
        {
            return new UnityEngine.Quaternion(q.X, q.Y, q.Z, q.W);
        }

        #endregion

        #region Ui

        #endregion

        #region Ecs

        public static IEcsEntityHolder HoldGameObject(this IEcsEntity entity, GameObject go)
        {
            var result = go.AddComponent<EcsGoHolderComponent>();
            result.EcsEntity = entity;
            return result;
        }

        class EcsGoHolderComponent : MonoBehaviour, IEcsEntityHolder
        {
            public IEcsEntity EcsEntity { get; set; }
#pragma warning disable 0649
            [SerializeField] private bool _showInfo;
#pragma warning restore 0649

            private void OnGUI()
            {
                if (!_showInfo)
                    return;

                var sb = new StringBuilder();

                foreach (var type in typeof(IEcsComponent).GetAllSubTypes(type => !type.IsAbstract))
                {
                    var c = EcsEntity.GetComponent(type);
                    if (c == null)
                        continue;
                    try
                    {
                        sb.AppendLine($"{type.Name}: {c.ToStringInfo()}");
                    }
                    catch (Exception e)
                    {
                        Debug.LogException(e);
                    }
                }

                GUILayout.Label(sb.ToString());
            }

            public void DestroyEntity()
            {
                EcsEntity.Destroy();
            }
        }

#if UNITY_EDITOR
        [CustomEditor(typeof(EcsGoHolderComponent))]
        class EcsGoHolderComponentEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                DrawDefaultInspector();

                if (GUILayout.Button("Destroy"))
                    ((EcsGoHolderComponent) target).DestroyEntity();
            }
        }
#endif

        private static string ToStringInfo(this object o)
        {
            return o == null ? "null" : o.ToJson();
        }

        public class GameObjectOwnerComponent : IEcsComponent, IDisposable
        {
            private readonly GameObject _go;
            private readonly IAssetsDestroyer _assetsDestroyer;

            public GameObject gameObject => _go;
            public Transform Transform => _go.transform;

            public T GetComponent<T>()
            {
                return _go.GetComponent<T>();
            }

            public GameObjectOwnerComponent(IEcsEntity ecsEntity, GameObject go, IAssetsDestroyer assetsDestroyer)
            {
                _go = go;
                _assetsDestroyer = assetsDestroyer;
                ecsEntity.HoldGameObject(_go);
                //Object.DontDestroyOnLoad(_go);
            }

            public void Dispose()
            {
                _assetsDestroyer.Destroy(_go);
            }
        }

        public static GameObject GetEcsView(this IEcsEntity entity)
        {
            return entity.Get<GameObjectOwnerComponent>()?.gameObject;
        }

        public static GameObject ConnectEcsView(this IAssetsDestroyer assetsDestroyer, IEcsEntity entity, GameObject goInstance)
        {
            entity.Add(new GameObjectOwnerComponent(entity, goInstance, assetsDestroyer));
            return goInstance;
        }

        public static GameObject CreateEcsView(this IAssetsFactory assetsFactory, IEcsEntity entity, string resourcePath, Transform parentTransform = null)
        {
            var goInstance = assetsFactory.LoadAndInstantiate(resourcePath, parentTransform);
            return assetsFactory.ConnectEcsView(entity, goInstance);
        }

        public static GameObject CreateEcsView(this IAssetsFactory assetsFactory, IEcsEntity entity, DataExtensions.VisualAssetConfigData configData, Transform parentTransform = null)
        {
            var goInstance = assetsFactory.CreateFromVisualConfig(configData);
            return assetsFactory.ConnectEcsView(entity, goInstance);
        }

        #endregion

        #region Di

        public static IConcreteTypeFactoryRegistration<T> RegisterFromComponentInNewPrefabResource<T>(this IContainer c,
            string path, Transform spawnRoot)
        {
            return c.Register<T>(container =>
                    container.Resolve<IAssetsFactory>().LoadAndInstantiate(path, spawnRoot).GetComponentInChildren<T>())
                .OnActivation(u => c.Inject(u));
        }

        public static IConcreteTypeFactoryRegistration<T> RegisterFromComponentInNewPrefab<T>(this IContainer c,
            GameObject prefab, Transform spawnRoot)
        {
            return c.Register<T>(container =>
                container.Resolve<IAssetsFactory>().Instantiate(prefab, spawnRoot).GetComponentInChildren<T>())
                .OnActivation(u => c.Inject(u));
        }

        public static IConcreteTypeFactoryRegistration<T> RegisterFromComponentOn<T>(this IContainer c, GameObject o)
        {
            return c.Register(o.GetComponentInChildren<T>)
                .OnActivation(u => c.Inject(u));
        }

        public static IConcreteTypeFactoryRegistration<T> RegisterFromNewComponentOn<T>(this IContainer c, GameObject o)
            where T : Component
        {
            return c.Register(o.AddComponent<T>)
                .OnActivation(u => c.Inject(u));
        }

        public static IDisposable SetActive(this IContainer container)
        {
            return ContextManager.SetActiveContainer(container);
        }

        #endregion

        #region Localization

        public static IPromise<ILocalization> LoadAllLocales(this IContainer container, string folder)
        {
            return container.LoadAllLocales(Application.systemLanguage, folder);
        }
        public static IPromise<ILocalization> LoadAllLocales(this IContainer container, SystemLanguage targetLanguage, string folder)
        {
            var localization = container.Resolve<ILocalization>();
            #if UNITY_EDITOR
            localization.IsDebug = true;
            #endif
            localization.Language = targetLanguage.ToString();
            foreach (SystemLanguage language in Enum.GetValues(typeof(SystemLanguage)))
            {
                var resource = Resources.Load<TextAsset>(Path.Combine(folder, language.ToString()));
                if (resource == null)
                {
                    if (Application.systemLanguage == language)
                        localization.Language = SystemLanguage.English.ToString();
                    Debug.LogWarning($"Localization for {language} missing");
                    continue;
                }

                using (var stream = new MemoryStream(resource.bytes))
                    container.Resolve<ILocalizationSerializer>().Load(stream).Done();
                Debug.Log($"Localization for {language} loaded");
            }

            return Promise<ILocalization>.Resolved(localization);
        }

        public static IList<string> GetAllKeys(this ILocalization localization)
        {
            return ((ILocalizationEditor) localization).Keys;
        }

        public static void SaveLocales(IEnumerable<LocalizationSource> sources, string rootPath = "Assets/Resources/Localization")
        {
            foreach (var localizationSource in sources)
            {
                var path = Path.Combine(rootPath, $"{localizationSource.Language}.json");
                File.WriteAllText(path, JsonConvert.SerializeObject(localizationSource, Formatting.Indented));
                UnityEngine.Debug.Log($"Save localization for {localizationSource.Language}");
            }
        }

        #endregion
    }
}
