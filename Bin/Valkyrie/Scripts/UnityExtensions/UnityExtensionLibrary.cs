﻿using Valkyrie.Data;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.UnityExtensions.Physics;
using Valkyrie.UnityExtensions.Pool;
using Valkyrie.UserInput;
using Valkyrie.UserInput.UnitySpecific;

namespace Valkyrie.UnityExtensions
{
    internal class UnityExtensionLibrary : ILibrary
    {
        public void Register(IContainer builder)
        {
            builder.Register<UnityProfiler>().AsInterfacesAndSelf().SingleInstance();
            
            builder.Register<Raycaster>().As<IRaycaster>().InstancePerDependency();

            builder.Register<InputHelper>().As<IInput>().SingleInstance();

            builder.Register<AssetsFactory>().As<IAssetsFactory>().As<IAssetsLoader>().As<IAssetsDestroyer>().SingleInstance();
            
            builder.Register<UnityPersistentStorage>().As<IPersistentStorage>().SingleInstance();

            builder.Register(container => Core.GetInstance()).AsSelf().SingleInstance();

            builder.Register<GameObjectPool>().As<IGameObjectPool>().SingleInstance();
            
            builder.Register<UnityControls>()
                .As<ComplexControls>()
                .As<IUnityControls>()
                .As<IControls>()
                .AsSelf()
                .SingleInstance();
        }
    }
}
