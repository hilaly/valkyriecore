using Valkyrie.Tools;

namespace Valkyrie.Misc
{
    class UnityProfiler : IProfiler
    {
        public void BeginSample(string name)
        {
            UnityEngine.Profiling.Profiler.BeginSample(name);
        }

        public void EndSample()
        {
            UnityEngine.Profiling.Profiler.EndSample();
        }
    }
}