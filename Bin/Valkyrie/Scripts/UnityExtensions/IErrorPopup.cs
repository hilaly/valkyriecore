namespace Valkyrie.UnityExtensions
{
    public interface IErrorPopup
    {
        string Title { get; set; }
        string Text { get; set; }
        string ButtonText { get; set; }

        void Close();
    }
}