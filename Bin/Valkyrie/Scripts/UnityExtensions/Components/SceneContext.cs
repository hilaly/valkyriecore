using UnityEngine;
using Valkyrie.Di;
using Valkyrie.UnityExtensions.Components;

namespace Valkyrie.Scripts.UnityExtensions.Components
{
    public class SceneContext : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private MonoBehaviourInstaller[] installers;
#pragma warning restore 649
        
        private void Awake()
        {
            var container = ContextManager.GetContainer();

            foreach (var installer in installers)
                installer.Build(container);

            container.Build();
        }
    }

    public abstract class MonoBehaviourInstaller : MonoBehaviour
    {
        public abstract void Build(IContainer container);
    }
}