﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Valkyrie.UnityExtensions.Components
{
    public class HideHandlerComponent : MonoBehaviour
    {
        readonly List<Action> _actions = new List<Action>();

        public void Add(Action action)
        {
            _actions.Add(action);
        }

        private void OnDisable()
        {
            var temp = _actions.ToArray();
            _actions.Clear();
            foreach (var action in temp)
                action?.Invoke();
        }
    }
}