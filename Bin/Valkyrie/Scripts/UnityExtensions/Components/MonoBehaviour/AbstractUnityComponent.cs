﻿using System.Reflection;
#if PHOTON_NETWORK
using MonoBehaviourScript = Photon.PunBehaviour;
#elif UNITY_NETWORK
using MonoBehaviourScript = UnityEngine.Networking.NetworkBehaviour;
#else
using MonoBehaviourScript = UnityEngine.MonoBehaviour;
#endif

namespace Valkyrie.UnityExtensions.Components
{
    public abstract class AbstractUnityComponent : MonoBehaviourScript
    {
#if UNITY_EDITOR
        protected AbstractUnityComponent()
        {
            var type = GetType();
            if (type.GetMethod("Update", BindingFlags.Instance) != null)
            {
                throw new AmbiguousMatchException(
                    string.Format("Can not realize Update methods in derived from BaseMonoScript class '{0}'", type.Name));
            }
        }
#endif

        // ReSharper disable MemberCanBePrivate.Global
        protected bool IsInited { get; private set; }
        protected bool IsEnabled { get; private set; }
        // ReSharper restore MemberCanBePrivate.Global

#region MonoBehaviour events

        // ReSharper disable UnusedMember.Global
        public void Awake()
        {
            SelfInit();
        }

        public void OnEnable()
        {
            EnableIfCan();
        }

        public void Start()
        {
            InitIfCan();
            EnableIfCan();
        }

        public void OnDisable()
        {
            Disable();
            IsEnabled = false;
        }

        public void OnDestroy()
        {
            if (IsInited)
            {
                Shutdown();
                IsInited = false;
            }
        }

        // ReSharper restore UnusedMember.Global

#endregion

        void InitIfCan()
        {
            if (!IsInited)
            {
                Init();
                IsInited = true;
            }
        }

        void EnableIfCan()
        {
            if (!IsInited)
                return;
            if (IsEnabled)
                return;
            Enable();
            IsEnabled = true;
        }

#region Methods for implementation

        protected virtual void SelfInit() { }
        // ReSharper disable once VirtualMemberNeverOverriden.Global
        protected virtual void Init() { }
        protected virtual void Enable() { }
        protected virtual void Disable() { }
        protected virtual void Shutdown() { }

#endregion
    }
}