﻿using System;
using UnityEngine;
using Valkyrie.Threading.Async;

namespace Valkyrie.UnityExtensions.Components
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class DisposableUnityComponent : MonoBehaviour
    {
        readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();

        public void Add(IDisposable disposable)
        {
            _compositeDisposable.Add(disposable);
        }

        // ReSharper disable once UnusedMember.Local
        private void OnDestroy()
        {
            _compositeDisposable.Dispose();
        }
    }
}
