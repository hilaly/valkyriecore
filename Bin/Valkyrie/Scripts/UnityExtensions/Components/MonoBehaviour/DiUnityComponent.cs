using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Di;
using Valkyrie.Threading.Async;

namespace Valkyrie.UnityExtensions.Components
{
    public abstract class DiUnityComponent : AbstractUnityComponent
    {
        #region Overrides of BaseMonoScript

        public static T TryToResolve<T>()
        {
            return ContextManager.GetContainer().TryResolve<T>();
        }
        
        protected IContainer GetActiveContainer()
        {
            return ContextManager.GetContainer();
        }

        protected override void SelfInit()
        {
            base.SelfInit();

            //Resolve properties
            GetActiveContainer().Inject(this);
        }

        #endregion

        protected void RegisterSelf()
        {
            GetActiveContainer().Register(this).AsInterfacesAndSelf();
            GetActiveContainer().Build();
        }
    }

    static class ContextManager
    {
        private static readonly List<IContainer> Containers = new List<IContainer>();
        
        internal static IContainer GetContainer()
        {
            if(!Containers.Any())
                return Core.GetInstance().RootScope;
            return Containers.Last();
        }

        internal static IDisposable SetActiveContainer(IContainer container)
        {
            Containers.Add(container);
            return Disposable.Create(() => Containers.Remove(container));
        }
    }
}