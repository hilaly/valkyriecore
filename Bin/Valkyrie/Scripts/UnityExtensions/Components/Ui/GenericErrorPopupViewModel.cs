using System;
using UnityEngine;
using Valkyrie.Data.Bind;
using Object = UnityEngine.Object;

namespace Valkyrie.UnityExtensions.Components
{
    [Bind]
    public class GenericErrorPopupViewModel : MonoBehaviour, IErrorPopup
    {
        [Bind] public string Title { get; set; }
        [Bind] public string Text { get; set; }
        [Bind] public string ButtonText { get; set; }
        
        public GenericErrorPopupViewModel(string title, string text, string buttonText)
        {
            Title = title;
            Text = text;
            ButtonText = buttonText;
        }

        [Bind] public void Close()
        {
            Destroy(gameObject);
        }
    }
}