using System;
using System.Linq;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Runtime.Console;
using Valkyrie.Threading.Async;
using Valkyrie.UserInput.UnitySpecific;

namespace Valkyrie.UnityExtensions.Components
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class UiRoot : DiUnityComponent
    {
#pragma warning disable 649
        [Inject] private IAssetsFactory _assetsFactory;
        [SerializeField] RectTransform _background;
        [SerializeField] RectTransform _foreground;
        [SerializeField] RectTransform _criticalPopup;
        [SerializeField] LoadingScreen _loadingScreen;
        [SerializeField] private Canvas _rootCanvas;
        [SerializeField] GameObject _errorPopupPrefab;
#pragma warning restore 649

        GameObject _lastBackgroundView;
        GameObject _lastForegroundView;
        GameObject _popup;

        static UiRoot GetSingleton()
        {
            var root = FindObjectOfType<UiRoot>();
            if (root == null)
            {
                var go = Core.GetInstance().RootScope.Resolve<IAssetsFactory>().LoadAndInstantiate(PathToPrefab);
                root = go.GetComponentInChildren<UiRoot>();
                DontDestroyOnLoad(go);
            }
            return root;
        }

        internal static string PathToPrefab = "Ui/UiRoot";

        #region Ui init

        public static void ShowBackground(string prefabPath, IContainer scope)
        {
            ShowBackground((GameObject) GetSingleton()._assetsFactory.LoadAsset(prefabPath), scope);
        }

        public static void ShowBackground(GameObject prefab, IContainer scope)
        {
            var root = GetSingleton();
            if (root._lastBackgroundView != null)
                Destroy(root._lastBackgroundView);
            using (scope.SetActive())
            {
                root._lastBackgroundView = root._assetsFactory.Instantiate(prefab, root._background);
                root._lastBackgroundView.SetActive(true);   
            }
        }

        public static void HideBackground()
        {
            var root = GetSingleton();
            if (root._lastBackgroundView != null)
                Destroy(root._lastBackgroundView);
            root._lastBackgroundView = null;
        }

        public static void ShowForeground(string prefabPath, IContainer scope)
        {
            ShowForeground((GameObject) GetSingleton()._assetsFactory.LoadAsset(prefabPath), scope);
        }

        public static void ShowForeground(GameObject prefab, IContainer scope)
        {
            var root = GetSingleton();
            if (root._lastForegroundView != null)
                Destroy(root._lastForegroundView);
            using (scope.SetActive())
            {
                root._lastForegroundView = root._assetsFactory.Instantiate(prefab, root._foreground);
                root._lastForegroundView.SetActive(true);   
            }
        }

        public static void HideForeground()
        {
            var root = GetSingleton();
            if (root._lastForegroundView != null)
                Destroy(root._lastForegroundView);
            root._lastForegroundView = null;
        }

        public static IPromise<IErrorPopup> ShowCriticalPopup()
        {
            CloseCriticalPopup();
            
            var root = GetSingleton();
            root._popup = (GameObject) root._assetsFactory.Instantiate(root._errorPopupPrefab, root._criticalPopup);
            root._popup.SetActive(true);
            return Promise<IErrorPopup>.Resolved(root._popup.GetComponentInChildren<IErrorPopup>());
        }

        public static void CloseCriticalPopup()
        {
            var root = GetSingleton();
            if (root._popup != null)
                Destroy(root._popup);
            root._popup = null;
        }

        public static ILoadingScreen ShowLoading()
        {
            var root = GetSingleton();
            root._loadingScreen.gameObject.SetActive(true);
            return root._loadingScreen;
        }

        public static Canvas RootCanvas => GetSingleton()._rootCanvas;

        public static IPromise ShowErrorPopup(string title, string text, string button)
        {
            var result = new Promise();
            ShowCriticalPopup().Then(popup =>
            {
                popup.Title = title;
                popup.Text = text;
                popup.ButtonText = button;
            }).Done();
            Disposable.Create(() => { result.Resolve(); }).AttachTo(GetSingleton()._popup);
            return result;
        }

        #endregion
        
        #region Cheats

        private bool _developerConsole;
        private Vector2 _consolePos;
        private string _consoleText;

        private void Update()
        {
            var showDeveloperConsole = Core.GetInstance().RootScope.Resolve<IInput>().GetTouches().Count > 3;
#if UNITY_EDITOR
            showDeveloperConsole = showDeveloperConsole || Input.GetKeyUp(KeyCode.Escape);
#endif
            if (showDeveloperConsole)
            {
                _developerConsole = !_developerConsole;
                if (_developerConsole)
                    ShowLoading();
                else
                    ShowLoading().Dispose();
            }
        }

        private void OnGUI()
        {
            if(!_developerConsole)
                return;
            var console = Core.GetInstance().RootScope.Resolve<IDeveloperConsole>();
            GUILayout.BeginVertical();
            _consolePos = GUILayout.BeginScrollView(_consolePos);
            foreach (var logRecord in console.Messages)
                GUILayout.Label($"{logRecord.Channel}: {logRecord.Message}");
            GUILayout.EndScrollView();
            var nameInput = "DEVELOP_INPUT_CONSOLE_FIELD";
            GUI.SetNextControlName(nameInput);
            _consoleText = GUILayout.TextField(_consoleText);
            if(GUI.GetNameOfFocusedControl() == nameInput
               && Event.current.type == EventType.KeyUp
               && new [] {KeyCode.Return, KeyCode.KeypadEnter}.Any(u => u == Event.current.keyCode))
            {
                if (_consoleText.NotNullOrEmpty())
                {
                    var temp = _consoleText;
                    _consoleText = string.Empty;
                    console.Execute(temp);
                }
            }
            
            GUILayout.EndVertical();
        }

        #endregion
    }
}