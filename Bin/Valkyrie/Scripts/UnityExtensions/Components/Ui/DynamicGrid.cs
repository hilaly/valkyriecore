﻿using UnityEngine;
using UnityEngine.UI;

namespace Valkyrie.UnityExtensions.Components
{
    [RequireComponent(typeof(GridLayoutGroup))]
    // ReSharper disable once UnusedMember.Global
    public class DynamicGrid : MonoBehaviour
    {
        private RectTransform _transform;
        private GridLayoutGroup _grid;

        [SerializeField] private int _columnCount = 2;

        // ReSharper disable once UnusedMember.Local
        void Awake()
        {
            _transform = GetComponent<RectTransform>();
            _grid = GetComponent<GridLayoutGroup>();

            UpdateCellSize();
        }

        void UpdateCellSize()
        {
            var size = _transform.rect.width / _columnCount;
            _grid.cellSize = new Vector2(size, size);
        }

        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            UpdateCellSize();
        }
    }
}
