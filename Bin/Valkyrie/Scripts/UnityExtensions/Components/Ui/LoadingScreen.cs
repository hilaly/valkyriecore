using UnityEngine;
using UnityEngine.UI;
using Valkyrie.Data.Bind;

namespace Valkyrie.UnityExtensions.Components
{
    // ReSharper disable once ClassNeverInstantiated.Global
    [Bind]
    public class LoadingScreen : MonoBehaviour, ILoadingScreen
    {
#pragma warning disable 649
        [SerializeField] Slider _progress;
        [SerializeField] Text _text;
        [SerializeField] private Text _progressText;
#pragma warning restore 649

        private float _progressValue = 0f;
        private string _desc;
        
        public void Dispose()
        {
            Progress = 0f;
            Description = string.Empty;
            gameObject.SetActive(false);
        }

        [Bind] public int ProgressPercent => (int) (_progressValue * 100);

        [Bind]
        public float Progress
        {
            get => _progressValue;
            set
            {
                _progressValue = value;
                
                if(_progress != null)
                    _progress.normalizedValue = value;
                if (_progressText != null)
                    _progressText.text = ProgressText;
            }
        }

        [Bind] public string ProgressText => $"{ProgressPercent} %";

        [Bind]
        public string Description
        {
            get => _desc;
            set
            {
                _desc = value;
                
                if(_text != null)
                    _text.text = value;
            }
        }
    }
}