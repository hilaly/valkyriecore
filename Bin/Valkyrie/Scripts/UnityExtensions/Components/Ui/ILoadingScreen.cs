using System;

namespace Valkyrie.UnityExtensions.Components
{
    public interface ILoadingScreen : IDisposable
    {
        float Progress { set; }
        string Description { set; }
    }
}