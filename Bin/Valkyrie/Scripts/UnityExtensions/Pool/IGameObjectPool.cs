using UnityEngine;

namespace Valkyrie.UnityExtensions.Pool
{
    public interface IGameObjectPool : IAssetsDestroyer
    {
        GameObject Instantiate(GameObject prefab);
        GameObject Instantiate(string prefabPath);

        void Clear();
    }
}