using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Profiling;
using Object = UnityEngine.Object;

namespace Valkyrie.UnityExtensions.Pool
{
    class GameObjectPool : IGameObjectPool, IDisposable
    {
        private readonly IAssetsFactory _assetsFactory;
        private readonly Dictionary<GameObject, List<GameObject>> _readyAssets = new Dictionary<GameObject, List<GameObject>>();

        public GameObjectPool(IAssetsFactory assetsFactory)
        {
            _assetsFactory = assetsFactory;
        }

        #region IGameObjectPool

        public GameObject Instantiate(GameObject prefab)
        {
            Profiler.BeginSample("GameObjectPool.Instantiate(GameObject)");
            var result = InstantiateImpl(prefab);
            result.SetActive(true);
            Profiler.EndSample();
            return result;
        }
        public GameObject Instantiate(string prefabPath)
        {
            Profiler.BeginSample("GameObjectPool.Instantiate(string)");
            var result = Instantiate(GetPrefab(prefabPath));
            result.SetActive(true);
            Profiler.EndSample();
            return result;
        }

        #endregion

        GameObject InstantiateImpl(GameObject prefab)
        {
            List<GameObject> readyAssets;
            if (_readyAssets.TryGetValue(prefab, out readyAssets))
            {
                if (readyAssets.Count > 0)
                {
                    var result = readyAssets[readyAssets.Count - 1];
                    readyAssets.RemoveAt(readyAssets.Count - 1);
                    return result;
                }

                return Create(prefab);
            }

            var instance = Create(prefab);
            _readyAssets.Add(prefab, new List<GameObject>());
            return instance;
        }

        GameObject Create(GameObject prefab)
        {
            var instance = _assetsFactory.Instantiate(prefab);

            var info = instance.AddComponent<PoolObjectInfo>();
            info.Prefab = prefab;
            info.Pool = this;

            return instance;
        }

        private GameObject GetPrefab(string prefabPath)
        {
            return _assetsFactory.LoadAsset<GameObject>(prefabPath);
        }

        public void Destroy(GameObject gameObject)
        {
            var info = gameObject.GetComponent<IPooledGameObject>();
            Debug.Assert(info != null && info.Pool == this);
            
            List<GameObject> readyAssets;
            if (!_readyAssets.TryGetValue(info.Prefab, out readyAssets))
            {
                readyAssets = new List<GameObject>();
                _readyAssets.Add(info.Prefab, readyAssets);
            }

            readyAssets.Add(gameObject);

            gameObject.SetActive(false);
        }

        public void Clear()
        {
            var allObjects = _readyAssets.SelectMany(u => u.Value);
            foreach (var o in allObjects)
                Object.Destroy(o);
            _readyAssets.Clear();
            Resources.UnloadUnusedAssets();
        }

        public void Dispose()
        {
            Clear();
        }
    }
}