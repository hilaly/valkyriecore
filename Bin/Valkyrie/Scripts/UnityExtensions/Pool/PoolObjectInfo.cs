using UnityEngine;

namespace Valkyrie.UnityExtensions.Pool
{
    public sealed class PoolObjectInfo : MonoBehaviour, IPooledGameObject
    {
        public GameObject Prefab { get; set; }
        public IGameObjectPool Pool { get; set; }

        public void Destroy()
        {
            Pool.Destroy(gameObject);
        }

        void OnDestroy()
        {
            Debug.LogWarningFormat("Destroy object from pool");
        }
    }
}