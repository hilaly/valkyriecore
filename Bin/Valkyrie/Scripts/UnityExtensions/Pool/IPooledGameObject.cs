using UnityEngine;

namespace Valkyrie.UnityExtensions.Pool
{
    public interface IPooledGameObject
    {
        GameObject Prefab { get; }
        IGameObjectPool Pool { get; }

        void Destroy();
    }
}