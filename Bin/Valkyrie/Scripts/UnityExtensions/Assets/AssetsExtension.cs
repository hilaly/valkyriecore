using UnityEngine;
using UnityEngine.SceneManagement;
using Valkyrie.Data;
using Valkyrie.Misc;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;
using Valkyrie.UnityExtensions.Components;

namespace Valkyrie.UnityExtensions
{
    public static class AssetsExtension
    {
        public static IPromise LoadScene(this IAssetsFactory assetsFactory, ILoadingScreen loadingProgressHandler, string sceneName, LoadSceneMode mode)
        {
            var result = new Promise();
            loadingProgressHandler.Progress = 0f;
            loadingProgressHandler.Description = "LID_LOADING_SCENE";
            assetsFactory.LoadScene(sceneName, mode)
                .Subscribe(f =>
                {
                    loadingProgressHandler.Progress = f;
                    result.ReportProgress(f);
                }, () => result.Resolve());
            return result;
        }

        public static IPromise LoadEmptyScene(this IAssetsFactory assetsFactory, ILoadingScreen loadingProgressHandler)
        {
            var result = new Promise();
            loadingProgressHandler.Progress = 0f;
            loadingProgressHandler.Description = "LID_LOADING_SCENE";
            assetsFactory.LoadEmptyScene()
                .Subscribe(f =>
                {
                    loadingProgressHandler.Progress = f;
                    result.ReportProgress(f);
                }, () => result.Resolve());
            return result;
        }

        public static GameObject LoadAndInstantiate(this IAssetsFactory assetsFactory, string assetPath)
        {
            return assetsFactory.Instantiate(assetsFactory.LoadAsset<GameObject>(assetPath));
        }

        public static GameObject LoadAndInstantiate(this IAssetsFactory assetsFactory, string assetPath, Transform parentTransform)
        {
            return assetsFactory.Instantiate(assetsFactory.LoadAsset<GameObject>(assetPath), parentTransform);
        }

        public static GameObject CreateFromVisualConfig(this IAssetsFactory assetsFactory,
            DataExtensions.VisualAssetConfigData configData, Transform parentTransform = null)
        {
            var result = assetsFactory.LoadAndInstantiate(configData.RootPrefab, parentTransform);
            foreach (var viewResource in configData.ChildPrefabs)
                if (viewResource.NotNullOrEmpty())
                    assetsFactory.LoadAndInstantiate(viewResource, result.transform).ResetTransform();
            return result;
        }

        public static void PlayParticleEffectAtPoint(this GameObject hitPrefab, Vector3 pos, Quaternion rot)
        {
            if (hitPrefab != null)
            {
                var hitVfx = Object.Instantiate(hitPrefab, pos, rot);
                var psHit = hitVfx.GetComponent<ParticleSystem>();
                if (psHit != null)
                    Object.Destroy(hitVfx, psHit.main.duration);
                else
                {
                    var psChild = hitVfx.transform.GetChild(0).GetComponent<ParticleSystem>();
                    if(psChild != null)
                        Object.Destroy(hitVfx, psChild.main.duration);
                    else
                        Object.Destroy(hitVfx);
                }
            }
        }
    }
}