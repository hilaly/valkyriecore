using System;
using UnityEngine.SceneManagement;
using Valkyrie.Threading.Async;
using Object = UnityEngine.Object;

namespace Valkyrie.UnityExtensions
{
    public interface IAssetsLoader
    {
        bool IsExist(string path);
        T LoadAsset<T>(string path) where T : Object;
        T[] LoadAllAssets<T>(string path) where T : Object;
        Object LoadAsset(string path);
        IObservable<float> LoadEmptyScene();
        IObservable<float> LoadScene(string sceneName, LoadSceneMode mode);
    }
}