using UnityEngine;

namespace Valkyrie.UnityExtensions
{
    public interface IAssetsDestroyer
    {
        void Destroy(GameObject go);
    }
}