﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valkyrie.Misc;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;
using Object = UnityEngine.Object;

namespace Valkyrie.UnityExtensions
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class AssetsFactory : IAssetsFactory
    {
        private readonly ILogInstance _log;

        public AssetsFactory(ILogService logSystem)
        {
            _log = logSystem.GetLog("UnityExtension.AssetsFactory");
        }

        #region Implementation of IAssetsLoader

        public bool IsExist(string path)
        {
            return Resources.Load(path) != null;
        }

        public T LoadAsset<T>(string path) where T : Object
        {
            var result = Resources.Load<T>(path);
            if (result == null)
            {
                _log.Error($"Couldn't load resource '{path}'");
            }
            return result;
        }

        public T[] LoadAllAssets<T>(string path) where T : Object
        {
            var result = Resources.LoadAll<T>(path);
            if (result == null)
            {
                _log.Error($"Couldn't load resource '{path}'");
            }
            return result;
        }

        public Object LoadAsset(string path)
        {
            var result = Resources.Load(path);
            if (result == null)
            {
                _log.Error($"Couldn't load resource '{path}'");
            }
            return result;
        }

        public IObservable<float> LoadEmptyScene()
        {
            const string emptySceneName = "Empty";
            return LoadScene(emptySceneName, LoadSceneMode.Single);
        }

        public IObservable<float> LoadScene(string sceneName, LoadSceneMode mode)
        {
            return SceneManager.LoadSceneAsync(sceneName, mode).ToObservable();
        }

        #endregion

        #region Implementation of IAssetsFactory

        public GameObject Instantiate(GameObject prefab)
        {
            return Object.Instantiate(prefab);
        }

        public GameObject Instantiate(GameObject prefab, Transform parent)
        {
            var result = Object.Instantiate(prefab, parent);
            return result;
        }

        #endregion

        #region Implementation of IAssetsDestroyer

        public void Destroy(GameObject go)
        {
            Object.Destroy(go);
        }

        #endregion
    }
}
