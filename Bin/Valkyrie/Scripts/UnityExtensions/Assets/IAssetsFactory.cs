using UnityEngine;

namespace Valkyrie.UnityExtensions
{
    public interface IAssetsFactory : IAssetsLoader, IAssetsDestroyer
    {
        GameObject Instantiate(GameObject prefab);
        GameObject Instantiate(GameObject prefab, Transform transform);
    }
}