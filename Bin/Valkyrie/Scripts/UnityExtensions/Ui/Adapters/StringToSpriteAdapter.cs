using System;
using System.Linq;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Data.Bind;
using Valkyrie.Di;
using Valkyrie.Math;
using Valkyrie.UnityExtensions;
using Vector2 = UnityEngine.Vector2;

namespace Valkyrie.Ui.Adapters
{
    public class ToFloatAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(int)
                   || type == typeof(long)
                   || type == typeof(short)
                   || type == typeof(byte);
        }

        public Type GetResultType()
        {
            return typeof(float);
        }

        public object Convert(object source)
        {
            if (source is int i)
                return (float) i;
            if (source is byte b)
                return (float) b;
            if (source is short s)
                return (float) s;
            if (source is long l)
                return (float) l;
            throw new InvalidCastException();
        }
    }
    
    public class BoolInversionAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(bool);
        }

        public Type GetResultType()
        {
            return typeof(bool);
        }

        public object Convert(object source)
        {
            var temp = (bool) source;
            return !temp;
        }
    }
    
    public class StringToSpriteAdapter : IBindingAdapter
    {
        private static Sprite _transparent;

        private static Sprite Transparent
        {
            get
            {
                if (_transparent == null)
                {
                    // Create a new 2x2 texture ARGB32 (32 bit with alpha) and no mipmaps
                    var texture = new Texture2D(2, 2, TextureFormat.ARGB32, false);
 
                    // set the pixel values
                    texture.SetPixel(0, 0, Color.clear);
                    texture.SetPixel(1, 0, Color.clear);
                    texture.SetPixel(0, 1, Color.clear);
                    texture.SetPixel(1, 1, Color.clear);
 
                    // Apply all SetPixel calls
                    texture.Apply();

                    _transparent = Sprite.Create(texture,
                        new Rect(Vector2.zero, new Vector2(texture.width, texture.height)), Vector2.one * 0.5f);
                }

                return _transparent;
            }
        }

        private string _lastValue;
#pragma warning disable 649
        [Inject] private IAssetsFactory _assetsLoader;
#pragma warning restore 649

        private string ImagePath
        {
            set
            {
                if(_lastValue == value)
                    return;
                _lastValue = value;
                if(_assetsLoader == null)
                    return;

                string imagePath = null;
                int imageIndex = -1;
                string spriteName = null;

                var datas = value?.Split(new[] {":"}, StringSplitOptions.RemoveEmptyEntries) ?? new string[0];
                switch (datas.Length)
                {
                    case 1:
                        imagePath = datas[0];
                        imageIndex = -1;
                        spriteName = null;
                        break;
                    case 2:
                        imagePath = datas[0];
                        if (!int.TryParse(datas[1], out imageIndex))
                        {
                            spriteName = datas[1];
                            imageIndex = -1;
                        }
                        break;
                }

                if (imagePath.IsNullOrEmpty())
                    _sprite = Transparent;
                else
                {
                    if (imageIndex >= 0 || spriteName.NotNullOrEmpty())
                    {
                        var allSprites = _assetsLoader.LoadAllAssets<Sprite>(imagePath);
                        _sprite = imageIndex >= 0 ? allSprites[imageIndex] : allSprites.FirstOrDefault(u => u.name == spriteName);
                    }
                    else
                        _sprite = _assetsLoader.LoadAsset<Sprite>(imagePath);
                }

                if (_sprite == null)
                    _sprite = Transparent;
            }
        }
        
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(string);
        }

        public Type GetResultType()
        {
            return typeof(Sprite);
        }

        public object Convert(object source)
        {
            ImagePath = (string) source;
            return _sprite;
        }

        Sprite _sprite;
    }
    
    public class IntToBigNumberStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(int);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            return ((int) source).ToBigNumberString();
        }
    }
    
    public class LongToBigNumberStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(long);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            return ((long) source).ToBigNumberString();
        }
    }

    public class TimeSpanToStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(TimeSpan);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            var timeSpan = (TimeSpan) source;
            
            var d = Mathf.FloorToInt((float) timeSpan.TotalDays);
            if (d > 0)
                return $"{d}d : {timeSpan.Hours:D2}h";
            
            var h = Mathf.FloorToInt((float) timeSpan.TotalHours);
            if (h > 0)
                return $"{h:D2}h : {timeSpan.Minutes:D2}m";
            return $"{Mathf.FloorToInt((float) timeSpan.TotalMinutes):D2}m : {timeSpan.Seconds:D2}s";
        }
    }
    
    public class TimeSpanToShortStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(TimeSpan);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            var timeSpan = (TimeSpan) source;
            
            var d = Mathf.FloorToInt((float) timeSpan.TotalDays);
            if (d > 0)
                return $"{d}:{timeSpan.Hours:D2}";
            
            var h = Mathf.FloorToInt((float) timeSpan.TotalHours);
            if (h > 0)
                return $"{h:D2}:{timeSpan.Minutes:D2}";
            return $"{Mathf.FloorToInt((float) timeSpan.TotalMinutes):D2}:{timeSpan.Seconds:D2}";
        }
    }

    public class TimeSpanCountdownAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return type == typeof(TimeSpan);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            var timeSpan = (TimeSpan) source;

            return timeSpan.TotalSeconds.ToString("F1");
        }
    }

    public class StringToBoolAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return typeof(string).IsAssignableFrom(type);
        }

        public Type GetResultType()
        {
            return typeof(bool);
        }

        public object Convert(object source) => ((string) source).NotNullOrEmpty();
    }

    public class InvertVector2Adapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return typeof(UnityEngine.Vector2).IsAssignableFrom(type);
        }

        public Type GetResultType()
        {
            return typeof(UnityEngine.Vector2);
        }

        public object Convert(object source)
        {
            return ((Vector2) source) * -1f;
        }
    }

    public class IntToStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return typeof(int).IsAssignableFrom(type);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            return ((int) source).ToString();
        }
    }

    public class LongToStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return typeof(long).IsAssignableFrom(type);
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            return ((long) source).ToString();
        }
    }
}