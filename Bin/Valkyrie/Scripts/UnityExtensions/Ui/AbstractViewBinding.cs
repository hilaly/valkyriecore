using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Data.Bind;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Ui
{
    public abstract class AbstractViewBinding : AbstractBindingComponent
    {
#pragma warning disable 649
        [Inject] private IDispatcher _dispatcher;
        [SerializeField] private bool _isTwoSided;
        
        [SerializeField] private string _viewProperty;
        [SerializeField] string _viewModelProperty;
        
        [SerializeField] string _changeEventName;
        [SerializeField] private bool _isPolling = true;
        
        [SerializeField] private string _sourceAdapterType = "None";
#pragma warning restore 649

        #if UNITY_EDITOR
        public bool IsTwoSided => _isTwoSided;
        #endif
        
        protected override void Init()
        {
            base.Init();

            var binding = this.BindViewModelProperty(_viewModelProperty, _changeEventName, _sourceAdapterType, out var disposeHandler);
            
            Bind(binding);

            if (_isTwoSided)
                binding.SetTwoSided(_dispatcher).AttachTo(disposeHandler);
            if (_isPolling)
                _dispatcher.EveryEndIteration(() =>
                {
                    if(this != null && gameObject != null && gameObject.activeInHierarchy)
                        binding.Update();
                }).AttachTo(disposeHandler);
        }

        protected virtual void Bind(Binding binding)
        {
            SplitTypeProperty(_viewProperty, out var typeName, out var propertyName);
            var component = gameObject.GetComponent(typeName);
            component.SetBinding(propertyName, binding);
        }
    }
}