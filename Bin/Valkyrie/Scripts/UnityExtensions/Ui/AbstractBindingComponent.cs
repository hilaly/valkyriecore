using System;
using System.Collections.Generic;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Data.Bind;
using Valkyrie.Di;
using Valkyrie.UnityExtensions.Components;

namespace Valkyrie.Ui
{
    public abstract class AbstractBindingComponent : DiUnityComponent
    {
#pragma warning disable 649
        [Inject] private IContainer _container;
#pragma warning restore 649
        
        internal static object GetModel(GameObject go, string modelTypeName, out GameObject disposeHandler)
        {
            foreach (var component in go.GetComponentsInParent<Component>(true))
            {
                if (component is Template template && template.GetTemplateType().Name == modelTypeName)
                {
                    disposeHandler = component.gameObject;
                    return template.ViewModel;
                }

                if (component.GetType().Name == modelTypeName)
                {
                    disposeHandler = component.gameObject;
                    return component;
                }
            }

            throw new Exception($"Can not find model {modelTypeName}");
        }

        internal static void SplitTypeProperty(string fullText, out string typeName, out string propertyName)
        {
            var colonIndex = fullText.IndexOf(':');
            var text = colonIndex >= 0 ? fullText.Substring(0, colonIndex) : fullText;
            var fullNameParts = text.Split(new []{'/', '.'});
            typeName = fullNameParts[fullNameParts.Length - 2];
            propertyName = fullNameParts[fullNameParts.Length - 1];
        }

        private static List<Type> _adapters;

        protected IBindingAdapter GetAdapter(string adapterType)
        {
            if (adapterType.IsNullOrEmpty() || adapterType == "None")
                return null;
            if (_adapters == null)
                _adapters = typeof(IBindingAdapter).GetAllSubTypes(u => !u.IsAbstract);
            return _container.Resolve<IBindingAdapter>(_adapters.Find(u => u.FullName == adapterType).FullName);
        }

        protected Binding BindViewModelProperty(string viewModelProperty, string viewModelChangeEventName, string adapterType, out GameObject disposeHandler)
        {
            SplitTypeProperty(viewModelProperty, out var typeName, out var propertyName);
            var viewModel = GetModel(gameObject, typeName, out disposeHandler);

            var binding = new Binding
            {
                Source = viewModel,
                Path = propertyName,
                UpdatedEventName = viewModelChangeEventName,
                AllowPrivateProperties = true,
                SourceConverter = GetAdapter(adapterType)
            };

            return binding;
        }
    }
}