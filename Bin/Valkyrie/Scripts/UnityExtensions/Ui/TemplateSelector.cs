using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.Threading.Scheduler;
using Valkyrie.UnityExtensions;

namespace Valkyrie.Ui
{
    public abstract class TemplateSelector : AbstractBindingComponent
    {
#pragma warning disable 649
        [Inject] protected IDispatcher _dispatcher;
        [Inject] private IAssetsFactory _assetsFactory;
        
        [SerializeField] protected string _viewModelProperty;
        [SerializeField] private GameObject _templateSelector;

        [SerializeField] protected bool _isPolling = true;
#pragma warning restore 649
        
        readonly List<GameObject> _templates = new List<GameObject>();

        protected override void SelfInit()
        {
            bool IsTemplate(GameObject o)
            {
                return o.GetComponents<Template>().Length > 0;
            }
            
            if (IsTemplate(_templateSelector))
                _templates.Add(_templateSelector);
            
            for(var i = 0; i < _templateSelector.transform.childCount; ++i)
            {
                var child = _templateSelector.transform.GetChild(i).gameObject;
                if (IsTemplate(child))
                    _templates.Add(child);
            }
            
            base.SelfInit();
        }

        public abstract bool IsValidViewModelProperty(PropertyInfo info);

        static bool FindComponent(GameObject o, Type viewModelType)
        {
            var us = o.GetComponents<Template>();
            for (var index = 0; index < us.Length; index++)
            {
                var u = us[index];
                if (u.GetTemplateType() == viewModelType)
                    return true;
            }

            return false;
        }

        GameObject SelectTemplate(Type viewModelType)
        {
            foreach (var template in _templates)
            {
                if (FindComponent(template, viewModelType))
                    return template;
            }
            
            if (FindComponent(_templateSelector, viewModelType))
                return _templateSelector;
            
            for(var i = 0; i < _templateSelector.transform.childCount; ++i)
            {
                var child = _templateSelector.transform.GetChild(i).gameObject;
                if (FindComponent(child, viewModelType))
                    return child;
            }

            throw new Exception($"Can not find template for {viewModelType.Name}");
        }

        protected GameObject SpawnTemplate(object viewModel)
        {
            var template = SelectTemplate(viewModel.GetType());
            var instance = _assetsFactory.Instantiate(template, transform);
            instance.GetComponent<Template>().ViewModel = viewModel;
            instance.SetActive(true);
            return instance;
        }

        protected GameObject RemoveTemplate(GameObject o)
        {
            Debug.Assert(o.transform.parent == transform);
            Destroy(o);
            return null;
        }

        protected override void Init()
        {
            base.Init();

            var binding = BindViewModelProperty(_viewModelProperty, null, string.Empty, out var disposeHandler);
            
            this.SetBinding("ViewModelProperty", binding);

            if (_isPolling)
                _dispatcher.EveryEndIteration(() =>
                {
                    if(this != null && gameObject != null && gameObject.activeInHierarchy)
                        binding.Update();
                }).AttachTo(disposeHandler);
        }

        public abstract object ViewModelProperty { set; }
    }
}