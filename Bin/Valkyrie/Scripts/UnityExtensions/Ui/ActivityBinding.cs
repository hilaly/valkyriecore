using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Di;
using Valkyrie.Misc;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Ui
{
    public class ActivityBinding : AbstractBindingComponent
    {
#pragma warning disable 649
        [Inject] private IDispatcher _dispatcher;
        
        [SerializeField] string _viewModelProperty;
        
        [SerializeField] private bool _isPolling = true;
        
        [SerializeField] private string _sourceAdapterType = "None";
#pragma warning restore 649

        protected override void Init()
        {
            base.Init();

            var binding = BindViewModelProperty(_viewModelProperty, null, _sourceAdapterType, out var disposeHandler);
            
            this.SetBinding("GameObjectActive", binding);
            if (_isPolling)
                _dispatcher.EveryEndIteration(() =>
                {
                    if(this != null && gameObject != null)
                        binding.Update();
                }).AttachTo(disposeHandler);
        }

        bool GameObjectActive
        {
            set => gameObject.SetActive(value);
        }
    }
}