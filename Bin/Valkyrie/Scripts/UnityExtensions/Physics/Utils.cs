﻿using System.Collections.Generic;
using UnityEngine;
using Valkyrie.Misc;

namespace Valkyrie.UnityExtensions.Physics
{
    class RaycastResult : IRaycastResult
    {
        public RaycastHit Hit { get; set; }

        public Collider Collider => Hit.collider;

        public float Distance => Hit.distance;

        public Vector3 Normal => Hit.normal;

        public Vector3 Point => Hit.point;
    }

    public static class Utils
    {
        public const int MaxRaycasts = 10;
        public const float RaycastMaxDistance = 100f;

        private static readonly RaycastHit[] HitResults = new RaycastHit[MaxRaycasts];
        private static readonly RaycastResult[] Results = new RaycastResult[MaxRaycasts];

        private static readonly RaycastHit DefaultRaycast = default(RaycastHit);

        static Utils()
        {
            for (var i = 0; i < MaxRaycasts; ++i)
                Results[i] = new RaycastResult {Hit = DefaultRaycast};
        }

        #region Casts

        public static IRaycastResult Raycast(Ray ray, int mask, float raycastDistance = RaycastMaxDistance)
        {
            var count = RaycastAll(ray, mask, raycastDistance, Results);
            return count > 0 ? new RaycastResult {Hit = HitResults[0]} : default(IRaycastResult);
        }
        /// <summary>
        /// Make raycasts, ordered by distance
        /// </summary>
        /// <param name="ray"></param>
        /// <param name="mask"></param>
        /// <param name="raycastDistance"></param>
        /// <param name="raycasts"></param>
        /// <returns></returns>
        public static int RaycastAll(Ray ray, int mask, float raycastDistance, IRaycastResult[] resultCasts)
        {
            var count = UnityEngine.Physics.RaycastNonAlloc(ray, HitResults, raycastDistance, mask);
            for (var i = 0; i < count - 1; ++i)
            {
                for (var j = i + 1; j < count; ++j)
                {
                    if(HitResults[i].distance < HitResults[j].distance)
                        continue;
                    var temp = HitResults[i];
                    HitResults[i] = HitResults[j];
                    HitResults[j] = temp;
                }
            }
            var smallCount = Mathf.Min(HitResults.Length, resultCasts.Length, count);
            for (var i = 0; i < smallCount; ++i)
                resultCasts[i] = new RaycastResult {Hit = HitResults[i]};
            for (var i = smallCount; i < resultCasts.Length; ++i)
                resultCasts[i] = null;
            return count;
        }

        #endregion

        public static IRaycastResult Raycast(this IRaycaster raycaster, Touch touch, float maxDistance)
        {
            return raycaster.Raycast(touch.ToWorldRay(), maxDistance);
        }

        public static IRaycastResult Raycast(this IRaycaster raycaster, Vector2 screenPoint, float maxDistance)
        {
            return raycaster.Raycast(screenPoint.ScreenPointToWorldRay(), maxDistance);
        }

        public static IEnumerable<IRaycastResult> RaycastAll(this IRaycaster raycaster, Touch touch, float maxDistance)
        {
            return raycaster.RaycastAll(touch.ToWorldRay(), maxDistance);
        }
    }
}
