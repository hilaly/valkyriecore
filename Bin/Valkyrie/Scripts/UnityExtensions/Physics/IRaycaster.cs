﻿using System.Collections.Generic;
using UnityEngine;

namespace Valkyrie.UnityExtensions.Physics
{
    public interface IRaycaster
    {
        // ReSharper disable once UnusedMemberInSuper.Global
        int RaycastMask { get; set; }
        void RaycastLayers(params string[] layersNames);

        IRaycastResult Raycast(Ray raycastRay, float maxDistance);
        IEnumerable<IRaycastResult> RaycastAll(Ray raycastRay, float maxDistance);
    }
}
