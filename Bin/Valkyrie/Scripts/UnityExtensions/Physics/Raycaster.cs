using System.Collections.Generic;
using UnityEngine;

namespace Valkyrie.UnityExtensions.Physics
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class Raycaster : IRaycaster
    {
        private readonly RaycastHit[] _arrayForNonAlloc = new RaycastHit[Utils.MaxRaycasts];

        readonly RaycastResult _tempResult = new RaycastResult();
        
        public Raycaster()
        {
            RaycastMask = UnityEngine.Physics.DefaultRaycastLayers;
        }

        #region Implementation of IRaycaster

        public int RaycastMask { get; set; }
        public void RaycastLayers(params string[] layersNames)
        {
            RaycastMask = LayerMask.GetMask(layersNames);
        }

        public IRaycastResult Raycast(Ray raycastRay, float maxDistance)
        {
            var count = UnityEngine.Physics.RaycastNonAlloc(raycastRay, _arrayForNonAlloc, maxDistance, RaycastMask);
            if (count == 0)
                return null;
            
            var min = _arrayForNonAlloc[0].distance;
            var index = 0;
            for (var i = 1; i < count; ++i)
            {
                if (_arrayForNonAlloc[i].distance >= min)
                    continue;
                index = i;
                min = _arrayForNonAlloc[i].distance;
            }
            
            _tempResult.Hit = _arrayForNonAlloc[index];
            return _tempResult;
        }

        public IEnumerable<IRaycastResult> RaycastAll(Ray raycastRay, float maxDistance)
        {
            var count = UnityEngine.Physics.RaycastNonAlloc(raycastRay, _arrayForNonAlloc, maxDistance, RaycastMask);
            for (var i = 0; i < count - 1; ++i)
            {
                for (var j = i + 1; j < count; ++j)
                {
                    if(_arrayForNonAlloc[i].distance < _arrayForNonAlloc[j].distance)
                        continue;
                    var temp = _arrayForNonAlloc[i];
                    _arrayForNonAlloc[i] = _arrayForNonAlloc[j];
                    _arrayForNonAlloc[j] = temp;
                }
            }

            for (var i = 0; i < count; ++i)
            {
                _tempResult.Hit = _arrayForNonAlloc[i];
                yield return _tempResult;
            }
        }

        #endregion
    }
}