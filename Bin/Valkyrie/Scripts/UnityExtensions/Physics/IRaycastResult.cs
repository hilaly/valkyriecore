﻿using UnityEngine;

namespace Valkyrie.UnityExtensions.Physics
{
    public interface IRaycastResult
    {
        Collider Collider { get; }
        float Distance { get; }
        Vector3 Normal { get; }
        Vector3 Point { get; }
    }
}