﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Valkyrie.Misc;
using Valkyrie.Procedural.Triangulation;

namespace Valkyrie.Procedural
{
    public static class SdfVisualization
    {
        private const int MaxVerticesCount = 64950; //must be divisible by 3, ie 3 verts == 1 triangle
        
        public static List<Mesh> Triangulate(this ITriangulator triangulator)
        {
            var meshes = new List<Mesh>();

            triangulator.Generate();

            var verts = triangulator.Vertices.Select(u => u.ToUnity()).ToList();
            var indices = triangulator.Indices.ToList();

            //A mesh in unity can only be made up of 65000 verts.
            //Need to split the verts between multiple meshes.

            int numMeshes = verts.Count / MaxVerticesCount + 1;

            for (int i = 0; i < numMeshes; i++)
            {
                List<Vector3> splitVerts = new List<Vector3>();
                List<int> splitIndices = new List<int>();

                for (int j = 0; j < MaxVerticesCount; j++)
                {
                    int idx = i * MaxVerticesCount + j;

                    if (idx < verts.Count)
                    {
                        splitVerts.Add(verts[idx]);
                        splitIndices.Add(j);
                    }
                }

                if (splitVerts.Count == 0) continue;

                Mesh mesh = new Mesh();
                mesh.SetVertices(splitVerts);
                mesh.SetTriangles(splitIndices, 0);
                mesh.RecalculateBounds();
                mesh.RecalculateNormals();

                meshes.Add(mesh);
            }

            return meshes;
        }
        
        class SdfVisualizationBehaviour : MonoBehaviour
        {
            Material _material;
            readonly List<GameObject> _meshes = new List<GameObject>();
            private ITriangulator _triangulator;

            private void OnDestroy()
            {
                foreach (var mesh in _meshes)
                    Destroy(mesh);
                _meshes.Clear();
            }

            public void Generate(ITriangulator triangulator, Material material)
            {
                _triangulator = triangulator;
                _material = material;
                
                foreach (var mesh in _meshes)
                    Destroy(mesh);
                _meshes.Clear();

                foreach (var mesh in _triangulator.Triangulate())
                {
                    GameObject go = new GameObject("Mesh");
                    go.transform.parent = transform;
                    go.AddComponent<MeshFilter>();
                    go.AddComponent<MeshRenderer>();
                    go.GetComponent<Renderer>().material = _material;
                    go.GetComponent<MeshFilter>().mesh = mesh;
                    //go.transform.localPosition = new Vector3(-triangulator.Width * 0.5f, -triangulator.Height * 0.5f, -triangulator.Length * 0.5f);
                    go.transform.Reset();

                    _meshes.Add(go);
                }
            }
        }
        
        public static GameObject Fill(ITriangulator triangulator, GameObject go, Material material)
        {
            var c = go.GetComponent<SdfVisualizationBehaviour>();
            if (c == null)
                c = go.AddComponent<SdfVisualizationBehaviour>();
            c.Generate(triangulator, material);
            return go;
        }

        public static GameObject Clear(GameObject go)
        {
            var c = go.GetComponent<SdfVisualizationBehaviour>();
            if (c != null)
                Object.Destroy(c);
            return go;
        }
    }
}