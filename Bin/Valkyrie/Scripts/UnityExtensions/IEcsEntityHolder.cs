using Valkyrie.Ecs.Entities;

namespace Valkyrie.UnityExtensions.Components
{
    public interface IEcsEntityHolder
    {
        IEcsEntity EcsEntity { get; }
    }
}