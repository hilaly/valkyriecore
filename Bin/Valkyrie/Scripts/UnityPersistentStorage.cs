using System;
using System.Collections.Generic;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Misc
{
    class UnityPersistentStorage : IPersistentStorage, IDisposable
    {
        private readonly Dictionary<string, object> _loadedData = new Dictionary<string, object>();
        private readonly IDisposable _disposable;

        private float _lastSaveTime;

        public UnityPersistentStorage(IDispatcher dispatcher)
        {
            _disposable = dispatcher.EveryPostUpdate(SaveAll);
        }

        private void SaveAll()
        {
            if(Time.realtimeSinceStartup - _lastSaveTime < 5f)
                return;
            _lastSaveTime = Time.realtimeSinceStartup;
            foreach (var o in _loadedData)
                PlayerPrefs.SetString(o.Key, o.Value.ToJson());
        }

        public T Get<T>(string key)
        {
            return (T) Get(key, typeof(T));
        }

        public object Get(string key, Type type)
        {
            if (!_loadedData.ContainsKey(key))
                if (PlayerPrefs.HasKey(key))
                    _loadedData.Add(key, PlayerPrefs.GetString(key).ToObject(type));
                else
                {
                    _loadedData.Add(key, Activator.CreateInstance(type));
                    PlayerPrefs.SetString(key, _loadedData[key].ToJson());
                }

            return _loadedData[key];
        }

        public void Set(string key, object value)
        {
            _loadedData[key] = value;
            PlayerPrefs.SetString(key, value.ToJson());
        }

        public bool Has(string key)
        {
            return _loadedData.ContainsKey(key) || PlayerPrefs.HasKey(key);
        }

        public void Dispose()
        {
            _disposable?.Dispose();
        }
    }
}