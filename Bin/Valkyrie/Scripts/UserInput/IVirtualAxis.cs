﻿namespace Valkyrie.UserInput
{
    public interface IVirtualAxis
    {
        /// <summary>
        /// -1;1 range
        /// </summary>
        float Value { get; }
    }
}