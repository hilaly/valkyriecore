﻿namespace Valkyrie.UserInput
{
    public interface IVirtualButton
    {
        bool IsPressed();
    }
}