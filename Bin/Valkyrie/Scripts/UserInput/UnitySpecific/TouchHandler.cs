using UnityEngine;
using UnityEngine.EventSystems;
using Valkyrie.UnityExtensions.Components;
using Vector2 = System.Numerics.Vector2;

namespace Valkyrie.UserInput.UnitySpecific
{
    [RequireComponent(typeof(NullGraphics))]
    public class TouchHandler : MonoBehaviour
        , IDragHandler, IBeginDragHandler, IEndDragHandler
        , IVirtualJoystick
    {
        private bool _dragged;

        public enum Mode
        {
            Normalized,
            Balanced
        }

#pragma warning disable 649
        [SerializeField] private Mode _mode;
#pragma warning restore 649
        
        public void OnDrag(PointerEventData eventData)
        {
            switch (_mode)
            {
                case Mode.Normalized:
                    Value = new Vector2(eventData.delta.x / Screen.width, eventData.delta.y / Screen.height);
                    break;
                case Mode.Balanced:
                {
                    var temp = Mathf.Min(Screen.width, Screen.height);
                    Value = new Vector2(eventData.delta.x / temp * ((float)Screen.width / Screen.height), eventData.delta.y / temp);
                    break;
                }
            }

            _dragged = true;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            switch (_mode)
            {
                case Mode.Normalized:
                    Value = new Vector2(eventData.delta.x / Screen.width, eventData.delta.y / Screen.height);
                    break;
                case Mode.Balanced:
                {
                    var temp = Mathf.Min(Screen.width, Screen.height);
                    Value = new Vector2(eventData.delta.x / temp * ((float)Screen.width / Screen.height), eventData.delta.y / temp);
                    break;
                }
            }

            _dragged = true;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            Value = Vector2.Zero;
        }
        

        public void Update()
        {
            if (!_dragged)
                Value = Vector2.Zero;
            _dragged = false;
        }

        public Vector2 Value { get; private set; }
    }
}