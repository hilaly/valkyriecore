using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valkyrie.Data.Bind;
using Valkyrie.Misc;
using Valkyrie.UnityExtensions.Components;

namespace Valkyrie.UserInput.UnitySpecific
{
    [Bind] public class Joystick2Axis : DiUnityComponent, IMoveJoystick
        , IDragHandler, IBeginDragHandler, IEndDragHandler
        , IPointerDownHandler, IPointerUpHandler
    {
#pragma warning disable 0649
        // ReSharper disable InconsistentNaming
        [SerializeField] private RectTransform _viewTransform;
        [SerializeField] private RectTransform _moveLeft;
        [SerializeField] private RectTransform _moveRight;
        [SerializeField] private RectTransform _moveUp;
        [SerializeField] private RectTransform _moveDown;
        [SerializeField] private RectTransform _stick;
        [SerializeField] private AnimationCurve _sensitivity;
        [SerializeField] private bool _isRound;
        [Range(0, 0.5f)] [SerializeField] private float _axisHardDeadZone = 0.25f;
        // ReSharper restore InconsistentNaming
#pragma warning restore 0649
        
        private Vector2 _defaultPosition;
        private bool _cruiseControl;

        public System.Numerics.Vector2 Value { get; private set; } = System.Numerics.Vector2.Zero;

        [Bind] public bool IsDynamic { get; set; }
        [Bind] public bool IsPressed { get; private set; }

        public bool CruiseControl
        {
            get => _cruiseControl;
            set
            {
                if(_cruiseControl == value)
                    return;
                _cruiseControl = value;
                if(!_cruiseControl)
                    Reset();
            }
        }

        #region DiComponent
        
        protected override void SelfInit()
        {
            base.SelfInit();
            _defaultPosition = _viewTransform.anchoredPosition;
        }

        protected override void Disable()
        {
            Reset();
            base.Disable();
        }
        
        #endregion

        private void Reset()
        {
            IsPressed = false;

            Value = System.Numerics.Vector2.Zero;
            _stick.anchoredPosition = Vector2.zero;
        }

        #region UnityHandlers
        
        public void OnPointerDown(PointerEventData eventData)
        {
            IsPressed = true;
            
            if(IsDynamic)
                UpdateDynamicView(eventData.position);
            else
                OnDrag(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            IsPressed = false;

            if (IsDynamic)
                ResetDynamicView();
			
            if (!_cruiseControl)
                Reset();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            IsPressed = true;

            if(!IsDynamic)
                return;
			
            OnDrag(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            IsPressed = false;

            if(!IsDynamic)
                return;

            ResetDynamicView();
			
            if (!_cruiseControl)
                Reset();
        }

        public void OnDrag(PointerEventData eventData)
        {
            IsPressed = true;

            var downPosition = _moveDown.position;
            var leftPosition = _moveLeft.position;
            var yAxisDelta = _moveUp.position - downPosition;
            var xAxisDelta = _moveRight.position - leftPosition;
            var pointerVecY = eventData.position - new Vector2(downPosition.x, downPosition.y);
            var pointerVecX = eventData.position - new Vector2(leftPosition.x, leftPosition.y);

            var yAxis = Mathf.Clamp(
                (Vector2.Dot(yAxisDelta, pointerVecY) / yAxisDelta.magnitude - yAxisDelta.magnitude * 0.5f) 
                / (yAxisDelta.magnitude * 0.5f), -1.0f, 1.0f);
			
            var xAxis = Mathf.Clamp(
                (Vector2.Dot(xAxisDelta, pointerVecX) / xAxisDelta.magnitude - xAxisDelta.magnitude * 0.5f) 
                / (xAxisDelta.magnitude * 0.5f), -1.0f, 1.0f);

            var vectorResult = new Vector2(xAxis, yAxis);
            if (vectorResult.sqrMagnitude < _axisHardDeadZone * _axisHardDeadZone)
            {
                vectorResult.x = ApplyDeadZone(vectorResult.x);
                vectorResult.y = ApplyDeadZone(vectorResult.y);
            }
			
            if (_isRound && vectorResult.sqrMagnitude > 1)
            {
                vectorResult.Normalize();
            }

            var length = vectorResult.magnitude;
            var multiplier = _sensitivity.Evaluate(length);
            Value = (vectorResult.normalized * multiplier).ToValkyrie();
			
            _stick.position = new Vector3(
                _moveLeft.position.x + (xAxisDelta.x + vectorResult.x * xAxisDelta.magnitude) * 0.5f, 
                _moveDown.position.y + (yAxisDelta.y + vectorResult.y * yAxisDelta.magnitude) * 0.5f, 0);
        }

        #endregion

        #region DeadZone
        
        private float ApplyDeadZone(float val)
        {
            if (Mathf.Abs(val) < _axisHardDeadZone)
            {
                return 0;
            }
			
            if (val > 0)
            {
                return (val - _axisHardDeadZone) / (1 - _axisHardDeadZone);
            }
			
            if (val < 0)
            {
                return (val + _axisHardDeadZone) / (1 - _axisHardDeadZone);
            }

            return val;
        }
        
        #endregion

        #region Dynamic Joystick
        
        void ResetDynamicView()
        {
            IsPressed = false;

            _viewTransform.anchoredPosition = _defaultPosition;
            LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
        }
        void UpdateDynamicView(Vector2 point)
        {
            _viewTransform.localPosition = new Vector3((point.x /*- Screen.width / 2.0f*/) / _viewTransform.parent.lossyScale.x, 
                (point.y /*- Screen.height / 2.0f*/) / _viewTransform.parent.lossyScale.y, 0);
            LayoutRebuilder.ForceRebuildLayoutImmediate(gameObject.GetComponent<RectTransform>());
        }
        
        #endregion
    }
}