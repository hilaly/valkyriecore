﻿namespace Valkyrie.UserInput
{
    public interface IMoveJoystick : IVirtualJoystick
    {
        bool CruiseControl { get; set; }
        bool IsDynamic { get; set; }
    }
    
    public interface IVirtualJoystick
    {
        System.Numerics.Vector2 Value { get; }
    }
}