using System.Collections.Generic;
using UnityEngine;
using Valkyrie.Data;
using Valkyrie.Tools;

namespace Valkyrie.Scripts.Tools
{
    public class UnityDb : IDbAccess
    {
        string GetName<T>()
        {
            return typeof(T).FullName;
        }
        
        public T Get<T>(string id)
        {
            var dictionary = PlayerPrefs.GetString(GetName<T>(), "{}").ToObject<Dictionary<string, T>>();
            if (dictionary.TryGetValue(id, out var result))
                return result;
            return default(T);
        }

        public void Set<T>(string id, T value)
        {
            var dictionary = PlayerPrefs.GetString(GetName<T>(), "{}").ToObject<Dictionary<string, T>>();
            dictionary[id] = value;
            PlayerPrefs.SetString(GetName<T>(), dictionary.ToJson());
        }

        public void Delete<T>(string id)
        {
            var dictionary = PlayerPrefs.GetString(GetName<T>(), "{}").ToObject<Dictionary<string, T>>();
            if(dictionary.Remove(id))
                PlayerPrefs.SetString(GetName<T>(), dictionary.ToJson());
        }
    }
}