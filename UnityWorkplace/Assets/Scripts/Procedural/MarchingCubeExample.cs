﻿using System;
using System.IO;
using UnityEngine;
using Valkyrie.Math;
using Valkyrie.Misc;
using Valkyrie.Procedural;
using Valkyrie.Procedural.Fields;
using Valkyrie.Procedural.Triangulation;
using Factory = Valkyrie.Procedural.Fields.Factory;
using Vector3 = UnityEngine.Vector3;

namespace Procedural
{
    public class MarchingCubeExample : MonoBehaviour
    {
        public Material m_material;
        public MARCHING_MODE mode = MARCHING_MODE.CUBES;

        private string _script = "main = { Sphere(1) }";

        private string _lastGeneratedScript;
        
        private ITriangulator _triangulator;
        
        [SerializeField] private bool _runtimeGenerate;
        [SerializeField] private bool _rotate;

        [SerializeField] private Vector3 _minBounding = -2 * Vector3.one;
        [SerializeField] private Vector3 _maxBounding = 2 * Vector3.one;
        [Range(2,128)][SerializeField] private int _size = 16;

        private string _savePath;
        private string _lastLoadedTextAsset;
        [SerializeField] private TextAsset _textAsset;

        void Start()
        {
            Application.runInBackground = true;
            
            if (_textAsset != null)
                _script = _lastLoadedTextAsset = _textAsset.text;

            ISignedDistanceField source = Factory.Sphere(0.75f);
            
            //Set the mode used to create the mesh.
            //Cubes is faster and creates less verts, tetrahedrons is slower and creates more verts but better represents the mesh surface.
            //Surface is the value that represents the surface of mesh
            //For example the perlin noise has a range of -1 to 1 so the mid point is where we want the surface to cut through.
            //The target value does not have to be the mid point it can be any value with in the range.
            var bounding = new BoundingBox3() { Min = _minBounding.ToValkyrie(), Max = _maxBounding.ToValkyrie() };
            var surface = 0f;
            
            _triangulator = mode == MARCHING_MODE.TETRAHEDRON 
                ? source.TriangulateTetrahedrons(bounding, surface)
                : source.TriangulateCubes(bounding, surface);
        }

        void Update()
        {
            if (_runtimeGenerate && _lastGeneratedScript != _script || _lastLoadedTextAsset != _textAsset.text)
            {
                if (_lastLoadedTextAsset != _textAsset.text)
                    _script = _lastLoadedTextAsset = _textAsset.text;
                _lastGeneratedScript = _script;
                try
                {
                    var source = Factory.Parse(_script).Construct("main");
                    if (source != null)
                    {
                        _triangulator.Voxels = source;
                        _triangulator.Width = _triangulator.Height = _triangulator.Length = _size;
                        _triangulator.Bounding = new BoundingBox3()
                            {Min = _minBounding.ToValkyrie(), Max = _maxBounding.ToValkyrie()};
                        SdfVisualization.Fill(_triangulator, gameObject, m_material);
                    }
                    else
                        SdfVisualization.Clear(gameObject);
                }
                catch (Exception e)
                {
                    Debug.LogException(e, gameObject);
                    SdfVisualization.Clear(gameObject);
                }
            }
            
            if(_rotate)
                transform.Rotate(Vector3.up, 10.0f * Time.deltaTime);
        }

        private void OnGUI()
        {
            _script = GUILayout.TextArea(_script);
            if (_textAsset != null)
            {
                _savePath = GUILayout.TextField(_savePath);
                var validPath = Path.HasExtension(_savePath);
                GUI.enabled = validPath;
                if (GUILayout.Button("Save"))
                    File.WriteAllText(_savePath, _script);
                GUI.enabled = true;
            }
        }
    }

}
