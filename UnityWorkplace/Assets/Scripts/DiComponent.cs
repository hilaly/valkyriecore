using System;
using Valkyrie.UnityExtensions.Components;

public class DiComponent : DiUnityComponent
{
	protected override void SelfInit()
	{
		if(Startup.Instance == null)
			throw new Exception("Startup is not created");
		base.SelfInit();
	}
}
