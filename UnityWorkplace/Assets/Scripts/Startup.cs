using Valkyrie;
using Valkyrie.Di;

public class Startup
{
	#region Singleton

	static Startup _instance;

	public static Startup Instance
	{
		get
		{
			if(_instance == null)
				_instance = BuildContainer().Resolve<Startup>();
			return _instance;
		}
	}

	#endregion

	#region properties

	public IContainer RootContainer { get; }

	#endregion

	#region Ctor

	public Startup(IContainer container)
	{
		RootContainer = container;
	}

	#endregion

	static IContainer BuildContainer()
	{
		return Factory.Start(Constants.ApplicationId)
			.UseUnity(30)
			.UseMvvm(Constants.UiRootPrefab)
			.UseLibrary(new ProjectTemplateLibrary())
			.Build();
	}
}
