﻿using UnityEngine;

namespace Pgm
{
    [DisallowMultipleComponent]
    [SelectionBase]
    public class Player : MonoBehaviour
    {
        private void Update()
        {
            ProcessInput();
        }

        private void ProcessInput()
        {
            Vector3 result = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
                result.x += 1;
        }
    }
}