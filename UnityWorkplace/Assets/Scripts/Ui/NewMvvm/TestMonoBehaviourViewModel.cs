using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valkyrie.Data.Bind;
using Valkyrie.UnityExtensions.Components;

namespace Ui.NewMvvm
{
    [Bind]
    public class TestMonoBehaviourViewModel : DiUnityComponent
    {
        [SerializeField] private string _someText;
        [SerializeField] private string _iconPath;

        [Bind] public bool IsVisible { get; set; }
        
        [Bind] public string SomeText
        {
            get => _someText;
            set => _someText = value;
        }

        [Bind]
        public void Split()
        {
            TemplateProp = CreateRandom();
        }

        static object CreateRandom()
        {
            switch (Random.Range(0, 2))
            {
                case 0:
                    return new TemplateA();
                case 1:
                    return new TemplateB();
                default:
                    return null;
            }
        }

        [Bind]
        public void Res()
        {
            _someText = string.Empty;

            var count = Random.Range(0, 5);
            var t = (List<object>) TemplatePropCollection;
            t.Clear();
            for (var i = 0; i < count; ++i)
                t.Add(CreateRandom());
        }
        
        [Bind] public object TemplateProp { get; set; }
        [Bind] public IEnumerable TemplatePropCollection { get; } = new List<object>();

        [Bind] public string IconPath => _iconPath;
    }
}