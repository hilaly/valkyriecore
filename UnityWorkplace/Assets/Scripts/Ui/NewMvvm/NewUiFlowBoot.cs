using UnityEngine;
using Valkyrie.UnityExtensions.Components;

namespace Ui.NewMvvm
{
    public class NewUiFlowBoot : MonoBehaviour
    {
        [SerializeField] private GameObject _testUiPrefab;
    
        private void Awake()
        {
            var container = Startup.Instance.RootContainer;
            UiRoot.ShowBackground(_testUiPrefab, container);
        }
    }
}