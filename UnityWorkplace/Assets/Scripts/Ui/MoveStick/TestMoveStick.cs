using UnityEngine;
using Valkyrie.Data.Bind;
using Valkyrie.Misc;
using Valkyrie.UnityExtensions.Components;
using Valkyrie.UserInput;

namespace Ui.MoveStick
{
    [Bind]
    public class TestMoveStick : MonoBehaviour
    {
        private IMoveJoystick _joystick;

        [SerializeField] private bool _cruiseControl;
        [SerializeField] private bool _dynamic;
        
        void Awake()
        {
            var instance = Startup.Instance;
            
            _joystick = GetComponentInChildren<IMoveJoystick>();
        }

        private void Update()
        {
            _joystick.CruiseControl = _cruiseControl;
            _joystick.IsDynamic = _dynamic;
        }

        [Bind] public Vector2 Position => _joystick?.Value.ToUnity() ?? Vector2.zero;

        [Bind]
        public void ShowError()
        {
            UiRoot.ShowErrorPopup("TEST_TITLE", "TEST_TEXT", "TEXT_BUTTON");
        }
    }
}