using System;
using System.Collections.Generic;
using UnityEngine;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.UnityExtensions;

namespace Ui
{
    public interface IUpdateFileStorage
    {
        IUpdateFile Get(string uid);

        void RegisterFile(string uid, string localResourcePath);

        IPromise Update(string serverAddress, int serverPort);
    }
    

    public interface IUpdateFile
    {
        byte[] Content { get; }
    }

    class ByteSourceUpdateFile : IUpdateFile
    {
        public byte[] Content { get; }

        public ByteSourceUpdateFile(byte[] content)
        {
            Content = content;
        }
    }

    class OfflineFileStorage : IUpdateFileStorage
    {
        private readonly IAssetsFactory _assetsFactory;
        private readonly Dictionary<string, string> _uidToLocalPathMapping = new Dictionary<string, string>();

        public OfflineFileStorage(IAssetsFactory assetsFactory)
        {
            _assetsFactory = assetsFactory;
        }

        public IUpdateFile Get(string uid)
        {
            if (_uidToLocalPathMapping.TryGetValue(uid, out var path))
                return new ByteSourceUpdateFile(_assetsFactory.LoadAsset<TextAsset>(path).bytes);
            throw new Exception($"{uid} is not registered as updatable file");
        }

        public void RegisterFile(string uid, string localResourcePath)
        {
            _uidToLocalPathMapping[uid] = localResourcePath;
        }

        public IPromise Update(string serverAddress, int serverPort)
        {
            return Promise.Resolved();
        }
    }
}