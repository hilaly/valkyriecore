using System;
using System.Linq;
using System.Xml;
using UnityEngine;
using Valkyrie.Di;

namespace Ui.Markup
{
    public interface IUiView
    {
        object Build(object parent);
    }
    
    public interface IUiViewFactory
    {
        IUiView Build(string name);
    }

    interface IUiBank
    {
        XmlNode GetStyle(string name);
        XmlNode GetView(string name);
    }
    
    class UnityViewFactory : IUiViewFactory
    {
        private readonly IUiBank _bank;

        public UnityViewFactory(IUiBank bank)
        {
            _bank = bank;
        }

        public IUiView Build(string name)
        {
            var viewSource = _bank.GetView(name);
            return new XmlSourceUiView(viewSource, _bank);
        }
    }

    class UiBank : IUiBank
    {
        private readonly IUpdateFileStorage _fileStorage;

        public UiBank(IUpdateFileStorage fileStorage)
        {
            _fileStorage = fileStorage;
            
            _fileStorage.RegisterFile("UI_Styles", "Ui/Styles");
            _fileStorage.RegisterFile("UI_Views", "Ui/Views");
        }

        public XmlNode GetStyle(string name)
        {
            var text = System.Text.Encoding.UTF8.GetString(_fileStorage.Get("UI_Styles").Content);
            var doc = new XmlDocument();
            doc.LoadXml(text);
            return doc.SelectSingleNode($"//root/{name}");
        }

        public XmlNode GetView(string name)
        {
            var text = System.Text.Encoding.UTF8.GetString(_fileStorage.Get("UI_Views").Content);
            var doc = new XmlDocument();
            doc.LoadXml(text);
            return doc.SelectSingleNode($"//root/{name}");
        }
    }

    class NewMvvmLibrary : ILibrary
    {
        public void Register(IContainer container)
        {
            container.Register<UnityViewFactory>().AsInterfacesAndSelf().SingleInstance();
            container.Register<UiBank>().AsInterfacesAndSelf().SingleInstance();
            container.Register<OfflineFileStorage>().AsInterfacesAndSelf().SingleInstance();
        }
    }
    
    class XmlSourceUiView : IUiView
    {
        private readonly XmlNode _source;
        private readonly IUiBank _uiBank;

        public XmlSourceUiView(XmlNode source, IUiBank uiBank)
        {
            _source = source;
            _uiBank = uiBank;
        }

        public object Build(object parent)
        {
            var result = Create(_source, parent as GameObject);
            Build(_source, result, result);
            return result;
        }

        string GetNodeUid(XmlNode node)
        {
            return node.Attributes?["uid"]?.Value ?? node.Name;
        }

        void Build(XmlNode node, GameObject go, GameObject root)
        {
            foreach (XmlNode child in node.SelectNodes("components/*"))
                ApplyComponent(child, go, root);
            
            foreach (XmlNode child in node.SelectNodes("children/*"))
                Build(child, go.GetComponent<RectTransform>().Find(GetNodeUid(child)).gameObject, root);
        }

        GameObject Create(XmlNode node, GameObject parent)
        {
            var result = Create(GetNodeUid(node), parent);
            
            foreach (XmlNode child in node.SelectNodes("components/*"))
                CreateComponent(child, result);

            foreach (XmlNode child in node.SelectNodes("children/*"))
                Create(child, result);

            return result;
        }
        
        private void ApplyComponent(XmlNode componentDesc, GameObject go, GameObject root)
        {
        }

        private void CreateComponent(XmlNode componentDesc, GameObject go)
        {
            var typeName = componentDesc.Name;

            var type = AppDomain.CurrentDomain.GetAssemblies().SelectMany(u => u.GetTypes())
                .First(u => u.FullName == typeName);
            
            go.AddComponent(type);
        }

        GameObject Create(string name, GameObject parent)
        {
            var result = new GameObject(name);
            var rectTransform = result.AddComponent<RectTransform>();
            if (parent != null)
            {
                var parentTransform = parent.GetComponent<RectTransform>();
                if (parentTransform != null)
                    rectTransform.SetParent(parentTransform, false);
            }
            return result;
        }
    }
}