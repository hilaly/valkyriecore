using UnityEngine;
using Valkyrie.Di;

namespace Ui.Markup
{
    public class UiMarkupLanguageBoot : MonoBehaviour
    {
        [SerializeField] private GameObject _uiRoot;
    
        private void Awake()
        {
            var container = Startup.Instance.RootContainer;
            TestNewMvvm(container);
        }
        void TestNewMvvm(IContainer container)
        {
            var viewFactory = container.Resolve<IUiViewFactory>();
            var view = viewFactory.Build("Complex");
            var model = new ComplexUiViewModel();
            //TODO: bind view and model

            view.Build(_uiRoot);
        }
    }
}