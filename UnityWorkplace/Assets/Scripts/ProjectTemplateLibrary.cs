using Valkyrie.Di;

class ProjectTemplateLibrary : ILibrary
{
	public void Register(IContainer container)
	{
		container.Register<Startup>().AsSelf().SingleInstance();
	}
}
