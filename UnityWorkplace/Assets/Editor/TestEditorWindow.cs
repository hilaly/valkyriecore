using UnityEditor;
using UnityEngine;
using Valkyrie;
using Valkyrie.Ui.Window;

namespace Editor
{
    public class TestEditorWindow : ValkyrieEditorWindow
    {
        [MenuItem("Valkyrie/Experimental/Editor Window")]
        private static void OpenWindow()
        {
            var window = GetWindow<TestEditorWindow>();
            window.MenuHeight = 20f;
            window.titleContent = new GUIContent("Test Window");
        }

        protected override void BuildLayout(EditorWindowFactory factory)
        {
            factory
                .BeginHorizontal()
                    .Panel(DrawGuiImpl, false)
                    .BeginVertical()
                        .Panel(DrawGuiImpl, true)
                        .Panel(DrawGuiImpl, true)
                    .EndVertical()
                .EndHorizontal();
        }

        private void DrawGuiImpl(Rect windowContentRect)
        {
            GUI.Box(windowContentRect, "BOX");
            //UseGuiLayout = GUILayout.Toggle(UseGuiLayout, "Use layout");
            MenuHeight = GUILayout.Toggle(MenuHeight > 0f, "Show menu") ? 20f : 0f;
        }
    }
}