using UnityEditor;

namespace Editor
{
    public static class UiEditorTool
    {
        [MenuItem("GameObject/Valkyrie/Save As Template", false, 14)]
        static void SaveTemplate()
        {}
        
        [MenuItem("GameObject/Valkyrie/Save As View", false, 13)]
        static void SaveView()
        {}
    }
}