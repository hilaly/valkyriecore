namespace PIO.Portable.Map
{
    public interface IStaticMapDataProvider
    {
        float GetHeight(float x, float z);
    }
}