﻿namespace PIO.Portable.Map
{
    class ZeroMapProvider : IStaticMapDataProvider
    {
        #region Implementation of IStaticMapDataProvider

        public float GetHeight(float x, float z)
        {
            return 0;
        }

        #endregion
    }
}