﻿namespace PIO.Portable.Map.Generation
{
    public class Restrictions
    {
        public int Width;
        public int Height;

        public int MinWidth;
        public int MinHeight;
    }
}