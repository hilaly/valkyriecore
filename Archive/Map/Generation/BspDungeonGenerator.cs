using PIO.Math;
using UnityEngine;
using Random = PIO.Math.Random;

namespace PIO.Portable.Map.Generation
{
    class BspDungeonGenerator : IDungeonGenerator
    {
		public bool FillInner;
		public bool FillAll;
		public bool DisableHalls;

        struct IntVector2
        {
            // ReSharper disable InconsistentNaming
            public int x;
            public int y;
            // ReSharper restore InconsistentNaming

            public IntVector2(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
			

            // ReSharper disable InconsistentNaming
            public static readonly IntVector2 zero = new IntVector2 {x=0,y=0};
            // ReSharper restore InconsistentNaming
        }

        class Room
        {
            public IntVector2 Min;
            public IntVector2 Max;
        }
        class BspNode
        {
            public IntVector2 Min;
            public IntVector2 Max;
            public int Width { get { return Max.x - Min.x + 1; } }
            public int Height { get { return Max.y - Min.y + 1; } }
            public BspNode[] Childs;
            public Room Room;
        }

        public TileRepresentation Generate(Restrictions restrictions, int seed)
        {
            var random = new Random(seed);
			var node = new BspNode { Min = IntVector2.zero, Max = new IntVector2(restrictions.Width, restrictions.Height) };
			var result = new TileRepresentation(node.Width, node.Height);
            Build(node, restrictions.MinWidth, restrictions.MinHeight, random);
            GenerateRooms(node, result, random);
            return result;
        }

        void Build(BspNode node, float width, float height, IRandom random)
        {
            if(node.Width <= width || node.Height <= height)
                return;

            var direction = random.Range(0, 2);

            switch(direction)
            {
                //Vertical
                case 0:
                {
                    var min = (int)(node.Height * 0.2);
                    if(min < 1)
                        min = 1;
                    var max = node.Height - min;
                    var t = random.Range(min, max);

                    t = node.Height / 2;

                    var y = t + node.Min.y;
                    var first = new BspNode { Min = new IntVector2(node.Min.x, y+1), Max = node.Max };
                    var second = new BspNode { Min = node.Min, Max = new IntVector2(node.Max.x, y) };
                    node.Childs = new[] {first, second};
                    Build(first, width, height, random);
                    Build(second, width, height, random);
                }
                    break;
                case 1:
                {
                    var min = (int)(node.Width * 0.2);
                    if(min < 1)
                        min = 1;
                    var max = node.Width - min;
                    var t = random.Range(min, max);

                    t = node.Width / 2;

                    var x = t + node.Min.x;
                    var first = new BspNode { Min = node.Min, Max = new IntVector2(x, node.Max.y) };
                    var second = new BspNode { Min = new IntVector2(x+1, node.Min.y), Max = node.Max };
                    node.Childs = new[] {first, second};
                    Build(first, width, height, random);
                    Build(second, width, height, random);
                }
                    break;
            }
        }

        void GenerateRooms(BspNode node, TileRepresentation result, IRandom random)
        {
            var roomIndex = 1;

            if(node.Childs != null)
            {
                GenerateRooms(node.Childs[0], result, random);
                GenerateRooms(node.Childs[1], result, random);
				if(!DisableHalls)
                    GenerateWalls(node, result, random);
            }
            else
            {
                var x0 = random.Range(node.Min.x + 1, node.Max.x);
                var x1 = random.Range(node.Min.x + 1, node.Max.x);
                var y0 = random.Range(node.Min.y + 1, node.Max.y);
                var y1 = random.Range(node.Min.y + 1, node.Max.y);
				if(FillInner)
				{
					x0 = node.Min.x + 1;
					x1 = node.Max.x - 1;
					y0 = node.Min.y + 1;
					y1 = node.Max.y - 1;
				}
				if(FillAll)
				{
					x0 = node.Min.x;
					x1 = node.Max.x;
					y0 = node.Min.y;
					y1 = node.Max.y;
				}

                //Normalize values
                if (x0 > x1)
                {
                    var temp = x0;
                    x0 = x1;
                    x1 = temp;
                }
                if (y0 > y1)
                {
                    var temp = y0;
                    y0 = y1;
                    y1 = temp;
                }

                node.Room = new Room { Min = new IntVector2(x0,y0), Max = new IntVector2(x1, y1)};
                for(int x = x0; x <= x1; ++x)
                    for(int y = y0; y <= y1; ++y)
                    {
                        if(result[x,y] != 0)
                            Debug.Break();
						result[x,y] = roomIndex;
                    }
				++roomIndex;
            }
        }

        void GenerateWalls(BspNode node, TileRepresentation result, IRandom random)
        {
            if(node.Childs == null)
                return;

            var roomIndex = 2;

            var n0 = node.Childs[0].Room;
            var n1 = node.Childs[1].Room;

            if(Between(n0.Min.x, n1.Min.x, n1.Max.x) || Between(n0.Max.x, n1.Min.x, n1.Max.x) ||
               Between(n1.Min.x, n0.Min.x, n0.Max.x) || Between(n1.Max.x, n0.Min.x, n0.Max.x))
            {
                //Vertical

                var x = random.Range(Max(n0.Min.x, n1.Min.x), Min(n0.Max.x, n1.Max.x) + 1);

                var y = Min(n0.Max.y, n1.Max.y);
                while(true)
                {
                    if(result[x, y] != 0)
                    {
                        ++y;
                        break;
                    }
                    --y;
                }

                while(result[x, y] == 0)
                {
					result[x, y] = roomIndex;
                    ++y;
                }
            }
            else
            {
                //Horizontal
                var min = Max(n0.Min.y, n1.Min.y);
                var max = Min(n0.Max.y, n1.Max.y);

                var y = random.Range(min, max + 1);

                var x = Min(n0.Max.x, n1.Max.x);
                while(true)
                {
                    if(result[x, y] != 0)
                    {
                        ++x;
                        break;
                    }
                    --x;
                }

                while(result[x, y] == 0)
                {
					result[x, y] = roomIndex;
                    ++x;
                }
            }

            node.Room = new Room
            {
                Min = new IntVector2(Min(n0.Min.x, n1.Min.x), Min(n0.Min.y, n1.Min.y)),
                Max = new IntVector2(Max(n0.Max.x, n1.Max.x), Max(n0.Max.y, n1.Max.y))
            };
            node.Childs = null;
        }

        int Max(int a, int b)
        {
            return a > b ? a : b;
        }

        int Min(int a, int b)
        {
            return a < b ? a : b;
        }

        bool Between(int value, int min, int max)
        {
            return value <= max && value >= min;
        }
    }
}