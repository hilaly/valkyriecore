namespace PIO.Portable.Map.Generation
{
    public interface IDungeonGenerator
    {
        TileRepresentation Generate(Restrictions restrictions, int seed);
    }
}