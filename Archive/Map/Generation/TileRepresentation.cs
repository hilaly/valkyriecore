﻿namespace PIO.Portable.Map.Generation
{
    public class TileRepresentation
    {
        public int Width;
        public int Height;

        readonly int[] _tiles;

        public TileRepresentation(int width, int height)
        {
            Width = width;
            Height = height;
            _tiles = new int[width*height];
        }
		
        public int this[int x, int y]
        {
            get
            {
                var index = y*Width + x;
                return _tiles[index];
            }
            set
            {
                var index = y*Width + x;
                _tiles[index] = value;
            }
        }
    }
}