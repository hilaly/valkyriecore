﻿using UnityEngine;

namespace PIO.Portable.Map
{
    public class MapManager : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private MapSettings _mapSettings;
#pragma warning restore 649

        public MapSettings Settings { get { return _mapSettings; } }
    }
}