﻿using PIO.Portable.Map;
using UnityEditor;
using UnityEngine;

namespace Assets.PIO.Portable.Map.Editor
{
    class MapSettingsWindow : EditorWindow
    {
        [MenuItem(BaseMenu + "Create/MapLayer")]
        public static void CreateLayer()
        {
            var ngo = new GameObject("MapLayer");
            ngo.AddComponent<MapLayer>();
            Selection.activeGameObject = ngo;
        }

        private const string BaseMenu = "TileMap/";
        private const string WindowName = "TileMapSettings";

        [MenuItem(BaseMenu + "Editor")]
        public static void ShowWindow()
        {
            var window = GetWindow(typeof(MapSettingsWindow), false, WindowName);
            window.Show();
        }

        #region Repaint

        public void OnSelectionChange()
        {
            Repaint();
        }

        public void Update()
        {
            Repaint();
        }

        #endregion

        // ReSharper disable once InconsistentNaming
        public void OnGUI()
        {
        }
    }
}
