﻿using PIO.Portable.Map;
using UnityEditor;
using UnityEngine;

namespace Assets.PIO.Portable.Map.Editor
{
    [CustomEditor(typeof(MapLayer))]
    public class MapLayerEditor : UnityEditor.Editor
    {
        SerializedProperty _settings;
        SerializedProperty _size;

        void OnEnable()
        {
            _settings = serializedObject.FindProperty("Settings");
            _size = serializedObject.FindProperty("Size");
        }

        #region Overrides of Editor

        public override void OnInspectorGUI()
        {
            var layer = (MapLayer) target;

            EditorGUILayout.BeginVertical();

            var settins = EditorGUILayout.ObjectField("Settings", layer.Settings, typeof (MapSettings), true);
            var size = EditorGUILayout.Vector2Field("Size", layer.Size);
            if (settins != layer.Settings ||
                size != layer.Size)
            {
                layer.Settings = (MapSettings) settins;
                layer.Size = size;

                layer.Rebuild();
                EditorUtility.SetDirty(target);
            }

            EditorGUILayout.EndVertical();

            serializedObject.ApplyModifiedProperties();

            if(GUILayout.Button("Rebuild"))
                layer.Rebuild();
        }

        #endregion
    }
}
