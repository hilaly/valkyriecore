using System.Collections.Generic;
using UnityEngine;

namespace PIO.Portable.Map
{
    public class MapLayer : MonoBehaviour
    {
        public MapSettings Settings;
        public Vector2 Size;

        readonly List<Room> _roomsList = new List<Room>();

        public IEnumerable<Room> Rooms { get { return _roomsList; } }
        
        public IStaticMapDataProvider StaticDataProvider { get; set; }

        Room CreateRoom(Vector3 position)
        {
            var go = new GameObject("Room");
            go.transform.SetParent(transform);
            go.transform.localPosition = position;
            go.transform.localRotation = Quaternion.identity;
            var room = go.AddComponent<Room>();
            _roomsList.Add(room);
            room.Init(this);
            return room;
        }

        void DestroyRoom(Room room)
        {
            _roomsList.Remove(room);
            if(Application.isPlaying)
                Destroy(room.gameObject);
            else
                DestroyImmediate(room.gameObject);
        }

        void Clear()
        {
            while (_roomsList.Count > 0)
            {
                DestroyRoom(_roomsList[0]);
            }
        }

        public void Rebuild()
        {
            Clear();

            StaticDataProvider = new ZeroMapProvider();

            var xRooms = Mathf.CeilToInt(Size.x/Settings.RoomSize.x);
            var zRooms = Mathf.CeilToInt(Size.y/Settings.RoomSize.y);

            for (var z = 0; z < zRooms; ++z)
            {
                for (var x = 0; x < xRooms; ++x)
                {
                    var position = new Vector3(x * Settings.RoomSize.x, 0, z * Settings.RoomSize.y);
                    CreateRoom(position);
                }
            }
        }
    }
}