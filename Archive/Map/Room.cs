﻿using UnityEngine;

namespace PIO.Portable.Map
{
    [RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
    public class Room : MonoBehaviour
    {
        private Mesh _mesh;

        public MapLayer Layer { get; private set; }

        public void Init(MapLayer parent)
        {
            Layer = parent;

            GenerateMesh();
        }

        void GenerateMesh()
        {
            GetComponent<MeshRenderer>().sharedMaterial = Layer.Settings.GroundMaterial;

            if (_mesh == null)
            {
                var filter = GetComponent<MeshFilter>();
                _mesh = filter.sharedMesh;
                if (_mesh == null)
                {
                    _mesh = new Mesh();
                    filter.mesh = _mesh;
                }
            }
            _mesh.Clear();

            var roomSize = new Vector3(Layer.Settings.RoomSize.x, 1, Layer.Settings.RoomSize.y);

            var tileSize = Layer.Settings.TileSize;

            var xSize = Mathf.CeilToInt(roomSize.x/tileSize.x);
            var zSize = Mathf.CeilToInt(roomSize.z/tileSize.z);

            var tilesCount = xSize*zSize;
            var verticesCount = tilesCount*4;
            var vertices = new Vector3[verticesCount];
            
            for (var z = 0; z < zSize; ++z)
            {
                var zPos = z * tileSize.z;
                for (var x = 0; x < xSize; ++x)
                {
                    var tileIndex = x + z*xSize;

                    var xPos = x * tileSize.x;

                    var index = tileIndex*4;
                    vertices[index + 0] = new Vector3(xPos, Layer.StaticDataProvider.GetHeight(xPos, zPos) * tileSize.y, zPos);
                    vertices[index + 1] = new Vector3(xPos + tileSize.x, Layer.StaticDataProvider.GetHeight(xPos + tileSize.x, zPos) * tileSize.y, zPos);
                    vertices[index + 2] = new Vector3(xPos, Layer.StaticDataProvider.GetHeight(xPos, zPos + tileSize.z) * tileSize.y, zPos + tileSize.z);
                    vertices[index + 3] = new Vector3(xPos + tileSize.x, Layer.StaticDataProvider.GetHeight(xPos + tileSize.x, zPos + tileSize.z) * tileSize.y, zPos + tileSize.z);
                }
            }

            var indices = new int[tilesCount * 6];
            for (var i = 0; i < tilesCount; ++i)
            {
                var index = i * 6;
                var vertexIndex = i*4;

                indices[index + 0] = vertexIndex + 0;
                indices[index + 1] = vertexIndex + 2;
                indices[index + 2] = vertexIndex + 1;

                indices[index + 3] = vertexIndex + 1;
                indices[index + 4] = vertexIndex + 2;
                indices[index + 5] = vertexIndex + 3;
            }

            _mesh.vertices = vertices;
            _mesh.triangles = indices;

            //_mesh.Optimize();
            _mesh.RecalculateNormals();
            _mesh.RecalculateBounds();
            _mesh.UploadMeshData(false);

        }
    }


}
