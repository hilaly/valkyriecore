﻿using UnityEngine;

namespace PIO.Portable.Map
{
    [CreateAssetMenu(menuName = "Maps/Settings")]
    public class MapSettings : ScriptableObject
    {
        public Vector3 TileSize = Vector3.one;
        public Vector2 RoomSize = new Vector2(64, 64);
        public Material GroundMaterial;
    }
}