﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    class ContainerBuilder : IContainerBuilder, IContainerRegistrator
    {
        public const string DefaultResolverId = "default";
        
        #region fields

        readonly List<IResolverBuilder> _registerTypes = new List<IResolverBuilder>();
        readonly List<ILibrary> _initModules = new List<ILibrary>();

        #endregion

        #region Dynamic resolving

        static object TypeConstructor(Type type, IDiResolver container, List<object> args)
        {
            return Utils.FabricateObject(type, args, container);
        }
        
        static T GenericConstructor<T>(IDiResolver container, List<object> args)
        {
            return (T)Utils.FabricateObject(typeof(T), args, container);
        }

        #endregion

        #region public Interface

        public IInstanceConfiguration<T> RegisterInstance<T>(T instance)
        {
            var result = new InstanceConfiguration<T>(instance, DefaultResolverId);
            _registerTypes.Add(result);
            return result;
        }

        public ITypeConfiguration RegisterType(Type type)
        {
            var result = new TypeConfiguration(type, (container, args) => TypeConstructor(type, container, args),
                DefaultResolverId);
            _registerTypes.Add(result);
            return result;
        }

        public ITypeConfiguration<T> RegisterType<T>()
        {
            return Register(GenericConstructor<T>);
        }
        
        public IGenericTypeConfiguration RegisterGenericType(Type type)
        {
            if (!type.IsGenericType)
                throw new InvalidOperationException($"Can not register {type.Name} as generic type");

            var result = new GenericTypeConfiguration(type, DefaultResolverId);
            _registerTypes.Add(result);
            return result;
        }

        public ITypeConfiguration<T> Register<T>(Func<IDiResolver, List<object>, T> resolver)
        {
            var result = new TypeConfiguration<T>(resolver, DefaultResolverId);
            _registerTypes.Add(result);
            return result;
        }

        public ITypeConfiguration<T> Register<T>(Func<IDiResolver, T> resolver)
        {
            return Register((container, args) => resolver(container));
        }

        public IContainer Build()
        {
            var result = new Container();

            //Register additional
            RegisterInstance(result).As<Container>().As<IContainer>();

            //Process libraries
            foreach (var module in _initModules)
                module.Load(this);

            //create resolvers
            var resolvers = new List<Container.ResolverData>();
            foreach(var builder in _registerTypes)
            {
                var resolver = builder.Build();
                foreach (var type in builder.ResolveTypes)
                    resolvers.Add(new Container.ResolverData { Id = builder.Id, Type = type, Resolver = resolver });
            }

            result.Init(resolvers);

            return result;
        }

        public IContainerRegistrator RegisterLibrary(ILibrary library)
        {
            _initModules.Add(library);
            return this;
        }

        #endregion
    }
}
