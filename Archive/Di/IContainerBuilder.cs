﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IContainerBuilder : IContainerRegistrator
    {
        IContainer Build();
    }
}