﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    interface IResolver
    {
        object Instantiate(IScope scope, Type requestedType, List<object> args);
    }
}
