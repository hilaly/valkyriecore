﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IContainerRegistrator
    {
        ITypeConfiguration<T> Register<T>(Func<IDiResolver, T> resolver);
        ITypeConfiguration<T> Register<T>(Func<IDiResolver, List<object>, T> resolver);
        IGenericTypeConfiguration RegisterGenericType(Type type);
        IInstanceConfiguration<T> RegisterInstance<T>(T instance);
        IContainerRegistrator RegisterLibrary(ILibrary library);
        ITypeConfiguration RegisterType(Type type);
        ITypeConfiguration<T> RegisterType<T>();
    }
}