﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    class ResolveArgs<T> : IResolveArgs<T>
    {
        public IScope Context { get; }

        public T Instance { get; }

        public ResolveArgs(IScope context, T instance)
        {
            Context = context;
            Instance = instance;
        }
    }
}
