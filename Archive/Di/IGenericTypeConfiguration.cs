using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IGenericTypeConfiguration
    {
        IGenericTypeConfiguration As(Type type);
        IGenericTypeConfiguration PropertiesAutowired();

        IGenericTypeConfiguration InstancePerDependency();
        IGenericTypeConfiguration SingleInstance();
        IGenericTypeConfiguration InstancePerScope();
    }
}