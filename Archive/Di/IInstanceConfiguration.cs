﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IInstanceConfiguration<T>
    {
        IInstanceConfiguration<T> AsSelf();
        IInstanceConfiguration<T> As<TK>();
        IInstanceConfiguration<T> BuildUp();

        IInstanceConfiguration<T> InstancePerScope(string id);

        IInstanceConfiguration<T> OnActivating(Action<IResolveArgs<T>> args);
        IInstanceConfiguration<T> OnActivated(Action<IResolveArgs<T>> args);
    }
}
