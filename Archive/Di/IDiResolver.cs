using System;
using System.Collections;
using System.Collections.Generic;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IDiResolver
    {
        T Resolve<T>(List<object> args);

        object Resolve(Type type, List<object> args);

        bool TryResolve(Type type, List<object> args, out object instance);

        IEnumerable<T> ResolveAll<T>();

        IEnumerable ResolveAll(Type type);
    }
}