﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IScope : IDisposable, IDiResolver
    {
        string Id { get; }

        IScope BeginScope(string scopeId);
    }
}
