﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    interface IResolverBuilder
    {
        string Id { get; }
        Type[] ResolveTypes { get; }

        IResolver Build();
    }
}
