﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface ILibrary
    {
        void Load(IContainerRegistrator builder);
    }
}