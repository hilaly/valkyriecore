﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Valkyrie.Data;
using Valkyrie.Data.PersistentData;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public static class Utils
    {
        static bool TryGet(Type type, IDiResolver container, List<object> args, out object result)
        {
            if (type.IsInstanceOfType(container))
            {
                result = container;
                if(!args.Contains(result))
                    args.Add(result);
                return true;
            }

            for (var i = 0; i < args.Count; ++i)
            {
                var instance = args[i];
                if (type.IsInstanceOfType(instance))
                {
                    result = instance;
                    if(!args.Contains(result))
                        args.Add(result);
                    return true;
                }
            }

            if (container.TryResolve(type, args, out result))
            {
                if(!args.Contains(result))
                    args.Add(result);
                return true;
            }
            return false;
        }

        static bool TryGet(Type type, IDiResolver container, List<object> args, out object result, ref int startsFrom)
        {
            for (var i = startsFrom; i < args.Count; ++i)
            {
                var instance = args[i];
                if (type.IsAssignableFrom(instance.GetType()))
                {
                    result = instance;
                    startsFrom = i + 1;
                    return true;
                }
            }

            for (var i = 0; i < startsFrom; ++i)
            {
                var instance = args[i];
                if (type.IsAssignableFrom(instance.GetType()))
                {
                    result = instance;
                    startsFrom = i + 1;
                    return true;
                }
            }

            return container.TryResolve(type, args, out result);
        }

        internal static IEnumerable<FieldInfo> GetFields(Type type, BindingFlags flags)
        {
            foreach (var propInfo in type.GetFields(flags))
                yield return propInfo;
            if (type.BaseType == null) yield break;
            foreach (
                var propInfo in
                GetFields(type.BaseType, BindingFlags.NonPublic | flags).Where(u => u.IsPrivate)
            )
                yield return propInfo;
        }

        public static object PropertiesResolverByAttribute(object instance, List<object> args, IDiResolver container)
        {
            var type = instance.GetType();
            IPersistentStorage persistentStorage = null;

            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var info in properties)
            {
                if (!info.CanWrite)
                    continue;

                var settingsAttribute = info.GetCustomAttribute<SettingsAttribute>(true);
                if (settingsAttribute != null)
                {
                    var keyValue = settingsAttribute.Name;
                    if (keyValue.IsNullOrEmpty())
                        throw new CanNotResolveException(type, $"Settings for property {info.Name} must have Name");

                    if (persistentStorage == null)
                        persistentStorage = container.Resolve<IPersistentStorage>();

                    if (TryGet(typeof(Scope), container, args, out var oScope) &&
                        TryGet(typeof(IDispatcher), container, args, out var oDispatcher))
                    {
                        if (!persistentStorage.Has(keyValue))
                        {
                            if(settingsAttribute.JsonValue.NotNullOrEmpty())
                                persistentStorage.Set(keyValue, settingsAttribute.JsonValue.ToObject(info.PropertyType));
                            else if (TryGet(info.PropertyType, container, args, out var defaultValue))
                                persistentStorage.Set(keyValue, defaultValue);
                        }

                        var dispatcher = (IDispatcher) oDispatcher;
                        var scope = (Scope) oScope;
                        var temp = persistentStorage.Get(keyValue, info.PropertyType);
                        scope.Add(dispatcher.EveryPostUpdate(() =>
                        {
                            var newValue = persistentStorage.Get(keyValue, info.PropertyType);
                            if (newValue != temp)
                                info.SetValue(instance, temp = newValue, null);
                            newValue = info.GetValue(instance, null);
                            if (newValue != temp)
                                persistentStorage.Set(keyValue, temp = newValue);
                        }));
                    }
                }

                var attribute = info.GetCustomAttribute<InjectAttribute>(true);
                if (attribute != null)
                {
                    if (TryGet(info.PropertyType, container, args, out var value))
                        info.SetValue(instance, value, null);
                    else if (!attribute.CanBeNull)
                        throw new CanNotResolveException(type,
                            $"Can not resolve {info.PropertyType} for property {info.Name}");
                }
            }

            var fields = GetFields(type, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var info in fields)
            {
                if (info.IsInitOnly)
                    continue;


                var settingsAttribute = info.GetCustomAttribute<SettingsAttribute>(true);
                if (settingsAttribute != null)
                {
                    var keyValue = settingsAttribute.Name;
                    if (keyValue.IsNullOrEmpty())
                        throw new CanNotResolveException(type, $"Settings for property {info.Name} must have Name");

                    if (persistentStorage == null)
                        persistentStorage = container.Resolve<IPersistentStorage>();

                    if (TryGet(typeof(Scope), container, args, out var oScope) &&
                        TryGet(typeof(IDispatcher), container, args, out var oDispatcher))
                    {
                        if (!persistentStorage.Has(keyValue))
                        {
                            if(settingsAttribute.JsonValue.NotNullOrEmpty())
                                persistentStorage.Set(keyValue, settingsAttribute.JsonValue.ToObject(info.FieldType));
                            else if (TryGet(info.FieldType, container, args, out var defaultValue))
                                persistentStorage.Set(keyValue, defaultValue);
                        }

                        var dispatcher = (IDispatcher) oDispatcher;
                        var scope = (Scope) oScope;
                        var temp = persistentStorage.Get(keyValue, info.FieldType);
                        scope.Add(dispatcher.EveryPostUpdate(() =>
                        {
                            var newValue = persistentStorage.Get(keyValue, info.FieldType);
                            if (newValue != temp)
                                info.SetValue(instance, temp = newValue);
                            newValue = info.GetValue(instance);
                            if (newValue != temp)
                                persistentStorage.Set(keyValue, temp = newValue);
                        }));
                    }
                }

                var attribute = info.GetCustomAttribute<InjectAttribute>(true);
                if (attribute != null)
                {
                    if (TryGet(info.FieldType, container, args, out var value))
                        info.SetValue(instance, value);
                    else if (!attribute.CanBeNull)
                        throw new CanNotResolveException(type,
                            $"Can not resolve {info.FieldType} for field {info.Name}");
                }
            }

            return instance;
        }

        public static object PropertiesResolverBySetters(object instance, List<object> args, IDiResolver container)
        {
            return instance;
            var type = instance.GetType();

            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var info in properties)
            {
                if (!info.CanWrite)
                    continue;

                var attribute = info.GetCustomAttribute<InjectAttribute>(true);
                if (attribute == null)
                    continue;

                if (TryGet(info.PropertyType, container, args, out var value))
                    info.SetValue(instance, value, null);
                else
                {
                    if (!attribute.CanBeNull)
                        throw new CanNotResolveException(type,
                            $"Can not resolve {info.PropertyType} for property {info.Name}");
                }
            }

            return instance;
        }

        public static object FabricateObject(Type instanceType, List<object> args, IDiResolver container)
        {
            var ctors = instanceType.GetConstructors();
            foreach (var ctor in ctors.OrderByDescending(u => u.GetParameters().Length))
            {
                var paramInfos = ctor.GetParameters();
                var ctorArgs = new object[paramInfos.Length];
                var complete = true;
                for (var i = 0; i < paramInfos.Length; ++i)
                {
                    if (!TryGet(paramInfos[i].ParameterType, container, args, out var arg))
                    {
                        complete = false;
                        break;
                    }

                    ctorArgs[i] = arg;
                }

                if (!complete)
                    continue;

                var result = ctor.Invoke(ctorArgs);
                return result;
            }

            throw new CanNotResolveException(instanceType, "Can not resolve all arguments for any constructor");
        }

        public static object Resolve(this IDiResolver container, Type type, params object[] args)
        {
            return container.Resolve(type, args.ToList());
        }

        public static T Resolve<T>(this IDiResolver container, params object[] args)
        {
            return container.Resolve<T>(args.ToList());
        }

        public static object Invoke(this IDiResolver container, MethodBase method, object instance,
            params object[] args)
        {
            var argsList = args.ToList();
            var paramInfos = method.GetParameters();
            var methodArgs = new object[paramInfos.Length];
            var complete = true;
            var startsFrom = 0;
            for (var i = 0; i < paramInfos.Length; ++i)
            {
                if (!TryGet(paramInfos[i].ParameterType, container, argsList, out var arg, ref startsFrom))
                {
                    complete = false;
                    break;
                }

                methodArgs[i] = arg;
            }

            if (!complete)
                throw new CanNotResolveException(method.GetType(), "Can not resolve all arguments for method");

            return method.Invoke(instance, methodArgs);
        }

        public static TResult Invoke<T1, T2, T3, T4, TResult>(this IDiResolver container,
            Func<T1, T2, T3, T4, TResult> callFunc, params object[] args)
        {
            return (TResult) container.Invoke(callFunc.Method, callFunc.Target, args);
        }

        public static TResult Invoke<T1, T2, T3, TResult>(this IDiResolver container,
            Func<T1, T2, T3, TResult> callFunc, params object[] args)
        {
            return (TResult) container.Invoke(callFunc.Method, callFunc.Target, args);
        }

        public static TResult Invoke<T1, T2, TResult>(this IDiResolver container,
            Func<T1, T2, TResult> callFunc, params object[] args)
        {
            return (TResult) container.Invoke(callFunc.Method, callFunc.Target, args);
        }

        public static TResult Invoke<T1, TResult>(this IDiResolver container,
            Func<T1, TResult> callFunc, params object[] args)
        {
            return (TResult) container.Invoke(callFunc.Method, callFunc.Target, args);
        }

        public static TResult Invoke<TResult>(this IDiResolver container,
            Func<TResult> callFunc, params object[] args)
        {
            return (TResult) container.Invoke(callFunc.Method, callFunc.Target, args);
        }

        public static void Invoke<T1, T2, T3, T4>(this IDiResolver container, Action<T1, T2, T3, T4> callAction,
            params object[] args)
        {
            container.Invoke(callAction.Method, callAction.Target, args);
        }

        public static void Invoke<T1, T2, T3>(this IDiResolver container, Action<T1, T2, T3> callAction,
            params object[] args)
        {
            container.Invoke(callAction.Method, callAction.Target, args);
        }

        public static void Invoke<T1, T2>(this IDiResolver container, Action<T1, T2> callAction,
            params object[] args)
        {
            container.Invoke(callAction.Method, callAction.Target, args);
        }

        public static void Invoke<T1>(this IDiResolver container, Action<T1> callAction,
            params object[] args)
        {
            container.Invoke(callAction.Method, callAction.Target, args);
        }
    }
}
