﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IResolveArgs<out T>
    {
        T Instance { get; }
        IScope Context { get; }
    }
}
