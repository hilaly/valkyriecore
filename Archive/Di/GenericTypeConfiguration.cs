using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    class GenericTypeConfiguration : IGenericTypeConfiguration, IResolverBuilder, IResolver
    {
        Dictionary<Type, TypeConfiguration> _configurations = new Dictionary<Type, TypeConfiguration>();
        
        readonly HashSet<Type> _resolveTypes = new HashSet<Type>();
        private ResolvingScopeType _resolvingType = ResolvingScopeType.InstancePerDependency;

        #region Ctor

        public GenericTypeConfiguration(Type instanceType, string defaultResolverId)
        {
            InstanceType = instanceType;
            Id = defaultResolverId;
        }

        #endregion

        #region Implementation of IGenericTypeConfiguration

        public IGenericTypeConfiguration As(Type type)
        {
            _resolveTypes.Add(type);
            return this;
        }
        public IGenericTypeConfiguration PropertiesAutowired()
        {
            ResolveProperties = true;
            return this;
        }

        public IGenericTypeConfiguration InstancePerDependency()
        {
            _resolvingType = ResolvingScopeType.InstancePerDependency;
            return this;
        }
        public IGenericTypeConfiguration SingleInstance()
        {
            _resolvingType = ResolvingScopeType.SingleInstance;
            return this;
        }
        public IGenericTypeConfiguration InstancePerScope()
        {
            _resolvingType = ResolvingScopeType.InstancePerScope;
            return this;
        }

        #endregion

        #region Implementation of IResolverBuilder

        private Type InstanceType { get; }

        public string Id { get; private set; }
        public Type[] ResolveTypes
        {
            get
            {
                if (_resolveTypes.Count == 0)
                    return new[] { InstanceType };
                return
                    _resolveTypes.ToArray();
            }
        }

        public IResolver Build()
        {
            return this;
        }

        #endregion

        #region Implementation of IResolver

        public object Instantiate(IScope container, Type requestedType, List<object> args)
        {
            var genericArgs = requestedType.GetGenericArguments();
            var concreteInstanceType = InstanceType.MakeGenericType(genericArgs);
            if (!_configurations.TryGetValue(concreteInstanceType, out var typeConfiguration))
                _configurations.Add(concreteInstanceType,
                    typeConfiguration = new TypeConfiguration(concreteInstanceType,
                        (resolver, os) => Utils.FabricateObject(concreteInstanceType, os, resolver), Id));
            return typeConfiguration.Build().Instantiate(container, requestedType, args);
        }

        private bool ResolveProperties { get; set; }

        #endregion
    }
}