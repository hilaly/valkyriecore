﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface IContainer
    {
        IScope BeginScope(string scopeId);
    }
}
