﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Threading.Async;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    class Scope : IScope
    {
        private readonly Container _container;
        private Action<Scope> _disposeCallback;
        private CompositeDisposable _disposable = new CompositeDisposable();

        public Scope(string id, Container container, Action<Scope> disposeCallback)
        {
            Id = id;
            _container = container;
            _disposeCallback = disposeCallback;
        }

        #region IScope

        public string Id { get; }

        public IScope BeginScope(string scopeId)
        {
            var result = new Scope(scopeId, _container, scope => _disposable.Remove(scope));
            _disposable.Add(result);
            return result;
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            if (_disposeCallback != null)
            {
                var temp = _disposeCallback;
                _disposeCallback = null;
                temp(this);
            }
            else
            {
                _disposable?.Dispose();
                _disposable = null;
            }
        }

        #endregion
        
        #region IDiResolver

        public T Resolve<T>(List<object> args)
        {
            return (T) Resolve(typeof(T), args);
        }

        public object Resolve(Type type, List<object> args)
        {
            if (!TryResolve(type, args, out var result))
                throw new CanNotResolveException(type, "Type with scope isn't registered");
            return result;
        }

        public bool TryResolve(Type type, List<object> args, out object instance)
        {
            var result = _container.TryResolve(this, type, args, out instance);
            if (result)
                OnResolve(instance);
            return result;
        }

        public IEnumerable<T> ResolveAll<T>()
        {
            return ResolveAll(typeof(T)).OfType<T>();
        }

        public IEnumerable ResolveAll(Type type)
        {
            var result = _container.ResolveAll(this, type);
            foreach (var o in result)
                OnResolve(o);
            return result;
        }
        
        #endregion

        void OnResolve(object o)
        {
            Add(o as IDisposable);
            //TODO: settigns handling
        }

        public void Add(IDisposable disposable)
        {
            if (disposable != null)
                _disposable.Add(disposable);
        }
    }
}
