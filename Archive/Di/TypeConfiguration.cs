﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    class TypeConfiguration : ITypeConfiguration, IResolverBuilder, IResolver
    {
        private readonly Func<IDiResolver, List<object>, object> _constructorResolver;
        private object _singleInstance;
        private readonly Dictionary<IScope, object> _perScopeInstances = new Dictionary<IScope, object>();

        public TypeConfiguration(Type type, Func<IDiResolver, List<object>, object> constructorResolver, string id)
        {
            InstanceType = type;
            _constructorResolver = constructorResolver;
            Id = id;
        }

        #region Implementation of ITypeConfiguration

        public ITypeConfiguration As<TBaseType>()
        {
            if (!typeof(TBaseType).IsAssignableFrom(InstanceType))
                throw new CanNotResolveException(InstanceType,
                    $"Can not resolve {InstanceType.Name} as {typeof(TBaseType).Name}");
            _resolveTypes.Add(typeof(TBaseType));
            return this;
        }

        public ITypeConfiguration AsSelf()
        {
            _resolveTypes.Add(InstanceType);
            return this;
        }

        public ITypeConfiguration PropertiesAutowired()
        {
            ResolveProperties = true;
            return this;
        }




        public ITypeConfiguration InstancePerDependency()
        {
            _resolvingType = ResolvingScopeType.InstancePerDependency;
            return this;
        }
        public ITypeConfiguration SingleInstance()
        {
            _resolvingType = ResolvingScopeType.SingleInstance;
            return this;
        }
        public ITypeConfiguration InstancePerScope()
        {
            _resolvingType = ResolvingScopeType.InstancePerScope;
            return this;
        }

        #endregion

        #region Implementation of IResolverBuilder

        private Type InstanceType { get; }

        private readonly HashSet<Type> _resolveTypes = new HashSet<Type>();
        public Type[] ResolveTypes => _resolveTypes.Count == 0 ? new[] { InstanceType } : _resolveTypes.ToArray();

        private bool ResolveProperties { get; set; }

        private ResolvingScopeType _resolvingType = ResolvingScopeType.InstancePerDependency;

        public string Id { get; }
        public IResolver Build()
        {
            return this;
        }

        #endregion

        #region Implementation of IResolver

        public object Instantiate(IScope scope, Type requestedType, List<object> args)
        {
            switch (_resolvingType)
            {
                case ResolvingScopeType.SingleInstance:
                    return _singleInstance ?? (_singleInstance = InstantiateImplementation(scope, args));
                case ResolvingScopeType.InstancePerScope:
                    if (!_perScopeInstances.TryGetValue(scope, out var result))
                        _perScopeInstances.Add(scope, result = InstantiateImplementation(scope, args));
                    return result;
                case ResolvingScopeType.InstancePerDependency:
                    return InstantiateImplementation(scope, args);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion

        object InstantiateImplementation(IScope container, List<object> args)
        {
            var result = _constructorResolver(container, args);
            Utils.PropertiesResolverByAttribute(result, args, container);
            if (ResolveProperties)
                Utils.PropertiesResolverBySetters(result, args, container);

            return result;
        }
    }

    [Obsolete("Use New.InjectAttribute", true)]
    class TypeConfiguration<T> : ITypeConfiguration<T>, IResolverBuilder, IResolver
    {
        #region Fields

        private readonly List<Action<IResolveArgs<T>>> _onActivatingActions = new List<Action<IResolveArgs<T>>>();
        private readonly List<Action<IResolveArgs<T>>> _onActivatedActions = new List<Action<IResolveArgs<T>>>();
        private readonly Func<IDiResolver, List<object>, T> _ctorResolver;
        private object _singleInstance;
        private readonly Dictionary<IScope, object> _perScopeInstances = new Dictionary<IScope, object>();

        #endregion

        public TypeConfiguration(Func<IDiResolver, List<object>, T> constructorResolver, string id)
        {
            Id = id;
            _ctorResolver = constructorResolver;
        }

        #region ITypeResolver

        private Type InstanceType => typeof(T);

        private readonly HashSet<Type> _resolveTypes = new HashSet<Type>();
        public Type[] ResolveTypes => _resolveTypes.Count == 0 ? new[] { InstanceType } : _resolveTypes.ToArray();

        private bool ResolveProperties { get; set; }

        private ResolvingScopeType _resolvingType = ResolvingScopeType.InstancePerDependency;

        public string Id { get; private set; }
        
        #endregion

        #region ITypeConfiguration

        public ITypeConfiguration<T> As<TBaseType>()
        {
            if (!typeof(TBaseType).IsAssignableFrom(InstanceType))
                throw new CanNotResolveException(InstanceType,
                    $"Can not resolve {InstanceType.Name} as {typeof(TBaseType).Name}");
            _resolveTypes.Add(typeof(TBaseType));
            return this;
        }

        public ITypeConfiguration<T> AsSelf()
        {
            return As<T>();
        }

        public ITypeConfiguration<T> PropertiesAutowired()
        {
            ResolveProperties = true;
            return this;
        }




        public ITypeConfiguration<T> InstancePerDependency()
        {
            _resolvingType = ResolvingScopeType.InstancePerDependency;
            return this;
        }
        public ITypeConfiguration<T> SingleInstance()
        {
            _resolvingType = ResolvingScopeType.SingleInstance;
            return this;
        }
        public ITypeConfiguration<T> InstancePerScope()
        {
            _resolvingType = ResolvingScopeType.InstancePerScope;
            return this;
        }
        public ITypeConfiguration<T> InstancePerScope(string id)
        {
            _resolvingType = ResolvingScopeType.InstancePerDependency;
            Id = id;
            return this;
        }

        public ITypeConfiguration<T> OnActivating(Action<IResolveArgs<T>> args)
        {
            _onActivatingActions.Add(args);
            return this;
        }

        public ITypeConfiguration<T> OnActivated(Action<IResolveArgs<T>> args)
        {
            _onActivatedActions.Add(args);
            return this;
        }

        public IResolver Build()
        {
            return this;
        }

        #endregion

        #region TypeConfiguration

        object InstantiateImplementation(IScope container, List<object> args)
        {
            var creationArgs = new ResolveArgs<T>(container, _ctorResolver(container, args));
            Utils.PropertiesResolverByAttribute(creationArgs.Instance, args, container);
            if (ResolveProperties)
                Utils.PropertiesResolverBySetters(creationArgs.Instance, args, container);

            foreach (var action in _onActivatingActions)
                action(creationArgs);
            foreach (var action in _onActivatedActions)
                action(creationArgs);

            return creationArgs.Instance;
        }

        public object Instantiate(IScope scope, Type requestedType, List<object> args)
        {
            switch (_resolvingType)
            {
                case ResolvingScopeType.SingleInstance:
                    return _singleInstance ?? (_singleInstance = InstantiateImplementation(scope, args));
                case ResolvingScopeType.InstancePerScope:
                    if (!_perScopeInstances.TryGetValue(scope, out var result))
                        _perScopeInstances.Add(scope, result = InstantiateImplementation(scope, args));
                    return result;
                case ResolvingScopeType.InstancePerDependency:
                    return InstantiateImplementation(scope, args);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}
