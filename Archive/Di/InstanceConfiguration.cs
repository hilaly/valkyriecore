﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    class InstanceConfiguration<T> : IInstanceConfiguration<T>, IResolverBuilder, IResolver
    {
        private readonly List<Action<IResolveArgs<T>>> _onActivatingActions = new List<Action<IResolveArgs<T>>>();
        private readonly List<Action<IResolveArgs<T>>> _onActivatedActions = new List<Action<IResolveArgs<T>>>();

        public string Id { get; private set; }

        private Type InstanceType => typeof(T);

        public bool IsSingleInstance => true;

        private bool ResolveProperties { get; set; }

        private readonly List<Type> _resolveTypes = new List<Type>();
        public Type[] ResolveTypes => _resolveTypes.Count == 0 ? new[] { InstanceType } : _resolveTypes.ToArray();

        public object Instantiate(IScope container, Type requestedType, List<object> args)
        {
            var creationArgs = new ResolveArgs<T>(container, _instance);
            if (ResolveProperties)
            {
                Utils.PropertiesResolverByAttribute(creationArgs.Instance, args, container);
                Utils.PropertiesResolverBySetters(creationArgs.Instance, args, container);
                ResolveProperties = false;
            }

            foreach (var action in _onActivatingActions)
                action(creationArgs);
            foreach (var action in _onActivatedActions)
                action(creationArgs);

            return _instance;
        }

        #region IInstanceConfiguration

        private readonly T _instance;

        public IInstanceConfiguration<T> As<TK>()
        {
            if (!typeof(TK).IsAssignableFrom(InstanceType))
                throw new CanNotResolveException(InstanceType,
                    $"Can not resolve {InstanceType.Name} as {typeof(TK).Name}");
            _resolveTypes.Add(typeof(TK));
            return this;
        }

        public IInstanceConfiguration<T> AsSelf()
        {
            return As<T>();
        }

        public IInstanceConfiguration<T> BuildUp()
        {
            ResolveProperties = true;
            return this;
        }

        public IInstanceConfiguration<T> OnActivating(Action<IResolveArgs<T>> args)
        {
            _onActivatingActions.Add(args);
            return this;
        }

        public IInstanceConfiguration<T> OnActivated(Action<IResolveArgs<T>> args)
        {
            _onActivatedActions.Add(args);
            return this;
        }

        #endregion

        public InstanceConfiguration(T instance, string id)
        {
            _instance = instance;
            Id = id;
        }

        public IResolver Build()
        {
            return this;
        }

        public IInstanceConfiguration<T> InstancePerScope(string id)
        {
            Id = id;
            return this;
        }
    }

}
