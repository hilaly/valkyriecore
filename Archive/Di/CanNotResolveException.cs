﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public class CanNotResolveException : Exception
    {
        public Type NeededType { get; }

        public CanNotResolveException(Type instanceType)
            : base($"Type {instanceType.Name} can not be resolved")
        {
            NeededType = instanceType;
        }

        public CanNotResolveException(Type instanceType, string message)
            : base($"Type {instanceType.Name} can not be resolved: {message}")
        {
            NeededType = instanceType;
        }
    }
}
