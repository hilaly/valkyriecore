﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public interface ITypeConfiguration
    {
        ITypeConfiguration AsSelf();
        ITypeConfiguration As<T>();

        ITypeConfiguration InstancePerDependency();
        ITypeConfiguration SingleInstance();
        ITypeConfiguration InstancePerScope();
    }

    [Obsolete("Use New.InjectAttribute", true)]
    public interface ITypeConfiguration<T>
    {
        ITypeConfiguration<T> AsSelf();
        ITypeConfiguration<T> As<TK>();

        ITypeConfiguration<T> InstancePerDependency();
        ITypeConfiguration<T> SingleInstance();
        ITypeConfiguration<T> InstancePerScope();

        ITypeConfiguration<T> OnActivating(Action<IResolveArgs<T>> args);
        ITypeConfiguration<T> OnActivated(Action<IResolveArgs<T>> args);
    }
}
