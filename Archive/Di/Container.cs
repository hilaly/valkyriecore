﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    internal class Container : IContainer
    {
        private static readonly List<object> EmptyArgs = new List<object>(1) {null};

        public class ResolverData
        {
            public string Id;
            public Type Type;
            public IResolver Resolver;
        }

        private readonly Dictionary<Type, Dictionary<string, List<IResolver>>> _resolvers = new Dictionary<Type, Dictionary<string, List<IResolver>>>();

        public void Init(IEnumerable<ResolverData> resolvers)
        {
            foreach (var e in resolvers)
            {
                if (_resolvers.ContainsKey(e.Type))
                {
                    if (_resolvers[e.Type].ContainsKey(e.Id))
                        _resolvers[e.Type][e.Id].Add(e.Resolver);
                    else
                        _resolvers[e.Type].Add(e.Id, new List<IResolver> { e.Resolver });
                }
                else
                    _resolvers.Add(e.Type,
                        new Dictionary<string, List<IResolver>>() { { e.Id, new List<IResolver> { e.Resolver } } });
            }
        }

        #region Resolvers

        List<object> CreateArgs(List<object> args, IScope scope)
        {
            if (args == null || args.Count == 0)
                return new List<object> {scope};

            if (args.Count > 0 && args[0] != scope)
                args.Insert(0, scope);

            return args;
        }
        
        public bool TryResolve(Scope scope, Type type, List<object> args, out object instance)
        {
            args = CreateArgs(args, scope);
            if (_resolvers.TryGetValue(type, out var list) &&
                list.TryGetValue(ContainerBuilder.DefaultResolverId, out var resolver))
                instance = resolver[0].Instantiate(scope, type, args);
            else if (type.IsGenericType && _resolvers.TryGetValue(type.GetGenericTypeDefinition(), out list) &&
                     list.TryGetValue(ContainerBuilder.DefaultResolverId, out resolver))
                instance = resolver[0].Instantiate(scope, type, args);
            else
                instance = null;

            return instance != null;
        }

        public object[] ResolveAll(Scope scope, Type type)
        {
            object[] result = null;
            EmptyArgs[0] = scope;

            if (_resolvers.TryGetValue(type, out var list))
                result = list.Values
                    .SelectMany(resolver => resolver.Select(u => u.Instantiate(scope, type, EmptyArgs)))
                    .ToArray();
            else
            {
                if (type.IsGenericType && _resolvers.TryGetValue(type.GetGenericTypeDefinition(), out list))
                    result = list.Values.SelectMany(resolver =>
                        resolver.Select(u => u.Instantiate(scope, type, EmptyArgs))).ToArray();
            }

            if (result == null)
                throw new CanNotResolveException(type, "Type with scope isn't registered");

            return result;
        }

        #endregion

        #region IContaner

        public IScope BeginScope(string scopeId)
        {
            return new Scope(scopeId, this, null);
        }

        #endregion
    }

}
