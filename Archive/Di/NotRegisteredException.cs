﻿using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    public class NotRegisteredException : Exception
    {
        public Type NeededType { get; }

        public NotRegisteredException(Type instanceType)
            : base($"Type {instanceType.Name} not registered")
        {
            NeededType = instanceType;
        }
    }
}
