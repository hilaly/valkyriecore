using System;

namespace Valkyrie.Di
{
    [Obsolete("Use New.InjectAttribute", true)]
    enum ResolvingScopeType
    {
        InstancePerDependency,
        InstancePerScope,
        SingleInstance,
    }
}