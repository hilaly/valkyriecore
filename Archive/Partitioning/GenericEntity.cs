namespace Valkyrie.Math.Space.Partitioning
{
    class GenericEntity<TType, TId> : Entity<GenericEntity<TType, TId>, TType, TId>
    {
        public GenericEntity(TId id, TType type, Vector position, IWorld<GenericEntity<TType, TId>, TType, TId> world) : base(id, type, position, world)
        {
        }
    }
}