﻿using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space.Partitioning
{
    class Entity<T, TType, TId> : IWorldObject<T, TType, TId>
        where T : class, IWorldObject<T, TType, TId>
    {
        private readonly TId _id;
        private readonly TType _type;
        private Vector _position;
        
        private readonly Subject<WorldObjectPositionMessage<T, TType, TId>> _positionChannel;
        private readonly Subject<WorldObjectDisposedMessage<T, TType, TId>> _disposedChannel;
        private readonly Subject<WorldObjectEventMessage<T, TType, TId>> _eventChannel;

        #region Implementation of ICollectable<out TType,out TId>

        public TId GetEntityId()
        {
            return _id;
        }

        public TType GetEntityType()
        {
            return _type;
        }

        #endregion

        #region Implementation of IWorldObject<T,TType,TId>

        public IObservable<WorldObjectPositionMessage<T, TType, TId>> PositionChannel
        {
            get { return _positionChannel; }
        }

        public IObservable<WorldObjectDisposedMessage<T, TType, TId>> DisposedChannel
        {
            get { return _disposedChannel; }
        }

        public IObservable<WorldObjectEventMessage<T, TType, TId>> EventChannel
        {
            get { return _eventChannel; }
        }

        public IWorld<T, TType, TId> World { get; private set; }
        public IRegion<T, TType, TId> Region { get; private set; }

        public Vector Position
        {
            get { return _position; }
            set
            {
                _position = value;
                UpdateInterestManagement();
            }
        }

        #endregion

        public Entity(TId id, TType type, Vector position, IWorld<T, TType, TId> world)
        {
            _id = id;
            _type = type;
            _position = position;
            World = world;

            _disposedChannel = new Subject<WorldObjectDisposedMessage<T, TType, TId>>();
            _positionChannel = new Subject<WorldObjectPositionMessage<T, TType, TId>>();
            _eventChannel = new Subject<WorldObjectEventMessage<T, TType, TId>>();

            //UpdateInterestManagement();
        }

        public void SharePosition()
        {
            var region = World.GetRegion(Position);
            _positionChannel.OnNext(GetPositionMessage(Position, region));
        }

        private void UpdateInterestManagement()
        {
            var region = World.GetRegion(Position);

            _positionChannel.OnNext(GetPositionMessage(Position, region));

            if (SetCurrentRegion(region))
            {
                //PositionChannel.Publish(GetPositionMessage(Position, region));
                region.Send(new WorldObjectSnapshot<T, TType, TId> { Source = this as T });
            }
        }

        private bool SetCurrentRegion(IRegion<T, TType, TId> region)
        {
            if (region == null)
            {
                if (Region == null)
                    return false;
                Region = null;
                _currentRegionSubscription.Dispose();
                _currentRegionSubscription = null;
                return false;
            }

            if (Region == null)
            {
                _currentRegionSubscription = region.Subscribe(Region_OnMessage, SubscriptionClose);
                Region = region;
                return true;
            }

            if (ReferenceEquals(region, Region))
                return false;

            var subscription = region.Subscribe(Region_OnMessage, SubscriptionClose);
            _currentRegionSubscription.Dispose();
            _currentRegionSubscription = subscription;
            Region = region;
            return true;
        }

        private void Region_OnMessage(WorldRegionMessage<T, TType, TId> obj)
        {
            obj.OnObjectReceive(this as T);
        }

        protected WorldObjectPositionMessage<T, TType, TId> GetPositionMessage(Vector vector, IRegion<T, TType, TId> region)
        {
            return new WorldObjectPositionMessage<T, TType, TId> { Source = this as T };
        }

        #region Region Subscription

        private IDisposable _currentRegionSubscription;
        
        public void SubscriptionClose()
        {
        }

        #endregion

        public void Dispose()
        {
            OnDestroy();

            SetCurrentRegion(null);
            _disposedChannel.OnNext(new WorldObjectDisposedMessage<T, TType, TId>(this as T));
            
            _eventChannel.OnCompleted();
            _disposedChannel.OnCompleted();
            _positionChannel.OnCompleted();
            
            _eventChannel.Dispose();
            _disposedChannel.Dispose();
            _positionChannel.Dispose();
        }

        protected virtual void OnDestroy()
        {
        }

    }
}