﻿namespace Valkyrie.Math.Space.Partitioning
{
    class WorldObjectDisposedMessage<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        public WorldObjectDisposedMessage(T worldObject)
        {
            Source = worldObject;
        }

        public T Source { get; set; }
    }
}