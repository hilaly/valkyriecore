using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space.Partitioning
{
    class InterestArea<T, TType, TId> : IInterestArea<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        #region Implementation of IInterestArea<T,TType,TId>

        public IInterestAreaFilter<T, TType, TId> Filter { get; set; }

        #region Size of interest area

        public Vector ViewDistanceEnter { get; set; }
        public Vector ViewDistanceExit { get; set; }

        #endregion

        public T AttachedObject { get; private set; }

        public void Attach(T entity)
        {
            if (AttachedObject.Equals(entity))
                return;

            if (AttachedObject != null)
                throw new Exception("Invalid attach");

            AttachedObject = entity;
            SetPosition(entity.Position);

            if (AutoSubscribeEntity(entity) == false)
            {
                WorldObjectAutoSubscription<T, TType, TId> autoSubsc;
                if (_autoManagedEntitySubscriptions.TryGetValue(entity, out autoSubsc))
                    AutoUnsubscribeEntity(autoSubsc);
            }

            var disposeSubscription = entity.DisposedChannel.Subscribe(AttachedEntity_OnDispose, SubscriptionClose);
            var positionSubscription = entity.PositionChannel.Subscribe(AttachedEntity_OnPosition, SubscriptionClose);
            var eventSubscription = entity.EventChannel.Subscribe(AttachedEntity_OnError, SubscriptionClose);

            ICompositeDisposable tempDisposable = new CompositeDisposable();
            tempDisposable.Add(disposeSubscription);
            tempDisposable.Add(positionSubscription);
            tempDisposable.Add(eventSubscription);
            _attachedEntitySubscription = tempDisposable;
        }

        public void Detach()
        {
            if (AttachedObject == null)
                return;

            _attachedEntitySubscription.Dispose();
            _attachedEntitySubscription = null;

            var ent = AttachedObject;
            AttachedObject = default(T);

            if (!_autoManagedEntitySubscriptions.ContainsKey(ent) && AutoSubscribeEntity(ent))
                ReceiveItemSnapshot(new WorldObjectSnapshot<T, TType, TId> {Source = ent});
        }


        #region Events for subscribers

        public event Action<T, Vector> OnFindEntity = (entity, position) => { };
        public event Action<T, Vector> OnEntityMove = (entity, position) => { };
        public event Action<T> OnLostEntity = entity => { };

        #endregion

        #endregion

        #region Calls for events

        protected virtual void OnEntitySubscribed(WorldObjectSnapshot<T, TType, TId> message)
        {
            OnFindEntity(message.Source, message.Source.Position);
        }

        protected virtual void OnEntityUnsubscribed(T entity)
        {
            OnLostEntity(entity);
        }

        protected virtual void OnEntityPosition(WorldObjectPositionMessage<T, TType, TId> message)
        {
            OnEntityMove(message.Source, message.Position);
        }

        #endregion

        #region Private fields

        private IDisposable _attachedEntitySubscription;

        private BoundingBox _regionInnerFocus;
        private BoundingBox _regionOuterFocus;

        #endregion

        #region Create/Destroy

        public InterestArea()
        {
            _snapShotRequest = new WorldObjectSnapshotRequest<T, TType, TId>(this);
        }

        public void Dispose()
        {
            if (AttachedObject != null)
            {
                _attachedEntitySubscription.Dispose();
                _attachedEntitySubscription = null;
                AttachedObject = default(T);
            }

            foreach (var disposable in _regionSubscriptions.Values)
            {
                disposable.Dispose();
            }
            _regionSubscriptions.Clear();
            foreach (var subscription in _autoManagedEntitySubscriptions.Values)
            {
                subscription.Dispose();
            }
            _autoManagedEntitySubscriptions.Clear();
        }

        public void Init(IWorld<T, TType, TId> world)
        {
            World = world;

            _regionInnerFocus = new BoundingBox {Max = World.Area.Min, Min = World.Area.Max};
            _regionOuterFocus = _regionInnerFocus;

            ViewDistanceEnter = Vector.Zero;
            ViewDistanceExit = Vector.Zero;
        }

        #endregion

        public IWorld<T, TType, TId> World { get; private set; }

        public Vector Position { get; private set; }

        private void AttachedEntity_OnError(WorldObjectEventMessage<T, TType, TId> obj)
        {
            obj.OnAreaReceived(this);
        }

        private void AttachedEntity_OnPosition(WorldObjectPositionMessage<T, TType, TId> obj)
        {
            if (AttachedObject.Equals(obj.Source))
                SetPosition(obj.Position);
        }

        private void UpdateInterestManagement()
        {
            var focus = BoundingBox.CreateFromPoints(Position - ViewDistanceExit,
                Position + ViewDistanceExit);
            var outerFocus = focus.IntersectWith(World.Area);

            focus = BoundingBox.CreateFromPoints(Position - ViewDistanceEnter,
                Position + ViewDistanceEnter);
            var innerFocus = focus.IntersectWith(World.Area);

            innerFocus = World.GetRegionAlignedBoundingBox(innerFocus);
            if (!innerFocus.Equals(_regionInnerFocus))
            {
                if (innerFocus.IsValid())
                {
                    var regions = _regionInnerFocus.IsValid()
                        ? World.GetRegionsExcept(innerFocus, _regionInnerFocus)
                        : World.GetRegions(innerFocus);
                    SubscribeRegions(regions.OfType<IRegion<T, TType, TId>>());
                }

                _regionInnerFocus = innerFocus;
            }

            outerFocus = World.GetRegionAlignedBoundingBox(outerFocus);
            if (outerFocus.Equals(_regionOuterFocus))
                return;
            if (outerFocus.IsValid())
            {
                var regions = _regionOuterFocus.IsValid()
                    ? World.GetRegionsExcept(_regionOuterFocus, outerFocus).OfType<IRegion<T, TType, TId>>()
                    : _regionSubscriptions.Keys.Where(r => !outerFocus.Contains(r.Coordinate)).ToArray();

                UnsubscribeRegions(regions);
            }

            _regionOuterFocus = outerFocus;
        }

        private void AttachedEntity_OnDispose(WorldObjectDisposedMessage<T, TType, TId> obj)
        {
            if (obj.Source.Equals(AttachedObject))
                Detach();
        }

        public void SubscriptionClose()
        {
        }

        public void SetPosition(Vector position)
        {
            Position = position;
            UpdateInterestManagement();
        }

        #region Region subscriptions

        private readonly WorldObjectSnapshotRequest<T, TType, TId> _snapShotRequest;

        private readonly Dictionary<IRegion<T, TType, TId>, IDisposable> _regionSubscriptions =
            new Dictionary<IRegion<T, TType, TId>, IDisposable>();

        private readonly Dictionary<T, WorldObjectAutoSubscription<T, TType, TId>> _autoManagedEntitySubscriptions =
            new Dictionary<T, WorldObjectAutoSubscription<T, TType, TId>>();

        protected IEnumerable<T> ViewedObjects { get { return _autoManagedEntitySubscriptions.Keys; } }

        private void SubscribeRegions(IEnumerable<IRegion<T, TType, TId>> regions)
        {
            foreach (var region in regions)
            {
                if (_regionSubscriptions.ContainsKey(region))
                    continue;
                var subscription = region.Subscribe(Region_OnMessage, SubscriptionClose);
                _regionSubscriptions.Add(region, subscription);
                region.Send(_snapShotRequest, this);
                //Console.WriteLine("Subscrive region {0} at {1}", region, region.Coordinate);
            }
        }

        private void UnsubscribeRegions(IEnumerable<IRegion<T, TType, TId>> regions)
        {
            var enumerable = regions as Region<T, TType, TId>[] ?? regions.ToArray();
            foreach (var region in enumerable)
            {
                IDisposable subscription;
                if (!_regionSubscriptions.TryGetValue(region, out subscription)) continue;
                subscription.Dispose();
                //Console.WriteLine("Unubscrive region {0} at {1}", region, region.Coordinate);
                _regionSubscriptions.Remove(region);
            }

            var itemSubscriptions = _autoManagedEntitySubscriptions.Values.Where(i => enumerable.Contains(i.WorldRegion)).ToList();
            foreach (var subscription in itemSubscriptions)
            {
                AutoUnsubscribeEntity(subscription);
            }
        }

        private void Region_OnMessage(WorldRegionMessage<T, TType, TId> obj)
        {
            obj.OnInterestAreaReceive(this);
        }

        #endregion

        #region Subscribed entities callbacks

        private void AutoSubscribedEntity_OnPosition(WorldObjectPositionMessage<T, TType, TId> message)
        {
            WorldObjectAutoSubscription<T, TType, TId> subscription;

            if (false == _autoManagedEntitySubscriptions.TryGetValue(message.Source, out subscription))
                return;

            subscription.EntityPosition = message.Position;

            if (message.Region == null)
            {
                AutoUnsubscribeEntity(subscription);
                return;
            }

            OnEntityPosition(message);

            if (ReferenceEquals(message.Region, subscription.WorldRegion))
            {
                return;
            }

            subscription.WorldRegion = message.Region;

            if (_regionSubscriptions.ContainsKey(subscription.WorldRegion) == false)
            {
                AutoUnsubscribeEntity(subscription);
            }
        }

        private void AutoSubscribedEntity_OnDispose(WorldObjectDisposedMessage<T, TType, TId> obj)
        {
            WorldObjectAutoSubscription<T, TType, TId> subscription;
            if (_autoManagedEntitySubscriptions.TryGetValue(obj.Source, out subscription))
                AutoUnsubscribeEntity(subscription);
        }

        private void AutoUnsubscribeEntity(WorldObjectAutoSubscription<T, TType, TId> subscription)
        {
            subscription.Dispose();
            _autoManagedEntitySubscriptions.Remove(subscription.WorldObject);
            OnEntityUnsubscribed(subscription.WorldObject);
        }

        #endregion

        bool AutoSubscribeEntity(T entity)
        {
            return !entity.Equals(AttachedObject) && ((Filter == null) || Filter.AutoSubscribeEntity(entity));
        }

        public void ReceiveItemSnapshot(WorldObjectSnapshot<T, TType, TId> message)
        {
            if (AutoSubscribeEntity(message.Source) == false)
                return;

            WorldObjectAutoSubscription<T, TType, TId> subscription;

            if (_autoManagedEntitySubscriptions.TryGetValue(message.Source, out subscription))
            {
                if (message.Source.Region == null)
                {
                    AutoUnsubscribeEntity(subscription);
                    return;
                }
                subscription.EntityPosition = message.Source.Position;
                subscription.WorldRegion = message.Source.Region;
                return;
            }

            if (message.Source.Region == null || _regionSubscriptions.ContainsKey(message.Source.Region) == false)
                return;

            var disposeListener = message.Source.DisposedChannel.Subscribe(AutoSubscribedEntity_OnDispose,
                SubscriptionClose);
            var positionListener = message.Source.PositionChannel.Subscribe(AutoSubscribedEntity_OnPosition,
                SubscriptionClose);

            var tempDisposable = new CompositeDisposable(disposeListener, positionListener);

            var entSubscription = new WorldObjectAutoSubscription<T, TType, TId>(message.Source, message.Source.Position,
                message.Source.Region, tempDisposable);
            _autoManagedEntitySubscriptions.Add(message.Source, entSubscription);

            OnEntitySubscribed(message);
        }
    }
}