namespace Valkyrie.Math.Space.Partitioning
{
    class WorldObjectSnapshotRequest<T, TType, TId> : WorldRegionMessage<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        public IInterestArea<T, TType, TId> Source { get; private set; }

        public WorldObjectSnapshotRequest(IInterestArea<T, TType, TId> interestArea)
        {
            Source = interestArea;
        }

        public override void OnInterestAreaReceive(IInterestArea<T, TType, TId> interestArea)
        {
        }

        public override void OnObjectReceive(T entity)
        {
            var snapshot = new WorldObjectSnapshot<T, TType, TId> { Source = entity };

            ((InterestArea<T, TType, TId>)Source).ReceiveItemSnapshot(snapshot);
        }
    }
}