namespace Valkyrie.Math.Space.Partitioning
{
    interface IInterestAreaFilter<in T, TType, TId> where T : IWorldObject<T, TType, TId>
    {
        bool AutoSubscribeEntity(T entity);
    }
}