using System;

namespace Valkyrie.Math.Space.Partitioning
{
    interface IInterestArea<T, TType, TId> : IDisposable where T : IWorldObject<T, TType, TId>
    {
        #region Filter for view about entities

        IInterestAreaFilter<T, TType, TId> Filter { get; set; }

        #endregion

        #region Size of interest area

        Vector ViewDistanceEnter { get; set; }
        Vector ViewDistanceExit { get; set; }

        #endregion

        #region Entity Attach/Detach

        T AttachedObject { get; }
        void Attach(T entity);
        void Detach();

        #endregion

        #region Events for subscribers

        event Action<T, Vector> OnFindEntity;
        event Action<T, Vector> OnEntityMove;
        event Action<T> OnLostEntity;

        #endregion

    }
}