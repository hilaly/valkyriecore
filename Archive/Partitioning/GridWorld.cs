﻿using System;
using System.Collections.Generic;
using Valkyrie.Math.Space.Partitioning.Collections;

// ReSharper disable ImpureMethodCallOnReadonlyValueField

namespace Valkyrie.Math.Space.Partitioning
{
    class GridWorld<T, TType, TId> : IWorld<T, TType, TId>, IDisposable
        where T : IWorldObject<T, TType, TId>
    {
        #region Constants and Fields

        private readonly ICollection<T, TType, TId> _entities;

        private readonly BoundingBox _rectangleArea;

        private readonly Vector _tileDimensions;

        private readonly Vector _tileSize;

        private readonly Region<T, TType, TId>[][][] _worldRegions;

        #endregion

        #region Constructors and Destructors

        public GridWorld(Vector corner1, Vector corner2, Vector tileDimensions, ICollection<T, TType, TId> itemCache)
            : this(BoundingBox.CreateFromPoints(corner1, corner2), tileDimensions, itemCache)
        {
        }

        public GridWorld(BoundingBox boundingBox, Vector tileDimensions, ICollection<T, TType, TId> entities)
        {
            // 3D grid
            _rectangleArea = new BoundingBox
                {
                    Min = new Vector { X = boundingBox.Min.X, Y = boundingBox.Min.Y, Z = boundingBox.Min.Z },
                    Max = new Vector { X = boundingBox.Max.X, Y = boundingBox.Max.Y, Z = boundingBox.Max.Z }
                };

            var size = new Vector { X = boundingBox.Max.X - boundingBox.Min.X + 1, Y = boundingBox.Max.Y - boundingBox.Min.Y + 1, Z = boundingBox.Max.Z - boundingBox.Min.Z + 1 };
            if (tileDimensions.X <= 0)
                tileDimensions.X = size.X;
            if (tileDimensions.Y <= 0)
                tileDimensions.Y = size.Y;
            if (tileDimensions.Z <= 0)
                tileDimensions.Z = size.Z;


            _tileDimensions = tileDimensions;
            _tileSize = new Vector { X = tileDimensions.X - 1, Y = tileDimensions.Y - 1, Z = tileDimensions.Z - 1 };
            _entities = entities;

            var regionsX = (int)System.Math.Ceiling(size.X / (double)tileDimensions.X);
            var regionsY = (int)System.Math.Ceiling(size.Y / (double)tileDimensions.Y);
            var regionsZ = (int) System.Math.Ceiling(size.Z/(double) tileDimensions.Z);

            _worldRegions = new Region<T, TType, TId>[regionsX][][];
            var current = boundingBox.Min;
            for (var x = 0; x < regionsX; x++)
            {
                _worldRegions[x] = new Region<T, TType, TId>[regionsY][];
                for (var y = 0; y < regionsY; y++)
                {
                    _worldRegions[x][y] = new Region<T, TType, TId>[regionsZ];
                    for(var z = 0; z < regionsZ; z++)
                    {
                        _worldRegions[x][y][z] = new Region<T, TType, TId>(current);
                        current.Z += tileDimensions.Z;
                    }

                    current.Y += tileDimensions.Y;
                    current.Z = boundingBox.Min.Z;
                }

                current.X += tileDimensions.X;
                current.Y = boundingBox.Min.Y;
            }
        }

        ~GridWorld()
        {
            Dispose(false);
        }

        #endregion

        #region Properties

        public BoundingBox Area
        {
            get
            {
                return _rectangleArea;
            }
        }

        public ICollection<T, TType, TId> Entities
        {
            get
            {
                return _entities;
            }
        }

        public Vector TileDimensions
        {
            get
            {
                return _tileDimensions;
            }
        }

        #endregion

        #region Implemented Interfaces

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region IWorld

        public Region<T, TType, TId> GetRegion(Vector position)
        {
            if (_rectangleArea.Contains(position))
            {
                return GetRegionAt(position);
            }

            return null;
        }

        public BoundingBox GetRegionAlignedBoundingBox(BoundingBox area)
        {
            area = _rectangleArea.IntersectWith(area);
            if (area.IsValid())
            {
                var result = new BoundingBox { Min = GetRegionAt(area.Min).Coordinate, Max = GetRegionAt(area.Max).Coordinate + _tileSize };
                return result;
            }

            return area;
        }

        public HashSet<Region<T, TType, TId>> GetRegions(BoundingBox area)
        {
            return new HashSet<Region<T, TType, TId>>(GetRegionEnumerableNewImpl(area));
        }

        public HashSet<Region<T, TType, TId>> GetRegionsExcept(BoundingBox area, BoundingBox except)
        {
            var result = new HashSet<Region<T, TType, TId>>();

            // min x
            if (area.Min.X < except.Min.X)
            {
                // get all left to except
                var box = new BoundingBox
                {
                    Min = area.Min,
                    Max = new Vector {X = System.Math.Min(area.Max.X, except.Min.X - 1), Y = area.Max.Y, Z = area.Max.Z}
                };
                result.UnionWith(GetRegionEnumerable(box));
            }
            // min y
            if (area.Min.Y < except.Min.Y)
            {
                // get all above except
                var box = new BoundingBox
                {
                    Min = area.Min,
                    Max = new Vector {X = area.Max.X, Y = System.Math.Min(area.Max.Y, except.Min.Y - 1), Z = area.Max.Z}
                };
                result.UnionWith(GetRegionEnumerable(box));
            }
            // min z
            if (area.Min.Z < except.Min.Z)
            {
                var box = new BoundingBox
                {
                    Min = area.Min,
                    Max = new Vector {X = area.Max.X, Y = area.Max.Y, Z = System.Math.Min(area.Max.Z, except.Min.Z - 1)}
                };
                result.UnionWith(GetRegionEnumerable(box));
            }

            // max x
            if (area.Max.X > except.Max.X)
            {
                // get all right to except
                var box = new BoundingBox
                {
                    Min = new Vector {X = System.Math.Max(area.Min.X, except.Max.X + 1), Y = area.Min.Y, Z = area.Min.Z},
                    Max = area.Max
                };
                result.UnionWith(GetRegionEnumerable(box));
            }
            // max y
            if (area.Max.Y > except.Max.Y)
            {
                // get all below except
                var box = new BoundingBox
                {
                    Min = new Vector { X = area.Min.X, Y = System.Math.Max(area.Min.Y, except.Max.Y + 1), Z = area.Min.Z},
                    Max = area.Max
                };
                result.UnionWith(GetRegionEnumerable(box));
            }
            // max z
            if (area.Max.Z > except.Max.Z)
            {
                var box = new BoundingBox
                {
                    Min = new Vector {X = area.Min.X, Y = area.Min.Y, Z = System.Math.Max(area.Min.Z, except.Max.Z + 1)},
                    Max = area.Max
                };
                result.UnionWith(GetRegionEnumerable(box));
            }

            return result;
        }

        #endregion

        #endregion

        #region Methods

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            foreach (var regionsOfRegions in _worldRegions)
                foreach (var regions in regionsOfRegions)
                    foreach (var region in regions)
                        region.Dispose();
        }

        private Region<T, TType, TId> GetRegionAt(Vector coordinate)
        {
            var relativePoint = coordinate - _rectangleArea.Min;
            var indexX = relativePoint.X/_tileDimensions.X;
            var indexY = relativePoint.Y/_tileDimensions.Y;
            var indexZ = relativePoint.Z/_tileDimensions.Z;
            return _worldRegions[indexX][indexY][indexZ];
        }

        private IEnumerable<Region<T, TType, TId>> GetRegionEnumerableNewImpl(BoundingBox area)
        {
            var overlap = _rectangleArea.IntersectWith(area);

            var relativeMinPoint = overlap.Min - _rectangleArea.Min;
            var minXindex = relativeMinPoint.X / _tileDimensions.X;
            var minYindex = relativeMinPoint.Y / _tileDimensions.Y;
            var minZindex = relativeMinPoint.Z / _tileDimensions.Z;

            var relativeMaxPoint = overlap.Max - _rectangleArea.Min;
            var maxXindex = relativeMaxPoint.X / _tileDimensions.X;
            var maxYindex = relativeMaxPoint.Y / _tileDimensions.Y;
            var maxZindex = relativeMaxPoint.Z / _tileDimensions.Z;

            for(var x = minXindex; x <= maxXindex; ++x)
                for(var y = minYindex; y <= maxYindex; ++y)
                    for (var z = minZindex; z <= maxZindex; z++)
                        yield return _worldRegions[x][y][z];
        }

        private IEnumerable<Region<T, TType, TId>> GetRegionEnumerable(BoundingBox area)
        {
            var overlap = _rectangleArea.IntersectWith(area);

            var current = overlap.Min;
            while (current.Y <= overlap.Max.Y)
            {
                foreach (var region in GetRegionsForY(overlap, current))
                    yield return region;
                current.Y += _tileDimensions.Y;
            }

            if (current.Y <= overlap.Max.Y)
                yield break;
            current.Y = overlap.Max.Y;
            foreach (var region in GetRegionsForY(overlap, current))
                yield return region;
        }

        private IEnumerable<Region<T, TType, TId>> GetRegionsForY(BoundingBox overlap, Vector current)
        {
            current.X = overlap.Min.X;
            while (current.X <= overlap.Max.X)
            {
                foreach (var region in GetRegionsForZ(overlap, current))
                    yield return region;
                current.X += _tileDimensions.X;
            }

            if (current.X <= overlap.Max.X)
                yield break;
            current.X = overlap.Max.X;
            foreach (var region in GetRegionsForZ(overlap, current))
                yield return region;
        }

        private IEnumerable<Region<T, TType, TId>> GetRegionsForZ(BoundingBox overlap, Vector current)
        {
            current.Z = overlap.Min.Z;
            while (current.Z <= overlap.Max.Z)
            {
                yield return GetRegionAt(current);
                current.Z += _tileDimensions.Z;
            }

            if (current.Z <= overlap.Max.Z)
                yield break;
            current.Z = overlap.Max.Z;
            yield return GetRegionAt(current);
        }

        #endregion
    }
}