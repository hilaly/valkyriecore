using System;
using Valkyrie.Math.Space.Partitioning.Collections;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space.Partitioning
{
    interface IWorldObject<T, TType, TId> : ICollectable<TType, TId>, IDisposable
        where T : IWorldObject<T, TType, TId>
    {
        IObservable<WorldObjectDisposedMessage<T, TType, TId>> DisposedChannel { get; }
        IObservable<WorldObjectPositionMessage<T, TType, TId>> PositionChannel { get; }
        IObservable<WorldObjectEventMessage<T, TType, TId>> EventChannel { get; }

        IWorld<T, TType, TId> World { get; }
        IRegion<T, TType, TId> Region { get; }

        Vector Position { get; set; }
    }
}