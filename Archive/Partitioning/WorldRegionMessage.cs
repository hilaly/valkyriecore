﻿namespace Valkyrie.Math.Space.Partitioning
{
    abstract class WorldRegionMessage<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        public abstract void OnInterestAreaReceive(IInterestArea<T, TType, TId> interestArea);

        public abstract void OnObjectReceive(T entity);
    }
}