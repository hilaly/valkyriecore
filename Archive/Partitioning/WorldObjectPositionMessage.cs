﻿namespace Valkyrie.Math.Space.Partitioning
{
    class WorldObjectPositionMessage<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        public T Source { get; set; }

        public Vector Position { get { return Source.Position; } }

        public IRegion<T, TType, TId> Region { get { return Source.Region; } }
    }
}