﻿using Valkyrie.Math.Space.Partitioning.Collections;

namespace Valkyrie.Math.Space.Partitioning
{
    class Space<TType, TId>
    {
        private readonly IWorld<GenericEntity<TType, TId>, TType, TId> _world;
        private DefaultEntityFactory<GenericEntity<TType, TId>, TType, TId> _entityFactory;

        public IWorld<GenericEntity<TType, TId>, TType, TId> World { get { return _world; } }

        public Space(BoundingBox bounding, Vector tileSize, ICollection<GenericEntity<TType, TId>, TType, TId> entities)
        {
            _world = new GridWorld<GenericEntity<TType, TId>, TType, TId>(bounding, tileSize, entities);
        }

        public GenericEntity<TType, TId> Create(TType type, TId id, Vector position)
        {
            var result = new GenericEntity<TType, TId>(id, type, position, _world);
            _entityFactory.CreateEntity(result, position);
            return result;
        }
    }
}
