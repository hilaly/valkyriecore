namespace Valkyrie.Math.Space.Partitioning
{
    class WorldObjectSnapshot<T, TType, TId> : WorldRegionMessage<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        public T Source { get; set; }

        public override string ToString()
        {
            return string.Format("{0}(Source {1}, {2}, {3})", GetType(), Source.GetEntityId(), Source.Region, Source.Position);
        }

        public override void OnInterestAreaReceive(IInterestArea<T, TType, TId> interestArea)
        {
            ((InterestArea<T, TType, TId>)interestArea).ReceiveItemSnapshot(this);
        }

        public override void OnObjectReceive(T entity)
        {
        }
    }
}