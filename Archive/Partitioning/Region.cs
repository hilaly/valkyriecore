﻿using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space.Partitioning
{
    class Region<T, TType, TId> : IRegion<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        #region Constants and Fields

        private readonly Vector _coordinate;

        private readonly int _hashCode;

        #endregion

        #region Constructors and Destructors

        public Region(Vector coordinate)
        {
            _coordinate = coordinate;
            _hashCode = _coordinate.GetHashCode();
            _innerChannel = new Subject<WorldRegionMessage<T, TType, TId>>();
        }

        #endregion

        #region Properties

        public Vector Coordinate
        {
            get
            {
                return _coordinate;
            }
        }

        #endregion

        #region Public Methods

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _hashCode;
        }

        #endregion

        private readonly Subject<WorldRegionMessage<T, TType, TId>> _innerChannel;

        #region Implementation of IPublisher<in WorldRegionMessage>

        public void Send(WorldRegionMessage<T, TType, TId> message)
        {
            _innerChannel.OnNext(message);
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            _innerChannel.Dispose();
        }

        #endregion

        public IDisposable Subscribe(IObserver<WorldRegionMessage<T, TType, TId>> observable)
        {
            return _innerChannel.Subscribe(observable);
        }

        public void Send(WorldRegionMessage<T, TType, TId> snapShotRequest, InterestArea<T, TType, TId> interestArea)
        {
            //TODO: think about interest area
            _innerChannel.OnNext(snapShotRequest);
        }
    }
}