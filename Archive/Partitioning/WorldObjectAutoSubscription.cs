using System;

namespace Valkyrie.Math.Space.Partitioning
{
    class WorldObjectAutoSubscription<T, TType, TId> : IDisposable
        where T : IWorldObject<T, TType, TId>
    {
        private readonly IDisposable _subscription;

        public T WorldObject { get; private set; }
        public Vector EntityPosition { get; set; }
        public IRegion<T, TType, TId> WorldRegion { get; set; }

        public WorldObjectAutoSubscription(T worldObject, Vector position, IRegion<T, TType, TId> worldRegion, IDisposable subscription)
        {
            _subscription = subscription;
            WorldObject = worldObject;
            EntityPosition = position;
            WorldRegion = worldRegion;
        }

        public void Dispose()
        {
            _subscription.Dispose();
        }
    }
}