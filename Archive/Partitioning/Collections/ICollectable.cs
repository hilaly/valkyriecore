﻿namespace Valkyrie.Math.Space.Partitioning.Collections
{
    public interface ICollectable<out TType, out TId>
    {
        TId GetEntityId();
        TType GetEntityType();
    }
}
