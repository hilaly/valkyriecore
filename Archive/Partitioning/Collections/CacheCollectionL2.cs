﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Valkyrie.Threading.Sync;

namespace Valkyrie.Math.Space.Partitioning.Collections
{
    class CacheCollectionL2<T, TType, TId> : IEnumerable<T>
        where T : ICollectable<TType, TId>
    {

        readonly Dictionary<TId, T> _entities = new Dictionary<TId, T>();
        readonly IReaderWriterLock _synchro;

        public CacheCollectionL2(LockRecursionPolicy policy)
        {
            _synchro = Lock.Create(policy);
        }

        public bool Add(T entity)
        {
            using (_synchro.LockWrite())
            {
                if (_entities.ContainsKey(entity.GetEntityId()))
                    return false;
                _entities.Add(entity.GetEntityId(), entity);
                return true;
            }
        }

        public bool Remove(TId entityId)
        {
            using (_synchro.LockWrite())
            {
                return _entities.Remove(entityId);
            }
        }

        public bool TryGetValue(TId entityId, out T entity)
        {
            using (_synchro.LockRead())
            {
                return _entities.TryGetValue(entityId, out entity);
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _entities.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}