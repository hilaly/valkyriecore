using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Valkyrie.Threading.Sync;

namespace Valkyrie.Math.Space.Partitioning.Collections
{
    class CacheCollection<T, TType, TId> : ICollection<T, TType, TId>
        where T : ICollectable<TType, TId>
    {
        readonly Dictionary<TType, CacheCollectionL2<T, TType, TId>> _itemCaches = new Dictionary<TType, CacheCollectionL2<T, TType, TId>>();
        readonly IReaderWriterLock _synchro;
        private readonly LockRecursionPolicy _policy;

        public CacheCollection(LockRecursionPolicy policy, IReaderWriterLock synchro)
        {
            _policy = policy;
            _synchro = synchro;
        }

        CacheCollectionL2<T, TType, TId> GetLevel2Cache(TType entityType)
        {
            using (var cLock = _synchro.LockRead())
            {
                CacheCollectionL2<T, TType, TId> result;
                if (_itemCaches.TryGetValue(entityType, out result))
                    return result;

                result = new CacheCollectionL2<T, TType, TId>(_policy);
                using (cLock.LockWrite())
                {
                    _itemCaches.Add(entityType, result);
                }
                return result;
            }
        }

        public bool TryGetValue(TType entityType, TId entityId, out T entity)
        {
            var cache = GetLevel2Cache(entityType);
            return cache.TryGetValue(entityId, out entity);
        }

        public bool Add(T entity)
        {
            var cache = GetLevel2Cache(entity.GetEntityType());
            return cache.Add(entity);
        }

        public bool Remove(TType entityType, TId entityId)
        {
            var cache = GetLevel2Cache(entityType);
            return cache.Remove(entityId);
        }

        public T Get(TId entityId)
        {
            foreach (var cache in _itemCaches.Values)
            {
                T entity;
                if (cache.TryGetValue(entityId, out entity))
                    return entity;
            }
            return default(T);
        }

        public IEnumerable<T> GetAll(TType entityType)
        {
            return GetLevel2Cache(entityType);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _itemCaches.Values.SelectMany(entityCacheL2 => entityCacheL2).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}