using System.Collections.Generic;

namespace Valkyrie.Math.Space.Partitioning.Collections
{
    public interface ICollection<T, in TType, in TId> : IEnumerable<T>
        where T : ICollectable<TType, TId>
    {
        // ReSharper disable UnusedMember.Global
        bool TryGetValue(TType entityType, TId entityId, out T entity);
        // ReSharper disable UnusedMethodReturnValue.Global
        bool Add(T entity);
        bool Remove(TType entityType, TId entityId);
        // ReSharper restore UnusedMethodReturnValue.Global

        T Get(TId entityId);

        IEnumerable<T> GetAll(TType entityType);
        // ReSharper restore UnusedMember.Global
    }
}