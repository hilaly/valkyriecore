﻿using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space.Partitioning
{
    interface IRegion<T, TType, TId> : IObservable<WorldRegionMessage<T, TType, TId>>
        where T : IWorldObject<T, TType, TId>
    {
        Vector Coordinate { get; }
        void Send(WorldRegionMessage<T, TType, TId> snapShotRequest, InterestArea<T, TType, TId> interestArea);
    }
}