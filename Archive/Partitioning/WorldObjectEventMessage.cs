﻿namespace Valkyrie.Math.Space.Partitioning
{
    abstract class WorldObjectEventMessage<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        public T Source { get; set; }

        public abstract void OnAreaReceived(IInterestArea<T, TType, TId> interestArea);

        public abstract void OnObjectReceived(T gameObject);
    }
}