namespace Valkyrie.Math.Space.Partitioning
{
    class DefaultEntityFactory<T, TType, TId>
        where T : class, IWorldObject<T, TType, TId>
    {
        #region IEnrtityFactory

        public T CreateEntity(T entity, Vector initPosition)
        {
            entity.World.Entities.Add(entity);
            entity.Position = initPosition;
            return entity;
        }

        public void DestroyEntity(T entity)
        {
            entity.World.Entities.Remove(entity.GetEntityType(), entity.GetEntityId());
            entity.Dispose();
        }

        #endregion

    }
}