using System.Collections.Generic;
using Valkyrie.Math.Space.Partitioning.Collections;

namespace Valkyrie.Math.Space.Partitioning
{
    interface IWorld<T, TType, TId>
        where T : IWorldObject<T, TType, TId>
    {
        #region Properties

        BoundingBox Area { get; }

        ICollection<T, TType, TId> Entities { get; }

        #endregion

        #region Public Methods

        Region<T, TType, TId> GetRegion(Vector position);

        BoundingBox GetRegionAlignedBoundingBox(BoundingBox area);

        HashSet<Region<T, TType, TId>> GetRegions(BoundingBox area);

        HashSet<Region<T, TType, TId>> GetRegionsExcept(BoundingBox area, BoundingBox except);

        #endregion
    }
}