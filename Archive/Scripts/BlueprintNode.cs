using System;
using System.Linq;
using PIO.Misc;
using UnityEngine;

namespace PIO.Scripts
{
    [Serializable]
    public abstract class BlueprintNode
    {
        public Vector2 Position { get; set; }
        public string Name { get; set; }

        readonly BlueprintDataPinDescription[] _pins;

        protected BlueprintNode()
        {
            _pins = new BlueprintDataPinDescription[0];
        }
        
        protected BlueprintNode(BlueprintDataPinDescription[] pins)
        {
            _pins = pins;
        }

        static IPin CreatePin(BlueprintDataPinDescription pin)
        {
            var result = new OwnDataPin();
            if (pin.defaultValue != null)
                result.Value = pin.defaultValue;
            return result;
        }

        #region Init

        public void InitData(BlueprintContext blueprintContext, Script owner)
        {
            blueprintContext.GetContext(this).Pins = _pins.Select(pin => CreatePin(pin)).ToArray();
        }
        public void InitConnection(BlueprintContext blueprintContext, Script owner)
        {
            var context = blueprintContext.GetContext(this);
            var edges = owner.GetEdges(this);
            foreach(var edge in edges.Where(e => e.Instance is DataPin))
            {
                var data = (DataPin)edge.Instance;
                context.PublishOutputValues += () =>
                {
                    var tailContext = blueprintContext.GetContext(edge.Tail);
                    var temp = context.Pins[data.outputId];
                    tailContext.Pins[data.inputId] = temp;
                };
            }
        }

        #endregion

        #region Execution

        public BlueprintConnection Execute(BlueprintContext context, Script owner)
        {
            var nodeContext = context.GetContext(this);
            var result = Process(nodeContext, context, owner);
            if(nodeContext.PublishOutputValues != null)
                nodeContext.PublishOutputValues();
            return result;
        }

        protected abstract BlueprintConnection Process(NodeContext nodeContext, BlueprintContext blueprintContext, Script owner);

        #endregion
    }
}