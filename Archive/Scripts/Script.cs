﻿using System.Collections.Generic;
using PIO.Math.Graphs;
using PIO.Misc;
using PIO.UnityExtensions;
using UnityEngine;

namespace PIO.Scripts
{
    [CreateAssetMenu(menuName = "PIO/Scripts/Blueprints")]
    public class Script : ScriptableObject, IGraph<BlueprintNode, BlueprintConnection>
    {
        #region Private fields

        private readonly IIdProvider _idProvider;
        
        readonly Dictionary<int, BlueprintNode> _nodes = new Dictionary<int, BlueprintNode>();
        readonly Dictionary<int, IEdge<BlueprintNode, BlueprintConnection>> _edges = new Dictionary<int, IEdge<BlueprintNode, BlueprintConnection>>();
        
        #endregion

        #region IGraph

        public IEnumerable<IEdge<BlueprintNode, BlueprintConnection>> Edges
        {
            get { return _edges.Values; }
        }

        public IEnumerable<BlueprintNode> Nodes
        {
            get { return _nodes.Values; }
        }

        public int AddNode(BlueprintNode node)
        {
            var newId = (int)_idProvider.Generate();
            _nodes.Add(newId, node);
            return newId;
        }

        public int Connect(BlueprintNode first, BlueprintNode second, BlueprintConnection edge, float length)
        {
            var newId = (int)_idProvider.Generate();
            IEdge<BlueprintNode, BlueprintConnection> newEdge =
                Boot.GetInstance()
                    .Container.Resolve<IEdge<BlueprintNode, BlueprintConnection>>(new object[] {edge, first, second, length});
            _edges.Add(newId, newEdge);
            return newId;
        }

        public IEnumerable<IEdge<BlueprintNode, BlueprintConnection>> GetArrows()
        {
            return _edges.Values;
        }

        public IEdge<BlueprintNode, BlueprintConnection> GetEdge(int id)
        {
            IEdge<BlueprintNode, BlueprintConnection> result;
            return _edges.TryGetValue(id, out result) ? result : null;
        }

        public BlueprintNode GetNode(int id)
        {
            BlueprintNode result;
            return _nodes.TryGetValue(id, out result) ? result : default(BlueprintNode);
        }

        #endregion
    }
}