using System;

namespace PIO.Scripts
{
    [Serializable]
    public class BlueprintDataPinDescription
    {
        public string Name;
        public ClassTypeReference type;
        public object defaultValue;
        public DataType pinType;

        public void SetValue(string v)
        {
        }
    }
}