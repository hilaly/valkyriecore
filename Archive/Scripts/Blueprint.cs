﻿using System;
using UnityEngine;

namespace PIO.Scripts
{
    [CreateAssetMenu(menuName = "PIO/Scripts/Blueprint")]
    public class Blueprint : ScriptableObject
    {
        public BlueprintNodeDescription[] nodes;
        public BlueprintConnectionDescription[] connections;

        private static BlueprintNodeDescription CreateVariableNode<T>()
        {
            return new BlueprintNodeDescription
            {
                Name = "Variable " + typeof(T).Name,
                position = Vector2.one * 100,
                pins = new[]
                {
                    new BlueprintDataPinDescription
                    {
                        Name = "Value",
                        pinType = DataType.InOut,
                        type = typeof(T),
                        defaultValue = default(T)
                    }
                }
            };
        }
        
        public static Color GetColor(Type type)
        {
            if (type == typeof(string))
                return Color.magenta;
            if (type == typeof(float))
                return Color.green;
            if (type == typeof(int))
                return (Color.green + Color.white) * 0.5f;
            if (type == typeof(Vector3))
                return Color.yellow;
            if (type == typeof(Quaternion) || type == typeof(Vector4))
                return (Color.blue + Color.green) * 0.5f;
            return Color.blue;
        }

        public static BlueprintNodeDescription CreateIntVariable()
        {
            return CreateVariableNode<int>();
        }
        public static BlueprintNodeDescription CreateFloatVariable()
        {
            return CreateVariableNode<float>();
        }
        public static BlueprintNodeDescription CreateStringVariable()
        {
            return CreateVariableNode<string>();
        }
        public static BlueprintNodeDescription CreateVector2Variable()
        {
            return CreateVariableNode<Vector2>();
        }
        public static BlueprintNodeDescription CreateVector3Variable()
        {
            return CreateVariableNode<Vector3>();
        }
        public static BlueprintNodeDescription CreateVector4Variable()
        {
            return CreateVariableNode<Vector4>();
        }
    }
}