﻿using UnityEditor;

namespace PIO.UnityExtensions
{
    public class FBXScaleFix : AssetPostprocessor
    {
        void OnPreprocessModel()
        {
            var modelImporter = (ModelImporter) assetImporter;
#if UNITY_2017
            modelImporter.useFileScale = false;
            modelImporter.useFileUnits = true;
            modelImporter.globalScale = 1f;
#endif
            modelImporter.importMaterials = false;
        }
    }
}