﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PIO.Math.Splines;
using PIO.Portable;
using PIO.Scripts;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Assets.PIO.Scripts.Scripts.Editor
{
    public class BlueprintEditorWindow : EditorWindow
    {
        #region Creation

        [MenuItem("PIO/Scripts/BLueprints")]
        static void ShowWindow()
        {
            var window = GetWindow<BlueprintEditorWindow>("Blueprints");
            window.Show();
            window.UpdateSelection();
        }

        #endregion

        #region MonoBehaviour override

        void OnSelectionChange()
        {
            UpdateSelection();
        }

        void Update()
        {
            Repaint();
        }

        #endregion

        #region Utility

        void UpdateSelection()
        {
            var selected = Selection.activeObject;
            if (!(selected is Blueprint))
                return;

            _currentBlueprint = (Blueprint)selected;
            Repaint();
        }

        #endregion

        Blueprint _currentBlueprint;

        #region Drawing

        private void OnGUI()
        {
            //EditorGUI.DrawRect(position, Color.black);
            GlDrawing.DrawRect(new Vector3(0,0), new Vector3(position.width, position.height), Color.black);

            if (_currentBlueprint == null)
                return;

            //Draw blueprint
            DrawBlueprint(_currentBlueprint);

            //Draw instruments
            var graphRect = DrawInstruments(_currentBlueprint);

            //Handle input
            HandleInput(graphRect);
        }

        #region Graph

        static float GetPinWidth(BlueprintDataPinDescription pin)
        {
            return GetLabelSize(pin.Name).x + 100 + 30;
        }

        #region SizeComputing helpers

        static Vector2 GetLabelSize(GUIContent content)
        {
            return GUI.skin.label.CalcSize(content);
        }
        static Vector2 GetLabelSize(string content)
        {
            return GetLabelSize(new GUIContent(content));
        }

        static Vector2 GetInputSize(GUIContent content)
        {
            return GUI.skin.textField.CalcSize(content);
        }
        static Vector2 GetInputSize(string content)
        {
            return GetInputSize(new GUIContent(content));
        }

        #endregion

        Vector3 _windowDisplace = new Vector3(100, 100, 0);

        static float GetNodeWidth(BlueprintNodeDescription node)
        {
            var inMaxValue = 0f;
            var outMaxValue = 0f;
            for(int i = 0; i < node.pins.Length; ++i)
            {
                var size = GetPinWidth(node.pins[i]);
                switch(node.pins[i].pinType)
                {
                    case DataType.In:
                        if (inMaxValue < size)
                            inMaxValue = size;
                        break;
                    case DataType.Out:
                        if (outMaxValue < size)
                            outMaxValue = size;
                        break;
                    case DataType.InOut:
                        if (inMaxValue < size)
                            inMaxValue = size;
                        if (outMaxValue < size)
                            outMaxValue = size;
                        break;
                }
            }

            return Mathf.Max(GetLabelSize(node.Name).x, inMaxValue + outMaxValue);
        }
        static float GetNodeHeight(BlueprintNodeDescription node)
        {
            var inMaxValue = 0;
            var outMaxValue = 0;
            for (int i = 0; i < node.pins.Length; ++i)
            {
                switch (node.pins[i].pinType)
                {
                    case DataType.In:
                        ++inMaxValue;
                        break;
                    case DataType.Out:
                        ++outMaxValue;
                        break;
                    case DataType.InOut:
                        ++inMaxValue;
                        ++outMaxValue;
                        break;
                }
            }
            return 30 * (1 + Mathf.Max(inMaxValue, outMaxValue));
        }

        void DrawBlueprint(Blueprint blueprint)
        {
            //Draw connections
            foreach(var conn in blueprint.connections)
            {
                DrawConnection(conn, blueprint);
            }
            //Draw nodes
            for(var index = 0; index < blueprint.nodes.Length; ++index)
            {
                var node = blueprint.nodes[index];
                if (_selectedNodes.Contains(index))
                    DrawNodeOutline(node);
                DrawNode(node);
            }
        }

        static void DrawConnection(BlueprintConnectionDescription conn, Blueprint blueprint)
        {
            var start = GetPinConnectionCoords(blueprint.nodes[conn.InNode], conn.InPin, false);
            var end = GetPinConnectionCoords(blueprint.nodes[conn.OutNode], conn.OutPin, true);
            var color = Blueprint.GetColor(blueprint.nodes[conn.InNode].pins[conn.InPin].type);
            GlDrawing.DrawLink(start, end, color);
        }

        void DrawNode(BlueprintNodeDescription node)
        {
            var position = node.position;
            var width = GetNodeWidth(node);
            var height = GetNodeHeight(node);

            GlDrawing.Draw(() =>
            {
                GL.Begin(GL.TRIANGLES);
                GL.Color(Color.gray * 0.5f);

                GL.Vertex(position);
                GL.Vertex(position + new Vector2(width, 0));
                GL.Vertex(position + new Vector2(0, height));

                GL.Vertex(position + new Vector2(0, height));
                GL.Vertex(position + new Vector2(width, 0));
                GL.Vertex(position + new Vector2(width, height));

                GL.End();
            }, _windowDisplace);

            //Title
            GUI.Label(new Rect(node.position, GetLabelSize(node.Name)), node.Name);
            int inIndex = 0;
            int outIndex = 0;
            for(int i = 0; i < node.pins.Length; ++i)
            {
                var pin = node.pins[i];
                if(pin.pinType == DataType.In || pin.pinType == DataType.InOut)
                {
                    ++inIndex;

                    var index = inIndex;
                    var pinColor = Blueprint.GetColor(pin.type);

                    var p = node.position;
                    //Icon
                    GlDrawing.DrawCircle(p + new Vector2(15, index * 30 + 15), 14, pinColor);
                    GlDrawing.DrawCircle(p + new Vector2(15, index * 30 + 15), 12, Color.black);
                    //Label
                    GUI.Label(new Rect(p + new Vector2(30, index * 30), GetLabelSize(pin.Name)), pin.Name);
                    //Value
                    pin.SetValue(GUI.TextField(new Rect(p + new Vector2(30 + GetLabelSize(pin.Name).x, index * 30), new Vector2(100, 30)), pin.defaultValue != null ? pin.defaultValue.ToString() : string.Empty));
                }

                if (pin.pinType == DataType.Out || pin.pinType == DataType.InOut)
                {
                    ++outIndex;

                    var index = outIndex;
                    var pinColor = Blueprint.GetColor(pin.type);

                    var p = node.position + new Vector2(width - GetPinWidth(pin), 0);
                    //Value
                    pin.SetValue(GUI.TextField(new Rect(p + new Vector2(0, index * 30), new Vector2(100, 30)), pin.defaultValue != null ? pin.defaultValue.ToString() : string.Empty));
                    //Label
                    GUI.Label(new Rect(p + new Vector2(100, index * 30), GetLabelSize(pin.Name)), pin.Name);
                    //Icon
                    GlDrawing.DrawCircle(p + new Vector2(GetLabelSize(pin.Name).x + 100 + 15, index * 30 + 15), 14, pinColor);
                    GlDrawing.DrawCircle(p + new Vector2(GetLabelSize(pin.Name).x + 100 + 15, index * 30 + 15), 12, Color.black);
                }
            }
        }

        static Vector2 GetPinConnectionCoords(BlueprintNodeDescription node, int index, bool isIn)
        {
            int inIndex = 0;
            int outIndex = 0;
            for (int i = 0; i < node.pins.Length; ++i)
            {
                var pin = node.pins[i];
                if (pin.pinType == DataType.In || pin.pinType == DataType.InOut)
                {
                    ++inIndex;

                    if (isIn && i == index)
                        return node.position + new Vector2(15, inIndex * 30 + 15);
                }

                if (pin.pinType == DataType.Out || pin.pinType == DataType.InOut)
                {
                    ++outIndex;

                    if (!isIn && i == index)
                    {
                        var width = GetNodeWidth(node);
                        var p = node.position + new Vector2(width - GetPinWidth(pin), 0);
                        return p + new Vector2(GetLabelSize(pin.Name).x + 100 + 15, outIndex * 30 + 15);
                    }
                }
            }

            return Vector2.zero;
        }

        void DrawNodeOutline(BlueprintNodeDescription node)
        {
            GlDrawing.Draw(() =>
            {
                var d = 3f;
                var position = node.position;
                var width = GetNodeWidth(node);
                var height = GetNodeHeight(node);

                GL.Begin(GL.TRIANGLES);
                GL.Color(Color.yellow);

                GL.Vertex(position + new Vector2(-d, -d));
                GL.Vertex(position + new Vector2(width + d, -d));
                GL.Vertex(position + new Vector2(-d, 0));
                GL.Vertex(position + new Vector2(-d, 0));
                GL.Vertex(position + new Vector2(width + d, -d));
                GL.Vertex(position + new Vector2(width + d, 0));

                GL.Vertex(position + new Vector2(-d, -d));
                GL.Vertex(position + new Vector2(0, -d));
                GL.Vertex(position + new Vector2(-d, height + d));
                GL.Vertex(position + new Vector2(-d, height + d));
                GL.Vertex(position + new Vector2(0, -d));
                GL.Vertex(position + new Vector2(0, height + d));

                GL.Vertex(position + new Vector2(width + d - d, -d));
                GL.Vertex(position + new Vector2(width + d, -d));
                GL.Vertex(position + new Vector2(width + d - d, height + d));
                GL.Vertex(position + new Vector2(width + d - d, height + d));
                GL.Vertex(position + new Vector2(width + d, -d));
                GL.Vertex(position + new Vector2(width + d, height + d));

                GL.Vertex(position + new Vector2(-d, height + d - d));
                GL.Vertex(position + new Vector2(width + d, height + d - d));
                GL.Vertex(position + new Vector2(-d, height + d));
                GL.Vertex(position + new Vector2(-d, height + d));
                GL.Vertex(position + new Vector2(width + d, height + d - d));
                GL.Vertex(position + new Vector2(width + d, height + d));

                GL.End();
            }, _windowDisplace);
        }

        #endregion

        #region Instrument panel

        Vector2 _instrumentScrollPos;

        Rect DrawInstruments(Blueprint blueprint)
        {
            var instrumentPanelWidth = 150;
            Rect toolRect = new Rect(0, 0, instrumentPanelWidth, position.height);
            EditorGUI.DrawRect(toolRect, Color.gray * 0.5f);

            _instrumentScrollPos = GUILayout.BeginScrollView(_instrumentScrollPos, GUILayout.Width(instrumentPanelWidth));

            Action<string, Func<BlueprintNodeDescription>> buttonCreation = (text, factory) =>
            {
                if (GUILayout.Button(text))
                {
                    var newNodes = new List<BlueprintNodeDescription>(blueprint.nodes);
                    newNodes.Add(factory());
                    blueprint.nodes = newNodes.ToArray();
                }
            };

            Action<string, Func<BlueprintNodeDescription>> variableDataCreation = (typeName, factory) =>
            {
                buttonCreation(string.Format("Create {0} Variable node", typeName), factory);
            };

            variableDataCreation("Int", Blueprint.CreateIntVariable);
            variableDataCreation("Float", Blueprint.CreateFloatVariable);
            variableDataCreation("String", Blueprint.CreateStringVariable);
            variableDataCreation("Vector2", Blueprint.CreateVector2Variable);
            variableDataCreation("Vector3", Blueprint.CreateVector3Variable);
            variableDataCreation("Vector4", Blueprint.CreateVector4Variable);

            GUILayout.EndScrollView();

            return new Rect(instrumentPanelWidth, 0, position.width - instrumentPanelWidth, position.height);
        }

        #endregion

        #region Input

        class BlueprintEditorDragData
        {
            public int originalDragNode = -1;
            public int originalDragPin = -1;
            public Vector2 originalPosition;
            public Vector2 originalNodePosition;

            public bool IsConnectionCreation
            {
                get
                {
                    return originalDragPin != -1;
                }
            }
            public bool IsNodeSelected
            {
                get
                {
                    return originalDragNode != -1;
                }
            }

            public DragAndDropVisualMode Mode
            {
                get
                {
                    if (IsConnectionCreation)
                        return DragAndDropVisualMode.Link;
                    if (IsNodeSelected)
                        return DragAndDropVisualMode.Move;

                    return DragAndDropVisualMode.None;
                }
            }

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.AppendLine(originalPosition.ToString());
                sb.AppendLine(originalDragNode.ToString());
                return sb.ToString();
            }
        }

        const string dragDropId = "BlueprintEditorScriptDragDropId";

        void HandleInput(Rect dropArea)
        {
            //Cache
            var currentEvent = Event.current;
            var currentEventType = currentEvent.type;
            
            if (currentEventType == EventType.DragExited)
                DragAndDrop.PrepareStartDrag();

            if (!dropArea.Contains(currentEvent.mousePosition))
                return;

            var dragData = GetDragDropData();

            switch (currentEventType)
            {
                case EventType.MouseDown:
                    DragAndDrop.PrepareStartDrag();

                    dragData = new BlueprintEditorDragData
                    {
                        originalDragNode = GetNodeIndexInPoint(currentEvent.mousePosition),
                        originalDragPin = GetPinIndexInPoint(currentEvent.mousePosition, false),
                        originalPosition = currentEvent.mousePosition
                    };
                    if(dragData.IsNodeSelected)
                    {
                        dragData.originalNodePosition = _currentBlueprint.nodes[dragData.originalDragNode].position;

                        if(!_selectedNodes.Contains(dragData.originalDragNode))
                        {
                            if (!currentEvent.control)
                                _selectedNodes.Clear();
                            _selectedNodes.Add(dragData.originalDragNode);
                        }
                    }

                    DragAndDrop.SetGenericData(dragDropId, dragData);
                    DragAndDrop.objectReferences = new Object[] { _currentBlueprint };

                    currentEvent.Use();

                    break;
                case EventType.MouseDrag:

                    if(dragData != null)
                    {
                        DragAndDrop.StartDrag("Dragging Blueprint");
                        currentEvent.Use();
                    }

                    break;
                case EventType.DragUpdated:
                    UpdateMoveByInput(currentEvent, dragData);

                    currentEvent.Use();
                    break;
                case EventType.Repaint:
                    if (dragData != null)
                    {
                        var text = dragData.ToString();
                        var style = GUI.skin.label;
                        var size = style.CalcSize(new GUIContent(text));
                        GUI.Label(new Rect(currentEvent.mousePosition, size), text);

                        DragAndDrop.visualMode = dragData.Mode;

                        if(dragData.IsConnectionCreation)
                        {
                            //Draw link
                            var start = GetPinConnectionCoords(_currentBlueprint.nodes[dragData.originalDragNode], dragData.originalDragPin, false);
                            var end = currentEvent.mousePosition;
                            var color = Blueprint.GetColor(_currentBlueprint.nodes[dragData.originalDragNode].pins[dragData.originalDragPin].type);
                            GlDrawing.DrawNewLink(start, end, color);
                        }
                        else if (dragData.IsNodeSelected)
                        {
                        }
                    }
                    break;
                case EventType.DragPerform:
                    DragAndDrop.AcceptDrag();
                    
                    if(dragData != null)
                    {
                        if (dragData.IsConnectionCreation)
                        {
                            var targetNode = GetNodeIndexInPoint(currentEvent.mousePosition);
                            if(targetNode != -1 && targetNode != dragData.originalDragNode)
                            {
                                var targetPin = GetPinIndexInPoint(currentEvent.mousePosition, true);
                                if(targetPin != -1)
                                {
                                    var sourcePin = _currentBlueprint.nodes[dragData.originalDragNode].pins[dragData.originalDragPin];
                                    var target = _currentBlueprint.nodes[targetNode].pins[targetPin];
                                    if(sourcePin.type.Type == target.type.Type)
                                    {
                                        var newConnections = new List<BlueprintConnectionDescription>(_currentBlueprint.connections);
                                        newConnections.Add(new BlueprintConnectionDescription { InNode = dragData.originalDragNode, InPin = dragData.originalDragPin, OutNode = targetNode, OutPin = targetPin });
                                        _currentBlueprint.connections = newConnections.ToArray();
                                    }
                                }
                            }
                        }
                        //TODO: process finish
                    }

                    currentEvent.Use();
                    break;
                case EventType.MouseMove:
                    UpdateMoveByInput(currentEvent, dragData);
                    break;
                case EventType.MouseUp:
                    if(dragData != null)
                    //{
                        if(dragData.IsNodeSelected)
                        {
                    //        if (dragData.originalPosition == currentEvent.mousePosition)
                    //        {
                    //            if (!currentEvent.control)
                    //                _selectedNodes.Clear();
                    //            _selectedNodes.Add(dragData.originalDragNode);
                    //        }
                        }
                        else
                        {
                            _selectedNodes.Clear();
                        }
                    //}
                    DragAndDrop.PrepareStartDrag();
                    break;
                case EventType.KeyUp:
                    switch(currentEvent.keyCode)
                    {
                        case KeyCode.Delete:
                            {
                                var nodes = _selectedNodes.OrderByDescending(u => u).ToArray();
                                _selectedNodes.Clear();
                                for (int i = 0; i < nodes.Length; ++i)
                                {
                                    var index = nodes[i];
                                    _currentBlueprint.connections = _currentBlueprint.connections.Where(u => u.InNode != index && u.OutNode != index).ToArray();
                                }
                            }
                            break;
                        case KeyCode.UpArrow:
                            _windowDisplace.y -= 10f;
                            break;
                        case KeyCode.DownArrow:
                            _windowDisplace.y += 10f;
                            break;
                        case KeyCode.LeftArrow:
                            _windowDisplace.x -= 10f;
                            break;
                        case KeyCode.RightArrow:
                            _windowDisplace.x += 10f;
                            break;
                    }
                    break;
            }
        }

        private void UpdateMoveByInput(Event currentEvent, BlueprintEditorDragData dragData)
        {
            if (dragData != null)
            {
                if (dragData.IsConnectionCreation)
                    return;

                if (dragData.IsNodeSelected)
                {
                    var totalDisplace = currentEvent.mousePosition - dragData.originalPosition;
                    var frameDisplace = totalDisplace - (_currentBlueprint.nodes[dragData.originalDragNode].position - dragData.originalNodePosition);

                    foreach (var index in _selectedNodes)
                    {
                        _currentBlueprint.nodes[index].position += frameDisplace;
                    }
                    Repaint();
                }
            }
        }

        List<int> _selectedNodes = new List<int>();

        BlueprintEditorDragData GetDragDropData()
        {
            return DragAndDrop.GetGenericData(dragDropId) as BlueprintEditorDragData;
        }

        bool IsValidTarget()
        {
            return true;
        }
        
        int GetNodeIndexInPoint(Vector2 point)
        {
            for(int i = 0; i < _currentBlueprint.nodes.Length; ++i)
            {
                var node = _currentBlueprint.nodes[i];
                var rect = new Rect(node.position, new Vector2(GetNodeWidth(node), GetNodeHeight(node)));
                if (rect.Contains(point))
                    return i;
            }
            return -1;
        }
        int GetPinIndexInPoint(Vector2 point, bool isIn)
        {
            for (int i = 0; i < _currentBlueprint.nodes.Length; ++i)
            {
                var node = _currentBlueprint.nodes[i];
                var rect = new Rect(node.position, new Vector2(GetNodeWidth(node), GetNodeHeight(node)));
                if (!rect.Contains(point))
                    continue;

                for (int j = 0; j < node.pins.Length; ++j)
                {
                    var center = GetPinConnectionCoords(node, j, isIn);
                    var pinRect = new Rect(center - new Vector2(15, 15), new Vector2(30, 30));
                    if (pinRect.Contains(point))
                        return j;
                }
            }
            return -1;
        }

        #endregion

        #endregion
    }

    public static class GlDrawing
    {
        static Material mat;

        static bool Begin()
        {
            if (Event.current.type != EventType.Repaint)
                return false;

            if (!mat)
            {
                mat = new Material(Shader.Find("PIO/Editor/Colored Lines"));
                mat.hideFlags = HideFlags.HideAndDontSave;
                mat.shader.hideFlags = HideFlags.HideAndDontSave;
            }

            GL.PushMatrix();
            mat.SetPass(0);
            return true;
        }

        static void End()
        {
            GL.PopMatrix();
        }

        public static void Draw(Action DrawImpl, Vector3 displace)
        {
            if (!Begin())
                return;

            GL.modelview.SetTRS(displace, Quaternion.identity, Vector3.one);
            DrawImpl();
            End();
        }

        public static void DrawLink(Vector3 start, Vector3 end, Color color)
        {
            var length = 200f;
            var spline = new Bezier(new[]
            {
                start.ToPio(),
                (start + new Vector3(length, 0)).ToPio(),
                (end - new Vector3(length, 0)).ToPio(),
                end.ToPio()
            });
            var segmentsCount = 30;

            Draw(() =>
            {
                GL.Begin(GL.LINES);
                GL.Color(color);

                for (int i = 0; i < segmentsCount; ++i)
                {
                    float s = i;
                    GL.Vertex(spline.GetPoint(s / segmentsCount).ToUnity());
                    GL.Vertex(spline.GetPoint(((s + 1f) / segmentsCount)).ToUnity());
                }

                GL.End();
            }, Vector3.zero);
        }

        public static void DrawNewLink(Vector3 start, Vector3 end, Color color)
        {
            var length = 200f;
            var spline = new Bezier(new[]
            {
                start.ToPio(),
                (start + new Vector3(length, 0)).ToPio(),
                end.ToPio(),
                end.ToPio()
            });
            var segmentsCount = 30;

            Draw(() =>
            {
                GL.Begin(GL.LINES);
                GL.Color(color);

                for (int i = 0; i < segmentsCount; ++i)
                {
                    float s = i;
                    GL.Vertex(spline.GetPoint(s / segmentsCount).ToUnity());
                    GL.Vertex(spline.GetPoint(((s + 1f) / segmentsCount)).ToUnity());
                }

                GL.End();
            }, Vector3.zero);
        }

        public static void DrawRect(Vector3 leftTop, Vector3 rightBottom, Color color)
        {
            Draw(() =>
            {
                GL.Begin(GL.TRIANGLES);
                GL.Color(color);

                GL.Vertex(leftTop);
                GL.Vertex(leftTop + new Vector3(rightBottom.x - leftTop.x, leftTop.y));
                GL.Vertex(leftTop + new Vector3(0, rightBottom.y - leftTop.y));

                GL.Vertex(leftTop + new Vector3(0, rightBottom.y - leftTop.y));
                GL.Vertex(leftTop + new Vector3(rightBottom.x - leftTop.x, leftTop.y));
                GL.Vertex(rightBottom);

                GL.End();
            }, Vector3.zero);
        }

        public static void DrawCircle(Vector2 center, float radius, Color color)
        {
            Draw(() =>
            {
            int triangleAmount = 20; //# of triangles used to draw circle
            float twicePi = 2.0f * Mathf.PI;

                GL.Begin(GL.TRIANGLES);
                GL.Color(color);

                var pos = center;
                for (var index = 0; index <= triangleAmount; index++)
                {
                    GL.Vertex(pos); // center of circle
                    var i = index;
                    GL.Vertex(pos + new Vector2(radius * Mathf.Cos(i * twicePi / triangleAmount), radius * Mathf.Sin(i * twicePi / triangleAmount)));
                    ++i;
                    GL.Vertex(pos + new Vector2(radius * Mathf.Cos(i * twicePi / triangleAmount), radius * Mathf.Sin(i * twicePi / triangleAmount)));
                };

                GL.End();
            }, Vector3.zero);
        }
    }
}