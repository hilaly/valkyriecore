using System;
using System.Linq;
using PIO.Misc;

namespace PIO.Scripts
{
    [Serializable]
    public abstract class OneOutputNode : BlueprintNode
    {
        protected OneOutputNode(BlueprintDataPinDescription[] blueprintDataPinDescription) : base(blueprintDataPinDescription)
        {
        }

        protected OneOutputNode()
        { }

        protected override BlueprintConnection Process(NodeContext nodeContext, BlueprintContext blueprintContext, Script owner)
        {
            return owner.GetEdges(this).FirstOrDefault(u => u is ExecutionPin).Instance;
        }
    }
}