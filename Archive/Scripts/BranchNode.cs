using System;
using System.Linq;
using PIO.Misc;

namespace PIO.Scripts
{
    [Serializable]
    public class BranchNode : BlueprintNode
    {
        public BranchNode()
            : base(new[]
            {
                new BlueprintDataPinDescription { Name = "Expression", pinType = DataType.In, type = typeof(bool) }
            })
        {

        }

        protected override BlueprintConnection Process(NodeContext nodeContext, BlueprintContext blueprintContext, Script owner)
        {
            if ((bool)nodeContext.Pins[0].Value)
                return owner.GetEdges(this).FirstOrDefault(u => u.Instance is ExecutionPin && ((ExecutionPin)u.Instance).name == "True").Instance;
            return owner.GetEdges(this).FirstOrDefault(u => u.Instance is ExecutionPin && ((ExecutionPin)u.Instance).name == "True").Instance;
        }
    }
}