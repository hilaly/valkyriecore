using System;

namespace PIO.Scripts
{
    [Serializable]
    public abstract class VariableNode<T> : OneOutputNode
    {
        protected VariableNode()
            : base(new[]
            {
                new BlueprintDataPinDescription { Name = "Value", pinType = DataType.InOut, type = typeof(T), defaultValue = default(T)}
            })
        { }
    }
}