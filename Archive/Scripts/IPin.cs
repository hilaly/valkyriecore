namespace PIO.Scripts
{
    public interface IPin
    {
        object Value { get; set; }
    }
}