using System.Collections.Generic;

namespace PIO.Scripts
{
    public class BlueprintContext
    {
        readonly Dictionary<BlueprintNode, NodeContext> _contexts = new Dictionary<BlueprintNode, NodeContext>();

        public NodeContext GetContext(BlueprintNode node)
        {
            NodeContext result;
            if (_contexts.TryGetValue(node, out result))
                return result;
            result = new NodeContext();
            _contexts.Add(node, result);
            return result;
        }
    }
}