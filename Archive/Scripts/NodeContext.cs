using System;

namespace PIO.Scripts
{
    public class NodeContext
    {
        public IPin[] Pins;
        public Action PublishOutputValues;
    }
}