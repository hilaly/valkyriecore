using System;

namespace PIO.Scripts
{
    [Serializable]
    public class BlueprintConnectionDescription
    {
        public int InNode;
        public int InPin;

        public int OutNode;
        public int OutPin;
    }
}