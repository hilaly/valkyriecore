using System;
using UnityEngine;

namespace PIO.Scripts
{
    [Serializable]
    public class BlueprintNodeDescription
    {
        public string Name;
        public Vector2 position;
        public BlueprintDataPinDescription[] pins;
    }
}