using System.Threading;

namespace Valkyrie.ToRemove
{
    class Settings : ISettings
    {
        public string ApplicationId { get; set; }
        public LockRecursionPolicy LockPolicy { get; set; }
        public int BackgroundThreadsCount { get; set; }
        public string ViewsPathFormat { get; set; }
        public string LogPath { get; set; }
        public bool ConsoleLog { get; set; }
        public string NetworkLogUrl { get; set; }

        public bool UseEcs { get; set; }
        public bool ResolveEcsSystems { get; set; }
    }
}