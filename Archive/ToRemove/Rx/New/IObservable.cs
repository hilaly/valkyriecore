using System;

namespace Valkyrie.ToRemove.Rx.New
{
    /// <summary>
    /// Defines a provider for push-based notification.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IObservable<out T>
    {
        /// <summary>
        /// Notifies the provider that an observer is to receive notifications.
        /// </summary>
        /// <param name="observer"></param>
        /// <returns></returns>
        IDisposable Subscribe(IObserver<T> observer);
    }
}