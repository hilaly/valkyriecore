using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Rx.New
{
    class ListObserver<T> : IComplexObserver<T>
    {
        private readonly List<IObserver<T>> _observers;

        public ListObserver() : this(new List<IObserver<T>>())
        {
        }

        private ListObserver(List<IObserver<T>> observers)
        {
            _observers = observers;
        }

        public void OnCompleted()
        {
            var targetObservers = _observers.ToArray();
            for (int i = 0; i < targetObservers.Length; i++)
            {
                targetObservers[i].OnCompleted();
            }
        }

        public void OnError(Exception error)
        {
            var targetObservers = _observers.ToArray();
            for (int i = 0; i < targetObservers.Length; i++)
            {
                targetObservers[i].OnError(error);
            }
        }

        public void OnNext(T value)
        {
            var targetObservers = _observers.ToArray();
            for (int i = 0; i < targetObservers.Length; i++)
            {
                targetObservers[i].OnNext(value);
            }
        }
        
        IObserver<T> IComplexObserver<T>.Add(IObserver<T> observer)
        {
            _observers.Add(observer);
            return this;
        }

        IObserver<T> IComplexObserver<T>.Remove(IObserver<T> observer)
        {
            _observers.Remove(observer);
            return this;
        }
    }
}