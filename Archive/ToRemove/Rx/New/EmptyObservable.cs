using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.ToRemove.Rx.New
{
    class EmptyObservable<T> : IObservable<T>
    {
        public IDisposable Subscribe(IObserver<T> observer)
        {
            return Disposable.Empty;
        }
    }
}