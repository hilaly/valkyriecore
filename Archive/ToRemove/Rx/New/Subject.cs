using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.ToRemove.Rx.New
{
    /// <summary>
    /// Represents an object that is both an observable sequence as well as an observer.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Subject<T> : ISubject<T>, IDisposable
    {
        private readonly object _lock = new object();
        private IComplexObserver<T> _outObserver = new ListObserver<T>();
        private bool _isStopped;
        private bool _isDisposed;
        private Exception _lastError;

        public void OnNext(T value)
        {
            _outObserver.OnNext(value);
        }

        public void OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");

            IObserver<T> old;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (_isStopped)
                    return;

                old = _outObserver;
                _outObserver = new ListObserver<T>();
                _isStopped = true;
                _lastError = error;
            }
            old.OnError(error);
        }

        public void OnCompleted()
        {
            IObserver<T> old;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (_isStopped)
                    return;

                old = _outObserver;
                _outObserver = new ListObserver<T>();
                _isStopped = true;
            }
            old.OnCompleted();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");

            Exception ex;

            lock (_lock)
            {
                ThrowIfDisposed();
                if (!_isStopped)
                {
                    _outObserver = (IComplexObserver<T>)_outObserver.Add(observer);
                    return new Subscription(this, observer);
                }
                ex = _lastError;
            }
            if (ex != null)
                observer.OnError(ex);
            else
                observer.OnCompleted();

            return Disposable.Empty;
        }

        public void Dispose()
        {
            lock (_lock)
            {
                _isDisposed = true;
                _outObserver = DisposedObserver<T>.Instance;
            }
        }

        void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException("");
        }

        class Subscription : IDisposable
        {
            readonly object _gate = new object();
            Subject<T> _parent;
            IObserver<T> _unsubscribeTarget;

            public Subscription(Subject<T> parent, IObserver<T> unsubscribeTarget)
            {
                _parent = parent;
                _unsubscribeTarget = unsubscribeTarget;
            }

            public void Dispose()
            {
                lock (_gate)
                {
                    if (_parent != null)
                    {
                        lock (_parent._lock)
                        {
                            _parent._outObserver = (IComplexObserver<T>)_parent._outObserver.Remove(_unsubscribeTarget);
                            _unsubscribeTarget = null;
                            _parent = null;
                        }
                    }
                }
            }
        }
    }
}