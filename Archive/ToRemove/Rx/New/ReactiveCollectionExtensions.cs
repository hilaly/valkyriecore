﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Threading.Async;

namespace Valkyrie.ToRemove.Rx.New
{
    public static class ReactiveCollectionExtensions
    {

        public static IDisposable Subscribe<T>(this IObservable<T> observable, Action<T> handler)
        {
            return observable.Subscribe(new ActionSubscription<T>().HandleNext(handler));
        }

        class ActionSubscription<T> : ISubject<T>
        {
            private readonly object _gate = new object();
            private readonly HashSet<Action<T>> _nextHandlers = new HashSet<Action<T>>();
            private readonly HashSet<Action<Exception>> _errorHandlers = new HashSet<Action<Exception>>();
            private readonly HashSet<Action> _completeHandlers = new HashSet<Action>();
            
            public void OnNext(T value)
            {
                List<Action<T>> handlers;
                lock (_gate)
                    handlers = _nextHandlers.ToList();
                foreach (var handler in handlers)
                    handler(value);
            }

            public void OnError(Exception error)
            {
                List<Action<Exception>> handlers;
                lock (_gate)
                    handlers = _errorHandlers.ToList();
                foreach (var handler in handlers)
                    handler(error);
            }

            public void OnCompleted()
            {
                throw new NotImplementedException();
            }

            public IDisposable Subscribe(IObserver<T> observer)
            {
                HandleNext(observer.OnNext).HandleComplete(observer.OnCompleted).HandleError(observer.OnError);
                return Disposable.Create(() =>
                {
                    lock (_gate)
                    {
                        _nextHandlers.Remove(observer.OnNext);
                        _errorHandlers.Remove(observer.OnError);
                        _completeHandlers.Remove(observer.OnCompleted);
                    }
                });
            }

            public ActionSubscription<T> HandleNext(Action<T> handler)
            {
                lock (_gate)
                    _nextHandlers.Add(handler);
                return this;
            }
            
            public ActionSubscription<T> HandleComplete(Action handler)
            {
                lock (_gate)
                    _completeHandlers.Add(handler);
                return this;}
            
            public ActionSubscription<T> HandleError(Action<Exception> handler)
            {
                lock (_gate)
                    _errorHandlers.Add(handler);
                return this;}
        }
    }
}