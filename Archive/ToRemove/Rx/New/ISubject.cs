namespace Valkyrie.ToRemove.Rx.New
{
    /// <summary>
    /// Represents an object that is both an observable sequence as well as an observer.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISubject<T> : ISubject<T,T>
    { }

    /// <summary>
    /// Represents an object that is both an observable sequence as well as an observer.
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    public interface ISubject<in TSource, out TResult> : IObserver<TSource>, IObservable<TResult>
    { }
}