using System;

namespace Valkyrie.ToRemove.Rx.New
{
    /// <summary>
    /// Provides a mechanism for receiving push-based notifications.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IObserver<in T>
    {
        /// <summary>
        /// Provides the observer with new data.
        /// </summary>
        /// <param name="value"></param>
        void OnNext(T value);
        /// <summary>
        /// Notifies the observer that the provider has experienced an error condition.
        /// </summary>
        /// <param name="error"></param>
        void OnError(Exception error);
        /// <summary>
        /// Notifies the observer that the provider has finished sending push-based notifications.
        /// </summary>
        void OnCompleted();
    }
}