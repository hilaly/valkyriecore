namespace Valkyrie.ToRemove.Rx.New
{
    interface IComplexObserver<T> : IObserver<T>
    {
        IObserver<T> Add(IObserver<T> observer);
        IObserver<T> Remove(IObserver<T> observer);
    }
}