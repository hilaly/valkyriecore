using System;
using System.Collections.Generic;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Time;

namespace Valkyrie.ToRemove.Rx.New
{
    /// <summary>
    /// Represents an object that is both an observable sequence as well as an observer.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ReplaySubject<T> : ISubject<T>, IDisposable
    {
        private readonly object _lock = new object();
        private IComplexObserver<T> _outObserver = new ListObserver<T>();
        private bool _isStopped;
        private bool _isDisposed;
        private Exception _lastError;

        private readonly int _bufferSize;
        private readonly TimeSpan _window;
        private readonly ITime _scheduler;

        Queue<TimeInterval<T>> _queue = new Queue<TimeInterval<T>>();
        private readonly float _startTime;

        public ReplaySubject() : this(int.MaxValue, TimeSpan.MaxValue, null)
        { }
        public ReplaySubject(int bufferSize) : this(bufferSize, TimeSpan.MaxValue, null)
        { }
        public ReplaySubject(TimeSpan window, ITime scheduler) : this(int.MaxValue, window, scheduler)
        { }
        public ReplaySubject(int bufferSize, TimeSpan window, ITime scheduler)
        {
            if (bufferSize < 0) throw new ArgumentOutOfRangeException("bufferSize");
            if (window < TimeSpan.Zero) throw new ArgumentOutOfRangeException("window");
            if (scheduler == null) throw new ArgumentNullException("scheduler");

            _bufferSize = bufferSize;
            _window = window;
            _scheduler = scheduler;

            _startTime = _scheduler.Time;
        }

        public void OnNext(T value)
        {
            IObserver<T> current;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (_isStopped) return;

                // enQ
                _queue.Enqueue(new TimeInterval<T>(value, TimeSpan.FromSeconds(_scheduler.Time - _startTime)));
                Trim();

                current = _outObserver;
            }
            current.OnNext(value);
        }

        public void OnError(Exception error)
        {
            if (error == null) throw new ArgumentNullException("error");

            IObserver<T> old;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (_isStopped) return;

                old = _outObserver;
                _outObserver = new ListObserver<T>();
                _isStopped = true;
                _lastError = error;
                Trim();
            }

            old.OnError(error);
        }

        public void OnCompleted()
        {
            IObserver<T> old;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (_isStopped) return;

                old = _outObserver;
                _outObserver = new ListObserver<T>();
                _isStopped = true;
                Trim();
            }

            old.OnCompleted();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");

            Exception ex;
            var subscription = default(Subscription);

            lock (_lock)
            {
                ThrowIfDisposed();
                if (!_isStopped)
                {
                    _outObserver = (IComplexObserver<T>)_outObserver.Add(observer);
                    subscription = new Subscription(this, observer);
                }

                ex = _lastError;
                Trim();
                foreach (var item in _queue)
                {
                    observer.OnNext(item.Value);
                }
            }

            if (subscription != null)
                return subscription;
            else if (ex != null)
                observer.OnError(ex);
            else
                observer.OnCompleted();

            return Disposable.Empty;
        }

        public void Dispose()
        {
            lock (_lock)
            {
                _isDisposed = true;
                _outObserver = DisposedObserver<T>.Instance;
                _lastError = null;
                _queue = null;

            }
        }
        void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException("");
        }
        void Trim()
        {
            while (_queue.Count > _bufferSize)
            {
                _queue.Dequeue();
            }
            var elapsedTime = TimeSpan.FromSeconds(_scheduler.Time - _startTime);
            while (_queue.Count > 0 && elapsedTime.Subtract(_queue.Peek().Interval).CompareTo(_window) > 0)
            {
                _queue.Dequeue();
            }
        }
        class Subscription : IDisposable
        {
            readonly object _gate = new object();
            ReplaySubject<T> _parent;
            IObserver<T> _unsubscribeTarget;

            public Subscription(ReplaySubject<T> parent, IObserver<T> unsubscribeTarget)
            {
                _parent = parent;
                _unsubscribeTarget = unsubscribeTarget;
            }

            public void Dispose()
            {
                lock (_gate)
                {
                    if (_parent != null)
                    {
                        lock (_parent._lock)
                        {
                            _parent._outObserver = (IComplexObserver<T>)_parent._outObserver.Remove(_unsubscribeTarget);
                            _unsubscribeTarget = null;
                            _parent = null;
                        }
                    }
                }
            }
        }
    }
}