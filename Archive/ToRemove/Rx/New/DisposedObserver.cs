using System;

namespace Valkyrie.ToRemove.Rx.New
{
    class DisposedObserver<T> : IComplexObserver<T>
    {
        public static readonly DisposedObserver<T> Instance = new DisposedObserver<T>();

        DisposedObserver()
        {

        }

        public void OnCompleted()
        {
            throw new ObjectDisposedException("");
        }

        public void OnError(Exception error)
        {
            throw new ObjectDisposedException("");
        }

        public void OnNext(T value)
        {
            throw new ObjectDisposedException("");
        }

        public IObserver<T> Add(IObserver<T> observer)
        {
            throw new ObjectDisposedException("");
        }

        public IObserver<T> Remove(IObserver<T> observer)
        {
            throw new ObjectDisposedException("");
        }
    }
}