using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    class RxDuplicateThread : IRxThread
    {
        public void Dispose()
        {
            _thread.Finished -= OnFinish;
            
            Finished = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;

        readonly IRxThread _thread;

        public RxDuplicateThread(IRxThread thread)
        {
            _thread = thread;
            _thread.Finished += OnFinish;
        }

        private void OnFinish()
        {
            var call = Finished;
            
            if (call != null)
                call();
        }
    }
    [Obsolete("Will be removed", true)]
    class RxDuplicateThread<T0> : IRxThread<T0>
    {
        public void Dispose()
        {
            _thread.FinishedArgs -= OnFinish;

            FinishedArgs = null;
            Finished = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0> FinishedArgs;

        readonly IRxThread<T0> _thread;

        public RxDuplicateThread(IRxThread<T0> thread)
        {
            _thread = thread;
            _thread.FinishedArgs += OnFinish;
        }

        private void OnFinish(T0 arg1)
        {
            var argsCall = FinishedArgs;
            var call = Finished;

            if (argsCall != null)
                argsCall(arg1);
            if (call != null)
                call();
        }
    }
    [Obsolete("Will be removed", true)]
    class RxDuplicateThread<T0, T1> : IRxThread<T0, T1>
    {
        public void Dispose()
        {
            _thread.FinishedArgs -= OnFinish;

            FinishedArgs = null;
            Finished = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0, T1> FinishedArgs;

        readonly IRxThread<T0, T1> _thread;

        public RxDuplicateThread(IRxThread<T0, T1> thread)
        {
            _thread = thread;
            _thread.FinishedArgs += OnFinish;
        }

        private void OnFinish(T0 arg1, T1 arg2)
        {
            var argsCall = FinishedArgs;
            var call = Finished;

            if (argsCall != null)
                argsCall(arg1, arg2);
            if (call != null)
                call();
        }
    }
    [Obsolete("Will be removed", true)]
    class RxDuplicateThread<T0, T1, T2> : IRxThread<T0, T1, T2>
    {
        public void Dispose()
        {
            _thread.FinishedArgs -= OnFinish;

            FinishedArgs = null;
            Finished = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0, T1, T2> FinishedArgs;

        readonly IRxThread<T0, T1, T2> _thread;

        public RxDuplicateThread(IRxThread<T0, T1, T2> thread)
        {
            _thread = thread;
            _thread.FinishedArgs += OnFinish;
        }

        private void OnFinish(T0 arg1, T1 arg2, T2 arg3)
        {
            var argsCall = FinishedArgs;
            var call = Finished;

            if (argsCall != null)
                argsCall(arg1, arg2, arg3);
            if (call != null)
                call();
        }
    }
    [Obsolete("Will be removed", true)]
    class RxDuplicateThread<T0, T1, T2, T3> : IRxThread<T0, T1, T2, T3>
    {
        public void Dispose()
        {
            _thread.FinishedArgs -= OnFinish;

            FinishedArgs = null;
            Finished = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0, T1, T2, T3> FinishedArgs;

        readonly IRxThread<T0, T1, T2, T3> _thread;

        public RxDuplicateThread(IRxThread<T0, T1, T2, T3> thread)
        {
            _thread = thread;
            _thread.FinishedArgs += OnFinish;
        }

        private void OnFinish(T0 arg1, T1 arg2, T2 arg3, T3 arg4)
        {
            var argsCall = FinishedArgs;
            var call = Finished;

            if (argsCall != null)
                argsCall(arg1, arg2, arg3, arg4);
            if (call != null)
                call();
        }
    }
}