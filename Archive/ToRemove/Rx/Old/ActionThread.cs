﻿using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    class ActionThread : GenericThread
    {
        readonly Action _action;
        private readonly ITask _task;

        public ActionThread(Action action, ITaskManager taskManager, bool executeOnMainThread)
        {
            _action = action;

            _task = taskManager.Create(Work);
            _task.ExecuteOnMainThread = executeOnMainThread;
        }

        public override void Start()
        {
            base.Start();
            _task.Perform();
        }

        void Work()
        {
            _action();
            Finish();
        }

        public override void Dispose()
        {
            base.Dispose();

            _task.Cancel();
        }
    }
}