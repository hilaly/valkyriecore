using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    public class ObservableProperty<T>
    {
        private T _value;
        public T Value
        {
            get { return _value; }
            set
            {
                _value = value;
                if (Changed != null)
                    Changed();
            }
        }

        public event Action Changed;

        public ObservableProperty()
        {
        }

        public ObservableProperty(T value)
        {
            _value = value;
        }
    }
}