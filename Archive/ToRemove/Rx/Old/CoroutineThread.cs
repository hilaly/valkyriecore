﻿using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    class CoroutineThread : GenericThread
    {
        private readonly ITaskManager _taskManager;
        private readonly bool _executeOnMainThread;
        private readonly Func<bool> _finishedFunc;
        private ITask _task;

        public CoroutineThread(ITaskManager taskManager, bool executeOnMainThread, Func<bool> finishedFunc)
        {
            _taskManager = taskManager;
            _executeOnMainThread = executeOnMainThread;
            _finishedFunc = finishedFunc;

            UpdateTask();
        }

        private void UpdateTask()
        {
            _task = _taskManager.Create(Work);
            _task.ExecuteOnMainThread = _executeOnMainThread;
        }

        private void Work()
        {
            if (!IsActive)
                return;

            if (_finishedFunc())
                Finish();

            UpdateTask();
            _task.Perform();
        }

        public override void Start()
        {
            base.Start();
            _task.Perform();
        }

        public override void Dispose()
        {
            base.Dispose();
            _task.Cancel();
        }
    }
}