﻿using System;
using Valkyrie.Tools.Time;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    class RxFactory : IRxFactory
    {
        internal static RxFactory Instance { get; private set; }

        public ITaskManager TaskManager { get; private set; }
        public ITimeService TimeSystem { get; private set; }

        public RxFactory(ITaskManager taskManager, ITimeService timeSystem)
        {
            TaskManager = taskManager;
            TimeSystem = timeSystem;
            Instance = this;
        }

        public IRxThread Start(Action action)
        {
            var result = new ActionThread(action, TaskManager, false);
            result.Start();
            return result;
        }
        public IRxThread EveryUpdate()
        {
            var result = new CoroutineThread(TaskManager, true, () => true);
            result.Start();
            return result;
        }

        public IRxThread StartTimer(float length)
        {
            return StartTimer(TimeSystem.Default, length);
        }
        public IRxThread StartTimer(ITime time, float length)
        {
            var startTime = time.Time;
            var result = new CoroutineThread(TaskManager, true, () =>
            {
                var trigger = time.Time - startTime >= length;
                if (trigger)
                {
                    while (time.Time - startTime >= length)
                    {
                        startTime += length;
                    }
                }
                return trigger;
            });
            result.Start();
            return result;
        }
        
        public IRxThread MirrorThread(IRxThread rxThread)
        {
            return new RxDuplicateThread(rxThread);
        }
        public IRxThread<T0> MirrorThread<T0>(IRxThread<T0> rxThread)
        {
            return new RxDuplicateThread<T0>(rxThread);
        }
        public IRxThread<T0, T1> MirrorThread<T0, T1>(IRxThread<T0, T1> rxThread)
        {
            return new RxDuplicateThread<T0, T1>(rxThread);
        }
        public IRxThread<T0, T1, T2> MirrorThread<T0, T1, T2>(IRxThread<T0, T1, T2> rxThread)
        {
            return new RxDuplicateThread<T0, T1, T2>(rxThread);
        }
        public IRxThread<T0, T1, T2, T3> MirrorThread<T0, T1, T2, T3>(IRxThread<T0, T1, T2, T3> rxThread)
        {
            return new RxDuplicateThread<T0, T1, T2, T3>(rxThread);
        }
    }
}
