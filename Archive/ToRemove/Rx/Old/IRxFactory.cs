using System;
using Valkyrie.Tools.Time;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    public interface IRxFactory
    {
        IRxThread Start(Action action);
        IRxThread EveryUpdate();

        IRxThread StartTimer(float length);
        IRxThread StartTimer(ITime time, float length);
        
        IRxThread MirrorThread(IRxThread rxThread);
        IRxThread<T0> MirrorThread<T0>(IRxThread<T0> rxThread);
        IRxThread<T0, T1> MirrorThread<T0, T1>(IRxThread<T0, T1> rxThread);
        IRxThread<T0, T1, T2> MirrorThread<T0, T1, T2>(IRxThread<T0, T1, T2> rxThread);
        IRxThread<T0, T1, T2, T3> MirrorThread<T0, T1, T2, T3>(IRxThread<T0, T1, T2, T3> rxThread);
    }
}