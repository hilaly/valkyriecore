using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    class EventThread : IRxEvent
    {
        public void Set()
        {
            var call = Finished;
            if (call != null)
                call();
        }

        public void Dispose()
        {
            Finished = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
    }
    [Obsolete("Will be removed", true)]
    class EventThread<T0> : IRxEvent<T0>
    {
        public void Set(T0 arg0)
        {
            var callArgs = FinishedArgs;
            var call = Finished;

            if (callArgs != null)
                callArgs(arg0);
            if (call != null)
                call();
        }

        public void Dispose()
        {
            Finished = null;
            FinishedArgs = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0> FinishedArgs;
    }
    [Obsolete("Will be removed", true)]
    class EventThread<T0, T1> : IRxEvent<T0, T1>
    {
        public void Set(T0 arg0, T1 arg1)
        {
            var callArgs = FinishedArgs;
            var call = Finished;

            if (callArgs != null)
                callArgs(arg0, arg1);
            if (call != null)
                call();
        }

        public void Dispose()
        {
            Finished = null;
            FinishedArgs = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0, T1> FinishedArgs;
    }
    [Obsolete("Will be removed", true)]
    class EventThread<T0, T1, T2> : IRxEvent<T0, T1, T2>
    {
        public void Set(T0 arg0, T1 arg1, T2 arg2)
        {
            var callArgs = FinishedArgs;
            var call = Finished;

            if (callArgs != null)
                callArgs(arg0, arg1, arg2);
            if (call != null)
                call();
        }

        public void Dispose()
        {
            Finished = null;
            FinishedArgs = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0, T1, T2> FinishedArgs;
    }
    [Obsolete("Will be removed", true)]
    class EventThread<T0, T1, T2, T3> : IRxEvent<T0, T1, T2, T3>
    {
        public void Set(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
            var callArgs = FinishedArgs;
            var call = Finished;

            if (callArgs != null)
                callArgs(arg0, arg1, arg2, arg3);
            if (call != null)
                call();
        }

        public void Dispose()
        {
            Finished = null;
            FinishedArgs = null;

            if (Disposed != null)
            {
                Disposed();
                Disposed = null;
            }
        }

        public event Action Finished;
        public event Action Disposed;
        public event Action<T0, T1, T2, T3> FinishedArgs;
    }
}