﻿using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    public static class RxExt
    {
        public static T Subscribe<T>(this T observable, Action<T> callOnFinish) where T : IRxThread
        {
            return Subscribe(observable, () => callOnFinish(observable));
        }
        public static T Subscribe<T>(this T observable, Action callOnFinish) where T : IRxThread
        {
            observable.Finished += callOnFinish;
            return observable;
        }
        public static IRxThread<T0> Subscribe<T0>(this IRxThread<T0> observable,
            Action<T0> callOnFinish)
        {
            observable.FinishedArgs += callOnFinish;
            return observable;
        }
        public static IRxThread<T0, T1> Subscribe<T0, T1>(this IRxThread<T0, T1> observable,
            Action<T0, T1> callOnFinish)
        {
            observable.FinishedArgs += callOnFinish;
            return observable;
        }
        public static IRxThread<T0, T1, T2> Subscribe<T0, T1, T2>(this IRxThread<T0, T1, T2> observable,
            Action<T0, T1, T2> callOnFinish)
        {
            observable.FinishedArgs += callOnFinish;
            return observable;
        }
        public static IRxThread<T0, T1, T2, T3> Subscribe<T0, T1, T2, T3>(this IRxThread<T0, T1, T2, T3> observable,
            Action<T0, T1, T2, T3> callOnFinish)
        {
            observable.FinishedArgs += callOnFinish;
            return observable;
        }

        public static ICompositeDisposable AttachTo(this IRxThread observable,
            ICompositeDisposable compositeDisposable)
        {
            compositeDisposable.Add(observable);
            return compositeDisposable;
        }

        public static IRxThread ObserveOnMainThread(this IRxThread thread)
        {
            var resultThread = new ActionThread(() => { }, RxFactory.Instance.TaskManager, true);
            thread.Subscribe(() => resultThread.Start());
            return resultThread;
        }
        public static IRxThread ObserveOnSeparateThread(this IRxThread thread)
        {
            var resultThread = new ActionThread(() => { }, RxFactory.Instance.TaskManager, false);
            thread.Subscribe(() => resultThread.Start());
            return resultThread;
        }
    }
}