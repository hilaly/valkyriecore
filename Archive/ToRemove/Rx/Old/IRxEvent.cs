using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    public interface IRxEvent : IRxThread
    {
        void Set();
    }
    [Obsolete("Will be removed", true)]
    public interface IRxEvent<T0> : IRxThread<T0>
    {
        void Set(T0 arg0);
    }
    [Obsolete("Will be removed", true)]
    public interface IRxEvent<T0, T1> : IRxThread<T0, T1>
    {
        void Set(T0 arg0, T1 arg1);
    }
    [Obsolete("Will be removed", true)]
    public interface IRxEvent<T0, T1, T2> : IRxThread<T0, T1, T2>
    {
        void Set(T0 arg0, T1 arg1, T2 arg2);
    }
    [Obsolete("Will be removed", true)]
    public interface IRxEvent<T0, T1, T2, T3> : IRxThread<T0, T1, T2, T3>
    {
        void Set(T0 arg0, T1 arg1, T2 arg2, T3 arg3);
    }
}