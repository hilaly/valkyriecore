﻿using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    class GenericThread : IRxThread
    {
        #region IObserable

        public event Action Finished;
        public event Action Disposed;

        #endregion

        #region Event invocator

        protected virtual void Finish()
        {
            if (Finished != null)
                Finished();
            IsStarted = false;
        }

        #endregion

        protected bool IsStarted { get; private set; }
        protected bool IsActive { get; private set; }

        #region IDisposable

        public virtual void Dispose()
        {
            IsActive = false;
            Finished = null;

            if (Disposed != null)
                Disposed();
        }

        #endregion

        public virtual void Start()
        {
            IsStarted = true;
        }

        public GenericThread()
        {
            IsActive = true;
        }
    }
}