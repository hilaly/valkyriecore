﻿using System;

namespace Valkyrie.ToRemove.Rx.Old
{
    [Obsolete("Will be removed", true)]
    public interface IRxThread : IDisposable
    {
        event Action Finished;
        event Action Disposed;
    }

    [Obsolete("Will be removed", true)]
    public interface IRxThread<T0> : IRxThread
    {
        event Action<T0> FinishedArgs;
    }

    [Obsolete("Will be removed", true)]
    public interface IRxThread<T0, T1> : IRxThread
    {
        event Action<T0, T1> FinishedArgs;
    }

    [Obsolete("Will be removed", true)]
    public interface IRxThread<T0, T1, T2> : IRxThread
    {
        event Action<T0, T1, T2> FinishedArgs;
    }

    [Obsolete("Will be removed", true)]
    public interface IRxThread<T0, T1, T2, T3> : IRxThread
    {
        event Action<T0, T1, T2, T3> FinishedArgs;
    }
}