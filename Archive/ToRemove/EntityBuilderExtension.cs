﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.ToRemove.Framework;
using Valkyrie.ToRemove.Framework.Systems.Management;

namespace Valkyrie.ToRemove
{
    static class EntityBuilderExtension
    {
        public static void Build(this IEntityBuilder entityBuilder, string id, IEnumerable<object> components)
        {
            var allNodes = typeof(EntityBuilderExtension).Assembly.GetTypes().Where(type => typeof(INode).IsAssignableFrom(type)).ToList();
            allNodes.Add(typeof(RemoveNode));

            entityBuilder.Build(id, allNodes.Select(u => TryCreate(u, components)).Where(u => u != null), components);
        }

        public static INode TryCreate(Type nodeType, IEnumerable<object> components)
        {
            return null;
        }
    }
}
