using System;
using System.Net;
using Valkyrie.Communication.Network;
using Valkyrie.Communication.Network.Lidgren;

namespace Valkyrie.ToRemove.LidgrenNetwork
{
    [Obsolete("Old network", true)]
    class BaseConnection : IConnection
    {
        private readonly Network _instanceNetwork;
        public NetConnection InternalConnection { get; private set; }
        public BaseConnection(NetConnection internalConnection, Network instanceNetwork)
        {
            InternalConnection = internalConnection;
            _instanceNetwork = instanceNetwork;
        }

        #region Implementation of IEventPublisher

        public void Send<TEvent>(TEvent message)
        {
            _instanceNetwork.Send(BaseNetMessage.CreateEvent(message), this);
        }

        public void Send<TRequest, TResponse>(TRequest request, object sender, Action<TRequest, object, TResponse> responseHandler)
        {
            Action<BaseNetMessage> callbackAction = message =>
            {
                var response = message.GetData<TResponse>();
                try
                {
                    responseHandler(request, sender, response);
                }
                catch (Exception e)
                {
                    _instanceNetwork.Log.LogError("Exception during handle network response: {0} request {1} response {2}", e.Message, request, response);
                }
            };

            _instanceNetwork.Send(BaseNetMessage.CreateRequest<TResponse>(request), this, callbackAction);
        }

        #endregion

        #region Implementation of IConnection

        public IPEndPoint EndPoint { get { return InternalConnection.RemoteEndPoint; } }

        public void Close()
        {
            InternalConnection.Disconnect("DisconnectByApplicationRequest");
            InternalConnection = null;
        }

        #endregion
    }
}