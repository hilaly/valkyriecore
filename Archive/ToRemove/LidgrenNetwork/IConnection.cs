﻿using System;
using System.Net;

namespace Valkyrie.ToRemove.LidgrenNetwork
{
    [Obsolete("Old network", true)]
    public interface IConnection
    {
        void Send<TEvent>(TEvent message);

        void Send<TRequest, TResponse>(TRequest request, object sender,
            Action<TRequest, object, TResponse> responseHandler);

        IPEndPoint EndPoint { get; }

        void Close();
    }
}
