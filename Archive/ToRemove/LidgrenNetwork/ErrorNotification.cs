using Valkyrie.Communication.Network;

namespace Valkyrie.ToRemove.LidgrenNetwork
{
    [NetworkMessage(NetDeliveryMethod.ReliableOrdered, 0)]
    public class ErrorNotification
    {
        public int Code;
        public string Description;
    }
}