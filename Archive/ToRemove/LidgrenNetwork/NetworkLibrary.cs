﻿using System;
using Valkyrie.Di;
using Valkyrie.Tools.Logs;
using Valkyrie.Tools.Time;

namespace Valkyrie.ToRemove.LidgrenNetwork
{
    [Obsolete("Old network", true)]
    class NetworkLibrary : ILibrary
    {
        private readonly string _aplicationId;
        private int _connectionsCount;
        private int _port;
        Func<IConnection, bool> _connectionRestriction = connection => true;

        public NetworkLibrary(string appId)
        {
            _aplicationId = appId;
        }

        public NetworkLibrary UseAsServer(int port, int connections)
        {
            _port = port;
            _connectionsCount = connections;
            return this;
        }

        public NetworkLibrary WithRestriction(Func<IConnection, bool> restriction)
        {
            _connectionRestriction = restriction;
            return this;
        }

        public void Load(IContainerRegistrator builder)
        {
            builder.Register(
                container =>
                    new Network(container.Resolve<ITimeService>(), container.Resolve<ILogService>(), _port,
                        _connectionsCount, _aplicationId, _connectionRestriction)).As<INetwork>().SingleInstance();
        }
    }
}
