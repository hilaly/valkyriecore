﻿using System;

namespace Valkyrie.ToRemove.LidgrenNetwork
{
    [Obsolete("Old network", true)]
    public interface INetwork
    {
        float RequestTimeout { get; set; }

        IConnection Connect(string host, int port);

        IDisposable Subscribe<TEvent>(Action<TEvent, IConnection> eventHandler);
        //void Unsubscribe<TEvent>(Action<TEvent, IConnection> eventHandler);
        IDisposable Subscribe<TRequest, TResponse>(Action<TRequest, IConnection, Action<TResponse>> requestHandler);
        //void Unsubscribe<TRequest, TResponse>(Action<TRequest, IConnection, Action<TResponse>> requestHandler);
    }
}