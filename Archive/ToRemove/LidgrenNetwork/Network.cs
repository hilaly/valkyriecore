﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Threading;
using Valkyrie.Communication.Network;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;
using Valkyrie.Tools.Time;

namespace Valkyrie.ToRemove.LidgrenNetwork
{
    [Obsolete("Old network", true)]
    class Network : INetwork
    {
        class CallActionOnDispose : IDisposable
        {
            private readonly Action _disposeMethod;

            public CallActionOnDispose(Action disposeMethod)
            {
                _disposeMethod = disposeMethod;
            }

            public void Dispose()
            {
                _disposeMethod();
            }
        }

        void ErrorDefaultHandler(ErrorNotification message, IConnection connection)
        {
            Log.LogError("Error {0}: {1}", message.Code, message.Description);
        }

        #region Private fields

        private readonly ITimeService _timeSystem;
        readonly NetPeer _netPeer;

        private readonly Dictionary<NetConnection, BaseConnection> _lidgrenConnections = new Dictionary<NetConnection, BaseConnection>();

        private readonly Func<IConnection, bool> _canCreateConnection;

        #endregion

        public ILogInstance Log { get; private set; }
        
        public Network(ITimeService timeSystem, ILogService logSystem, int port, int conectionsCount, string appIdentifier, Func<IConnection, bool> canCreateConnection)
        {
            RequestTimeout = 30f;

            _appIdentifier = appIdentifier;
            _timeSystem = timeSystem;
            _canCreateConnection = canCreateConnection;

            Log = logSystem.GetLog("Valkyrie.Network");

            Subscribe<ErrorNotification>(ErrorDefaultHandler);

            if (SynchronizationContext.Current == null)
                SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            var configuration = new NetPeerConfiguration(appIdentifier)
            {
                AutoFlushSendQueue = true
            };
            if (port > 0)
            {
                configuration.MaximumConnections = conectionsCount;
                configuration.Port = port;

                _netPeer = new NetServer(configuration);
            }
            else
            {
                _netPeer = new NetClient(configuration);
            }
            _netPeer.RegisterReceivedCallback(ReceiveIncomingMessage, SynchronizationContext.Current);
            _netPeer.Start();
        }

        #region Implementation of INetwork

        public float RequestTimeout { get; set; }

        public IConnection Connect(string host, int port)
        {
            var hailMessage = _netPeer.CreateMessage(_appIdentifier);
            var internalConnection = _netPeer.Connect(host, port, hailMessage);
            return new BaseConnection(internalConnection, this);
        }

        #endregion

        struct MessageStore
        {
            public NetIncomingMessage Message;
            public BaseConnection Connection;
        }

        readonly List<MessageStore> _msgs = new List<MessageStore>();

        class ResponseHandleData
        {
            public ITimer Timer;
            public Action<BaseNetMessage> Callback;
        }

        readonly Dictionary<int, ResponseHandleData> _responseReceivers = new Dictionary<int, ResponseHandleData>();

        public void Update()
        {
            MessageStore[] messagesToProcess;
            lock (_msgs)
            {
                if(_msgs.Count == 0)
                    return;

                messagesToProcess = _msgs.ToArray();
                _msgs.Clear();
            }

            foreach (var store in messagesToProcess)
            {
                var connection = store.Connection;
                var msg = new BaseNetMessage();
                store.Message.ReadAllFields(msg, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);

                HandleNetworkMessage(msg, connection);
            }
            
            _netPeer.FlushSendQueue();
        }

        private void HandleNetworkMessage(BaseNetMessage msg, BaseConnection connection)
        {
            if (msg.IsResponse)
            {
                ResponseHandleData callbackData;
                lock (_responseReceivers)
                    if (_responseReceivers.TryGetValue(msg.UniqueId, out callbackData))
                        _responseReceivers.Remove(msg.UniqueId);
                if (callbackData != null)
                    try
                    {
                        callbackData.Timer.Dispose();
                        callbackData.Callback(msg);
                    }
                    catch (Exception exception)
                    {
                        Log.LogError("Exception in network response handler: {0} {1}", exception.Message, msg);
                    }
                else
                    Log.LogWarn("Can not handle response {0}", msg.GetData().GetType().Name);
            }
            else
            {
                if (string.IsNullOrEmpty(msg.ResponseType))
                {
                    if (!InvokeIncomingEvent(msg.GetData(), connection))
                        Send(
                            BaseNetMessage.CreateError(msg, (int) HttpStatusCode.BadRequest,
                                "Unknown event"), connection);
                }
                else
                {
                    Action<object> handler = response => { Send(BaseNetMessage.CreateResponse(msg, response), connection); };
                    var responseType = Type.GetType(msg.ResponseType);
                    if (!Invoke(msg.GetData(), connection, responseType, handler))
                        Send(
                            BaseNetMessage.CreateError(msg, (int) HttpStatusCode.BadRequest,
                                "Unknown request"), connection);
                }
            }
        }

        public void Send(BaseNetMessage netMessage, BaseConnection connection)
        {
            var internalMsg = _netPeer.CreateMessage();
            internalMsg.WriteAllFields(netMessage, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);

            _netPeer.SendMessage(internalMsg, connection.InternalConnection, netMessage.Delivery, netMessage.Channel);
        }

        public void Send(BaseNetMessage netMessage, BaseConnection connection, Action<BaseNetMessage> callback)
        {
            var timer = _timeSystem.Default.StartTimer(RequestTimeout);
            timer.Chain(() =>
            {
                lock (_responseReceivers)
                    _responseReceivers.Remove(netMessage.UniqueId);
                InvokeIncomingEvent(
                    new ErrorNotification
                    {
                        Code = (int) HttpStatusCode.RequestTimeout,
                        Description = "Server didn't response in time"
                    }, connection);
            });

            lock (_responseReceivers)
                _responseReceivers.Add(netMessage.UniqueId, new ResponseHandleData {Callback = callback, Timer = timer});

            var internalMsg = _netPeer.CreateMessage();
            internalMsg.WriteAllFields(netMessage, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public);

            _netPeer.SendMessage(internalMsg, connection.InternalConnection, netMessage.Delivery, netMessage.Channel);
        }

        #region Receive messages

        private void ReceiveIncomingMessage(object peer)
        {
            Handle((NetPeer)peer);
        }

        void Handle(NetPeer peer)
        {
            var logger = Log;
            NetIncomingMessage im;
            while ((im = peer.ReadMessage()) != null)
            {
                switch (im.MessageType)
                {
                    case NetIncomingMessageType.VerboseDebugMessage:
                        logger.LogInfo(im.ReadString());
                        break;
                    case NetIncomingMessageType.DebugMessage:
                        logger.LogDebug(im.ReadString());
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        logger.LogWarn(im.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        logger.LogError(im.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        var status = (NetConnectionStatus)im.ReadByte();
                        var reason = im.ReadString();
                        logger.LogDebug(status + ": " + reason);

                        //TODO: handle disconnecting
                        switch (status)
                        {
                            case NetConnectionStatus.None:
                                break;
                            case NetConnectionStatus.InitiatedConnect:
                                break;
                            case NetConnectionStatus.ReceivedInitiation:
                                break;
                            case NetConnectionStatus.RespondedAwaitingApproval:
                                break;
                            case NetConnectionStatus.RespondedConnect:
                                break;
                            case NetConnectionStatus.Connected:
                                EstablishConnectionIfNeed(im, logger);
                                break;
                            case NetConnectionStatus.Disconnecting:
                                break;
                            case NetConnectionStatus.Disconnected:
                                lock (_lidgrenConnections)
                                {
                                    BaseConnection baseConnection;
                                    if (_lidgrenConnections.TryGetValue(im.SenderConnection, out baseConnection))
                                    {
                                        _lidgrenConnections.Remove(baseConnection.InternalConnection);
                                        logger.LogInfo("Closed connection with {0}", im.SenderEndPoint);
                                    }
                                }
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                        break;
                    case NetIncomingMessageType.Data:
                        var connection = EstablishConnectionIfNeed(im, logger);
                        OnIncomingData(im, connection);
                        break;
                    default:
                        logger.LogWarn("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes");
                        break;
                }
            }
        }

        private void OnIncomingData(NetIncomingMessage im, BaseConnection connection)
        {
            lock (_msgs)
                _msgs.Add(new MessageStore {Connection = connection, Message = im});
        }

        private BaseConnection EstablishConnectionIfNeed(NetIncomingMessage im, ILogInstance logSystem)
        {
            lock (_lidgrenConnections)
            {
                BaseConnection bc;
                if (_lidgrenConnections.TryGetValue(im.SenderConnection, out bc))
                    return bc;

                bc = new BaseConnection(im.SenderConnection, this);
                if (_canCreateConnection == null || _canCreateConnection(bc))
                {
                    _lidgrenConnections.Add(bc.InternalConnection, bc);
                    logSystem.LogInfo("Established incoming connection with {0}", bc.EndPoint);
                }
                else
                {
                    im.SenderConnection.Disconnect("BlockedByApplication");
                    logSystem.LogInfo("Incoming connection with {0} blocked by outer logic", bc.EndPoint);
                    bc = null;
                }
                return bc;
            }
        }

        #endregion

        #region Implementation of INetwork

        readonly Dictionary<object, object> _mapToRemove = new Dictionary<object, object>();
        readonly Dictionary<Type, Action<object, IConnection>> _eventsHandlers = new Dictionary<Type, Action<object, IConnection>>();
        readonly Dictionary<KeyValuePair<Type, Type>, Action<object, IConnection, Action<object>>> _requestsHandlers = new Dictionary<KeyValuePair<Type, Type>, Action<object, IConnection, Action<object>>>();
        private readonly string _appIdentifier;

        public IDisposable Subscribe<TEvent>(Action<TEvent, IConnection> eventHandler)
        {
            Action<object, IConnection> handler = (message, connection) => eventHandler((TEvent) message, connection);
            _mapToRemove.Add(eventHandler, handler);

            if (!_eventsHandlers.ContainsKey(typeof (TEvent)))
                _eventsHandlers.Add(typeof (TEvent), handler);
            else
                Log.LogError("Can not register event handler for event {0}, already registered", typeof(TEvent).Name);

            return new CallActionOnDispose(() => Unsubscribe(eventHandler));
        }

        public void Unsubscribe<TEvent>(Action<TEvent, IConnection> eventHandler)
        {
            object toRemove;
            if (!_mapToRemove.TryGetValue(eventHandler, out toRemove))
            {
                return;
            }
            _mapToRemove.Remove(eventHandler);

            Action<object, IConnection> handlers;
            if (_eventsHandlers.TryGetValue(typeof (TEvent), out handlers) && handlers == (Action<object, IConnection>) toRemove)
                _eventsHandlers.Remove(typeof (TEvent));
        }

        public IDisposable Subscribe<TRequest, TResponse>(Action<TRequest, IConnection, Action<TResponse>> requestHandler)
        {
            Action<object, IConnection, Action<object>> handler = (request, connection, responseHandler)
                => requestHandler((TRequest) request, connection, response => responseHandler(response));
            _mapToRemove.Add(requestHandler, handler);

            var key = new KeyValuePair<Type, Type>(typeof(TRequest), typeof(TResponse));
            if (!_requestsHandlers.ContainsKey(key))
                _requestsHandlers.Add(key, handler);
            else
                Log.LogError("Can not register request handler for request {0} response {1}, already registered", typeof(TRequest).Name, typeof(TResponse).Name);

            return new CallActionOnDispose(() => Unsubscribe(requestHandler));
        }

        public void Unsubscribe<TRequest, TResponse>(Action<TRequest, IConnection, Action<TResponse>> requestHandler)
        {
            object toRemove;
            if (!_mapToRemove.TryGetValue(requestHandler, out toRemove))
            {
                return;
            }

            _mapToRemove.Remove(requestHandler);

            var key = new KeyValuePair<Type, Type>(typeof(TRequest), typeof(TResponse));
            Action<object, IConnection, Action<object>> handlers;
            if (_requestsHandlers.TryGetValue(key, out handlers) && handlers == (Action<object, IConnection, Action<object>>) toRemove)
                _requestsHandlers.Remove(key);
        }

        #endregion

        bool InvokeIncomingEvent(object request, BaseConnection connection)
        {
            Action<object, IConnection> handler;
            if (!_eventsHandlers.TryGetValue(request.GetType(), out handler))
                return false;

            try
            {
                handler(request, connection);
            }
            catch (Exception e)
            {
                Log.LogError("Exception in event handler event {0} data {1} error {2}", request.GetType().Name, request, e.Message);
            }

            return true;
        }

        bool Invoke(object request, BaseConnection connection, Type responseType, Action<object> responseCallback)
        {
            Action<object, IConnection, Action<object>> handler;
            if (
                !_requestsHandlers.TryGetValue(new KeyValuePair<Type, Type>(request.GetType(), responseType),
                    out handler))
                return false;

            try
            {
                handler(request, connection, responseCallback);
            }
            catch (Exception e)
            {
                Log.LogError("Exception in request handler for request {0} response {1} data {2} error {3}", request.GetType().Name, responseType.Name, request, e.Message);
            }
            return true;
        }
    }
}