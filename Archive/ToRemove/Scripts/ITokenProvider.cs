using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Scripts
{
    internal interface ITokenProvider : IEnumerable<Token>, IDisposable
    {
        
    }
}