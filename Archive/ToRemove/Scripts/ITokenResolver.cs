namespace Valkyrie.ToRemove.Scripts
{
    internal interface ITokenResolver
    {
        string GetCategory(string text);
    }
}