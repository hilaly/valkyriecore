using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Valkyrie.ToRemove.Scripts
{
    class TokenReader : ITokenReader
    {
        private readonly Stream _stream;
        private StreamReader _reader;
        private int currentToken = -1;

        public TokenReader(Stream stream)
        {
            _stream = stream;
        }

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<string> GetEnumerator()
        {
            while (true)
            {
                var result = GetNextToken();
                if (string.IsNullOrEmpty(result))
                {
                    _reader.Dispose();
                    _reader = null;
                    yield break;
                }
                yield return result;
            }
        }

        #endregion

        private string GetNextToken()
        {
            if (_reader == null)
            {
                _reader = new StreamReader(_stream);
            }

            var resultString = new StringBuilder();
            if (currentToken >= 0)
            {
                resultString.Append((char) currentToken);
                if (TokenCategorizer.IsSymbol(currentToken))
                {
                    var token = Read();
                    if (TokenCategorizer.IsSymbol(token) && TokenCategorizer.IsBigToken(resultString, token))
                    {
                        resultString.Append((char) token);
                        currentToken = -1;
                    }
                    else if (TokenCategorizer.IsEmpty(token))
                        currentToken = -1;
                    else
                        currentToken = token;
                    return Token.BuildString(resultString);
                }
                currentToken = -1;
            }

            while (!_reader.EndOfStream)
            {
                var token = Read();
                var isEmpty = TokenCategorizer.IsEmpty(token);
                if (isEmpty && resultString.Length > 0)
                    return Token.BuildString(resultString);
                var isSymbol = TokenCategorizer.IsSymbol(token);
                if (isSymbol && resultString.Length > 0)
                {
                    if (TokenCategorizer.IsBigToken(resultString, token))
                        resultString.Append((char) token);
                    else
                        currentToken = token;
                    return Token.BuildString(resultString);
                }
                if (!isEmpty)
                    resultString.Append((char) token);
            }
            return Token.BuildString(resultString);
        }

        int Read()
        {
            if (_reader == null || _reader.EndOfStream)
                return -1;
            var result = _reader.Read();
            Debug.Assert(result != -1);
            return result;
        }

        public void Dispose()
        {
            if (_reader != null)
            {
                _reader.Dispose();
                _reader = null;
            }
        }
    }
}