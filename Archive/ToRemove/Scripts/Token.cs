﻿using System.Text;

namespace Valkyrie.ToRemove.Scripts
{
    class Token
    {
        private Token()
            {}
        
        public string type;
        public string text;

        public static string BuildString(StringBuilder resultString)
        {
            var text = resultString.ToString();
            return text;
        }

        public static Token BuildToken(string text, string category)
        {
            return new Token {type = category, text = text};
        }
    }
}