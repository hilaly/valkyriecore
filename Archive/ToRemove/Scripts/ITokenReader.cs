using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Scripts
{
    internal interface ITokenReader : IEnumerable<string>, IDisposable
    {
    }
}