using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Valkyrie.ToRemove.Scripts
{
    [Serializable]
    [DataContract]
    class ScriptParserDescription
    {
        [DataMember] public List<TokenCategoryDescription> TokenCategories;
    }
}