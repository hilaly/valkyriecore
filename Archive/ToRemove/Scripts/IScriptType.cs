﻿namespace Valkyrie.ToRemove.Scripts
{
    public interface IScriptType
    {
        void AddMethod(string name);
    }
}