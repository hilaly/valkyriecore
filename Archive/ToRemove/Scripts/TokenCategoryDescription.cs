using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace Valkyrie.ToRemove.Scripts
{
    [Serializable]
    [DataContract]
    class TokenCategoryDescription
    {
        [DataMember] public string RegExpr;
        [DataMember] public string Type;

        public bool IsMeet(string text)
        {
            var ex = new Regex(RegExpr);
            return ex.IsMatch(text);
        }
    }
}