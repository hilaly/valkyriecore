using Valkyrie.ToRemove.Scripts.TestImpl.Nodes;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    class NullAstCompiler : IAstCompiler
    {
        public ICompiledUnit Compile(IAstNode tree)
        {
            return new NullCompiledUnit(tree.ToString());
        }
    }
}