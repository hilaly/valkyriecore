using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    [Serializable]
    class ScriptTokenTreeDescription
    {
        public string GeneratedNode;
        public List<string> SubNodesExpr;
        public string ArgumentsFormat;
        public string GeneratedNodeType;
    }
}