using Valkyrie.ToRemove.Scripts.TestImpl.Nodes;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    internal interface IAstBuilder
    {
        IAstNode Build(ITokenProvider tokenProvider);
    }
}