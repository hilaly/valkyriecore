using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    internal interface ITokenReader : IEnumerable<string>, IDisposable
    {
    }
}