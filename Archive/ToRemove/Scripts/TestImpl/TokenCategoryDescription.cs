using System;
using System.Text.RegularExpressions;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    [Serializable]
    class TokenCategoryDescription
    {
        public string RegExpr;
        public string Type;

        public bool IsMeet(string text)
        {
            var ex = new Regex(RegExpr);
            return ex.IsMatch(text);
        }
    }
}