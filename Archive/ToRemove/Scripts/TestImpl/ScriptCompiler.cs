namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    class ScriptCompiler : IScriptCompiler
    {
        private readonly IAstBuilder _astBuilder;
        private readonly IAstCompiler _astCompiler;

        public ScriptCompiler(IAstBuilder astBuilder, IAstCompiler astCompiler)
        {
            _astBuilder = astBuilder;
            _astCompiler = astCompiler;
        }

        public ICompiledUnit Compile(ITokenProvider tokenProvider)
        {
            var ast = _astBuilder.Build(tokenProvider);
            var compiled = _astCompiler.Compile(ast);
            return compiled;
        }
    }
}