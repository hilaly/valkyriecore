﻿namespace Valkyrie.ToRemove.Scripts.TestImpl.Nodes
{
    class NullAstNode : IAstNode
    {
        public string _info;

        public override string ToString()
        {
            return _info;
        }
    }
}
