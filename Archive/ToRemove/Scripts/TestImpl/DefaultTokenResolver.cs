using System.Linq;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    class DefaultTokenResolver : ITokenResolver
    {
        private readonly ScriptParserDescription _description;

        public DefaultTokenResolver(ScriptParserDescription description)
        {
            _description = description;
        }

        public string GetCategory(string text)
        {
            var category = _description.TokenCategories.FirstOrDefault(u => u.IsMeet(text));
            return category == null ? "UNKNOWN" : category.Type;
        }
    }
}