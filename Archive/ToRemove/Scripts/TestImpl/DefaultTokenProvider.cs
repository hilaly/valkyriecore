using System.Collections;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    class DefaultTokenProvider : ITokenProvider
    {
        private readonly ITokenReader _tokenReader;
        private readonly ITokenResolver _tokenResolver;

        public DefaultTokenProvider(ITokenReader tokenReader, ITokenResolver tokenResolver)
        {
            _tokenReader = tokenReader;
            _tokenResolver = tokenResolver;
        }

        public void Dispose()
        {
            _tokenReader.Dispose();
        }
        
        #region IEnumerable

        public IEnumerator<Token> GetEnumerator()
        {
            foreach (string tokenText in _tokenReader)
            {
                yield return Token.BuildToken(tokenText, _tokenResolver.GetCategory(tokenText));
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        #endregion
    }
}