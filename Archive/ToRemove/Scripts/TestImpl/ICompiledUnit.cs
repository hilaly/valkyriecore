namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    internal interface ICompiledUnit
    {
        string GetInfo();
    }
}