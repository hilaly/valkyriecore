using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    [Serializable]
    class ScriptParserDescription
    {
        public List<TokenCategoryDescription> TokenCategories;
        public List<ScriptTokenTreeDescription> TokenTrees;
    }
}