using System.Collections.Generic;
using System.Linq;
using Valkyrie.ToRemove.Scripts.TestImpl.Nodes;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    class DefaultAstBuilder : IAstBuilder
    {
        private readonly ScriptParserDescription _description;

        public DefaultAstBuilder(ScriptParserDescription description)
        {
            _description = description;
        }

        public IAstNode Build(ITokenProvider tokenProvider)
        {
            var tokens = tokenProvider.ToList();

            var firstDesc = new List<ScriptTokenTreeDescription>() { _description.TokenTrees[0] };

            return TryBuild(firstDesc, _description.TokenTrees, tokens);
        }

        private IAstNode TryBuild(List<ScriptTokenTreeDescription> tryToFindDescs, List<ScriptTokenTreeDescription> tokenTrees, List<Token> tokens)
        {
            foreach(var currentDesc in tryToFindDescs)
            {

            }

            return new NullAstNode { _info = "Not implemented ASTbuilder" };
        }
    }
}