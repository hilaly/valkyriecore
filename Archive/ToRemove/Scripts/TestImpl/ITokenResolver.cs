namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    internal interface ITokenResolver
    {
        string GetCategory(string text);
    }
}