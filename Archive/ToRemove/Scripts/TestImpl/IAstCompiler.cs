using Valkyrie.ToRemove.Scripts.TestImpl.Nodes;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    interface IAstCompiler
    {
        ICompiledUnit Compile(IAstNode tree);
    }
}