namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    internal class NullCompiledUnit : ICompiledUnit
    {
        private readonly string _info;

        public NullCompiledUnit(string info)
        {
            _info = info;
        }

        public string GetInfo()
        {
            return _info;
        }
    }
}