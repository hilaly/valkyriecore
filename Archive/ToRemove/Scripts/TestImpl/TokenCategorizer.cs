﻿using System.Text;

namespace Valkyrie.ToRemove.Scripts.TestImpl
{
    static class TokenCategorizer
    {
        public const string PointCategory = "point";
        
        #region TokenReader

        public static bool IsBigToken(StringBuilder resultString, int token)
        {
            if (resultString.Length != 1)
                return false;
            var nt = new string(new char[2] {resultString[0], (char) token});
            switch (nt)
            {
                case "==":
                case "<=":
                case ">=":
                case "!=":
                case "+=":
                case "-=":
                case "*=":
                case "/=":
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsSymbol(int token)
        {
            var s = (char) token;
            switch (s)
            {
                case '(':
                case ')':
                case ':':
                case ',':
                case '.':
                case '-':
                case '+':
                case '=':
                case '*':
                case '/':
                case '<':
                case '>':
                case '!':
                case '[':
                case ']':
                case '{':
                case '}':
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsEmpty(int token)
        {
            switch (token)
            {
                case -1:
                case ' ':
                case '\t':
                case '\n':
                case '\r':
                case '\f':
                case '\v':
                    return true;
                default:
                    return false;
            }
        }

        #endregion
    }
}