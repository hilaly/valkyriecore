using System;
using Valkyrie.Di;
using Valkyrie.Threading.Async;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    class CommunicationProvider : ICommunicationProvider
    {
#pragma warning disable 649
        [Inject] private IContainer _container;
#pragma warning restore 649

        public IMessageChannel<TEvent> CreateChannel<TEvent>()
        {
            return _container.Resolve<IMessageChannel<TEvent>>();
        }

        public ICompositeDisposable CreateMultiSubscription()
        {
            return _container.Resolve<ICompositeDisposable>();
        }
    }
}