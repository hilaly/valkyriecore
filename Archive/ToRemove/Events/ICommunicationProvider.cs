using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    public interface ICommunicationProvider
    {
        IMessageChannel<TEvent> CreateChannel<TEvent>();

        ICompositeDisposable CreateMultiSubscription();
    }
}