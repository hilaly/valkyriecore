using System;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    class Subscription<T> : ISubscriber<T>, IDisposable
    {
        private readonly Action<T, object> _message;
        private readonly Action<Subscription<T>> _this;
        private readonly Action<IDisposable> _subscriber;

        public Subscription(Action<T, object> message, Action<Subscription<T>> @this, Action<IDisposable> subscriber)
        {
            _message = message;
            _this = @this;
            _subscriber = subscriber;
        }

        #region Implementation of IDisposable

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _this(this);
            _subscriber(this);
        }

        #endregion

        #region Implementation of ISubscriber<in T>

        public void Receive(T msg, object sender)
        {
            _message(msg, sender);
        }

        #endregion
    }
}