using System;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    public interface IEventPublisher
    {
        void Send<TEvent>(TEvent message, object sender);

        void Send<TRequest, TResponse>(TRequest request, object sender,
            Action<TRequest, object, TResponse> responseHandler);
    }
}