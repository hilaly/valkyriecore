using System;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    public interface ISubscriber<in TEvent>
    {
        // ReSharper disable UnusedMemberInSuper.Global
        void Receive(TEvent msg, object sender);
        // ReSharper restore UnusedMemberInSuper.Global
    }
}