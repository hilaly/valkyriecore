using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    class MessageChannel<T> : IMessageChannel<T>
    {
        readonly HashSet<Subscription<T>> _subscriptions = new HashSet<Subscription<T>>();

        private void OnDestroySubscription(Subscription<T> s)
        {
            _subscriptions.Remove(s);
        }
        
        #region Implementation of IPublisher<in T>

        public void Send(T message, object sender)
        {
            var k = _subscriptions.ToArray();
            foreach (var s in k)
                s.Receive(message, sender);
        }

        #endregion

        #region Implementation of IMessageChannel<T>

        public IDisposable Subscribe(Action<T, object> receiveMessage, Action<IDisposable> onCloseChannel)
        {
            var s = new Subscription<T>(receiveMessage, OnDestroySubscription, onCloseChannel);
            _subscriptions.Add(s);
            return s;
        }

        #endregion

        public void Dispose()
        {
            var s = _subscriptions.ToArray();
            foreach (var subscription in s)
                subscription.Dispose();
        }
    }
}