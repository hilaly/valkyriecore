using System;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    public interface IMessageChannel<TEvent> : IDisposable
    {
        void Send(TEvent message, object sender);
        IDisposable Subscribe(Action<TEvent, object> receiveMessage, Action<IDisposable> onCloseChannel);
    }
}