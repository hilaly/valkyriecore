using System;
using Valkyrie.ToRemove.Rx.Old;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    public interface IEventSystem
    {
        IDisposable Subscribe<TEvent>(Action<TEvent, object> eventHandler);
        IDisposable Subscribe<TRequest, TResponse>(Action<TRequest, object, Action<TResponse>> requestHandler);
        IRxThread<TEvent, object> GetEventThread<TEvent>();
    }
}