using System;
using System.Collections.Generic;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;
using Valkyrie.ToRemove.Rx.Old;

namespace Valkyrie.ToRemove.Events
{
    [Obsolete("Will be removed", true)]
    // ReSharper disable ClassNeverInstantiated.Global
    class EventSystem : CommunicationProvider, IEventSystem, IEventPublisher
        // ReSharper restore ClassNeverInstantiated.Global
    {
        #region Private fields
        
        private readonly ILogInstance _logInstance;
        private readonly IRxFactory _rxFactory;
        readonly Dictionary<KeyValuePair<Type, Type>, HashSet<object>> _requestsHandlers = new Dictionary<KeyValuePair<Type, Type>, HashSet<object>>();


        readonly Dictionary<Type, HashSet<object>> _eventsHandlers = new Dictionary<Type, HashSet<object>>();

        readonly Dictionary<Type, IRxThread> _eventThreads = new Dictionary<Type, IRxThread>();

        #endregion

        #region Ctor

        public EventSystem(ILogService logSystem, IRxFactory rxFactory)
        {
            _rxFactory = rxFactory;
            _logInstance = logSystem.GetLog("Valkyrie.EventSystem");
        }

        #endregion

        private IRxEvent<TEvent, object> GetMainRxThread<TEvent>()
        {
            IRxThread thread;
            if (!_eventThreads.TryGetValue(typeof(TEvent), out thread))
            {
                thread = new EventThread<TEvent, object>();
                _eventThreads.Add(typeof(TEvent), thread);
            }
            return (IRxEvent<TEvent, object>)thread;
        }
        
        #region IEventPublisher

        public virtual void Send<TEvent>(TEvent message, object sender)
        {
            _logInstance.LogInfo("Sending message {0} from {1}", message, sender);

            GetMainRxThread<TEvent>().Set(message, sender);
            
            HashSet<object> handlers;
            if (!_eventsHandlers.TryGetValue(typeof(TEvent), out handlers))
                return;

            foreach (var handler in handlers)
                ((Action<TEvent, object>) handler)(message, sender);
        }

        public virtual void Send<TRequest, TResponse>(TRequest request, object sender, Action<TRequest, object, TResponse> responseHandler)
        {
            _logInstance.LogInfo("Sending request {0} from {1}", request, sender);

            HashSet<object> handlers;
            if (!_requestsHandlers.TryGetValue(new KeyValuePair<Type, Type>(typeof(TRequest), typeof(TResponse)), out handlers))
                return;

            Action<TResponse> callback = response => responseHandler(request, sender, response);

            foreach (var handler in handlers)
                ((Action<TRequest, object, Action<TResponse>>)handler)(request, sender, callback);
        }

        #endregion

        #region IEventSystem

        public IDisposable Subscribe<TEvent>(Action<TEvent, object> eventHandler)
        {
            if (_eventsHandlers.ContainsKey(typeof (TEvent)))
                _eventsHandlers[typeof (TEvent)].Add(eventHandler);
            else
                _eventsHandlers.Add(typeof(TEvent), new HashSet<object> { eventHandler });

            return Disposable.Create(() => Unsubscribe(eventHandler));
        }

        public IDisposable Subscribe<TRequest, TResponse>(Action<TRequest, object, Action<TResponse>> requestHandler)
        {
            var key = new KeyValuePair<Type, Type>(typeof(TRequest), typeof(TResponse));
            if (_requestsHandlers.ContainsKey(key))
                _requestsHandlers[key].Add(requestHandler);
            else
                _requestsHandlers.Add(key, new HashSet<object> {requestHandler});


            return Disposable.Create(() => Unsubscribe(requestHandler));
        }

        void Unsubscribe<TEvent>(Action<TEvent, object> eventHandler)
        {
            HashSet<object> handlers;
            if (_eventsHandlers.TryGetValue(typeof(TEvent), out handlers))
                handlers.Remove(eventHandler);
        }

        void Unsubscribe<TRequest, TResponse>(Action<TRequest, object, Action<TResponse>> requestHandler)
        {
            var key = new KeyValuePair<Type, Type>(typeof(TRequest), typeof(TResponse));
            HashSet<object> handlers;
            if (_requestsHandlers.TryGetValue(key, out handlers))
                handlers.Remove(requestHandler);
        }

        #endregion

        public IRxThread<TEvent, object> GetEventThread<TEvent>()
        {
            return _rxFactory.MirrorThread(GetMainRxThread<TEvent>());
        }
    }
}