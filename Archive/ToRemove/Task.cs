using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Will be removed", true)]
    class Task : ITask
    {
        public TaskManager _creator;

        public Action _work;
        public TaskState _state = TaskState.Undefined;
        public Task _parent;
        public int _priority;
        public bool _mainThread;
        public float _delay;
        
        public TaskState State { get { return _state; } }
        public ITask Parent { set { _parent = value as Task; } }
        public int Priority { set { _priority = value; } }
        public bool ExecuteOnMainThread { set { _mainThread = value; } }
        public void Perform()
        {
            _creator.Flush(this);
        }

        public void Cancel()
        {
            _creator.Cancel(this);
        }

        public float Delay { set { _delay = value; } }
    }
}