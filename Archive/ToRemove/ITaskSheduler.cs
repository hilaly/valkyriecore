using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Will be removed", true)]
    public interface ITaskSheduler
    {
        void Wait(ITask task);
    }
}