using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Will be removed", true)]
    public enum TaskState
    {
        Undefined,
        Queued,
        InProgress,
        Finished
    }
}