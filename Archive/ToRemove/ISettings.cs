﻿using System.Threading;

namespace Valkyrie.ToRemove
{
    interface ISettings
    {
        string ApplicationId { get; }
        LockRecursionPolicy LockPolicy { get; }
        int BackgroundThreadsCount { get; }
        string ViewsPathFormat { get; }

        string LogPath { get; }
        bool ConsoleLog { get; }
        string NetworkLogUrl { get; }

        bool UseEcs { get; set; }
        bool ResolveEcsSystems { get; set; }
    }
}