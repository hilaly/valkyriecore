using System;
using System.Diagnostics;

namespace Valkyrie.ToRemove
{
    [Obsolete("Need remove", true)]
    class DummyDiagnostics : IDiagnostics
    {
        public void Assert(bool condition, string message)
        {
            Debug.Assert(condition, message);
        }
    }
}