using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Will be removed", true)]
    public interface ITask
    {
        /// <summary>
        /// Used only on other threads
        /// </summary>
        float Delay { set; }
        /// <summary>
        /// Dont use, because not implemented
        /// </summary>
        int Priority { set; }
        /// <summary>
        /// Dont use, because not implemented
        /// </summary>
        ITask Parent { set; }
        /// <summary>
        /// Track current state of task
        /// </summary>
        TaskState State { get; }
        bool ExecuteOnMainThread { set; }

        void Perform();
        void Cancel();
    }
}