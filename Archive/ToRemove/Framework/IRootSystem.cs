﻿using System.Collections.Generic;

namespace Valkyrie.ToRemove.Framework
{
    public interface IRootSystem
    {
        IEnumerable<ISystem> Systems { get; }

        void Add(ISystem module);
        void Remove(ISystem module);


        void Update(float dt);
    }
}
