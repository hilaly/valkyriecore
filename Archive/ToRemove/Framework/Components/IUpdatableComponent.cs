﻿namespace Valkyrie.ToRemove.Framework.Components
{
    public interface IUpdatableComponent
    {
        void UpdateComponent(float deltaTime);
    }
}