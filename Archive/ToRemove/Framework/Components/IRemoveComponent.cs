﻿using System;

namespace Valkyrie.ToRemove.Framework.Components
{
    public interface IRemoveComponent
    {
        event Action<IRemoveComponent> OnRemove;

        bool Remove { get; set; }

        void Destroy();
    }
}
