﻿namespace Valkyrie.ToRemove.Framework
{
    public interface IUpdatable
    {
        void Update(float dt);
    }
}
