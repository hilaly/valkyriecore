﻿using System;

namespace Valkyrie.ToRemove.Framework
{
    public interface ISystem
    {
        Type[] NodesTypes { get; }

        void Add(string id, INode node);
        void Remove(string id, INode node);
    }
}
