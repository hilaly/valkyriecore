﻿using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Framework
{
    public interface IQuerySystem : ISystem
    {
        IEnumerable<T> Get<T>() where T : INode;
        IEnumerable<T> Get<T>(Func<T, bool> expression) where T : INode;
        T Get<T>(string id) where T : INode;
    }
}
