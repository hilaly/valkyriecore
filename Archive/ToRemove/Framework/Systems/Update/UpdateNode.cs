﻿using System.Collections.Generic;
using Valkyrie.ToRemove.Framework.Components;

namespace Valkyrie.ToRemove.Framework.Systems.Update
{
    public class UpdateNode : INode
    {
        public UpdateNode(IEnumerable<IUpdatableComponent> components)
        {
            Components = new List<IUpdatableComponent>(components);
        }

        public List<IUpdatableComponent> Components { get; private set; }
    }
}
