﻿using System;

namespace Valkyrie.ToRemove.Framework.Systems.Update
{
    class UpdatableSystem : BaseQuerySystem, IUpdatable
    {
        private static readonly Type[] _nodesTypes = {typeof (UpdateNode)};

        #region Overrides of BaseQuerySystem

        public override Type[] NodesTypes
        {
            get { return _nodesTypes; }
        }

        #endregion

        #region Implementation of IUpdatable

        public void Update(float dt)
        {
            foreach (var node in Get<UpdateNode>())
                foreach (var component in node.Components)
                    component.UpdateComponent(dt);
        }

        #endregion
    }
}
