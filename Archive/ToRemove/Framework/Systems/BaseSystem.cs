﻿using System;
using System.Collections.Generic;

namespace Valkyrie.ToRemove.Framework.Systems
{
    public abstract class BaseSystem<T> : ISystem
        where T : class, INode
    {
        static readonly Type[] _nodesTypes = { typeof(T) };
        public Type[] NodesTypes { get { return _nodesTypes; } }

        private readonly List<T> _nodes = new List<T>();
        protected List<T> Nodes { get { return _nodes; } }

        public virtual void Add(string id, INode node)
        {
            _nodes.Add(node as T);
        }

        public virtual void Remove(string id, INode node)
        {
            _nodes.Remove(node as T);
        }

        protected T Get(string id)
        {
            throw new NotImplementedException();
        }
    }
}
