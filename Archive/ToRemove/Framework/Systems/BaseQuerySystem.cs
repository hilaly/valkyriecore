﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.ToRemove.Framework.Systems
{
    public abstract class BaseQuerySystem : IQuerySystem
    {
        #region fields

        class NodesCache
        {
            public bool Rebuild;
            public List<INode> Nodes;
        }

        readonly Dictionary<string, HashSet<INode>> _nodes = new Dictionary<string, HashSet<INode>>();
        readonly Dictionary<Type, NodesCache> _nodesCash = new Dictionary<Type, NodesCache>();

        #endregion

        #region ISystem

        public abstract Type[] NodesTypes { get; }

        public virtual void Add(string id, INode node)
        {
            HashSet<INode> collection;
            if(!_nodes.TryGetValue(id, out collection))
            {
                collection = new HashSet<INode>();
                _nodes.Add(id, collection);
            }
            collection.Add(node);

            if(_nodesCash.ContainsKey(node.GetType()))
            {
                _nodesCash[node.GetType()].Rebuild = true;
            }
            else
            {
                _nodesCash.Add(node.GetType(), new NodesCache { Rebuild = false, Nodes = new List<INode> { node } });
            }
        }

        public virtual void Remove(string id, INode node)
        {
                _nodesCash[node.GetType()].Rebuild = true;

            HashSet<INode> collection = _nodes[id];
            collection.Remove(node);
            if (collection.Count == 0)
                _nodes.Remove(id);
        }

        #endregion

        void RebuildCash<T>(NodesCache cash)
        {
            cash.Nodes.Clear();
            foreach (var hash in _nodes.Values)
                foreach (var node in hash)
                    if (node is T)
                        cash.Nodes.Add(node);
            cash.Rebuild = false;
        }

        #region IQuerySystem

        public IEnumerable<T> Get<T>() where T : INode
        {
            if (!_nodesCash.ContainsKey(typeof(T)))
                yield break;

            var cash = _nodesCash[typeof(T)];
            if(cash.Rebuild)
                RebuildCash<T>(cash);
            
            for (var i = 0; i < cash.Nodes.Count; ++i)
                yield return (T)cash.Nodes[i];
        }

        public T Get<T>(string id) where T : INode
        {
            HashSet<INode> collection;
            if (_nodes.TryGetValue(id, out collection))
                return (T)collection.FirstOrDefault(u => u is T);
            return default(T);
        }

        [Obsolete("Not optimized version, use Get() version and self check after")]
        public IEnumerable<T> Get<T>(Func<T, bool> expression) where T : INode
        {
            foreach (var e in Get<T>())
            {
                if (expression(e))
                    yield return e;
            }
        }

        #endregion
    }
}
