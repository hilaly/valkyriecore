﻿using Valkyrie.ToRemove.Framework.Components;

namespace Valkyrie.ToRemove.Framework.Systems.Management
{
    public class RemoveNode : INode
    {
        public IRemoveComponent Component { get; private set; }

        public RemoveNode(IRemoveComponent component)
        {
            Component = component;
        }
    }
}
