﻿using System;

namespace Valkyrie.ToRemove.Framework.Systems.Management
{
    class RemoveSystem : BaseQuerySystem, IUpdatable
    {
        readonly IEntityBuilder _builder;
        static readonly Type[] _nodesTypes = new[] { typeof(RemoveNode) };

        public RemoveSystem(IEntityBuilder builder)
        {
            _builder = builder;
        }

        public override Type[] NodesTypes { get { return _nodesTypes; } }
        
        public void Update(float dt)
        {
            foreach (var node in Get<RemoveNode>())
            {
                var removeComponent = node.Component;

                if (!removeComponent.Remove)
                    continue;

                _builder.Destroy(node);
                removeComponent.Destroy();
            }
        }
    }
}
