﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Valkyrie.ToRemove.Framework.Components;

namespace Valkyrie.ToRemove.Framework.Internal
{
    class EntityBuilder : IEntityBuilder
    {
        readonly IRootSystem _rootSystem;

        public EntityBuilder(IRootSystem rootSystem)
        {
            _rootSystem = rootSystem;
        }

        class Entity
        {
            public string Id;
            public IEnumerable<INode> Nodes;
        }

        readonly List<Entity> _entities = new List<Entity>();

        #region Implementation of IEntityBuilder

        public void Build(string id, IEnumerable<INode> nodes, IEnumerable<object> components)
        {
            Debug.Assert(components.Any(u => u is IRemoveComponent), "Entity must have IRemoveComponent");
            _entities.Add(new Entity { Id = id, Nodes = nodes });

            foreach (var system in _rootSystem.Systems)
                foreach (var node in nodes.Where(node => system.NodesTypes.Contains(node.GetType())))
                    system.Add(id, node);
        }

        public void Destroy(INode node)
        {
            foreach(var e in _entities.Where(u => u.Nodes.Contains(node)).ToList())
            {
                foreach (var system in _rootSystem.Systems)
                    foreach (var n in e.Nodes.Where(n => system.NodesTypes.Contains(n.GetType())))
                        system.Remove(e.Id, n);

                _entities.Remove(e);
            }
        }

        #endregion
    }
}