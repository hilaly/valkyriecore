﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Tools;
using Valkyrie.Tools.Logs;

namespace Valkyrie.ToRemove.Framework.Internal
{
    class RootSystem : IRootSystem
    {
        private readonly ILogInstance _log;
        private readonly IProfiler _profiler;
        private readonly List<ISystem> _modules = new List<ISystem>();

        public RootSystem(ILogService system, IProfiler profiler)
        {
            _log = system.GetLog("Valkyrie.RootSystem");
            _profiler = profiler;
        }

        #region Implementation of IRootModule

        public IEnumerable<ISystem> Systems { get { return _modules; } }
        public void Add(ISystem module)
        {
            _log.LogInfo("Add {0} system", module.GetType().Name);
            _modules.Add(module);
        }

        public void Remove(ISystem module)
        {
            if (_modules.Remove(module))
                _log.LogInfo("Remove {0} system", module.GetType().Name);
        }

        #endregion

        public void Update(float dt)
        {
            foreach(var u in _modules.OfType<IUpdatable>())
            {
                _profiler.BeginSample(u.GetType().Name);
                try
                {
                    u.Update(dt);
                }
                catch(Exception e)
                {
                    _log.LogError("Exception {1} in {0}: {2}", u.GetType().Name, e, e.Message);
                }
                _profiler.EndSample();
            }
        }
    }
}