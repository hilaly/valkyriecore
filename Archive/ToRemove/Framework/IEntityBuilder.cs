﻿using System.Collections.Generic;

namespace Valkyrie.ToRemove.Framework
{
    public interface IEntityBuilder
    {
        void Build(string id, IEnumerable<INode> nodes, IEnumerable<object> components);
        void Destroy(INode node);
    }
}