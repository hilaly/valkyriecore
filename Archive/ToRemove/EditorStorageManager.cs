﻿using System;
using System.IO;

namespace Valkyrie.ToRemove
{
    [Obsolete("Need remove", true)]
    class EditorStorageManager : IStorage
    {
        public byte[] ReadFile(string path)
        {
            return File.ReadAllBytes(path);
        }

        public void WriteFile(string path, byte[] bytes)
        {
            string directory = new FileInfo(path).Directory.FullName;
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            File.WriteAllBytes(path, bytes);
        }
    }
}
