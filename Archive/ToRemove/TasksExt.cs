using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("To remove", true)]
    public static class TasksExt
    {
        public static bool IsFinished(this ITask task)
        {
            switch (task.State)
            {
                case TaskState.Undefined:
                case TaskState.Finished:
                    return true;
                case TaskState.Queued:
                case TaskState.InProgress:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}