using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Will be removed", true)]
    public interface ITaskManager
    {
        ITask Create(Action workFunction);
    }
}