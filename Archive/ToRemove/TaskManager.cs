using System;
using System.Collections.Generic;
using System.Threading;
using Valkyrie.Tools;
using Valkyrie.Tools.Logs;
using Valkyrie.Tools.Time;

namespace Valkyrie.ToRemove
{
    [Obsolete("Will be removed", true)]
    class TaskManager : ITaskManager, ITaskSheduler
    {
        const int DefaultPriority = 100;

        readonly ILogInstance _log;
        private readonly ITimeService _timeSystem;

        readonly Pool<Task> _pool;

        readonly List<Task> _delayed = new List<Task>();
        List<Task> _actions = new List<Task>();
        List<Task> _currentActions = new List<Task>();

        public TaskManager(ILogService logSystem, ITimeService timeSystem, ISettings settings)
        {
            _timeSystem = timeSystem;
            _log = logSystem.GetLog("Valkyrie.Execution");

            ThreadPool.SetMaxThreads(settings.BackgroundThreadsCount, 0);

            _pool = new Pool<Task>(100, InitTask, DestroyTask);
        }

        public void Cancel(Task task)
        {
            if (task._mainThread)
            {
                if (task._delay != 0)
                {
                    lock(_delayed)
                        if(_delayed.Remove(task))
                            return;
                }
                else
                {
                    lock(_actions)
                        if(_actions.Remove(task))
                            return;
                }
            }
            else
            {
                //TODO: implement remove non main thread task
            }
        }

        public void Flush(Task task)
        {
            if (task._mainThread)
            {
                if (task._delay != 0)
                {
                    task._delay += _timeSystem.TimeSinceStartup.Seconds;
                    lock (_delayed)
                        _delayed.Add(task);
                }
                else
                    lock (_actions)
                        _actions.Add(task);

                task._state = TaskState.Queued;
            }
            else
            {
                task._state = TaskState.Queued;
                ThreadPool.QueueUserWorkItem(ExecuteHandleExceptions, task);
            }
        }

        #region ITaskManager

        public ITask Create(Action work)
        {
            Task result;
            lock (_pool)
                result = _pool.New();
            result._work = work;
            return result;
        }

        #endregion

        #region ITaskSheduler

        public void Wait(ITask task)
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            lock (_actions)
            {
                var temp = _currentActions;
                _currentActions = _actions;
                _actions = temp;
            }
            lock (_delayed)
            {
                var time = _timeSystem.TimeSinceStartup.Seconds;
                for (int i = 0; i < _delayed.Count; )
                {
                    var temp = _delayed[i];
                    if (temp._delay <= time)
                    {
                        _currentActions.Add(temp);

                        _delayed[i] = _delayed[_delayed.Count - 1];
                        _delayed.RemoveAt(_delayed.Count - 1);
                    }
                    else
                        ++i;
                }
            }

            foreach (var task in _currentActions)
                ExecuteHandleExceptions(task);
            _currentActions.Clear();
        }

        void ExecuteHandleExceptions(object action)
        {
            var task = (Task)action;
            try
            {
                task._state = TaskState.InProgress;
                task._work();
                task._state = TaskState.Finished;

                //TODO: call finish event
            }
            catch (Exception e)
            {
                _log.LogError("Exception in task {0}: {1}, {2}", task, e.Message, e.StackTrace);
            }
            finally
            {
                lock (_pool)
                    _pool.Store(task);
            }
        }

        void Execute(object action)
        {
            var task = (Task) action;
            task._state = TaskState.InProgress;
            task._work();
            task._state = TaskState.Finished;
            lock (_pool)
                _pool.Store(task);
        }

        #endregion

        #region For Pool

        private void DestroyTask(Task task)
        {
            task._work = null;
            task._state = TaskState.Undefined;
        }

        private void InitTask(Task task)
        {
            task._creator = this;
            task._parent = null;
            task._priority = DefaultPriority;
            task._mainThread = false;
            task._delay = 0;
        }

        #endregion
    }
}