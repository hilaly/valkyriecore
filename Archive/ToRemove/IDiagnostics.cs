﻿using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Need remove", true)]
    public interface IDiagnostics
    {
        void Assert(bool condition, string message);
    }
}
