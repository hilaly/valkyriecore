﻿using System;

namespace Valkyrie.ToRemove
{
    [Obsolete("Need remove", true)]
    public interface IStorage
    {
        void WriteFile(string path, byte[] bytes);
        byte[] ReadFile(string path);
    }
}
