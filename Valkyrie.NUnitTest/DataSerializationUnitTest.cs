using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using NUnit.Framework;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class DataSerializationUnitTest
    {
        #region classes

        [Contract]
        class EnumHolder
        {
            public enum EnumValue
            {
                Undefined,
                Defined,
            }

            public EnumValue Value;
        }

        [Contract()]
        class HolderHolder
        {
            [ContractMember(1)] public ByteFieldHolder Holder { get; set; }
        }

        [Contract]
        class ByteFieldHolder
        {
            [ContractMember(1)] public byte Field;
            [ContractMember(2)] public ushort UShort; 

            public override bool Equals(object obj)
            {
                var holder = obj as ByteFieldHolder;
                return holder != null && holder.Field == Field && holder.UShort == UShort;
            }

            protected bool Equals(ByteFieldHolder other)
            {
                return Field == other.Field;
            }

            public override int GetHashCode()
            {
                return Field.GetHashCode();
            }
        }

        [Contract]
        class IntFieldHolder : ByteFieldHolder
        {
            [ContractMember(3)] public int Int;
        }

        [Contract]
        class FloatFieldHolder : IntFieldHolder
        {
            [ContractMember(4)] public float Float { get; set; }
        }

        [Contract]
        class CompressedFloatFieldHolder : FloatFieldHolder
        {
            [ContractMember(5)]
            [CompressedFloat(CompessedType.Byte, MaxValue = 10, MinValue = -10)]
            public float CompressedFloatByte { get; set; }

            [ContractMember(6)]
            [CompressedFloat(CompessedType.Short, MaxValue = 10, MinValue = -10)]
            public float CompressedFloatShort { get; set; }
        }

        [Contract]
        class StringFieldHolder : FloatFieldHolder
        {
            [ContractMember(5)] public string String;
        }

        [Contract(7, 0)]
        class ListHolder
        {
            [ContractMember(2)] public List<int> Int = new List<int>();

            [ContractMember(3)] [CompressedFloat(CompessedType.Byte, MaxValue = 20, MinValue = -20)]
            public List<float> Float = new List<float>();

            [ContractMember(4)] public List<ByteFieldHolder> Holder = new List<ByteFieldHolder>();
        }

        [Contract(7, 0)]
        class ArrayHolder
        {
            [ContractMember(2)] public int[] Int;
            [ContractMember(4)] public ByteFieldHolder[] Holder;
        }

        [Contract(7, 0)]
        class DictionaryHolder
        {
            [ContractMember(2)] public Dictionary<int, int> Int { get; set; }
            [ContractMember(4)] public Dictionary<int, ByteFieldHolder> Holder;
        }

        #endregion

        public void TestEnumSerialization()
        {
            var serializer = new ContractSerializer();

            var source = new EnumHolder {Value = EnumHolder.EnumValue.Defined};
            var destination = (EnumHolder) serializer.Deserialize(serializer.Serialize(source, true));
            Assert.AreEqual(source.Value, destination.Value);

            source.Value = EnumHolder.EnumValue.Undefined;
            destination = (EnumHolder) serializer.Deserialize(serializer.Serialize(source, true));
            Assert.AreEqual(source.Value, destination.Value);
        }

        [TestCase(0)]
        [TestCase(5)]
        [TestCase(9)]
        public void TestByteSerialization(byte value)
        {
            var serializer = new ContractSerializer();

            var source = new ByteFieldHolder {Field = value, UShort = value};
            var destination = (ByteFieldHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Field, destination.Field);
        }

        [TestCase(0)]
        [TestCase(5)]
        [TestCase(9)]
        public void TestDataSerialization(byte value)
        {
            var serializer = new ContractSerializer();

            var source = new HolderHolder() {Holder = new ByteFieldHolder() {Field = value, UShort = value}};
            var destination = (HolderHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Holder, destination.Holder);
            Assert.AreEqual(source.Holder.Field, destination.Holder.Field);
        }

        [TestCase(0)]
        [TestCase(5)]
        [TestCase(9)]
        public void TestIntSerialization(int value)
        {
            var serializer = new ContractSerializer();

            var source = new IntFieldHolder {Field = (byte) value, Int = value, UShort = (ushort) value};
            var destination = (IntFieldHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Field, destination.Field);
            Assert.AreEqual(source.Int, destination.Int);
        }

        [TestCase(0)]
        [TestCase(5)]
        [TestCase(9)]
        public void TestFloatSerialization(float value)
        {
            var serializer = new ContractSerializer();

            var source = new FloatFieldHolder {Field = (byte) value, Int = (int) value, Float = value, UShort = (ushort) value};
            var destination = (FloatFieldHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Field, destination.Field);
            Assert.AreEqual(source.Int, destination.Int);
            Assert.AreEqual(source.Float, destination.Float);
        }

        [TestCase(0f)]
        [TestCase(5f)]
        [TestCase(9f)]
        [TestCase(9f / 183)]
        [TestCase(9f / 4.15f)]
        [TestCase(9f / 187)]
        public void TestCompressedFloatSerialization(float value)
        {
            var serializer = new ContractSerializer();

            var source = new CompressedFloatFieldHolder
            {
                Field = (byte) value,
                Int = (int) value,
                Float = value,
                CompressedFloatByte = value,
                CompressedFloatShort = value
            };
            var destination = (CompressedFloatFieldHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Field, destination.Field);
            Assert.AreEqual(source.Int, destination.Int);
            Assert.AreEqual(source.Float, destination.Float);
            Assert.AreEqual(source.CompressedFloatShort, destination.CompressedFloatShort, 20f / ushort.MaxValue);
            Assert.AreEqual(source.CompressedFloatByte, destination.CompressedFloatByte, 20f / byte.MaxValue);
        }

        [Test]
        public void TestContractSerialization()
        {
            var serializer = new ContractSerializer();
            foreach (var type in typeof(object).GetAllSubTypes(u => !u.IsAbstract)
                .Where(u => u.GetCustomAttribute<ContractAttribute>(true) != null))
            {
                if (type.GetConstructor(new Type[0]) == null)
                    continue;
                try
                {
                    var source = Activator.CreateInstance(type);
                    var destination = serializer.Deserialize(serializer.Serialize(source, true));
                }
                catch (Exception e)
                {
                    throw new SerializationException($"For type {type.Name}", e);
                }
            }
        }

        [TestCase(0, null)]
        [TestCase(5, "asdf")]
        [TestCase(9, "asdf")]
        public void TestStringSerialization(float value, string text)
        {
            var serializer = new ContractSerializer();

            var source = new StringFieldHolder {Field = (byte) value, Int = (int) value, Float = value, String = text};
            var destination = (StringFieldHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Field, destination.Field);
            Assert.AreEqual(source.Int, destination.Int);
            Assert.AreEqual(source.Float, destination.Float);
            Assert.AreEqual(source.String, destination.String);
        }

        [TestCase(new[] {2, 0, 3})]
        [TestCase(new[] {2, 0, 3})]
        [TestCase(new[] {2, 0, 3, 2, 0, 5, 19, -18, -15, 6})]
        public void TestListSerialization(int[] args)
        {
            var serializer = new ContractSerializer();

            var source = new ListHolder
            {
                Int = args.ToList(),
                Float = args.Select(u => (float)u).ToList(),
                Holder = args.Select(u => new ByteFieldHolder() {Field = (byte) u}).ToList()
            };
            var destination = (ListHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Int.Count, destination.Int.Count);
            for (var i = 0; i < source.Int.Count; ++i)
                Assert.AreEqual(source.Int[i], destination.Int[i]);
            Assert.AreEqual(source.Holder.Count, destination.Holder.Count);
            for (var i = 0; i < source.Holder.Count; ++i)
                Assert.AreEqual(source.Holder[i].Field, destination.Holder[i].Field);
            
            Assert.AreEqual(source.Float.Count, destination.Float.Count);
            for (var i = 0; i < source.Float.Count; ++i)
                Assert.AreEqual(source.Float[i], destination.Float[i], 40f / byte.MaxValue);
        }

        [TestCase(new[] {2, 0, 3})]
        [TestCase(new[] {2, 0, 3})]
        [TestCase(new[] {2, 0, 3, 2, 0, 5, 19, 28, 156})]
        public void TestArraySerialization(int[] args)
        {
            var serializer = new ContractSerializer();

            var h = args.Select(u => new ByteFieldHolder() {Field = (byte) u}).ToList();
            h.Add(null);
            var source = new ArrayHolder()
            {
                Int = args,
                Holder = h.ToArray()
            };

            var destination = (ArrayHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Int.Length, destination.Int.Length);
            for (var i = 0; i < source.Int.Length; ++i)
                Assert.AreEqual(source.Int[i], destination.Int[i]);
            Assert.AreEqual(source.Holder.Length, destination.Holder.Length);
            for (var i = 0; i < source.Holder.Length; ++i)
                if (source.Holder[i] != null)
                    Assert.AreEqual(source.Holder[i].Field, destination.Holder[i].Field);
        }

        [TestCase(new[] {2, 0, 3})]
        [TestCase(new[] {2, 0, 3})]
        [TestCase(new[] {2, 0, 3, 5, 19, 28, 156})]
        public void TestDictionarySerialization(int[] args)
        {
            var serializer = new ContractSerializer();

            var source = new DictionaryHolder()
            {
                Int = args.ToDictionary(u => u, i => i),
                Holder = args.ToDictionary(u => u, i => new ByteFieldHolder() {Field = (byte) i})
            };
            var destination = (DictionaryHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(source.Int.Count, destination.Int.Count);
            foreach (var key in source.Int.Keys)
                Assert.AreEqual(source.Int[key], destination.Int[key]);
            Assert.AreEqual(source.Holder.Count, destination.Holder.Count);
            foreach (var key in source.Holder.Keys)
                Assert.AreEqual(source.Holder[key], destination.Holder[key]);

            source = new DictionaryHolder()
            {
                Int = null,
                Holder = null
            };
            destination = (DictionaryHolder) serializer.Deserialize(serializer.Serialize(source, true));

            Assert.AreEqual(null, destination.Int);
            Assert.AreEqual(null, destination.Holder);
        }
    }
}