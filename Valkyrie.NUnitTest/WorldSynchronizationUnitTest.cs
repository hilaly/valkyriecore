﻿using System;
using System.Numerics;
using System.Reflection;
using NUnit.Framework;
using Valkyrie.Communication.Network;
using Valkyrie.Data.Serialization;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Entities;
using Valkyrie.Math.Space;
using Valkyrie.Math.Space.Management;
using Valkyrie.Math.Space.Sync;
using Valkyrie.Rpc;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class WorldSynchronizationUnitTest : GenericUnitTests
    {
        [Sync(EntityType.Server)]
        [Contract]
        class TestSyncComponent : IEcsComponent
        {
            
        }
        
        [Sync(EntityType.Controller)]
        [Contract]
        class ControllerTestSyncComponent : IEcsComponent
        {
            [ContractMember(0)] public float Value;
        }

        private const ushort ServerObjectId = 1;

        IWorld StartServerWorld(ITest test, string worldName, int port)
        {
            return StartServerWorld(test, worldName, port, (world, args) =>
            {
                var r = world.FindObject(ServerObjectId);
                world.CreateObserver(r).SetViewDistance(new Vector2(5, 5), new Vector2(5, 5));
                return r;
            });
        }

        IWorld StartServerWorld(ITest test, string worldName, int port, Func<IWorld, string, IWorldObject> handler)
        {
            var serverContainer = test.Container.CreateChild()
                .RegisterLibrary(Libraries.Ecs())
                .RegisterLibrary(Libraries.WorldsServer())
                .Build();

            var worldsManager = serverContainer.Resolve<IWorldsManager>();
            worldsManager.Start(new ServerPort(port) {AppId = "EX", AuthKey = "ExKey", MaxConnections = 10})
                .Done();
            var result = worldsManager.CreateWorld(worldName, serverContainer)
                .SetBounding(Vector2.One * -30, Vector2.One * 30)
                .SetRegionSize(new Vector2(10,10))
                .SetOnline(30, handler)
                .Build();

            return result;
        }

        IWorld CreateClientWorld(ITest test, string worldName, int port, string arg = null)
        {
            var clientContainer = test.Container.CreateChild()
                .RegisterLibrary(Libraries.Ecs())
                .RegisterLibrary(Libraries.World(true))
                .Build();

            IWorld result = null;
            var network = clientContainer.Resolve<INetwork>();
            network.Connect("127.0.0.1", port, "EX", "ExKey")
                .Then(channel => clientContainer.Resolve<IWorldCreator>()
                    .ConnectOnlineWorld(channel, worldName, arg, clientContainer))
                .Done(world => result = world, exception => throw exception);
            
            test.Update(2f);

            return result;
        }
        
        [Test]
        public void TestAvatarCreation()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                #region Worlds Creation

                test.Container.RegisterLibrary(Libraries.Network).Build();
                var worldName = MethodBase.GetCurrentMethod().Name;
                var port = 24009;

                var serverWorld = StartServerWorld(test, worldName, port);
                Assert.IsNotNull(serverWorld);
                
                var serverInstance = serverWorld.CreateObject(ServerObjectId, Vector2.Zero);
                Assert.IsNotNull(serverInstance);
                
                var clientWorld = CreateClientWorld(test, worldName, port);
                Assert.IsNotNull(clientWorld);

                Assert.AreNotEqual(serverWorld, clientWorld);
                
                test.Update(0.2f);

                var clientId = EcsExtensions.GetNetworkObjectId(ServerObjectId);

                test.Update(1f);
                
                var clientInstance = clientWorld.FindObject(clientId);
                Assert.IsNotNull(clientInstance);
                Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                test.Update(0.2f);
                
                #endregion
                
                serverInstance.Destroy();
                
                test.Update(1f);

                Assert.IsNull(serverWorld.FindObject(ServerObjectId));
                Assert.IsNull(clientWorld.FindObject(clientId));
            }
        }
        
        [Test]
        public void TestServerToClientObjectViewLostSynchronization()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                #region Worlds Creation

                test.Container.RegisterLibrary(Libraries.Network).Build();
                var worldName = MethodBase.GetCurrentMethod().Name;
                var port = 24012;

                var serverWorld = StartServerWorld(test, worldName, port);
                Assert.IsNotNull(serverWorld);
                
                var serverInstance = serverWorld.CreateObject(ServerObjectId, Vector2.One * 30);
                Assert.IsNotNull(serverInstance);
                
                var clientWorld = CreateClientWorld(test, worldName, port);
                Assert.IsNotNull(clientWorld);

                Assert.AreNotEqual(serverWorld, clientWorld);
                
                test.Update(0.2f);

                var clientId = EcsExtensions.GetNetworkObjectId(ServerObjectId);

                test.Update(1f);
                
                var clientInstance = clientWorld.FindObject(clientId);
                Assert.IsNotNull(clientInstance);
                Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                test.Update(0.2f);
                
                #endregion

                ushort oId = 123;
                var observedInstance = serverWorld.CreateObject(oId, Vector2.Zero);
                
                test.Update(1f);

                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                observedInstance.Position = new Vector2(30, 30);
                
                test.Update(1f);

                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                observedInstance.Position = new Vector2(-30, -30);
                
                test.Update(1f);

                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                observedInstance.Position = new Vector2(30, 30);
                
                test.Update(1f);
                
                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                observedInstance.Destroy();
                
                test.Update(1f);

                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
            }
        }
        
        [Test]
        public void TestServerToClientObjectComponentsSynchronization()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                #region Worlds Creation

                test.Container.RegisterLibrary(Libraries.Network).Build();
                var worldName = MethodBase.GetCurrentMethod().Name;
                var port = 24013;

                var serverWorld = StartServerWorld(test, worldName, port);
                Assert.IsNotNull(serverWorld);
                
                var serverInstance = serverWorld.CreateObject(ServerObjectId, Vector2.One * 30);
                Assert.IsNotNull(serverInstance);
                
                var clientWorld = CreateClientWorld(test, worldName, port);
                Assert.IsNotNull(clientWorld);

                Assert.AreNotEqual(serverWorld, clientWorld);
                
                test.Update(0.2f);

                var clientId = EcsExtensions.GetNetworkObjectId(ServerObjectId);

                test.Update(1f);
                
                var clientInstance = clientWorld.FindObject(clientId);
                Assert.IsNotNull(clientInstance);
                Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                test.Update(0.2f);
                
                #endregion

                ushort oId = 123;
                var observedInstance = serverWorld.CreateObject(oId, Vector2.Zero).SetOnline();
                
                test.Update(1f);

                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                observedInstance.Position = new Vector2(30, 30);
                
                test.Update(1f);

                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                var pos = new Vector2(29, 29);
                observedInstance.Position = pos;
                
                test.Update(1f);

                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.LessOrEqual((clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).Position - pos)
                    .Length(), 0.1f);
                
                pos = new Vector2(28, 28);
                observedInstance.Position = pos;
                
                test.Update(1f);

                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.LessOrEqual((clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).Position - pos)
                    .Length(), 0.1f);
                
                pos = new Vector2(30, 30);
                observedInstance.Position = pos;
                
                test.Update(1f);

                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.LessOrEqual((clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).Position - pos)
                    .Length(), 0.1f);

                serverInstance.Position = new Vector2(-30, -30);
                test.Update(1f);
                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.LessOrEqual((serverInstance.Position - clientInstance.Position).Length(), 0.1f);
            }
        }
        
        [Test]
        public void TestServerToClientObjectComponentsAddRemoveSynchronization()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                #region Worlds Creation

                test.Container.RegisterLibrary(Libraries.Network).Build();
                var worldName = MethodBase.GetCurrentMethod().Name;
                var port = 24014;

                var serverWorld = StartServerWorld(test, worldName, port);
                Assert.IsNotNull(serverWorld);
                
                var serverInstance = serverWorld.CreateObject(ServerObjectId, Vector2.One * 30);
                Assert.IsNotNull(serverInstance);
                
                var clientWorld = CreateClientWorld(test, worldName, port);
                Assert.IsNotNull(clientWorld);

                Assert.AreNotEqual(serverWorld, clientWorld);
                
                test.Update(1f);

                var clientId = EcsExtensions.GetNetworkObjectId(ServerObjectId);

                test.Update(1f);
                
                var clientInstance = clientWorld.FindObject(clientId);
                Assert.IsNotNull(clientInstance);
                Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                test.Update(1f);
                
                #endregion

                test.Log("================================================");
                
                ushort oId = 123;
                var observedInstance = serverWorld.CreateObject(oId, Vector2.One * 30).SetOnline();
                
                test.Update(1f);
                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.IsFalse(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).EcsEntity
                    .HasComponent<TestSyncComponent>());
                
                observedInstance.EcsEntity.Add(new TestSyncComponent());
                
                test.Update(1f);
                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.IsTrue(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).EcsEntity
                    .HasComponent<TestSyncComponent>());
                
                observedInstance.EcsEntity.Remove<TestSyncComponent>();
                
                test.Update(1f);
                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.IsFalse(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).EcsEntity
                    .HasComponent<TestSyncComponent>());

                observedInstance.Position = Vector2.One * -30;
                
                test.Update(1f);
                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                
                observedInstance.EcsEntity.Add(new TestSyncComponent());
                
                test.Update(1f);
                Assert.IsNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));

                observedInstance.Position = Vector2.One * 30;
                
                test.Update(1f);
                Assert.IsNotNull(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)));
                Assert.IsTrue(clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(oId)).EcsEntity
                    .HasComponent<TestSyncComponent>());
            }
        }

        [Test]
        public void TestControllerToServerSynchronization()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                #region Worlds Creation

                test.Container.RegisterLibrary(Libraries.Network).Build();
                var worldName = MethodBase.GetCurrentMethod().Name;
                var port = 24015;

                var serverWorld = StartServerWorld(test, worldName, port);
                Assert.IsNotNull(serverWorld);
                
                var serverInstance = serverWorld.CreateObject(ServerObjectId, Vector2.Zero);
                Assert.IsNotNull(serverInstance);
                
                var clientWorld = CreateClientWorld(test, worldName, port);
                Assert.IsNotNull(clientWorld);

                Assert.AreNotEqual(serverWorld, clientWorld);
                
                test.Update(0.2f);

                var clientId = EcsExtensions.GetNetworkObjectId(ServerObjectId);

                test.Update(1f);
                
                var clientInstance = clientWorld.FindObject(clientId);
                Assert.IsNotNull(clientInstance);
                Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                test.Update(0.2f);
                
                #endregion

                clientInstance.EcsEntity.Add(new ControllerTestSyncComponent {Value = 30});
                
                test.Update(1f);
                
                Assert.IsNotNull(clientInstance);
                Assert.IsNotNull(serverInstance.EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.AreEqual(30f, serverInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value, 0.1f);

                serverInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value = -30;
                
                test.Update(0.2f);
                
                Assert.IsNotNull(clientInstance);
                Assert.IsNotNull(serverInstance.EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.AreEqual(30f, serverInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value, 0.1f);

                clientInstance.EcsEntity.Remove<ControllerTestSyncComponent>();
                
                test.Update(1f);
                
                Assert.IsNotNull(clientInstance);
                Assert.IsNull(clientInstance.EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.IsNull(serverInstance.EcsEntity.Get<ControllerTestSyncComponent>());
            }
        }
        
        [Test]
        public void TestControllerToOtherClientSynchronization()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                #region Worlds Creation

                test.Container.RegisterLibrary(Libraries.Network).Build();
                var worldName = MethodBase.GetCurrentMethod().Name;
                var port = 24016;

                var serverWorld = StartServerWorld(test, worldName, port, (world, s) =>
                {
                    var r = world.CreateObject(ushort.Parse(s), Vector2.Zero);
                    world.CreateObserver(r).SetViewDistance(new Vector2(5, 5), new Vector2(5, 5));
                    return r;
                });
                Assert.IsNotNull(serverWorld);
                
                var clientId = ServerObjectId;
                var clientWorld = CreateClientWorld(test, worldName, port, clientId.ToString());
                Assert.IsNotNull(clientWorld);

                Assert.AreNotEqual(serverWorld, clientWorld);
                
                test.Update(0.2f);

                var serverInstance = serverWorld.FindObject(clientId);
                Assert.IsNotNull(serverInstance);
                
                test.Update(1f);
                
                var clientInstance = clientWorld.FindObject(clientId);
                Assert.IsNotNull(clientInstance);
                Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                test.Update(0.2f);
                
                #endregion

                clientInstance.EcsEntity.Add(new ControllerTestSyncComponent {Value = 30});
                
                test.Update(1f);
                
                Assert.IsNotNull(clientInstance);
                Assert.IsNotNull(serverInstance.EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.AreEqual(30f, serverInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value, 0.1f);

                serverInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value = -30;
                
                test.Update(0.2f);
                
                Assert.IsNotNull(clientInstance);
                Assert.IsNotNull(serverInstance.EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.AreEqual(30f, serverInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value, 0.1f);

                ushort replicaId = 5;
                var replicaWorld = CreateClientWorld(test, worldName, port, replicaId.ToString());
                test.Update(1f);

                Assert.IsNotNull(replicaWorld.FindObject(clientId));
                Assert.IsNotNull(replicaWorld.FindObject(clientId).EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.AreEqual(30f, replicaWorld.FindObject(clientId).EcsEntity.Get<ControllerTestSyncComponent>().Value, 0.1f);
                
                clientInstance.EcsEntity.Get<ControllerTestSyncComponent>().Value = -30;
                
                test.Update(1f);
                Assert.IsNotNull(replicaWorld.FindObject(clientId));
                Assert.IsNotNull(replicaWorld.FindObject(clientId).EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.AreEqual(-30f, replicaWorld.FindObject(clientId).EcsEntity.Get<ControllerTestSyncComponent>().Value, 0.1f);

                clientInstance.EcsEntity.Remove<ControllerTestSyncComponent>();
                
                test.Update(1f);
                Assert.IsNotNull(replicaWorld.FindObject(clientId));
                Assert.IsNull(replicaWorld.FindObject(clientId).EcsEntity.Get<ControllerTestSyncComponent>());
                Assert.IsNull(clientWorld.FindObject(clientId).EcsEntity.Get<ControllerTestSyncComponent>());
            }
        }
        
            [Test]
            public void TestControlGranting()
            {
                using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
                {
                    #region Worlds Creation

                    test.Container.RegisterLibrary(Libraries.Network).Build();
                    var worldName = MethodBase.GetCurrentMethod().Name;
                    var port = 24017;

                    var serverWorld = StartServerWorld(test, worldName, port, (world, s) =>
                    {
                        var r = world.CreateObject(ushort.Parse(s), Vector2.Zero);
                        world.CreateObserver(r).SetViewDistance(new Vector2(5, 5), new Vector2(5, 5));
                        return r;
                    });
                    Assert.IsNotNull(serverWorld);
                
                    var clientWorld = CreateClientWorld(test, worldName, port, ServerObjectId.ToString());
                    Assert.IsNotNull(clientWorld);
                
                    var serverInstance = serverWorld.FindObject(ServerObjectId);
                    Assert.IsNotNull(serverInstance);

                    Assert.AreNotEqual(serverWorld, clientWorld);
                
                    test.Update(0.2f);

                    var clientId = EcsExtensions.GetNetworkObjectId(ServerObjectId);

                    test.Update(1f);
                
                    var clientInstance = clientWorld.FindObject(clientId);
                    Assert.IsNotNull(clientInstance);
                    Assert.AreEqual(serverInstance.Position, clientInstance.Position);
                
                    test.Update(0.2f);
                
                    #endregion

                    ushort replicaId = 234;
                    var serverReplica = serverWorld.CreateObject(replicaId, Vector2.Zero);
                    serverReplica.SetOnline();
                    
                    test.Update(1f);

                    var clientReplica = clientWorld.FindObject(EcsExtensions.GetNetworkObjectId(replicaId));
                    Assert.IsNotNull(clientReplica);
                    serverWorld.CreateObserver(serverReplica).SetViewDistance(new Vector2(5, 5), new Vector2(5, 5));
                    serverReplica.SetAvatar(serverInstance.GetControllerChannel(), serverWorld.SimulationTick);
                    
                    test.Update(1f);
                    test.Update(1f);
                    
                    Assert.IsTrue(clientReplica.EcsEntity.IsControlled());
                
                    serverInstance.Destroy();
                
                    test.Update(1f);
                    test.Update(1f);
                    test.Update(1f);

                    Assert.IsNull(serverWorld.FindObject(ServerObjectId));
                    Assert.IsNull(clientWorld.FindObject(clientId));
                }
            }
    }
}