﻿using System.Numerics;
using NUnit.Framework;
using Valkyrie.Ecs;
using Valkyrie.Math.Space;
using Valkyrie.Math.Space.Management;
using Valkyrie.Threading.Async;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class SpaceUnitTest : GenericUnitTests
    {
        #region Collections

        [Test]
        public void TestWorldObserver()
        {
            using (var test = CreateTest("Test world observer"))
            {
                test.Container.RegisterLibrary(Libraries.Ecs()).RegisterLibrary(Libraries.World(false, true)).Build();
                test.Container.Resolve<IEcsContext>().Start();

                var find2 = 0;
                var find3 = 0;
                var lost2 = 0;
                var lost3 = 0;

                var world = test.Container.Resolve<IWorldCreator>()
                    .CreateWorld("", test.Container)
                    .SetBounding(Vector2.One * -30, Vector2.One * 30)
                    .SetRegionSize(new Vector2(20, 20))
                    .Build();

                var o = world.CreateObject(1, new Vector2(20, 20));
                var subscription = world.CreateObserver(o);
                subscription.SetViewDistance(new Vector2(20, 20), new Vector2(20, 20));
                subscription.ObserveObjectFind().Subscribe(worldObject =>
                {
                    switch (worldObject.GetUid())
                    {
                            case 2:
                                find2++;
                                break;
                            case 3:
                                find3++;
                                break;
                    }
                    test.Log("Find object {0}", worldObject.GetUid());
                });
                subscription.ObserveObjectLost().Subscribe(worldObject =>
                {
                    switch (worldObject.GetUid())
                    {
                        case 2:
                            lost2++;
                            break;
                        case 3:
                            lost3++;
                            break;
                    }
                    test.Log("Lost object {0}", worldObject.GetUid());
                });
                subscription.ObserveEvents().Subscribe(msg =>
                {
                    test.Log("Message {1} on object {0}", msg.Sender.GetUid(), msg);
                });

                var temp0 = world.CreateObject(2, new Vector2(-15, -15));
                var temp1 = world.CreateObject(3, new Vector2(3, 5));
                temp0.Position = Vector2.Zero;
                temp1.Position = new Vector2(-15, -15);
                o.Position = Vector2.Zero;

                Assert.AreEqual(1, find2);
                Assert.AreEqual(0, lost2);
                Assert.AreEqual(2, find3);
                Assert.AreEqual(1, lost3);
            }
        }

        [Test]
        public void TestWorldStreamer()
        {
            using (var test = CreateTest("Test world streamer"))
            {
                test.Container.RegisterLibrary(Libraries.Ecs()).RegisterLibrary(Libraries.World(false)).Build();
                
                test.Container.Resolve<IEcsContext>().Start();

                var editor = test.Container.Resolve<IWorldCreator>().CreateWorld("", test.Container)
                    .SetBounding(Vector2.One * -30, Vector2.One * 30).SetRegionSize(Vector2.One * 20);
                editor.AddObject(Vector2.Zero, null);
                var world = editor.Build();

                var findCount = 0;
                var lostCount = 0;

                var o = world.CreateObject(1, Vector2.Zero);
                var subscription = world.CreateObserver(o);
                subscription.SetViewDistance(new Vector2(60, 60), new Vector2(60, 60));
                subscription.ObserveObjectFind().Subscribe(worldObject =>
                {
                    findCount++;
                    test.Log("Find object {0}", worldObject.GetUid());
                });
                subscription.ObserveObjectLost().Subscribe(worldObject =>
                {
                    lostCount++;
                    test.Log("Lost object {0}", worldObject.GetUid());
                });
                
                Assert.AreEqual(0, findCount);
                Assert.AreEqual(0, lostCount);

                var streamer = world.CreateStreamController(new Vector2(30, 30), new Vector2(30, 30), Vector2.Zero);
                
                Assert.AreEqual(1, findCount);
                Assert.AreEqual(0, lostCount);
                
                streamer.Dispose();
                test.Update(0.1f);
                
                Assert.AreEqual(1, findCount);
                Assert.AreEqual(1, lostCount);
                
                streamer = world.CreateStreamController(new Vector2(30, 30), new Vector2(30, 30), Vector2.Zero);
                
                Assert.AreEqual(2, findCount);
                Assert.AreEqual(1, lostCount);
                
                streamer.Dispose();
                test.Update(0.1f);
                
                Assert.AreEqual(2, findCount);
                Assert.AreEqual(2, lostCount);
            }
        }

        #endregion
    }
}