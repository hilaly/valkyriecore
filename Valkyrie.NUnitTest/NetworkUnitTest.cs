﻿using System;
using System.Reflection;
using NUnit.Framework;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class NetworkUnitTest : GenericUnitTests
    {
        [NetworkMessage(NetDeliveryMethod.ReliableOrdered, 0)]
        [Contract]
        class TestNetworkRequest
        {
            [ContractMember(0)] public int value;
        }

        [NetworkMessage(NetDeliveryMethod.ReliableOrdered, 0)]
        [Contract]
        class TestNetworkResponse
        {
            [ContractMember(0)] public int value;
        }

        void SendTestMessage(ITest executor, INetworkChannel channel, int value)
        {
            channel.Send(new TestNetworkRequest {value = value});
            executor.Update(1);
        }

        [Test]
        public void NetworkCreation()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();

                var network = test.Container.Resolve<INetwork>();
                Assert.IsNotNull(network);
            }
        }

        [Test]
        public void ClientNetworkCreation()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();

                var network = test.Container.Resolve<INetwork>();
                
                var channel = new Channel(new ServerPort() { Address = "127.0.0.1", Port = 25000, AppId = "null", AuthKey = null});
                channel.ConnectAsync().Done();
                test.Update(1f);
            }
        }

        [Test]
        public void ServerNetworkCreation()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();

                var network = test.Container.Resolve<INetwork>();

                INetworkChannel channel = null;
                IPromise promise = null;
                Exception error = null;

                promise = Promise.Sequence(
                    () => network.Listen(25001, 25, "null", (networkChannel, s) => ErrorCodes.Ok).Then((c) => { }),
                    () => network.Listen(25002, 25, "null", (networkChannel, s) => ErrorCodes.Ok).Then((c) => { }),
                    () => network.Connect("127.0.0.1", 25002, "null", null).Then(c =>
                    {
                        channel = c;
                        promise = null;
                        Promise.Resolved();
                    })).Catch((e) =>
                {
                    error = e;
                    promise = null;
                });

                test.Run(() => promise != null);

                Assert.IsNull(error);
                Assert.IsNotNull(channel);
                channel.Dispose();
            }
        }

        [Test]
        public void RequestsHandling()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();

                int eventsCounter = 0;
                int iterations = 3;


                var network = test.Container.Resolve<INetwork>();
                INetworkChannel channel = null;
                IPromise promise = null;
                Exception error = null;

                INetworkListener listener = null;
                promise = Promise.Sequence(
                    () => network.Listen(25003, 25, "null", (networkChannel, s) => ErrorCodes.Ok).Then((c) =>
                    {
                        c.Subscribe<TestNetworkRequest, TestNetworkResponse>((message, handler) =>
                            handler(new TestNetworkResponse {value = message.value}));
                    }),
                    () => network.Listen(25004, 25, "null", (networkChannel, s) => ErrorCodes.Ok).Then((c) =>
                    {
                        c.Subscribe<TestNetworkRequest, TestNetworkResponse>((message, handler) =>
                            handler(new TestNetworkResponse {value = message.value}));
                    }),
                    () => network.Connect("127.0.0.1", 25003, "null", null).Then(c =>
                    {
                        channel = c;
                        promise = null;
                        Promise.Resolved();
                    })).Catch((e) =>
                {
                    error = e;
                    promise = null;
                });

                var st = DateTime.UtcNow;
                test.Run(() => promise != null && (DateTime.UtcNow - st).TotalSeconds < 5);

                Assert.IsNull(error);
                Assert.IsNotNull(channel);

                test.Update(1);
                
                for (int i = 0; i < iterations; ++i)
                {
                    channel.Send<TestNetworkRequest, TestNetworkResponse>(
                            new TestNetworkRequest {value = eventsCounter}, response => true)
                        .Then(response =>
                            Assert.AreEqual(eventsCounter++, response.value));
                    test.Update(1);
                }

                for (int i = 0; i < iterations; ++i)
                {
                    channel.Send<TestNetworkRequest, TestNetworkResponse>(
                            new TestNetworkRequest {value = eventsCounter}, response => true)
                        .Then(response =>
                            Assert.AreEqual(eventsCounter++, response.value));
                    test.Update(1);
                }

                channel.Dispose();

                Assert.AreEqual(iterations * 2, eventsCounter);
            }
        }

        [Test]
        public void EventsHandling()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();

                int eventsCounter = 0;
                int iterations = 3;

                var network = test.Container.Resolve<INetwork>();
                INetworkChannel channel = null;
                IPromise promise = null;
                Exception error = null;

                promise = Promise.Sequence(
                    () => network.Listen(25005, 25, "null", (networkChannel, s) => ErrorCodes.Ok).Then((c) =>
                    {
                        c.Subscribe<TestNetworkRequest>(message => Assert.AreEqual(eventsCounter++, message.value));
                    }),
                    () => network.Listen(25006, 25, "null", (networkChannel, s) => ErrorCodes.Ok).Then((c) =>
                    {
                        c.Subscribe<TestNetworkRequest>(message => Assert.AreEqual(eventsCounter++, message.value));
                    }),
                    () => network.Connect("127.0.0.1", 25005, "null", null).Then(c =>
                    {
                        channel = c;
                        promise = null;
                        Promise.Resolved();
                    })).Catch((e) =>
                {
                    error = e;
                    promise = null;
                });

                var st = DateTime.UtcNow;
                test.Run(() => promise != null && (DateTime.UtcNow - st).TotalSeconds < 5);

                Assert.IsNull(error);
                Assert.IsNotNull(channel);


                test.Update(1);
                for (int i = 0; i < iterations; ++i)
                    SendTestMessage(test, channel, eventsCounter);
                test.Update(1);

                channel.Dispose();

                Assert.AreEqual(iterations, eventsCounter);
            }
        }
    }
}