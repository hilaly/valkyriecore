using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class RpcUnitTest : GenericUnitTests
    {
        #region Help class defines

        [Contract]
        class TestRequest
        {
            [ContractMember(0)] public int Value;
        }

        [Contract]
        class TestResponse
        {
            [ContractMember(0)] public int Value;
        }

        class TestClient : ClientBase
        {
            public TestClient(INetworkChannel networkChannel) : base(networkChannel)
            {
            }

            public IPromise<TestResponse> Unary1(TestRequest request)
            {
                return SingleToSingle<TestRequest, TestResponse>("Unary1", request, new ClientCallOptions());
            }
            public IPromise<TestResponse> Unary2(TestRequest request)
            {
                return SingleToSingle<TestRequest, TestResponse>("Unary2", request, new ClientCallOptions());
            }

            public IAsyncClientStreamingCall<TestRequest, TestResponse> Client1()
            {
                return ManyToSingle<TestRequest, TestResponse>("Client1", new ClientCallOptions());
            }
            public IAsyncClientStreamingCall<TestRequest, TestResponse> Client2()
            {
                return ManyToSingle<TestRequest, TestResponse>("Client2", new ClientCallOptions());
            }

            public IAsyncServerStreamingCall<TestResponse> Server1(TestRequest request)
            {
                return SingleToMany<TestRequest, TestResponse>(
                    "Server1", request,
                    new ClientCallOptions());
            }
            public IAsyncServerStreamingCall<TestResponse> Server2(TestRequest request)
            {
                return SingleToMany<TestRequest, TestResponse>(
                    "Server2", request,
                    new ClientCallOptions());
            }

            public IAsyncDuplexStreamingCall<TestRequest, TestResponse> Duplex1()
            {
                return ManyToMany<TestRequest, TestResponse>(
                    "Duplex1",
                    new ClientCallOptions());
            }
            public IAsyncDuplexStreamingCall<TestRequest, TestResponse> Duplex2()
            {
                return ManyToMany<TestRequest, TestResponse>(
                    "Duplex2",
                    new ClientCallOptions());
            }
        }

        class TestService
        {
            IPromise<TestResponse> Unary1(TestRequest request, ServerCallContext context)
            {
                return Promise<TestResponse>.Resolved(new TestResponse() {Value = request.Value + 1});
            }

            IPromise<TestResponse> Unary2(TestRequest request, ServerCallContext context)
            {
                return Promise<TestResponse>.Resolved(new TestResponse() {Value = request.Value + 2});
            }

            void Server1(TestRequest request, IObserver<TestResponse> responsesStream,
                ServerCallContext context)
            {
                var response = new TestResponse() {Value = request.Value + 1};
                for (int i = 0; i < request.Value; i++)
                    responsesStream.OnNext(response);
                responsesStream.OnCompleted();
            }
            
            void Server2(TestRequest request, IObserver<TestResponse> responsesStream, ServerCallContext context)
            {
                var response = new TestResponse() {Value = request.Value + 2};
                for (int i = 0; i < request.Value; i++)
                    responsesStream.OnNext(response);
                responsesStream.OnCompleted();
            }

            IPromise<TestResponse> Client1(IObservable<TestRequest> requestsStream,
                ServerCallContext context)
            {
                var response = new TestResponse();
                var result = new Promise<TestResponse>();
                var d = requestsStream.Subscribe(request => { response.Value += request.Value + 1; },
                    exception => result.Reject(exception), () => result.Resolve(response));
                return result.Then(r =>
                {
                    d.Dispose();
                    return r;
                });
            }

            IPromise<TestResponse> Client2(IObservable<TestRequest> requestsStream,
                ServerCallContext context)
            {
                var response = new TestResponse();
                var result = new Promise<TestResponse>();
                var d = requestsStream.Subscribe(request => { response.Value += request.Value + 2; },
                    exception => result.Reject(exception), () => result.Resolve(response));
                return result.Then(r =>
                {
                    d.Dispose();
                    return r;
                });
            }

            void Duplex1(IObservable<TestRequest> requestsStream,
                IObserver<TestResponse> responsesStream, ServerCallContext context)
            {
                IDisposable d = null;
                d = requestsStream.Subscribe(request =>
                    {
                        var response = new TestResponse() {Value = request.Value + 1};
                        for (int i = 0; i < request.Value; i++)
                            responsesStream.OnNext(response);
                    },
                    exception =>
                    {
                        responsesStream.OnError(exception);
                        d.Dispose();
                    }, () =>
                    {
                        responsesStream.OnCompleted();
                        d.Dispose();
                    });
            }

            void Duplex2(IObservable<TestRequest> requestsStream,
                IObserver<TestResponse> responsesStream, ServerCallContext context)
            {
                IDisposable d = null;
                d = requestsStream.Subscribe(request =>
                    {
                        var response = new TestResponse() {Value = request.Value + 2};
                        for (int i = 0; i < request.Value; i++)
                            responsesStream.OnNext(response);
                    },
                    exception =>
                    {
                        responsesStream.OnError(exception);
                        d.Dispose();
                    }, () =>
                    {
                        responsesStream.OnCompleted();
                        d.Dispose();
                    });
            }
            

            public ServiceDefinition BindService()
            {
                return new ServiceDefinition()
                    .BindMethod("Unary1", (Func<TestRequest, ServerCallContext, IPromise<TestResponse>>) Unary1)
                    .BindMethod("Unary2", (Func<TestRequest, ServerCallContext, IPromise<TestResponse>>) Unary2)
                    .BindMethod("Server1", (Action<TestRequest, IObserver<TestResponse>, ServerCallContext>) Server1)
                    .BindMethod("Server2", (Action<TestRequest, IObserver<TestResponse>, ServerCallContext>) Server2)
                    .BindMethod("Client1", (Func<IObservable<TestRequest>, ServerCallContext, IPromise<TestResponse>>) Client1)
                    .BindMethod("Client2", (Func<IObservable<TestRequest>, ServerCallContext, IPromise<TestResponse>>) Client2)
                    .BindMethod("Duplex1", (Action<IObservable<TestRequest>, IObserver<TestResponse>, ServerCallContext>) Duplex1)
                    .BindMethod("Duplex2", (Action<IObservable<TestRequest>, IObserver<TestResponse>, ServerCallContext>) Duplex2);
            }
        }

        #endregion

        [Test]
        public void ServerStart()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();
                var network = test.Container.Resolve<INetwork>();
                var serverPort = new ServerPort(new Random(DateTime.UtcNow.Millisecond).Next(20000, 50000))
                    {AppId = "test_app_id", AuthKey = "test_app_token"};
                var server = new Rpc.Server(network, test.Container.Resolve<ILogService>())
                {
                    Ports = new[] {serverPort},
                    Services = new[] {new TestService().BindService()}
                };
                var started = false;
                server.Start().Done(() => started = true);
                test.Update(1f);
                Assert.IsTrue(started, "Server not started in 1 second");
            }
        }

        [Test]
        public void ClientStart()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();
                var network = test.Container.Resolve<INetwork>();
                var serverPort = new ServerPort(new Random(DateTime.UtcNow.Millisecond).Next(20000, 50000))
                    {AppId = "test_app_id", AuthKey = "test_app_token", Address = "127.0.0.1"};
                var server = new Rpc.Server(network, test.Container.Resolve<ILogService>())
                {
                    Ports = new[] {serverPort},
                    Services = new[] {new TestService().BindService()}
                };
                var started = false;
                server.Start().Done(() => started = true);
                test.Update(1f);
                Assert.IsTrue(started, "Server not started in 1 second");

                TestClient client = null;
                network.Connect(serverPort)
                    .Then(channel => client = new TestClient(channel))
                    .Done();

                test.Update(1f);
                Assert.NotNull(client, "Client not started in 1 second");
            }
        }

        [Test]
        public void ClientSend()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                test.Container.RegisterLibrary(Libraries.Network).Build();
                var network = test.Container.Resolve<INetwork>();
                var serverPort = new ServerPort(new Random(DateTime.UtcNow.Millisecond).Next(20000, 50000))
                    {AppId = "test_app_id", AuthKey = "test_app_token", Address = "127.0.0.1"};
                var server = new Rpc.Server(network, test.Container.Resolve<ILogService>())
                {
                    Ports = new[] {serverPort},
                    Services = new[] {new TestService().BindService()}
                };
                var started = false;
                server.Start().Done(() => started = true);
                test.Update(1f);
                Assert.IsTrue(started, "Server not started in 1 second");

                TestClient client = null;
                network.Connect(serverPort)
                    .Then(channel => client = new TestClient(channel))
                    .Done();

                test.Update(1f);
                Assert.NotNull(client, "Client not started in 1 second");

                client.Unary1(new TestRequest {Value = 5}).Done();
                test.Update(1f);
            }
        }

        TestClient PrepareTest(ITest test)
        {
            test.Container.RegisterLibrary(Libraries.Network).Build();
            var network = test.Container.Resolve<INetwork>();
            var serverPort = new ServerPort(new Random(DateTime.UtcNow.Millisecond).Next(20000, 50000))
                {AppId = "test_app_id", AuthKey = "test_app_token", Address = "127.0.0.1"};
            var server = new Rpc.Server(network, test.Container.Resolve<ILogService>())
            {
                Ports = new[] {serverPort},
                Services = new[] {new TestService().BindService()}
            };
            var started = false;
            server.Start().Done(() => started = true);
            test.Update(1f);
            Assert.IsTrue(started, "Server not started in 1 second");

            TestClient client = null;
            network.Connect(serverPort)
                .Then(channel => client = new TestClient(channel))
                .Done();

            test.Update(1f);
            Assert.NotNull(client, "Client not started in 1 second");
            return client;
        }
        
        [Test]
        public void TestUnaryTest()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                int responseValue = 0;
                var client = PrepareTest(test);
                //Unary test
                
                responseValue = 0;
                client.Unary1(new TestRequest {Value = 5})
                    .Then(response => responseValue = response.Value)
                    .Done();
                test.Update(1f);
                Assert.AreEqual(6, responseValue);

                responseValue = 0;
                client.Unary2(new TestRequest {Value = 5})
                    .Then(response => responseValue = response.Value)
                    .Done();
                test.Update(1f);
                Assert.AreEqual(7, responseValue);
            }
        }

        [Test]
        public void TestClientStreamTest()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                int responseValue = 0;
                var client = PrepareTest(test);
                
                // client stream test
                {
                    responseValue = 0;
                    var call = client.Client1();
                    call.RequestsStream.Write(new TestRequest() { Value = 5});
                    call.RequestsStream.Write(new TestRequest() { Value = 5});
                    call.RequestsStream.Write(new TestRequest() { Value = 5});
                    call.RequestsStream.Close();

                    call.Response.Done(response => responseValue = response.Value, e => throw e);
                    test.Update(1f);
                    Assert.AreEqual(18, responseValue);
                }
                {
                    responseValue = 0;
                    var call = client.Client2();
                    call.RequestsStream.Write(new TestRequest() { Value = 5});
                    call.RequestsStream.Write(new TestRequest() { Value = 5});
                    call.RequestsStream.Write(new TestRequest() { Value = 5});
                    call.RequestsStream.Close();

                    call.Response.Done(response => responseValue = response.Value, e => throw e);
                    test.Update(1f);
                    Assert.AreEqual(21, responseValue);
                }
            }
        }

        [Test]
        public void TestServerStreamTest()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                int responseValue = 0;
                var client = PrepareTest(test);
                
                // server stream test
                {
                    responseValue = 0;
                    var call = client.Server1(new TestRequest() {Value = 5});
                    using (var d = call.ResponsesStream.Subscribe(response =>
                    {
                        Assert.AreEqual(6, response.Value);
                        responseValue += 1;
                    }))
                    {
                        test.Update(1f);
                        Assert.AreEqual(5, responseValue);
                    }
                }
                {
                    responseValue = 0;
                    var call = client.Server2(new TestRequest() {Value = 5});
                    using (var d = call.ResponsesStream.Subscribe(response =>
                    {
                        Assert.AreEqual(7, response.Value);
                        responseValue += 1;
                    }))
                    {
                        test.Update(1f);
                        Assert.AreEqual(5, responseValue);
                    }
                }
            }
        }

        [Test]
        public void TestDuplexStreaming()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                int responseValue = 0;
                var client = PrepareTest(test);
                
                // server stream test
                {
                    responseValue = 0;
                    var call = client.Duplex1();
                    using (var d = call.ResponsesStream.Subscribe(response =>
                    {
                        Assert.AreEqual(6, response.Value);
                        responseValue += 1;
                    }))
                    {
                        call.RequestsStream.Write(new TestRequest() { Value = 5});
                        call.RequestsStream.Write(new TestRequest() { Value = 5});
                        call.RequestsStream.Write(new TestRequest() { Value = 5});
                        test.Update(1f);
                        
                        call.RequestsStream.Close();
                        
                        test.Update(1f);
                        Assert.AreEqual(15, responseValue);
                    }
                }
                {
                    responseValue = 0;
                    var call = client.Duplex2();
                    using (var d = call.ResponsesStream.Subscribe(response =>
                    {
                        Assert.AreEqual(7, response.Value);
                        responseValue += 1;
                    }))
                    {
                        call.RequestsStream.Write(new TestRequest() { Value = 5});
                        call.RequestsStream.Write(new TestRequest() { Value = 5});
                        call.RequestsStream.Write(new TestRequest() { Value = 5});
                        call.RequestsStream.Close();
                        
                        test.Update(1f);
                        Assert.AreEqual(15, responseValue);
                    }
                }
            }
        }

        #region Parsing and generating proto files

        List<string> GetAllProtoFiles()
        {
            string GetRootDirectory()
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.Combine(Path.GetDirectoryName(path), "./../..");
            }

            var directoryInfo = new DirectoryInfo(GetRootDirectory());
            TestContext.Out.WriteLine($"Start founding proto files in {directoryInfo.FullName}");

            var files = directoryInfo.GetFiles("*.proto", SearchOption.AllDirectories).ToList();

            TestContext.Out.WriteLine(
                $"Found {files.Count} proto files");
            return files.ConvertAll(u => u.FullName);
        }

        [Test]
        public void TestParseProtoFiles()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                foreach (var fn in GetAllProtoFiles())
                {
                    try
                    {
                        using (var ms = new MemoryStream())
                        using (var sw = new StreamWriter(ms))
                        {
                            using (var fs = new FileStream(fn, FileMode.Open))
                                ProtoParser.Parse(fs, sw);

                            sw.Flush();
                            ms.Seek(0, SeekOrigin.Begin);
                            var text = ms.ReadAllText();
                            Assert.True(text.StartsWith("// <auto-generated>"));
                            test.Log(text);
                        }
                    }
                    catch (Exception e)
                    {
                        test.Log($"{fn}: {e}");
                        throw;
                    }
                }
            }
        }

        [Test]
        public void TestLexProtoFiles()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                foreach (var fn in GetAllProtoFiles())
                {
                    test.Log($"START parse {fn}");
                    try
                    {
                        using (var fs = new FileStream(fn, FileMode.Open))
                            ProtoParser.GetParser().GetLexer().Parse(fs).ForEach(u => test.Log(u.ToString()));
                        test.Log($"SUCCESS parse {fn}");
                    }
                    catch (Exception e)
                    {
                        test.Log($"FAILED parse {fn}: {e}");
                        throw;
                    }
                }
            }
        }

        [Test]
        public void TestAstProtoFiles()
        {
            using (var test = CreateTest(MethodBase.GetCurrentMethod().Name))
            {
                foreach (var fn in GetAllProtoFiles())
                {
                    test.Log($"START parse {fn}");
                    try
                    {
                        using (var fs = new FileStream(fn, FileMode.Open))
                            ProtoParser.GetParser().Parse(fs);
                        test.Log($"SUCCESS parse {fn}");
                    }
                    catch (Exception e)
                    {
                        test.Log($"FAILED parse {fn}: {e}");
                        throw;
                    }
                }
            }
        }

        #endregion
    }
}