﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Valkyrie.Threading.Async;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class AsyncOpUnitTest : GenericUnitTests
    {
        [Test]
        public void GenericSubject()
        {
            using (var test = CreateTest("GenericSubject"))
            {
                var s = new Subject<string>();

                var iterationCount = 10;

                var values = new List<string>();
                var errors = new List<Exception>();
                var completed = false;
                using (s.Subscribe(value => values.Add(value), exception => errors.Add(exception), () => completed = true))
                {
                    for (var i = 0; i < iterationCount; i++)
                    {
                        s.OnNext(i.ToString());
                        s.OnError(new Exception(i.ToString()));
                    }
                    s.OnCompleted();
                }
                
                Assert.AreEqual(false, completed);
                Assert.AreEqual(1, values.Count);
                Assert.AreEqual(1, errors.Count);

                for (var i = 0; i < 1; i++)
                {
                    Assert.AreEqual(i.ToString(), values[i]);
                    Assert.AreEqual(i.ToString(), errors[i].Message);
                }
            }
        }
        [Test]
        public void NonGenericSubject()
        {
            using (var test = CreateTest("NonGenericSubject"))
            {
                var s = new Subject();

                var iterationCount = 10;

                var values = new List<string>();
                var errors = new List<Exception>();
                var completed = false;
                using (s.Subscribe(() => values.Add(string.Empty), exception => errors.Add(exception), () => completed = true))
                {
                    for (var i = 0; i < iterationCount; i++)
                    {
                        s.OnNext();
                        s.OnError(new Exception(i.ToString()));
                    }
                    s.OnCompleted();
                }
                
                Assert.AreEqual(false, completed);
                Assert.AreEqual(1, values.Count);
                Assert.AreEqual(1, errors.Count);

                for (var i = 0; i < 1; i++)
                {
                    Assert.AreEqual(string.Empty, values[i]);
                    Assert.AreEqual(i.ToString(), errors[i].Message);
                }
            }
        }
        [Test]
        public void GenericSubjectNonCompleteOnError()
        {
            using (var test = CreateTest("NonCompleteSubject"))
            {
                var s = new Subject<string>() {CompleteOnError = false};

                var iterationCount = 10;

                var values = new List<string>();
                var errors = new List<Exception>();
                var completed = false;
                using (s.Subscribe(value => values.Add(value), exception => errors.Add(exception), () => completed = true))
                {
                    for (var i = 0; i < iterationCount; i++)
                    {
                        s.OnNext(i.ToString());
                        s.OnError(new Exception(i.ToString()));
                    }
                    s.OnCompleted();
                }
                
                Assert.AreEqual(true, completed);
                Assert.AreEqual(iterationCount, values.Count);
                Assert.AreEqual(iterationCount, errors.Count);

                for (var i = 0; i < iterationCount; i++)
                {
                    Assert.AreEqual(i.ToString(), values[i]);
                    Assert.AreEqual(i.ToString(), errors[i].Message);
                }
            }
        }
        [Test]
        public void NonGenericSubjectNonCompleteOnError()
        {
            using (var test = CreateTest("NonGenericSubject"))
            {
                var s = new Subject() {CompleteOnError = false};

                var iterationCount = 10;

                var values = new List<string>();
                var errors = new List<Exception>();
                var completed = false;
                using (s.Subscribe(() => values.Add(string.Empty), exception => errors.Add(exception), () => completed = true))
                {
                    for (var i = 0; i < iterationCount; i++)
                    {
                        s.OnNext();
                        s.OnError(new Exception(i.ToString()));
                    }
                    s.OnCompleted();
                }
                
                Assert.AreEqual(true, completed);
                Assert.AreEqual(iterationCount, values.Count);
                Assert.AreEqual(iterationCount, errors.Count);

                for (var i = 0; i < iterationCount; i++)
                {
                    Assert.AreEqual(string.Empty, values[i]);
                    Assert.AreEqual(i.ToString(), errors[i].Message);
                }
            }
        }

        [Test]
        public void ChainTest()
        {
            var chainCalled = false;
            Observable.Completed().Chain(() => chainCalled = true);
            Assert.AreEqual(true, chainCalled);
            chainCalled = false;
            var s = new Subject();
                s.Chain(() => chainCalled = true);
            s.OnCompleted();
            
            Assert.AreEqual(true, chainCalled);
        }

        [Test]
        public void WhereTest()
        {
            var source = new Subject<int>() {CompleteOnError = false};

            var iterations = 10;
            var valueCounter = 0;
            var exceptionCounter = 0;
            using (source.Where(value => value % 2 == 0, ex => ex is ObjectDisposedException)
                .Subscribe(value => valueCounter++, exception => exceptionCounter++))
            {
                for (var i = 0; i < iterations; ++i)
                {
                    source.OnNext(i);
                    source.OnError(new Exception());
                }
                source.OnError(new ObjectDisposedException("UNKNOWN"));
                source.OnError(new ArrayTypeMismatchException());
                source.OnError(new ObjectDisposedException("UNKNOWN"));
                source.OnError(new ArrayTypeMismatchException());
            }

            Assert.AreEqual((int) System.Math.Floor((float) iterations / 2), valueCounter);
            Assert.AreEqual(2, exceptionCounter);
        }
    }
}