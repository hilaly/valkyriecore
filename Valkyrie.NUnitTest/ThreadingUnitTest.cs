﻿using System.Threading;
using NUnit.Framework;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class ThreadingUnitTest : GenericUnitTests
    { 
        static void WaitInThread(ITest test, int delay)
        {
            var name = $"Wait{delay}ms";

            test.Log("Start task {0} on thread {1}[{2}]", name, Thread.CurrentThread.Name, Thread.CurrentThread.ManagedThreadId);
            Thread.Sleep(delay);
            test.Log("End task {0} on thread {1}[{2}]", name, Thread.CurrentThread.Name, Thread.CurrentThread.ManagedThreadId);
        }

        [Test]
        public void TestParallelExecution()
        {
            using (var test = CreateTest("ParallelTasksExecution"))
            {
                var mtid = Thread.CurrentThread.ManagedThreadId;
                test.Log("Main thread {0}", mtid);

                var dispatcher = test.Container.Resolve<IDispatcher>();

                var task0completion = false;
                var task1completion = false;

                WaitInThread(test, 1);
                
                using (var d = new CompositeDisposable())
                {
                    d.Add(dispatcher.ExecuteOnPoolThread(() => WaitInThread(test, 1))
                        .Chain(() =>
                        {
                            task0completion = true;
                            Assert.AreNotEqual(mtid, Thread.CurrentThread.ManagedThreadId);
                        }));
                    d.Add(dispatcher.ExecuteOnPoolThread(() => WaitInThread(test, 3))
                        .Chain(() =>
                        {
                            task1completion = true;
                            Assert.AreNotEqual(mtid, Thread.CurrentThread.ManagedThreadId);
                        }));
                
                    dispatcher.WaitAll();
                    WaitInThread(test, 5);
                }

                Assert.AreEqual(true, task0completion);
                Assert.AreEqual(true, task1completion);

                test.Log("Tasks finished");
            }
        }

        [Test]
        public void TestMainThreadExecution()
        {
            using (var test = CreateTest("MainThreadTasksExecution"))
            {
                var mtid = Thread.CurrentThread.ManagedThreadId;
                test.Log("Main thread {0}", mtid);

                var dispatcher = test.Container.Resolve<IDispatcher>();

                var task0completion = false;
                var task1completion = false;

                using (var d = new CompositeDisposable())
                {
                    d.Add(dispatcher.ExecuteOnPoolThread(() => WaitInThread(test, 10))
                        .Chain(() =>
                        {
                            task0completion = true;
                            Assert.AreNotEqual(mtid, Thread.CurrentThread.ManagedThreadId);
                        }));
                    d.Add(dispatcher.ExecuteOnMainThread(() => WaitInThread(test, 30))
                        .Chain(() =>
                        {
                            task1completion = true;
                            Assert.AreEqual(mtid, Thread.CurrentThread.ManagedThreadId);
                        }));
                
                    dispatcher.WaitAll();
                }
                WaitInThread(test, 100);

                Assert.AreEqual(true, task0completion);
                Assert.AreEqual(true, task1completion);

                test.Log("Tasks finished");
            }
        }

        [Test]
        public void TestMainThreadExecutionAll()
        {
            using (var test = CreateTest("AllTasksOnMainThreadExecution"))
            {
                var mtid = Thread.CurrentThread.ManagedThreadId;
                test.Log("Main thread {0}", mtid);

                var dispatcher = test.Container.Resolve<IDispatcher>();

                var task0completion = false;
                var task1completion = false;

                using (var d = new CompositeDisposable())
                {
                    d.Add(dispatcher.ExecuteOnMainThread(() => WaitInThread(test, 10))
                        .Chain(() =>
                        {
                            task0completion = true;
                            Assert.AreEqual(mtid, Thread.CurrentThread.ManagedThreadId);
                        }));
                    d.Add(dispatcher.ExecuteOnMainThread(() => WaitInThread(test, 30))
                        .Chain(() =>
                        {
                            task1completion = true;
                            Assert.AreEqual(mtid, Thread.CurrentThread.ManagedThreadId);
                        }));
                
                    dispatcher.WaitAll();
                }

                Assert.AreEqual(true, task0completion);
                Assert.AreEqual(true, task1completion);

                test.Log("Tasks finished");
            }
        }
        
        [Test]
        public void TestQueueExecution()
        {
            using (var test = CreateTest("QueueTasksExecution"))
            {
                var mtid = Thread.CurrentThread.ManagedThreadId;
                test.Log("Main thread {0}", mtid);

                var dispatcher = test.Container.Resolve<IDispatcher>();

                var task0completion = false;
                var task1completion = false;
                var task2completion = false;

                using (var d = new CompositeDisposable())
                {
                    d.Add(dispatcher.ExecuteOnAnyThread(() => WaitInThread(test, 1))
                        .Chain(() =>
                        {
                            task0completion = true;
                            d.Add(dispatcher.ExecuteOnAnyThread(() => WaitInThread(test, 2))
                                .Chain(() =>
                                {
                                    task1completion = true;
                                    d.Add(dispatcher.ExecuteOnAnyThread(() => WaitInThread(test, 3))
                                        .Chain(() =>
                                        {
                                            task2completion = true;
                                        }));
                                }));
                        }));
                
                    dispatcher.WaitAll();
                }

                Assert.AreEqual(true, task0completion);
                Assert.AreEqual(true, task1completion);
                Assert.AreEqual(true, task2completion);

                test.Log("Tasks finished");
            }
        }
    }
}