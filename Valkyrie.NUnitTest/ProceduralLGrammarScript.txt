sp1 = { Sphere(1) }
sp2 = { Sphere(2) }

b1 = { Box(.3,.1,.15) }


main1 = {
  Union(
    Sphere(0.1), Rotate(Translate( Union(b1(),Rotate(Translate(b1(),1,0,0),0,45,0)) ,1,0,0),0,45,0)
  )
}

main = {
Translate(Scale(Union(
   Translate(Scale(Sphere(.5),1,.5,.5), 1,0.5,0.5),
   Sphere(.5)
),1,.5,.5),1,.5,.5)
}