using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Valkyrie.Procedural.Fields;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class GrammarUnitTest : GenericUnitTests
    {
        private const string SimpleGrammarText = "node -> node | EMPTY\n";

        private const string ComplexGrammarText =
            "<node-empty-def> ::= \n" +
            "<node-field-def> ::= <node-empty-def> | EMPTY \n" +
            
            "<node-opt-BOA-def> ::= BEFORE [OPT] AFTER \n" +
            "<node-opt-BO-def> ::= BEFORE [OPT] \n" +
            "<node-opt-OA-def> ::= [OPT] AFTER\n" +
            "<node-opt-O-def> ::= [OPT] \n" +
            
            "<node-zeroOrAny-BOA-def> ::= BEFORE OPT* AFTER \n" +
            "<node-zeroOrAny-BO-def> ::= BEFORE OPT* \n" +
            "<node-zeroOrAny-OA-def> ::= OPT* AFTER \n" +
            "<node-zeroOrAny-O-def> ::=  OPT*  \n" +
            
            "<node-oneOrAny-BOA-def> ::= BEFORE OPT+ AFTER \n" +
            "<node-oneOrAny-BO-def> ::= BEFORE OPT+ \n" +
            "<node-oneOrAny-OA-def> ::= OPT+ AFTER \n" +
            "<node-oneOrAny-O-def> ::=  OPT+  \n";
        
        private const string ScriptText = "collage { sphere(3.0) + sphere(4) }\ncollage = { sphere(3.0) + sphere(4) }";

        [Test]
        public void GrammarDefTest()
        {
            using (var test = CreateTest("GrammarDefTest"))
            {
                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(SimpleGrammarText)))
                {
                    var grammarDef = Grammar.Grammar.Parse(stream);
                    test.Log("Simple grammar: " + grammarDef);   
                }
                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(ComplexGrammarText)))
                {
                    var grammarDef = Grammar.Grammar.Parse(stream);
                    test.Log("Complex grammar: " + grammarDef);   
                }
            }
        }

        [Test]
        public void GrammerLexerTest()
        {
            using (var test = CreateTest("GrammarLexerTest"))
            {
                using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(ScriptText)))
                    test.Log(string.Join("\n",
                        Factory.GetSdfGrammarParser().GetLexer().Parse(stream).Select(u => u.ToString())));
            }
        }

        [Test]
        public void GrammarAstConstructorTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                var constructor = Factory.GetSdfGrammarParser();
                using (var scriptStream = File.OpenRead("ProceduralLGrammarScript.txt"))
                    test.Log(constructor.Parse(scriptStream).ToString());
            }
        }

        [Test]
        public void GrammarCompilerTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                var constructor = Factory.GetSdfGrammarParser();
                using (var scriptStream = File.OpenRead("ProceduralLGrammarScript.txt"))
                {
                    var node = constructor.Parse(scriptStream);
                    var fp = node.Compile();
                    test.Log(fp.ToString());
                }
            }
        }

        [Test]
        public void SdfGrammarParsingTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                using (var scriptStream = File.OpenRead("ProceduralLGrammarScript.txt"))
                {
                    var fp = Factory.Parse(scriptStream);
                    test.Log(fp.ToString());
                }
            }
        }

        [Test]
        public void GrammarSelfCompilerTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                using (var parserStream = File.OpenRead("ParserTest.txt"))
                {
                    var parser = Grammar.Grammar.Create(parserStream);
                    test.Log(parser.GetLexer().ToString());
                    test.Log(parser.ToString());
                    using (var scriptStream = File.OpenRead("TestGrammar.txt"))
                    {
                        var lexems = parser.GetLexer().Parse(scriptStream);
                        test.Log(string.Join("\n", lexems.Select(u => u.ToString())));
                        var ast = parser.Parse(scriptStream);
                        test.Log(ast.ToString());
                    }
                }
            }
        }
    }
}