﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Valkyrie.Di;

// ReSharper disable ClassNeverInstantiated.Local
// ReSharper disable UnusedAutoPropertyAccessor.Local
// ReSharper disable InconsistentNaming
// ReSharper disable UnusedMember.Local

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class DependencyInjectionUnitTest : GenericUnitTests
    {
        #region Helpers

        private interface IDummy
        {
            
        }

        private class DummyClass : IDummy
        {
            
        }

        private interface IDependant
        {
            IDummy Dependency { get; }
        }

        private class DependantClass : IDependant
        {
            public IDummy Dependency { get; }

            public DependantClass(IDummy dependency)
            {
                Dependency = dependency;
            }
        }

        private class DerivedClass : DependantClass
        {
            public IDummy Second { get; }

            public DerivedClass(IDummy dependency, IDummy second) : base(dependency)
            {
                Second = second;
            }
        }

        private static IContainer DefaultContainer =>
            Configurator.Start(Assembly.GetCallingAssembly().FullName);

        private class ConditionalConstructorDependency
        {
            public IDummy Required { get; }
            public IDummy Optional { get; }

            public ConditionalConstructorDependency(IDummy required, [Inject(Name = "Invalid", IsOptional = true)] IDummy optional)
            {
                Required = required;
                Optional = optional;
            }
        }

        private class DoubleDependency
        {
            public IDummy Unnamed { get; }
            public IDummy Named { get; }

            public DoubleDependency(IDummy unnamed, [Inject(Name = "Name")] IDummy named)
            {
                Unnamed = unnamed;
                Named = named;
            }
        }

        private class FieldDependency
        {
#pragma warning disable 649
            [Inject] public IDummy Unnamed;
            [Inject(Name = "Name")] public IDummy Named;
            
            [Inject] private IDummy _unnamed;
            [Inject(Name = "Name")] private IDummy _named;
#pragma warning restore 649

            // ReSharper disable once ConvertToAutoProperty
            public IDummy NamedPrivate => _named;
            // ReSharper disable once ConvertToAutoProperty
            public IDummy UnnamedPrivate => _unnamed;
        }

        private class PropertyDependency
        {
            [Inject] public IDummy Unnamed { get; set; }
            [Inject(Name = "Name")] public IDummy Named { get; set; }
            
            [Inject] private IDummy _unnamed { get; set; }
            [Inject(Name = "Name")] private IDummy _named { get; set; }

            // ReSharper disable once ConvertToAutoProperty
            public IDummy NamedPrivate => _named;
            // ReSharper disable once ConvertToAutoProperty
            public IDummy UnnamedPrivate => _unnamed;
        }

        private class MethodDependency
        {
            [Inject] public void InjectPublic([Inject(Name = "Name")] IDummy named, IDummy unnamed)
            {
                Unnamed = unnamed;
                Named = named;
            }
            
            [Inject] private void InjectPrivate([Inject(Name = "Name")] IDummy named, IDummy unnamed)
            {
                _unnamed = unnamed;
                _named = named;
            }
            
            public IDummy Unnamed { get; private set; }
            public IDummy Named { get; private set; }
            
            private IDummy _unnamed { get; set; }
            private IDummy _named { get; set; }

            // ReSharper disable once ConvertToAutoProperty
            public IDummy NamedPrivate => _named;
            // ReSharper disable once ConvertToAutoProperty
            public IDummy UnnamedPrivate => _unnamed;
        }
        
        private class OnActivatingCall
        {
            public bool ActivatedCalled { get; set; }
        }
        
        private class DisposableClass : IDisposable
        {
            public void Dispose()
            {
                Disposed = true;
            }

            public bool Disposed { get; private set; }
        }
        #endregion

        [Test]
        public void AContainerCreation()
        {
            Assert.NotNull(DefaultContainer);
        }
        
        [Test]
        public void ContainerResolveContainerAsSelf()
        {
            var container = DefaultContainer;
            
            container.Build();

            Assert.AreSame(container, container.Resolve<IContainer>());
        }

        #region OnActivating
        
        [Test]
        public void InstanceOnActivating()
        {
            var container = DefaultContainer;
            container.Register(new OnActivatingCall()).AsInterfacesAndSelf().OnActivation(ctx => ctx.Instance.ActivatedCalled = true);
            container.Build();
            
            Assert.NotNull(container.Resolve<OnActivatingCall>());
            Assert.IsTrue(container.Resolve<OnActivatingCall>().ActivatedCalled);
        }

        [Test]
        public void TypeOnActivating()
        {
            var container = DefaultContainer;
            container.Register<OnActivatingCall>().AsInterfacesAndSelf().OnActivation(ctx => ctx.Instance.ActivatedCalled = true).InstancePerDependency();
            container.Build();
            
            Assert.NotNull(container.Resolve<OnActivatingCall>());
            Assert.IsTrue(container.Resolve<OnActivatingCall>().ActivatedCalled);
        }

        [Test]
        public void FactoryOnActivating()
        {
            var container = DefaultContainer;
            container.Register(() => new OnActivatingCall()).AsInterfacesAndSelf().OnActivation(ctx => ctx.Instance.ActivatedCalled = true).InstancePerDependency();
            container.Build();
            
            Assert.NotNull(container.Resolve<OnActivatingCall>());
            Assert.IsTrue(container.Resolve<OnActivatingCall>().ActivatedCalled);
        }
        
        #endregion
        
        #region Disposing

        [Test]
        public void InstanceDispose()
        {
            var root = DefaultContainer;
            var container = root.CreateChild();
            var child = container.CreateChild();

            child.Register(new DisposableClass()).AsInterfacesAndSelf();
            child.Build();
            container.Register(new DisposableClass()).AsInterfacesAndSelf();
            container.Build();

            var instance = child.TryResolve<DisposableClass>();
            Assert.NotNull(instance);
            child.Dispose();
            Assert.IsTrue(instance.Disposed);

            instance = container.TryResolve<DisposableClass>();
            Assert.NotNull(instance);
            container.Dispose();
            Assert.IsTrue(instance.Disposed);
        }
        
        [Test]
        public void TypeDispose()
        {
            var root = DefaultContainer;
            var container = root.CreateChild();

            container.Register<DisposableClass>().AsInterfacesAndSelf().InstancePerDependency();
            container.Build();

            var instance0 = container.TryResolve<DisposableClass>();
            var instance1 = container.TryResolve<DisposableClass>();
            Assert.NotNull(instance0);
            Assert.NotNull(instance1);
            Assert.AreNotSame(instance0, instance1);
            root.Dispose();
            Assert.IsTrue(instance0.Disposed);
            Assert.IsTrue(instance1.Disposed);
        }
        
        [Test]
        public void FactoryDispose()
        {
            var root = DefaultContainer;
            var container = root.CreateChild();

            container.Register(() => new DisposableClass()).AsInterfacesAndSelf().InstancePerDependency();
            container.Build();

            var instance0 = container.TryResolve<DisposableClass>();
            var instance1 = container.TryResolve<DisposableClass>();
            Assert.NotNull(instance0);
            Assert.NotNull(instance1);
            Assert.AreNotSame(instance0, instance1);
            root.Dispose();
            Assert.IsTrue(instance0.Disposed);
            Assert.IsTrue(instance1.Disposed);
        }
        
        #endregion
        
        #region Simple resolving
        
        #region Instances

        [Test]
        public void UnnamedInstanceInterfaceResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance).As<IDummy>();

            container.Build();
            
            Assert.IsNull(container.TryResolve<DummyClass>());
            Assert.IsNull(container.TryResolve<IDummy>("Name"));
            Assert.AreSame(instance, container.TryResolve<IDummy>());
            Assert.AreSame(container.TryResolve<IDummy>(), container.TryResolve<IDummy>());
        }

        [Test]
        public void NamedInstanceInterfaceResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance, "Name").As<IDummy>();

            container.Build();
            
            Assert.IsNull(container.TryResolve<DummyClass>("Name"));
            Assert.IsNull(container.TryResolve<IDummy>());
            Assert.AreSame(instance, container.TryResolve<IDummy>("Name"));
            Assert.AreSame(container.TryResolve<IDummy>("Name"), container.TryResolve<IDummy>("Name"));
        }

        [Test]
        public void UnnamedInstanceSingleInstanceResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance).AsSelf();

            container.Build();
            
            Assert.AreSame(instance, container.TryResolve<DummyClass>());
            Assert.AreSame(instance, container.TryResolve<DummyClass>());
            Assert.AreSame(container.TryResolve<DummyClass>(), container.TryResolve<DummyClass>());
        }

        [Test]
        public void UnnamedInstanceRegistrationUnnamedResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance).AsSelf();

            container.Build();
            Assert.AreSame(instance, container.TryResolve<DummyClass>());
        }

        [Test]
        public void UnnamedInstanceRegistrationNamedResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance).AsSelf();

            container.Build();
            Assert.Null(container.TryResolve<DummyClass>("Name"));
        }

        [Test]
        public void NamedInstanceRegistrationUnnamedResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance, "Name").AsSelf();

            container.Build();
            Assert.Null(container.TryResolve<DummyClass>());
        }

        [Test]
        public void NamedInstanceRegistrationNamedResolved()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance, "Name").AsSelf();

            container.Build();
            Assert.AreSame(instance, container.TryResolve<DummyClass>("Name"));
        }

        [Test]
        public void UnnamedAndNamedInstanceRegistrationUnnamedResolved()
        {
            var instance = new DummyClass();
            var namedInstance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance).AsSelf();
            container.Register(namedInstance, "Name").AsSelf();

            container.Build();
            Assert.AreSame(instance, container.TryResolve<DummyClass>());
            Assert.AreNotSame(namedInstance, container.TryResolve<DummyClass>());
        }

        [Test]
        public void UnnamedAndNamedInstanceRegistrationNamedResolved()
        {
            var instance = new DummyClass();
            var namedInstance = new DummyClass();
            
            var container = DefaultContainer;

            container.Register(instance).AsSelf();
            container.Register(namedInstance, "Name").AsSelf();

            container.Build();
            Assert.AreSame(namedInstance, container.TryResolve<DummyClass>("Name"));
            Assert.AreNotSame(instance, container.TryResolve<DummyClass>("Name"));
        }
        
        #endregion
        
        #region Factories registration

        [Test]
        public void UnnamedFactoryRegistrationUnnamedResolvedSingleInstance()
        {
            DummyClass Factory() => new DummyClass();

            var container = DefaultContainer;

            container.Register(Factory).AsSelf().SingleInstance();
            
            container.Build();
            Assert.AreSame(container.TryResolve<DummyClass>(), container.TryResolve<DummyClass>());
        }
        
        [Test]
        public void UnnamedFactoryRegistrationNamedResolvedSingleInstance()
        {
            DummyClass Factory() => new DummyClass();

            var container = DefaultContainer;

            container.Register(Factory).AsSelf().SingleInstance();
            
            container.Build();
            Assert.Null(container.TryResolve<DummyClass>("Name"));
        }

        [Test]
        public void NamedFactoryRegistrationUnnamedResolvedSingleInstance()
        {
            DummyClass Factory() => new DummyClass();

            var container = DefaultContainer;

            container.Register(Factory, "Name").AsSelf().SingleInstance();
            
            container.Build();
            Assert.AreSame(container.TryResolve<DummyClass>("Name"), container.TryResolve<DummyClass>("Name"));
        }
        
        [Test]
        public void NamedFactoryRegistrationNamedResolvedSingleInstance()
        {
            DummyClass Factory() => new DummyClass();

            var container = DefaultContainer;

            container.Register(Factory, "Name").AsSelf().SingleInstance();
            
            container.Build();
            Assert.Null(container.TryResolve<DummyClass>());
        }
        
        #endregion
        
        #region Types registration

        [Test]
        public void UnnamedTypeRegistrationUnnamedResolvedSingleInstance()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().AsSelf().SingleInstance();
            
            container.Build();
            Assert.AreSame(container.TryResolve<DummyClass>(), container.TryResolve<DummyClass>());
        }
        
        [Test]
        public void UnnamedTypeRegistrationNamedResolvedSingleInstance()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().AsSelf().SingleInstance();
            
            container.Build();
            Assert.Null(container.TryResolve<DummyClass>("Name"));
        }

        [Test]
        public void NamedTypeRegistrationUnnamedResolvedSingleInstance()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>("Name").AsSelf().SingleInstance();
            
            container.Build();
            Assert.AreSame(container.TryResolve<DummyClass>("Name"), container.TryResolve<DummyClass>("Name"));
        }
        
        [Test]
        public void NamedTypeRegistrationNamedResolvedSingleInstance()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>("Name").AsSelf().SingleInstance();
            
            container.Build();
            Assert.Null(container.TryResolve<DummyClass>());
        }
        
        #endregion
        
        #endregion
        
        #region Constructor Dependency Injection
        
        #region Factory

        [Test]
        public void UnnamedDependantUnnamedDependency()
        {
            DummyClass DependencyFactory() => new DummyClass();
            DependantClass DependantFactory(IContainer c) => new DependantClass(c.Resolve<IDummy>());
            
            var container = DefaultContainer;

            container.Register(DependencyFactory).As<IDummy>().SingleInstance();
            container.Register(DependantFactory).As<IDependant>().SingleInstance();
            
            container.Build();

            Assert.NotNull(container.TryResolve<IDummy>());
            Assert.NotNull(container.TryResolve<IDependant>());
            Assert.AreSame(container.TryResolve<IDependant>(), container.TryResolve<IDependant>());
            Assert.AreSame(container.TryResolve<IDependant>().Dependency, container.TryResolve<IDummy>());
        }
        
        #endregion

        [Test]
        public void SingleInstanceDependency()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().As<IDummy>().SingleInstance();
            container.Register<DerivedClass>().AsSelf().InstancePerDependency();
            container.Build();

            Assert.NotNull(container.TryResolve<DerivedClass>());
            var der = container.Resolve<DerivedClass>();
            Assert.AreSame(der.Dependency, der.Second);
            var sec = container.Resolve<DerivedClass>();
            Assert.AreSame(sec.Dependency, sec.Second);
            Assert.AreSame(der.Dependency, sec.Second);
        }

        [Test]
        public void ScopeInstanceDependency()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().As<IDummy>().InstancePerScope();
            container.Register<DerivedClass>().AsSelf().InstancePerDependency();
            container.Build();

            Assert.NotNull(container.TryResolve<DerivedClass>());
            var der = container.Resolve<DerivedClass>();
            Assert.AreSame(der.Dependency, der.Second);
            var sec = container.Resolve<DerivedClass>();
            Assert.AreSame(sec.Dependency, sec.Second);
            Assert.AreNotSame(der.Dependency, sec.Second);
        }

        [Test]
        public void MultiInstanceDependency()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().As<IDummy>().InstancePerDependency();
            container.Register<DerivedClass>().AsSelf().InstancePerDependency();
            container.Build();

            Assert.NotNull(container.TryResolve<DerivedClass>());
            var der = container.Resolve<DerivedClass>();
            Assert.AreNotSame(der.Dependency, der.Second);
            var sec = container.Resolve<DerivedClass>();
            Assert.AreNotSame(sec.Dependency, sec.Second);
            Assert.AreNotSame(der.Dependency, sec.Second);
        }

        [Test]
        public void NamedResolvingInConstructor()
        {
            var first = new DummyClass();
            var second = new DummyClass();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<DoubleDependency>().AsSelf().InstancePerDependency();

            container.Build();

            var d = container.TryResolve<DoubleDependency>();
            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
        }

        [Test]
        public void NotRequiredParameterInConstructor()
        {
            var first = new DummyClass();
            
            var container = DefaultContainer;
            
            container.Register(first).AsInterfaces();
            container.Register<ConditionalConstructorDependency>().AsSelf().InstancePerDependency();

            container.Build();

            var d = container.TryResolve<ConditionalConstructorDependency>();
            Assert.NotNull(d);
            Assert.AreSame(first, d.Required);
            Assert.Null(d.Optional);
        }
        
        #endregion
        
        #region Fields DI
        
        [Test]
        public void NamedResolvingInFields()
        {
            var first = new DummyClass();
            var second = new DummyClass();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<FieldDependency>().AsSelf().InstancePerDependency();

            container.Build();

            var d = container.TryResolve<FieldDependency>();
            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
            Assert.AreSame(second, d.NamedPrivate);
            Assert.AreSame(first, d.UnnamedPrivate);
        }
        
        [Test]
        public void InjectFieldsIntoObject()
        {
            var first = new DummyClass();
            var second = new DummyClass();
            var d = new FieldDependency();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<FieldDependency>().AsSelf().InstancePerDependency();

            container.Build();
            container.Inject(d);

            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
            Assert.AreSame(second, d.NamedPrivate);
            Assert.AreSame(first, d.UnnamedPrivate);
        }
        
        #endregion
        
        #region Properties DI
        
        [Test]
        public void NamedResolvingInProperties()
        {
            var first = new DummyClass();
            var second = new DummyClass();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<PropertyDependency>().AsSelf().InstancePerDependency();

            container.Build();

            var d = container.TryResolve<PropertyDependency>();
            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
            Assert.AreSame(second, d.NamedPrivate);
            Assert.AreSame(first, d.UnnamedPrivate);
        }
        
        [Test]
        public void InjectPropertiesIntoObject()
        {
            var first = new DummyClass();
            var second = new DummyClass();
            var d = new PropertyDependency();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<PropertyDependency>().AsSelf().InstancePerDependency();

            container.Build();

            container.Inject(d);
            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
            Assert.AreSame(second, d.NamedPrivate);
            Assert.AreSame(first, d.UnnamedPrivate);
        }
        
        #endregion
        
        #region Methods DI
        
        [Test]
        public void NamedResolvingInMethods()
        {
            var first = new DummyClass();
            var second = new DummyClass();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<MethodDependency>().AsSelf().InstancePerDependency();

            container.Build();

            var d = container.TryResolve<MethodDependency>();
            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
            Assert.AreSame(second, d.NamedPrivate);
            Assert.AreSame(first, d.UnnamedPrivate);
        }
        
        [Test]
        public void InjectMethodsIntoObject()
        {
            var first = new DummyClass();
            var second = new DummyClass();
            var d = new MethodDependency();

            var container = DefaultContainer;

            container.Register(first).AsInterfaces();
            container.Register(second, "Name").AsInterfaces();
            container.Register<MethodDependency>().AsSelf().InstancePerDependency();

            container.Build();

            container.Inject(d);
            Assert.NotNull(d);
            Assert.AreNotSame(d.Named, d.Unnamed);
            Assert.AreSame(second, d.Named);
            Assert.AreSame(first, d.Unnamed);
            Assert.AreSame(second, d.NamedPrivate);
            Assert.AreSame(first, d.UnnamedPrivate);
        }
        
        #endregion
        
        #region ChildContainers
        
        [Test]
        public void ChildContainerDonOverrideUnnamed()
        {
            var instance = new DummyClass();
            var childInstance = new DummyClass();
            
            var container = DefaultContainer;
            var childContainer = container.CreateChild();

            container.Register(instance).AsSelf();
            container.Build();

            childContainer.Register(childInstance, "Name").AsSelf();
            childContainer.Build();

            Assert.AreSame(instance, container.TryResolve<DummyClass>());
            Assert.AreSame(childInstance, childContainer.TryResolve<DummyClass>("Name"));
            Assert.AreNotSame(container.TryResolve<DummyClass>(), childContainer.TryResolve<DummyClass>("Name"));
        }

        [Test]
        public void ChildContainerDonOverrideNamed()
        {
            var instance = new DummyClass();
            var childInstance = new DummyClass();
            
            var container = DefaultContainer;
            var childContainer = container.CreateChild();

            container.Register(instance, "Name").AsSelf();
            container.Build();

            childContainer.Register(childInstance).AsSelf();
            childContainer.Build();

            Assert.AreSame(instance, container.TryResolve<DummyClass>("Name"));
            Assert.AreSame(childInstance, childContainer.TryResolve<DummyClass>());
            Assert.AreNotSame(container.TryResolve<DummyClass>("Name"), childContainer.TryResolve<DummyClass>());
        }

        [Test]
        public void ChildContainerOverrideUnnamed()
        {
            var instance = new DummyClass();
            var childInstance = new DummyClass();
            
            var container = DefaultContainer;
            var childContainer = container.CreateChild();

            container.Register(instance).AsSelf();
            container.Build();

            childContainer.Register(childInstance).AsSelf();
            childContainer.Build();

            Assert.AreNotSame(childContainer.TryResolve<DummyClass>(), container.TryResolve<DummyClass>());
            Assert.AreSame(instance, container.TryResolve<DummyClass>());
            Assert.AreSame(childInstance, childContainer.TryResolve<DummyClass>());
        }

        [Test]
        public void ChildContainerOverrideNamed()
        {
            var instance = new DummyClass();
            var childInstance = new DummyClass();
            
            var container = DefaultContainer;
            var childContainer = container.CreateChild();

            container.Register(instance, "Name").AsSelf();
            container.Build();

            childContainer.Register(childInstance, "Name").AsSelf();
            childContainer.Build();

            Assert.AreNotSame(childContainer.TryResolve<DummyClass>("Name"), container.TryResolve<DummyClass>("Name"));
            Assert.AreSame(instance, container.TryResolve<DummyClass>("Name"));
            Assert.AreSame(childInstance, childContainer.TryResolve<DummyClass>("Name"));
        }

        [Test]
        public void ChildContainerResolveDependantOnParent()
        {
            var instance = new DummyClass();
            
            var container = DefaultContainer;
            var childContainer = container.CreateChild();

            container.Register(instance).AsSelf().As<IDummy>();
            container.Build();

            childContainer.Register<DependantClass>("Name").AsSelf().InstancePerDependency();
            childContainer.Build();

            Assert.NotNull(childContainer.TryResolve<DependantClass>("Name"));
        }

        [Test]
        public void ResolveAllOnContainer()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().AsInterfaces().InstancePerDependency();
            container.Register<DummyClass>("1").AsInterfaces().InstancePerDependency();
            container.Register<DummyClass>("2").AsInterfaces().InstancePerDependency();
            container.Register<DummyClass>("3").AsInterfaces().InstancePerDependency();

            container.Build();

            var all = container.ResolveAll<IDummy>();
            Assert.AreEqual(4, all.Count());
        }

        [Test]
        public void ResolveAllOnChildContainer()
        {
            var container = DefaultContainer;

            container.Register<DummyClass>().AsInterfaces().InstancePerDependency();
            container.Register<DummyClass>("1").AsInterfaces().InstancePerDependency();
            container.Register<DummyClass>("2").AsInterfaces().InstancePerDependency();
            container.Register<DummyClass>("3").AsInterfaces().InstancePerDependency();

            container.Build();

            var childContainer = container.CreateChild();
            
            childContainer.Register<DummyClass>().AsInterfaces().InstancePerDependency();
            childContainer.Register<DummyClass>("1").AsInterfaces().InstancePerDependency();
            childContainer.Register<DummyClass>("2").AsInterfaces().InstancePerDependency();
            childContainer.Register<DummyClass>("3").AsInterfaces().InstancePerDependency();

            childContainer.Build();

            Assert.AreEqual(4, container.ResolveAll<IDummy>().Count());
            Assert.AreEqual(8, childContainer.ResolveAll<IDummy>().Count());
        }
        
        #endregion
    }
}