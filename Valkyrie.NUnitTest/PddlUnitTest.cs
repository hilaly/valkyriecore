using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using NUnit.Framework;
using Valkyrie.Ai.Planning;
using Valkyrie.Ai.Planning.Internal;
using Valkyrie.Data;
using Factory = Valkyrie.Ai.Planning.Internal.Factory;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class PddlUnitTest : GenericUnitTests
    {
        private string ForAllGrammarText = "(define (domain forall-domain)"
                                           + "(:action make-product"
                                           + ":parameters (?p - product)"
                                           + ":precondition (and (not (made ?p)))"
                                           + ":effect "
                                           + "             (forall (?o - order)"
                                           + "                   (when (includes ?o ?p))"
                                           + "                   (delivered ?o ?p) ) ) )";
        private string GrammarText = "(define (domain elevators-netbenefit-numeric)"
                                     + " " + "(:requirements :typing :numeric-fluents :goal-utilities)"
                                     + " " + "(:types 	elevator - object"
                                     + " " + "slow-elevator fast-elevator - elevator"
                                     + " " + "passenger - object"
                                     + " " + "floor - object"
                                     + " " + ")"
                                     + " " + "(:functions (total-cost) - number"
                                     + " " + "(travel-slow ?f1 - floor ?f2 - floor) - number"
                                     + " " + "(travel-fast ?f1 - floor ?f2 - floor) - number "
                                     + " " + "(passengers ?lift - elevator) - number"
                                     + " " + "(capacity ?lift - elevator) - number"
                                     + " " + ")"
                                     + " " + "(:action move-up-slow"
                                     + " " + ":parameters (?lift - slow-elevator ?f1 - floor ?f2 - floor )"
                                     + " :precondition (and (lift-at ?lift ?f1) (above ?f1 ?f2 ) (reachable-floor ?lift ?f2) )"
                                     + " :effect (and (lift-at ?lift ?f2) (not (lift-at ?lift ?f1)) (increase (total-cost) (travel-slow ?f1 ?f2))))"
                                     + " " + ")"
                                     + "(define (problem elevators-netbenefit-p8_3_1)"
                                     + "(:domain elevators-netbenefit-numeric)"
                                     + "(:objects f0 f1 f2 f3 f4 f5 f6 f7 f8  - floor p0 p1 p2  - passenger fast0  - fast-elevator slow0-0 slow1-0 - slow-elevator)"
                                     + "(:init"
                                     + "(above f0 f1) (above f0 f2) (above f0 f3) (above f0 f4) (above f0 f5) (above f0 f6) (above f0 f7) (above f0 f8) "
                                     + "(above f1 f2) (above f1 f3) (above f1 f4) (above f1 f5) (above f1 f6) (above f1 f7) (above f1 f8) "
                                     + "(above f2 f3) (above f2 f4) (above f2 f5) (above f2 f6) (above f2 f7) (above f2 f8) "
                                     + "(above f3 f4) (above f3 f5) (above f3 f6) (above f3 f7) (above f3 f8) "
                                     + "(above f4 f5) (above f4 f6) (above f4 f7) (above f4 f8) "
                                     + "(above f5 f6) (above f5 f7) (above f5 f8)" 
                                     + "(above f6 f7) (above f6 f8)" 
                                     + "(above f7 f8) "
                                     + "(lift-at fast0 f0)"
                                     + "(= (passengers fast0) 0)"
                                     + "(= (capacity fast0) 3)"
                                     + "(reachable-floor fast0 f0)(reachable-floor fast0 f2)(reachable-floor fast0 f4)(reachable-floor fast0 f6)(reachable-floor fast0 f8)"
                                     + ""
                                     + "(lift-at slow0-0 f2)"
                                     + "(= (passengers slow0-0) 0)"
                                     + "(= (capacity slow0-0) 2)"
                                     + "(reachable-floor slow0-0 f0)(reachable-floor slow0-0 f1)(reachable-floor slow0-0 f2)(reachable-floor slow0-0 f3)(reachable-floor slow0-0 f4)"
                                     + ""
                                     + "(lift-at slow1-0 f4)"
                                     + "(= (passengers slow1-0) 0)"
                                     + "(= (capacity slow1-0) 2)"
                                     + "(reachable-floor slow1-0 f4)(reachable-floor slow1-0 f5)(reachable-floor slow1-0 f6)(reachable-floor slow1-0 f7)(reachable-floor slow1-0 f8)"
                                     + ""
                                     + "(passenger-at p0 f8)"
                                     + "(passenger-at p1 f3)"
                                     + "(passenger-at p2 f2)"
                                     + ""
                                     + "(= (travel-slow f0 f1) 6) (= (travel-slow f0 f2) 7) (= (travel-slow f0 f3) 8) (= (travel-slow f0 f4) 9) (= (travel-slow f1 f2) 6) (= (travel-slow f1 f3) 7) (= (travel-slow f1 f4) 8) (= (travel-slow f2 f3) 6) (= (travel-slow f2 f4) 7) (= (travel-slow f3 f4) 6)" 
                                     + ""
                                     + "(= (travel-slow f4 f5) 6) (= (travel-slow f4 f6) 7) (= (travel-slow f4 f7) 8) (= (travel-slow f4 f8) 9) (= (travel-slow f5 f6) 6) (= (travel-slow f5 f7) 7) (= (travel-slow f5 f8) 8) (= (travel-slow f6 f7) 6) (= (travel-slow f6 f8) 7) (= (travel-slow f7 f8) 6)" 
                                     + ""
                                     + ""
                                     + "(= (travel-fast f0 f2) 7) (= (travel-fast f0 f4) 13) (= (travel-fast f0 f6) 19) (= (travel-fast f0 f8) 25)" 
                                     + ""
                                     + "(= (travel-fast f2 f4) 7) (= (travel-fast f2 f6) 13) (= (travel-fast f2 f8) 19)" 
                                     + ""
                                     + "(= (travel-fast f4 f6) 7) (= (travel-fast f4 f8) 13)" 
                                     + ""
                                     + "(= (travel-fast f6 f8) 7)" 
                                     + ""
                                     + "(= (total-cost) 0)"
                                     + ""
                                     + ")"
                                     + ""
                                     + "(:goal"
                                     + "(and"
                                     + "(passenger-at p0 f4) "
                                     + "(passenger-at p1 f6) "
                                     + "(passenger-at p2 f1)" 
                                     + "))"
                                     + ")";
        List<KeyValuePair<string, string>> GetPddlProblems()
        {
            string GetRootDirectory()
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.Combine(Path.GetDirectoryName(path), "./../..");
            }

            var directoryInfo = new DirectoryInfo(GetRootDirectory());
            TestContext.Out.WriteLine($"Start founding pddl files in {directoryInfo.FullName}");

            var files = directoryInfo.GetFiles("*.pddl", SearchOption.AllDirectories).ToList();
            var result = new List<KeyValuePair<string, string>>();
            foreach (var domainFile in files)
            {
                if (!domainFile.Name.Contains("-domain"))
                    continue;
                var problemName = domainFile.Name.Replace("-domain", "");
                var problemFile = files.Find(u => u.Name == problemName);
                if (problemFile == null)
                    TestContext.Out.WriteLine($"Can not find problem {problemName} for domain {domainFile.Name}");
                else
                    result.Add(new KeyValuePair<string, string>(domainFile.FullName, problemFile.FullName));
            }

            TestContext.Out.WriteLine(
                $"Found {result.Count} pddl pairs");
            return result;
        }

        [Test]
        public void GrammarLexerTest()
        {
            using (var test = CreateTest("GrammarLexerTest"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();
                var compiler = pSystem.Compiler;
                
                var collection = new HashSet<string>();

                foreach (var valuePair in GetPddlProblems())
                {
                    string fn = valuePair.Key;
                    try
                    {
                        using (var fs = new FileStream(fn, FileMode.Open))
                            foreach (var lexem in pSystem.Parser.GetLexer().Parse(fs))
                                collection.Add(lexem.Value);
                        using (var fs = new FileStream(fn = valuePair.Value, FileMode.Open))
                            foreach (var lexem in pSystem.Parser.GetLexer().Parse(fs))
                                collection.Add(lexem.Value);
                    }
                    catch (Exception e)
                    {
                        test.Log($"{fn}: {e}");
                        throw;
                    }
                }

                test.Log("Unique Lexems {1}:\n {0}",
                    string.Join("\n ", collection.OrderBy(u => u)), collection.Count);
            }
        }

        [Test]
        public void GrammarAstConstructorTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();
                var compiler = pSystem.Compiler;
                
                var constructor = pSystem.Parser;
                using (var scriptStream = new MemoryStream(Encoding.UTF8.GetBytes(GrammarText)))
                    test.Log("Domain:\n{0}", constructor.Parse(scriptStream).ToString());

                return;

                foreach (var valuePair in GetPddlProblems())
                {
                    try
                    {
                        using (var scriptStream = File.OpenRead(valuePair.Key))
                            test.Log("Domain:\n{0}", constructor.Parse(scriptStream).ToString());
                        using (var scriptStream = File.OpenRead(valuePair.Value))
                            test.Log("Problem:\n{0}", constructor.Parse(scriptStream).ToString());
                    }
                    catch (Exception e)
                    {
                        test.Log($"{valuePair.Key}: {e}");
                        throw;
                    }
                }
            }
        }

        [Test]
        public void GrammarCompilerTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();

                using (var scriptStream = new MemoryStream(Encoding.UTF8.GetBytes(GrammarText)))
                {
                    var problems = pSystem.Compile(scriptStream);
                    Assert.AreEqual(1, pSystem.Domains.Count());
                    Assert.AreEqual(1, problems.Count);
                    foreach (var domain in pSystem.Domains)
                        test.Log(domain.ToString());
                    foreach (var problem in problems)
                        test.Log(problem.ToString());
                }
                
                return;

                foreach (var valuePair in GetPddlProblems())
                {
                    try
                    {
                        using (var scriptStream = File.OpenRead(valuePair.Key))
                            test.Log("Domain:\n{0}", pSystem.Compile(scriptStream).ToString());
                        using (var scriptStream = File.OpenRead(valuePair.Value))
                            test.Log("Problem:\n{0}", pSystem.Compile(scriptStream).ToString());
                    }
                    catch (Exception e)
                    {
                        test.Log($"{valuePair.Key}: {e}");
                        throw;
                    }
                }
            }
        }
        [Test]
        public void ForAllCompilerTest()
        {
            using (var test = CreateTest("GrammarAstConstructorTest"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();

                using (var scriptStream = new MemoryStream(Encoding.UTF8.GetBytes(ForAllGrammarText)))
                {
                    var problems = pSystem.Compile(scriptStream);
                    Assert.AreEqual(1, pSystem.Domains.Count());
                    Assert.AreEqual(1, problems.Count);
                    foreach (var domain in pSystem.Domains)
                        test.Log(domain.ToString());
                    foreach (var problem in problems)
                        test.Log(problem.ToString());
                }
            }
        }

        [Test]
        public void StripsPlanner()
        {
            using (var test = CreateTest("TestPlanner"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();
                var compiler = pSystem.Compiler;
                
                var domainBuilder = Factory.CreateDomain("cargo");

                compiler.Compile(domainBuilder,
                    "(in ?c ?p)",
                    "(out ?c)",
                    "(at-airport ?x ?a)",
                    "(plane ?p)",
                    "(airport ?a)",
                    "(cargo ?c)",
                    "(count ?o - type) - number");
                compiler.Compile(domainBuilder,
                    "(:action load :parameters (?c ?p ?a) :precondition (and (at-airport ?c ?a) (at-airport ?p ?a) (out ?c) (plane ?p) (cargo ?c) (airport ?a) (<= (count ?c) (count ?p))) :effect (and (not(at-airport ?c ?a)) (in ?c ?p) (not (out ?c))))");
                compiler.Compile(domainBuilder,
                    "(:action unload :parameters (?c ?p ?a) :precondition (and (in ?c ?p) (at-airport ?p ?a) (plane ?p) (cargo ?c) (airport ?a)) :effect (and (not (in ?c ?p)) (at-airport ?c ?a) (out ?c)))");
                compiler.Compile(domainBuilder,
                    "(:action fly :parameters (?p ?from ?to) :precondition (and (at-airport ?p ?from) (plane ?p) (airport ?from) (airport ?to)) :effect (and (not (at-airport ?p ?from)) (at-airport ?p ?to)))");

                var domain = domainBuilder.Build();
                test.Log(domain.ToString());
                test.Log(domain.Planner.ToString());

                var problem = domain.CreateProblem("cargo1");
                compiler.Compile(problem,
                    "c1 c2 jfk sfo p1 p2",
                    "(airport jfk)",
                    "(airport sfo)",
                    "(plane p1)",
                    "(plane p2)",
                    "(cargo c1)",
                    "(cargo c2)",
                    "(at-airport p1 jfk)",
                    "(at-airport p2 sfo)",
                    "(at-airport c1 sfo)",
                    "(at-airport c2 jfk)",
                    "(in c2 p1)",
                    "(out c1)",
                    "(= (count c1) 3)",
                    "(= (count p2) 10)");
                compiler.Compile(problem, "(:goal (and (at-airport c1 jfk) (out c1) (at-airport c2 sfo) (out c2)))");
                
                test.Log(problem.ToString());

                var result = domain.Planner.Resolve(problem);
                test.Log(result.ToStringDesc());

                var temp = result.Steps.Select(u => u.ToString()).ToList();
                Assert.AreEqual(5, result.Steps.Length);
                Assert.IsTrue(temp.Contains("(fly p1 jfk sfo)"));
                Assert.IsTrue(temp.Contains("(unload c2 p1 sfo)"));
                Assert.IsTrue(temp.Contains("(load c1 p2 sfo)"));
                Assert.IsTrue(temp.Contains("(fly p2 sfo jfk)"));
                Assert.IsTrue(temp.Contains("(unload c1 p2 jfk)"));
                Assert.AreEqual(5f, result.Cost);
            }
        }

        [Test]
        public void CompilerFunctionTest()
        {
            using (var test = CreateTest("CompilerFunctionTest"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();
                var compiler = pSystem.Compiler;
                
                var domainBuilder = Factory.CreateDomain("TEST");
                
                compiler.Compile(domainBuilder, "( func0 ) - number");
                compiler.Compile(domainBuilder, "( func1 ?A ) - number");
                compiler.Compile(domainBuilder, 
                    "( func2 ?A ?B - type ) - number",
                    "( func3 ?A - type ?B ?C - type ) - number");

                var domain = domainBuilder.Build();

                test.Log(domain.ToString());
                
                foreach (var function in domain.Functions)
                {
                    switch (function.Name)
                    {
                        case "func0":
                            Assert.AreEqual(0, function.ArgsCount);
                            break;
                        case "func1":
                            Assert.AreEqual(1, function.ArgsCount);
                            break;
                        case "func2":
                            Assert.AreEqual(2, function.ArgsCount);
                            break;
                        case "func3":
                            Assert.AreEqual(3, function.ArgsCount);
                            break;
                        default:
                            throw new InvalidDataException($"Unknown function {function.Name}");
                    }
                }
            }
        }
        
        [Test]
        public void CompilerPredicateTest()
        {
            using (var test = CreateTest("CompilerPredicateTest"))
            {
                var pSystem = test.Container.RegisterLibrary(Libraries.Ai()).Build().Resolve<IPlanningSystem>();
                var compiler = pSystem.Compiler;
                
                var domainBuilder = Factory.CreateDomain("TEST");
                
                compiler.Compile(domainBuilder, "( predicate0 )");
                compiler.Compile(domainBuilder, 
                    "( predicate1 ?A )",
                    "( predicate2 ?A ?B )",
                    "( predicate3 ?A - type ?B ?C )");

                var domain = domainBuilder.Build();

                test.Log(domain.ToString());
                
                foreach (var function in domain.Predicates)
                {
                    switch (function.Name)
                    {
                        case "predicate0":
                            Assert.AreEqual(0, function.ArgsCount);
                            break;
                        case "predicate1":
                            Assert.AreEqual(1, function.ArgsCount);
                            break;
                        case "predicate2":
                            Assert.AreEqual(2, function.ArgsCount);
                            break;
                        case "predicate3":
                            Assert.AreEqual(3, function.ArgsCount);
                            break;
                        default:
                            throw new InvalidDataException($"Unknown predicate {function.Name}");
                    }
                }
            }
        }
    }
}