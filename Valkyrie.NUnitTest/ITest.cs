﻿using System;
using Valkyrie.Di;

namespace Valkyrie.NUnitTest
{
    public interface ITest : IDisposable
    {
        IContainer Container { get; }
        string ClientUid { get; }
        
        void Log(string msg);
        void Log(string format, params object[] args);

        void Update(float time);
        void Run(Func<bool> checker);
    }
}