using NUnit.Framework;
using Valkyrie.Data;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class UtilsUnitTest : GenericUnitTests
    {
        [TestCase("Asd", "Asd")]
        [TestCase("asd", "Asd")]
        [TestCase("_asd", "Asd")]
        [TestCase("__asd", "Asd")]
        [TestCase("_first_second", "FirstSecond")]
        [TestCase("first_second", "FirstSecond")]
        [TestCase("_first_Second", "FirstSecond")]
        [TestCase("_First_second", "FirstSecond")]
        [TestCase("_First_Second", "FirstSecond")]
        [TestCase("__First__second", "FirstSecond")]
        public void ToPropertyNameConvert(string source, string result)
        {
            Assert.AreEqual(result, source.ConvertToCamelCasePropertyName());
        }
        [TestCase("Asd", "_asd")]
        [TestCase("asd", "_asd")]
        [TestCase("_asd", "_asd")]
        [TestCase("__asd", "_asd")]
        [TestCase("_first_second", "_firstSecond")]
        [TestCase("first_second", "_firstSecond")]
        [TestCase("_first_Second", "_firstSecond")]
        [TestCase("_First_second", "_firstSecond")]
        [TestCase("_First_Second", "_firstSecond")]
        [TestCase("__First__second", "_firstSecond")]
        public void ToFieldNameConvert(string source, string result)
        {
            Assert.AreEqual(result, source.ConvertToCamelCaseFieldName());
        }
    }
}