using NUnit.Framework;
using Valkyrie.NUnitTest.Utility;

namespace Valkyrie.NUnitTest
{
    public class GenericUnitTests
    {
        #region Utils

        /// <summary>
        /// Gets or sets the test context which provides
        /// information about and functionality for the current test run.
        /// </summary>
        // ReSharper disable UnusedAutoPropertyAccessor.Global
        // ReSharper disable MemberCanBePrivate.Global
        public TestContext TestContext { get; set; }
        // ReSharper restore MemberCanBePrivate.Global
        // ReSharper restore UnusedAutoPropertyAccessor.Global

        protected ITest CreateTest(string name)
        {
            return new TestInstance(name, TestContext);
        }

        #endregion
    }
}