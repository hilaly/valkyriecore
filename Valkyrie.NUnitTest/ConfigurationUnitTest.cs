﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NUnit.Framework;
using Valkyrie.Data.Config;
using Valkyrie.NUnitTest.Utility;
using Valkyrie.Tools;
using Random = System.Random;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class ConfigurationUnitTest : GenericUnitTests
    {
        private readonly string xmlSource = @"<root>
                                        <data Id=""1"" RefClass=""TestConfigData"" field0=""10"" field1=""10.3"" />
                                        <data Id=""3"" RefClass=""TestConfigData"" field0=""20.5"" Base=""1"" />
                                    </root>";

        [Test]
        public void TestStructureSerialization()
        {
            using (var test = CreateTest("TestStructureSerialization"))
            {
                test.Container.RegisterLibrary(Libraries.Data).Build();

                var serializer = test.Container.Resolve<IConfiguration>().StartEdit();
                var editor = serializer;

                var r = new Random();
                Func<TestConfigDataWithStruct.Test> creator = () => new TestConfigDataWithStruct.Test
                {
                    F = (float) r.NextDouble(),
                    I = r.Next(),
                    U = r.Next().ToString()
                };

                const string testId = "ASD";
                
                var source = new TestConfigDataWithStruct
                {
                    Id = testId,
                    Instance = creator(),
                    List = new List<TestConfigDataWithStruct.Test>() { creator(), creator(), creator() }
                };
                editor.Add(source);
                var memoryStream = new MemoryStream();
                serializer.Save(memoryStream, ConfigType.Xml);
                memoryStream.Seek(0, SeekOrigin.Begin);
                test.Log(new StreamReader(memoryStream).ReadToEnd());
                memoryStream.Seek(0, SeekOrigin.Begin);
                
                editor.Remove(source);
                serializer.Load(memoryStream, ConfigType.Xml);
                var target = editor.Get<TestConfigDataWithStruct>(testId);
                
                Assert.NotNull(target);
                Assert.AreEqual(source.Id, target.Id);
                Assert.AreEqual(source.List.Count, target.List.Count);

                bool compare(TestConfigDataWithStruct.Test a, TestConfigDataWithStruct.Test b)
                {
                    Assert.AreEqual(a.U, b.U);
                    Assert.AreEqual(a.F, b.F);
                    Assert.AreEqual(a.I, b.I);
                    
                    return true;
                }
                
                Assert.True(compare(source.Instance, target.Instance));
                for (var i = 0; i < source.List.Count; ++i)
                    Assert.True(compare(source.List[i], target.List[i]));
            }
        }

        [Test]
        public void TestLoading()
        {
            using (var test = CreateTest("Loading configuration"))
            {
                test.Container.RegisterLibrary(Libraries.Data).Build();
                
                var cfg = test.Container.Resolve<IConfiguration>();
                var edit = cfg.StartEdit();
                edit.Load(new MemoryStream(Encoding.Default.GetBytes(xmlSource)), ConfigType.Xml);
                edit.Fetch();
                
                Assert.AreEqual(10, cfg.Get<TestConfigData>("1").field0);
                Assert.AreEqual(10.3f, cfg.Get<TestConfigData>("1").field1);

                Assert.AreEqual(20.5f, cfg.Get<TestConfigData>("3").field0);
            }
        }

        [Test]
        public void TestOverridingLoading()
        {
            using (var test = CreateTest("Overriding configuration"))
            {
                test.Container.RegisterLibrary(Libraries.Data).Build();

                var cfg = test.Container.Resolve<IConfiguration>();
                var edit = cfg.StartEdit();
                edit.Load(new MemoryStream(Encoding.Default.GetBytes(xmlSource)), ConfigType.Xml);
                edit.Fetch();

                Assert.AreEqual(10, cfg.Get<TestConfigData>("1").field0);
                Assert.AreEqual(10.3f, cfg.Get<TestConfigData>("1").field1);

                Assert.AreEqual(20.5f, cfg.Get<TestConfigData>("3").field0);
                Assert.AreEqual(10.3f, cfg.Get<TestConfigData>("3").field1);
            }
        }

        [Test]
        public void TestEnvironmentConfig()
        {
            using (var test = CreateTest("Environment config from stream"))
            {
                test.Container.RegisterLibrary(Libraries.Data).Build();
                var cfg = test.Container.Resolve<IEnvironmentConfig>();

                const string id = "TEST";
                
                var intVal = 3;
                cfg.Set(id, intVal);
                Assert.AreEqual(intVal, cfg.Get<int>(id));

                var floatVal = 3f;
                cfg.Set(id, floatVal);
                Assert.AreEqual(floatVal, cfg.Get<float>(id));

                var strVal = "ASD";
                cfg.Set(id, strVal);
                Assert.AreEqual(strVal, cfg.Get<string>(id));
            }
        }
    }
}