using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;
using Valkyrie.Data;
using Valkyrie.Math;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class DataExtensionsUnitTest
    {
        [TestCase(100000, 3)]
        [TestCase(10000000, 2)]
        public void TestRandomRangeInt(int count, int range)
        {
            Dictionary<int, int> variants = new Dictionary<int, int>();
            for (int i = 0; i < range; i++)
                variants[i] = 0;
            
            for (var i = 0; i < count; ++i)
            {
                var value = Random.SRange(0, range);
                Debug.Assert(value >= 0);
                Debug.Assert(value < range);
                variants[value] += 1;
            }

            Debug.Print($"{string.Join(", ", variants.Select(u => $"{u.Key}={u.Value}"))}");
            Debug.Flush();
        }
        [TestCase(100000, 3)]
        public void TestRandomListInt(int count, int range)
        {
            var collection = new List<int>();
            for (var i = 0; i < range; ++i)
            {
                collection.Add(i);
            }
            for (var i = 0; i < count; ++i)
            {
                var value = collection.Random();
                Debug.Assert(value >= 0);
                Debug.Assert(value < range);
            }
        }
    }
}