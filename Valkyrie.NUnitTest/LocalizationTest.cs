using System.IO;
using NUnit.Framework;
using Valkyrie.Data.Localization;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class LocalizationTest : GenericUnitTests
    {
        [Test]
        public void TestLoading()
        {
            using (var test = CreateTest("LocalizationLoading"))
            {
                test.Container.RegisterLibrary(Libraries.Data).Build();

                var localizationSerializer = test.Container.Resolve<ILocalizationSerializer>();
                using (var stream = File.OpenRead("en.json"))
                    localizationSerializer.Load(stream);
                using (var stream = File.OpenRead("ru.json"))
                    localizationSerializer.Load(stream);
            }
        }

        [Test]
        public void TestLanguageChanging()
        {
            using (var test = CreateTest("LanguageChanging"))
            {
                test.Container.RegisterLibrary(Libraries.Data).Build();
                
                var localizationSerializer = test.Container.Resolve<ILocalizationSerializer>();
                using (var stream = File.OpenRead("en.json"))
                    localizationSerializer.Load(stream);
                using (var stream = File.OpenRead("ru.json"))
                    localizationSerializer.Load(stream);
                
                var localization = test.Container.Resolve<ILocalization>();

                string result;
                
                localization.Language = "en";
                result = localization.GetString("a");
                test.Log("Checking for string a, locale:{0}, value:{1}", localization.Language, result);
                Assert.AreEqual(result, "a");

                localization.Language = "ru";
                result = localization.GetString("a");
                test.Log("Checking for string a, locale:{0}, value:{1}", localization.Language, result);
                Assert.AreEqual(result, "а");

                localization.Language = "en";
                result = localization.GetString("a");
                test.Log("Checking for string a, locale:{0}, value:{1}", localization.Language, result);
                Assert.AreEqual(result, "a");
            }
        }
    }
}