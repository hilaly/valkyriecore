﻿using System;
using NUnit.Framework;
using Valkyrie.Runtime.Console;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class DeveloperConsoleTest : GenericUnitTests
    {
        [Test]
        public void ExecuteUnknownCommand()
        {
            using (var test = CreateTest("Execute unknown command"))
            {
                var console = test.Container.Resolve<IDeveloperConsole>();
                
                console.Execute("draw");
                test.Container.Resolve<IExecutor>().Iterate();
                console.Execute("draw 1");
                test.Container.Resolve<IExecutor>().Iterate();
                console.Execute("draw 1 2");
                test.Container.Resolve<IExecutor>().Iterate();
                console.Execute("draw 1 2 3");
                test.Container.Resolve<IExecutor>().Iterate();
                console.Execute("draw 1 2 3 4");
                test.Container.Resolve<IExecutor>().Iterate();
            }
        }
        [Test]
        public void ExecuteListCommand()
        {
            using (var test = CreateTest("Execute HELP command"))
            {
                var console = test.Container.Resolve<IDeveloperConsole>();

                console.Execute("help");
                console.Execute("help help");
                console.Execute("help list 1");
            }
        }
        [Test]
        public void RegisterExternalStaticCommand()
        {
            using (var test = CreateTest("Execute HELP command"))
            {
                var console = test.Container.Resolve<IDeveloperConsole>();
                console.RegisterCommand(GetType());

                console.Execute("help");
                console.Execute("TEST_STATIC");
            }
        }

        [DeveloperConsoleCommand(Alias = "TEST_STATIC", Description = "empty static function", Usage = "TEST_STATIC")]
        static void StaticConsoleCommand()
        {
            throw new ApplicationException();
        }
    }
}