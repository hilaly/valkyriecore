using System;
using System.IO;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using Valkyrie.Di;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.NUnitTest.Utility
{
    class TestInstance : ITest
    {
        private const string SecretKey = "ZU-ZHU";
        
        #region Fields

        private readonly string _name;
        private readonly TestContext _testContext;
        private readonly string _tempDirectory;

        #endregion

        #region Properties

        public IContainer Container { get; }
        public string ClientUid => SecretKey;

        #endregion

        #region Logs

        void LogImpl(string msg)
        {
            //Console.WriteLine("Test {0}: {1}", _name, msg);
            TestContext.Out.WriteLine("Test {0}: {1}", _name, msg);
        }

        public void Log(string msg)
        {
            LogImpl(msg);
        }

        public void Log(string format, params object[] args)
        {
            LogImpl(string.Format(format, args));
        }

        public void Update(float time)
        {
            var executor = Container.Resolve<IExecutor>();
            
            var t = DateTime.UtcNow;
            while ((DateTime.UtcNow - t).TotalSeconds < time)
                executor.Iterate();
        }
        
        public void Run(Func<bool> checker)
        {
            while (checker())
                Container.Resolve<IExecutor>().Iterate();
        }


        #endregion

        #region Init

        IContainer GetPioContainer()
        {
            return Configurator.Start(SecretKey)
                .Build();
        }

        public TestInstance(string name, TestContext testContext)
        {
            _tempDirectory = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            _name = name;
            _testContext = testContext;

            Container = GetPioContainer();
            //Container.Resolve<IEcsContext>().Engines.UseFixedTimestamp = 0.2f;

            Log("started");
            Log("Working directory {0}", _tempDirectory);
            Log("Main thread {0}[{1}]", Thread.CurrentThread.Name, Thread.CurrentThread.ManagedThreadId);

            Promise.UnhandledException += (sender, args) =>
            {
                TestContext.Out.WriteLine("Test {0}, exception: {1}", _name, args.Exception);
            };
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Directory.SetCurrentDirectory(_tempDirectory);
            Log("finished");
        }

        #endregion
    }
}