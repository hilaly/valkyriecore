﻿using System.Collections.Generic;
using Valkyrie.Data.Config;

namespace Valkyrie.NUnitTest.Utility
{
    public class TestConfigData : BaseConfigData
    {
        public float field0;
        public float field1;
    }

    public class TestConfigDataWithStruct : BaseConfigData
    {
        public struct Test
        {
            public int I;
            public float F;
            public string U;
        }

        public Test Instance;
        public List<Test> List;
    }
}