﻿using System.Collections.Generic;
using NUnit.Framework;
using Valkyrie.Threading.Async;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class ReactiveCollectionsUnitTest : GenericUnitTests
    {
        [Test]
        public void ReactiveCollection()
        {
            var test = new ReactiveCollection<int>();

            var iterationsCount = 10;
            var addedValues = new List<int>();
            var removedValues = new List<int>();

            using (new CompositeDisposable(
                test.ObserveAdd().Subscribe(u => addedValues.Add(u.Value)),
                test.ObserveRemove().Subscribe(u => removedValues.Add(u.Value))
                ))
            {
                for(var i = 0; i < iterationsCount; ++i)
                    test.Add(i);
                for(var i = 0; i < iterationsCount; ++i)
                    test.Remove(i);
            }
            
            Assert.AreEqual(iterationsCount, addedValues.Count);
            Assert.AreEqual(iterationsCount, removedValues.Count);

            for (var i = 0; i < iterationsCount; ++i)
            {
                Assert.IsTrue(addedValues.Contains(i));
                Assert.IsTrue(removedValues.Contains(i));
            }
        }

        [Test]
        public void ReactiveDictionary()
        {
            var test = new ReactiveDictionary<int, string>();

            var iterationsCount = 10;
            var addedValues = new List<KeyValuePair<int, string>>();
            var removedValues = new List<KeyValuePair<int, string>>();

            using (new CompositeDisposable(
                test.ObserveAdd().Subscribe(value => addedValues.Add(new KeyValuePair<int, string>(value.Key, value.Value))),
                test.ObserveRemove().Subscribe(value => removedValues.Add(new KeyValuePair<int, string>(value.Key, value.Value)))
            ))
            {
                for(var i = 0; i < iterationsCount; ++i)
                    test.Add(i, i.ToString());
                for(var i = 0; i < iterationsCount; ++i)
                    test.Remove(i);
            }
            
            Assert.AreEqual(iterationsCount, addedValues.Count);
            Assert.AreEqual(iterationsCount, removedValues.Count);

            for (var i = 0; i < iterationsCount; ++i)
            {
                var template = new KeyValuePair<int, string>(i, i.ToString());
                Assert.IsTrue(addedValues.Contains(template));
                Assert.IsTrue(removedValues.Contains(template));
            }
        }
    }
}