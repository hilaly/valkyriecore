using System;
using System.Collections.Generic;
using NUnit.Framework;
using Valkyrie.Math.Algorithms;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class GeneratorsUnitTest : GenericUnitTests
    {
        private static void Test<T>(IGenerator<T> generator, Func<int, T> valueCall)
        {
            var generator1 = generator.CloneAndRestart();

            for (var i = 0; i < 100; ++i)
            {
                Assert.AreEqual(generator.Current, generator.Current);
                Assert.AreEqual(valueCall(i), generator.Current);
                generator.MoveNext();
            }

            for (var i = 0; i < 100; ++i)
            {
                Assert.AreEqual(generator1.Current, generator1.Current);
                Assert.AreEqual(valueCall(i), generator1.Current);
                generator1.MoveNext();
            }

            for (var i = 0; i < 100; ++i)
            {
                Assert.AreEqual(generator.Current, generator1.Current);
                generator.MoveNext();
                generator1.MoveNext();
            }

            generator.Reset();
            generator1.Reset();
            
            for (var i = 0; i < 100; ++i)
            {
                Assert.AreEqual(generator.Current, generator1.Current);
                generator.MoveNext();
                generator1.MoveNext();
            }
        }
        
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        public void ConstantGenerator(int element)
        {
            Test(Generator.Constant(element), (i) => element);
        }

        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        public void CountGenerator(int element)
        {
            Test(Generator.Count(element), i => i % element);
        }

        [TestCase(0, 1, 2, 3)]
        [TestCase(13, 12, 8)]
        [TestCase("ASD", "GRB", "BBB")]
        public void RepeatGenerator(params object[] args)
        {
            var source = new List<object>(args);
            Test(Generator.Repeat(args), i => source[i % source.Count]);
        }

        [TestCase(3)]
        [TestCase(5)]
        [TestCase(7)]
        public void SelectGenerator(int element)
        {
            Test(Generator.Constant(element).Select(e => e + 1), i => element + 1);
        }

        [Test]
        public void WhereGenerator()
        {
            Test(Generator.Count(4).Where(u => u % 2 == 0), i => (i % 2 == 0) ? 0 : 2);
        }

        [TestCase(3, 15)]
        [TestCase(5, 2)]
        [TestCase(7, 9)]
        public void RepeatEachGenerator(int count, int sequence)
        {
            Test(Generator.Count(sequence).RepeatEach(count), i => (i / count) % sequence);
        }

        [TestCase(2, "A", "B", "C", "D")]
        [TestCase(3, "A", "B", "C", "D")]
        [TestCase(4, "A", "B", "C", "D")]
        public void ChooseGenerator(int count, params string[] args)
        {
            var list = new List<string>(args);
            Test(Generator.Count(count).Choose(args), i => list[i % count]);
        }
    }
}