﻿using System.Numerics;
using System.Text;
using NUnit.Framework;
using Valkyrie.Data;
using Valkyrie.Math;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class MiscUnitTest : GenericUnitTests
    {
        class TestForCopy
        {
            private int a;
            private int b;
            public int c;
            public int d { get; set; }
            public int e { get; private set; }

            public override bool Equals(object obj)
            {
                return obj is TestForCopy && Equals((TestForCopy) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = a;
                    hashCode = (hashCode * 397) ^ b;
                    hashCode = (hashCode * 397) ^ c;
                    hashCode = (hashCode * 397) ^ d;
                    hashCode = (hashCode * 397) ^ e;
                    return hashCode;
                }
            }

            public bool Equals(TestForCopy o)
            {
                return a.Equals(o.a) &&
                       o.b.Equals(b) &&
                       o.c.Equals(c)
                       && o.d.Equals(d)
                       && o.e.Equals(e);
            }

            public static TestForCopy Init()
            {
                return new TestForCopy
                {
                    a = 1,
                    b = 2,
                    c = 3,
                    d = 4,
                    e = 5
                };
            }
        }

        [Test]
        public void ObjectCopyTest()
        {
            using (var test = CreateTest("Math"))
            {
                var source = TestForCopy.Init();
                var result = source.MakeCopy();
                Assert.AreEqual(source, result, "Copy dont works");
            }
        }

        [Test]
        public void JsonSerializationTest()
        {
            using (var test = CreateTest("Math"))
            {
                test.Log(Vector2.Zero.ToJson());
                test.Log(Vector3.Zero.ToJson());
                test.Log(Quaternion.Identity.ToJson());

                test.Log(new IntVec2().ToJson());

                test.Log(new BoundingBox2().ToJson());
                test.Log(new BoundingBox3().ToJson());

                Assert.AreEqual(Vector2.UnitY, Vector2.UnitY.ToJson().ToObject<Vector2>());
                Assert.AreEqual(Vector3.UnitY, Vector3.UnitY.ToJson().ToObject<Vector3>());
                Assert.AreEqual(Quaternion.Identity, Quaternion.Identity.ToJson().ToObject<Quaternion>());
            }
        }

        [Test]
        public void StringHashTest()
        {
            for (var i = 'A'; i <= 'Z'; i++)
            {
                var sb = new StringBuilder();
                for (var j = 'A'; j <= i; ++j)
                    sb.Append(j);
                var value = sb.ToString();
                var hash = value.ComputeHash();
                if (value.Length < 20)
                    Assert.AreEqual(value, hash);
                else
                    Assert.AreEqual(40, hash.Length);
            }
        }
    }
}