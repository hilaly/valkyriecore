using System;
using System.Text;
using NUnit.Framework;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Infrastructure;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;

namespace Valkyrie.NUnitTest
{
    [TestFixture]
    public class FileUpdatingUnitTest : GenericUnitTests
    {
        [TestCase(18602)]
        public void ComplexTest(int port)
        {
            using (var test = CreateTest("ComplexTest"))
            {
                test.Container
                    .RegisterLibrary(Libraries.Data)
                    .RegisterLibrary(Libraries.Network)
                    .Build();

                Exception error = new TimeoutException();
                var connectionPort = new ServerPort
                    {Address = "127.0.0.1", Port = port, AppId = "TestFileServer", AuthKey = "3"};

                var fileServer = test.Container.Resolve<IFileServer>();
                var fileStorage = test.Container.Resolve<IFileStorage>();

                Promise.Resolved()
                    .Then(() => TestFileServer(fileServer, fileStorage, connectionPort,
                        test.Container.Resolve<INetwork>()))
                    .Then(fileServer.Clear)
                    .Done(() => error = null, e => error = e);
                test.Update(5f);
                if (error != null)
                    throw error;
            }
        }

        private IPromise TestFileServer(IFileServer fileServer, IFileStorage fileStorage, ServerPort port,
            INetwork network)
        {
            string fileId = "testFileId";

            byte[] GetContent(string data)
            {
                return Encoding.UTF8.GetBytes($"CONTENT_VERSION_{data}");
            }

            string GetHash(string data)
            {
                return GetContent(data).ComputeHash();
            }

            IPromise CheckContent(string version, string data)
            {
                return Promise.Resolved()
                    .Then(() => fileStorage.Upload(fileId, version, GetContent(data)))
                    .Then(() => fileStorage.Download(fileId, version))
                    .Then(content => Assert.AreEqual(GetHash(data), content.ComputeHash()));
            }

            return Promise.Resolved()
                .Then(() => fileServer.Init("ServerFileCache", port))
                .Then(() => fileServer.Clear())
                .Then(() => network.Connect(port))
                .Then(channel => fileStorage.Init(channel, "FileCache"))
                .Then(() => CheckContent("0.0", "0.0"))
                .Then(() => CheckContent("0.0", "0.1"))
                .Then(() => CheckContent("0.1", "0.1"))
                .Then(() => CheckContent("0.1", "0.1"))
                .Then(() => CheckContent("0.0", "0.0"))
                .Then(() => CheckContent("0.2", "0.0"))
                .Then(() => CheckContent("0.2", "0.1"));
        }
    }
}