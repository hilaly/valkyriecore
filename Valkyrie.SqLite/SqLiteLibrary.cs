using Valkyrie.Di;
using Valkyrie.SqLite.Database;

namespace Valkyrie.SqLite
{
    public class SqLiteLibrary : ILibrary
    {
        public void Register(IContainer container)
        {
            container.Register<SqlitePersistentStorage>().AsInterfacesAndSelf().SingleInstance();
            container.Register<SqLiteDb>().AsInterfacesAndSelf().SingleInstance();
        }
    }
}