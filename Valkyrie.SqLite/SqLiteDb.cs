﻿using System.Linq;
using Valkyrie.Data;
using Valkyrie.SqLite.Database;
using Valkyrie.Tools;
using Valkyrie.Tools.Logs;

namespace Valkyrie.SqLite
{
    class SqLiteDb : IDbAccess
    {
        private readonly ILogInstance _log;
        private readonly SqliteDbConnection _connection;

        public SqLiteDb(IEnvironmentConfig config, ILogService logService)
        {
            _log = logService.GetLog("Valkyrie.Database");

            if (!config.HasKey("database_address"))
                _log.Error($"Environment config has not 'database_address'");
            else
            {
                _connection = new SqliteDbConnection(DatabaseProvider.GetDb(config.Get("database_address", "temp")));
                _connection.CreateTable<DbDataRecord>();
            }
        }

        string GetUid<T>(string id)
        {
            return typeof(T).FullName + id;
        }

        public T Get<T>(string id)
        {
            var uid = GetUid<T>(id);
            var record = _connection.Table<DbDataRecord>().FirstOrDefault(u => u.Key == uid);
            if (record != null && record.Data.NotNullOrEmpty())
                return record.Data.ToObject<T>();
            return default(T);
        }

        public void Set<T>(string id, T value)
        {
            var uid = GetUid<T>(id);
            _connection.InsertOrReplace(new DbDataRecord() {Key = uid, Data = value.ToJson()});
        }

        public void Delete<T>(string id)
        {
            var uid = GetUid<T>(id);
            _connection.Delete<T>(uid);
        }
    }
}