using System.Collections.Generic;

namespace Valkyrie.SqLite.Database
{
    public interface IDatabaseConnection
    {
        int Update(object o);
        void CreateTable<T>();
        IEnumerable<T> Table<T>() where T : new();
        T Get<T>(string key) where T : new();
        void InsertOrReplace(object o);
    }
}