using System.IO;
using SQLite;

namespace Valkyrie.SqLite.Database
{
    internal class DatabaseProvider
    {
        private static string GetDatabasePath(string name)
        {
            var directory = Path.Combine(Directory.GetCurrentDirectory(), "Databases");
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            return Path.Combine(directory, $"{name}.db");
        }

        public static SQLiteConnection GetDb(string name)
        {
            return new SQLiteConnection(GetDatabasePath(name),
                SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create | SQLiteOpenFlags.SharedCache);
        }
    }
}