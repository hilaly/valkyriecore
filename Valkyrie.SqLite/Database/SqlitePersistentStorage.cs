using System;
using System.Collections.Generic;
using System.Linq;
using SQLite;
using Valkyrie.Data;

namespace Valkyrie.SqLite.Database
{
    internal class SqlitePersistentStorage : IPersistentStorage, IDisposable
    {
        [Table("PersistentStorage")]
        private class PersistentData
        {
            [PrimaryKey]
            public string Id { get; set; }
            public string JsonValue { get; set; }
        }

        private readonly IDatabaseConnection _db;
        private readonly Dictionary<string, object> _loadedData = new Dictionary<string, object>();

        public SqlitePersistentStorage(IDatabaseConnection db)
        {
            _db = db;
            _db.CreateTable<PersistentData>();
        }

        public void Dispose()
        {
            foreach (var o in _loadedData)
                _db.Update(new PersistentData {Id = o.Key, JsonValue = o.Value.ToJson()});
            _loadedData.Clear();
        }
        
        #region IPersistentStorage

        public T Get<T>(string key)
        {
            return (T) Get(key, typeof(T));
        }

        public object Get(string key, Type type)
        {
            if (!_loadedData.ContainsKey(key))
            {
                var o = _db.Table<PersistentData>().FirstOrDefault(u => u.Id == key);
                if (o != null)
                    _loadedData.Add(key, o.JsonValue.ToObject(type));
                else
                {
                    var r = Activator.CreateInstance(type);
                    _loadedData.Add(key, r);
                    _db.InsertOrReplace(new PersistentData {Id = key, JsonValue = r.ToJson()});
                }
            }

            return _loadedData[key];
        }

        public void Set(string key, object value)
        {
            _loadedData[key] = value;
            _db.InsertOrReplace(new PersistentData {Id = key, JsonValue = value.ToJson()});
        }

        public bool Has(string key)
        {
            return _loadedData.ContainsKey(key) || _db.Table<PersistentData>().FirstOrDefault(u => u.Id == key) != null;
        }
        
        #endregion
    }
}