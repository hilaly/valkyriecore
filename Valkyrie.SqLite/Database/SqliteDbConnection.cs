using System;
using System.Collections.Generic;
using SQLite;

namespace Valkyrie.SqLite.Database
{
    internal class SqliteDbConnection : IDatabaseConnection, IDisposable
    {
        private readonly SQLiteConnection _connection;

        public SqliteDbConnection(SQLiteConnection connection)
        {
            _connection = connection;
        }

        public void Dispose()
        {
            _connection.Dispose();
        }

        public int Update(object obj)
        {
            return _connection.Update(obj);
        }

        public void CreateTable<T>()
        {
            _connection.CreateTable<T>();
        }

        public IEnumerable<T> Table<T>() where T : new()
        {
            return _connection.Table<T>();
        }

        public T Get<T>(string key) where T : new()
        {
            return _connection.Get<T>(key);
        }

        public void InsertOrReplace(object o)
        {
            _connection.InsertOrReplace(o);
        }

        public void Delete<T>(string key)
        {
            _connection.Delete<T>(key);
        }
    }
}