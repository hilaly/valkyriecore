using SQLite;

namespace Valkyrie.SqLite.Database
{
    [Table("Records")]
    class DbDataRecord
    {
        [PrimaryKey]
        public string Key { get; set; }
        public string Data { get; set; }
    }
}