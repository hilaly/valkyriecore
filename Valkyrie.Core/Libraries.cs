﻿using System;
using System.Threading;
using Valkyrie.Ai.Behaviour;
using Valkyrie.Ai.Planning.Internal;
using Valkyrie.Communication.Network;
using Valkyrie.Communication.Network.LidgrenImpl;
using Valkyrie.Data;
using Valkyrie.Data.Config;
using Valkyrie.Data.Localization;
using Valkyrie.Data.Serialization;
using Valkyrie.Di;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Engines;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Groups;
using Valkyrie.Infrastructure;
using Valkyrie.Math;
using Valkyrie.Math.Space;
using Valkyrie.Math.Space.Management;
using Valkyrie.Math.Space.Sync;
using Valkyrie.Network.LiteNetLibImpl;
using Valkyrie.Network.MiniUdpImpl;
using Valkyrie.Runtime.Console;
using Valkyrie.Scripts;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Threading.Sync;
using Valkyrie.Tools;
using Valkyrie.Tools.Logs;
using Valkyrie.Tools.Time;

namespace Valkyrie
{
    /// <summary>
    /// Misc -> разное
    /// Utils -> полезное, код упрощающий работу с другим кодом
    /// Tools -> инструмент, код, который реализует некий функционал
    /// </summary>
    public static class Libraries
    {
        class ActionLibrary : ILibrary
        {
            private readonly Action<IContainer> _call;

            public ActionLibrary(Action<IContainer> call)
            {
                _call = call;
            }

            public void Register(IContainer container)
            {
                _call(container);
            }
        }

        public static ILibrary Network => new ActionLibrary(builder =>
        {
            //Dispatcher, Log, ContractSerializer, AppInfo
            
            //builder.Register<MiniUdpService>().AsInterfacesAndSelf().SingleInstance();
            //builder.Register<LiteNetService>().AsInterfacesAndSelf().SingleInstance();
            builder.Register<LidgrenNetwork>().AsInterfacesAndSelf().SingleInstance();

            builder.Register<Channel>().AsInterfacesAndSelf().InstancePerDependency();
        });

        internal static ILibrary ToolsLibrary(string secretKey)
        {
            var appInfo = new MetaInfo(secretKey)
            {
                LockPolicy = LockRecursionPolicy.NoRecursion,
                BackgroundThreadsCount = 7
            };

            return new ActionLibrary(builder =>
            {
                builder.Register(appInfo).AsSelf();

                builder.Register<ReadWriteLockSlim>().As<IReaderWriterLock>().InstancePerDependency(); //AppInfo
                builder.Register<CompositeDisposable>().As<ICompositeDisposable>().AsSelf()
                    .InstancePerDependency(); //None

                builder.Register<StringInterning>().As<IStringInterning>().AsSelf().SingleInstance(); //None
                builder.Register<DummyProfiler>().As<IProfiler>().SingleInstance(); //None
                builder.Register<ContractSerializer>().As<IContractSerializer>().AsSelf().SingleInstance(); //None
                builder.Register<LogService>().As<ILogService>().AsSelf().SingleInstance(); //None
                builder.Register<SimplePacker>().AsInterfacesAndSelf().InstancePerDependency(); //None

                builder.Register<DeveloperConsole>().AsSelf().As<IDeveloperConsole>().SingleInstance(); //Log, Strings

                builder.Register<Dispatcher>().As<IDispatcher>().AsSelf().SingleInstance(); //AppInfo, Log
                builder.Register<TimeService>().As<ITimeService>().AsSelf().SingleInstance(); //Dispatcher
                builder.Register<Executor>().As<IExecutor>().AsSelf()
                    .SingleInstance(); //Dispatcher, Time, DeveloperConsole
                builder.Register<Scheduler>().AsInterfacesAndSelf().SingleInstance();

                builder.Register<StringIdProvider>().As<IIdProvider<string>>().InstancePerDependency();
                builder.Register<LongIdProvider>().As<IIdProvider<long>>().InstancePerDependency();
                builder.Register<IntIdProvider>().As<IIdProvider<int>>().InstancePerDependency();
                builder.Register<ShortIdProvider>().As<IIdProvider<short>>().InstancePerDependency();
                builder.Register<ByteIdProvider>().As<IIdProvider<byte>>().InstancePerDependency();
                builder.Register<UShortIdProvider>().As<IIdProvider<ushort>>().InstancePerDependency();
            });
        }

        public static ILibrary Ecs()
        {
            return new ActionLibrary(builder =>
            {
                builder.Register<EcsContext>().As<IEcsContext>().AsSelf().SingleInstance();
                builder.Register<EcsEntitiesDatabase>().As<IEcsEntitiesDatabase>().AsSelf().SingleInstance();
                //builder.Register<EcsEntity>().As<IEcsEntity>().AsSelf();
                builder.Register<GroupHolder>().As<IGroupHolder>().AsSelf().InstancePerDependency();

                builder.Register<EcsExtensions.DestroyEntityEngine>().AsSelf().InstancePerScope();

                builder.Register<EnginesRoot>().AsSelf().As<IEcsEngines>()
                    .OnActivation(args =>
                    {
                        var engineRoot = args.Instance;

                        engineRoot.Add(args.TryResolve<ApplySyncDataServerEngine>());
                        engineRoot.Add(args.TryResolve<ApplySyncDataClientEngine>());

                        //All other
                        foreach (var ecsSystem in args.ResolveAll<IEcsEngine>())
                            engineRoot.Add(ecsSystem);

                        engineRoot.Add(args.TryResolve<CollectSyncDataClientEngine>());
                        engineRoot.Add(args.TryResolve<CollectSyncDataServerEngine>());
                        engineRoot.Add(args.TryResolve<CollectFoundLostDataServerEngine>());

                        //engineRoot.Add(args.TryResolve<SendSyncDataClientEngine>());
                        engineRoot.Add(args.TryResolve<SendSyncDataClientEngine>());
                        engineRoot.Add(args.TryResolve<SendSyncDataServerEngine>());

                        engineRoot.Add(args.TryResolve<ObserverLostFindCleanupEngine>());

                        //Destroy entites
                        engineRoot.Add(args.TryResolve<EcsExtensions.DestroyEntityEngine>());
                    }).SingleInstance();
            });
        }

        public static ILibrary WorldsServer()
        {
            return new ActionLibrary(builder =>
            {
                builder.Register<World2>().As<IWorld>().AsSelf().InstancePerScope();

                builder.Register<WorldsServer>().AsInterfacesAndSelf().SingleInstance();

                //builder.Register<ApplySyncDataClientEngine>().AsSelf().InstancePerScope();
                builder.Register<ApplySyncDataServerEngine>().AsSelf().InstancePerScope();

                //builder.Register<CollectSyncDataClientEngine>().AsSelf().InstancePerScope();
                builder.Register<CollectSyncDataServerEngine>().AsSelf().InstancePerScope();
                builder.Register<CollectFoundLostDataServerEngine>().AsSelf().InstancePerScope();

                //builder.Register<SendSyncDataClientEngine>().AsSelf().InstancePerScope();
                builder.Register<SendSyncDataServerEngine>().AsSelf().InstancePerScope();
                builder.Register<ObserverLostFindCleanupEngine>().AsSelf().InstancePerScope();
            });
        }

        public static ILibrary World(bool isOnline, bool newWorldCreator = true)
        {
            return new ActionLibrary(builder =>
            {
                builder.Register<World2>().As<IWorld>().AsSelf().InstancePerScope();
                builder.Register<OnlineWorld2>().As<IWorld>().AsSelf().InstancePerScope();

                if (newWorldCreator)
                    builder.Register<ClientWorldCreator>().AsInterfacesAndSelf().InstancePerScope();

                if (isOnline)
                {
                    builder.Register<ApplySyncDataClientEngine>().AsSelf().InstancePerScope();
                    //builder.Register<ApplySyncDataServerEngine>().AsSelf().InstancePerScope();

                    builder.Register<CollectSyncDataClientEngine>().AsSelf().InstancePerScope();
                    //builder.Register<CollectSyncDataServerEngine>().AsSelf().InstancePerScope();
                    //builder.Register<CollectFoundLostDataServerEngine>().AsSelf().InstancePerScope();

                    builder.Register<SendSyncDataClientEngine>().AsSelf().InstancePerScope();
                    //builder.Register<SendSyncDataClientEngine>().AsSelf().InstancePerScope();
                    //builder.Register<SendSyncDataServerEngine>().AsSelf().InstancePerScope();
                    builder.Register<ObserverLostFindCleanupEngine>().AsSelf().InstancePerScope();
                }
            });
        }

        public static ILibrary Data =>
            new ActionLibrary(builder =>
            {
                builder.Register<EnvironmentConfigService>().As<IEnvironmentConfig>().SingleInstance(); //None
                builder.Register<Configuration>().As<IConfiguration>().AsSelf().SingleInstance(); //Strings, container
                builder.Register<LocalizationService>().AsInterfacesAndSelf().SingleInstance();
                builder.Register<LocalFileStorage>().AsInterfacesAndSelf().SingleInstance();
                builder.Register<FileStorageServer>().AsInterfacesAndSelf().SingleInstance();
            });

        public static ILibrary Ai()
        {
            return new ActionLibrary((container) =>
            {
                container.Register<PlanningSystem>().AsInterfacesAndSelf().SingleInstance();
                container.Register<BehaviourTreeCompiler>().AsInterfacesAndSelf().SingleInstance();
            });
        }
        
        public static ILibrary Scripts => new ActionLibrary(builder =>
        {
            builder.Register<ScriptEngine>().AsInterfacesAndSelf().SingleInstance();
        });
    }
}