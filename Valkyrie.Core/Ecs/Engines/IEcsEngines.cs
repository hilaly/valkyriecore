namespace Valkyrie.Ecs.Engines
{
    public interface IEcsEngines
    {
        int Tick { get; set; }
        float UseFixedTimestamp { get; set; }
        int MaxAllowedIterations { get; set; }

        void Add(IEcsEngine engine);

        //void Initialize();
        //void Execute();
        //void Cleanup();

        //ISequence<IEcsEngine> StartSequence<TEngine>() where TEngine : class, IEcsEntity;
    }
}