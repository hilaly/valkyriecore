namespace Valkyrie.Ecs.Engines
{
    public struct Iteration
    {
        public readonly int SimulationTick;
        public readonly int LocalTick;
        public readonly int ServerTick;
        
        public readonly float DeltaTime;
        public readonly float Time;

        public Iteration(int tick, float deltaTime, float time, int localTick, int serverTick)
        {
            SimulationTick = tick;
            DeltaTime = deltaTime;
            Time = time;
            LocalTick = localTick;
            ServerTick = serverTick;
        }
    }
}