using System.Collections.Generic;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Groups;

namespace Valkyrie.Ecs.Engines
{
    public abstract class QueryEngine : IEcsEngine, IEcsInitializeEngine
    {
        readonly IEcsEntitiesDatabase _entitiesDatabase;
        private IEcsGroup _innerGroup;
        private readonly List<IEcsEntity> _buffer = new List<IEcsEntity>();

        protected QueryEngine(IEcsEntitiesDatabase entitiesDatabase)
        {
            _entitiesDatabase = entitiesDatabase;
        }

        public virtual void Initialize()
        {
            _innerGroup = _entitiesDatabase.GetGroup(GetBuilder());
        }

        protected abstract GroupBuilder GetBuilder();

        protected IList<IEcsEntity> GetEntities()
        {
            return _innerGroup.GetEntities(_buffer);
        }
    }
}