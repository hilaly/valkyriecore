namespace Valkyrie.Ecs.Engines
{
    public interface IEcsExecuteEngine : IEcsEngine
    {
        void Execute(Iteration iterationInfo);
    }
}