using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Sequence;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Time;

namespace Valkyrie.Ecs.Engines
{
    internal class EnginesRoot : IEcsEngines, IDisposable
    {
        private CompositeDisposable _disposable = new CompositeDisposable();
        
        private bool _initialized;
        private readonly ITimeService _timeService;
        private float _iterationTime = 0f;

        private readonly List<IEcsEngine> _allSystems = new List<IEcsEngine>();
        private readonly List<IEcsInitializeEngine> _initializeSystems = new List<IEcsInitializeEngine>();
        private readonly List<IEcsExecuteEngine> _executeSystems = new List<IEcsExecuteEngine>();
        private readonly List<IEcsCleanupEngine> _cleanupSystems = new List<IEcsCleanupEngine>();

        ISequence<IEcsEngine> _mainSequence = new Sequence<IEcsEngine>();
        readonly List<ISequence<IEcsEngine>> _otherSequences = new List<ISequence<IEcsEngine>>();
        private int _localTickDisplace;
        private int _serverTickDisplace;

        public int Tick { get; set; } = 0;
        public int LocalTick
        {
            get => Tick + _localTickDisplace;
            set => _localTickDisplace = value - Tick;
        }
        public int ServerTick
        {
            get => Tick + _serverTickDisplace;
            set => _serverTickDisplace = value - Tick;
        }
        
        public float UseFixedTimestamp { get; set; } = -1;
        public int IterationsPerSecond => (int) (UseFixedTimestamp > 0 ? 1f / UseFixedTimestamp : 1f);

        public int MaxAllowedIterations { get; set; } = 10;

        public EnginesRoot(ITimeService timeService, IDispatcher dispatcher)
        {
            _timeService = timeService;
            _disposable.Add(dispatcher.EveryUpdate(Execute));
        }

        public void Add(IEcsEngine engine)
        {
            lock (_allSystems)
                _allSystems.Add(engine);

            if (engine is IEcsCleanupEngine cleanupSystem)
                lock (_cleanupSystems)
                    _cleanupSystems.Add(cleanupSystem);

            if (engine is IEcsExecuteEngine executeSystem)
                lock(_executeSystems)
                    _executeSystems.Add(executeSystem);

            if (engine is IEcsInitializeEngine initializeSystem)
            {
                lock (_initializeSystems)
                {
                    _initializeSystems.Add(initializeSystem);
                }
                if (_initialized)
                    initializeSystem.Initialize();
            }
        }

        public void Initialize()
        {
            if(_initialized)
                throw new Exception("Already initialized");

            lock (_initializeSystems)
                _initializeSystems.ForEach(system => system.Initialize());

            _initialized = true;
        }

        public void Execute()
        {
            if(!_initialized)
                return;
            
            void IterationTick(Iteration arg)
            {
                lock (_executeSystems)
                    _executeSystems.ForEach(system =>
                    {
                        system.Execute(arg);
                    });
                lock (_cleanupSystems)
                    _cleanupSystems.ForEach(system =>
                    {
                        system.Cleanup();
                    });
            }

            if (UseFixedTimestamp <= 0f)
            {
                _iterationTime = 0f;
                Iteration arg;
                unchecked
                {
                    arg = new Iteration(Tick++, _timeService.Default.DeltaTime, _timeService.Default.Time, LocalTick, ServerTick);   
                }
                IterationTick(arg);
            }
            else
            {
                _iterationTime += _timeService.Default.DeltaTime;
                var start = _timeService.Default.Time - _iterationTime;
                int iterationNumber = 0;
                while (_iterationTime >= UseFixedTimestamp && iterationNumber < MaxAllowedIterations)
                {
                    iterationNumber++;
                    Iteration arg;
                    unchecked
                    {
                        arg = new Iteration(Tick++, UseFixedTimestamp, start, LocalTick, ServerTick);   
                    }
                    IterationTick(arg);

                    _iterationTime -= UseFixedTimestamp;
                    start += UseFixedTimestamp;
                }
            }
        }

        public IEnumerable<IEcsEngine> Engines
        {
            get { lock(_allSystems) return _allSystems; }
        }

        public ISequence<IEcsEngine> StartSequence<TEngine>() where TEngine : class, IEcsEntity
        {
            IEcsEngine engine;
            lock (_allSystems)
                engine = _allSystems.FirstOrDefault(u => u.GetType() == typeof(TEngine));

            if (engine == null)
                throw new InvalidOperationException($"Engine {typeof(TEngine).Name} isn't registered in engine");

            var result = new Sequence<IEcsEngine>();
            _otherSequences.Add(result);
            return result.Step(engine);
        }

        public void Dispose()
        {
            _disposable.Dispose();
        }
    }
}