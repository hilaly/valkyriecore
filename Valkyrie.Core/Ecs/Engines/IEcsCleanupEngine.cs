namespace Valkyrie.Ecs.Engines
{
    public interface IEcsCleanupEngine : IEcsEngine
    {
        void Cleanup();
    }
}