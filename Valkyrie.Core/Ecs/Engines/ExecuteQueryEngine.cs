using System.Collections.Generic;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Engines
{
    public abstract class ExecuteQueryEngine : QueryEngine, IEcsExecuteEngine
    {
        protected ExecuteQueryEngine(IEcsEntitiesDatabase entitiesDatabase) : base(entitiesDatabase)
        {
        }

        public void Execute(Iteration iterationInfo)
        {
            Execute(iterationInfo, GetEntities());
        }

        protected abstract void Execute(Iteration tickInfo, IList<IEcsEntity> entities);
    }
}