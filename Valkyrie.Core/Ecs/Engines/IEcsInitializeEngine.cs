namespace Valkyrie.Ecs.Engines
{
    public interface IEcsInitializeEngine : IEcsEngine
    {
        void Initialize();
    }
}