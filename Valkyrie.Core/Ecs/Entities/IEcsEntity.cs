namespace Valkyrie.Ecs.Entities
{
    public interface IEcsEntity
    {
        ushort Id { get; }

        void Add<T>(T component) where T : IEcsComponent;
        void Remove<T>() where T : IEcsComponent;

        T Get<T>() where T : IEcsComponent;
        //void Destroy();
    }
}