﻿namespace Valkyrie.Ecs.Entities
{
    interface IEntityMatcher
    {
        bool IsMatch(EcsEntity entity);
    }
}