using System;
using System.Collections.Generic;
using System.Diagnostics;
using Valkyrie.Ecs.Groups;
using Valkyrie.Math;
using Valkyrie.Threading.Async;

namespace Valkyrie.Ecs.Entities
{
    class EcsEntitiesDatabase : IEcsEntitiesDatabase, IDisposable
    {
        private readonly IIdProvider<ushort> _entitiesIdProvider;
        readonly ReactiveDictionary<ushort, EcsEntity> _entities = new ReactiveDictionary<ushort, EcsEntity>();

        readonly Dictionary<int, CommonEcsGroup> _groups = new Dictionary<int, CommonEcsGroup>();

        public EcsEntitiesDatabase(IIdProvider<ushort> idProvider)
        {
            _entitiesIdProvider = idProvider;
            _entitiesIdProvider.Prepare(100);
        }

        public IEcsEntity GetEntity(ushort id)
        {
            lock (_entities)
                return _entities.TryGetValue(id, out var result) ? result : default(IEcsEntity);
        }

        #region Groups

        public IEcsGroup GetGroup(GroupBuilder builder)
        {
            var matcher = builder.Build();
            
            if (!_groups.TryGetValue(matcher.GetHashCode(), out var result))
            {
                lock (_entities)
                    result = new CommonEcsGroup(_entities, matcher);
                
                _groups.Add(matcher.GetHashCode(), result);
            }

            return result;
        }

        #endregion

        #region Creation/Destroying

        public IEcsEntity CreateEntity(IEnumerable<IEcsComponent> components)
        {
            return CreateEntity(_entitiesIdProvider.Generate(), components);
        }

        public IEcsEntity CreateEntity(ushort id, IEnumerable<IEcsComponent> components)
        {
            Debug.Assert(GetEntity(id) == null);
            var entity = new EcsEntity(id, components, DestroyEntity);
            lock (_entities)
                _entities.Add(entity.Id, entity);
            
            foreach (var ecsGroup in _groups.Values)
                ecsGroup.OnEntityAdded(entity);
            
            return entity;
        }

        private void DestroyEntity(EcsEntity destroyingEntity)
        {
            foreach (var ecsGroup in _groups.Values)
                ecsGroup.OnEntityRemoved(destroyingEntity);
            
            _entitiesIdProvider.Release(destroyingEntity.Id);
            lock (_entities)
                _entities.Remove(destroyingEntity.Id);
            destroyingEntity.Dispose();
        }

        #endregion

        public void Dispose()
        {
        }
    }
}