using System;
using System.Collections.Generic;
using Valkyrie.Threading.Async;

namespace Valkyrie.Ecs.Entities
{
    internal class EcsEntity : IEcsEntity, IDisposable
    {
        private readonly Action<EcsEntity> _destroyAction;
        private bool _isDisposed;

        internal readonly IEcsComponent[] Components;

        private readonly Subject<CollectionAddEvent<IEcsComponent>> _componentAdded =
            new Subject<CollectionAddEvent<IEcsComponent>>();

        private readonly Subject<CollectionRemoveEvent<IEcsComponent>> _componentRemoved =
            new Subject<CollectionRemoveEvent<IEcsComponent>>();

        public EcsEntity(ushort id, IEnumerable<IEcsComponent> components, Action<EcsEntity> destroyAction)
        {
            Id = id;
            _destroyAction = destroyAction;
            Components = new IEcsComponent[EcsExtensions.GetComponentsCount()];
            foreach (var component in components)
                Add(component);
        }

        public ushort Id { get; }

        internal IObservable<CollectionAddEvent<IEcsComponent>> ObserveAdd()
        {
            return _componentAdded;
        }

        internal IObservable<CollectionRemoveEvent<IEcsComponent>> ObserveRemove()
        {
            return _componentRemoved;
        }

        internal void Add(IEcsComponent component)
        {
            Add(component, EcsExtensions.GetComponentIndex(component.GetType()));
        }

        internal void Add(IEcsComponent component, int index)
        {
            ThrowIfDisposed();
            lock (Components)
                Components[index] = component;
            _componentAdded.OnNext(new CollectionAddEvent<IEcsComponent>(index, component));
        }

        public void Add<T>(T component) where T : IEcsComponent
        {
            ThrowIfDisposed();
            var index = EcsExtensions.IndexHolder<T>.index;
            lock (Components)
                Components[index] = component;
            _componentAdded.OnNext(new CollectionAddEvent<IEcsComponent>(index, component));
        }

        internal void Remove(int index)
        {
            ThrowIfDisposed();
            IEcsComponent toRemove;
            lock (Components)
            {
                toRemove = Components[index];
                if(toRemove == null)
                    return;
                
                Components[index] = null;
            }

            (toRemove as IDisposable)?.Dispose();
            _componentRemoved.OnNext(new CollectionRemoveEvent<IEcsComponent>(index, toRemove));
        }

        public void Remove<T>() where T : IEcsComponent
        {
            ThrowIfDisposed();
            Remove(EcsExtensions.IndexHolder<T>.index);
        }

        public T Get<T>() where T : IEcsComponent
        {
            lock (Components)
                return (T) Components[EcsExtensions.IndexHolder<T>.index];
        }

        public void Destroy()
        {
            _destroyAction(this);
        }

        void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException("");
        }

        public void Dispose()
        {
            if (_isDisposed)
                return;

            foreach (var component in Components)
                (component as IDisposable)?.Dispose();

            _componentAdded.Dispose();
            _componentRemoved.Dispose();

            _isDisposed = true;
        }
    }
}