﻿using System.Collections.Generic;
using Valkyrie.Ecs.Groups;

namespace Valkyrie.Ecs.Entities
{
    public interface IEcsEntitiesDatabase
    {
        IEcsEntity GetEntity(ushort id);

        IEcsGroup GetGroup(GroupBuilder builder);

        IEcsEntity CreateEntity(IEnumerable<IEcsComponent> components);
        IEcsEntity CreateEntity(ushort id, IEnumerable<IEcsComponent> components);
    }
}