using System;
using Valkyrie.Ecs.Engines;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs
{
    class EcsContext : IEcsContext, IDisposable
    {
        private readonly EnginesRoot _engines;
        private bool _initialized;

        public EcsContext(IEcsEntitiesDatabase ecsEntitiesDatabase, EnginesRoot engines)
        {
            EcsEntitiesDatabase = ecsEntitiesDatabase;
            _engines = engines;
        }

        public IEcsEntitiesDatabase EcsEntitiesDatabase { get; }

        public IEcsEngines Engines => _engines;

        public void Start()
        {
            if(_initialized)
                return;
            _initialized = true;
            _engines.Initialize();
        }

        public void Dispose()
        {
        }
    }
}