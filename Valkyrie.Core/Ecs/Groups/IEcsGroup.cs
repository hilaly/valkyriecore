﻿using System.Collections.Generic;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Groups
{
    public interface IEcsGroup : IEnumerable<IEcsEntity>
    {
        int Count { get; }
        
        List<IEcsEntity> GetEntities(List<IEcsEntity> buffer);
    }
}