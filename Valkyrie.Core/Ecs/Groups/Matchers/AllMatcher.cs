﻿using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Groups.Matchers
{
    class AllMatcher : IEntityMatcher
    {
        private readonly IEntityMatcher[] _matchers;

        public AllMatcher(params IEntityMatcher[] matchers)
        {
            _matchers = matchers;
        }

        public bool IsMatch(EcsEntity entity)
        {
            for(var i = 0; i < _matchers.Length; ++i)
                if (!_matchers[i].IsMatch(entity))
                    return false;
            return true;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 17;
                foreach (var matcher in _matchers)
                    hash = hash * 23 + matcher.GetHashCode();
                return hash;   
            }
        }
    }
}