﻿using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Groups.Matchers
{
    class AnyMatcher : IEntityMatcher
    {
        private readonly IEntityMatcher[] _matchers;

        public AnyMatcher(params IEntityMatcher[] matchers)
        {
            _matchers = matchers;
        }

        public bool IsMatch(EcsEntity entity)
        {
            for(var i = 0; i < _matchers.Length; ++i)
                if (_matchers[i].IsMatch(entity))
                    return true;
            return false;
        }
        
        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 19;
                foreach (var matcher in _matchers)
                    hash = hash * 7 + matcher.GetHashCode();
                return hash;   
            }
        }
    }
}