﻿using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Groups.Matchers
{
    class NotTypeMatcher<T> : IEntityMatcher where T : IEcsComponent
    {
        public bool IsMatch(EcsEntity entity)
        {
            return entity.Get<T>() == null;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return 9 * 13 + typeof(T).GetHashCode();
            }
        }
    }
}