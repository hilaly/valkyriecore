﻿using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Groups.Matchers
{
    class TypeMatcher<T> : IEntityMatcher where T : IEcsComponent
    {
        public bool IsMatch(EcsEntity entity)
        {
            return entity.Get<T>() != null;
        }

        public override int GetHashCode()
        {
            return typeof(T).GetHashCode();
        }
    }
}