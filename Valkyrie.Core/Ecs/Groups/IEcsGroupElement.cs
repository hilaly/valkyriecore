﻿using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs.Groups
{
    public interface IEcsGroupElement
    {
        IEcsEntity Entity { get; }
    }
}