﻿using System.Collections.Generic;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Groups.Matchers;

namespace Valkyrie.Ecs.Groups
{
    public interface IGroupHolder
    {
        void Setup(GroupBuilder groupBuilder);
        
        IList<IEcsEntity> GetEntities();
    }
    
    sealed class GroupHolder : IGroupHolder
    {
        private readonly IEcsEntitiesDatabase _edb;
        private IEcsGroup _group;
        private readonly List<IEcsEntity> _buffer = new List<IEcsEntity>();

        public GroupHolder(IEcsEntitiesDatabase edb)
        {
            _edb = edb;
        }

        public void Setup(GroupBuilder groupBuilder)
        {
            _group = _edb.GetGroup(groupBuilder);
        }

        public IList<IEcsEntity> GetEntities()
        {
            return _group.GetEntities(_buffer);
        }
    }
    
    public sealed class GroupBuilder
    {
        private List<IEntityMatcher> _matchers;
        
        private GroupBuilder()
        {
        }
        
        #region None

        public static GroupBuilder Empty()
        {
            return new GroupBuilder {_matchers = new List<IEntityMatcher>()};
        }
        
        #endregion

        #region All
        
        public static GroupBuilder AllOf<TComponent0, TComponent1, TComponent2, TComponent3>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
            where TComponent2 : class, IEcsComponent
            where TComponent3 : class, IEcsComponent
        {
            return new GroupBuilder
            {
                _matchers = new List<IEntityMatcher>
                {
                    new TypeMatcher<TComponent0>(),
                    new TypeMatcher<TComponent1>(),
                    new TypeMatcher<TComponent2>(),
                    new TypeMatcher<TComponent3>()
                }
            };
        }
        
        public static GroupBuilder AllOf<TComponent0, TComponent1, TComponent2>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
            where TComponent2 : class, IEcsComponent
        {
            return new GroupBuilder
            {
                _matchers = new List<IEntityMatcher>
                {
                    new TypeMatcher<TComponent0>(),
                    new TypeMatcher<TComponent1>(),
                    new TypeMatcher<TComponent2>()
                }
            };
        }
        
        public static GroupBuilder AllOf<TComponent0, TComponent1>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
        {
            var result = new GroupBuilder(); 
            return new GroupBuilder
            {
                _matchers = new List<IEntityMatcher>
                {
                    new TypeMatcher<TComponent0>(),
                    new TypeMatcher<TComponent1>()
                }
            };
        }
        
        public static GroupBuilder AllOf<TComponent0>()
            where TComponent0 : class, IEcsComponent
        {
            return new GroupBuilder
            {
                _matchers = new List<IEntityMatcher>
                {
                    new TypeMatcher<TComponent0>()
                }
            };
        }
        
        #endregion

        #region Not
        
        public GroupBuilder NotOf<TComponent0, TComponent1, TComponent2, TComponent3>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
            where TComponent2 : class, IEcsComponent
            where TComponent3 : class, IEcsComponent
        {
            _matchers.AddRange(new IEntityMatcher[]
                {
                    new NotTypeMatcher<TComponent0>(),
                    new NotTypeMatcher<TComponent1>(),
                    new NotTypeMatcher<TComponent2>(),
                    new NotTypeMatcher<TComponent3>()   
                }
            );
            return this;
        }

        public GroupBuilder NotOf<TComponent0, TComponent1, TComponent2>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
            where TComponent2 : class, IEcsComponent
        {
            _matchers.AddRange(new IEntityMatcher[]
                {
                    new NotTypeMatcher<TComponent0>(),
                    new NotTypeMatcher<TComponent1>(),
                    new NotTypeMatcher<TComponent2>()   
                }
            );
            return this;
        }

        public GroupBuilder NotOf<TComponent0, TComponent1>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
        {
            _matchers.AddRange(new IEntityMatcher[]
                {
                    new NotTypeMatcher<TComponent0>(),
                    new NotTypeMatcher<TComponent1>()   
                }
            );
            return this;
        }

        public GroupBuilder NotOf<TComponent0>()
            where TComponent0 : class, IEcsComponent
        {
            _matchers.AddRange(new IEntityMatcher[]
                {
                    new NotTypeMatcher<TComponent0>()   
                }
            );
            return this;
        }

        #endregion

        #region Any

        public GroupBuilder AnyOf<TComponent0, TComponent1, TComponent2, TComponent3>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
            where TComponent2 : class, IEcsComponent
            where TComponent3 : class, IEcsComponent
        {
            _matchers.Add(new AnyMatcher(
                    new TypeMatcher<TComponent0>(),
                    new TypeMatcher<TComponent1>(),
                    new TypeMatcher<TComponent2>(),
                    new TypeMatcher<TComponent3>()
                )
            );
            return this;
        }
        
        public GroupBuilder AnyOf<TComponent0, TComponent1, TComponent2>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
            where TComponent2 : class, IEcsComponent
        {
            _matchers.Add(new AnyMatcher(
                    new TypeMatcher<TComponent0>(),
                    new TypeMatcher<TComponent1>(),
                    new TypeMatcher<TComponent2>()
                )
            );
            return this;
        }
        
        public GroupBuilder AnyOf<TComponent0, TComponent1>()
            where TComponent0 : class, IEcsComponent
            where TComponent1 : class, IEcsComponent
        {
            _matchers.Add(new AnyMatcher(
                    new TypeMatcher<TComponent0>(),
                    new TypeMatcher<TComponent1>()
                )
            );
            return this;
        }
        
        #endregion

        internal IEntityMatcher Build()
        {
            if (_matchers.Count > 1)
                return new AllMatcher(_matchers.ToArray());
            else
                return _matchers[0];
        }
    }
}