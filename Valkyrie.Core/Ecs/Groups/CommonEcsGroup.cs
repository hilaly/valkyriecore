﻿using System;
using System.Collections;
using System.Collections.Generic;
using Valkyrie.Ecs.Entities;
using Valkyrie.Threading.Async;

namespace Valkyrie.Ecs.Groups
{
    class CommonEcsGroup : IEcsGroup, IDisposable
    {
        private readonly IEntityMatcher _matcher;
        private readonly CompositeDisposable _disposable;
        private readonly HashSet<IEcsEntity> _ecsEntities = new HashSet<IEcsEntity>();
        private readonly Dictionary<EcsEntity, IDisposable> _notAddedEntitiesSubscriptions =
            new Dictionary<EcsEntity, IDisposable>();
        private readonly Dictionary<EcsEntity, IDisposable> _addedEntitiesSubscriptions =
            new Dictionary<EcsEntity, IDisposable>();

        public CommonEcsGroup(IReadOnlyReactiveDictionary<ushort, EcsEntity> entities, IEntityMatcher matcher)
        {
            _matcher = matcher;
            _disposable = new CompositeDisposable(
                /*entities.ObserveAdd().Subscribe(OnEntityAdded),
                entities.ObserveRemove().Subscribe(OnEntityRemoved)*/
            );
            
            lock(entities)
                foreach (var entityPair in entities)
                {
                    var entity = entityPair.Value;
                    if (_matcher.IsMatch(entity))
                        AddEntity(entity);
                    else
                        LookEntity(entity);
                }
        }

        public int Count => _ecsEntities.Count;

        public List<IEcsEntity> GetEntities(List<IEcsEntity> buffer)
        {
            buffer.Clear();
            lock(_ecsEntities)
                buffer.AddRange(_ecsEntities);
            return buffer;
        }

        #region IEnumerable
        
        public IEnumerator<IEcsEntity> GetEnumerator()
        {
            lock (_ecsEntities)
                return _ecsEntities.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        #endregion
        
        #region Subscriptions

        public void OnEntityAdded(EcsEntity entity)
        {
            if (_matcher.IsMatch(entity))
                AddEntity(entity);
            else
                LookEntity(entity);
        }

        public void OnEntityRemoved(EcsEntity entity)
        {
            if (_matcher.IsMatch(entity))
                RemoveEntity(entity);
            else
                UnlookEntity(entity);
        }
        
        void OnEntityAdded(DictionaryAddEvent<string, EcsEntity> evt)
        {
            OnEntityAdded(evt.Value);
        }

        void OnEntityRemoved(DictionaryRemoveEvent<string, EcsEntity> evt)
        {
            OnEntityRemoved(evt.Value);
        }

        private void AddEntity(EcsEntity entity)
        {
            lock(_ecsEntities)
                _ecsEntities.Add(entity);
            
            SubscribeOnMatch(entity);
        }

        private void RemoveEntity(EcsEntity entity)
        {
            UnsubscribeOnMatch(entity);
            
            lock (_ecsEntities)
                _ecsEntities.Remove(entity);
        }

        void LookEntity(EcsEntity entity)
        {
            var subscription = new CompositeDisposable(
                entity.ObserveAdd().Subscribe(args => CheckNotAddedEntity(entity)),
                entity.ObserveRemove().Subscribe(args => CheckNotAddedEntity(entity))
            );
            lock(_notAddedEntitiesSubscriptions)
                _notAddedEntitiesSubscriptions.Add(entity, subscription);
        }

        void UnlookEntity(EcsEntity entity)
        {
            IDisposable subscription;
            lock (_notAddedEntitiesSubscriptions)
            {
                subscription = _notAddedEntitiesSubscriptions[entity];
                _notAddedEntitiesSubscriptions.Remove(entity);
            }
            subscription.Dispose();
        }

        void SubscribeOnMatch(EcsEntity entity)
        {
            var subscription = new CompositeDisposable(
                entity.ObserveAdd().Subscribe(args => CheckAddedEntity(entity)),
                entity.ObserveRemove().Subscribe(args => CheckAddedEntity(entity))
            );
            lock(_addedEntitiesSubscriptions)
                _addedEntitiesSubscriptions.Add(entity, subscription);
        }

        void UnsubscribeOnMatch(EcsEntity entity)
        {
            IDisposable subscription;
            lock (_addedEntitiesSubscriptions)
            {
                subscription = _addedEntitiesSubscriptions[entity];
                _addedEntitiesSubscriptions.Remove(entity);
            }
            subscription.Dispose();
        }

        void CheckNotAddedEntity(EcsEntity entity)
        {
            if(!_matcher.IsMatch(entity))
                return;
            
            UnlookEntity(entity);
            AddEntity(entity);
        }

        void CheckAddedEntity(EcsEntity entity)
        {
            if (_matcher.IsMatch(entity))
                return;
            
            RemoveEntity(entity);
            LookEntity(entity);
        }

        #endregion
        
        #region IDisposable

        public void Dispose()
        {
            _disposable.Dispose();
        }
        
        #endregion
    }
}