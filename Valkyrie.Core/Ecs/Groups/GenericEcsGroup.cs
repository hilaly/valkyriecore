using System;
using System.Collections;
using System.Collections.Generic;
using Valkyrie.Ecs.Entities;
using Valkyrie.Threading.Async;

namespace Valkyrie.Ecs.Groups
{
    abstract class GenericEcsGroup<TGroupElement> : IDisposable, IEnumerable<TGroupElement>
        where TGroupElement : IEcsGroupElement
    {
        private readonly IEntityMatcher _matcher;
        private readonly Dictionary<EcsEntity, TGroupElement> _entitiesCache =
            new Dictionary<EcsEntity, TGroupElement>();
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        private readonly Dictionary<EcsEntity, IDisposable> _entitiesSubscriptions =
            new Dictionary<EcsEntity, IDisposable>();

        protected GenericEcsGroup(IReadOnlyReactiveDictionary<string, EcsEntity> entities, IEntityMatcher matcher)
        {
            _matcher = matcher;
            _disposable.Add(entities.ObserveAdd().Subscribe(OnEntityAdded));
            _disposable.Add(entities.ObserveRemove().Subscribe(OnEntityRemoved));

            lock(entities)
                foreach (var entityPair in entities)
                {
                    var entity = entityPair.Value;
                    if (_matcher.IsMatch(entity))
                        ObserveEntity(entity);
                    else
                        LookEntity(entity);
                }
        }

        protected abstract TGroupElement CreateElement(EcsEntity entity);

        #region Properties

        public int Count { get { return _entitiesCache.Count; } }
        
        #endregion

        #region Subscriptions

        #region Dictionaries



        void OnEntityAdded(DictionaryAddEvent<string, EcsEntity> evt)
        {
            var entity = evt.Value;
            if (_matcher.IsMatch(entity))
                ObserveEntity(entity);
            else
                LookEntity(entity);
        }

        void OnEntityRemoved(DictionaryRemoveEvent<string, EcsEntity> evt)
        {
            var entity = evt.Value;
            if (_matcher.IsMatch(entity))
                UnobserveEntity(entity);
            else
                UnlookEntity(entity);
        }

        #endregion

        #region Entites subscriptions

        void ObserveEntity(EcsEntity entity)
        {
            lock (_entitiesCache)
                _entitiesCache.Add(entity, CreateElement(entity));
            SubscribeOnMatch(entity);
        }

        void UnobserveEntity(EcsEntity entity)
        {
            UnsubscribeOnMatch(entity);
            lock (_entitiesCache)
                _entitiesCache.Remove(entity);
        }

        void LookEntity(EcsEntity entity)
        {
            var entitySubscription = new CompositeDisposable();
            entitySubscription.Add(entity.ObserveAdd().Subscribe(args => EntityAddComponent(entity)));
            lock (_entitiesSubscriptions)
                _entitiesSubscriptions.Add(entity, entitySubscription);
        }

        void UnlookEntity(EcsEntity entity)
        {
            IDisposable subscription;
            lock (_entitiesSubscriptions)
            {
                subscription = _entitiesSubscriptions[entity];
                _entitiesSubscriptions.Remove(entity);
            }
            subscription.Dispose();
        }

        private void SubscribeOnMatch(EcsEntity entity)
        {
            var entitySubscriptions = new CompositeDisposable();
            entitySubscriptions.Add(
                entity.ObserveRemove().Subscribe(args => EntityRemoveComponent(entity)));
            lock (_entitiesSubscriptions)
                _entitiesSubscriptions.Add(entity, entitySubscriptions);
        }

        private void UnsubscribeOnMatch(EcsEntity entity)
        {
            IDisposable subscription;
            lock (_entitiesSubscriptions)
            {
                subscription = _entitiesSubscriptions[entity];
                _entitiesSubscriptions.Remove(entity);
            }
            subscription.Dispose();
        }

        void EntityAddComponent(EcsEntity entity)
        {
            if (!_matcher.IsMatch(entity))
                return;
            UnlookEntity(entity);
            ObserveEntity(entity);
        }

        void EntityRemoveComponent(EcsEntity entity)
        {
            if (_matcher.IsMatch(entity))
                return;
            UnobserveEntity(entity);
            LookEntity(entity);
        }

        #endregion

        #endregion

        #region IDisposable

        public void Dispose()
        {
            _disposable.Dispose();
        }

        #endregion

        #region IEnumerable

        public IEnumerator<TGroupElement> GetEnumerator()
        {
            return GetEntities().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region IGroup

        public IEnumerable<TGroupElement> GetEntities()
        {
            return _entitiesCache.Values;
        }

        public List<TGroupElement> GetEntities(List<TGroupElement> buffer)
        {
            buffer.Clear();
            buffer.AddRange(GetEntities());
            return buffer;
        }

        #endregion
    }
}