﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Valkyrie.Ecs.Sequence
{
    class ConditionalSequence<T, TCondition> : IConditionalSequence<T,TCondition>, IStep<T>
    {
        readonly Dictionary<TCondition, IStep<T>> _statesTree = new Dictionary<TCondition, IStep<T>>();

        public ISequence<T> Option(TCondition condition, T step)
        {
            var result = new Sequence<T>();
            _statesTree.Add(condition, result);
            return result.Step(step);
        }

        public void Visit(ISequenceVisitor<T> visitor, ref object condition)
        {
            if (condition == null)
                throw new NullReferenceException("Condition is null");
            if(!(condition is TCondition))
                throw new InvalidDataException("Condition is invalid type");
            IStep<T> step;
            if (_statesTree.TryGetValue((TCondition) condition, out step))
                step.Visit(visitor, ref condition);
            else
                throw new InvalidOperationException("Havent branch with condition");
        }
    }
}