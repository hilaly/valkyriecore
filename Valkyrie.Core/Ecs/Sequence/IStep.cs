﻿namespace Valkyrie.Ecs.Sequence
{
    interface IStep<T>
    {
        void Visit(ISequenceVisitor<T> visitor, ref object condition);
    }
}