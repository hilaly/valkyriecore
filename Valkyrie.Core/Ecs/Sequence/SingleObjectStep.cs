﻿namespace Valkyrie.Ecs.Sequence
{
    class SingleObjectStep<T> : IStep<T>
    {
        private readonly T _o;

        public SingleObjectStep(T o)
        {
            _o = o;
        }

        public void Visit(ISequenceVisitor<T> visitor, ref object condition)
        {
            visitor.Visit(_o, ref condition);
        }
    }
}