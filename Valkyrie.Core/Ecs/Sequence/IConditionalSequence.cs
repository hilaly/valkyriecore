namespace Valkyrie.Ecs.Sequence
{
    public interface IConditionalSequence<T, in TCondition>
    {
        ISequence<T> Option(TCondition condition, T step);
    }
}