namespace Valkyrie.Ecs.Sequence
{
    public interface ISequence<T>
    {
        void Iterate(ISequenceVisitor<T> visitor);

        ISequence<T> Step(T step);
        IConditionalSequence<T, TCondition> Step<TCondition>();
    }
}