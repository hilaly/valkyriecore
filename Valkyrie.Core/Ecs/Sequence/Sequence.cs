﻿using System.Collections.Generic;

namespace Valkyrie.Ecs.Sequence
{
    public class Sequence<T> : ISequence<T>, IStep<T>
    {
        readonly List<IStep<T>> _steps = new List<IStep<T>>();

        public void Iterate(ISequenceVisitor<T> visitor)
        {
            object condition = null;
            Visit(visitor, ref condition);
        }

        public ISequence<T> Step(T step)
        {
            _steps.Add(new SingleObjectStep<T>(step));
            return this;
        }

        public IConditionalSequence<T, TCondition> Step<TCondition>()
        {
            var result = new ConditionalSequence<T, TCondition>();
            _steps.Add(result);
            return result;
        }

        public void Visit(ISequenceVisitor<T> visitor, ref object condition)
        {
            for (var index = 0; index < _steps.Count; ++index)
                _steps[index].Visit(visitor, ref condition);
        }
    }
}
