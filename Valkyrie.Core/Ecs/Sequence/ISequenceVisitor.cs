namespace Valkyrie.Ecs.Sequence
{
    public interface ISequenceVisitor<in T>
    {
        void Visit(T element, ref object condition);
    }
}