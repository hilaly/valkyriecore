using Valkyrie.Ecs.Engines;

namespace Valkyrie.Ecs.NewFeature
{
    public interface IEcsResultEngine<out TResult> : IEcsEngine
    {
        TResult Execute();
    }
}