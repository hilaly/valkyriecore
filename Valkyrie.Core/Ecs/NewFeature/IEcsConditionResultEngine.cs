using Valkyrie.Ecs.Engines;

namespace Valkyrie.Ecs.NewFeature
{
    public interface IEcsConditionResultEngine<in TCondition, out TResult> : IEcsEngine
    {
        TResult Execute(TCondition condition);
    }
}