using Valkyrie.Ecs.Engines;

namespace Valkyrie.Ecs.NewFeature
{
    public interface IEcsSequence
    {
        IEcsSequence Step<TEngine>() where TEngine : class, IEcsEngine;
    }
}