using Valkyrie.Ecs.Engines;

namespace Valkyrie.Ecs.NewFeature
{
    public interface IEcsConditionEngine<in TCondition> : IEcsEngine
    {
        void Execute(TCondition condition);
    }
}