using Valkyrie.Ecs.Engines;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs
{
    public interface IEcsContext
    {
        IEcsEntitiesDatabase EcsEntitiesDatabase { get; }
        IEcsEngines Engines { get; }

        void Start();
    }
}