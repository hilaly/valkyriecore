using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.Ecs.Engines;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Groups;
using Valkyrie.Math.Space.Sync;

namespace Valkyrie.Ecs
{
    public static class EcsExtensions
    {
        private static Dictionary<Type, int> _componentTypesToIndexMapping;
        private static int _componenetsCount = -1;

        private static List<Type> ComponentsMapping;

        internal static List<Type> GetIndexMappingForComponents()
        {
            if (ComponentsMapping == null)
                ComponentsMapping = GetComponentsMapping().OrderBy(u => u.Value).Select(u => u.Key).ToList();
            return ComponentsMapping;
        }
        internal static Dictionary<Type, int> GetComponentsMapping()
        {
            if (_componentTypesToIndexMapping == null)
            {
                _componentTypesToIndexMapping = new Dictionary<Type, int>();
                var subtypes = typeof(IEcsComponent).GetAllSubTypes(type => !type.IsAbstract && type.IsClass);
                for (var i = 0; i < subtypes.Count; ++i)
                {
                    var ct = subtypes[i];
                    typeof(IndexHolder<>).MakeGenericType(ct).GetField("index").SetValue(null, i);
                    _componentTypesToIndexMapping.Add(ct, i);
                    if (ct.GetCustomAttribute<SyncAttribute>(false) != null &&
                        ct.GetCustomAttribute<ContractAttribute>(false) != null)
                    {
                        SyncComponents.Add(i);
                        var syncAttr = ct.GetCustomAttribute<SyncAttribute>(false);
                        
                        if (syncAttr.DontDestroyOnNetwork)
                            DontDestroyNetworkComponents.Add(i);
                        if((syncAttr.SyncFrom & EntityType.Controller) != 0)
                            ControllerSendsComponents.Add(i);
                        if((syncAttr.SyncFrom & EntityType.Controller) != 0 || (syncAttr.SyncFrom & EntityType.Server) != 0)
                            OwnerSendsComponents.Add(i);
                        if ((syncAttr.SyncFrom & EntityType.Controller) != 0 &&
                            (syncAttr.SyncFrom & EntityType.Server) == 0)
                            OnlyControllerSendsComponents.Add(i);
                        if ((syncAttr.SyncFrom & EntityType.Controller) == 0 &&
                            (syncAttr.SyncFrom & EntityType.Server) != 0)
                            OnlyOwnerSendsComponents.Add(i);

                        if ((syncAttr.SyncFrom & EntityType.Controller) != 0 ||
                            (syncAttr.SyncFrom & EntityType.Server) != 0)
                            ServerSendsComponents.Add(i);
                    }
                }
                _componenetsCount = subtypes.Count;
            }

            return _componentTypesToIndexMapping;
        }
        
        internal static int GetComponentsCount()
        {
            if (_componenetsCount < 0)
                return GetComponentsMapping().Count;
            return _componenetsCount;
        }

        internal static int GetComponentIndex(Type componentType)
        {
            return GetComponentsMapping()[componentType];
        }
            
        public static IEcsEntity CreateEntity(this IEcsEntitiesDatabase entitiesDatabase)
        {
            return entitiesDatabase.CreateEntity(Enumerable.Empty<IEcsComponent>());
        }
        public static IEcsEntity CreateEntity(this IEcsEntitiesDatabase entitiesDatabase, ushort id)
        {
            return entitiesDatabase.CreateEntity(id, Enumerable.Empty<IEcsComponent>());
        }

        public static void DestroyIfEmpty(this IEcsEntity entity)
        {
            if(((EcsEntity)entity).Components.All(u => u == null))
                entity.Destroy();
        }

        public static void AddComponent(this IEcsEntity entity, IEcsComponent component)
        {
            ((EcsEntity) entity).Add(component);
        }

        public static bool HasComponent<T>(this IEcsEntity entity) where T : IEcsComponent
        {
            return entity.Get<T>() != null;
        }
        public static bool HasComponent(this IEcsEntity entity, Type componentType)
        {
            return GetComponent(entity, componentType) != null;
        }
        public static IEcsComponent GetComponent(this IEcsEntity entity, Type componentType)
        {
            return ((EcsEntity) entity).Components[GetComponentIndex(componentType)];
        }

        public static T GetOrCreate<T>(this IEcsEntity entity) where T : class, IEcsComponent, new()
        {
            var result = entity.Get<T>();
            if (result == null)
            {
                result = new T();
                entity.Add(result);
            }
            return result;
        }
        
        internal class IndexHolder<T> where T : IEcsComponent
        {
            public static int index = -1;
        }
        
        internal static List<int> DontDestroyNetworkComponents = new List<int>();
        internal static List<int> SyncComponents = new List<int>();
        
        internal static List<int> OwnerSendsComponents = new List<int>();
        internal static List<int> OnlyOwnerSendsComponents = new List<int>();
        internal static List<int> ControllerSendsComponents = new List<int>();
        internal static List<int> OnlyControllerSendsComponents = new List<int>();
        internal static List<int> ServerSendsComponents = new List<int>();

        [Obsolete("NotImplemented", true)]
        internal static List<KeyValuePair<int, Func<IEcsComponent>>> AutocreatedOnController =
            new List<KeyValuePair<int, Func<IEcsComponent>>>();

        public static ushort GetNetworkObjectId(ushort serverId)
        {
            return serverId;
        }

        public static IEcsEntity Destroy(this IEcsEntity entity)
        {
            entity.GetOrCreate<NeedDestroyComponent>();
            return entity;
        }

        internal class NeedDestroyComponent : IEcsComponent
        {
        }
        
        internal class DestroyEntityEngine : IEcsCleanupEngine
        {
            private readonly IEcsGroup _cleanupGroup;
            private readonly List<IEcsEntity> _buffer = new List<IEcsEntity>();

            public DestroyEntityEngine(IEcsEntitiesDatabase edb)
            {
                _cleanupGroup = edb.GetGroup(GroupBuilder.AllOf<NeedDestroyComponent>());
            }

            public void Cleanup()
            {
                foreach (var entity in _cleanupGroup.GetEntities(_buffer))
                    ((EcsEntity) entity).Destroy();
            }
        }
    }
}