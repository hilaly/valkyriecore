using System;
using System.Collections.Generic;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs
{
    public interface IContext : IDisposable
    {
        int TotalComponents { get; }

        Stack<IEcsComponent>[] ComponentPools { get; }
        ContextInfo ContextInfo { get; }

        int Count { get; }
    }
}