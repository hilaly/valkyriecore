using System;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Ecs
{
    public abstract class Entity : IEntity
    {
        public event Action<int, IEntity> OnComponentAdded;
        public event Action<int, IEntity> OnComponentRemoved;
        public event Action<IEntity> OnEntityDestroy;
        
        public int Id { get; set; }

        private readonly IEcsComponent[] _components;

        protected Entity(int componentsCount)
        {
            _components = new IEcsComponent[componentsCount];
        }

        protected IEcsComponent GetComponent(int index)
        {
            if(!HasComponent(index))
                throw new Exception($"Component does not exist");

            return _components[index];
        }

        protected IEcsComponent AddComponent(IEcsComponent component, int index)
        {
            if (HasComponent(index))
                throw new Exception($"Component already exist");
            
            var result = _components[index] = component;
            OnComponentAdded?.Invoke(index, this);
            return result;
        }
        
        protected void RemoveComponent(int index)
        {
            if (!HasComponent(index))
                throw new Exception($"Component does not exist");
            
            _components[index] = null;
            OnComponentRemoved?.Invoke(index, this);
        }

        public bool HasComponent(int index)
        {
            return _components[index] != null;
        }

        public void Destroy()
        {
            OnEntityDestroy?.Invoke(this);
        }

        public void RemoveAllComponents()
        {
            for (var index = 0; index < _components.Length; index++)
                if (_components[index] != null)
                    RemoveComponent(index);
        }

        public bool HasComponents(int[] indices)
        {
            foreach (var index in indices)
                if (_components[index] == null)
                    return false;
            return true;
        }
    }
}