using System.Collections.Generic;
using System.Linq;
using Valkyrie.Ecs.Entities;
using Valkyrie.Threading.Async;
using Valkyrie.Tools;

namespace Valkyrie.Ecs
{
    public interface IContext<T> : IContext where T : class, IEntity
    {
        T CreateEntity();
        bool HasEntity(T entity);
        T[] GetEntities();

        IGroup<T> GetGroup(IMatcher<T> builder);
    }
    
    public abstract class Context<T> : IContext<T> where T : class, IEntity
    {
        public int TotalComponents { get; }
        public Stack<IEcsComponent>[] ComponentPools { get; }
        public ContextInfo ContextInfo { get; }
        
        public int Count => _entitiesCache.Count;

        protected Context(int totalComponents)
        {
            TotalComponents = totalComponents;
            _entitiesPool = new Pool<T>(10, () =>
                {
                    var temp = CreateEntityImpl();
                    temp.OnEntityDestroy += DestroyEntityInternal;
                    return temp;
                },
                entity => entity.RemoveAllComponents(),
                entity =>
                {
                    entity.RemoveAllComponents();
                    _entitiesCache.Remove(entity);
                });
        }

        private void DestroyEntityInternal(IEntity obj)
        {
            _entitiesPool.Store((T) obj);
        }

        protected abstract T CreateEntityImpl();

        #region Typed Context

        private readonly Pool<T> _entitiesPool;
        private readonly ReactiveCollection<T> _entitiesCache = new ReactiveCollection<T>(10);


        public T CreateEntity()
        {
            var result = _entitiesPool.New();
            _entitiesCache.Add(result);
            return result;
        }

        public bool HasEntity(T entity)
        {
            return _entitiesCache.Contains(entity);
        }

        public T[] GetEntities()
        {
            return _entitiesCache.ToArray();
        }
        
        #endregion

        #region groups

        private readonly Dictionary<int, Group<T>> _groups = new Dictionary<int, Group<T>>();

        public IGroup<T> GetGroup(IMatcher<T> matcher)
        {
            if (!_groups.TryGetValue(matcher.GetHashCode(), out var result))
            {
                lock (_groups)
                    result = new Group<T>(_entitiesCache, matcher);
                
                _groups.Add(matcher.GetHashCode(), result);
            }

            return result;
        }

        #endregion

        public void Dispose()
        {
            lock (_groups)
            {
                foreach (var groupsValue in _groups.Values) groupsValue.Dispose();
                _groups.Clear();
            }
        }
    }
}