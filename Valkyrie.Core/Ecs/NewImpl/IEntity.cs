using System;

namespace Valkyrie.Ecs
{
    public interface IEntity
    {
        event Action<int, IEntity> OnComponentAdded;
        event Action<int, IEntity> OnComponentRemoved;
        event Action<IEntity> OnEntityDestroy;
        
        int Id { get; }

        void RemoveAllComponents();
        bool HasComponents(int[] indices);
        bool HasComponent(int index);

        void Destroy();
    }
}