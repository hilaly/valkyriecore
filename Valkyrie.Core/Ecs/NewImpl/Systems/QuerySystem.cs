using System.Collections.Generic;

namespace Valkyrie.Ecs
{
    public abstract class QuerySystem<TContext, TEntity> : IExecuteSystem, IInitializeSystem
        where TEntity : class, IEntity
        where TContext : Context<TEntity>
    {
        private readonly TContext _context;
        private IGroup<TEntity> _innerGroup;
        private readonly List<TEntity> _buffer = new List<TEntity>();

        protected QuerySystem(TContext context)
        {
            _context = context;
        }

        public void Initialize()
        {
            _innerGroup = _context.GetGroup(GetMatcher());
        }

        public void Execute()
        {
            if(_innerGroup.Count == 0)
                return;

            var entities = _innerGroup.GetEntities(_buffer);
            foreach (var entity in entities)
                Execute(entity);
        }

        protected abstract IMatcher<TEntity> GetMatcher();

        protected abstract void Execute(TEntity entity);
    }
}