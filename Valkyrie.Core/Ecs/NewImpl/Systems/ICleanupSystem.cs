namespace Valkyrie.Ecs
{
    public interface ICleanupSystem : ISystem
    {
        void Cleanup();
    }
}