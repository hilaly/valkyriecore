namespace Valkyrie.Ecs
{
    public interface IInitializeSystem : ISystem
    {
        void Initialize();
    }
}