namespace Valkyrie.Ecs
{
    public interface IShutdownSystem : ISystem
    {
        void Shutdown();
    }
}