using System.Collections.Generic;

namespace Valkyrie.Ecs
{
    public class Systems : IInitializeSystem, IShutdownSystem, IExecuteSystem, ICleanupSystem
    {
        readonly List<IInitializeSystem> _initializeSystems = new List<IInitializeSystem>();
        readonly List<IShutdownSystem> _shutdownSystems = new List<IShutdownSystem>();
        readonly List<IExecuteSystem> _executeSystems = new List<IExecuteSystem>();
        readonly List<ICleanupSystem> _cleanupSystems = new List<ICleanupSystem>();

        public void Add(ISystem system)
        {
            if (system is IInitializeSystem initializeSystem)
                _initializeSystems.Add(initializeSystem);
            if (system is IShutdownSystem shutdownSystem)
                _shutdownSystems.Add(shutdownSystem);
            if (system is IExecuteSystem executeSystem)
                _executeSystems.Add(executeSystem);
            if (system is ICleanupSystem cleanupSystem)
                _cleanupSystems.Add(cleanupSystem);
        }

        public void Initialize()
        {
            foreach (var system in _initializeSystems) system.Initialize();
        }

        public void Shutdown()
        {
            foreach (var system in _shutdownSystems) system.Shutdown();
        }

        public void Execute()
        {
            foreach (var system in _executeSystems) system.Execute();
        }

        public void Cleanup()
        {
            foreach (var system in _cleanupSystems) system.Cleanup();
        }
    }
}