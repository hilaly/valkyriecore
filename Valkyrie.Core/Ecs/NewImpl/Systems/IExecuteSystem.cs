namespace Valkyrie.Ecs
{
    public interface IExecuteSystem : ISystem
    {
        void Execute();
    }
}