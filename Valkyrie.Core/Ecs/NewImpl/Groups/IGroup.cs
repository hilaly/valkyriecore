using System.Collections.Generic;

namespace Valkyrie.Ecs
{
    public interface IGroup<T> : IEnumerable<T>
        where T : class, IEntity
    {
        int Count { get; }
        List<T> GetEntities(List<T> buffer);
    }
}