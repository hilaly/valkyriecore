using System;
using System.Collections;
using System.Collections.Generic;
using Valkyrie.Threading.Async;

namespace Valkyrie.Ecs
{
    class Group<T> : IGroup<T>, IDisposable
        where T : class, IEntity
    {
        private readonly IReadOnlyReactiveCollection<T> _entities;
        private readonly IMatcher<T> _matcher;
        private readonly List<T> _cached = new List<T>();
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly Dictionary<T, IDisposable> _addedEntitiesSubscriptions = new Dictionary<T, IDisposable>();
        private readonly Dictionary<T, IDisposable> _noAddedEntitiesSubscriptions = new Dictionary<T, IDisposable>();

        public Group(IReadOnlyReactiveCollection<T> entities, IMatcher<T> matcher)
        {
            _entities = entities;
            _matcher = matcher;

            Init();
        }

        public IEnumerator<T> GetEnumerator()
        {
            lock(_cached)
                return _cached.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count
        {
            get
            {
                lock (_cached)
                    return _cached.Count;
            }
        }

        public List<T> GetEntities(List<T> buffer)
        {
            buffer.Clear();
            lock(_cached)
                buffer.AddRange(_cached);
            return buffer;
        }

        private void Init()
        {
            _compositeDisposable.Add(_entities.ObserveAdd().Subscribe(OnEntityAdded));
            _compositeDisposable.Add(_entities.ObserveRemove().Subscribe(OnEntityRemoved));

            foreach (var entity in _entities)
            {
                if(_matcher.IsMatch(entity))
                    AddEntity(entity);
                else
                    ObserveEntity(entity);
            }
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
        }

        void OnEntityAdded(CollectionAddEvent<T> arg)
        {
            if (_matcher.IsMatch(arg.Value))
                AddEntity(arg.Value);
            else
                ObserveEntity(arg.Value);
        }

        void OnEntityRemoved(CollectionRemoveEvent<T> arg)
        {
            if (_matcher.IsMatch(arg.Value))
                RemoveEntity(arg.Value);
            else
                UnobserveEntity(arg.Value);
        }

        private void UnobserveEntity(T value)
        {
            IDisposable subscription;
            lock (_noAddedEntitiesSubscriptions)
            {
                subscription = _noAddedEntitiesSubscriptions[value];
                _noAddedEntitiesSubscriptions.Remove(value);
            }
            subscription.Dispose();
        }

        private void ObserveEntity(T value)
        {
            var entitySubscription = Disposable.Create(() =>
            {
                value.OnComponentAdded -= CheckNotAddedEntity;
                value.OnComponentRemoved -= CheckNotAddedEntity;
            });

            lock (_noAddedEntitiesSubscriptions)
                _noAddedEntitiesSubscriptions.Add(value, entitySubscription);

            value.OnComponentAdded += CheckNotAddedEntity;
            value.OnComponentRemoved += CheckNotAddedEntity;
        }

        private void AddEntity(T value)
        {
            lock(_cached)
                _cached.Add(value);

            SubscribeMatchedEntity(value);

            //TODO: call Entity added!!
        }

        private void SubscribeMatchedEntity(T value)
        {
            var entitySubscription = Disposable.Create(() =>
            {
                value.OnComponentAdded -= CheckAddedEntity;
                value.OnComponentRemoved -= CheckAddedEntity;
            });

            lock (_addedEntitiesSubscriptions)
                _addedEntitiesSubscriptions.Add(value, entitySubscription);

            value.OnComponentAdded += CheckAddedEntity;
            value.OnComponentRemoved += CheckAddedEntity;
        }

        private void CheckAddedEntity(int index, IEntity entity)
        {
            var value = (T) entity;
            if(_matcher.IsMatch(value))
                return;
            
            RemoveEntity(value);
            ObserveEntity(value);
        }

        private void CheckNotAddedEntity(int index, IEntity entity)
        {
            var value = (T) entity;
            if(!_matcher.IsMatch(value))
                return;

            UnobserveEntity(value);
            AddEntity(value);
        }

        void UnsubscribeMatchedEntity(T value)
        {
            IDisposable subscription;
            lock (_addedEntitiesSubscriptions)
            {
                subscription = _addedEntitiesSubscriptions[value];
                _addedEntitiesSubscriptions.Remove(value);
            }
            subscription.Dispose();
        }

        private void RemoveEntity(T value)
        {
            UnsubscribeMatchedEntity(value);
            
            //TODO: call entity removed
            
            lock (_cached)
                if (!_cached.Remove(value))
                    throw new Exception($"Error in group subscriptions");
        }
    }
}