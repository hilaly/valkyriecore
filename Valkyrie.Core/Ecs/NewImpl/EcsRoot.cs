namespace Valkyrie.Ecs
{
    public abstract class EcsRoot
    {
        private bool _initialized;
        
        public Systems Systems { get; } = new Systems();

        public void Initialize()
        {
            if(_initialized)
                return;

            _initialized = true;
            Systems.Initialize();
        }

        public void Execute()
        {
            if(!_initialized)
                Initialize();
            
            Systems.Execute();
            Systems.Cleanup();
        }

        public void Shutdown()
        {
            if(_initialized)
                Systems.Shutdown();
            _initialized = false;
        }
    }
}