namespace Valkyrie.Ecs
{
    public interface IMatcher<in T>
        where T : class, IEntity
    {
        bool IsMatch(T entity);

        int[] Indices { get; }
    }
}