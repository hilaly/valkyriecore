using System.Collections.Generic;

namespace Valkyrie.Ecs
{
    class AndMatcher<T> : IAllOfMatcher<T> where T : class, IEntity
    {
        readonly List<IMatcher<T>> _matchers = new List<IMatcher<T>>();

        public bool IsMatch(T entity)
        {
            for (int i = 0; i < _matchers.Count; i++)
                if (!_matchers[i].IsMatch(entity))
                    return false;

            return true;
        }

        public override int GetHashCode()
        {
            var result = 0;
            for (var index = _matchers.Count - 1; index >= 0; index--)
            {
                var matcher = _matchers[index];
                result |= (matcher.GetHashCode()) << (index * 8);
            }

            return result;
        }

        public int[] Indices { get; } = new int[0];

        public IMatcher<T> NoneOf(params IMatcher<T>[] matchers)
        {
            _matchers.Add(new NoneOfMatcher<T>(matchers));
            return this;
        }

        public IMatcher<T> NoneOf(params int[] indices)
        {
            _matchers.Add(new NoneOfMatcher<T>(indices));
            return this;
        }

        public IAnyOfMatcher<T> AnyOf(params IMatcher<T>[] matchers)
        {
            _matchers.Add(new AnyOfMatcher<T>(matchers));
            return this;
        }

        public IAnyOfMatcher<T> AnyOf(params int[] indices)
        {
            _matchers.Add(new AnyOfMatcher<T>(indices));
            return this;
        }

        public IAllOfMatcher<T> AllOf(params IMatcher<T>[] matchers)
        {
            _matchers.Add(new AllOfMatcher<T>(matchers));
            return this;
        }

        public IAllOfMatcher<T> AllOf(params int[] indices)
        {
            _matchers.Add(new AllOfMatcher<T>(indices));
            return this;
        }
    }
}