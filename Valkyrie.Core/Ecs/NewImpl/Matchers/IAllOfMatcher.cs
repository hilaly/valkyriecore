namespace Valkyrie.Ecs
{
    public interface IAllOfMatcher<T> : IAnyOfMatcher<T> where T : class, IEntity
    {
        public IAnyOfMatcher<T> AnyOf(params IMatcher<T>[] matchers);
        public IAnyOfMatcher<T> AnyOf(params int[] indices);
    }
}