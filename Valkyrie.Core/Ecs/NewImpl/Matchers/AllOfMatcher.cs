using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Ecs
{
    class AllOfMatcher<T> : IMatcher<T> where T : class, IEntity
    {
        private readonly int[] _indices;

        public override int GetHashCode()
        {
            var result = 0;
            for (var index = _indices.Length - 1; index >= 0; --index)
                result ^= _indices[index] << index;
            return result;
        }

        public AllOfMatcher(params int[] indices)
        {
            _indices = indices.OrderBy(x => x).ToArray();
        }

        public AllOfMatcher(params IMatcher<T>[] matchers)
        {
            var temp = new HashSet<int>();
            foreach (var matcher in matchers)
            foreach (var index in matcher.Indices)
                temp.Add(index);
            _indices = temp.OrderBy(x => x).ToArray();
        }

        public bool IsMatch(T entity)
        {
            return entity.HasComponents(_indices);
        }

        public int[] Indices => _indices;
    }
}