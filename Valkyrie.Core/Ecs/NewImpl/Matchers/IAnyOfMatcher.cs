namespace Valkyrie.Ecs
{
    public interface IAnyOfMatcher<T> : IMatcher<T> where T : class, IEntity
    {
        public IMatcher<T> NoneOf(params IMatcher<T>[] matchers);
        public IMatcher<T> NoneOf(params int[] indices);
    }
}