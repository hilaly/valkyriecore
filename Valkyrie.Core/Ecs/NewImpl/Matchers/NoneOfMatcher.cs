using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Ecs
{
    class NoneOfMatcher<T> : IMatcher<T> where T : class, IEntity
    {
        private readonly int[] _indices;

        public override int GetHashCode()
        {
            var result = 0;
            for (var index = _indices.Length - 1; index >= 0; --index)
                result ^= _indices[index] << index;
            return result;
        }

        public NoneOfMatcher(params int[] indices)
        {
            _indices = indices.OrderBy(x => x).ToArray();
        }

        public NoneOfMatcher(IMatcher<T>[] matchers)
        {
            var temp = new HashSet<int>();
            for (var index1 = 0; index1 < matchers.Length; index1++)
            {
                var matcher = matchers[index1];
                for (var i = 0; i < matcher.Indices.Length; i++)
                {
                    var index = matcher.Indices[i];
                    temp.Add(index);
                }
            }

            _indices = temp.OrderBy(x => x).ToArray();
        }

        public bool IsMatch(T entity)
        {
            for (var i = 0; i < _indices.Length; ++i)
                if (entity.HasComponent(_indices[i]))
                    return false;
            return true;
        }

        public int[] Indices => _indices;
    }
}