namespace Valkyrie.Ecs
{
    public static class Matcher<T>
        where T : class, IEntity
    {
        public static IAllOfMatcher<T> AllOf(params int[] indices)
        {
            return new AndMatcher<T>().AllOf(indices);
        }

        public static IAllOfMatcher<T> AllOf(params IMatcher<T>[] matchers)
        {
            return new AndMatcher<T>().AllOf(matchers);
        }

        public static IAnyOfMatcher<T> AnyOf(params int[] indices)
        {
            return new AndMatcher<T>().AnyOf(indices);
        }

        public static IAnyOfMatcher<T> AnyOf(params IMatcher<T>[] matchers)
        {
            return new AndMatcher<T>().AnyOf(matchers);
        }

        public static IMatcher<T> NoneOf(params int[] indices)
        {
            return new AndMatcher<T>().NoneOf(indices);
        }

        public static IMatcher<T> NoneOf(params IMatcher<T>[] matchers)
        {
            return new AndMatcher<T>().NoneOf(matchers);
        }

        public static IMatcher<T> ComponentMatcher(int index)
        {
            return new AllOfMatcher<T>(index);
        }
    }
}