﻿using System;

namespace Valkyrie.Threading.Scheduler
{
    public interface IExecutor : IDisposable
    {
        void Run(int frameRate);
        
        void Iterate();
        void PostIterate();
    }
}
