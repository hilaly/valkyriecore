﻿using System;
using System.Diagnostics;
using System.Threading;
using Valkyrie.Runtime.Console;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Time;

namespace Valkyrie.Threading.Scheduler
{
    [DeveloperConsoleCommand(Alias = "stop")]
    class Executor : IExecutor
    {
        private readonly IDeveloperConsole _console;
        
        private readonly Dispatcher _dispatcher;
        private readonly TimeService _timeSystem;

        readonly CompositeDisposable _disposables = new CompositeDisposable();
        private bool _worked;
        private bool _paused;

        public Executor(TimeService timeSystem, Dispatcher taskManager, IDeveloperConsole console)
        {
            _timeSystem = timeSystem;
            _dispatcher = taskManager;
            _console = console;
            
            _console.RegisterCommand(this);
        }

        public void Dispose()
        {
            _disposables.Dispose();
            _worked = false;
        }

        public void Run(int frameRate)
        {
            if(_console != null)
                _dispatcher.ExecuteOnPoolThread(ConsoleReadTask);

            var iterationLength = (long)(1000f / frameRate);
            
            var sw = new Stopwatch();
            _worked = true;
            while (_worked)
            {
                sw.Start();
                if (!_paused)
                {
                    Iterate();
                    PostIterate();   
                }
                sw.Stop();
                var ms = sw.ElapsedMilliseconds;
                sw.Reset();
                if (ms < iterationLength)
                    Thread.Sleep((int) (iterationLength - ms));
            }
        }

        private void ConsoleReadTask()
        {
            if(_console == null)
                return;
            while (true)
            {
                try
                {
                    _console.Execute(Console.ReadLine());
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public void Iterate()
        {
            try
            {
                _timeSystem.Update();
                _dispatcher.Update();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void PostIterate()
        {
            try
            {
                _dispatcher.PostUpdate();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [DeveloperConsoleCommand(Usage = "type 'stop' for stop execution",
            Description = "Stops execution of engine and quit")]
        string InvokeStop()
        {
            if (_worked == false)
                return "Engine is not started";
            _worked = false;
            return "Engine stopped";
        }

        [DeveloperConsoleCommand(Usage = "type 'pause' to pause engine iterating", Description = "Pause engine", Alias = "pause")]
        string InvokePause()
        {
            if (_paused)
                return string.Empty;
            _paused = true;
            return "Engine execution paused";
        }

        [DeveloperConsoleCommand(Usage = "type 'pause' to pause engine iterating", Description = "Pause engine", Alias = "resume")]
        string InvokeResume()
        {
            if (!_paused)
                return string.Empty;
            _paused = false;
            return "Engine execution resumed";
        }
    }
}