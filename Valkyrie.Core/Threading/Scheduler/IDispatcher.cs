﻿using System;
using Valkyrie.Threading.Async;

namespace Valkyrie.Threading.Scheduler
{
    public interface IDispatcher
    {
        IObservable ExecuteOnMainThread(Action action);
        IObservable ExecuteOnAnyThread(Action action);
        IObservable ExecuteOnPoolThread(Action action);

        IDisposable EveryUpdate(Action action);
        IDisposable EveryPostUpdate(Action action);
        IDisposable EveryEndIteration(Action action);
        
        void WaitAll();
    }
}