﻿using System;
using System.Collections.Generic;
using System.Threading;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Threading.Scheduler
{
    public class Scheduler : IDisposable
    {
        readonly List<IDisposable> _disposables = new List<IDisposable>();

        public void Dispose()
        {
            lock (_disposables)
            {
                _disposables.ForEach(u => u.Dispose());
                _disposables.Clear();
            }
        }

        // public async Task Run(Action action)
        // {
        //     await Task.Run(action);
        // }

        public void Run(Action action, int delayMilliseconds)
        {
            var timer = new System.Timers.Timer()
            {
                AutoReset = false,
                Enabled = true,
                Interval = delayMilliseconds
            };
            void StopTimer()
            {
                timer.Dispose();
                action();
            }
            timer.Elapsed += (s, e) => StopTimer();
        }

        public void Run(Action action, TimeSpan delay, TimeSpan period)
        {
            _disposables.Add(new System.Threading.Timer((s) => action(), null, delay, period));
        }
    }
    
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class Dispatcher : IDispatcher
    {
        private readonly List<ExecuteArg> _anyThreadArgs = new List<ExecuteArg>();
        private readonly List<ExecuteArg> _mainThreadArgs = new List<ExecuteArg>();
        private readonly Subject _updateSubject = new Subject();
        private readonly Subject _postUpdateSubject = new Subject();
        private readonly Subject _endIterationSubject = new Subject();
        private readonly int _mainThreadId;
        private readonly ILogInstance _log;

        public Dispatcher(MetaInfo applicationInfo, ILogService logService)
        {
            _mainThreadId = Thread.CurrentThread.ManagedThreadId;

            ThreadPool.SetMaxThreads(applicationInfo.BackgroundThreadsCount, 0);

            _log = logService.GetLog("Dispatcher");
            _updateSubject.Catch((ex) =>
            {
                _log.Error(ex.ToString());
                throw ex;
            });
            _postUpdateSubject.Catch((ex) =>
            {
                _log.Error(ex.ToString());
                throw ex;
            });
            _endIterationSubject.Catch((ex) =>
            {
                _log.Error(ex.ToString());
                throw ex;
            });
        }

        public IObservable ExecuteOnMainThread(Action action)
        {
            return Create(action, executeArg =>
            {
                lock (_mainThreadArgs)
                    _mainThreadArgs.Add(executeArg);
            });
        }

        public IObservable ExecuteOnAnyThread(Action action)
        {
            return Create(action, executeArg =>
            {
                lock (_anyThreadArgs)
                    _anyThreadArgs.Add(executeArg);
            });
        }

        public IObservable ExecuteOnPoolThread(Action action)
        {
            return Create(action, executeArg => { ThreadPool.QueueUserWorkItem(HandleAction, executeArg); });
        }

        IObservable Create(Action action, Action<ExecuteArg> handleCreationAction)
        {
            var result = new ExecuteArg
            {
                Subject = new Subject {CompleteOnError = true},
                Action = action,
                Log = _log
            };
            handleCreationAction(result);
            return result.Subject;
        }

        public IDisposable EveryUpdate(Action action)
        {
            return _updateSubject.Subscribe(action);
        }

        public IDisposable EveryPostUpdate(Action action)
        {
            return _postUpdateSubject.Subscribe(action);
        }

        public IDisposable EveryEndIteration(Action action)
        {
            return _endIterationSubject.Subscribe(action);
        }

        private static void HandleAction(object arg)
        {
            ((ExecuteArg) arg).Invoke();
        }

        private class ExecuteArg
        {
            public Subject Subject;
            public Action Action;
            public ILogInstance Log;

            public void Invoke()
            {
                try
                {
                    Action();
                    Subject.OnCompleted();
                }
                catch (Exception e)
                {
                    Log.Error(e.ToString());
                    Subject.OnError(e);
                    throw;
                }
            }
        }

        ExecuteArg PickArg()
        {
            if (Thread.CurrentThread.ManagedThreadId == _mainThreadId)
            {
                lock(_mainThreadArgs)
                    if (_mainThreadArgs.Count > 0)
                    {
                        var result = _mainThreadArgs[0];
                        _mainThreadArgs.RemoveAt(0);
                        return result;
                    }
            }
            
            lock(_anyThreadArgs)
                if (_anyThreadArgs.Count > 0)
                {
                    var result = _anyThreadArgs[0];
                    _anyThreadArgs.RemoveAt(0);
                    return result;
                }

            return null;
        }

        void IDispatcher.WaitAll()
        {
            var arg = PickArg();
            while (arg != null)
            {
                arg.Invoke();
                arg = PickArg();
            }
        }

        public void Update()
        {
            try
            {
                _updateSubject.OnNext();
            }
            catch (Exception e)
            {
                _log.Error($"Exception during update: {e}");
            }

            try
            {
                ((IDispatcher)this).WaitAll();
            }
            catch (Exception e)
            {
                _log.Error($"Exception during WaitAll: {e}");
            }

            try
            {
                _postUpdateSubject.OnNext();
            }
            catch (Exception e)
            {
                _log.Error($"Exception during post update: {e}");
            }
        }

        public void PostUpdate()
        {
            _endIterationSubject.OnNext();
        }
    }
}