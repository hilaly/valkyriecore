﻿namespace Valkyrie.Threading.Async
{
    public struct DictionaryAddEvent<TKey, TValue>
    {
        public DictionaryAddEvent(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; private set; }
        public TValue Value { get; private set; }
    }
}