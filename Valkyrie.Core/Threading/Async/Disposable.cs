using System;

namespace Valkyrie.Threading.Async
{
    public static class Disposable
    {
        public static readonly IDisposable Empty = EmptyDisposable.Singleton;

        public static IDisposable Create(Action disposeAction)
        {
            return new AnonymousDisposable(disposeAction);
        }

        public static IDisposable Create<TState>(TState state, Action<TState> disposeAction)
        {
            return new AnonymousDisposable<TState>(state, disposeAction);
        }

        class EmptyDisposable : IDisposable
        {
            public static readonly EmptyDisposable Singleton = new EmptyDisposable();

            private EmptyDisposable()
            {

            }

            public void Dispose()
            {
            }
        }

        class AnonymousDisposable : IDisposable
        {
            bool _isDisposed;
            readonly Action _dispose;

            public AnonymousDisposable(Action dispose)
            {
                _dispose = dispose;
            }

            public void Dispose()
            {
                if (!_isDisposed)
                {
                    _isDisposed = true;
                    _dispose();
                }
            }
        }

        class AnonymousDisposable<T> : IDisposable
        {
            bool _isDisposed;
            readonly T _state;
            readonly Action<T> _dispose;

            public AnonymousDisposable(T state, Action<T> dispose)
            {
                _state = state;
                _dispose = dispose;
            }

            public void Dispose()
            {
                if (!_isDisposed)
                {
                    _isDisposed = true;
                    _dispose(_state);
                }
            }
        }
    }
}