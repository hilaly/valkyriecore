﻿namespace Valkyrie.Threading.Async
{
    interface IComplexObserver<T> : System.IObserver<T>
    {
        void Add(System.IObserver<T> observer);
        void Remove(System.IObserver<T> observer);
        
        int Count { get; }
    }
    interface IComplexObserver : IObserver
    {
        void Add(IObserver observer);
        void Remove(IObserver observer);
        
        int Count { get; }
    }
}