﻿using System;

namespace Valkyrie.Threading.Async
{
    public interface IObservable
    {
        IDisposable Subscribe(IObserver observable);
    }
}