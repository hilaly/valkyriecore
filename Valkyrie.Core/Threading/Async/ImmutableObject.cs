﻿using System;

namespace Valkyrie.Threading.Async
{
    public class ImmutableObject : IEquatable<ImmutableObject>
    {
        public static ImmutableObject Default { get; } = new ImmutableObject();

        public static bool operator ==(ImmutableObject first, ImmutableObject second)
        {
            return true;
        }

        public static bool operator !=(ImmutableObject first, ImmutableObject second)
        {
            return false;
        }

        public bool Equals(ImmutableObject other)
        {
            return true;
        }
        public override bool Equals(object obj)
        {
            return obj is ImmutableObject;
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public override string ToString()
        {
            return "()";
        }
    }
}