﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Valkyrie.Threading.Async
{
    // IReadOnlyList<out T> is from .NET 4.5

    [Serializable]
    public class ReactiveCollection<T> : IReactiveCollection<T>, IDisposable
    {
        private bool _isDisposed;
        private readonly List<T> _innerList;

        private readonly Subject<CollectionAddEvent<T>> _addSubject =
            new Subject<CollectionAddEvent<T>> {CompleteOnError = false};
        private readonly Subject<CollectionRemoveEvent<T>> _removeSubject = new Subject<CollectionRemoveEvent<T>> {CompleteOnError = false};

        #region Ctor

        public ReactiveCollection()
        {
            _innerList = new List<T>();
        }

        public ReactiveCollection(int capacity, bool fillDefault = false)
        {
            _innerList = new List<T>(capacity);
            if(fillDefault)
                for(var i = 0; i < capacity; ++i)
                    _innerList.Add(default(T));
        }

        public ReactiveCollection(IEnumerable<T> sourceEnumerable)
        {
            _innerList = new List<T>(sourceEnumerable);
        }

        #endregion

        #region IEnumerable

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        #endregion

        #region IReadonlyReactiveCollecion

        public int Count
        {
            get { return _innerList.Count; }
        }

        public T this[int index]
        {
            get { return _innerList[index]; }
            set
            {
                var old = _innerList[index];
                _innerList[index] = value;
                
                _removeSubject.OnNext(new CollectionRemoveEvent<T>(index, old));
                _addSubject.OnNext(new CollectionAddEvent<T>(index, value));
            }
        }

        public IObservable<CollectionAddEvent<T>> ObserveAdd()
        {
            return _addSubject;
        }

        public IObservable<int> ObserveCountChanged(bool notifyCurrentCount = false)
        {
            throw new NotImplementedException();
        }

        public IObservable<CollectionMoveEvent<T>> ObserveMove()
        {
            throw new NotImplementedException();
        }

        public IObservable<CollectionRemoveEvent<T>> ObserveRemove()
        {
            return _removeSubject;
        }

        public IObservable<CollectionReplaceEvent<T>> ObserveReplace()
        {
            throw new NotImplementedException();
        }

        public IObservable ObserveReset()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IReactiveCollection

        public void Add(T item)
        {
            _innerList.Add(item);
            _addSubject.OnNext(new CollectionAddEvent<T>(_innerList.Count - 1, item));
        }

        public void Clear()
        {
            var values = _innerList.ToArray();
            _innerList.Clear();
            for (var i = values.Length - 1; i >= 0; --i)
                _removeSubject.OnNext(new CollectionRemoveEvent<T>(i, values[i]));
        }

        public bool Contains(T item)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _innerList.CopyTo(array, arrayIndex);
        }

        public bool IsReadOnly { get; }

        public bool Remove(T item)
        {
            var result = _innerList.Remove(item);
            if(result)
                _removeSubject.OnNext(new CollectionRemoveEvent<T>(-1, item));
            return result;
        }

        public int IndexOf(T item)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            var item = _innerList[index];
            _innerList.RemoveAt(index);
            _removeSubject.OnNext(new CollectionRemoveEvent<T>(index, item));
        }

        public void Move(int oldIndex, int newIndex)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            if(_isDisposed)
                return;
            
            _isDisposed = true;
            _addSubject.Dispose();
            _removeSubject.Dispose();
        }

        #endregion
    }
}