﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Threading.Async
{
    public interface IReadOnlyReactiveCollection<T> : IEnumerable<T>
    {
        int Count { get; }
        T this[int index] { get; }
        IObservable<CollectionAddEvent<T>> ObserveAdd();
        IObservable<int> ObserveCountChanged(bool notifyCurrentCount = false);
        IObservable<CollectionMoveEvent<T>> ObserveMove();
        IObservable<CollectionRemoveEvent<T>> ObserveRemove();
        IObservable<CollectionReplaceEvent<T>> ObserveReplace();
        IObservable ObserveReset();
    }
}