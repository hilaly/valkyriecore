﻿using System;

namespace Valkyrie.Threading.Async
{
    public class Subject : ISubject, IDisposable
    {
        private readonly object _lock = new object(); 
        private readonly IComplexObserver _observer = new ListObserver();
        private bool _isDisposed;
        private bool _isCompleted;
        private Exception _lastError;

        public bool CompleteOnError { get; set; } = true;

        public IDisposable Subscribe(IObserver observable)
        {
            if (observable == null)
                throw new ArgumentNullException("observable");

            Exception ex;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (!_isCompleted)
                {
                    _observer.Add(observable);
                    return new Subscription(this, observable);
                }

                ex = _lastError;
            }

            if (ex != null)
                observable.OnError(ex);
            else
                observable.OnCompleted();
            
            return Disposable.Empty;
        }

        public void OnNext()
        {
            lock (_lock)
            {
                ThrowIfDisposed();
                if(_isCompleted)
                    return;
                _observer.OnNext();
            }
        }

        public void OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");

            lock (_lock)
            {
                ThrowIfDisposed();
                if(_isCompleted)
                    return;

                if(CompleteOnError)
                    _isCompleted = true;
                _lastError = error;
                _observer.OnError(error);
            }
        }

        public void OnCompleted()
        {
            lock (_lock)
            {
                ThrowIfDisposed();
                if(_isCompleted)
                    return;

                _isCompleted = true;
                _observer.OnCompleted();
            }
        }

        public void Dispose()
        {
            lock (_lock)
            {
                ThrowIfDisposed();
                _isDisposed = true;
            }
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException("Already disposed");
        }

        class Subscription : IDisposable
        {
            private readonly Subject _subject;
            private readonly IObserver _observer;

            private bool _disposed;

            public Subscription(Subject subject, IObserver observer)
            {
                _subject = subject;
                _observer = observer;
            }

            public void Dispose()
            {
                if (_disposed)
                    throw new ObjectDisposedException("Already disposed");

                _subject._observer.Remove(_observer);
                _disposed = true;
            }
        }
        
    }
    
    public class Subject<T> : ISubject<T>, IDisposable
    {
        private readonly object _lock = new object(); 
        private readonly IComplexObserver<T> _observer = new ListObserver<T>();
        private bool _isDisposed;
        private bool _isCompleted;
        private Exception _lastError;

        public bool CompleteOnError { get; set; } = true;

        public IDisposable Subscribe(IObserver<T> observable)
        {
            if (observable == null)
                throw new ArgumentNullException("observable");

            Exception ex;
            lock (_lock)
            {
                ThrowIfDisposed();
                if (!_isCompleted)
                {
                    _observer.Add(observable);
                    return new Subscription(this, observable);
                }

                ex = _lastError;
            }

            if (ex != null)
                observable.OnError(ex);
            else
                observable.OnCompleted();
            
            return Disposable.Empty;
        }

        public void OnNext(T item)
        {
            lock (_lock)
            {
                ThrowIfDisposed();
                if(_isCompleted)
                    return;
                
                ThrowIfDisposed();
                _observer.OnNext(item);
            }
        }

        public void OnError(Exception error)
        {
            if (error == null)
                throw new ArgumentNullException("error");

            lock (_lock)
            {
                ThrowIfDisposed();
                if(_isCompleted)
                    return;

                if(CompleteOnError)
                    _isCompleted = true;
                _lastError = error;
                _observer.OnError(error);
            }
        }

        public void OnCompleted()
        {
            lock (_lock)
            {
                ThrowIfDisposed();
                if(_isCompleted)
                    return;

                _isCompleted = true;
                _observer.OnCompleted();
            }
        }

        public void Dispose()
        {
            lock (_lock)
            {
                ThrowIfDisposed();
                _isDisposed = true;
            }
        }

        private void ThrowIfDisposed()
        {
            if (_isDisposed)
                throw new ObjectDisposedException("Already disposed");
        }

        class Subscription : IDisposable
        {
            private readonly Subject<T> _subject;
            private readonly IObserver<T> _observer;

            private bool _disposed;

            public Subscription(Subject<T> subject, IObserver<T> observer)
            {
                _subject = subject;
                _observer = observer;
            }

            public void Dispose()
            {
                if (_disposed)
                    throw new ObjectDisposedException("Already disposed");

                _subject._observer.Remove(_observer);
                _disposed = true;
            }
        }
    }
}