﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Threading.Async
{
    public static class AsyncExtensions
    {
        #region Subscribe
        
        public static IDisposable Subscribe<T>(this IObservable<T> observable, Action<T> onValue, Action<Exception> onError, Action onComplete)
        {
            return observable.Subscribe(Observer.Create<T>(onValue, onError, onComplete));
        }
        public static IDisposable Subscribe<T>(this IObservable<T> observable, Action<T> onValue)
        {
            return observable.Subscribe(onValue, null, null);
        }
        public static IDisposable Subscribe<T>(this IObservable<T> observable, Action<T> onValue, Action onComplete)
        {
            return observable.Subscribe(onValue, null, onComplete);
        }
        public static IDisposable Subscribe<T>(this IObservable<T> observable, Action<T> onValue, Action<Exception> onError)
        {
            return observable.Subscribe(onValue, onError, null);
        }
        public static IDisposable Subscribe(this IObservable observable, Action onValue, Action<Exception> onError, Action onComplete)
        {
            return observable.Subscribe(Observer.Create(onValue, onError, onComplete));
        }
        public static IDisposable Subscribe(this IObservable observable, Action onValue)
        {
            return observable.Subscribe(onValue, null, null);
        }
        public static IDisposable Subscribe(this IObservable observable, Action onValue, Action onComplete)
        {
            return observable.Subscribe(onValue, null, onComplete);
        }
        public static IDisposable Subscribe(this IObservable observable, Action onValue, Action<Exception> onError)
        {
            return observable.Subscribe(onValue, onError, null);
        }

        public static IDisposable Catch(this IObservable observable, Action<Exception> onError)
        {
            return observable.Subscribe(null, onError, null);
        }
        public static IDisposable Catch<T>(this IObservable<T> observable, Action<Exception> onError)
        {
            return observable.Subscribe(null, onError, null);
        }
        
        #endregion
        
        #region Where

        public static IObservable<T> Where<T>(this IObservable<T> observable, Func<T, bool> valueFilter,
            Func<Exception, bool> errorFilter)
        {
            return Observer.CreateWhere(observable, valueFilter, errorFilter);
        }

        public static IObservable<T> Where<T>(this IObservable<T> observable, Func<T, bool> valueFilter)
        {
            return observable.Where(valueFilter, null);
        }

        public static IObservable<T> Where<T>(this IObservable<T> observable, Func<Exception, bool> errorFilter)
        {
            return observable.Where(null, errorFilter);
        }
        
        #endregion
        
        public static IDisposable Chain<T>(this IObservable<T> observable, Action onComplete)
        {
            return observable.Subscribe(Observer.Create<T>(null, null, onComplete));
        }
        public static IDisposable Chain(this IObservable observable, Action onComplete)
        {
            return observable.Subscribe(Observer.Create(null, null, onComplete));
        }
        
        #region Promise like
        
        

        #endregion
        
        #region Collections
        
        public static ReactiveCollection<T> ToReactiveCollection<T>(this IEnumerable<T> source)
        {
            return new ReactiveCollection<T>(source);
        }
        
        #endregion
    }
}