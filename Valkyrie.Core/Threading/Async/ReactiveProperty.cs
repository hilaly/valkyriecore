﻿using System;

namespace Valkyrie.Threading.Async
{
    [Serializable]
    public class ReactiveProperty<T> : IReactiveProperty<T>, IDisposable
    {
        private T _value;
        [NonSerialized]
        private readonly Subject<T> _subject = new Subject<T>();

        public ReactiveProperty() : this(default(T))
        {
        }
        public ReactiveProperty(T value)
        {
            _value = value;
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return _subject.Subscribe(observer);
        }

        T IReadOnlyReactiveProperty<T>.Value => _value;

        public T Value
        {
            get => _value;
            set
            {
                _value = value;
                _subject.OnNext(_value);
            }
        }
        public bool HasValue => _value.Equals(default(T));

        public void Dispose()
        {
            _subject.Dispose();
        }

        public static implicit operator T(ReactiveProperty<T> d)
        {
            return d._value;
        }
    }
}
