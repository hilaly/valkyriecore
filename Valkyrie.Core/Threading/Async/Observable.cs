﻿using System;

namespace Valkyrie.Threading.Async
{
    public static class Observable
    {
        public static IObservable Completed()
        {
            var result = new Subject();
            result.OnCompleted();
            return result;
        }
        public static IObservable Error(Exception e)
        {
            var result = new Subject();
            result.OnError(e);
            return result;
        }

        public static IObservable<T> Completed<T>()
        {
            var result = new Subject<T>();
            result.OnCompleted();
            return result;
        }
        public static IObservable<T> Error<T>(Exception e)
        {
            var result = new Subject<T>();
            result.OnError(e);
            return result;
        }
    }
}