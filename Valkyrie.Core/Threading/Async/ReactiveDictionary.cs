﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Valkyrie.Threading.Async
{
    public class ReactiveDictionary<TKey, TValue> : IReadOnlyReactiveDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _dictionaryImplementation = new Dictionary<TKey, TValue>();

        private readonly Subject<DictionaryAddEvent<TKey, TValue>> _addSubject =
            new Subject<DictionaryAddEvent<TKey, TValue>> {CompleteOnError = false};

        private readonly Subject<DictionaryRemoveEvent<TKey, TValue>> _removeSubject =
            new Subject<DictionaryRemoveEvent<TKey, TValue>> {CompleteOnError = false};

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dictionaryImplementation.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _dictionaryImplementation).GetEnumerator();
        }

        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            Add(item.Key, item.Value);
        }

        public void Clear()
        {
            _dictionaryImplementation.Clear();
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            return ContainsKey(item.Key);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        public int Count => _dictionaryImplementation.Count;

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool ContainsKey(TKey key)
        {
            return _dictionaryImplementation.ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            _dictionaryImplementation.Add(key, value);

            _addSubject.OnNext(new DictionaryAddEvent<TKey, TValue>(key, value));
        }

        public bool Remove(TKey key)
        {
            if (_dictionaryImplementation.ContainsKey(key))
            {
                var value = _dictionaryImplementation[key];
                _dictionaryImplementation.Remove(key);

                _removeSubject.OnNext(new DictionaryRemoveEvent<TKey, TValue>(key, value));
            }

            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _dictionaryImplementation.TryGetValue(key, out value);
        }

        public TValue this[TKey key]
        {
            get => _dictionaryImplementation[key];
            set => _dictionaryImplementation[key] = value;
        }

        public ICollection<TKey> Keys => _dictionaryImplementation.Keys;

        public ICollection<TValue> Values => _dictionaryImplementation.Values;

        public IObservable<DictionaryAddEvent<TKey, TValue>> ObserveAdd()
        {
            return _addSubject;
        }

        public IObservable<DictionaryRemoveEvent<TKey, TValue>> ObserveRemove()
        {
            return _removeSubject;
        }
    }
}