﻿using System;

namespace Valkyrie.Threading.Async
{
    public interface IObserver
    {
        void OnNext();
        void OnError(Exception error);
        void OnCompleted();
    }
}