﻿using System;

namespace Valkyrie.Threading.Async
{
    public interface IReadOnlyReactiveProperty<T> : IObservable<T>
    {
        T Value { get; }
        bool HasValue { get; }
    }
}