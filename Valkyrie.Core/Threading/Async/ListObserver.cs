﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Threading.Async
{
    class ListObserver<T> : IComplexObserver<T>
    {
        readonly List<IObserver<T>> _observers = new List<IObserver<T>>();
        readonly List<IObserver<T>> _cache = new List<IObserver<T>>();
        private bool _needUpdate = true;

        public int Count => _observers.Count;

        List<IObserver<T>> GetEnumerableCollection()
        {
            if (_needUpdate)
            {
                _cache.Clear();
                _cache.AddRange(_observers);
                _needUpdate = false;
            }

            return _cache;
        }
        
        public void OnNext(T item)
        {
            lock (_observers)
            {
                foreach (var observer in GetEnumerableCollection())
                    observer.OnNext(item);
            }
        }

        public void OnError(Exception error)
        {
            lock (_observers)
            {
                foreach (var observer in GetEnumerableCollection())
                    observer.OnError(error);
            }
        }

        public void OnCompleted()
        {
            lock (_observers)
            {
                foreach (var observer in GetEnumerableCollection())
                    observer.OnCompleted();
            }
        }

        public void Add(IObserver<T> observer)
        {
            lock (_observers)
            {
                _observers.Add(observer);
                _needUpdate = true;
            }
        }

        public void Remove(IObserver<T> observer)
        {
            lock (_observers)
            {
                _observers.Remove(observer);
                _needUpdate = true;
            }
        }
    }
    class ListObserver : IComplexObserver
    {
        readonly List<IObserver> _observers = new List<IObserver>();
        readonly List<IObserver> _cache = new List<IObserver>();
        private bool _needUpdate = true;

        public int Count => _observers.Count;

        List<IObserver> GetEnumerableCollection()
        {
            if (_needUpdate)
            {
                _cache.Clear();
                _cache.AddRange(_observers);
                _needUpdate = false;
            }

            return _cache;
        }
        
        public void OnNext()
        {
            lock (_observers)
            {
                foreach (var observer in GetEnumerableCollection())
                    observer.OnNext();
            }
        }

        public void OnError(Exception error)
        {
            lock (_observers)
            {
                foreach (var observer in GetEnumerableCollection())
                    observer.OnError(error);
            }
        }

        public void OnCompleted()
        {
            lock (_observers)
            {
                foreach (var observer in GetEnumerableCollection())
                    observer.OnCompleted();
            }
        }

        public void Add(IObserver observer)
        {
            lock (_observers)
            {
                _observers.Add(observer);
                _needUpdate = true;
            }
        }

        public void Remove(IObserver observer)
        {
            lock (_observers)
            {
                _observers.Remove(observer);
                _needUpdate = true;
            }
        }
    }
}