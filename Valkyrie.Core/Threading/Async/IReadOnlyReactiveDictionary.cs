﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Threading.Async
{
    public interface IReadOnlyReactiveDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        IObservable<DictionaryAddEvent<TKey, TValue>> ObserveAdd();
        IObservable<DictionaryRemoveEvent<TKey, TValue>> ObserveRemove();
    }
}