﻿namespace Valkyrie.Threading.Async
{
    public struct DictionaryRemoveEvent<TKey, TValue>
    {
        public DictionaryRemoveEvent(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }

        public TKey Key { get; private set; }
        public TValue Value { get; private set; }
    }
}