﻿using System;

namespace Valkyrie.Threading.Async
{
    public static class Observer
    {
        public static ISubject<T> Create<T>(Action<T> onValue, Action<Exception> onError, Action onComplete)
        {
            return new ActionCallSubject<T>(onValue, onError, onComplete);
        }
        public static ISubject Create(Action onValue, Action<Exception> onError, Action onComplete)
        {
            return new ActionCallSubject(onValue, onError, onComplete);
        }

        public static ISubject<T> CreateWhere<T>(IObservable<T> observable, Func<T, bool> itemSelector,
            Func<Exception, bool> errorSelector)
        {
            return new WhereObservable<T>(observable, itemSelector, errorSelector);
        }

        class WhereObservable<T> : ISubject<T>, IDisposable
        {
            readonly CompositeDisposable _disposeAction = new CompositeDisposable();
            private readonly Func<T, bool> _itemSelector;
            private readonly Func<Exception, bool> _errorSelector;
            private readonly IComplexObserver<T> _outObserver;
            private bool _isCompleted;
            private bool _isDisposed;

            public WhereObservable(IObservable<T> source, Func<T, bool> itemSelector, Func<Exception, bool> errorSelector)
            {
                _itemSelector = itemSelector;
                _errorSelector = errorSelector;
                _disposeAction.Add(source.Subscribe(this));
                _outObserver = new ListObserver<T>();
            }

            void ThrowIfDisposed()
            {
                if(_isDisposed)
                    throw new ObjectDisposedException("WhereObservable");
            }

            public IDisposable Subscribe(IObserver<T> observable)
            {
                ThrowIfDisposed();
                
                if (_isCompleted)
                {
                    observable.OnCompleted();
                    return Disposable.Empty;
                }
                else
                {
                    _outObserver.Add(observable);
                    return Disposable.Create(() =>
                    {
                        _outObserver.Remove(observable);
                        if(_outObserver.Count == 0)
                            Dispose();
                    });
                }
            }

            public void OnNext(T item)
            {
                ThrowIfDisposed();

                if (_itemSelector == null || _itemSelector(item))
                    _outObserver.OnNext(item);
            }

            public void OnError(Exception error)
            {
                ThrowIfDisposed();

                if (_errorSelector == null || _errorSelector(error))
                    _outObserver.OnError(error);
            }

            public void OnCompleted()
            {
                ThrowIfDisposed();

                _isCompleted = true;
                _outObserver.OnCompleted();
            }

            public bool CompleteOnError { get; set; }
            
            public void Dispose()
            {
                ThrowIfDisposed();

                _isDisposed = true;
                _disposeAction.Dispose();
            }
        }

        class ActionCallSubject<T> : ISubject<T>
        {
            private readonly Action<T> _onNext;
            private readonly Action<Exception> _onError;
            private readonly Action _onComplete;

            public bool CompleteOnError { get; set; } = true;

            public ActionCallSubject(Action<T> onNext, Action<Exception> onError, Action onComplete)
            {
                _onNext = onNext;
                _onError = onError;
                _onComplete = onComplete;
            }

            public IDisposable Subscribe(IObserver<T> observable)
            {
                throw new NotImplementedException();
            }

            public void OnNext(T item)
            {
                _onNext?.Invoke(item);
            }

            public void OnError(Exception error)
            {
                _onError?.Invoke(error);
            }

            public void OnCompleted()
            {
                _onComplete?.Invoke();
            }
        }
        class ActionCallSubject : ISubject
        {
            private readonly Action _onNext;
            private readonly Action<Exception> _onError;
            private readonly Action _onComplete;

            public bool CompleteOnError { get; set; } = true;

            public ActionCallSubject(Action onNext, Action<Exception> onError, Action onComplete)
            {
                _onNext = onNext;
                _onError = onError;
                _onComplete = onComplete;
            }

            public IDisposable Subscribe(IObserver observable)
            {
                throw new NotImplementedException();
            }

            public void OnNext()
            {
                _onNext?.Invoke();
            }

            public void OnError(Exception error)
            {
                _onError?.Invoke(error);
            }

            public void OnCompleted()
            {
                _onComplete?.Invoke();
            }
        }
    }
}