using System;
using System.Collections;

namespace Valkyrie.Threading.Async
{
    public interface ICompositeDisposable : IDisposable, IEnumerable
    {
        void Add(IDisposable subscription);
        bool Remove(IDisposable subscription);
    }
}