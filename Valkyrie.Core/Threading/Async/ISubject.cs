﻿namespace Valkyrie.Threading.Async
{
    public interface ISubject<T> : ISubject<T,T>
    {
        bool CompleteOnError { get; set; }
    }

    public interface ISubject<out TSource, in TResult> : System.IObservable<TSource>, System.IObserver<TResult>
    {
        
    }

    public interface ISubject : IObservable, IObserver
    {
        bool CompleteOnError { get; set; }
    }
}