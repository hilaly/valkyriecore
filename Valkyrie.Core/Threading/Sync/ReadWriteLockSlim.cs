﻿using System;
using System.Threading;

namespace Valkyrie.Threading.Sync
{
    internal class ReadWriteLockSlim : IDisposable, IReaderWriterLock
    {
        private readonly ReaderWriterLockSlim _sync;

        public ReadWriteLockSlim(MetaInfo applicationInfo)
        {
            _sync = new ReaderWriterLockSlim(applicationInfo.LockPolicy);
        }

        public void Dispose()
        {
            _sync.Dispose();
        }

        public IDisposable LockWrite()
        {
            return new WriteLock(_sync);
        }

        public IReadLocker LockRead()
        {
            return new ReadWriteLock(_sync);
        }
    }
}
