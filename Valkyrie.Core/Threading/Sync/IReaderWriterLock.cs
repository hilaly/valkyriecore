﻿using System;

namespace Valkyrie.Threading.Sync
{
    public interface IReaderWriterLock
    {
        IDisposable LockWrite();
        IReadLocker LockRead();
    }
}