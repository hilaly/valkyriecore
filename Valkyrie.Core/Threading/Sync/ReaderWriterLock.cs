﻿using System;

namespace Valkyrie.Threading.Sync
{
    class ReaderWriterLock : IReaderWriterLock
    {
        private readonly System.Threading.ReaderWriterLock _lock = new System.Threading.ReaderWriterLock();
        private readonly int _lockTimeout;

        public ReaderWriterLock(int lockTimeout)
        {
            _lockTimeout = lockTimeout;
        }

        public IDisposable LockWrite()
        {
            return new WriteLocker(_lock, _lockTimeout);
        }

        public IReadLocker LockRead()
        {
            return new ReadLocker(_lock, _lockTimeout);
        }
    }
}