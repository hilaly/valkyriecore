﻿using System;

namespace Valkyrie.Threading.Sync
{
    class WriteLocker : IDisposable
    {
        private readonly System.Threading.ReaderWriterLock _lock;

        public WriteLocker(System.Threading.ReaderWriterLock @lock, int lockTimeout)
        {
            _lock = @lock;
            _lock.AcquireWriterLock(lockTimeout);
        }

        public void Dispose()
        {
            _lock.ReleaseWriterLock();
        }
    }
}