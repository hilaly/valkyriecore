﻿using System;

namespace Valkyrie.Threading.Sync
{
    class ReadLocker : IReadLocker
    {
        private readonly System.Threading.ReaderWriterLock _lock;
        private readonly int _lockTimeout;

        internal ReadLocker(System.Threading.ReaderWriterLock @lock, int lockTimeout)
        {
            _lock = @lock;
            _lockTimeout = lockTimeout;

            _lock.AcquireReaderLock(_lockTimeout);
        }

        public IDisposable LockWrite()
        {
            return new ReadWriteLocker(_lock, _lockTimeout);
        }

        public void Dispose()
        {
            _lock.ReleaseReaderLock();
        }
    }
}