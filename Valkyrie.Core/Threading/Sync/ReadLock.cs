﻿using System;
using System.Threading;

namespace Valkyrie.Threading.Sync
{
    class ReadLock : IDisposable
    {
        private readonly ReaderWriterLockSlim _lock;

        public ReadLock(ReaderWriterLockSlim @lock)
        {
            _lock = @lock;
            _lock.EnterReadLock();
        }

        public void Dispose()
        {
            _lock.ExitReadLock();
        }
    }
}