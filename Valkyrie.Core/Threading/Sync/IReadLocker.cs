﻿using System;

namespace Valkyrie.Threading.Sync
{
    public interface IReadLocker : IDisposable
    {
        IDisposable LockWrite();
    }
}