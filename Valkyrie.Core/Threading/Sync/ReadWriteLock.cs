﻿using System;
using System.Threading;

namespace Valkyrie.Threading.Sync
{
    class ReadWriteLock : IReadLocker
    {
        private readonly ReaderWriterLockSlim _lock;

        internal ReadWriteLock(ReaderWriterLockSlim @lock)
        {
            _lock = @lock;

            _lock.EnterUpgradeableReadLock();
        }

        public void Dispose()
        {
            _lock.ExitUpgradeableReadLock();
        }

        public IDisposable LockWrite()
        {
            return new WriteLock(_lock);
        }
    }
}