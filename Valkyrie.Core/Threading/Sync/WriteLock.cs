﻿using System;
using System.Threading;

namespace Valkyrie.Threading.Sync
{
    class WriteLock : IDisposable
    {
        private readonly ReaderWriterLockSlim _lock;

        internal WriteLock(ReaderWriterLockSlim @lock)
        {
            _lock = @lock;
            _lock.EnterWriteLock();
        }

        public void Dispose()
        {
            _lock.ExitWriteLock();
        }
    }
}