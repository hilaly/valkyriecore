﻿using System;
using System.Threading;

namespace Valkyrie.Threading.Sync
{
    class ReadWriteLocker : IDisposable
    {
        private readonly System.Threading.ReaderWriterLock _lock;
        private LockCookie _lc;
        internal ReadWriteLocker(System.Threading.ReaderWriterLock @lock, int lockTimeout)
        {
            _lock = @lock;
            _lc = _lock.UpgradeToWriterLock(lockTimeout);
        }
        public void Dispose()
        {
            _lock.DowngradeFromWriterLock(ref _lc);
        }
    }
}