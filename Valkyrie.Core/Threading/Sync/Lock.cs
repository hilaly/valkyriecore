﻿namespace Valkyrie.Threading.Sync
{
    static class Lock
    {
        public static IReaderWriterLock Create(MetaInfo applicationInfo)
        {
            return new ReadWriteLockSlim(applicationInfo);
        }
    }
}