using System;

namespace Valkyrie.Utils
{
    public class UpdateTimer
    {
        private float _time;
        public float Delay { get; set; }

        public bool Update(float dt)
        {
            if (Delay <= 0f)
                return true;

            _time += dt;
            if (_time >= Delay)
            {
                _time -= Delay;
                return true;
            }

            return false;
        }
    }
}