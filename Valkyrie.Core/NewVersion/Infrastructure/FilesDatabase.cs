using System;
using System.Collections.Generic;
using System.IO;
using Valkyrie.Data;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Infrastructure
{
    class FilesDatabase : IDisposable
    {
        private readonly string _rootPath;

        internal interface IFileInfo
        {
            string Hash { get; }
        }

        class FileInfo
        {
            public string Name;
            public List<FileVersion> Versions = new List<FileVersion>();
        }

        class FileVersion
        {
            public string Version;
            public List<FileInstance> Instances = new List<FileInstance>();
        }

        class FileInstance : IFileInfo
        {
            public string LocalPath;
            public string Hash { get; set; }
        }

        private Dictionary<string, FileInfo> _database;

        public FilesDatabase(string pathRoot)
        {
            _rootPath = pathRoot;
            if (!Directory.Exists(_rootPath))
                Directory.CreateDirectory(_rootPath);
            LoadDb();
            
            //TODO: ADD LOGS
        }

        public bool TryGet(string fileName, string version, out IFileInfo o)
        {
            if (_database.TryGetValue(fileName, out var fileInfo))
            {
                var fileVersion = fileInfo.Versions.Find(x => x.Version == version);
                if (fileVersion != null && fileVersion.Instances.Count > 0)
                {
                    o = fileVersion.Instances.Find(x => true);
                    return o != null;
                }
            }

            o = null;
            return false;
        }

        public IPromise<bool> Set(string fileName, string version, byte[] content, out string hash)
        {
            try
            {
                hash = content.ComputeHash();
                var localPath = $"{Guid.NewGuid().ToString()}.valkyrieFilePatch";
                if (!_database.TryGetValue(fileName, out var fileInfo))
                    _database.Add(fileName, fileInfo = new FileInfo {Name = fileName});
                var fileVersion = fileInfo.Versions.Find(x => x.Version == version);
                if (fileVersion == null)
                    fileInfo.Versions.Add(fileVersion = new FileVersion() {Version = version});
                fileVersion.Instances.Insert(0, new FileInstance() {Hash = hash, LocalPath = localPath});
                File.WriteAllBytes(Path.Combine(_rootPath, localPath), content);
                SaveDb();
                return Promise<bool>.Resolved(true);
            }
            catch (Exception e)
            {
                hash = null;
                return Promise<bool>.Resolved(false);
            }
        }

        public void Dispose()
        {
            SaveDb();
        }

        private const string DbFileName = "ValkyrieFileStorageDatabase.json";

        private void SaveDb()
        {
            File.WriteAllText(Path.Combine(_rootPath, DbFileName), _database.ToJson());
        }

        private void LoadDb()
        {
            if (File.Exists(Path.Combine(_rootPath, DbFileName)))
                _database = File.ReadAllText(Path.Combine(_rootPath, DbFileName))
                    .ToObject<Dictionary<string, FileInfo>>();
            else if (_database == null) _database = new Dictionary<string, FileInfo>();
        }

        public IPromise<byte[]> Read(IFileInfo fileInfo)
        {
            return Promise<byte[]>.Resolved(
                File.ReadAllBytes(Path.Combine(_rootPath, ((FileInstance) fileInfo).LocalPath)));
        }

        public IPromise Clear()
        {
            foreach (var s in Directory.EnumerateFiles(_rootPath, "*.valkyrieFilePatch"))
                File.Delete(s);
            _database.Clear();
            SaveDb();
            return Promise.Resolved();
        }
    }
}