using System;
using System.IO;
using Valkyrie.Data;
using Valkyrie.Infrastructure.Protocol;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;

namespace Valkyrie.Infrastructure
{
    class ServerFileStorage : FileStorageService.ServerBase, IDisposable
    {
        private CompositeDisposable _compositeDisposable = new CompositeDisposable();

        private FilesDatabase _filesDb;

        public ServerFileStorage(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            _filesDb = new FilesDatabase(path);
        }

        public override IPromise<DownloadFileResponse> Download(DownloadFileRequest request, ServerCallContext context)
        {
            if (_filesDb.TryGet(request.fileName, request.version, out var instance))
            {
                if (instance.Hash == request.hash)
                    return Promise<DownloadFileResponse>.Resolved(new DownloadFileResponse()
                    {
                        result = DownloadFileResponse.DownloadFileResult.FILE_IS_UP_TO_DATE,
                        content = null,
                        hash = instance.Hash
                    });

                return _filesDb.Read(instance).Then(content => new DownloadFileResponse()
                {
                    content = content,
                    hash = instance.Hash,
                    result = DownloadFileResponse.DownloadFileResult.OK
                });
            }

            return Promise<DownloadFileResponse>.Resolved(new DownloadFileResponse()
            {
                result = DownloadFileResponse.DownloadFileResult.FILE_NOT_FOUND
            });
        }

        public override IPromise<UploadFileResponse> Upload(UploadFileRequest request, ServerCallContext context)
        {
            var name = request.fileName;
            var version = request.version;
            var content = request.content;
            return Update(name, version, content, out var hash)
                .Then(wasSaved =>
                {
                    return new UploadFileResponse()
                    {
                        error = (int) (wasSaved ? ErrorCodes.Ok : ErrorCodes.InternalError),
                        hash = hash,
                        errorDescription = wasSaved ? null : $"Unknown"
                    };
                });
        }

        public IPromise<bool> Update(string name, string version, byte[] content, out string hash)
        {
            return _filesDb.Set(name, version, content, out hash);
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
            _compositeDisposable = null;
        }

        public IPromise Init()
        {
            return Promise.Resolved();
        }

        public IPromise Clear()
        {
            return _filesDb.Clear();
        }
    }
}