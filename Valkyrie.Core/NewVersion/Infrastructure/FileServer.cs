using System;
using Valkyrie.Communication.Network;
using Valkyrie.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;

namespace Valkyrie.Infrastructure.Protocol
{
    [Obsolete("Dont use it")]
    public class DownloadFileRequest
    {
        public string fileName;
        public string version;
        public string hash;
    }
    [Obsolete("Dont use it")]
    public class DownloadFileResponse
    {
        public DownloadFileResult result;
        public byte[] content;
        public string hash;

        public enum DownloadFileResult
        {
            FILE_IS_UP_TO_DATE,
            OK,
            FILE_NOT_FOUND
        }
    }
    [Obsolete("Dont use it")]
    public class UploadFileRequest
    {
        public string fileName;
        public string version;
        public byte[] content;
    }
    [Obsolete("Dont use it")]
    public class UploadFileResponse
    {
        public int error;
        public string hash;
        public string errorDescription;
    }
    [Obsolete("Dont use it")]
    class FileStorageService
    {
        [Obsolete("Dont use it")]
        public class ServerBase
        {
            public virtual IPromise<DownloadFileResponse> Download(DownloadFileRequest request, ServerCallContext context)
            {
                throw new NotImplementedException();
            }

            public virtual IPromise<UploadFileResponse> Upload(UploadFileRequest request, ServerCallContext context)
            {
                throw new NotImplementedException();
            }
        }

        [Obsolete("Dont use it")]
        public class Client : ClientBase
        {
            public Client(INetworkChannel channel) : base(channel)
            {
            }

            public Client(IClient networkClient) : base(networkClient)
            {
            }

            public IPromise<DownloadFileResponse> Download(DownloadFileRequest downloadFileRequest)
            {
                throw new NotImplementedException();
            }

            public IPromise<UploadFileResponse> Upload(UploadFileRequest uploadFileRequest)
            {
                throw new NotImplementedException();
            }
        }

        public static ServiceDefinition BindService(ServerFileStorage service)
        {
            throw new NotImplementedException();
        }
    }
}