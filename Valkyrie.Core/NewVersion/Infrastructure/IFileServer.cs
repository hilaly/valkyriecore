using System;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;

namespace Valkyrie.Infrastructure
{
    public interface IFileServer : IDisposable
    {
        IPromise Init(string path, params ServerPort[] ports);
        IPromise<string> Patch(string path, string version, byte[] content);
        IPromise Clear();
    }
}