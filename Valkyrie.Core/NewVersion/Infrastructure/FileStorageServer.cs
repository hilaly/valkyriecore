using System;
using Valkyrie.Communication.Network;
using Valkyrie.Infrastructure.Protocol;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Infrastructure
{
    class FileStorageServer : IFileServer
    {
        private readonly INetwork _network;
        private readonly ILogService _logService;

        private CompositeDisposable _compositeDisposable;
        private ServerFileStorage _service;

        public FileStorageServer(INetwork network, ILogService logService)
        {
            _network = network;
            _logService = logService;
            _compositeDisposable = new CompositeDisposable();
        }

        public IPromise Init(string path, params ServerPort[] ports)
        {
            _service = new ServerFileStorage(path);
            var server = new Server(_network, _logService)
            {
                Services = new[] {FileStorageService.BindService(_service)},
                Ports = ports
            };

            _compositeDisposable.Add(server);
            _compositeDisposable.Add(_service);

            return _service.Init()
                .Then(server.Start);
        }

        public IPromise<string> Patch(string path, string version, byte[] content)
        {
            return _service.Update(path, version, content, out var hash)
                .Then(x => x ? Promise<string>.Resolved(hash) : Promise<string>.Rejected(new Exception("Failed")));
        }

        public IPromise Clear()
        {
            return _service.Clear();
        }

        public void Dispose()
        {
            _compositeDisposable?.Dispose();
            _compositeDisposable = null;
        }
    }
}