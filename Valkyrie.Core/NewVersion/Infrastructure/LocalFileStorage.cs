using System;
using System.IO;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Infrastructure.Protocol;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Infrastructure
{
    class LocalFileStorage : IFileStorage
    {
        private FileStorageService.Client _client;
        private string _cachePath;

        public IPromise Init(INetworkChannel networkChannel, string localCashPath)
        {
            _cachePath = localCashPath;
            if (!Directory.Exists(_cachePath))
                Directory.CreateDirectory(_cachePath);
            _client = new FileStorageService.Client(networkChannel);
            return Promise.Resolved();
        }

        string NormalizeFileName(string path)
        {
            return path.Replace('/', '.').Replace('\\', '.');
        }

        public IPromise<byte[]> Download(string path, string version)
        {
            if (_client == null)
                return Promise<byte[]>.Rejected(new Exception("Storage is not initialized"));

            var filename = NormalizeFileName(path);

            return _client.Download(new DownloadFileRequest
            {
                hash = GetFileHash(filename),
                fileName = filename,
                version = version
            }).Then(response =>
            {
                if (response.result == DownloadFileResponse.DownloadFileResult.FILE_NOT_FOUND)
                    return Promise<byte[]>.Rejected(new Exception($"File {filename} not found"));
                if (response.result == DownloadFileResponse.DownloadFileResult.OK)
                    SaveFile(filename, response.content);
                return LoadLocal(filename);
            });
        }

        private string GetFileHash(string filename)
        {
            var fullPath = Path.Combine(_cachePath, filename);
            return DataExtensions.GetFileHash(fullPath);
        }

        private void SaveFile(string filename, byte[] content)
        {
            var fullPath = Path.Combine(_cachePath, filename);
            File.WriteAllBytes(fullPath, content);
        }

        private IPromise<byte[]> LoadLocal(string filename)
        {
            var fullPath = Path.Combine(_cachePath, filename);
            if (File.Exists(fullPath))
                return Promise<byte[]>.Resolved(File.ReadAllBytes(fullPath));
            return Promise<byte[]>.Rejected(new FileNotFoundException(filename));
        }

        public IPromise Upload(string filename, string version, byte[] content)
        {
            if (_client == null)
                return Promise.Rejected(new Exception("Storage is not initialized"));

            filename = NormalizeFileName(filename);

            return _client.Upload(new UploadFileRequest
                {
                    fileName = filename,
                    content = content,
                    version = version
                })
                .Then(response =>
                {
                    if (response.error == (int) ErrorCodes.Ok)
                        return Promise.Resolved();
                    return Promise.Rejected(
                        new Exception($"Code={(ErrorCodes) response.error}, Desc='{response.errorDescription}'"));
                });
        }
    }
}