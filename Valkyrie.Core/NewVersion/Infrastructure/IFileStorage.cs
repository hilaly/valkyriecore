using Valkyrie.Communication.Network;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Infrastructure
{
    public interface IFileStorage
    {
        IPromise Init(INetworkChannel networkChannel, string localCashPath);
        
        IPromise<byte[]> Download(string filename, string version);
        IPromise Upload(string filename, string version, byte[] content);
        
        //TODO: deleting files
        //TODO: request files list
    }
}