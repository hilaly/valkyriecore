namespace Valkyrie.Ai.Behaviour
{
    /// <summary>
    /// The return type when invoking behaviour tree nodes.
    /// </summary>
    public enum BehaviourTreeStatus
    {
        Success,
        Failure,
        Running
    }
}