using System;
using Valkyrie.Data;
using Valkyrie.Di;
using Valkyrie.Grammar;
using Valkyrie.Scripts;

namespace Valkyrie.Ai.Behaviour
{
    public class BehaviourTreeCompiler
    {
#pragma warning disable 649
        [Inject(IsOptional = true)] private IScriptEngine _scriptEngine;
#pragma warning restore 649
        private readonly IAstConstructor _astConstructor;

        public BehaviourTreeCompiler()
        {
            using var stream = DataExtensions.GetEmbeddedResourceStream("behaviourTreeGrammarDefinition.txt");
            _astConstructor = Grammar.Grammar.Create(stream);
        }

        public IBehaviourTreeBuilder Create()
        {
            return new BehaviourTreeBuilder();
        }

        public IBehaviourTreeNode Compile(string source)
        {
            if (_scriptEngine == null)
                throw new Exception($"You must use ScriptsLibrary to construct behaviour trees from source");
            
            Console.WriteLine(source);

            using var stream = source.ToStream();
            var ast = _astConstructor.Parse(stream);
            return Compile(ast);
        }

        private IBehaviourTreeNode Compile(IAstNode source)
        {
            var builder = Create();
            builder.Sequence("root");
            Build(builder, source);
            builder.End();
            return builder.Build();
        }

        private void Build(IBehaviourTreeBuilder builder, IAstNode node)
        {
            var nodes = (node as NonTerminalNode)?.Nodes;
            switch (node.Name)
            {
                case "<root>":
                case "<node>":
                case "<generated-zerocountlist-<node>>":
                {
                    foreach (var astNode in nodes)
                        Build(builder, astNode);
                    break;
                }
                case "<action>":
                {
                    builder.Do("", CreateScriptNode(nodes[0]));
                    break;
                }
                case "<select>":
                {
                    builder.Selector("");
                    Build(builder, nodes[2]);
                    builder.End();
                    break;
                }
                case "<sequence>":
                {
                    builder.Sequence("");
                    Build(builder, nodes[2]);
                    builder.End();
                    break;
                }
                case "<parallel>":
                {
                    var childNodes = node.FindAll("<node>");
                    builder.Parallel("", childNodes.Count, childNodes.Count);
                    Build(builder, nodes[2]);
                    builder.End();
                    break;
                }
                case "<condition>":
                {
                    builder.Sequence("");
                    builder.Condition("", CreateScriptCondition(nodes[1]));
                    Build(builder, nodes[2]);
                    builder.End();
                    break;
                }
                default:
                    throw new Exception($"Unimplemented node {node.Name}");
            }
        }

        private Func<NodeCheckArgs, bool> CreateScriptCondition(IAstNode node)
        {
            var scriptCode = GetScriptText(node);
            return args => (bool) _scriptEngine.Eval(scriptCode);
        }

        private Func<NodeCheckArgs, BehaviourTreeStatus> CreateScriptNode(IAstNode node)
        {
            var scriptCode = GetScriptText(node);
            return args =>
            {
                try
                {
                    var ret = _scriptEngine.Eval(scriptCode);
                    if (ret is BehaviourTreeStatus bts)
                        return bts;
                    return BehaviourTreeStatus.Success;
                }
                catch (Exception)
                {
                    return BehaviourTreeStatus.Failure;
                }
            };
        }

        string GetScriptText(IAstNode node)
        {
            if (node.Name != "<script>")
                throw new Exception($"Script can be compiled only from <script> node");
            if(node.GetChilds().Count != 3)
                throw new Exception($"Script node was corrupted");

            return node.GetChilds()[1].ConvertTreeToString(" ");
        }
    }
}