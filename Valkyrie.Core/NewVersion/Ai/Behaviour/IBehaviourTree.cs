namespace Valkyrie.Ai.Behaviour
{
    public interface IBehaviourTree
    {
        void Execute();
    }
}