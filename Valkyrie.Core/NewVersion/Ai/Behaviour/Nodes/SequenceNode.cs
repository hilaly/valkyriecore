using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Valkyrie.Ai.Behaviour
{
    /// <summary>
    /// Runs child nodes in sequence, until one fails.
    /// </summary>
    public class SequenceNode : IParentBehaviourTreeNode
    {
        /// <summary>
        /// Name of the node.
        /// </summary>
        private string name;

        /// <summary>
        /// List of child nodes.
        /// </summary>
        private List<IBehaviourTreeNode> children = new List<IBehaviourTreeNode>(); //todo: this could be optimized as a baked array.

        public SequenceNode(string name)
        {
            this.name = name;
        }

        public BehaviourTreeStatus Tick(NodeCheckArgs args)
        {
            foreach (var child in children)
            {
                var childStatus = child.Tick(args);
                if (childStatus != BehaviourTreeStatus.Success)
                {
                    return childStatus;
                }
            }

            return BehaviourTreeStatus.Success;
        }

        /// <summary>
        /// Add a child to the sequence.
        /// </summary>
        public void AddChild(IBehaviourTreeNode child)
        {
            children.Add(child);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("sequence [");
            foreach (var s in children.SelectMany(child => child.ToString().Split('\n')))
                sb.AppendLine($"\t{s}");
            sb.Append("]");
            return sb.ToString();
        }
    }
}