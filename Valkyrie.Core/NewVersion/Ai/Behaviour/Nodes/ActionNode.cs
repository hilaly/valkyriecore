using System;

namespace Valkyrie.Ai.Behaviour
{
    /// <summary>
    /// A behaviour tree leaf node for running an action.
    /// </summary>
    public class ActionNode : IBehaviourTreeNode
    {
        /// <summary>
        /// The name of the node.
        /// </summary>
        private string name;

        /// <summary>
        /// Function to invoke for the action.
        /// </summary>
        private Func<NodeCheckArgs, BehaviourTreeStatus> fn;
        

        public ActionNode(string name, Func<NodeCheckArgs, BehaviourTreeStatus> fn)
        {
            this.name=name;
            this.fn=fn;
        }

        public BehaviourTreeStatus Tick(NodeCheckArgs args)
        {
            return fn(args);
        }

        public override string ToString()
        {
            return "<action>";
        }
    }
}