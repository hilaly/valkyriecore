using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Valkyrie.Ai.Behaviour
{
    /// <summary>
    /// Selects the first node that succeeds. Tries successive nodes until it finds one that doesn't fail.
    /// </summary>
    public class SelectorNode : IParentBehaviourTreeNode
    {
        /// <summary>
        /// The name of the node.
        /// </summary>
        private string name;

        /// <summary>
        /// List of child nodes.
        /// </summary>
        private List<IBehaviourTreeNode> children = new List<IBehaviourTreeNode>(); //todo: optimization, bake this to an array.

        public SelectorNode(string name)
        {
            this.name = name;
        }

        public BehaviourTreeStatus Tick(NodeCheckArgs args)
        {
            foreach (var child in children)
            {
                var childStatus = child.Tick(args);
                if (childStatus != BehaviourTreeStatus.Failure)
                {
                    return childStatus;
                }
            }

            return BehaviourTreeStatus.Failure;
        }

        /// <summary>
        /// Add a child node to the selector.
        /// </summary>
        public void AddChild(IBehaviourTreeNode child)
        {
            children.Add(child);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("select [");
            foreach (var s in children.SelectMany(child => child.ToString().Split('\n')))
                sb.AppendLine($"\t{s}");
            sb.Append("]");
            return sb.ToString();
        }
    }
}