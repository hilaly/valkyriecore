using System;

namespace Valkyrie.Ai.Behaviour
{
    public interface IBehaviourTreeBuilder
    {
        /// <summary>
        /// Create an action node.
        /// </summary>
        IBehaviourTreeBuilder Do(string name, Func<NodeCheckArgs, BehaviourTreeStatus> fn);

        /// <summary>
        /// Like an action node... but the function can return true/false and is mapped to success/failure.
        /// </summary>
        IBehaviourTreeBuilder Condition(string name, Func<NodeCheckArgs, bool> fn);

        /// <summary>
        /// Create an inverter node that inverts the success/failure of its children.
        /// </summary>
        IBehaviourTreeBuilder Inverter(string name);

        /// <summary>
        /// Create a sequence node.
        /// </summary>
        IBehaviourTreeBuilder Sequence(string name);

        /// <summary>
        /// Create a parallel node.
        /// </summary>
        IBehaviourTreeBuilder Parallel(string name, int numRequiredToFail, int numRequiredToSucceed);

        /// <summary>
        /// Create a selector node.
        /// </summary>
        IBehaviourTreeBuilder Selector(string name);

        /// <summary>
        /// Splice a sub tree into the parent tree.
        /// </summary>
        IBehaviourTreeBuilder Splice(IBehaviourTreeNode subTree);

        /// <summary>
        /// Build the actual tree.
        /// </summary>
        IBehaviourTreeNode Build();

        /// <summary>
        /// Ends a sequence of children.
        /// </summary>
        IBehaviourTreeBuilder End();
    }
}