﻿using System.Reflection;
using Valkyrie.Ai.Behaviour;
using Valkyrie.Ai.FSM;
using Valkyrie.Di;

namespace Valkyrie.Ai
{
    public static class Utils
    {
        public static IFsm CreateFsm()
        {
            return new Fsm();
        }

        public static IFsm ToFsm(this IContainer c, object o)
        {
            var type = o.GetType();
            var fsm = CreateFsm();

            foreach (var methodInfo in type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
            {
                foreach (var attribute in methodInfo.GetCustomAttributes<FsmEnterStateAttribute>())
                    fsm.In(attribute.State)
                        .OnEnter(ev =>
                        {
                            if(ev == null)
                                c.Invoke(methodInfo, o, fsm);
                            else
                                c.Invoke(methodInfo, o, ev, fsm);
                        });

                foreach (var attribute in methodInfo.GetCustomAttributes<FsmExitStateAttribute>())
                    fsm.In(attribute.State)
                        .OnExit(() => c.Invoke(methodInfo, o, fsm));

                foreach (var attribute in methodInfo.GetCustomAttributes<FsmEventStateAttribute>())
                {
                    if (attribute.EventType != null)
                        fsm.In(attribute.State)
                            .On(attribute.EventType, (tr, ev) => c.Invoke(methodInfo, o, tr, ev, fsm));
                    else
                        fsm.In(attribute.State)
                            .OnAny((tr, ev) => c.Invoke(methodInfo, o, tr, (object) ev, fsm));
                }
            }

            object startState = type.GetCustomAttribute<FsmStartStateAttribute>()?.State;
            foreach (var property in type.GetProperties(BindingFlags.Instance | BindingFlags.GetProperty))
            {
                var attribute = property.GetCustomAttribute<FsmStartStateAttribute>();
                if(attribute == null)
                    continue;
                startState = attribute.State;
                break;
            }

            if(startState != null)
                fsm.Start(startState);
            
            return fsm;
        }

        public static IBehaviourTreeBuilder CreateBehaviourTree()
        {
            return new BehaviourTreeBuilder();
        }
    }
}