﻿namespace Valkyrie.Ai.FSM
{
    public interface IEventRouter
    {
        void Raise(object eventInstance);
    }
}