using System;
// ReSharper disable ClassNeverInstantiated.Global

namespace Valkyrie.Ai.FSM
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
    public class FsmStartStateAttribute : Attribute
    {
        public FsmStartStateAttribute(object state)
        {
            State = state;
        }

        public object State { get; }
    }
    
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class FsmEventStateAttribute : Attribute
    {
        public FsmEventStateAttribute(object state)
        {
            State = state;
        }

        public object State { get; }
        // ReSharper disable once UnusedAutoPropertyAccessor.Global
        public Type EventType { get; set; }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class FsmEnterStateAttribute : Attribute
    {
        public FsmEnterStateAttribute(object state)
        {
            State = state;
        }

        public object State { get; }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class FsmExitStateAttribute : Attribute
    {
        public FsmExitStateAttribute(object state)
        {
            State = state;
        }

        public object State { get; }
    }
}