﻿namespace Valkyrie.Ai.FSM
{
    public interface IStateMachineDefinition : IStateDefinition
    {
        IStateMachineDefinition ResetOnEnter();
    }
}