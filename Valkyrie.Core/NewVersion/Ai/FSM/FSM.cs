﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Ai.FSM
{
    public class Fsm : IFsm, IFsmTransitor
    {
        class StateDefinition : IStateDefinition
        {
            public Action<object> EnterAction;
            public Action ExitAction;

            public readonly Dictionary<Type, Action<IFsmTransitor, object>> EventActions =
                new Dictionary<Type, Action<IFsmTransitor, object>>();

            public Action<IFsmTransitor, object> AnyAction;

            public IStateDefinition OnEnter(Action callback)
            {
                return OnEnter(o => callback());
            }

            public IStateDefinition OnEnter(Action<object> callback)
            {
                EnterAction = callback;
                return this;
            }

            public IStateDefinition OnExit(Action callback)
            {
                ExitAction = callback;
                return this;
            }

            public IStateDefinition On(Type eventType, Action<IFsmTransitor, object> callback)
            {
                if (EventActions.ContainsKey(eventType))
                    EventActions[eventType] = callback;
                else
                    EventActions.Add(eventType, callback);

                return this;
            }

            public IStateDefinition On<TEvent>(Action callback)
            {
                return On<TEvent>((tr, ev) => callback());
            }

            public IStateDefinition On<TEvent>(Action<TEvent> callback)
            {
                return On<TEvent>((tr, ev) => callback(ev));
            }

            public IStateDefinition On<TEvent>(Action<IFsmTransitor, TEvent> callback)
            {
                void CallAction(IFsmTransitor transition, object ev) => callback(transition, (TEvent) ev);
                return On(typeof(TEvent), CallAction);

                if (EventActions.ContainsKey(typeof(TEvent)))
                    EventActions[typeof(TEvent)] = CallAction;
                else
                    EventActions.Add(typeof(TEvent), CallAction);

                return this;
            }

            public IStateDefinition OnAny(Action<object> callback)
            {
                return OnAny((tr, o) => callback(o));
            }

            public IStateDefinition OnAny(Action<IFsmTransitor, object> callback)
            {
                AnyAction = callback;
                return this;
            }
        }

        class StateMachineDefinition : StateDefinition, IStateMachineDefinition
        {
            private readonly Fsm _fsm;
            private object _resetState;

            public StateMachineDefinition(Fsm fsm)
            {
                _fsm = fsm;
                _resetState = _fsm._currentState;

                OnEnter(o => _fsm.Switch(_resetState, o)).OnExit(() =>
                {
                    _resetState = _fsm._currentState;
                    _fsm.Switch(null, null);
                }).OnAny(_fsm.Raise);
            }

            public IStateMachineDefinition ResetOnEnter()
            {
                _resetState = _fsm._initialState;
                
                OnExit(() =>
                {
                    _resetState = _fsm._initialState;
                    _fsm.Switch(null, null);
                });
                return this;
            }
        }

        private object _initialState;

        private readonly Dictionary<object, StateDefinition> _registeredStates =
            new Dictionary<object, StateDefinition>();

        private readonly Dictionary<Type, Action<IFsmTransitor, object>> _eventActions =
            new Dictionary<Type, Action<IFsmTransitor, object>>();

        private object _currentState;
        private StateDefinition _currentDefinition;

        #region IFsm

        #region IEventRouter

        public void Raise(object eventInstance)
        {
            var eventType = eventInstance.GetType();

            if (_eventActions.TryGetValue(eventType, out var callAction))
                callAction(this, eventInstance);
            if (_currentDefinition.EventActions.TryGetValue(eventType, out callAction))
                callAction(this, eventInstance);
            else
                _currentDefinition.AnyAction?.Invoke(this, eventInstance);
        }

        #endregion

        public object State => _currentState;

        public IStateDefinition In(object state)
        {
            if (!_registeredStates.TryGetValue(state, out var result))
                _registeredStates.Add(state, result = new StateDefinition());
            return result;
        }

        public IStateMachineDefinition In(IFsm innerFsm)
        {
            if (!_registeredStates.TryGetValue(innerFsm, out var result))
                _registeredStates.Add(innerFsm, result = new StateMachineDefinition((Fsm) innerFsm));
            return (IStateMachineDefinition) result;
        }

        public void Start(object state)
        {
            Switch(_initialState = state);
        }

        public IFsm On<TEvent>(Action callback)
        {
            return On<TEvent>((tr, ev) => callback());
        }

        public IFsm On<TEvent>(Action<TEvent> callback)
        {
            return On<TEvent>((tr, ev) => callback(ev));
        }

        public IFsm On<TEvent>(Action<IFsmTransitor, TEvent> callback)
        {
            void CallAction(IFsmTransitor transition, object ev) => callback(transition, (TEvent) ev);

            if (_eventActions.ContainsKey(typeof(TEvent)))
                _eventActions[typeof(TEvent)] = CallAction;
            else
                _eventActions.Add(typeof(TEvent), CallAction);

            return this;
        }

        #endregion

        #region IFsmTransitor

        public void Switch(object newState)
        {
            Switch(newState, null);
        }

        public void Switch(object newState, object additionalArg)
        {
            if (_currentState == newState)
                return;
            _currentDefinition?.ExitAction?.Invoke();
            
            _currentState = newState;

            if (_currentState != null)
                _registeredStates.TryGetValue(_currentState, out _currentDefinition);
            else
                _currentDefinition = null;
            _currentDefinition?.EnterAction?.Invoke(additionalArg);
        }

        #endregion
    }
}