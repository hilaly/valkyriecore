﻿using System;

namespace Valkyrie.Ai.FSM
{
    public interface IFsm : IEventRouter
    {
        object State { get; }
        
        IStateDefinition In(object state);
        IStateMachineDefinition In(IFsm innerFsm);

        IFsm On<TEvent>(Action callback);
        IFsm On<TEvent>(Action<TEvent> callback);
        IFsm On<TEvent>(Action<IFsmTransitor, TEvent> callback);

        void Start(object state);
    }
}