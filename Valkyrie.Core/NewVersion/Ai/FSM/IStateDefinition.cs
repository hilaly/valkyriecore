﻿using System;

namespace Valkyrie.Ai.FSM
{
    public interface IStateDefinition
    {
        IStateDefinition OnEnter(Action callback);
        IStateDefinition OnEnter(Action<object> callback);

        IStateDefinition OnExit(Action callback);

        IStateDefinition On(Type eventType, Action<IFsmTransitor, object> callback);
        IStateDefinition On<TEvent>(Action callback);
        IStateDefinition On<TEvent>(Action<TEvent> callback);
        IStateDefinition On<TEvent>(Action<IFsmTransitor, TEvent> callback);

        IStateDefinition OnAny(Action<object> callback);
        IStateDefinition OnAny(Action<IFsmTransitor, object> callback);
    }
}