﻿namespace Valkyrie.Ai.FSM
{
    public interface IFsmTransitor
    {
        void Switch(object newState);
        void Switch(object newState, object additionalArg);
    }
}