namespace Valkyrie.Ai.Planning
{
    public interface IActionCondition
    {
        bool IsMatch(IPlanningContext ctx);

        void Apply(IPlanningContext ctx);
    }
}