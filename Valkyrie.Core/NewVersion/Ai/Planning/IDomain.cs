using System.Collections.Generic;

namespace Valkyrie.Ai.Planning
{
    public interface IDomain
    {
        string Name { get; }
        IEnumerable<IActionInfo> Actions { get; }
        IEnumerable<IPredicateInfo> Predicates { get; }
        IEnumerable<IFunctionInfo> Functions { get; }

        IPlanner Planner { get; }
    }
}