using System.Collections.Generic;

namespace Valkyrie.Ai.Planning
{
    public interface IState
    {
        IEnumerable<string> Objects { get; }
        IEnumerable<KeyValuePair<string, string>> Predicates { get; }
        IEnumerable<KeyValuePair<string, float>> Values { get; }

        void AddPredicate(string predicate, string variables);
        void RemovePredicate(string predicate, string variables);
        bool HasPredicate(string predicate, string variables);

        void AddObjects(string objects);
        void RemoveObjects(string objects);
        bool HasObjects(string objects);

        float GetValue(string valueName);
        void AddValue(string valueName, float value);
        void RemoveValue(string valueName);
        bool HasValue(string valueName);

        IState Clone();
    }
}