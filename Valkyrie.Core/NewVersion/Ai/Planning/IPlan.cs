namespace Valkyrie.Ai.Planning
{
    public interface IPlan
    {
        bool IsValid { get; }
        PlanStep[] Steps { get; }
        float Cost { get; }
    }
}