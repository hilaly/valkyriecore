using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Ai.Planning
{
    public interface IPlanner
    {
        IDomain Domain { get; }

        IPlan Resolve(IProblem problem);

        IPromise<IPlan> ResolveAsync(IDispatcher dispatcher, IProblem problem);
    }
}