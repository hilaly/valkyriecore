namespace Valkyrie.Ai.Planning
{
    public interface IDomainBuilder
    {
        IDomain Build();

        IDomainBuilder DefinePredicate(string name, string parameters);

        IDomainBuilder DefineAction(string name, string parameters,
            IActionCondition precondition,
            IActionCondition effect,
            float cost);

        IDomainBuilder DefineFunction(string name, string parameters);
    }
}