using Valkyrie.Data;

namespace Valkyrie.Ai.Planning
{
    public struct PlanStep
    {
        public readonly string Action;
        public readonly string[] Args;
        public readonly float Cost;

        public PlanStep(string action, string[] args, float cost)
        {
            Action = action;
            Args = args;
            Cost = cost;
        }

        public override string ToString()
        {
            return $"({Action} {Args.Join(" ")})";
        }
    }
}