namespace Valkyrie.Ai.Planning
{
    public interface IActionInfo
    {
        string Name { get; }
        string[] Parameters { get; }
        int ArgsCount { get; }
        IActionCondition Precondition { get; }
        IActionCondition Effect { get; }
        float Cost { get; }
    }
}