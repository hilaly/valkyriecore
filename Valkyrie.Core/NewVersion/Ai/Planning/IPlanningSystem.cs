using System.Collections.Generic;
using System.IO;
using Valkyrie.Grammar;

namespace Valkyrie.Ai.Planning
{
    public interface IPlanningSystem
    {
        IAstConstructor Parser { get; }
        ICompiler Compiler { get; }
        
        IEnumerable<IDomain> Domains { get; }

        List<IProblem> Compile(Stream sourceStream);
    }
}