namespace Valkyrie.Ai.Planning
{
    public interface IFunctionInfo
    {
        string Name { get; }
        int ArgsCount { get; }
    }
}