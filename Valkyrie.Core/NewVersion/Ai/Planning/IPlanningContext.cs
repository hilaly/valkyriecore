namespace Valkyrie.Ai.Planning
{
    public interface IPlanningContext
    {
        IState State { get; }
        
        string GetVariable(string variableName);
        float GetValue(string method);
        void SetValue(string method, float value);
        bool HasValue(string s);
    }
}