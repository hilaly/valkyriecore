namespace Valkyrie.Ai.Planning
{
    public interface ICompiler
    {
        void Compile(IDomainBuilder domainBuilder, params string[] definesList);
        void Compile(IProblem problem, params string[] definesList);
    }
}