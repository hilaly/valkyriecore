namespace Valkyrie.Ai.Planning
{
    public interface IPredicateInfo
    {
        string Name { get; }
        int ArgsCount { get; }
    }
}