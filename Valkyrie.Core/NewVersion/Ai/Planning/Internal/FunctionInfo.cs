namespace Valkyrie.Ai.Planning.Internal
{
    internal class FunctionInfo : PredicateInfo, IFunctionInfo
    {
        public FunctionInfo(string name, string parameters) : base(name, parameters)
        {
        }
    }
}