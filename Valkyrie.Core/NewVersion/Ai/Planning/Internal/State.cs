using System.Collections.Generic;
using System.Linq;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    internal class State : IState
    {
        #region Objects

        private readonly HashSet<string> _objectsSet = new HashSet<string>();

        public IEnumerable<string> Objects => _objectsSet;

        public string ObjectsDefine => _objectsSet.Join(" ");
        public string PredicatesDefine => Predicates.Select(u => $"({u.Key} {u.Value})").Join(" ");
        public string ValuesDefine => _values.Select(u => $"(= ({u.Key}) {u.Value:F3})").Join(" ");

        public void AddObjects(string objects)
        {
            _objectsSet.UnionWith(Factory.ParseVariables(objects));
        }

        public void RemoveObjects(string objects)
        {
            _objectsSet.ExceptWith(Factory.ParseVariables(objects));
        }

        public bool HasObjects(string objects)
        {
            var os = Factory.ParseVariables(objects);
            for (var index = 0; index < os.Length; index++)
                if (!_objectsSet.Contains(os[index]))
                    return false;
            return true;
        }

        public float GetValue(string valueName)
        {
            return _values[valueName];
        }

        public bool DirectHasObject(string id)
        {
            return _objectsSet.Contains(id);
        }

        #endregion

        #region Predicates

        private readonly Dictionary<string, HashSet<string>> _predicates = new Dictionary<string, HashSet<string>>();

        public IEnumerable<KeyValuePair<string, string>> Predicates
        {
            get
            {
                foreach (var pair in _predicates)
                foreach (var set in pair.Value)
                    yield return new KeyValuePair<string, string>(pair.Key, set);
            }
        }

        private HashSet<string> GetPredicate(string predicate)
        {
            if (!_predicates.TryGetValue(predicate, out var set))
                _predicates.Add(predicate, set = new HashSet<string>());
            return set;
        }

        public void AddPredicate(string predicate, string variables)
        {
            GetPredicate(predicate).Add(variables);
        }

        public void RemovePredicate(string predicate, string variables)
        {
            GetPredicate(predicate).Remove(variables);
        }

        public bool HasPredicate(string predicate, string variables)
        {
            return GetPredicate(predicate).Contains(variables);
        }

        #endregion

        #region Values

        private readonly Dictionary<string, float> _values = new Dictionary<string, float>();

        public IEnumerable<KeyValuePair<string, float>> Values => _values;
        public void AddValue(string valueName, float value)
        {
            _values[valueName] = value;
        }

        public void RemoveValue(string valueName)
        {
            _values.Remove(valueName);
        }

        public bool HasValue(string valueName)
        {
            return _values.ContainsKey(valueName);
        }

        #endregion
        
        #region Utils

        public IState Clone()
        {
            return Union(this);
        }

        public static State Union(params IState[] states)
        {
            var result = new State();

            foreach (var state in states)
            {
                foreach (var o in state.Objects)
                    result._objectsSet.Add(o);
                foreach (var statePredicate in state.Predicates)
                    result.GetPredicate(statePredicate.Key).Add(statePredicate.Value);
                foreach (var stateValue in state.Values)
                    result._values[stateValue.Key] = stateValue.Value;
            }

            return result;
        }

        public override bool Equals(object obj)
        {
            return obj is IState state && Equals(this, state);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((17 * 23 + ObjectsDefine.GetHashCode()) * 23 + PredicatesDefine.GetHashCode())*23 + ValuesDefine.GetHashCode();
            }
        }

        public static bool Equals(IState state0, IState state1)
        {
            const float Tolerance = 0.01f;
            return state0.HasObjects(state1.Objects.Join(" "))
                   && state1.HasObjects(state0.Objects.Join(" "))
                   && state0.Predicates.All(u => state1.HasPredicate(u.Key, u.Value))
                   && state1.Predicates.All(u => state0.HasPredicate(u.Key, u.Value))
                   && state0.Values.All(u => state1.HasValue(u.Key) && System.Math.Abs(state1.GetValue(u.Key) - u.Value) < Tolerance)
                   && state1.Values.All(u => state0.HasValue(u.Key) && System.Math.Abs(state0.GetValue(u.Key) - u.Value) < Tolerance);
        }

        #endregion
    }
}