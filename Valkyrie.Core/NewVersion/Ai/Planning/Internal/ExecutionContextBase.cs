using System.Collections.Generic;

namespace Valkyrie.Ai.Planning.Internal
{
    class ExecutionContextBase : IPlanningContext
    {
        public readonly string[] Parameters;
        public List<string> Args;
        public IState State { get; set; }
        public IPlanningContext ParentContext;

        public ExecutionContextBase(string[] parameters)
        {
            Parameters = parameters;
        }

        public string GetVariable(string variableName)
        {
            for(var i = 0; i < Parameters.Length; ++i)
                if (variableName == Parameters[i])
                    return Args[i];
            return ParentContext != null
                ? ParentContext.GetVariable(variableName)
                : variableName;
        }

        public float GetValue(string method)
        {
            return State.GetValue(method);
        }

        public void SetValue(string method, float value)
        {
            State.AddValue(method, value);
        }

        public bool HasValue(string s)
        {
            return State.HasValue(s);
        }
    }
}