using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    public static class Factory
    {
        public static IDomainBuilder CreateDomain(string name)
        {
            return new DomainBuilder(name);
        }
        
        public static IProblem CreateProblem(this IDomain domain, string name)
        {
            return new Problem(domain, name);
        }

        #region Utils

        private const string VariableName = @"[A-Za-z][A-Za-z0-9\-]*";
        private static readonly Regex VariablesRegex = new Regex(@"^\??" + VariableName + "$");

        internal static string[] ParseVariables(string variables)
        {
            var result = variables.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var s in result)
                if (!VariablesRegex.IsMatch(s))
                    throw new Exception($"{s} is not valid variable declaration");

            return result;
        }

        internal static List<List<string>> CollectArgs(IState state, int count)
        {
            var os = state.Objects.ToList();
            var argsCollection = new List<List<string>> {new List<string>()};
            for (var i = 0; i < count; ++i)
                Clone(argsCollection, os.Count - 1, (oIndex, list) => list.Add(os[oIndex]));

            return argsCollection;
        }

        private static void Clone(List<List<string>> variants, int cloneCount, Action<int, List<string>> actionForClone)
        {
            var allNew = new List<List<string>>();
            for (var i = 0; i < cloneCount; ++i)
            {
                var temp = variants.Select(u => new List<string>(u)).ToList();
                allNew.AddRange(temp);
                foreach (var list in temp)
                    actionForClone(i + 1, list);
            }

            foreach (var list in variants)
                actionForClone(0, list);

            variants.AddRange(allNew);
        }

        #endregion

        #region Conditions

        #region Conditions factory methods

        public static IActionCondition Empty()
        {
            return new EmptyActionCondition();
        }

        public static IActionCondition And(params IActionCondition[] conditions)
        {
            return new AndActionCondition(conditions);
        }

        public static IActionCondition Or(params IActionCondition[] conditions)
        {
            return new OrActionCondition(conditions);
        }

        public static IActionCondition Not(IActionCondition condition)
        {
            return new NotActionCondition(condition);
        }

        public static IActionCondition Predicate(string name, string parameters)
        {
            return new PredicateActionCondition(name, parameters);
        }

        public static IActionCondition NumericOperator(string op, ArgString arg0, ArgString arg1)
        {
            return new OperatorActionCondition(op, arg0, arg1);
        }

        public static IActionCondition EffectWhen(IActionCondition condition)
        {
            return new WhenEffectActionCondition(condition);
        }

        public static IActionCondition EffectForAll(string args, IActionCondition selectVariablesCondition, IActionCondition effect)
        {
            return new ForAllEffectActionCondition(selectVariablesCondition, effect, ParseVariables(args));
        }

        #endregion

        #region Conditions classes
        
        private class EmptyActionCondition : IActionCondition
        {
            public bool IsMatch(IPlanningContext ctx)
            {
                return true;
            }

            public void Apply(IPlanningContext ctx)
            {
            }
        }

        private class AndActionCondition : IActionCondition
        {
            private readonly IActionCondition[] _innerConditions;

            public AndActionCondition(IActionCondition[] innerConditions)
            {
                _innerConditions = innerConditions;
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                // ReSharper disable once ForCanBeConvertedToForeach
                // ReSharper disable once LoopCanBeConvertedToQuery
                for (var index = 0; index < _innerConditions.Length; index++)
                {
                    var condition = _innerConditions[index];
                    if (!condition.IsMatch(ctx))
                        return false;
                }

                return true;
            }

            public void Apply(IPlanningContext ctx)
            {
                foreach (var condition in _innerConditions)
                    condition.Apply(ctx);
            }

            public override string ToString()
            {
                return $"(and {_innerConditions.Join(" ")})";
            }
        }

        private class OrActionCondition : IActionCondition
        {
            private readonly IActionCondition[] _innerConditions;

            public OrActionCondition(IActionCondition[] innerConditions)
            {
                _innerConditions = innerConditions;
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                // ReSharper disable once ForCanBeConvertedToForeach
                // ReSharper disable once LoopCanBeConvertedToQuery
                for (var index = 0; index < _innerConditions.Length; index++)
                {
                    var condition = _innerConditions[index];
                    if (condition.IsMatch(ctx))
                        return true;
                }

                return false;
            }

            public void Apply(IPlanningContext ctx)
            {
                throw new InvalidOperationException("Or operator could not be used in effect");
            }

            public override string ToString()
            {
                return $"(or {_innerConditions.Join(" ")})";
            }
        }

        private class NotActionCondition : IActionCondition
        {
            private readonly IActionCondition _innerCondition;

            public NotActionCondition(IActionCondition innerCondition)
            {
                _innerCondition = innerCondition;
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                return !_innerCondition.IsMatch(ctx);
            }

            public void Apply(IPlanningContext ctx)
            {
                if (_innerCondition is PredicateActionCondition pac)
                    ctx.State.RemovePredicate(pac.Predicate, pac.GetVariables(ctx));
                else
                    throw new Exception(
                        "Only predicate action condition can be used as inner condition with not operator");
            }

            public override string ToString()
            {
                return $"(not {_innerCondition})";
            }
        }

        private class PredicateActionCondition : IActionCondition
        {
            private readonly string[] _variables;
            public string Predicate { get; }

            public string GetVariables(IPlanningContext ctx)
            {
                return _variables.Select(ctx.GetVariable).ToArray().Join(" ");
            }

            public PredicateActionCondition(string predicate, string variables)
            {
                Predicate = predicate;
                _variables = ParseVariables(variables);
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                return ctx.State.HasPredicate(Predicate, GetVariables(ctx));
            }

            public void Apply(IPlanningContext ctx)
            {
                ctx.State.AddPredicate(Predicate, GetVariables(ctx));
            }

            public override string ToString()
            {
                return $"({Predicate} {_variables.Join(" ")})";
            }
        }
        
        private class OperatorActionCondition : IActionCondition
        {
            private readonly string _op;
            private readonly ArgString _arg0;
            private readonly ArgString _arg1;

            bool GetValues(IPlanningContext ctx, out float arg0, out float arg1)
            {
                if (!_arg0.TryGetValue(ctx, out arg0))
                {
                    arg1 = 0;
                    return false;
                }

                if(!_arg1.TryGetValue(ctx, out arg1))
                    return false;
                
                return true;
            }

            public OperatorActionCondition(string op, ArgString arg0, ArgString arg1)
            {
                _op = op;
                _arg0 = arg0;
                _arg1 = arg1;
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                switch (_op)
                {
                    case "decrease":
                    case "increase":
                    case "assign":
                        throw new InvalidOperationException($"{_op} can not be used in precondition section");
                    case ">":
                    {
                        return GetValues(ctx, out var f0, out var f1) && f0 > f1;
                    }
                    case "<":
                    {
                        return GetValues(ctx, out var f0, out var f1) && f0 < f1;
                    }
                    case ">=":
                    {
                        return GetValues(ctx, out var f0, out var f1) && f0 >= f1;
                    }
                    case "<=":
                    {
                        return GetValues(ctx, out var f0, out var f1) && f0 <= f1;
                    }
                    default:
                        throw new NotImplementedException();
                }
            }

            public void Apply(IPlanningContext ctx)
            {
                switch (_op)
                {
                    case "decrease":
                    {
                        if(GetValues(ctx, out var f0, out var f1))
                            ctx.SetValue(_arg0.GetString(ctx), f0 - f1);
                        else
                            throw new InvalidOperationException($"Can not find {_arg0}, {_arg1}");
                        return;
                    }
                    case "increase":
                    {
                        if(GetValues(ctx, out var f0, out var f1))
                            ctx.SetValue(_arg0.GetString(ctx), f0 + f1);
                        else
                            throw new InvalidOperationException($"Can not find {_arg0}, {_arg1}");
                        return;
                    }
                    case "assign":
                    {
                        if(GetValues(ctx, out var f0, out var f1))
                            ctx.SetValue(_arg0.GetString(ctx), f1);
                        else
                            throw new InvalidOperationException($"Can not find {_arg0}, {_arg1}");
                        return;
                    }
                    case ">":
                    case "<":
                    case ">=":
                    case "<=":
                        throw new InvalidOperationException($"{_op} can not be used in effect section");
                    default:
                        throw new NotImplementedException();
                }
            }

            public override string ToString()
            {
                return $"({_op} {_arg0} {_arg1})";
            }
        }
        
        private class WhenEffectActionCondition : IActionCondition
        {
            private readonly IActionCondition _condition;

            public WhenEffectActionCondition(IActionCondition condition)
            {
                _condition = condition;
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                return _condition.IsMatch(ctx);
            }

            public void Apply(IPlanningContext ctx)
            {
                throw new NotImplementedException();
            }

            public override string ToString()
            {
                return $"(when {_condition})";
            }
        }

        private class ForAllEffectActionCondition : IActionCondition
        {
            private readonly IActionCondition _condition;
            private readonly IActionCondition _effect;
            private readonly string[] _args;

            public ForAllEffectActionCondition(IActionCondition condition, IActionCondition effect, string[] args)
            {
                _condition = condition;
                _effect = effect;
                _args = args;
            }

            public bool IsMatch(IPlanningContext ctx)
            {
                throw new Exception($"Can not use forall effect in precondition section");
            }

            public void Apply(IPlanningContext ctx)
            {
                var argsCollection = CollectArgs(ctx.State, _args.Length);
                foreach (var argsList in argsCollection)
                {
                    var tempCtx = new ExecutionContextBase(_args)
                    {
                        Args = argsList,
                        State = ctx.State,
                        ParentContext = ctx
                    };
                    
                    if (_condition.IsMatch(tempCtx))
                        _effect.Apply(tempCtx);
                }
            }

            public override string ToString()
            {
                return $"(forall ({_args.Join(" ")}) {_effect})";
            }
        }
        
        #endregion

        #endregion
    }
}