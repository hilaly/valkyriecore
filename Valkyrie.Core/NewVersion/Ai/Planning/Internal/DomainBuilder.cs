using System.Collections.Generic;

namespace Valkyrie.Ai.Planning.Internal
{
    internal class DomainBuilder : IDomainBuilder
    {
        private readonly string _name;
        private readonly List<ActionInfo> _actions = new List<ActionInfo>();
        private readonly List<PredicateInfo> _predicates = new List<PredicateInfo>();
        private readonly List<FunctionInfo> _functions = new List<FunctionInfo>();

        public DomainBuilder(string name)
        {
            _name = name;
        }

        public IDomain Build()
        {
            return new Domain(_name, _actions, _predicates, _functions);
        }

        public IDomainBuilder DefinePredicate(string name, string parameters)
        {
            _predicates.Add(new PredicateInfo(name, parameters));
            return this;
        }

        public IDomainBuilder DefineAction(string name, string parameters, IActionCondition precondition, IActionCondition effect, float cost)
        {
            _actions.Add(new ActionInfo(name, parameters, precondition, effect, cost));
            return this;
        }

        public IDomainBuilder DefineFunction(string name, string parameters)
        {
            _functions.Add(new FunctionInfo(name, parameters));
            return this;
        }
    }
}