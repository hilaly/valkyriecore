using System.Linq;
using System.Text;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    internal class Problem : IProblem
    {
        private readonly State _initialState = new State();

        public string Name { get; }
        public IDomain Domain { get; }
        public IState InitialState => _initialState.Clone();
        public IActionCondition Goal { get; private set; }

        public Problem(IDomain domain, string name)
        {
            Domain = domain;
            Name = name;
        }

        public IProblem InitObjects(string list)
        {
            //TODO: type check
            _initialState.AddObjects(list);
            return this;
        }

        public IProblem InitPredicate(string predicate, string args)
        {
            //TODO: type check
            //TODO: predicates define check
            //TODO: objects define check
            _initialState.AddPredicate(predicate, args);
            return this;
        }

        public IProblem InitFunction(string function, string args, float value)
        {
            //TODO: type check
            //TODO: predicates define check
            //TODO: objects define check
            _initialState.AddValue($"({function} {args})", value);
            return this;
        }

        public IProblem InitGoal(IActionCondition condition)
        {
            //TODO: validate condition
            Goal = condition;
            return this;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"(define (problem {Name})");
            sb.Append($"\t(:domain {Domain.Name})");
            var os = _initialState.Objects.ToArray();
            if (os.Length > 0)
                sb.Append($"\n\t(:objects {os.Join(" ")})");
            var predicates = _initialState.Predicates.ToArray();
            var values = _initialState.Values.ToArray();
            if (predicates.Length > 0 || values.Length > 0)
            {
                sb.Append($"\n\t(:init");
                if (predicates.Length > 0)
                    sb.Append($"\n\t\t{predicates.Select(u => $"({u.Key} {u.Value})").Join("\n\t\t")})");
                if (values.Length > 0)
                    sb.Append($"\n\t\t{values.Select(u => $"(= ({u.Key}) {u.Value:F3})").Join("\n\t\t")}");
                sb.Append("\n\t)");
            }
            if (Goal != null)
                sb.Append($"\n(:goal {Goal})");
            sb.Append(")");
            
            return sb.ToString();
        }
    }
}