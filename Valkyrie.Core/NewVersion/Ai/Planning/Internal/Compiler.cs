using System.Collections.Generic;
using System.Linq;
using Valkyrie.Data;
using Valkyrie.Grammar;

namespace Valkyrie.Ai.Planning.Internal
{
    class Compiler : ICompiler
    {
        private readonly IAstConstructor _compilerParser;

        public Compiler(IAstConstructor compilerParser)
        {
            _compilerParser = compilerParser;
        }

        #region ICompiler

        public void Compile(IDomainBuilder domainBuilder, params string[] definesList)
        {
            Compile(domainBuilder, null, definesList);
        }

        public void Compile(IProblem problem, params string[] definesList)
        {
            Compile(null, problem, definesList);
        }

        void Compile(IDomainBuilder domainBuilder, IProblem problem, string[] defines)
        {
            foreach (var text in defines)
            {
                IAstNode ast;
                using (var stream = text.ToStream())
                    ast = _compilerParser.Parse(stream);

                CompileInternal(new CompileContext
                {
                    RootNode = ast,
                    CurrentNode = ast.GetChilds()[0],
                    DomainBuilder = domainBuilder,
                    Problem = problem
                });
            }
        }

        public CompileContext Compile(CompileContext ctx)
        {
            var defs = ctx.CurrentNode.FindAll("DEFINITION");
            if (defs.Count == 0)
                throw new GrammarCompileException(ctx.CurrentNode, $"Have not any defines");
            
            foreach (var def in defs)
            {
                var child = def.GetChilds()[0];
                switch (child.Name)
                {
                    case "<domain>":
                    {
                        var domainFactory = Factory.CreateDomain(child.Find("<domain-symbol>").GetString());
                        ctx.DomainBuilder = domainFactory;
                        ctx.CurrentNode = child;
                        foreach (var cs in child.FindAll("<opt-def>.<predicates-def>.<atomic-formula-skeleton>"))
                        {
                            ctx.CurrentNode = cs;
                            CompileInternal(ctx);
                        }
                        foreach (var cs in child.FindAll("<opt-def>.<functions-def>.<atomic-function-skeleton>"))
                        {
                            ctx.CurrentNode = cs;
                            CompileInternal(ctx);
                        }
                        foreach (var cs in child.FindAll("<action-def>"))
                        {
                            ctx.CurrentNode = cs;
                            CompileInternal(ctx);
                        }
                        ctx.Domains.Add(ctx.DomainBuilder.Build());
                        ctx.DomainBuilder = null;
                        break;
                    }
                    case "<problem>":
                        var domainName = child.Find("<domain-symbol>").GetString();
                        var domain = ctx.Domains.FindLast(u => u.Name == domainName);
                        ctx.CurrentNode = child;
                        ctx.Problem = domain.CreateProblem(child.Find("<problem-symbol>").GetString());
                        foreach (var cs in child.FindAll("<problem-objects>.<typed-list-object>"))
                        {
                            ctx.CurrentNode = cs;
                            CompileInternal(ctx);
                        }
                        foreach (var cs in child.FindAll("<problem-init>.<problem-init-record>.<init-el-function>"))
                        {
                            ctx.CurrentNode = cs;
                            CompileInternal(ctx);
                        }
                        foreach (var cs in child.FindAll("<problem-init>.<problem-init-record>.<init-el-predicate>"))
                        {
                            ctx.CurrentNode = cs;
                            CompileInternal(ctx);
                        }

                        ctx.CurrentNode = child.Find("<goal-def>");
                        CompileInternal(ctx);
                        ctx.Problems.Add(ctx.Problem);
                        ctx.Problem = null;
                        break;
                    default:
                        throw new GrammarCompileException(child, $"Unknown node {child.Name}");
                }
            }
            return ctx;
        }

        #endregion

        #region Compile impl

        void CompileInternal(CompileContext context)
        {
            var node = context.CurrentNode;
            switch (node.Name)
            {
                case "<root>":
                {
                    var temp = node;
                    context.CurrentNode = node.GetChilds()[0];
                    CompileInternal(context);
                    context.CurrentNode = temp;
                    return;
                }
                case "<atomic-function-skeleton>":
                    context.DomainBuilder.DefineFunction(
                        node.Find("<function-symbol>").GetString(),
                        CompileVariables(node.FindAll("<typed-list-variable>")));
                    return;
                case "<atomic-formula-skeleton>":
                    context.DomainBuilder.DefinePredicate(
                        node.Find("<predicate>").GetString(),
                        CompileVariables(node.FindAll("<typed-list-variable>")));
                    return;
                case "<action-def>":
                    context.DomainBuilder.DefineAction(node.Find("<action-symbol>").GetString(),
                        CompileVariables(node.FindAll("<typed-list-variable>")),
                        CompileCondition(node.Find("<action-def-body>.<action-precondition>.<condition>")),
                        CompileEffect(node.Find("<action-def-body>.<action-effect>.<effect>")), 1
                    );
                    return;
                case "<typed-list-object>":
                    context.Problem.InitObjects(
                        CompileVariables(node.FindAll("<object>")));
                    return;
                case "<init-el-predicate>":
                    context.Problem.InitPredicate(
                        node.Find("<predicate>").GetString(),
                        CompileVariables(node.FindAll("<typed-list-object>")));
                    return;
                case "<init-el-function>":
                    context.Problem.InitFunction(
                        node.Find("<function-symbol>").GetString(),
                        CompileVariables(node.FindAll("<typed-list-object>")),
                        node.Find("<number>").GetFloat()
                    );
                    return;
                case "<goal-def>":
                    context.Problem.InitGoal(CompileCondition(node.Find("<condition>")));
                    return;
                default:
                    throw new GrammarCompileException(node);
            }
        }

        IActionCondition CompileCondition(IAstNode node)
        {
            if (node == null)
                return Factory.Empty();
            switch (node.Name)
            {
                case "<condition>":
                {
                    var childs = node.GetChilds();
                    switch (childs[1].Name)
                    {
                        case "and":
                            return Factory.And(node.FindAll("<condition>").Select(CompileCondition).ToArray());
                        case "or":
                            return Factory.Or(node.FindAll("<condition>").Select(CompileCondition).ToArray());
                        case "not":
                            return Factory.Not(CompileCondition(node.Find("<condition>")));
                        case "<op>":
                            var temp = node.FindAll("<op-func>");
                            return Factory.NumericOperator(childs[1].GetString(), CompileOerationFunc(temp[0]),
                                CompileOerationFunc(temp[1]));
                        case "<predicate>":
                            var nodes = node.FindAll("<typed-list-variable>");
                            if (nodes.Count == 0)
                                nodes = node.FindAll("<typed-list-object>");
                            return Factory.Predicate(childs[1].GetString(), CompileVariables(nodes));
                        case "when":
                            return Factory.EffectWhen(CompileCondition(node.Find("<condition>")));
                        default:
                            throw new GrammarCompileException(node, $"Can not compile condition from {node.Name}");
                    }
                }
                default:
                    throw new GrammarCompileException(node, $"Can not compile condition from {node.Name}");
            }
        }

        IActionCondition CompileEffect(IAstNode node)
        {
            if (node == null)
                return Factory.Empty();
            switch (node.Name)
            {
                case "<effect>":
                {
                    var childs = node.GetChilds();
                    switch (childs[1].Name)
                    {
                        case "and":
                            return Factory.And(node.FindAll("<effect>").Select(CompileEffect).ToArray());
                        case "or":
                            return Factory.Or(node.FindAll("<effect>").Select(CompileEffect).ToArray());
                        case "not":
                        {
                            var nodes = node.FindAll("<typed-list-variable>");
                            if (nodes.Count == 0)
                                nodes = node.FindAll("<typed-list-object>");
                            return Factory.Not(Factory.Predicate(node.Find("<predicate>").GetString(),
                                CompileVariables(nodes)));
                        }
                        case "<op>":
                            var temp = node.FindAll("<op-func>");
                            return Factory.NumericOperator(childs[1].GetString(), CompileOerationFunc(temp[0]),
                                CompileOerationFunc(temp[1]));
                        case "<predicate>":
                        {
                            var nodes = node.FindAll("<typed-list-variable>");
                            if (nodes.Count == 0)
                                nodes = node.FindAll("<typed-list-object>");
                            return Factory.Predicate(childs[1].GetString(), CompileVariables(nodes));
                        }
                        case "forall":
                            return Factory.EffectForAll(CompileVariables(node.FindAll("<typed-list-variable>")),
                                CompileCondition(node.Find("<condition>")), CompileEffect(node.Find("<effect>")));
                        default:
                            throw new GrammarCompileException(node, $"Can not compile effect from {node.Name}");
                    }
                }
                default:
                    throw new GrammarCompileException(node, $"Can not compile effect from {node.Name}");
            }
        }

        private ArgString CompileOerationFunc(IAstNode astNode)
        {
            var number = astNode.Find("<number>");
            if (number != null)
                return ArgString.Function(number.GetFloat());
            return ArgString.Function(astNode.Find("<function-symbol>").GetString(),
                CompileVariables(astNode.FindAll("<typed-list-object>")));
        }

        string CompileVariables(IEnumerable<IAstNode> nodes)
        {
            string CompileVariablesList(IAstNode node)
            {
                if (node == null)
                    return "";
                switch (node.Name)
                {
                    case "<object>":
                    case "<variable>":
                        return node.GetString();
                    case "<typed-list-object>":
                        return node.FindAll("<object>").Select(u => u.GetString()).Join(" ");
                    case "<typed-list-variable>":
                        return node.FindAll("<variable>").Select(u => u.GetString()).Join(" ");
                    default:
                        throw new GrammarCompileException(node, $"Can not parse {node.Name} for variables");
                }
            }

            return nodes.Select(CompileVariablesList).Join(" ").Replace("  ", " ");
        }

        #endregion
    }
}