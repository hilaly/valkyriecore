using System.Collections.Generic;
using System.IO;
using Valkyrie.Data;
using Valkyrie.Grammar;

namespace Valkyrie.Ai.Planning.Internal
{
    class PlanningSystem : IPlanningSystem
    {
        #region static
        
        private static IAstConstructor _sdfParser;

        private static IAstConstructor GetPddlParser()
        {
            return _sdfParser ?? (_sdfParser = GetAstConstructorFromEmbeddedResource("pdgLanguage.txt"));
        }

        private static IAstConstructor GetAstConstructorFromEmbeddedResource(string resource)
        {
            using (var stream = DataExtensions.GetEmbeddedResourceStream(resource))
                return Grammar.Grammar.Create(stream);
        }
        
        #endregion

        private readonly HashSet<IDomain> _domains = new HashSet<IDomain>();
        private readonly Compiler _compiler;
        public ICompiler Compiler => _compiler;
        public IAstConstructor Parser => GetPddlParser();
        public IEnumerable<IDomain> Domains => _domains;

        public List<IProblem> Compile(Stream sourceStream)
        {
            var ast = Parser.Parse(sourceStream);
            var ctx = new CompileContext()
            {
                Domains = new List<IDomain>(_domains),
                RootNode = ast,
                CurrentNode = ast
            };
            ctx = _compiler.Compile(ctx);
            foreach (var domain in ctx.Domains)
                _domains.Add(domain);
            return ctx.Problems;
        }

        public PlanningSystem()
        {
            _compiler = new Compiler(Parser);
        }
    }
}