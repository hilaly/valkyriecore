using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    internal class PredicateInfo : IPredicateInfo
    {
        public string Name { get; }
        public int ArgsCount => Parameters.Length;
        
        public string[] Parameters { get; }

        public PredicateInfo(string name, string parameters)
        {
            Name = name;
            Parameters = Factory.ParseVariables(parameters);
        }

        public override string ToString()
        {
            return Parameters.Length > 0
                ? $"({Name} {Parameters.Join(" ")})"
                : $"({Name})";
        }
    }
}