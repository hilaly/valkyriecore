using System.Linq;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    public class ArgString
    {
        private readonly string _function;
        private readonly string[] _args;
        private readonly float _value;

        ArgString(string function, string[] args)
        {
            _function = function;
            _args = args;
        }

        private ArgString(float value)
        {
            _value = value;
        }

        public static ArgString Function(string function, string variables)
        {
            return new ArgString(function, Factory.ParseVariables(variables));
        }

        public static ArgString Function(float value)
        {
            return new ArgString(value);
        }

        public string GetString(IPlanningContext ctx)
        {
            return _function.NotNullOrEmpty()
                ? $"({_function} {_args.Select(ctx.GetVariable).Join(" ")})"
                : _value.ToString("F3");
        }

        public bool TryGetValue(IPlanningContext ctx, out float value)
        {
            if (_function.NotNullOrEmpty())
            {
                var str = GetString(ctx);
                if (ctx.HasValue(str))
                {
                    value = ctx.GetValue(str);
                    return true;
                }
                else
                {
                    value = 0f;
                    return false;
                }
            }
            else
            {
                value = _value;
                return true;
            }
        }

        public override string ToString()
        {
            return _function.NotNullOrEmpty()
                ? $"({_function} {_args.Join(" ")})"
                : _value.ToString("F3");
        }
    }
}