using System.Linq;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    class DomainPlan : IPlan
    {
        public bool IsValid { get; }
        public PlanStep[] Steps { get; }
        public float Cost { get; }

        public DomainPlan(PlanStep[] steps, bool isValid)
        {
            Steps = steps;
            IsValid = isValid;
            Cost = Steps.Sum(u => u.Cost);
        }

        public static DomainPlan Failed => new DomainPlan(new PlanStep[0], false);

        public override string ToString()
        {
            return this.ToStringDesc();
        }
    }
}