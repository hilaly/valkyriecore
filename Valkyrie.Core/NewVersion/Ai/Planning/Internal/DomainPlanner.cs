using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Ai.Planning.Internal
{
    class DomainPlanner : IPlanner
    {
        public IDomain Domain { get; }
        
        public DomainPlanner(IDomain domain)
        {
            Domain = domain;
        }

        public IPlan Resolve(IProblem problem)
        {
            var goal = problem.Goal;

            var closedNodes = new List<DomainPlanState>();
            var openNodes = new List<DomainPlanState>() {new DomainPlanState(problem.InitialState, null, 0, 0, null)};

            while (openNodes.Count > 0)
            {
                var currentNode = openNodes[openNodes.Count - 1];
                openNodes.RemoveAt(openNodes.Count - 1);
                closedNodes.Add(currentNode);

                var actionsList = FindActions(currentNode.State);
                foreach (var actionExecution in actionsList)
                {
                    actionExecution.State = State.Union(currentNode.State);
                    actionExecution.Action.Effect.Apply(actionExecution);
                    if (goal.IsMatch(actionExecution))
                    {
                        return Consruct(currentNode.State, actionExecution, closedNodes);
                    }
                    if(closedNodes.Any(closed => State.Equals(closed.State, actionExecution.State)))
                        continue;
                    if(openNodes.Any(opened => State.Equals(opened.State, actionExecution.State)))
                        continue;
                    
                    //TODO: create heuristics
                    var pathNode = new DomainPlanState(actionExecution.State, currentNode.State, currentNode.G + actionExecution.Action.Cost, 100, actionExecution);
                    bool added = false;
                    for (var i = openNodes.Count - 1; i >= 0; --i)
                    {
                        var test = openNodes[i];
                        if (test.F < pathNode.F)
                            continue;
                        openNodes.Insert(i + 1, pathNode);
                        added = true;
                        break;
                    }
                    if(!added)
                        openNodes.Insert(0, pathNode);
                }
                
                //TODO: remove it
                if(closedNodes.Count > 300)
                    break;
            }

            return DomainPlan.Failed;
        }

        public IPromise<IPlan> ResolveAsync(IDispatcher dispatcher, IProblem problem)
        {
            var result = new Promise<IPlan>();
            dispatcher.ExecuteOnPoolThread(() =>
            {
                try
                {
                    result.Resolve(Resolve(problem));
                }
                catch (Exception e)
                {
                    result.Reject(e);
                }
            });
            return result;
        }

        private IPlan Consruct(IState state, ActionExecutionContext lastAction, List<DomainPlanState> nodes)
        {
            var pathActions = new List<ActionExecutionContext> {lastAction};
            while (state != null)
            {
                var step = nodes.Find(u => ReferenceEquals(u.State, state));
                if (step != null)
                {
                    if(step.Parent != null)
                        pathActions.Insert(0, step.ActionInfo);
                    state = step.Parent;
                }
                else
                    state = null;
            }

            return new DomainPlan(pathActions.Select(u => new PlanStep(u.Action.Name, u.Args.ToArray(), u.Action.Cost)).ToArray(),
                true);
        }

        private List<ActionExecutionContext> FindActions(IState state)
        {
            var result = new List<ActionExecutionContext>();
            foreach (var action in Domain.Actions)
            {
                var argsCollection = Factory.CollectArgs(state, action.ArgsCount);
                foreach (var argsList in argsCollection)
                {
                    var ctx = new ActionExecutionContext(action.Parameters)
                        {Args = argsList, Action = action, State = state};
                    if(action.Precondition.IsMatch(ctx))
                        result.Add(ctx);
                }
            }

            return result;
        }

        class ActionExecutionContext : ExecutionContextBase
        {
            public IActionInfo Action;

            public ActionExecutionContext(string[] parameters):base(parameters)
            {
            }
        }

        class DomainPlanState
        {
            public IState State { get; }
            public IState Parent { get; }
            public ActionExecutionContext ActionInfo { get; }
            public float G;
            public float F;

            public DomainPlanState(IState state, IState parent, float g, float h, ActionExecutionContext actionInfo)
            {
                State = state;
                Parent = parent;
                ActionInfo = actionInfo;
                G = g;
                F = G + h;
            }
        }
    }
}