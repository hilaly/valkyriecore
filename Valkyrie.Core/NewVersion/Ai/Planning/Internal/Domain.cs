using System.Collections.Generic;
using System.Text;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    internal class Domain : IDomain
    {
        public string Name { get; }
        private readonly List<ActionInfo> _actions;
        private readonly List<PredicateInfo> _predicates;
        private readonly List<FunctionInfo> _functions;

        public Domain(string name, List<ActionInfo> actions, List<PredicateInfo> predicates,
            List<FunctionInfo> functions)
        {
            Name = name;
            _actions = actions;
            _predicates = predicates;
            _functions = functions;

            Planner = new DomainPlanner(this);
        }

        public IEnumerable<IFunctionInfo> Functions
        {
            get
            {
                foreach (var functionInfo in _functions)
                    yield return functionInfo;
            }
        }

        public IEnumerable<IActionInfo> Actions
        {
            get
            {
                foreach (var actionInfo in _actions)
                    yield return actionInfo;
            }
        }

        public IEnumerable<IPredicateInfo> Predicates
        {
            get
            {
                foreach (var predicate in _predicates)
                    yield return predicate;
            }
        }

        public IPlanner Planner { get; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"(define (domain {Name})");
            //TODO: add requirements
            if (_predicates.Count > 0)
                sb.AppendLine($"\t(:predicates {_predicates.Join("\n\t\t")})");
            if (_functions.Count > 0)
                sb.AppendLine($"\t(:functions {_functions.Join("\n\t\t")})");
            foreach (var actionInfo in _actions)
            foreach (var s in actionInfo.ToString().Split('\n'))
                sb.Append("\t").AppendLine(s);
            sb.Append(")");

            return sb.ToString();
        }
    }
}