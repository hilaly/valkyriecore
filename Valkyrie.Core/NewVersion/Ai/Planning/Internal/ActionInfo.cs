using System.Text;
using Valkyrie.Data;

namespace Valkyrie.Ai.Planning.Internal
{
    internal class ActionInfo : PredicateInfo, IActionInfo
    {
        public IActionCondition Precondition { get; }
        public IActionCondition Effect { get; }
        public float Cost { get; }

        public ActionInfo(string name, string parameters,
            IActionCondition precondition,
            IActionCondition effect,
            float cost)
            : base(name, parameters)
        {
            Precondition = precondition;
            Effect = effect;
            Cost = cost;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append($"(:action {Name} :parameters ({Parameters.Join(" ")})");
            if (Precondition != null)
                sb.Append($"\n\t:precondition {Precondition}");
            if (Effect != null)
                sb.Append($"\n\t:effect {Effect}");
            sb.Append(")");
            return sb.ToString();
        }
    }
}