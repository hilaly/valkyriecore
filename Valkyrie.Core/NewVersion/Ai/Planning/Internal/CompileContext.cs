using System.Collections.Generic;
using Valkyrie.Grammar;

namespace Valkyrie.Ai.Planning.Internal
{
    class CompileContext
    {
        public IAstNode RootNode;
        public IAstNode CurrentNode;
        public IDomainBuilder DomainBuilder;
        public IProblem Problem;
        
        public List<IDomain> Domains = new List<IDomain>();
        public List<IProblem> Problems = new List<IProblem>();
    }
}