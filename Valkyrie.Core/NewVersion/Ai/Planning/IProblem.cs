namespace Valkyrie.Ai.Planning
{
    public interface IProblem
    {
        string Name { get; }
        IDomain Domain { get; }
        
        IState InitialState { get; }
        IActionCondition Goal { get; }
        
        IProblem InitObjects(string objectsList);
        IProblem InitPredicate(string predicate, string args);
        IProblem InitFunction(string function, string args, float value);
        IProblem InitGoal(IActionCondition condition);
    }
}