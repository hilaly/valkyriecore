﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	/// <summary>
	/// Big integer class based on BouncyCastle (http://www.bouncycastle.org) big integer code
	/// </summary>
	internal class NetBigInteger
	{
		private const long Imask = 0xffffffffL;
		private const ulong Uimask = Imask;

		private static readonly int[] ZeroMagnitude = new int[0];
		private static readonly byte[] ZeroEncoding = new byte[0];

		public static readonly NetBigInteger Zero = new NetBigInteger(0, ZeroMagnitude, false);
		public static readonly NetBigInteger One = CreateUValueOf(1);
		public static readonly NetBigInteger Two = CreateUValueOf(2);
		public static readonly NetBigInteger Three = CreateUValueOf(3);
		public static readonly NetBigInteger Ten = CreateUValueOf(10);

		private const int Chunk2 = 1;
		private static readonly NetBigInteger Radix2 = ValueOf(2);
		private static readonly NetBigInteger Radix2E = Radix2.Pow(Chunk2);

		private const int Chunk10 = 19;
		private static readonly NetBigInteger Radix10 = ValueOf(10);
		private static readonly NetBigInteger Radix10E = Radix10.Pow(Chunk10);

		private const int Chunk16 = 16;
		private static readonly NetBigInteger Radix16 = ValueOf(16);
		private static readonly NetBigInteger Radix16E = Radix16.Pow(Chunk16);

		private const int BitsPerByte = 8;
		private const int BitsPerInt = 32;
		private const int BytesPerInt = 4;

		private int _mSign; // -1 means -ve; +1 means +ve; 0 means 0;
		private int[] _mMagnitude; // array of ints with [0] being the most significant
		private int _mNumBits = -1; // cache BitCount() value
		private int _mNumBitLength = -1; // cache calcBitLength() value
		private long _mQuote = -1L; // -m^(-1) mod b, b = 2^32 (see Montgomery mult.)

		private static int GetByteLength(
			int nBits)
		{
			return (nBits + BitsPerByte - 1) / BitsPerByte;
		}

		private NetBigInteger()
		{
		}

		private NetBigInteger(
			int signum,
			int[] mag,
			bool checkMag)
		{
			if (checkMag)
			{
				var i = 0;
				while (i < mag.Length && mag[i] == 0)
				{
					++i;
				}

				if (i == mag.Length)
				{
					//					sign = 0;
					_mMagnitude = ZeroMagnitude;
				}
				else
				{
					_mSign = signum;

					if (i == 0)
					{
						_mMagnitude = mag;
					}
					else
					{
						// strip leading 0 words
						_mMagnitude = new int[mag.Length - i];
						Array.Copy(mag, i, _mMagnitude, 0, _mMagnitude.Length);
					}
				}
			}
			else
			{
				_mSign = signum;
				_mMagnitude = mag;
			}
		}

		public NetBigInteger(
			string value)
			: this(value, 10)
		{
		}

		public NetBigInteger(
			string str,
			int radix)
		{
			if (str.Length == 0)
				throw new FormatException("Zero length BigInteger");

			NumberStyles style;
			int chunk;
			NetBigInteger r;
			NetBigInteger rE;

			switch (radix)
			{
				case 2:
					// Is there anyway to restrict to binary digits?
					style = NumberStyles.Integer;
					chunk = Chunk2;
					r = Radix2;
					rE = Radix2E;
					break;
				case 10:
					// This style seems to handle spaces and minus sign already (our processing redundant?)
					style = NumberStyles.Integer;
					chunk = Chunk10;
					r = Radix10;
					rE = Radix10E;
					break;
				case 16:
					// TODO Should this be HexNumber?
					style = NumberStyles.AllowHexSpecifier;
					chunk = Chunk16;
					r = Radix16;
					rE = Radix16E;
					break;
				default:
					throw new FormatException("Only bases 2, 10, or 16 allowed");
			}


			var index = 0;
			_mSign = 1;

			if (str[0] == '-')
			{
				if (str.Length == 1)
					throw new FormatException("Zero length BigInteger");

				_mSign = -1;
				index = 1;
			}

			// strip leading zeros from the string str
			while (index < str.Length && Int32.Parse(str[index].ToString(), style) == 0)
			{
				index++;
			}

			if (index >= str.Length)
			{
				// zero value - we're done
				_mSign = 0;
				_mMagnitude = ZeroMagnitude;
				return;
			}

			//////
			// could we work out the max number of ints required to store
			// str.Length digits in the given base, then allocate that
			// storage in one hit?, then Generate the magnitude in one hit too?
			//////

			var b = Zero;


			var next = index + chunk;

			if (next <= str.Length)
			{
				do
				{
					var s = str.Substring(index, chunk);
					var i = ulong.Parse(s, style);
					var bi = CreateUValueOf(i);

					switch (radix)
					{
						case 2:
							if (i > 1)
								throw new FormatException("Bad character in radix 2 string: " + s);

							b = b.ShiftLeft(1);
							break;
						case 16:
							b = b.ShiftLeft(64);
							break;
						default:
							b = b.Multiply(rE);
							break;
					}

					b = b.Add(bi);

					index = next;
					next += chunk;
				}
				while (next <= str.Length);
			}

			if (index < str.Length)
			{
				var s = str.Substring(index);
				var i = ulong.Parse(s, style);
				var bi = CreateUValueOf(i);

				if (b._mSign > 0)
				{
					if (radix == 2)
					{
						// NB: Can't reach here since we are parsing one char at a time
						Debug.Assert(false);
					}
					else if (radix == 16)
					{
						b = b.ShiftLeft(s.Length << 2);
					}
					else
					{
						b = b.Multiply(r.Pow(s.Length));
					}

					b = b.Add(bi);
				}
				else
				{
					b = bi;
				}
			}

			// Note: This is the previous (slower) algorithm
			//			while (index < value.Length)
			//            {
			//				char c = value[index];
			//				string s = c.ToString();
			//				int i = Int32.Parse(s, style);
			//
			//                b = b.Multiply(r).Add(ValueOf(i));
			//                index++;
			//            }

			_mMagnitude = b._mMagnitude;
		}

		public NetBigInteger(
			byte[] bytes)
			: this(bytes, 0, bytes.Length)
		{
		}

		public NetBigInteger(
			byte[] bytes,
			int offset,
			int length)
		{
			if (length == 0)
				throw new FormatException("Zero length BigInteger");
			if ((sbyte)bytes[offset] < 0)
			{
				_mSign = -1;

				var end = offset + length;

				int iBval;
				// strip leading sign bytes
				for (iBval = offset; iBval < end && ((sbyte)bytes[iBval] == -1); iBval++)
				{
				}

				if (iBval >= end)
				{
					_mMagnitude = One._mMagnitude;
				}
				else
				{
					var numBytes = end - iBval;
					var inverse = new byte[numBytes];

					var index = 0;
					while (index < numBytes)
					{
						inverse[index++] = (byte)~bytes[iBval++];
					}

					Debug.Assert(iBval == end);

					while (inverse[--index] == byte.MaxValue)
					{
						inverse[index] = byte.MinValue;
					}

					inverse[index]++;

					_mMagnitude = MakeMagnitude(inverse, 0, inverse.Length);
				}
			}
			else
			{
				// strip leading zero bytes and return magnitude bytes
				_mMagnitude = MakeMagnitude(bytes, offset, length);
				_mSign = _mMagnitude.Length > 0 ? 1 : 0;
			}
		}

		private static int[] MakeMagnitude(
			byte[] bytes,
			int offset,
			int length)
		{
			var end = offset + length;

			// strip leading zeros
			int firstSignificant;
			for (firstSignificant = offset; firstSignificant < end
				&& bytes[firstSignificant] == 0; firstSignificant++)
			{
			}

			if (firstSignificant >= end)
			{
				return ZeroMagnitude;
			}

			var nInts = (end - firstSignificant + 3) / BytesPerInt;
			var bCount = (end - firstSignificant) % BytesPerInt;
			if (bCount == 0)
			{
				bCount = BytesPerInt;
			}

			if (nInts < 1)
			{
				return ZeroMagnitude;
			}

			var mag = new int[nInts];

			var v = 0;
			var magnitudeIndex = 0;
			for (var i = firstSignificant; i < end; ++i)
			{
				v <<= 8;
				v |= bytes[i] & 0xff;
				bCount--;
				if (bCount <= 0)
				{
					mag[magnitudeIndex] = v;
					magnitudeIndex++;
					bCount = BytesPerInt;
					v = 0;
				}
			}

			if (magnitudeIndex < mag.Length)
			{
				mag[magnitudeIndex] = v;
			}

			return mag;
		}

		public NetBigInteger(
			int sign,
			byte[] bytes)
			: this(sign, bytes, 0, bytes.Length)
		{
		}

		public NetBigInteger(
			int sign,
			byte[] bytes,
			int offset,
			int length)
		{
			if (sign < -1 || sign > 1)
				throw new FormatException("Invalid sign value");

			if (sign == 0)
			{
				//sign = 0;
				_mMagnitude = ZeroMagnitude;
			}
			else
			{
				// copy bytes
				_mMagnitude = MakeMagnitude(bytes, offset, length);
				_mSign = _mMagnitude.Length < 1 ? 0 : sign;
			}
		}

		public NetBigInteger Abs()
		{
			return _mSign >= 0 ? this : Negate();
		}

		// return a = a + b - b preserved.
		private static int[] AddMagnitudes(
			int[] a,
			int[] b)
		{
			var tI = a.Length - 1;
			var vI = b.Length - 1;
			long m = 0;

			while (vI >= 0)
			{
				m += ((uint)a[tI] + (long)(uint)b[vI--]);
				a[tI--] = (int)m;
				m = (long)((ulong)m >> 32);
			}

			if (m != 0)
			{
				while (tI >= 0 && ++a[tI--] == 0)
				{
				}
			}

			return a;
		}

		public NetBigInteger Add(
			NetBigInteger value)
		{
			if (_mSign == 0)
				return value;

			if (_mSign != value._mSign)
			{
				if (value._mSign == 0)
					return this;

				if (value._mSign < 0)
					return Subtract(value.Negate());

				return value.Subtract(Negate());
			}

			return AddToMagnitude(value._mMagnitude);
		}

		private NetBigInteger AddToMagnitude(
			int[] magToAdd)
		{
			int[] big, small;
			if (_mMagnitude.Length < magToAdd.Length)
			{
				big = magToAdd;
				small = _mMagnitude;
			}
			else
			{
				big = _mMagnitude;
				small = magToAdd;
			}

			// Conservatively avoid over-allocation when no overflow possible
			var limit = uint.MaxValue;
			if (big.Length == small.Length)
				limit -= (uint)small[0];

			var possibleOverflow = (uint)big[0] >= limit;

			int[] bigCopy;
			if (possibleOverflow)
			{
				bigCopy = new int[big.Length + 1];
				big.CopyTo(bigCopy, 1);
			}
			else
			{
				bigCopy = (int[])big.Clone();
			}

			bigCopy = AddMagnitudes(bigCopy, small);

			return new NetBigInteger(_mSign, bigCopy, possibleOverflow);
		}

		public NetBigInteger And(
			NetBigInteger value)
		{
			if (_mSign == 0 || value._mSign == 0)
			{
				return Zero;
			}

			var aMag = _mSign > 0
				? _mMagnitude
				: Add(One)._mMagnitude;

			var bMag = value._mSign > 0
				? value._mMagnitude
				: value.Add(One)._mMagnitude;

			var resultNeg = _mSign < 0 && value._mSign < 0;
			var resultLength = Math.Max(aMag.Length, bMag.Length);
			var resultMag = new int[resultLength];

			var aStart = resultMag.Length - aMag.Length;
			var bStart = resultMag.Length - bMag.Length;

			for (var i = 0; i < resultMag.Length; ++i)
			{
				var aWord = i >= aStart ? aMag[i - aStart] : 0;
				var bWord = i >= bStart ? bMag[i - bStart] : 0;

				if (_mSign < 0)
				{
					aWord = ~aWord;
				}

				if (value._mSign < 0)
				{
					bWord = ~bWord;
				}

				resultMag[i] = aWord & bWord;

				if (resultNeg)
				{
					resultMag[i] = ~resultMag[i];
				}
			}

			var result = new NetBigInteger(1, resultMag, true);

			if (resultNeg)
			{
				result = result.Not();
			}

			return result;
		}
	
		private int CalcBitLength(
			int indx,
			int[] mag)
		{
			for (; ; )
			{
				if (indx >= mag.Length)
					return 0;

				if (mag[indx] != 0)
					break;

				++indx;
			}

			// bit length for everything after the first int
			var bitLength = 32 * ((mag.Length - indx) - 1);

			// and determine bitlength of first int
			var firstMag = mag[indx];
			bitLength += BitLen(firstMag);

			// Check for negative powers of two
			if (_mSign < 0 && ((firstMag & -firstMag) == firstMag))
			{
				do
				{
					if (++indx >= mag.Length)
					{
						--bitLength;
						break;
					}
				}
				while (mag[indx] == 0);
			}

			return bitLength;
		}

		public int BitLength
		{
			get
			{
				if (_mNumBitLength == -1)
				{
					_mNumBitLength = _mSign == 0
						? 0
						: CalcBitLength(0, _mMagnitude);
				}

				return _mNumBitLength;
			}
		}

		//
		// BitLen(value) is the number of bits in value.
		//
		private static int BitLen(
			int w)
		{
			// Binary search - decision tree (5 tests, rarely 6)
			return (w < 1 << 15 ? (w < 1 << 7
				? (w < 1 << 3 ? (w < 1 << 1
				? (w < 1 << 0 ? (w < 0 ? 32 : 0) : 1)
				: (w < 1 << 2 ? 2 : 3)) : (w < 1 << 5
				? (w < 1 << 4 ? 4 : 5)
				: (w < 1 << 6 ? 6 : 7)))
				: (w < 1 << 11
				? (w < 1 << 9 ? (w < 1 << 8 ? 8 : 9) : (w < 1 << 10 ? 10 : 11))
				: (w < 1 << 13 ? (w < 1 << 12 ? 12 : 13) : (w < 1 << 14 ? 14 : 15)))) : (w < 1 << 23 ? (w < 1 << 19
				? (w < 1 << 17 ? (w < 1 << 16 ? 16 : 17) : (w < 1 << 18 ? 18 : 19))
				: (w < 1 << 21 ? (w < 1 << 20 ? 20 : 21) : (w < 1 << 22 ? 22 : 23))) : (w < 1 << 27
				? (w < 1 << 25 ? (w < 1 << 24 ? 24 : 25) : (w < 1 << 26 ? 26 : 27))
				: (w < 1 << 29 ? (w < 1 << 28 ? 28 : 29) : (w < 1 << 30 ? 30 : 31)))));
		}

		private bool QuickPow2Check()
		{
			return _mSign > 0 && _mNumBits == 1;
		}

		public int CompareTo(
			object obj)
		{
			return CompareTo((NetBigInteger)obj);
		}

		
		// unsigned comparison on two arrays - note the arrays may
		// start with leading zeros.
		private static int CompareTo(
			int xIndx,
			int[] x,
			int yIndx,
			int[] y)
		{
			while (xIndx != x.Length && x[xIndx] == 0)
			{
				xIndx++;
			}

			while (yIndx != y.Length && y[yIndx] == 0)
			{
				yIndx++;
			}

			return CompareNoLeadingZeroes(xIndx, x, yIndx, y);
		}

		private static int CompareNoLeadingZeroes(
			int xIndx,
			int[] x,
			int yIndx,
			int[] y)
		{
			var diff = (x.Length - y.Length) - (xIndx - yIndx);

			if (diff != 0)
			{
				return diff < 0 ? -1 : 1;
			}

			// lengths of magnitudes the same, test the magnitude values

			while (xIndx < x.Length)
			{
				var v1 = (uint)x[xIndx++];
				var v2 = (uint)y[yIndx++];

				if (v1 != v2)
					return v1 < v2 ? -1 : 1;
			}

			return 0;
		}

		public int CompareTo(
			NetBigInteger value)
		{
			return _mSign < value._mSign ? -1
				: _mSign > value._mSign ? 1
				: _mSign == 0 ? 0
				: _mSign * CompareNoLeadingZeroes(0, _mMagnitude, 0, value._mMagnitude);
		}

		// return z = x / y - done in place (z value preserved, x contains the remainder)
		private int[] Divide(
			int[] x,
			int[] y)
		{
			var xStart = 0;
			while (xStart < x.Length && x[xStart] == 0)
			{
				++xStart;
			}

			var yStart = 0;
			while (yStart < y.Length && y[yStart] == 0)
			{
				++yStart;
			}

			Debug.Assert(yStart < y.Length);

			var xyCmp = CompareNoLeadingZeroes(xStart, x, yStart, y);
			int[] count;

			if (xyCmp > 0)
			{
				var yBitLength = CalcBitLength(yStart, y);
				var xBitLength = CalcBitLength(xStart, x);
				var shift = xBitLength - yBitLength;

				int[] iCount;
				var iCountStart = 0;

				int[] c;
				var cStart = 0;
				var cBitLength = yBitLength;
				if (shift > 0)
				{
					//					iCount = ShiftLeft(One.magnitude, shift);
					iCount = new int[(shift >> 5) + 1];
					iCount[0] = 1 << (shift % 32);

					c = ShiftLeft(y, shift);
					cBitLength += shift;
				}
				else
				{
					iCount = new[] { 1 };

					var len = y.Length - yStart;
					c = new int[len];
					Array.Copy(y, yStart, c, 0, len);
				}

				count = new int[iCount.Length];

				for (; ; )
				{
					if (cBitLength < xBitLength
						|| CompareNoLeadingZeroes(xStart, x, cStart, c) >= 0)
					{
						Subtract(xStart, x, cStart, c);
						AddMagnitudes(count, iCount);

						while (x[xStart] == 0)
						{
							if (++xStart == x.Length)
								return count;
						}

						//xBitLength = calcBitLength(xStart, x);
						xBitLength = 32 * (x.Length - xStart - 1) + BitLen(x[xStart]);

						if (xBitLength <= yBitLength)
						{
							if (xBitLength < yBitLength)
								return count;

							xyCmp = CompareNoLeadingZeroes(xStart, x, yStart, y);

							if (xyCmp <= 0)
								break;
						}
					}

					shift = cBitLength - xBitLength;

					// NB: The case where c[cStart] is 1-bit is harmless
					if (shift == 1)
					{
						var firstC = (uint)c[cStart] >> 1;
						var firstX = (uint)x[xStart];
						if (firstC > firstX)
							++shift;
					}

					if (shift < 2)
					{
						c = ShiftRightOneInPlace(cStart, c);
						--cBitLength;
						iCount = ShiftRightOneInPlace(iCountStart, iCount);
					}
					else
					{
						c = ShiftRightInPlace(cStart, c, shift);
						cBitLength -= shift;
						iCount = ShiftRightInPlace(iCountStart, iCount, shift);
					}

					//cStart = c.Length - ((cBitLength + 31) / 32);
					while (c[cStart] == 0)
					{
						++cStart;
					}

					while (iCount[iCountStart] == 0)
					{
						++iCountStart;
					}
				}
			}
			else
			{
				count = new int[1];
			}

			if (xyCmp == 0)
			{
				AddMagnitudes(count, One._mMagnitude);
				Array.Clear(x, xStart, x.Length - xStart);
			}

			return count;
		}

		public NetBigInteger Divide(
			NetBigInteger val)
		{
			if (val._mSign == 0)
				throw new ArithmeticException("Division by zero error");

			if (_mSign == 0)
				return Zero;

			if (val.QuickPow2Check()) // val is power of two
			{
				var result = Abs().ShiftRight(val.Abs().BitLength - 1);
				return val._mSign == _mSign ? result : result.Negate();
			}

			var mag = (int[])_mMagnitude.Clone();

			return new NetBigInteger(_mSign * val._mSign, Divide(mag, val._mMagnitude), true);
		}

		public NetBigInteger[] DivideAndRemainder(
			NetBigInteger val)
		{
			if (val._mSign == 0)
				throw new ArithmeticException("Division by zero error");

			var biggies = new NetBigInteger[2];

			if (_mSign == 0)
			{
				biggies[0] = Zero;
				biggies[1] = Zero;
			}
			else if (val.QuickPow2Check()) // val is power of two
			{
				var e = val.Abs().BitLength - 1;
				var quotient = Abs().ShiftRight(e);
				var remainder = LastNBits(e);

				biggies[0] = val._mSign == _mSign ? quotient : quotient.Negate();
				biggies[1] = new NetBigInteger(_mSign, remainder, true);
			}
			else
			{
				var remainder = (int[])_mMagnitude.Clone();
				var quotient = Divide(remainder, val._mMagnitude);

				biggies[0] = new NetBigInteger(_mSign * val._mSign, quotient, true);
				biggies[1] = new NetBigInteger(_mSign, remainder, true);
			}

			return biggies;
		}

		public override bool Equals(
			object obj)
		{
			if (obj == this)
				return true;

			var biggie = obj as NetBigInteger;
			if (biggie == null)
				return false;

			if (biggie._mSign != _mSign || biggie._mMagnitude.Length != _mMagnitude.Length)
				return false;

			for (var i = 0; i < _mMagnitude.Length; i++)
			{
				if (biggie._mMagnitude[i] != _mMagnitude[i])
				{
					return false;
				}
			}

			return true;
		}

		public NetBigInteger Gcd(
			NetBigInteger value)
		{
			if (value._mSign == 0)
				return Abs();

			if (_mSign == 0)
				return value.Abs();

		    var u = this;
			var v = value;

			while (v._mSign != 0)
			{
				var r = u.Mod(v);
				u = v;
				v = r;
			}

			return u;
		}

		public override int GetHashCode()
		{
			var hc = _mMagnitude.Length;
			if (_mMagnitude.Length > 0)
			{
				hc ^= _mMagnitude[0];

				if (_mMagnitude.Length > 1)
				{
					hc ^= _mMagnitude[_mMagnitude.Length - 1];
				}
			}

			return _mSign < 0 ? ~hc : hc;
		}

		private NetBigInteger Inc()
		{
			if (_mSign == 0)
				return One;

			if (_mSign < 0)
				return new NetBigInteger(-1, DoSubBigLil(_mMagnitude, One._mMagnitude), true);

			return AddToMagnitude(One._mMagnitude);
		}

		public int IntValue
		{
			get
			{
				return _mSign == 0 ? 0
					: _mSign > 0 ? _mMagnitude[_mMagnitude.Length - 1]
					: -_mMagnitude[_mMagnitude.Length - 1];
			}
		}
	
		public NetBigInteger Max(
			NetBigInteger value)
		{
			return CompareTo(value) > 0 ? this : value;
		}

		public NetBigInteger Min(
			NetBigInteger value)
		{
			return CompareTo(value) < 0 ? this : value;
		}

		public NetBigInteger Mod(
			NetBigInteger m)
		{
			if (m._mSign < 1)
				throw new ArithmeticException("Modulus must be positive");

			var biggie = Remainder(m);

			return (biggie._mSign >= 0 ? biggie : biggie.Add(m));
		}

		public NetBigInteger ModInverse(
			NetBigInteger m)
		{
			if (m._mSign < 1)
				throw new ArithmeticException("Modulus must be positive");

			var x = new NetBigInteger();
			var gcd = ExtEuclid(this, m, x, null);

			if (!gcd.Equals(One))
				throw new ArithmeticException("Numbers not relatively prime.");

			if (x._mSign < 0)
			{
				x._mSign = 1;
				//x = m.Subtract(x);
				x._mMagnitude = DoSubBigLil(m._mMagnitude, x._mMagnitude);
			}

			return x;
		}

		private static NetBigInteger ExtEuclid(
			NetBigInteger a,
			NetBigInteger b,
			NetBigInteger u1Out,
			NetBigInteger u2Out)
		{
			var u1 = One;
			var u3 = a;
			var v1 = Zero;
			var v3 = b;

			while (v3._mSign > 0)
			{
				var q = u3.DivideAndRemainder(v3);

				var tmp = v1.Multiply(q[0]);
				var tn = u1.Subtract(tmp);
				u1 = v1;
				v1 = tn;

				u3 = v3;
				v3 = q[1];
			}

			if (u1Out != null)
			{
				u1Out._mSign = u1._mSign;
				u1Out._mMagnitude = u1._mMagnitude;
			}

			if (u2Out != null)
			{
				var tmp = u1.Multiply(a);
				tmp = u3.Subtract(tmp);
				var res = tmp.Divide(b);
				u2Out._mSign = res._mSign;
				u2Out._mMagnitude = res._mMagnitude;
			}

			return u3;
		}

		private static void ZeroOut(
			int[] x)
		{
			Array.Clear(x, 0, x.Length);
		}

		public NetBigInteger ModPow(
			NetBigInteger exponent,
			NetBigInteger m)
		{
			if (m._mSign < 1)
				throw new ArithmeticException("Modulus must be positive");

			if (m.Equals(One))
				return Zero;

			if (exponent._mSign == 0)
				return One;

			if (_mSign == 0)
				return Zero;

			int[] zVal = null;
			int[] yAccum = null;

		    // Montgomery exponentiation is only possible if the modulus is odd,
			// but AFAIK, this is always the case for crypto algo's
			var useMonty = ((m._mMagnitude[m._mMagnitude.Length - 1] & 1) == 1);
			long mQ = 0;
			if (useMonty)
			{
				mQ = m.GetMQuote();

				// tmp = this * R mod m
				var tmp = ShiftLeft(32 * m._mMagnitude.Length).Mod(m);
				zVal = tmp._mMagnitude;

				useMonty = (zVal.Length <= m._mMagnitude.Length);

				if (useMonty)
				{
					yAccum = new int[m._mMagnitude.Length + 1];
					if (zVal.Length < m._mMagnitude.Length)
					{
						var longZ = new int[m._mMagnitude.Length];
						zVal.CopyTo(longZ, longZ.Length - zVal.Length);
						zVal = longZ;
					}
				}
			}

			if (!useMonty)
			{
				if (_mMagnitude.Length <= m._mMagnitude.Length)
				{
					//zAccum = new int[m.magnitude.Length * 2];
					zVal = new int[m._mMagnitude.Length];
					_mMagnitude.CopyTo(zVal, zVal.Length - _mMagnitude.Length);
				}
				else
				{
					//
					// in normal practice we'll never see ..
					//
					var tmp = Remainder(m);

					//zAccum = new int[m.magnitude.Length * 2];
					zVal = new int[m._mMagnitude.Length];
					tmp._mMagnitude.CopyTo(zVal, zVal.Length - tmp._mMagnitude.Length);
				}

				yAccum = new int[m._mMagnitude.Length * 2];
			}

			var yVal = new int[m._mMagnitude.Length];

			//
			// from LSW to MSW
			//
			for (var i = 0; i < exponent._mMagnitude.Length; i++)
			{
				var v = exponent._mMagnitude[i];
				var bits = 0;

				if (i == 0)
				{
					while (v > 0)
					{
						v <<= 1;
						bits++;
					}

					//
					// first time in initialise y
					//
					zVal.CopyTo(yVal, 0);

					v <<= 1;
					bits++;
				}

				while (v != 0)
				{
					if (useMonty)
					{
						// Montgomery square algo doesn't exist, and a normal
						// square followed by a Montgomery reduction proved to
						// be almost as heavy as a Montgomery mulitply.
						MultiplyMonty(yAccum, yVal, yVal, m._mMagnitude, mQ);
					}
					else
					{
						Square(yAccum, yVal);
						Remainder(yAccum, m._mMagnitude);
						Array.Copy(yAccum, yAccum.Length - yVal.Length, yVal, 0, yVal.Length);
						ZeroOut(yAccum);
					}
					bits++;

					if (v < 0)
					{
						if (useMonty)
						{
							MultiplyMonty(yAccum, yVal, zVal, m._mMagnitude, mQ);
						}
						else
						{
							Multiply(yAccum, yVal, zVal);
							Remainder(yAccum, m._mMagnitude);
							Array.Copy(yAccum, yAccum.Length - yVal.Length, yVal, 0,
								yVal.Length);
							ZeroOut(yAccum);
						}
					}

					v <<= 1;
				}

				while (bits < 32)
				{
					if (useMonty)
					{
						MultiplyMonty(yAccum, yVal, yVal, m._mMagnitude, mQ);
					}
					else
					{
						Square(yAccum, yVal);
						Remainder(yAccum, m._mMagnitude);
						Array.Copy(yAccum, yAccum.Length - yVal.Length, yVal, 0, yVal.Length);
						ZeroOut(yAccum);
					}
					bits++;
				}
			}

			if (useMonty)
			{
				// Return y * R^(-1) mod m by doing y * 1 * R^(-1) mod m
				ZeroOut(zVal);
				zVal[zVal.Length - 1] = 1;
				MultiplyMonty(yAccum, yVal, zVal, m._mMagnitude, mQ);
			}

			var result = new NetBigInteger(1, yVal, true);

			return exponent._mSign > 0
				? result
				: result.ModInverse(m);
		}

		// return w with w = x * x - w is assumed to have enough space.
		private static int[] Square(
			int[] w,
			int[] x)
		{
			// Note: this method allows w to be only (2 * x.Length - 1) words if result will fit
			//			if (w.Length != 2 * x.Length)
			//				throw new ArgumentException("no I don't think so...");

		    ulong u1, u2;

		    var wBase = w.Length - 1;

			for (var i = x.Length - 1; i != 0; i--)
			{
				ulong v = (uint)x[i];

				u1 = v * v;
				u2 = u1 >> 32;
				u1 = (uint)u1;

				u1 += (uint)w[wBase];

				w[wBase] = (int)(uint)u1;
				var c = u2 + (u1 >> 32);

				for (var j = i - 1; j >= 0; j--)
				{
					--wBase;
					u1 = v * (uint)x[j];
					u2 = u1 >> 31; // multiply by 2!
					u1 = (uint)(u1 << 1); // multiply by 2!
					u1 += c + (uint)w[wBase];

					w[wBase] = (int)(uint)u1;
					c = u2 + (u1 >> 32);
				}

				c += (uint)w[--wBase];
				w[wBase] = (int)(uint)c;

				if (--wBase >= 0)
				{
					w[wBase] = (int)(uint)(c >> 32);
				}
				else
				{
					Debug.Assert((uint)(c >> 32) == 0);
				}
				wBase += i;
			}

			u1 = (uint)x[0];
			u1 = u1 * u1;
			u2 = u1 >> 32;
			u1 = u1 & Imask;

			u1 += (uint)w[wBase];

			w[wBase] = (int)(uint)u1;
			if (--wBase >= 0)
			{
				w[wBase] = (int)(uint)(u2 + (u1 >> 32) + (uint)w[wBase]);
			}
			else
			{
				Debug.Assert((uint)(u2 + (u1 >> 32)) == 0);
			}

			return w;
		}

		// return x with x = y * z - x is assumed to have enough space.
		private static int[] Multiply(
			int[] x,
			int[] y,
			int[] z)
		{
			var i = z.Length;

			if (i < 1)
				return x;

			var xBase = x.Length - y.Length;

			for (; ; )
			{
				var a = z[--i] & Imask;
				long val = 0;

				for (var j = y.Length - 1; j >= 0; j--)
				{
					val += a * (y[j] & Imask) + (x[xBase + j] & Imask);

					x[xBase + j] = (int)val;

					val = (long)((ulong)val >> 32);
				}

				--xBase;

				if (i < 1)
				{
					if (xBase >= 0)
					{
						x[xBase] = (int)val;
					}
					else
					{
						Debug.Assert(val == 0);
					}
					break;
				}

				x[xBase] = (int)val;
			}

			return x;
		}

		private static long FastExtEuclid(
			long a,
			long b,
			long[] uOut)
		{
			long u1 = 1;
			var u3 = a;
			long v1 = 0;
			var v3 = b;

			while (v3 > 0)
			{
			    var q = u3 / v3;

				var tn = u1 - (v1 * q);
				u1 = v1;
				v1 = tn;

				tn = u3 - (v3 * q);
				u3 = v3;
				v3 = tn;
			}

			uOut[0] = u1;
			uOut[1] = (u3 - (u1 * a)) / b;

			return u3;
		}

		private static long FastModInverse(
			long v,
			long m)
		{
			if (m < 1)
				throw new ArithmeticException("Modulus must be positive");

			var x = new long[2];
			var gcd = FastExtEuclid(v, m, x);

			if (gcd != 1)
				throw new ArithmeticException("Numbers not relatively prime.");

			if (x[0] < 0)
			{
				x[0] += m;
			}

			return x[0];
		}

		private long GetMQuote()
		{
			Debug.Assert(_mSign > 0);

			if (_mQuote != -1)
			{
				return _mQuote; // already calculated
			}

			if (_mMagnitude.Length == 0 || (_mMagnitude[_mMagnitude.Length - 1] & 1) == 0)
			{
				return -1; // not for even numbers
			}

			var v = (((~_mMagnitude[_mMagnitude.Length - 1]) | 1) & 0xffffffffL);
			_mQuote = FastModInverse(v, 0x100000000L);

			return _mQuote;
		}

		private static void MultiplyMonty(
			int[] a,
			int[] x,
			int[] y,
			int[] m,
			long mQuote)
		// mQuote = -m^(-1) mod b
		{
			if (m.Length == 1)
			{
				x[0] = (int)MultiplyMontyNIsOne((uint)x[0], (uint)y[0], (uint)m[0], (ulong)mQuote);
				return;
			}

			var n = m.Length;
			var nMinus1 = n - 1;
			var y0 = y[nMinus1] & Imask;

			// 1. a = 0 (Notation: a = (a_{n} a_{n-1} ... a_{0})_{b} )
			Array.Clear(a, 0, n + 1);

			// 2. for i from 0 to (n - 1) do the following:
			for (var i = n; i > 0; i--)
			{
				var xI = x[i - 1] & Imask;

				// 2.1 u = ((a[0] + (x[i] * y[0]) * mQuote) mod b
				var u = ((((a[n] & Imask) + ((xI * y0) & Imask)) & Imask) * mQuote) & Imask;

				// 2.2 a = (a + x_i * y + u * m) / b
				var prod1 = xI * y0;
				var prod2 = u * (m[nMinus1] & Imask);
				var tmp = (a[n] & Imask) + (prod1 & Imask) + (prod2 & Imask);
				var carry = (long)((ulong)prod1 >> 32) + (long)((ulong)prod2 >> 32) + (long)((ulong)tmp >> 32);
				for (var j = nMinus1; j > 0; j--)
				{
					prod1 = xI * (y[j - 1] & Imask);
					prod2 = u * (m[j - 1] & Imask);
					tmp = (a[j] & Imask) + (prod1 & Imask) + (prod2 & Imask) + (carry & Imask);
					carry = (long)((ulong)carry >> 32) + (long)((ulong)prod1 >> 32) +
						(long)((ulong)prod2 >> 32) + (long)((ulong)tmp >> 32);
					a[j + 1] = (int)tmp; // division by b
				}
				carry += (a[0] & Imask);
				a[1] = (int)carry;
				a[0] = (int)((ulong)carry >> 32); // OJO!!!!!
			}

			// 3. if x >= m the x = x - m
			if (CompareTo(0, a, 0, m) >= 0)
			{
				Subtract(0, a, 0, m);
			}

			// put the result in x
			Array.Copy(a, 1, x, 0, n);
		}

		private static uint MultiplyMontyNIsOne(
			uint x,
			uint y,
			uint m,
			ulong mQuote)
		{
			ulong um = m;
			var prod1 = x * (ulong)y;
			var u = (prod1 * mQuote) & Uimask;
			var prod2 = u * um;
			var tmp = (prod1 & Uimask) + (prod2 & Uimask);
			var carry = (prod1 >> 32) + (prod2 >> 32) + (tmp >> 32);

			if (carry > um)
			{
				carry -= um;
			}

			return (uint)(carry & Uimask);
		}

		public NetBigInteger Modulus(
			NetBigInteger val)
		{
			return Mod(val);
		}

		public NetBigInteger Multiply(
			NetBigInteger val)
		{
			if (_mSign == 0 || val._mSign == 0)
				return Zero;

			if (val.QuickPow2Check()) // val is power of two
			{
				var result = ShiftLeft(val.Abs().BitLength - 1);
				return val._mSign > 0 ? result : result.Negate();
			}

			if (QuickPow2Check()) // this is power of two
			{
				var result = val.ShiftLeft(Abs().BitLength - 1);
				return _mSign > 0 ? result : result.Negate();
			}

			var maxBitLength = BitLength + val.BitLength;
			var resLength = (maxBitLength + BitsPerInt - 1) / BitsPerInt;

			var res = new int[resLength];

			if (val == this)
			{
				Square(res, _mMagnitude);
			}
			else
			{
				Multiply(res, _mMagnitude, val._mMagnitude);
			}

			return new NetBigInteger(_mSign * val._mSign, res, true);
		}

		public NetBigInteger Negate()
		{
			if (_mSign == 0)
				return this;

			return new NetBigInteger(-_mSign, _mMagnitude, false);
		}

		public NetBigInteger Not()
		{
			return Inc().Negate();
		}

		public NetBigInteger Pow(int exp)
		{
			if (exp < 0)
			{
				throw new ArithmeticException("Negative exponent");
			}

			if (exp == 0)
			{
				return One;
			}

			if (_mSign == 0 || Equals(One))
			{
				return this;
			}

			var y = One;
			var z = this;

			for (; ; )
			{
				if ((exp & 0x1) == 1)
				{
					y = y.Multiply(z);
				}
				exp >>= 1;
				if (exp == 0) break;
				z = z.Multiply(z);
			}

			return y;
		}
		
		private int Remainder(
			int m)
		{
			Debug.Assert(m > 0);

			long acc = 0;
			for (var pos = 0; pos < _mMagnitude.Length; ++pos)
			{
				long posVal = (uint)_mMagnitude[pos];
				acc = (acc << 32 | posVal) % m;
			}

			return (int)acc;
		}

		// return x = x % y - done in place (y value preserved)
		private int[] Remainder(
			int[] x,
			int[] y)
		{
			var xStart = 0;
			while (xStart < x.Length && x[xStart] == 0)
			{
				++xStart;
			}

			var yStart = 0;
			while (yStart < y.Length && y[yStart] == 0)
			{
				++yStart;
			}

			Debug.Assert(yStart < y.Length);

			var xyCmp = CompareNoLeadingZeroes(xStart, x, yStart, y);

			if (xyCmp > 0)
			{
				var yBitLength = CalcBitLength(yStart, y);
				var xBitLength = CalcBitLength(xStart, x);
				var shift = xBitLength - yBitLength;

				int[] c;
				var cStart = 0;
				var cBitLength = yBitLength;
				if (shift > 0)
				{
					c = ShiftLeft(y, shift);
					cBitLength += shift;
					Debug.Assert(c[0] != 0);
				}
				else
				{
					var len = y.Length - yStart;
					c = new int[len];
					Array.Copy(y, yStart, c, 0, len);
				}

				for (; ; )
				{
					if (cBitLength < xBitLength
						|| CompareNoLeadingZeroes(xStart, x, cStart, c) >= 0)
					{
						Subtract(xStart, x, cStart, c);

						while (x[xStart] == 0)
						{
							if (++xStart == x.Length)
								return x;
						}

						//xBitLength = calcBitLength(xStart, x);
						xBitLength = 32 * (x.Length - xStart - 1) + BitLen(x[xStart]);

						if (xBitLength <= yBitLength)
						{
							if (xBitLength < yBitLength)
								return x;

							xyCmp = CompareNoLeadingZeroes(xStart, x, yStart, y);

							if (xyCmp <= 0)
								break;
						}
					}

					shift = cBitLength - xBitLength;

					// NB: The case where c[cStart] is 1-bit is harmless
					if (shift == 1)
					{
						var firstC = (uint)c[cStart] >> 1;
						var firstX = (uint)x[xStart];
						if (firstC > firstX)
							++shift;
					}

					if (shift < 2)
					{
						c = ShiftRightOneInPlace(cStart, c);
						--cBitLength;
					}
					else
					{
						c = ShiftRightInPlace(cStart, c, shift);
						cBitLength -= shift;
					}

					//cStart = c.Length - ((cBitLength + 31) / 32);
					while (c[cStart] == 0)
					{
						++cStart;
					}
				}
			}

			if (xyCmp == 0)
			{
				Array.Clear(x, xStart, x.Length - xStart);
			}

			return x;
		}

		public NetBigInteger Remainder(
			NetBigInteger n)
		{
			if (n._mSign == 0)
				throw new ArithmeticException("Division by zero error");

			if (_mSign == 0)
				return Zero;

			// For small values, use fast remainder method
			if (n._mMagnitude.Length == 1)
			{
				var val = n._mMagnitude[0];

				if (val > 0)
				{
					if (val == 1)
						return Zero;

					var rem = Remainder(val);

					return rem == 0
						? Zero
						: new NetBigInteger(_mSign, new[] { rem }, false);
				}
			}

			if (CompareNoLeadingZeroes(0, _mMagnitude, 0, n._mMagnitude) < 0)
				return this;

			int[] result;
			if (n.QuickPow2Check())  // n is power of two
			{
				result = LastNBits(n.Abs().BitLength - 1);
			}
			else
			{
				result = (int[])_mMagnitude.Clone();
				result = Remainder(result, n._mMagnitude);
			}

			return new NetBigInteger(_mSign, result, true);
		}

		private int[] LastNBits(
			int n)
		{
			if (n < 1)
				return ZeroMagnitude;

			var numWords = (n + BitsPerInt - 1) / BitsPerInt;
			numWords = Math.Min(numWords, _mMagnitude.Length);
			var result = new int[numWords];

			Array.Copy(_mMagnitude, _mMagnitude.Length - numWords, result, 0, numWords);

			var hiBits = n % 32;
			if (hiBits != 0)
			{
				result[0] &= ~(-1 << hiBits);
			}

			return result;
		}


		// do a left shift - this returns a new array.
		private static int[] ShiftLeft(
			int[] mag,
			int n)
		{
			var nInts = (int)((uint)n >> 5);
			var nBits = n & 0x1f;
			var magLen = mag.Length;
			int[] newMag;

			if (nBits == 0)
			{
				newMag = new int[magLen + nInts];
				mag.CopyTo(newMag, 0);
			}
			else
			{
				var i = 0;
				var nBits2 = 32 - nBits;
				var highBits = (int)((uint)mag[0] >> nBits2);

				if (highBits != 0)
				{
					newMag = new int[magLen + nInts + 1];
					newMag[i++] = highBits;
				}
				else
				{
					newMag = new int[magLen + nInts];
				}

				var m = mag[0];
				for (var j = 0; j < magLen - 1; j++)
				{
					var next = mag[j + 1];

					newMag[i++] = (m << nBits) | (int)((uint)next >> nBits2);
					m = next;
				}

				newMag[i] = mag[magLen - 1] << nBits;
			}

			return newMag;
		}

		public NetBigInteger ShiftLeft(
			int n)
		{
			if (_mSign == 0 || _mMagnitude.Length == 0)
				return Zero;

			if (n == 0)
				return this;

			if (n < 0)
				return ShiftRight(-n);

			var result = new NetBigInteger(_mSign, ShiftLeft(_mMagnitude, n), true);

			if (_mNumBits != -1)
			{
				result._mNumBits = _mSign > 0
					? _mNumBits
					: _mNumBits + n;
			}

			if (_mNumBitLength != -1)
			{
				result._mNumBitLength = _mNumBitLength + n;
			}

			return result;
		}

		// do a right shift - this does it in place.
		private static int[] ShiftRightInPlace(
			int start,
			int[] mag,
			int n)
		{
			var nInts = (int)((uint)n >> 5) + start;
			var nBits = n & 0x1f;
			var magEnd = mag.Length - 1;

			if (nInts != start)
			{
				var delta = (nInts - start);

				for (var i = magEnd; i >= nInts; i--)
				{
					mag[i] = mag[i - delta];
				}
				for (var i = nInts - 1; i >= start; i--)
				{
					mag[i] = 0;
				}
			}

			if (nBits != 0)
			{
				var nBits2 = 32 - nBits;
				var m = mag[magEnd];

				for (var i = magEnd; i > nInts; --i)
				{
					var next = mag[i - 1];

					mag[i] = (int)((uint)m >> nBits) | (next << nBits2);
					m = next;
				}

				mag[nInts] = (int)((uint)mag[nInts] >> nBits);
			}

			return mag;
		}

		// do a right shift by one - this does it in place.
		private static int[] ShiftRightOneInPlace(
			int start,
			int[] mag)
		{
			var i = mag.Length;
			var m = mag[i - 1];

			while (--i > start)
			{
				var next = mag[i - 1];
				mag[i] = ((int)((uint)m >> 1)) | (next << 31);
				m = next;
			}

			mag[start] = (int)((uint)mag[start] >> 1);

			return mag;
		}

		public NetBigInteger ShiftRight(
			int n)
		{
			if (n == 0)
				return this;

			if (n < 0)
				return ShiftLeft(-n);

			if (n >= BitLength)
				return (_mSign < 0 ? One.Negate() : Zero);

			//			int[] res = (int[]) magnitude.Clone();
			//
			//			res = ShiftRightInPlace(0, res, n);
			//
			//			return new BigInteger(sign, res, true);

			var resultLength = (BitLength - n + 31) >> 5;
			var res = new int[resultLength];

			var numInts = n >> 5;
			var numBits = n & 31;

			if (numBits == 0)
			{
				Array.Copy(_mMagnitude, 0, res, 0, res.Length);
			}
			else
			{
				var numBits2 = 32 - numBits;

				var magPos = _mMagnitude.Length - 1 - numInts;
				for (var i = resultLength - 1; i >= 0; --i)
				{
					res[i] = (int)((uint)_mMagnitude[magPos--] >> numBits);

					if (magPos >= 0)
					{
						res[i] |= _mMagnitude[magPos] << numBits2;
					}
				}
			}

			Debug.Assert(res[0] != 0);

			return new NetBigInteger(_mSign, res, false);
		}

		public int SignValue
		{
			get { return _mSign; }
		}

		// returns x = x - y - we assume x is >= y
		private static int[] Subtract(
			int xStart,
			int[] x,
			int yStart,
			int[] y)
		{
			Debug.Assert(yStart < y.Length);
			Debug.Assert(x.Length - xStart >= y.Length - yStart);

			var iT = x.Length;
			var iV = y.Length;
		    var borrow = 0;

			do
			{
				var m = (x[--iT] & Imask) - (y[--iV] & Imask) + borrow;
				x[iT] = (int)m;

				//				borrow = (m < 0) ? -1 : 0;
				borrow = (int)(m >> 63);
			}
			while (iV > yStart);

			if (borrow != 0)
			{
				while (--x[--iT] == -1)
				{
				}
			}

			return x;
		}

		public NetBigInteger Subtract(
			NetBigInteger n)
		{
			if (n._mSign == 0)
				return this;

			if (_mSign == 0)
				return n.Negate();

			if (_mSign != n._mSign)
				return Add(n.Negate());

			var compare = CompareNoLeadingZeroes(0, _mMagnitude, 0, n._mMagnitude);
			if (compare == 0)
				return Zero;

			NetBigInteger bigun, lilun;
			if (compare < 0)
			{
				bigun = n;
				lilun = this;
			}
			else
			{
				bigun = this;
				lilun = n;
			}

			return new NetBigInteger(_mSign * compare, DoSubBigLil(bigun._mMagnitude, lilun._mMagnitude), true);
		}

		private static int[] DoSubBigLil(
			int[] bigMag,
			int[] lilMag)
		{
			var res = (int[])bigMag.Clone();

			return Subtract(0, res, 0, lilMag);
		}

		public byte[] ToByteArray()
		{
			return ToByteArray(false);
		}

		public byte[] ToByteArrayUnsigned()
		{
			return ToByteArray(true);
		}

		private byte[] ToByteArray(
			bool unsigned)
		{
			if (_mSign == 0)
				return unsigned ? ZeroEncoding : new byte[1];

			var nBits = (unsigned && _mSign > 0)
				? BitLength
				: BitLength + 1;

			var nBytes = GetByteLength(nBits);
			var bytes = new byte[nBytes];

			var magIndex = _mMagnitude.Length;
			var bytesIndex = bytes.Length;

			if (_mSign > 0)
			{
				while (magIndex > 1)
				{
					var mag = (uint)_mMagnitude[--magIndex];
					bytes[--bytesIndex] = (byte)mag;
					bytes[--bytesIndex] = (byte)(mag >> 8);
					bytes[--bytesIndex] = (byte)(mag >> 16);
					bytes[--bytesIndex] = (byte)(mag >> 24);
				}

				var lastMag = (uint)_mMagnitude[0];
				while (lastMag > byte.MaxValue)
				{
					bytes[--bytesIndex] = (byte)lastMag;
					lastMag >>= 8;
				}

				bytes[--bytesIndex] = (byte)lastMag;
			}
			else // sign < 0
			{
				var carry = true;

				while (magIndex > 1)
				{
					var mag = ~((uint)_mMagnitude[--magIndex]);

					if (carry)
					{
						carry = (++mag == uint.MinValue);
					}

					bytes[--bytesIndex] = (byte)mag;
					bytes[--bytesIndex] = (byte)(mag >> 8);
					bytes[--bytesIndex] = (byte)(mag >> 16);
					bytes[--bytesIndex] = (byte)(mag >> 24);
				}

				var lastMag = (uint)_mMagnitude[0];

				if (carry)
				{
					// Never wraps because magnitude[0] != 0
					--lastMag;
				}

				while (lastMag > byte.MaxValue)
				{
					bytes[--bytesIndex] = (byte)~lastMag;
					lastMag >>= 8;
				}

				bytes[--bytesIndex] = (byte)~lastMag;

				if (bytesIndex > 0)
				{
					bytes[--bytesIndex] = byte.MaxValue;
				}
			}

			return bytes;
		}

		public override string ToString()
		{
			return ToString(10);
		}

		public string ToString(
			int radix)
		{
			switch (radix)
			{
				case 2:
				case 10:
				case 16:
					break;
				default:
					throw new FormatException("Only bases 2, 10, 16 are allowed");
			}

			// NB: Can only happen to internally managed instances
			if (_mMagnitude == null)
				return "null";

			if (_mSign == 0)
				return "0";

			Debug.Assert(_mMagnitude.Length > 0);

			var sb = new StringBuilder();

			if (radix == 16)
			{
				sb.Append(_mMagnitude[0].ToString("x"));

				for (var i = 1; i < _mMagnitude.Length; i++)
				{
					sb.Append(_mMagnitude[i].ToString("x8"));
				}
			}
			else if (radix == 2)
			{
				sb.Append('1');

				for (var i = BitLength - 2; i >= 0; --i)
				{
					sb.Append(TestBit(i) ? '1' : '0');
				}
			}
			else
			{
				// This is algorithm 1a from chapter 4.4 in Seminumerical Algorithms, slow but it works
				var S = new Stack();
				var bs = ValueOf(radix);

				var u = Abs();

			    while (u._mSign != 0)
				{
					var b = u.Mod(bs);
					if (b._mSign == 0)
					{
						S.Push("0");
					}
					else
					{
						// see how to interact with different bases
						S.Push(b._mMagnitude[0].ToString("d"));
					}
					u = u.Divide(bs);
				}

				// Then pop the stack
				while (S.Count != 0)
				{
					sb.Append((string)S.Pop());
				}
			}

			var s = sb.ToString();

			Debug.Assert(s.Length > 0);

			// Strip leading zeros. (We know this number is not all zeroes though)
			if (s[0] == '0')
			{
				var nonZeroPos = 0;
				while (s[++nonZeroPos] == '0') { }

				s = s.Substring(nonZeroPos);
			}

			if (_mSign == -1)
			{
				s = "-" + s;
			}

			return s;
		}

		private static NetBigInteger CreateUValueOf(
			ulong value)
		{
			var msw = (int)(value >> 32);
			var lsw = (int)value;

			if (msw != 0)
				return new NetBigInteger(1, new[] { msw, lsw }, false);

			if (lsw != 0)
			{
				var n = new NetBigInteger(1, new[] { lsw }, false);
				// Check for a power of two
				if ((lsw & -lsw) == lsw)
				{
					n._mNumBits = 1;
				}
				return n;
			}

			return Zero;
		}

		private static NetBigInteger CreateValueOf(
			long value)
		{
			if (value < 0)
			{
				if (value == long.MinValue)
					return CreateValueOf(~value).Not();

				return CreateValueOf(-value).Negate();
			}

			return CreateUValueOf((ulong)value);
		}

		public static NetBigInteger ValueOf(
			long value)
		{
			switch (value)
			{
				case 0:
					return Zero;
				case 1:
					return One;
				case 2:
					return Two;
				case 3:
					return Three;
				case 10:
					return Ten;
			}

			return CreateValueOf(value);
		}

		public int GetLowestSetBit()
		{
			if (_mSign == 0)
				return -1;

			var w = _mMagnitude.Length;

			while (--w > 0)
			{
				if (_mMagnitude[w] != 0)
					break;
			}

			var word = _mMagnitude[w];
			Debug.Assert(word != 0);

			var b = (word & 0x0000FFFF) == 0
				? (word & 0x00FF0000) == 0
					? 7
					: 15
				: (word & 0x000000FF) == 0
					? 23
					: 31;

			while (b > 0)
			{
				if ((word << b) == int.MinValue)
					break;

				b--;
			}

			return ((_mMagnitude.Length - w) * 32 - (b + 1));
		}

		public bool TestBit(
			int n)
		{
			if (n < 0)
				throw new ArithmeticException("Bit position must not be negative");

			if (_mSign < 0)
				return !Not().TestBit(n);

			var wordNum = n / 32;
			if (wordNum >= _mMagnitude.Length)
				return false;

			var word = _mMagnitude[_mMagnitude.Length - 1 - wordNum];
			return ((word >> (n % 32)) & 1) > 0;
		}
	}
}