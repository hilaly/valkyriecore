﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	/// <summary>
	/// Represents a local peer capable of holding zero, one or more connections to remote peers
	/// </summary>
	public partial class NetPeer
	{
		private static int _sInitializedPeersCount;

		private int _mListenPort;
	    private readonly object _mMessageReceivedEventCreationLock = new object();

		internal readonly List<NetConnection> MConnections;
		private readonly Dictionary<IPEndPoint, NetConnection> _mConnectionLookup;

		private string _mShutdownReason;

		/// <summary>
		/// Gets the NetPeerStatus of the NetPeer
		/// </summary>
		public NetPeerStatus Status { get { return _mStatus; } }

		/// <summary>
		/// Signalling event which can be waited on to determine when a message is queued for reading.
		/// Note that there is no guarantee that after the event is signaled the blocked thread will 
		/// find the message in the queue. Other user created threads could be preempted and dequeue 
		/// the message before the waiting thread wakes up.
		/// </summary>
		public AutoResetEvent MessageReceivedEvent
		{
			get
			{
				if (_mMessageReceivedEvent == null)
				{
					lock (_mMessageReceivedEventCreationLock) // make sure we don't create more than one event object
					{
						if (_mMessageReceivedEvent == null)
							_mMessageReceivedEvent = new AutoResetEvent(false);
					}
				}
				return _mMessageReceivedEvent;
			}
		}

		/// <summary>
		/// Gets a unique identifier for this NetPeer based on Mac address and ip/port. Note! Not available until Start() has been called!
		/// </summary>
		public long UniqueIdentifier { get { return MUniqueIdentifier; } }

		/// <summary>
		/// Gets the port number this NetPeer is listening and sending on, if Start() has been called
		/// </summary>
		public int Port { get { return _mListenPort; } }

		/// <summary>
		/// Returns an UPnP object if enabled in the NetPeerConfiguration
		/// </summary>
		public NetUpnP UpnP { get { return _mUpnp; } }

		/// <summary>
		/// Gets or sets the application defined object containing data about the peer
		/// </summary>
		public object Tag { get; set; }

	    /// <summary>
		/// Gets a copy of the list of connections
		/// </summary>
		public List<NetConnection> Connections
		{
			get
			{
				lock (MConnections)
					return new List<NetConnection>(MConnections);
			}
		}

		/// <summary>
		/// Gets the number of active connections
		/// </summary>
		public int ConnectionsCount
		{
			get { return MConnections.Count; }
		}

		/// <summary>
		/// Statistics on this NetPeer since it was initialized
		/// </summary>
		public NetPeerStatistics Statistics
		{
			get { return MStatistics; }
		}

		/// <summary>
		/// Gets the configuration used to instanciate this NetPeer
		/// </summary>
		public NetPeerConfiguration Configuration { get { return MConfiguration; } }

		/// <summary>
		/// NetPeer constructor
		/// </summary>
		public NetPeer(NetPeerConfiguration config)
		{
			MConfiguration = config;
			MStatistics = new NetPeerStatistics(this);
			_mReleasedIncomingMessages = new NetQueue<NetIncomingMessage>(4);
			MUnsentUnconnectedMessages = new NetQueue<NetTuple<IPEndPoint, NetOutgoingMessage>>(2);
			MConnections = new List<NetConnection>();
			_mConnectionLookup = new Dictionary<IPEndPoint, NetConnection>();
			MHandshakes = new Dictionary<IPEndPoint, NetConnection>();
			_mSenderRemote = new IPEndPoint(IPAddress.Any, 0);
			_mStatus = NetPeerStatus.NotRunning;
			_mReceivedFragmentGroups = new Dictionary<NetConnection, Dictionary<int, ReceivedFragmentGroup>>();	
		}

		/// <summary>
		/// Binds to socket and spawns the networking thread
		/// </summary>
		public void Start()
		{
			if (_mStatus != NetPeerStatus.NotRunning)
			{
				// already running! Just ignore...
				LogWarning("Start() called on already running NetPeer - ignoring.");
				return;
			}

			_mStatus = NetPeerStatus.Starting;

			// fix network thread name
			if (MConfiguration.NetworkThreadName == "Lidgren network thread")
			{
				var pc = Interlocked.Increment(ref _sInitializedPeersCount);
				MConfiguration.NetworkThreadName = "Lidgren network thread " + pc;
			}

			InitializeNetwork();
			
			// start network thread
			_mNetworkThread = new Thread(NetworkLoop);
			_mNetworkThread.Name = MConfiguration.NetworkThreadName;
			_mNetworkThread.IsBackground = true;
			_mNetworkThread.Start();

			// send upnp discovery
			if (_mUpnp != null)
				_mUpnp.Discover(this);

			// allow some time for network thread to start up in case they call Connect() or UPnP calls immediately
			Thread.Sleep(50);
		}

		/// <summary>
		/// Get the connection, if any, for a certain remote endpoint
		/// </summary>
		public NetConnection GetConnection(IPEndPoint ep)
		{
			NetConnection retval;

			// this should not pose a threading problem, m_connectionLookup is never added to concurrently
			// and TryGetValue will not throw an exception on fail, only yield null, which is acceptable
			_mConnectionLookup.TryGetValue(ep, out retval);

			return retval;
		}

		/// <summary>
		/// Read a pending message from any connection, blocking up to maxMillis if needed
		/// </summary>
		public NetIncomingMessage WaitMessage(int maxMillis)
		{
			var msg = ReadMessage();
			if (msg != null)
				return msg; // no need to wait; we already have a message to deliver
			var msgEvt = MessageReceivedEvent;
			msgEvt.WaitOne(maxMillis);
			return ReadMessage();
		}

		/// <summary>
		/// Read a pending message from any connection, if any
		/// </summary>
		public NetIncomingMessage ReadMessage()
		{
			NetIncomingMessage retval;
			if (_mReleasedIncomingMessages.TryDequeue(out retval))
			{
				if (retval.MessageType == NetIncomingMessageType.StatusChanged)
				{
					var status = (NetConnectionStatus)retval.PeekByte();
					retval.SenderConnection.MVisibleStatus = status;
				}
			}
			return retval;
		}

		/// <summary>
		/// Read a pending message from any connection, if any
		/// </summary>
		public int ReadMessages(IList<NetIncomingMessage> addTo)
		{
			var added = _mReleasedIncomingMessages.TryDrain(addTo);
			if (added > 0)
			{
				for (var i = 0; i < added; i++)
				{
					var index = addTo.Count - added + i;
					var nim = addTo[index];
					if (nim.MessageType == NetIncomingMessageType.StatusChanged)
					{
						var status = (NetConnectionStatus)nim.PeekByte();
						nim.SenderConnection.MVisibleStatus = status;
					}
				}
			}
			return added;
		}

		// send message immediately and recycle it
		internal void SendLibrary(NetOutgoingMessage msg, IPEndPoint recipient)
		{
			VerifyNetworkThread();
			NetException.Assert(msg.MIsSent == false);

			bool connReset;
			var len = msg.Encode(MSendBuffer, 0, 0);
			SendPacket(len, recipient, 1, out connReset);

			// no reliability, no multiple recipients - we can just recycle this message immediately
			msg.MRecyclingCount = 0;
			Recycle(msg);
		}

		/// <summary>
		/// Create a connection to a remote endpoint
		/// </summary>
		public NetConnection Connect(string host, int port)
		{
			return Connect(new IPEndPoint(NetUtility.Resolve(host), port), null);
		}

		/// <summary>
		/// Create a connection to a remote endpoint
		/// </summary>
		public NetConnection Connect(string host, int port, NetOutgoingMessage hailMessage)
		{
			return Connect(new IPEndPoint(NetUtility.Resolve(host), port), hailMessage);
		}

		/// <summary>
		/// Create a connection to a remote endpoint
		/// </summary>
		public NetConnection Connect(IPEndPoint remoteEndPoint)
		{
			return Connect(remoteEndPoint, null);
		}

		/// <summary>
		/// Create a connection to a remote endpoint
		/// </summary>
		public virtual NetConnection Connect(IPEndPoint remoteEndPoint, NetOutgoingMessage hailMessage)
		{
			if (remoteEndPoint == null)
				throw new ArgumentNullException("remoteEndPoint");

			lock (MConnections)
			{
				if (_mStatus == NetPeerStatus.NotRunning)
					throw new NetException("Must call Start() first");

				if (_mConnectionLookup.ContainsKey(remoteEndPoint))
					throw new NetException("Already connected to that endpoint!");

				NetConnection hs;
				if (MHandshakes.TryGetValue(remoteEndPoint, out hs))
				{
					// already trying to connect to that endpoint; make another try
					switch (hs.MStatus)
					{
						case NetConnectionStatus.InitiatedConnect:
							// send another connect
							hs.MConnectRequested = true;
							break;
						case NetConnectionStatus.RespondedConnect:
							// send another response
							hs.SendConnectResponse(NetTime.Now, false);
							break;
						default:
							// weird
							LogWarning("Weird situation; Connect() already in progress to remote endpoint; but hs status is " + hs.MStatus);
							break;
					}
					return hs;
				}

			    var conn = new NetConnection(this, remoteEndPoint)
			    {
			        MStatus = NetConnectionStatus.InitiatedConnect,
			        MLocalHailMessage = hailMessage,
			        MConnectRequested = true,
			        MConnectionInitiator = true
			    };

			    // handle on network thread

			    MHandshakes.Add(remoteEndPoint, conn);

				return conn;
			}
		}

		/// <summary>
		/// Send raw bytes; only used for debugging
		/// </summary>
#if DEBUG
		public void RawSend(byte[] arr, int offset, int length, IPEndPoint destination)
#else
		public void RawSend(byte[] arr, int offset, int length, IPEndPoint destination)
#endif
	{
			// wrong thread - this miiiight crash with network thread... but what's a boy to do.
			Array.Copy(arr, offset, MSendBuffer, 0, length);
			bool unused;
			SendPacket(length, destination, 1, out unused);
		}

		/// <summary>
		/// In DEBUG, throws an exception, in RELEASE logs an error message
		/// </summary>
		/// <param name="message"></param>
		internal void ThrowOrLog(string message)
		{
#if DEBUG
			throw new NetException(message);
#else
			LogError(message);
#endif
		}

		/// <summary>
		/// Disconnects all active connections and closes the socket
		/// </summary>
		public void Shutdown(string bye)
		{
			// called on user thread
			if (_mSocket == null)
				return; // already shut down

			LogDebug("Shutdown requested");
			_mShutdownReason = bye;
			_mStatus = NetPeerStatus.ShutdownRequested;
		}
	}
}
