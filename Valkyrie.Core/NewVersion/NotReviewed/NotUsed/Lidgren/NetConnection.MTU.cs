﻿// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetConnection  
	{
		private enum ExpandMtuStatus
		{
			None,
			InProgress,
			Finished
		}

		private const int CProtocolMaxMtu = (int)(((ushort.MaxValue / 8.0f) - 1.0f));

		private ExpandMtuStatus _mExpandMtuStatus;

		private int _mLargestSuccessfulMtu;
		private int _mSmallestFailedMtu;

		private int _mLastSentMtuAttemptSize;
		private double _mLastSentMtuAttemptTime;
		private int _mMtuAttemptFails;

		internal int MCurrentMtu;

		/// <summary>
		/// Gets the current MTU in bytes. If PeerConfiguration.AutoExpandMTU is false, this will be PeerConfiguration.MaximumTransmissionUnit.
		/// </summary>
		public int CurrentMtu { get { return MCurrentMtu; } }

		internal void InitExpandMtu(double now)
		{
			_mLastSentMtuAttemptTime = now + MPeerConfiguration.MExpandMtuFrequency + 1.5f + _mAverageRoundtripTime; // wait a tiny bit before starting to expand mtu
			_mLargestSuccessfulMtu = 512;
			_mSmallestFailedMtu = -1;
			MCurrentMtu = MPeerConfiguration.MaximumTransmissionUnit;
		}

		private void MtuExpansionHeartbeat(double now)
		{
			if (_mExpandMtuStatus == ExpandMtuStatus.Finished)
				return;

			if (_mExpandMtuStatus == ExpandMtuStatus.None)
			{
				if (MPeerConfiguration.MAutoExpandMtu == false)
				{
					FinalizeMtu(MCurrentMtu);
					return;
				}

				// begin expansion
				ExpandMtu(now);
				return;
			}

			if (now > _mLastSentMtuAttemptTime + MPeerConfiguration.ExpandMtuFrequency)
			{
				_mMtuAttemptFails++;
				if (_mMtuAttemptFails == 3)
				{
					FinalizeMtu(MCurrentMtu);
					return;
				}

				// timed out; ie. failed
				_mSmallestFailedMtu = _mLastSentMtuAttemptSize;
				ExpandMtu(now);
			}
		}

		private void ExpandMtu(double now)
		{
			int tryMtu;

			// we've nevered encountered failure
			if (_mSmallestFailedMtu == -1)
			{
				// we've never encountered failure; expand by 25% each time
				tryMtu = (int)(MCurrentMtu * 1.25f);
				//m_peer.LogDebug("Trying MTU " + tryMTU);
			}
			else
			{
				// we HAVE encountered failure; so try in between
				tryMtu = (int)((_mSmallestFailedMtu + (float)_mLargestSuccessfulMtu) / 2.0f);
				//m_peer.LogDebug("Trying MTU " + m_smallestFailedMTU + " <-> " + m_largestSuccessfulMTU + " = " + tryMTU);
			}

			if (tryMtu > CProtocolMaxMtu)
				tryMtu = CProtocolMaxMtu;

			if (tryMtu == _mLargestSuccessfulMtu)
			{
				//m_peer.LogDebug("Found optimal MTU - exiting");
				FinalizeMtu(_mLargestSuccessfulMtu);
				return;
			}

			SendExpandMtu(now, tryMtu);
		}

		private void SendExpandMtu(double now, int size)
		{
			var om = MPeer.CreateMessage(size);
			var tmp = new byte[size];
			om.Write(tmp);
			om.MMessageType = NetMessageType.ExpandMtuRequest;
			var len = om.Encode(MPeer.MSendBuffer, 0, 0);

            var ok = MPeer.SendMtuPacket(len, MRemoteEndPoint);
			if (ok == false)
			{
				//m_peer.LogDebug("Send MTU failed for size " + size);

				// failure
				if (_mSmallestFailedMtu == -1 || size < _mSmallestFailedMtu)
				{
					_mSmallestFailedMtu = size;
					_mMtuAttemptFails++;
					if (_mMtuAttemptFails >= MPeerConfiguration.ExpandMtuFailAttempts)
					{
						FinalizeMtu(_mLargestSuccessfulMtu);
						return;
					}
				}
				ExpandMtu(now);
				return;
			}

			_mLastSentMtuAttemptSize = size;
			_mLastSentMtuAttemptTime = now;

			MStatistics.PacketSent(len, 1);
			MPeer.Recycle(om);
		}

		private void FinalizeMtu(int size)
		{
			if (_mExpandMtuStatus == ExpandMtuStatus.Finished)
				return;
			_mExpandMtuStatus = ExpandMtuStatus.Finished;
			MCurrentMtu = size;
			if (MCurrentMtu != MPeerConfiguration.MMaximumTransmissionUnit)
				MPeer.LogDebug("Expanded Maximum Transmission Unit to: " + MCurrentMtu + " bytes");
		}

		private void SendMtuSuccess(int size)
		{
			var om = MPeer.CreateMessage(4);
			om.Write(size);
			om.MMessageType = NetMessageType.ExpandMtuSuccess;
			var len = om.Encode(MPeer.MSendBuffer, 0, 0);
			bool connectionReset;
			MPeer.SendPacket(len, MRemoteEndPoint, 1, out connectionReset);
			MPeer.Recycle(om);

			//m_peer.LogDebug("Received MTU expand request for " + size + " bytes");

			MStatistics.PacketSent(len, 1);
		}

		private void HandleExpandMtuSuccess(double now, int size)
		{
			if (size > _mLargestSuccessfulMtu)
				_mLargestSuccessfulMtu = size;

			if (size < MCurrentMtu)
			{
				//m_peer.LogDebug("Received low MTU expand success (size " + size + "); current mtu is " + m_currentMTU);
				return;
			}

			//m_peer.LogDebug("Expanding MTU to " + size);
			MCurrentMtu = size;

			ExpandMtu(now);
		}
	}
}
