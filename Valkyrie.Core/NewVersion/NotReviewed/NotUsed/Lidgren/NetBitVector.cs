﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;
using System.Runtime.CompilerServices;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	/// <summary>
	/// Fixed size vector of booleans
	/// </summary>
	public sealed class NetBitVector
	{
		private readonly int _mCapacity;
		private readonly int[] _mData;
		private int _mNumBitsSet;

		/// <summary>
		/// Gets the number of bits/booleans stored in this vector
		/// </summary>
		public int Capacity { get { return _mCapacity; } }

		/// <summary>
		/// NetBitVector constructor
		/// </summary>
		public NetBitVector(int bitsCapacity)
		{
			_mCapacity = bitsCapacity;
			_mData = new int[(bitsCapacity + 31) / 32];
		}

		/// <summary>
		/// Returns true if all bits/booleans are set to zero/false
		/// </summary>
		public bool IsEmpty()
		{
			return (_mNumBitsSet == 0);
		}

		/// <summary>
		/// Returns the number of bits/booleans set to one/true
		/// </summary>
		/// <returns></returns>
		public int Count()
		{
			return _mNumBitsSet;
		}

		/// <summary>
		/// Shift all bits one step down, cycling the first bit to the top
		/// </summary>
		public void RotateDown()
		{
			var lenMinusOne = _mData.Length - 1;

			var firstBit = _mData[0] & 1;
			for (var i = 0; i < lenMinusOne; i++)
				_mData[i] = ((_mData[i] >> 1) & ~(1 << 31)) | _mData[i + 1] << 31;

			var lastIndex = _mCapacity - 1 - (32 * lenMinusOne);

			// special handling of last int
			var cur = _mData[lenMinusOne];
			cur = cur >> 1;
			cur |= firstBit << lastIndex;

			_mData[lenMinusOne] = cur;
		}

		/// <summary>
		/// Gets the first (lowest) index set to true
		/// </summary>
		public int GetFirstSetIndex()
		{
			var idx = 0;

			var data = _mData[0];
			while (data == 0)
			{
				idx++;
				data = _mData[idx];
			}

			var a = 0;
			while (((data >> a) & 1) == 0)
				a++;

			return (idx * 32) + a;
		}

		/// <summary>
		/// Gets the bit/bool at the specified index
		/// </summary>
		public bool Get(int bitIndex)
		{
			NetException.Assert(bitIndex >= 0 && bitIndex < _mCapacity);

			return (_mData[bitIndex / 32] & (1 << (bitIndex % 32))) != 0;
		}

		/// <summary>
		/// Sets or clears the bit/bool at the specified index
		/// </summary>
		public void Set(int bitIndex, bool value)
		{
			NetException.Assert(bitIndex >= 0 && bitIndex < _mCapacity);

			var idx = bitIndex / 32;
			if (value)
			{
				if ((_mData[idx] & (1 << (bitIndex % 32))) == 0)
					_mNumBitsSet++;
				_mData[idx] |= (1 << (bitIndex % 32));
			}
			else
			{
				if ((_mData[idx] & (1 << (bitIndex % 32))) != 0)
					_mNumBitsSet--;
				_mData[idx] &= (~(1 << (bitIndex % 32)));
			}
		}

		/// <summary>
		/// Gets the bit/bool at the specified index
		/// </summary>
		[IndexerName("Bit")]
		public bool this[int index]
		{
			get { return Get(index); }
			set { Set(index, value); }
		}

		/// <summary>
		/// Sets all bits/booleans to zero/false
		/// </summary>
		public void Clear()
		{
			Array.Clear(_mData, 0, _mData.Length);
			_mNumBitsSet = 0;
			NetException.Assert(IsEmpty());
		}

		/// <summary>
		/// Returns a string that represents this object
		/// </summary>
		public override string ToString()
		{
			var bdr = new StringBuilder(_mCapacity + 2);
			bdr.Append('[');
			for (var i = 0; i < _mCapacity; i++)
				bdr.Append(Get(_mCapacity - i - 1) ? '1' : '0');
			bdr.Append(']');
			return bdr.ToString();
		}
	}
}
