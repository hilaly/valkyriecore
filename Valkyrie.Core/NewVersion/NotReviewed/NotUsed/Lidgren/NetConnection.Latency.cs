﻿// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetConnection
	{
		private double _mSentPingTime;
		private int _mSentPingNumber;
		private double _mAverageRoundtripTime;
		private double _mTimeoutDeadline = double.MaxValue;

		// local time value + m_remoteTimeOffset = remote time value
		internal double MRemoteTimeOffset;

		/// <summary>
		/// Gets the current average roundtrip time in seconds
		/// </summary>
		public float AverageRoundtripTime { get { return (float)_mAverageRoundtripTime; } }

		/// <summary>
		/// Time offset between this peer and the remote peer
		/// </summary>
		public float RemoteTimeOffset { get { return (float)MRemoteTimeOffset; } }

		// this might happen more than once
		internal void InitializeRemoteTimeOffset(float remoteSendTime)
		{
			MRemoteTimeOffset = (remoteSendTime + (_mAverageRoundtripTime / 2.0)) - NetTime.Now;
		}
		
		/// <summary>
		/// Gets local time value comparable to NetTime.Now from a remote value
		/// </summary>
		public double GetLocalTime(double remoteTimestamp)
		{
			return remoteTimestamp - MRemoteTimeOffset;
		}

		/// <summary>
		/// Gets the remote time value for a local time value produced by NetTime.Now
		/// </summary>
		public double GetRemoteTime(double localTimestamp)
		{
			return localTimestamp + MRemoteTimeOffset;
		}

		internal void InitializePing()
		{
			var now = NetTime.Now;

			// randomize ping sent time (0.25 - 1.0 x ping interval)
			_mSentPingTime = now;
			_mSentPingTime -= (MPeerConfiguration.PingInterval * 0.25f); // delay ping for a little while
			_mSentPingTime -= (MwcRandom.Instance.NextSingle() * (MPeerConfiguration.PingInterval * 0.75f));
			_mTimeoutDeadline = now + (MPeerConfiguration.MConnectionTimeout * 2.0); // initially allow a little more time

			// make it better, quick :-)
			SendPing();
		}

		internal void SendPing()
		{
			MPeer.VerifyNetworkThread();

			_mSentPingNumber++;

			_mSentPingTime = NetTime.Now;
			var om = MPeer.CreateMessage(1);
			om.Write((byte)_mSentPingNumber); // truncating to 0-255
			om.MMessageType = NetMessageType.Ping;

			var len = om.Encode(MPeer.MSendBuffer, 0, 0);
			bool connectionReset;
			MPeer.SendPacket(len, MRemoteEndPoint, 1, out connectionReset);

			MStatistics.PacketSent(len, 1);
			MPeer.Recycle(om);
		}

		internal void SendPong(int pingNumber)
		{
			MPeer.VerifyNetworkThread();

			var om = MPeer.CreateMessage(5);
			om.Write((byte)pingNumber);
			om.Write((float)NetTime.Now); // we should update this value to reflect the exact point in time the packet is SENT
			om.MMessageType = NetMessageType.Pong;

			var len = om.Encode(MPeer.MSendBuffer, 0, 0);
			bool connectionReset;

			MPeer.SendPacket(len, MRemoteEndPoint, 1, out connectionReset);

			MStatistics.PacketSent(len, 1);
			MPeer.Recycle(om);
		}

		internal void ReceivedPong(double now, int pongNumber, float remoteSendTime)
		{
			if ((byte)pongNumber != (byte)_mSentPingNumber)
			{
				MPeer.LogVerbose("Ping/Pong mismatch; dropped message?");
				return;
			}

			_mTimeoutDeadline = now + MPeerConfiguration.MConnectionTimeout;

			var rtt = now - _mSentPingTime;
			NetException.Assert(rtt >= 0);

			var diff = (remoteSendTime + (rtt / 2.0)) - now;

			if (_mAverageRoundtripTime < 0)
			{
				MRemoteTimeOffset = diff;
				_mAverageRoundtripTime = rtt;
				MPeer.LogDebug("Initiated average roundtrip time to " + NetTime.ToReadable(_mAverageRoundtripTime) + " Remote time is: " + (now + diff));
			}
			else
			{
				_mAverageRoundtripTime = (_mAverageRoundtripTime * 0.7) + (rtt * 0.3);

				MRemoteTimeOffset = ((MRemoteTimeOffset * (_mSentPingNumber - 1)) + diff) / _mSentPingNumber;
				MPeer.LogVerbose("Updated average roundtrip time to " + NetTime.ToReadable(_mAverageRoundtripTime) + ", remote time to " + (now + MRemoteTimeOffset) + " (ie. diff " + MRemoteTimeOffset + ")");
			}

			// update resend delay for all channels
			var resendDelay = GetResendDelay();
			foreach (var chan in MSendChannels)
			{
				var rchan = chan as NetReliableSenderChannel;
				if (rchan != null)
					rchan.MResendDelay = resendDelay;
			}

			// m_peer.LogVerbose("Timeout deadline pushed to  " + m_timeoutDeadline);

			// notify the application that average rtt changed
			if (MPeer.MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.ConnectionLatencyUpdated))
			{
				var update = MPeer.CreateIncomingMessage(NetIncomingMessageType.ConnectionLatencyUpdated, 4);
				update.MSenderConnection = this;
				update.MSenderEndPoint = MRemoteEndPoint;
				update.Write((float)rtt);
				MPeer.ReleaseMessage(update);
			}
		}
	}
}
