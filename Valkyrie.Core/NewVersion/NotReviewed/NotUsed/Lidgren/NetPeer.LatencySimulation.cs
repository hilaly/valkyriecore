﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

*/
//#define USE_RELEASE_STATISTICS

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetPeer
	{

#if DEBUG
		private readonly List<DelayedPacket> _mDelayedPackets = new List<DelayedPacket>();

		private class DelayedPacket
		{
			public byte[] Data;
			public double DelayedUntil;
			public IPEndPoint Target;
		}

		internal void SendPacket(int numBytes, IPEndPoint target, int numMessages, out bool connectionReset)
		{
			connectionReset = false;

			// simulate loss
			var loss = MConfiguration.MLoss;
			if (loss > 0.0f)
			{
				if ((float)MwcRandom.Instance.NextDouble() < loss)
				{
					LogVerbose("Sending packet " + numBytes + " bytes - SIMULATED LOST!");
					return; // packet "lost"
				}
			}

			MStatistics.PacketSent(numBytes, numMessages);

			// simulate latency
			var m = MConfiguration.MMinimumOneWayLatency;
			var r = MConfiguration.MRandomOneWayLatency;
			if (m == 0.0f && r == 0.0f)
			{
				// no latency simulation
				// LogVerbose("Sending packet " + numBytes + " bytes");
				var wasSent = ActuallySendPacket(MSendBuffer, numBytes, target, out connectionReset);
				// TODO: handle wasSent == false?

				if (MConfiguration.MDuplicates > 0.0f && MwcRandom.Instance.NextDouble() < MConfiguration.MDuplicates)
					ActuallySendPacket(MSendBuffer, numBytes, target, out connectionReset); // send it again!

				return;
			}

			var num = 1;
			if (MConfiguration.MDuplicates > 0.0f && MwcRandom.Instance.NextSingle() < MConfiguration.MDuplicates)
				num++;

		    for (var i = 0; i < num; i++)
			{
				var delay = MConfiguration.MMinimumOneWayLatency + (MwcRandom.Instance.NextSingle() * MConfiguration.MRandomOneWayLatency);

				// Enqueue delayed packet
			    var p = new DelayedPacket
			    {
			        Target = target,
			        Data = new byte[numBytes]
			    };
			    Buffer.BlockCopy(MSendBuffer, 0, p.Data, 0, numBytes);
				p.DelayedUntil = NetTime.Now + delay;

				_mDelayedPackets.Add(p);
			}

			// LogVerbose("Sending packet " + numBytes + " bytes - delayed " + NetTime.ToReadable(delay));
		}

		private void SendDelayedPackets()
		{
			if (_mDelayedPackets.Count <= 0)
				return;

			var now = NetTime.Now;

		    RestartDelaySending:
			foreach (var p in _mDelayedPackets)
			{
				if (now > p.DelayedUntil)
				{
				    bool connectionReset;
				    ActuallySendPacket(p.Data, p.Data.Length, p.Target, out connectionReset);
					_mDelayedPackets.Remove(p);
					goto RestartDelaySending;
				}
			}
		}

		private void FlushDelayedPackets()
		{
			try
			{
				bool connectionReset;
				foreach (var p in _mDelayedPackets)
					ActuallySendPacket(p.Data, p.Data.Length, p.Target, out connectionReset);
				_mDelayedPackets.Clear();
			}
			catch { }
		}

		internal bool ActuallySendPacket(byte[] data, int numBytes, IPEndPoint target, out bool connectionReset)
		{
			connectionReset = false;
			try
			{
				// TODO: refactor this check outta here
				if (target.Address == IPAddress.Broadcast)
				{
					// Some networks do not allow 
					// a global broadcast so we use the BroadcastAddress from the configuration
					// this can be resolved to a local broadcast addresss e.g 192.168.x.255                    
					target.Address = MConfiguration.BroadcastAddress;
					_mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
				}

				var bytesSent = _mSocket.SendTo(data, 0, numBytes, SocketFlags.None, target);
				if (numBytes != bytesSent)
					LogWarning("Failed to send the full " + numBytes + "; only " + bytesSent + " bytes sent in packet!");

				// LogDebug("Sent " + numBytes + " bytes");
			}
			catch (SocketException sx)
			{
				if (sx.SocketErrorCode == SocketError.WouldBlock)
				{
					// send buffer full?
					LogWarning("Socket threw exception; would block - send buffer full? Increase in NetPeerConfiguration");
					return false;
				}
				if (sx.SocketErrorCode == SocketError.ConnectionReset)
				{
					// connection reset by peer, aka connection forcibly closed aka "ICMP port unreachable" 
					connectionReset = true;
					return false;
				}
				LogError("Failed to send packet: " + sx);
			}
			catch (Exception ex)
			{
				LogError("Failed to send packet: " + ex);
			}
			finally
			{
				if (target.Address == IPAddress.Broadcast)
					_mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, false);
			}
			return true;
		}

		internal bool SendMtuPacket(int numBytes, IPEndPoint target)
		{
			try
			{
				_mSocket.DontFragment = true;
				var bytesSent = _mSocket.SendTo(MSendBuffer, 0, numBytes, SocketFlags.None, target);
				if (numBytes != bytesSent)
					LogWarning("Failed to send the full " + numBytes + "; only " + bytesSent + " bytes sent in packet!");

				MStatistics.PacketSent(numBytes, 1);
			}
			catch (SocketException sx)
			{
				if (sx.SocketErrorCode == SocketError.MessageSize)
					return false;
				if (sx.SocketErrorCode == SocketError.WouldBlock)
				{
					// send buffer full?
					LogWarning("Socket threw exception; would block - send buffer full? Increase in NetPeerConfiguration");
					return true;
				}
				if (sx.SocketErrorCode == SocketError.ConnectionReset)
					return true;
				LogError("Failed to send packet: (" + sx.SocketErrorCode + ") " + sx);
			}
			catch (Exception ex)
			{
				LogError("Failed to send packet: " + ex);
			}
			finally
			{
				_mSocket.DontFragment = false;
			}
			return true;
		}
#else
		internal bool SendMtuPacket(int numBytes, IPEndPoint target)
		{
			try
			{
				_mSocket.DontFragment = true;
				int bytesSent = _mSocket.SendTo(MSendBuffer, 0, numBytes, SocketFlags.None, target);
				if (numBytes != bytesSent)
					LogWarning("Failed to send the full " + numBytes + "; only " + bytesSent + " bytes sent in packet!");
			}
			catch (SocketException sx)
			{
				if (sx.SocketErrorCode == SocketError.MessageSize)
					return false;
				if (sx.SocketErrorCode == SocketError.WouldBlock)
				{
					// send buffer full?
					LogWarning("Socket threw exception; would block - send buffer full? Increase in NetPeerConfiguration");
					return true;
				}
				if (sx.SocketErrorCode == SocketError.ConnectionReset)
					return true;
				LogError("Failed to send packet: (" + sx.SocketErrorCode + ") " + sx);
			}
			catch (Exception ex)
			{
				LogError("Failed to send packet: " + ex);
			}
			finally
			{
				_mSocket.DontFragment = false;
			}
			return true;
		}

		//
		// Release - just send the packet straight away
		//
		internal void SendPacket(int numBytes, IPEndPoint target, int numMessages, out bool connectionReset)
		{
#if USE_RELEASE_STATISTICS
			m_statistics.PacketSent(numBytes, numMessages);
#endif
			connectionReset = false;
			try
			{
				// TODO: refactor this check outta here
				if (target.Address == IPAddress.Broadcast)
					_mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);

				int bytesSent = _mSocket.SendTo(MSendBuffer, 0, numBytes, SocketFlags.None, target);
				if (numBytes != bytesSent)
					LogWarning("Failed to send the full " + numBytes + "; only " + bytesSent + " bytes sent in packet!");
			}
			catch (SocketException sx)
			{
				if (sx.SocketErrorCode == SocketError.WouldBlock)
				{
					// send buffer full?
					LogWarning("Socket threw exception; would block - send buffer full? Increase in NetPeerConfiguration");
					return;
				}
				if (sx.SocketErrorCode == SocketError.ConnectionReset)
				{
					// connection reset by peer, aka connection forcibly closed aka "ICMP port unreachable" 
					connectionReset = true;
					return;
				}
				LogError("Failed to send packet: " + sx);
			}
			catch (Exception ex)
			{
				LogError("Failed to send packet: " + ex);
			}
			finally
			{
				if (target.Address == IPAddress.Broadcast)
					_mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, false);
			}
			return;
		}

		private void FlushDelayedPackets()
		{
		}
#endif
    }
}