﻿// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	internal sealed class NetReliableOrderedReceiver : NetReceiverChannelBase
	{
		private int _mWindowStart;
		private readonly int _mWindowSize;
		private readonly NetBitVector _mEarlyReceived;
		internal NetIncomingMessage[] MWithheldMessages;

		public NetReliableOrderedReceiver(NetConnection connection, int windowSize)
			: base(connection)
		{
			_mWindowSize = windowSize;
			MWithheldMessages = new NetIncomingMessage[windowSize];
			_mEarlyReceived = new NetBitVector(windowSize);
		}

		private void AdvanceWindow()
		{
			_mEarlyReceived.Set(_mWindowStart % _mWindowSize, false);
			_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;
		}

		internal override void ReceiveMessage(NetIncomingMessage message)
		{
			var relate = NetUtility.RelativeSequenceNumber(message.MSequenceNumber, _mWindowStart);

			// ack no matter what
			MConnection.QueueAck(message.MReceivedMessageType, message.MSequenceNumber);

			if (relate == 0)
			{
				// Log("Received message #" + message.SequenceNumber + " right on time");

				//
				// excellent, right on time
				//
				//m_peer.LogVerbose("Received RIGHT-ON-TIME " + message);

				AdvanceWindow();
				MPeer.ReleaseMessage(message);

				// release withheld messages
				var nextSeqNr = (message.MSequenceNumber + 1) % NetConstants.NumSequenceNumbers;

				while (_mEarlyReceived[nextSeqNr % _mWindowSize])
				{
					message = MWithheldMessages[nextSeqNr % _mWindowSize];
					NetException.Assert(message != null);

					// remove it from withheld messages
					MWithheldMessages[nextSeqNr % _mWindowSize] = null;

					MPeer.LogVerbose("Releasing withheld message #" + message);

					MPeer.ReleaseMessage(message);

					AdvanceWindow();
					nextSeqNr++;
				}

				return;
			}

			if (relate < 0)
			{
				MPeer.LogVerbose("Received message #" + message.MSequenceNumber + " DROPPING DUPLICATE");
				// duplicate
				return;
			}

			// relate > 0 = early message
			if (relate > _mWindowSize)
			{
				// too early message!
				MPeer.LogDebug("Received " + message + " TOO EARLY! Expected " + _mWindowStart);
				return;
			}

			_mEarlyReceived.Set(message.MSequenceNumber % _mWindowSize, true);
			MPeer.LogVerbose("Received " + message + " WITHHOLDING, waiting for " + _mWindowStart);
			MWithheldMessages[message.MSequenceNumber % _mWindowSize] = message;
		}
	}
}
