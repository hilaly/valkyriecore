﻿#if !__ANDROID__ && !IOS && !UNITY_WEBPLAYER && !UNITY_ANDROID && !UNITY_IPHONE
//#define IS_MAC_AVAILABLE
#endif

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetPeer
	{
		private NetPeerStatus _mStatus;
		private Thread _mNetworkThread;
		private Socket _mSocket;
		internal byte[] MSendBuffer;
		internal byte[] MReceiveBuffer;
		internal NetIncomingMessage MReadHelperMessage;
		private EndPoint _mSenderRemote;
		private readonly object _mInitializeLock = new object();
		private uint _mFrameCounter;
		private double _mLastHeartbeat;
		private double _mLastSocketBind = float.MinValue;
		private NetUpnP _mUpnp;
		internal bool MNeedFlushSendQueue;

		internal readonly NetPeerConfiguration MConfiguration;
		private readonly NetQueue<NetIncomingMessage> _mReleasedIncomingMessages;
		internal readonly NetQueue<NetTuple<IPEndPoint, NetOutgoingMessage>> MUnsentUnconnectedMessages;

		internal Dictionary<IPEndPoint, NetConnection> MHandshakes;

		internal readonly NetPeerStatistics MStatistics;
		internal long MUniqueIdentifier;
		internal bool MExecuteFlushSendQueue;

		private AutoResetEvent _mMessageReceivedEvent;
		private List<NetTuple<SynchronizationContext, SendOrPostCallback>> _mReceiveCallbacks;

		/// <summary>
		/// Gets the socket, if Start() has been called
		/// </summary>
		public Socket Socket { get { return _mSocket; } }

		/// <summary>
		/// Call this to register a callback for when a new message arrives
		/// </summary>
		public void RegisterReceivedCallback(SendOrPostCallback callback, SynchronizationContext syncContext = null)
		{
			if (syncContext == null)
				syncContext = SynchronizationContext.Current;
			if (syncContext == null)
				throw new NetException("Need a SynchronizationContext to register callback on correct thread!");
			if (_mReceiveCallbacks == null)
				_mReceiveCallbacks = new List<NetTuple<SynchronizationContext, SendOrPostCallback>>();
			_mReceiveCallbacks.Add(new NetTuple<SynchronizationContext, SendOrPostCallback>(syncContext, callback));
		}

		/// <summary>
		/// Call this to unregister a callback, but remember to do it in the same synchronization context!
		/// </summary>
		public void UnregisterReceivedCallback(SendOrPostCallback callback)
		{
			if (_mReceiveCallbacks == null)
				return;

			// remove all callbacks regardless of sync context
		RestartRemoveCallbacks:
			for (var i = 0; i < _mReceiveCallbacks.Count; i++)
			{
				if (_mReceiveCallbacks[i].Item2.Equals(callback))
				{
					_mReceiveCallbacks.RemoveAt(i);
					goto RestartRemoveCallbacks;
				}
			}
			if (_mReceiveCallbacks.Count < 1)
				_mReceiveCallbacks = null;
		}

		internal void ReleaseMessage(NetIncomingMessage msg)
		{
			NetException.Assert(msg.MIncomingMessageType != NetIncomingMessageType.Error);

			if (msg.MIsFragment)
			{
				HandleReleasedFragment(msg);
				return;
			}

			_mReleasedIncomingMessages.Enqueue(msg);

			if (_mMessageReceivedEvent != null)
				_mMessageReceivedEvent.Set();

			if (_mReceiveCallbacks != null)
			{
				foreach (var tuple in _mReceiveCallbacks)
				{
					try
					{
						tuple.Item1.Post(tuple.Item2, this);
					}
					catch (Exception ex)
					{
						LogWarning("Receive callback exception:" + ex);
					}
				}
			}
		}

		private void BindSocket(bool reBind)
		{
			var now = NetTime.Now;
			if (now - _mLastSocketBind < 1.0)
			{
				LogDebug("Suppressed socket rebind; last bound " + (now - _mLastSocketBind) + " seconds ago");
				return; // only allow rebind once every second
			}
			_mLastSocketBind = now;

			if (_mSocket == null)
				_mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

			if (reBind)
				_mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1); 

			_mSocket.ReceiveBufferSize = MConfiguration.ReceiveBufferSize;
			_mSocket.SendBufferSize = MConfiguration.SendBufferSize;
			_mSocket.Blocking = false;

			var ep = (EndPoint)new IPEndPoint(MConfiguration.LocalAddress, reBind ? _mListenPort : MConfiguration.Port);
			_mSocket.Bind(ep);

			try
			{
				const uint iocIn = 0x80000000;
				const uint iocVendor = 0x18000000;
				var sioUdpConnreset = iocIn | iocVendor | 12;
				_mSocket.IOControl((int)sioUdpConnreset, new[] { Convert.ToByte(false) }, null);
			}
			catch
			{
				// ignore; SIO_UDP_CONNRESET not supported on this platform
			}

			var boundEp = _mSocket.LocalEndPoint as IPEndPoint;
			LogDebug("Socket bound to " + boundEp + ": " + _mSocket.IsBound);
			_mListenPort = boundEp.Port;
		}

		private void InitializeNetwork()
		{
			lock (_mInitializeLock)
			{
				MConfiguration.Lock();

				if (_mStatus == NetPeerStatus.Running)
					return;

				if (MConfiguration.MEnableUpnP)
					_mUpnp = new NetUpnP(this);

				InitializePools();

				_mReleasedIncomingMessages.Clear();
				MUnsentUnconnectedMessages.Clear();
				MHandshakes.Clear();

				// bind to socket
				BindSocket(false);

				MReceiveBuffer = new byte[MConfiguration.ReceiveBufferSize];
				MSendBuffer = new byte[MConfiguration.SendBufferSize];
			    MReadHelperMessage = new NetIncomingMessage(NetIncomingMessageType.Error) {MData = MReceiveBuffer};

			    var macBytes = new byte[8];
				MwcRandom.Instance.NextBytes(macBytes);

#if IS_MAC_AVAILABLE
				try
				{
					var pa = NetUtility.GetMacAddress();
					if (pa != null)
					{
						macBytes = pa.GetAddressBytes();
						LogVerbose("Mac address is " + NetUtility.ToHexString(macBytes));
					}
					else
					{
						LogWarning("Failed to get Mac address");
					}
				}
				catch (NotSupportedException)
				{
					// not supported; lets just keep the random bytes set above
				}
#endif
				var boundEp = _mSocket.LocalEndPoint as IPEndPoint;
				var epBytes = BitConverter.GetBytes(boundEp.GetHashCode());
				var combined = new byte[epBytes.Length + macBytes.Length];
				Array.Copy(epBytes, 0, combined, 0, epBytes.Length);
				Array.Copy(macBytes, 0, combined, epBytes.Length, macBytes.Length);
				MUniqueIdentifier = BitConverter.ToInt64(NetUtility.CreateSHA1Hash(combined), 0);

				_mStatus = NetPeerStatus.Running;
			}
		}

		private void NetworkLoop()
		{
			VerifyNetworkThread();

			LogDebug("Network thread started");

			//
			// Network loop
			//
			do
			{
				try
				{
					Heartbeat();
				}
				catch (Exception ex)
				{
					LogWarning(ex.ToString());
				}
			} while (_mStatus == NetPeerStatus.Running);

			//
			// perform shutdown
			//
			ExecutePeerShutdown();
		}

		private void ExecutePeerShutdown()
		{
			VerifyNetworkThread();

			LogDebug("Shutting down...");

			// disconnect and make one final heartbeat
			var list = new List<NetConnection>(MHandshakes.Count + MConnections.Count);
			lock (MConnections)
			{
			    list.AddRange(MConnections.Where(conn => conn != null));

			    lock (MHandshakes)
			    {
			        list.AddRange(MHandshakes.Values.Where(hs => hs != null));

			        // shut down connections
					foreach (var conn in list)
						conn.Shutdown(_mShutdownReason);
			    }
			}

		    FlushDelayedPackets();

			// one final heartbeat, will send stuff and do disconnect
			Heartbeat();

			Thread.Sleep(10);

			lock (_mInitializeLock)
			{
				try
				{
					if (_mSocket != null)
					{
						try
						{
							_mSocket.Shutdown(SocketShutdown.Receive);
						}
						catch(Exception ex)
						{
							LogDebug("Socket.Shutdown exception: " + ex);
						}

						try
						{
							_mSocket.Close(2); // 2 seconds timeout
						}
						catch (Exception ex)
						{
							LogDebug("Socket.Close exception: " + ex);
						}
					}
				}
				finally
				{
					_mSocket = null;
					_mStatus = NetPeerStatus.NotRunning;
					LogDebug("Shutdown complete");

					// wake up any threads waiting for server shutdown
					if (_mMessageReceivedEvent != null)
						_mMessageReceivedEvent.Set();
				}

				_mLastSocketBind = float.MinValue;
				MReceiveBuffer = null;
				MSendBuffer = null;
				MUnsentUnconnectedMessages.Clear();
				MConnections.Clear();
				_mConnectionLookup.Clear();
				MHandshakes.Clear();
			}
		}

		private void Heartbeat()
		{
			VerifyNetworkThread();

			var now = NetTime.Now;
			var delta = now - _mLastHeartbeat;

			var maxChbpS = 1250 - MConnections.Count;
			if (maxChbpS < 250)
				maxChbpS = 250;
			if (delta > (1.0 / maxChbpS) || delta < 0.0) // max connection heartbeats/second max
			{
				_mFrameCounter++;
				_mLastHeartbeat = now;

				// do handshake heartbeats
				if ((_mFrameCounter % 3) == 0)
				{
					foreach (var kvp in MHandshakes)
					{
						var conn = kvp.Value;
#if DEBUG
						// sanity check
						if (kvp.Key != kvp.Key)
							LogWarning("Sanity fail! Connection in handshake list under wrong key!");
#endif
						conn.UnconnectedHeartbeat(now);
						if (conn.MStatus == NetConnectionStatus.Connected || conn.MStatus == NetConnectionStatus.Disconnected)
						{
#if DEBUG
							// sanity check
							if (conn.MStatus == NetConnectionStatus.Disconnected && MHandshakes.ContainsKey(conn.RemoteEndPoint))
							{
								LogWarning("Sanity fail! Handshakes list contained disconnected connection!");
								MHandshakes.Remove(conn.RemoteEndPoint);
							}
#endif
							break; // collection has been modified
						}
					}
				}

#if DEBUG
				SendDelayedPackets();
#endif

				// update m_executeFlushSendQueue
				if (MConfiguration.MAutoFlushSendQueue && MNeedFlushSendQueue)
				{
					MExecuteFlushSendQueue = true;
					MNeedFlushSendQueue = false; // a race condition to this variable will simply result in a single superfluous call to FlushSendQueue()
				}

				// do connection heartbeats
				lock (MConnections)
				{
					for (var i = MConnections.Count - 1; i >= 0; i--)
					{
						var conn = MConnections[i];
						conn.Heartbeat(now, _mFrameCounter);
						if (conn.MStatus == NetConnectionStatus.Disconnected)
						{
							//
							// remove connection
							//
							MConnections.RemoveAt(i);
							_mConnectionLookup.Remove(conn.RemoteEndPoint);
						}
					}
				}
				MExecuteFlushSendQueue = false;

				// send unsent unconnected messages
				NetTuple<IPEndPoint, NetOutgoingMessage> unsent;
				while (MUnsentUnconnectedMessages.TryDequeue(out unsent))
				{
					var om = unsent.Item2;

					var len = om.Encode(MSendBuffer, 0, 0);

					Interlocked.Decrement(ref om.MRecyclingCount);
					if (om.MRecyclingCount <= 0)
						Recycle(om);

					bool connReset;
					SendPacket(len, unsent.Item1, 1, out connReset);
				}
			}

			//
			// read from socket
			//
			if (_mSocket == null)
				return;

			if (!_mSocket.Poll(1000, SelectMode.SelectRead)) // wait up to 1 ms for data to arrive
				return;

			//if (m_socket == null || m_socket.Available < 1)
			//	return;

			// update now
			now = NetTime.Now;

			do
			{
				var bytesReceived = 0;
				try
				{
					bytesReceived = _mSocket.ReceiveFrom(MReceiveBuffer, 0, MReceiveBuffer.Length, SocketFlags.None, ref _mSenderRemote);
				}
				catch (SocketException sx)
				{
					switch (sx.SocketErrorCode)
					{
						case SocketError.ConnectionReset:
							// connection reset by peer, aka connection forcibly closed aka "ICMP port unreachable" 
							// we should shut down the connection; but m_senderRemote seemingly cannot be trusted, so which connection should we shut down?!
							// So, what to do?
							LogWarning("ConnectionReset");
							return;

						case SocketError.NotConnected:
							// socket is unbound; try to rebind it (happens on mobile when process goes to sleep)
							BindSocket(true);
							return;

						default:
							LogWarning("Socket exception: " + sx);
							return;
					}
				}

				if (bytesReceived < NetConstants.HeaderByteSize)
					return;

				//LogVerbose("Received " + bytesReceived + " bytes");

				var ipsender = (IPEndPoint)_mSenderRemote;

				if (_mUpnp != null && now < _mUpnp.MDiscoveryResponseDeadline && bytesReceived > 32)
				{
					// is this an UPnP response?
					var resp = Encoding.ASCII.GetString(MReceiveBuffer, 0, bytesReceived);
					if (resp.Contains("upnp:rootdevice") || resp.Contains("UPnP/1.0"))
					{
						try
						{
							resp = resp.Substring(resp.ToLower().IndexOf("location:", StringComparison.Ordinal) + 9);
							resp = resp.Substring(0, resp.IndexOf("\r", StringComparison.Ordinal)).Trim();
							_mUpnp.ExtractServiceUrl(resp);
							return;
						}
						catch (Exception ex)
						{
							LogDebug("Failed to parse UPnP response: " + ex);

							// don't try to parse this packet further
							return;
						}
					}
				}

				NetConnection sender = null;
				_mConnectionLookup.TryGetValue(ipsender, out sender);

				//
				// parse packet into messages
				//
				var numMessages = 0;
				var numFragments = 0;
				var ptr = 0;
				while ((bytesReceived - ptr) >= NetConstants.HeaderByteSize)
				{
					// decode header
					//  8 bits - NetMessageType
					//  1 bit  - Fragment?
					// 15 bits - Sequence number
					// 16 bits - Payload length in bits

					numMessages++;

					var tp = (NetMessageType)MReceiveBuffer[ptr++];

					var low = MReceiveBuffer[ptr++];
					var high = MReceiveBuffer[ptr++];

					var isFragment = ((low & 1) == 1);
					var sequenceNumber = (ushort)((low >> 1) | (high << 7));

					numFragments++;

					var payloadBitLength = (ushort)(MReceiveBuffer[ptr++] | (MReceiveBuffer[ptr++] << 8));
					var payloadByteLength = NetUtility.BytesToHoldBits(payloadBitLength);

					if (bytesReceived - ptr < payloadByteLength)
					{
						LogWarning("Malformed packet; stated payload length " + payloadByteLength + ", remaining bytes " + (bytesReceived - ptr));
						return;
					}

					if (tp >= NetMessageType.Unused1 && tp <= NetMessageType.Unused29)
					{
						ThrowOrLog("Unexpected NetMessageType: " + tp);
						return;
					}

					try
					{
						if (tp >= NetMessageType.LibraryError)
						{
							if (sender != null)
								sender.ReceivedLibraryMessage(tp, ptr, payloadByteLength);
							else
								ReceivedUnconnectedLibraryMessage(now, ipsender, tp, ptr, payloadByteLength);
						}
						else
						{
							if (sender == null && !MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.UnconnectedData))
								return; // dropping unconnected message since it's not enabled

							var msg = CreateIncomingMessage(NetIncomingMessageType.Data, payloadByteLength);
							msg.MIsFragment = isFragment;
							msg.MReceiveTime = now;
							msg.MSequenceNumber = sequenceNumber;
							msg.MReceivedMessageType = tp;
							msg.MSenderConnection = sender;
							msg.MSenderEndPoint = ipsender;
							msg.MBitLength = payloadBitLength;

							Buffer.BlockCopy(MReceiveBuffer, ptr, msg.MData, 0, payloadByteLength);
							if (sender != null)
							{
								if (tp == NetMessageType.Unconnected)
								{
									// We're connected; but we can still send unconnected messages to this peer
									msg.MIncomingMessageType = NetIncomingMessageType.UnconnectedData;
									ReleaseMessage(msg);
								}
								else
								{
									// connected application (non-library) message
									sender.ReceivedMessage(msg);
								}
							}
							else
							{
								// at this point we know the message type is enabled
								// unconnected application (non-library) message
								msg.MIncomingMessageType = NetIncomingMessageType.UnconnectedData;
								ReleaseMessage(msg);
							}
						}
					}
					catch (Exception ex)
					{
						LogError("Packet parsing error: " + ex.Message + " from " + ipsender);
					}
					ptr += payloadByteLength;
				}

				MStatistics.PacketReceived(bytesReceived, numMessages, numFragments);
				if (sender != null)
					sender.MStatistics.PacketReceived(bytesReceived, numMessages, numFragments);

			} while (_mSocket.Available > 0);
		}

		/// <summary>
		/// If NetPeerConfiguration.AutoFlushSendQueue() is false; you need to call this to send all messages queued using SendMessage()
		/// </summary>
		public void FlushSendQueue()
		{
			MExecuteFlushSendQueue = true;
		}

		internal void HandleIncomingDiscoveryRequest(double now, IPEndPoint senderEndPoint, int ptr, int payloadByteLength)
		{
			if (MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.DiscoveryRequest))
			{
				var dm = CreateIncomingMessage(NetIncomingMessageType.DiscoveryRequest, payloadByteLength);
				if (payloadByteLength > 0)
					Buffer.BlockCopy(MReceiveBuffer, ptr, dm.MData, 0, payloadByteLength);
				dm.MReceiveTime = now;
				dm.MBitLength = payloadByteLength * 8;
				dm.MSenderEndPoint = senderEndPoint;
				ReleaseMessage(dm);
			}
		}

		internal void HandleIncomingDiscoveryResponse(double now, IPEndPoint senderEndPoint, int ptr, int payloadByteLength)
		{
			if (MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.DiscoveryResponse))
			{
				var dr = CreateIncomingMessage(NetIncomingMessageType.DiscoveryResponse, payloadByteLength);
				if (payloadByteLength > 0)
					Buffer.BlockCopy(MReceiveBuffer, ptr, dr.MData, 0, payloadByteLength);
				dr.MReceiveTime = now;
				dr.MBitLength = payloadByteLength * 8;
				dr.MSenderEndPoint = senderEndPoint;
				ReleaseMessage(dr);
			}
		}

		private void ReceivedUnconnectedLibraryMessage(double now, IPEndPoint senderEndPoint, NetMessageType tp, int ptr, int payloadByteLength)
		{
			NetConnection shake;
			if (MHandshakes.TryGetValue(senderEndPoint, out shake))
			{
				shake.ReceivedHandshake(now, tp, ptr, payloadByteLength);
				return;
			}

			//
			// Library message from a completely unknown sender; lets just accept Connect
			//
			switch (tp)
			{
				case NetMessageType.Discovery:
					HandleIncomingDiscoveryRequest(now, senderEndPoint, ptr, payloadByteLength);
					return;
				case NetMessageType.DiscoveryResponse:
					HandleIncomingDiscoveryResponse(now, senderEndPoint, ptr, payloadByteLength);
					return;
				case NetMessageType.NatIntroduction:
					if (MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.NatIntroductionSuccess))
						HandleNatIntroduction(ptr);
					return;
				case NetMessageType.NatPunchMessage:
					if (MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.NatIntroductionSuccess))
						HandleNatPunch(ptr, senderEndPoint);
					return;
				case NetMessageType.ConnectResponse:

					lock (MHandshakes)
					{
						foreach (var hs in MHandshakes)
						{
							if (hs.Key.Address.Equals(senderEndPoint.Address))
							{
								if (hs.Value.MConnectionInitiator)
								{
									//
									// We are currently trying to connection to XX.XX.XX.XX:Y
									// ... but we just received a ConnectResponse from XX.XX.XX.XX:Z
									// Lets just assume the router decided to use this port instead
									//
									var hsconn = hs.Value;
									_mConnectionLookup.Remove(hs.Key);
									MHandshakes.Remove(hs.Key);

									LogDebug("Detected host port change; rerouting connection to " + senderEndPoint);
									hsconn.MutateEndPoint(senderEndPoint);

									_mConnectionLookup.Add(senderEndPoint, hsconn);
									MHandshakes.Add(senderEndPoint, hsconn);

									hsconn.ReceivedHandshake(now, tp, ptr, payloadByteLength);
									return;
								}
							}
						}
					}

					LogWarning("Received unhandled library message " + tp + " from " + senderEndPoint);
					return;
				case NetMessageType.Connect:
					if (MConfiguration.AcceptIncomingConnections == false)
					{
						LogWarning("Received Connect, but we're not accepting incoming connections!");
						return;
					}
					// handle connect
					// It's someone wanting to shake hands with us!

					var reservedSlots = MHandshakes.Count + MConnections.Count;
					if (reservedSlots >= MConfiguration.MMaximumConnections)
					{
						// server full
						var full = CreateMessage("Server full");
						full.MMessageType = NetMessageType.Disconnect;
						SendLibrary(full, senderEndPoint);
						return;
					}

					// Ok, start handshake!
					var conn = new NetConnection(this, senderEndPoint);
					conn.MStatus = NetConnectionStatus.ReceivedInitiation;
					MHandshakes.Add(senderEndPoint, conn);
					conn.ReceivedHandshake(now, tp, ptr, payloadByteLength);
					return;

				case NetMessageType.Disconnect:
					// this is probably ok
					LogVerbose("Received Disconnect from unconnected source: " + senderEndPoint);
					return;
				default:
					LogWarning("Received unhandled library message " + tp + " from " + senderEndPoint);
					return;
			}
		}

		internal void AcceptConnection(NetConnection conn)
		{
			// LogDebug("Accepted connection " + conn);
			conn.InitExpandMtu(NetTime.Now);

			if (MHandshakes.Remove(conn.MRemoteEndPoint) == false)
				LogWarning("AcceptConnection called but m_handshakes did not contain it!");

			lock (MConnections)
			{
				if (MConnections.Contains(conn))
				{
					LogWarning("AcceptConnection called but m_connection already contains it!");
				}
				else
				{
					MConnections.Add(conn);
					_mConnectionLookup.Add(conn.MRemoteEndPoint, conn);
				}
			}
		}

		[Conditional("DEBUG")]
		internal void VerifyNetworkThread()
		{
			var ct = Thread.CurrentThread;
			if (Thread.CurrentThread != _mNetworkThread)
				throw new NetException("Executing on wrong thread! Should be library system thread (is " + ct.Name + " mId " + ct.ManagedThreadId + ")");
		}

		internal NetIncomingMessage SetupReadHelperMessage(int ptr, int payloadLength)
		{
			VerifyNetworkThread();

			MReadHelperMessage.MBitLength = (ptr + payloadLength) * 8;
			MReadHelperMessage.MReadPosition = (ptr * 8);
			return MReadHelperMessage;
		}
	}
}