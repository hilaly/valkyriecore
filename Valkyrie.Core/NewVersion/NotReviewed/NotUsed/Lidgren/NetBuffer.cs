﻿using System;
using System.Collections.Generic;
using System.Reflection;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetBuffer
	{
		/// <summary>
		/// Number of bytes to overallocate for each message to avoid resizing
		/// </summary>
		protected const int COverAllocateAmount = 4;

		private static readonly Dictionary<Type, MethodInfo> SReadMethods;
		private static readonly Dictionary<Type, MethodInfo> SWriteMethods;

		internal byte[] MData;
		internal int MBitLength;
		internal int MReadPosition;

		/// <summary>
		/// Gets or sets the internal data buffer
		/// </summary>
		public byte[] Data
		{
			get { return MData; }
			set { MData = value; }
		}

		/// <summary>
		/// Gets or sets the length of the used portion of the buffer in bytes
		/// </summary>
		public int LengthBytes
		{
			get { return ((MBitLength + 7) >> 3); }
			set
			{
				MBitLength = value * 8;
				InternalEnsureBufferSize(MBitLength);
			}
		}

		/// <summary>
		/// Gets or sets the length of the used portion of the buffer in bits
		/// </summary>
		public int LengthBits
		{
			get { return MBitLength; }
			set
			{
				MBitLength = value;
				InternalEnsureBufferSize(MBitLength);
			}
		}

		/// <summary>
		/// Gets or sets the read position in the buffer, in bits (not bytes)
		/// </summary>
		public long Position
		{
			get { return MReadPosition; }
			set { MReadPosition = (int)value; }
		}

		/// <summary>
		/// Gets the position in the buffer in bytes; note that the bits of the first returned byte may already have been read - check the Position property to make sure.
		/// </summary>
		public int PositionInBytes
		{
			get { return MReadPosition / 8; }
		}
		
		static NetBuffer()
		{
			SReadMethods = new Dictionary<Type, MethodInfo>();
			var methods = typeof(NetIncomingMessage).GetMethods(BindingFlags.Instance | BindingFlags.Public);
			foreach (var mi in methods)
			{
				if (mi.GetParameters().Length == 0 && mi.Name.StartsWith("Read", StringComparison.InvariantCulture) && mi.Name.Substring(4) == mi.ReturnType.Name)
				{
					SReadMethods[mi.ReturnType] = mi;
				}
                else if (mi.GetParameters().Length == 0 && mi.Name.StartsWith("Read", StringComparison.InvariantCulture) && mi.ReturnType.Name.Contains("List"))
                    SReadMethods[mi.ReturnType] = mi;
			}

			SWriteMethods = new Dictionary<Type, MethodInfo>();
			methods = typeof(NetOutgoingMessage).GetMethods(BindingFlags.Instance | BindingFlags.Public);
			foreach (var mi in methods)
			{
				if (mi.Name.Equals("Write", StringComparison.InvariantCulture))
				{
					var pis = mi.GetParameters();
					if (pis.Length == 1)
						SWriteMethods[pis[0].ParameterType] = mi;
				}
			}
		}
	}
}
