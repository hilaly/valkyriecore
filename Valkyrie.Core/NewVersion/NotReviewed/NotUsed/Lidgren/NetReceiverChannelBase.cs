﻿// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	internal abstract class NetReceiverChannelBase
	{
		internal NetPeer MPeer;
		internal NetConnection MConnection;

	    protected NetReceiverChannelBase(NetConnection connection)
		{
			MConnection = connection;
			MPeer = connection.MPeer;
		}

		internal abstract void ReceiveMessage(NetIncomingMessage msg);
	}
}
