﻿using System.Collections.Generic;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetPeer
	{
		internal List<byte[]> MStoragePool;
		private NetQueue<NetOutgoingMessage> _mOutgoingMessagesPool;
		private NetQueue<NetIncomingMessage> _mIncomingMessagesPool;

		internal int MStoragePoolBytes;
		internal int MStorageSlotsUsedCount;
		private int _mMaxCacheCount;

		private void InitializePools()
		{
			MStorageSlotsUsedCount = 0;

			if (MConfiguration.UseMessageRecycling)
			{
				MStoragePool = new List<byte[]>(16);
				_mOutgoingMessagesPool = new NetQueue<NetOutgoingMessage>(4);
				_mIncomingMessagesPool = new NetQueue<NetIncomingMessage>(4);
			}
			else
			{
				MStoragePool = null;
				_mOutgoingMessagesPool = null;
				_mIncomingMessagesPool = null;
			}

			_mMaxCacheCount = MConfiguration.RecycledCacheMaxCount;
		}

		internal byte[] GetStorage(int minimumCapacityInBytes)
		{
			if (MStoragePool == null)
				return new byte[minimumCapacityInBytes];

			lock (MStoragePool)
			{
				for (var i = 0; i < MStoragePool.Count; i++)
				{
					var retval = MStoragePool[i];
					if (retval != null && retval.Length >= minimumCapacityInBytes)
					{
						MStoragePool[i] = null;
						MStorageSlotsUsedCount--;
						MStoragePoolBytes -= retval.Length;
						return retval;
					}
				}
			}
			MStatistics.MBytesAllocated += minimumCapacityInBytes;
			return new byte[minimumCapacityInBytes];
		}

		internal void Recycle(byte[] storage)
		{
			if (MStoragePool == null || storage == null)
				return;

			lock (MStoragePool)
			{
				var cnt = MStoragePool.Count;
				for (var i = 0; i < cnt; i++)
				{
					if (MStoragePool[i] == null)
					{
						MStorageSlotsUsedCount++;
						MStoragePoolBytes += storage.Length;
						MStoragePool[i] = storage;
						return;
					}
				}

				if (MStoragePool.Count >= _mMaxCacheCount)
				{
					// pool is full; replace randomly chosen entry to keep size distribution
					var idx = NetRandom.Instance.Next(MStoragePool.Count);

					MStoragePoolBytes -= MStoragePool[idx].Length;
					MStoragePoolBytes += storage.Length;
					
					MStoragePool[idx] = storage; // replace
				}
				else
				{
					MStorageSlotsUsedCount++;
					MStoragePoolBytes += storage.Length;
					MStoragePool.Add(storage);
				}
			}
		}

		/// <summary>
		/// Creates a new message for sending
		/// </summary>
		public NetOutgoingMessage CreateMessage()
		{
			return CreateMessage(MConfiguration.MDefaultOutgoingMessageCapacity);
		}

		/// <summary>
		/// Creates a new message for sending and writes the provided string to it
		/// </summary>
		public NetOutgoingMessage CreateMessage(string content)
		{
			var om = CreateMessage(2 + content.Length); // fair guess
			om.Write(content);
			return om;
		}

		/// <summary>
		/// Creates a new message for sending
		/// </summary>
		/// <param name="initialCapacity">initial capacity in bytes</param>
		public NetOutgoingMessage CreateMessage(int initialCapacity)
		{
			NetOutgoingMessage retval;
			if (_mOutgoingMessagesPool == null || !_mOutgoingMessagesPool.TryDequeue(out retval))
				retval = new NetOutgoingMessage();

			NetException.Assert(retval.MRecyclingCount == 0, "Wrong recycling count! Should be zero" + retval.MRecyclingCount);

			if (initialCapacity > 0)
				retval.MData = GetStorage(initialCapacity);

			return retval;
		}

		internal NetIncomingMessage CreateIncomingMessage(NetIncomingMessageType tp, byte[] useStorageData)
		{
			NetIncomingMessage retval;
			if (_mIncomingMessagesPool == null || !_mIncomingMessagesPool.TryDequeue(out retval))
				retval = new NetIncomingMessage(tp);
			else
				retval.MIncomingMessageType = tp;
			retval.MData = useStorageData;
			return retval;
		}

		internal NetIncomingMessage CreateIncomingMessage(NetIncomingMessageType tp, int minimumByteSize)
		{
			NetIncomingMessage retval;
			if (_mIncomingMessagesPool == null || !_mIncomingMessagesPool.TryDequeue(out retval))
				retval = new NetIncomingMessage(tp);
			else
				retval.MIncomingMessageType = tp;
			retval.MData = GetStorage(minimumByteSize);
			return retval;
		}

		/// <summary>
		/// Recycles a NetIncomingMessage instance for reuse; taking pressure off the garbage collector
		/// </summary>
		public void Recycle(NetIncomingMessage msg)
		{
			if (_mIncomingMessagesPool == null || msg == null)
				return;

			NetException.Assert(_mIncomingMessagesPool.Contains(msg) == false, "Recyling already recycled incoming message! Thread race?");

			var storage = msg.MData;
			msg.MData = null;
			Recycle(storage);
			msg.Reset();

			if (_mIncomingMessagesPool.Count < _mMaxCacheCount)
				_mIncomingMessagesPool.Enqueue(msg);
		}

		/// <summary>
		/// Recycles a list of NetIncomingMessage instances for reuse; taking pressure off the garbage collector
		/// </summary>
		public void Recycle(IEnumerable<NetIncomingMessage> toRecycle)
		{
			if (_mIncomingMessagesPool == null)
				return;
			foreach (var im in toRecycle)
				Recycle(im);
		}

		internal void Recycle(NetOutgoingMessage msg)
		{
			if (_mOutgoingMessagesPool == null)
				return;
#if DEBUG
			NetException.Assert(_mOutgoingMessagesPool.Contains(msg) == false, "Recyling already recycled outgoing message! Thread race?");
			if (msg.MRecyclingCount != 0)
				LogWarning("Wrong recycling count! should be zero; found " + msg.MRecyclingCount);
#endif
			// setting m_recyclingCount to zero SHOULD be an unnecessary maneuver, if it's not zero something is wrong
			// however, in RELEASE, we'll just have to accept this and move on with life
			msg.MRecyclingCount = 0;

			var storage = msg.MData;
			msg.MData = null;

			// message fragments cannot be recycled
			// TODO: find a way to recycle large message after all fragments has been acknowledged; or? possibly better just to garbage collect them
			if (msg.MFragmentGroup == 0)
				Recycle(storage);

			msg.Reset();
			if (_mOutgoingMessagesPool.Count < _mMaxCacheCount)
				_mOutgoingMessagesPool.Enqueue(msg);
		}

		/// <summary>
		/// Creates an incoming message with the required capacity for releasing to the application
		/// </summary>
		internal NetIncomingMessage CreateIncomingMessage(NetIncomingMessageType tp, string text)
		{
			NetIncomingMessage retval;
			if (string.IsNullOrEmpty(text))
			{
				retval = CreateIncomingMessage(tp, 1);
				retval.Write(string.Empty);
				return retval;
			}

			var numBytes = Encoding.UTF8.GetByteCount(text);
			retval = CreateIncomingMessage(tp, numBytes + (numBytes > 127 ? 2 : 1));
			retval.Write(text);

			return retval;
		}
	}
}
