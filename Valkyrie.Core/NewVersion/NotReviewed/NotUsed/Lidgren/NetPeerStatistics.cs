﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

// Uncomment the line below to get statistics in RELEASE builds
//#define USE_RELEASE_STATISTICS

using System.Diagnostics;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	/// <summary>
	/// Statistics for a NetPeer instance
	/// </summary>
	public sealed class NetPeerStatistics
	{
		private readonly NetPeer _mPeer;

		internal int MSentPackets;
		internal int MReceivedPackets;

		internal int MSentMessages;
		internal int MReceivedMessages;
		internal int MReceivedFragments;

		internal int MSentBytes;
		internal int MReceivedBytes;

		internal long MBytesAllocated;

		internal NetPeerStatistics(NetPeer peer)
		{
			_mPeer = peer;
			Reset();
		}

		internal void Reset()
		{
			MSentPackets = 0;
			MReceivedPackets = 0;

			MSentMessages = 0;
			MReceivedMessages = 0;
			MReceivedFragments = 0;

			MSentBytes = 0;
			MReceivedBytes = 0;

			MBytesAllocated = 0;
		}

		/// <summary>
		/// Gets the number of sent packets since the NetPeer was initialized
		/// </summary>
		public int SentPackets { get { return MSentPackets; } }

		/// <summary>
		/// Gets the number of received packets since the NetPeer was initialized
		/// </summary>
		public int ReceivedPackets { get { return MReceivedPackets; } }

		/// <summary>
		/// Gets the number of sent messages since the NetPeer was initialized
		/// </summary>
		public int SentMessages { get { return MSentMessages; } }

		/// <summary>
		/// Gets the number of received messages since the NetPeer was initialized
		/// </summary>
		public int ReceivedMessages { get { return MReceivedMessages; } }

		/// <summary>
		/// Gets the number of sent bytes since the NetPeer was initialized
		/// </summary>
		public int SentBytes { get { return MSentBytes; } }

		/// <summary>
		/// Gets the number of received bytes since the NetPeer was initialized
		/// </summary>
		public int ReceivedBytes { get { return MReceivedBytes; } }

		/// <summary>
		/// Gets the number of bytes allocated (and possibly garbage collected) for message storage
		/// </summary>
		public long StorageBytesAllocated { get { return MBytesAllocated; } }

		/// <summary>
		/// Gets the number of bytes in the recycled pool
		/// </summary>
		public int BytesInRecyclePool { get { return _mPeer.MStoragePoolBytes; } }

#if USE_RELEASE_STATISTICS
		internal void PacketSent(int numBytes, int numMessages)
		{
			m_sentPackets++;
			m_sentBytes += numBytes;
			m_sentMessages += numMessages;
		}
#else
		[Conditional("DEBUG")]
		internal void PacketSent(int numBytes, int numMessages)
		{
			MSentPackets++;
			MSentBytes += numBytes;
			MSentMessages += numMessages;
		}
#endif

#if USE_RELEASE_STATISTICS
		internal void PacketReceived(int numBytes, int numMessages)
		{
			m_receivedPackets++;
			m_receivedBytes += numBytes;
			m_receivedMessages += numMessages;
		}
#else
		[Conditional("DEBUG")]
		internal void PacketReceived(int numBytes, int numMessages, int numFragments)
		{
			MReceivedPackets++;
			MReceivedBytes += numBytes;
			MReceivedMessages += numMessages;
			MReceivedFragments += numFragments;
		}
#endif

		/// <summary>
		/// Returns a string that represents this object
		/// </summary>
		public override string ToString()
		{
			var bdr = new StringBuilder();
			bdr.AppendLine(_mPeer.ConnectionsCount + " connections");
#if DEBUG || USE_RELEASE_STATISTICS
			bdr.AppendLine("Sent " + MSentBytes + " bytes in " + MSentMessages + " messages in " + MSentPackets + " packets");
			bdr.AppendLine("Received " + MReceivedBytes + " bytes in " + MReceivedMessages + " messages (of which " + MReceivedFragments + " fragments) in " + MReceivedPackets + " packets");
#else
			bdr.AppendLine("Sent (n/a) bytes in (n/a) messages in (n/a) packets");
			bdr.AppendLine("Received (n/a) bytes in (n/a) messages in (n/a) packets");
#endif
			bdr.AppendLine("Storage allocated " + MBytesAllocated + " bytes");
			if (_mPeer.MStoragePool != null)
				bdr.AppendLine("Recycled pool " + _mPeer.MStoragePoolBytes + " bytes (" + _mPeer.MStorageSlotsUsedCount + " entries)");
			return bdr.ToString();
		}
	}
}
