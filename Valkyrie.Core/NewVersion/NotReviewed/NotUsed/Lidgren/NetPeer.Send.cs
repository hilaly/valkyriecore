﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public partial class NetPeer
	{
		/// <summary>
		/// Send a message to a specific connection
		/// </summary>
		/// <param name="msg">The message to send</param>
		/// <param name="recipient">The recipient connection</param>
		/// <param name="method">How to deliver the message</param>
		public NetSendResult SendMessage(NetOutgoingMessage msg, NetConnection recipient, NetDeliveryMethod method)
		{
			return SendMessage(msg, recipient, method, 0);
		}

		/// <summary>
		/// Send a message to a specific connection
		/// </summary>
		/// <param name="msg">The message to send</param>
		/// <param name="recipient">The recipient connection</param>
		/// <param name="method">How to deliver the message</param>
		/// <param name="sequenceChannel">Sequence channel within the delivery method</param>
		public NetSendResult SendMessage(NetOutgoingMessage msg, NetConnection recipient, NetDeliveryMethod method, int sequenceChannel)
		{
			if (msg == null)
				throw new ArgumentNullException("msg");
			if (recipient == null)
				throw new ArgumentNullException("recipient");
			if (sequenceChannel >= NetConstants.NetChannelsPerDeliveryMethod)
				throw new ArgumentOutOfRangeException("sequenceChannel");

			NetException.Assert(
				((method != NetDeliveryMethod.Unreliable && method != NetDeliveryMethod.ReliableUnordered) ||
				((method == NetDeliveryMethod.Unreliable || method == NetDeliveryMethod.ReliableUnordered) && sequenceChannel == 0)),
				"Delivery method " + method + " cannot use sequence channels other than 0!"
			);

			NetException.Assert(method != NetDeliveryMethod.Unknown, "Bad delivery method!");

			if (msg.MIsSent)
				throw new NetException("This message has already been sent! Use NetPeer.SendMessage() to send to multiple recipients efficiently");
			msg.MIsSent = true;

			var suppressFragmentation = (method == NetDeliveryMethod.Unreliable || method == NetDeliveryMethod.UnreliableSequenced) && MConfiguration.UnreliableSizeBehaviour != NetUnreliableSizeBehaviour.NormalFragmentation;

			var len = NetConstants.UnfragmentedMessageHeaderSize + msg.LengthBytes; // headers + length, faster than calling msg.GetEncodedSize
			if (len <= recipient.MCurrentMtu || suppressFragmentation)
			{
				Interlocked.Increment(ref msg.MRecyclingCount);
				return recipient.EnqueueMessage(msg, method, sequenceChannel);
			}
		    // message must be fragmented!
		    if (recipient.MStatus != NetConnectionStatus.Connected)
		        return NetSendResult.FailedNotConnected;
		    return SendFragmentedMessage(msg, new[] { recipient }, method, sequenceChannel);
		}

		internal static int GetMtu(IList<NetConnection> recipients)
		{
			var count = recipients.Count;

			var mtu = int.MaxValue;
			if (count < 1)
			{
#if DEBUG
				throw new NetException("GetMTU called with no recipients");
#else
				// we don't have access to the particular peer, so just use default MTU
				return NetPeerConfiguration.KDefaultMtu;
#endif
            }

			for(var i=0;i<count;i++)
			{
				var conn = recipients[i];
				var cmtu = conn.MCurrentMtu;
				if (cmtu < mtu)
					mtu = cmtu;
			}
			return mtu;
		}

		/// <summary>
		/// Send a message to a list of connections
		/// </summary>
		/// <param name="msg">The message to send</param>
		/// <param name="recipients">The list of recipients to send to</param>
		/// <param name="method">How to deliver the message</param>
		/// <param name="sequenceChannel">Sequence channel within the delivery method</param>
		public void SendMessage(NetOutgoingMessage msg, IList<NetConnection> recipients, NetDeliveryMethod method, int sequenceChannel)
		{
			if (msg == null)
				throw new ArgumentNullException("msg");
			if (recipients == null)
				throw new ArgumentNullException("recipients");
			if (recipients.Count < 1)
				throw new NetException("recipients must contain at least one item");
			if (method == NetDeliveryMethod.Unreliable || method == NetDeliveryMethod.ReliableUnordered)
				NetException.Assert(sequenceChannel == 0, "Delivery method " + method + " cannot use sequence channels other than 0!");
			if (msg.MIsSent)
				throw new NetException("This message has already been sent! Use NetPeer.SendMessage() to send to multiple recipients efficiently");
			msg.MIsSent = true;

			var mtu = GetMtu(recipients);

			var len = msg.GetEncodedSize();
			if (len <= mtu)
			{
				Interlocked.Add(ref msg.MRecyclingCount, recipients.Count);
				foreach (var conn in recipients)
				{
					if (conn == null)
					{
						Interlocked.Decrement(ref msg.MRecyclingCount);
						continue;
					}
					var res = conn.EnqueueMessage(msg, method, sequenceChannel);
					if (res == NetSendResult.Dropped)
						Interlocked.Decrement(ref msg.MRecyclingCount);
				}
			}
			else
			{
				// message must be fragmented!
				SendFragmentedMessage(msg, recipients, method, sequenceChannel);
			}
		}

		/// <summary>
		/// Send a message to an unconnected host
		/// </summary>
		public void SendUnconnectedMessage(NetOutgoingMessage msg, string host, int port)
		{
			if (msg == null)
				throw new ArgumentNullException("msg");
			if (host == null)
				throw new ArgumentNullException("host");
			if (msg.MIsSent)
				throw new NetException("This message has already been sent! Use NetPeer.SendMessage() to send to multiple recipients efficiently");
			if (msg.LengthBytes > MConfiguration.MaximumTransmissionUnit)
				throw new NetException("Unconnected messages too long! Must be shorter than NetConfiguration.MaximumTransmissionUnit (currently " + MConfiguration.MaximumTransmissionUnit + ")");

			msg.MIsSent = true;
			msg.MMessageType = NetMessageType.Unconnected;

			var adr = NetUtility.Resolve(host);
			if (adr == null)
				throw new NetException("Failed to resolve " + host);

			Interlocked.Increment(ref msg.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(new IPEndPoint(adr, port), msg));
		}

		/// <summary>
		/// Send a message to an unconnected host
		/// </summary>
		public void SendUnconnectedMessage(NetOutgoingMessage msg, IPEndPoint recipient)
		{
			if (msg == null)
				throw new ArgumentNullException("msg");
			if (recipient == null)
				throw new ArgumentNullException("recipient");
			if (msg.MIsSent)
				throw new NetException("This message has already been sent! Use NetPeer.SendMessage() to send to multiple recipients efficiently");
			if (msg.LengthBytes > MConfiguration.MaximumTransmissionUnit)
				throw new NetException("Unconnected messages too long! Must be shorter than NetConfiguration.MaximumTransmissionUnit (currently " + MConfiguration.MaximumTransmissionUnit + ")");

			msg.MMessageType = NetMessageType.Unconnected;
			msg.MIsSent = true;

			Interlocked.Increment(ref msg.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(recipient, msg));
		}

		/// <summary>
		/// Send a message to an unconnected host
		/// </summary>
		public void SendUnconnectedMessage(NetOutgoingMessage msg, IList<IPEndPoint> recipients)
		{
			if (msg == null)
				throw new ArgumentNullException("msg");
			if (recipients == null)
				throw new ArgumentNullException("recipients");
			if (recipients.Count < 1)
				throw new NetException("recipients must contain at least one item");
			if (msg.MIsSent)
				throw new NetException("This message has already been sent! Use NetPeer.SendMessage() to send to multiple recipients efficiently");
			if (msg.LengthBytes > MConfiguration.MaximumTransmissionUnit)
				throw new NetException("Unconnected messages too long! Must be shorter than NetConfiguration.MaximumTransmissionUnit (currently " + MConfiguration.MaximumTransmissionUnit + ")");

			msg.MMessageType = NetMessageType.Unconnected;
			msg.MIsSent = true;

			Interlocked.Add(ref msg.MRecyclingCount, recipients.Count);
			foreach(var ep in recipients)
				MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(ep, msg));
		}

		/// <summary>
		/// Send a message to this exact same netpeer (loopback)
		/// </summary>
		public void SendUnconnectedToSelf(NetOutgoingMessage om)
		{
			if (om == null)
				throw new ArgumentNullException("msg");
			if (om.MIsSent)
				throw new NetException("This message has already been sent! Use NetPeer.SendMessage() to send to multiple recipients efficiently");

			om.MMessageType = NetMessageType.Unconnected;
			om.MIsSent = true;

			if (MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.UnconnectedData) == false)
			{
				Interlocked.Decrement(ref om.MRecyclingCount);
				return; // dropping unconnected message since it's not enabled for receiving
			}

			// convert outgoing to incoming
			var im = CreateIncomingMessage(NetIncomingMessageType.UnconnectedData, om.LengthBytes);
			im.Write(om);
			im.MIsFragment = false;
			im.MReceiveTime = NetTime.Now;
			im.MSenderConnection = null;
			im.MSenderEndPoint = _mSocket.LocalEndPoint as IPEndPoint;
			NetException.Assert(im.MBitLength == om.LengthBits);

			// recycle outgoing message
			Recycle(om);

			ReleaseMessage(im);
		}
	}
}
