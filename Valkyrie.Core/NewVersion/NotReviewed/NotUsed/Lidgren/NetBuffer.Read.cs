﻿using System;
using System.Net;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	/// <summary>
	/// Base class for NetIncomingMessage and NetOutgoingMessage
	/// </summary>
	public partial class NetBuffer
	{
		private const string CReadOverflowError = "Trying to read past the buffer size - likely caused by mismatching Write/Reads, different size or order.";

		/// <summary>
		/// Reads a boolean value (stored as a single bit) written using Write(bool)
		/// </summary>
		public bool ReadBoolean()
		{
			NetException.Assert(MBitLength - MReadPosition >= 1, CReadOverflowError);
			var retval = NetBitWriter.ReadByte(MData, 1, MReadPosition);
			MReadPosition += 1;
			return (retval > 0 ? true : false);
		}
		
		/// <summary>
		/// Reads a byte
		/// </summary>
		public byte ReadByte()
		{
			NetException.Assert(MBitLength - MReadPosition >= 8, CReadOverflowError);
			var retval = NetBitWriter.ReadByte(MData, 8, MReadPosition);
			MReadPosition += 8;
			return retval;
		}

		/// <summary>
		/// Reads a byte and returns true or false for success
		/// </summary>
		public bool ReadByte(out byte result)
		{
			if (MBitLength - MReadPosition < 8)
			{
				result = 0;
				return false;
			}
			result = NetBitWriter.ReadByte(MData, 8, MReadPosition);
			MReadPosition += 8;
			return true;
		}

		/// <summary>
		/// Reads a signed byte
		/// </summary>
		[CLSCompliant(false)]
		public sbyte ReadSByte()
		{
			NetException.Assert(MBitLength - MReadPosition >= 8, CReadOverflowError);
			var retval = NetBitWriter.ReadByte(MData, 8, MReadPosition);
			MReadPosition += 8;
			return (sbyte)retval;
		}

		/// <summary>
		/// Reads 1 to 8 bits into a byte
		/// </summary>
		public byte ReadByte(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 8, "ReadByte(bits) can only read between 1 and 8 bits");
			var retval = NetBitWriter.ReadByte(MData, numberOfBits, MReadPosition);
			MReadPosition += numberOfBits;
			return retval;
		}

		/// <summary>
		/// Reads the specified number of bytes
		/// </summary>
		public byte[] ReadBytes(int numberOfBytes)
		{
			NetException.Assert(MBitLength - MReadPosition + 7 >= (numberOfBytes * 8), CReadOverflowError);

			var retval = new byte[numberOfBytes];
			NetBitWriter.ReadBytes(MData, numberOfBytes, MReadPosition, retval, 0);
			MReadPosition += (8 * numberOfBytes);
			return retval;
		}

		/// <summary>
		/// Reads the specified number of bytes and returns true for success
		/// </summary>
		public bool ReadBytes(int numberOfBytes, out byte[] result)
		{
			if (MBitLength - MReadPosition + 7 < (numberOfBytes * 8))
			{
				result = null;
				return false;
			}

			result = new byte[numberOfBytes];
			NetBitWriter.ReadBytes(MData, numberOfBytes, MReadPosition, result, 0);
			MReadPosition += (8 * numberOfBytes);
			return true;
		}

		/// <summary>
		/// Reads the specified number of bytes into a preallocated array
		/// </summary>
		/// <param name="into">The destination array</param>
		/// <param name="offset">The offset where to start writing in the destination array</param>
		/// <param name="numberOfBytes">The number of bytes to read</param>
		public void ReadBytes(byte[] into, int offset, int numberOfBytes)
		{
			NetException.Assert(MBitLength - MReadPosition + 7 >= (numberOfBytes * 8), CReadOverflowError);
			NetException.Assert(offset + numberOfBytes <= into.Length);

			NetBitWriter.ReadBytes(MData, numberOfBytes, MReadPosition, into, offset);
			MReadPosition += (8 * numberOfBytes);
		}

		/// <summary>
		/// Reads the specified number of bits into a preallocated array
		/// </summary>
		/// <param name="into">The destination array</param>
		/// <param name="offset">The offset where to start writing in the destination array</param>
		/// <param name="numberOfBits">The number of bits to read</param>
		public void ReadBits(byte[] into, int offset, int numberOfBits)
		{
			NetException.Assert(MBitLength - MReadPosition >= numberOfBits, CReadOverflowError);
			NetException.Assert(offset + NetUtility.BytesToHoldBits(numberOfBits) <= into.Length);

			var numberOfWholeBytes = numberOfBits / 8;
			var extraBits = numberOfBits - (numberOfWholeBytes * 8);

			NetBitWriter.ReadBytes(MData, numberOfWholeBytes, MReadPosition, into, offset);
			MReadPosition += (8 * numberOfWholeBytes);

			if (extraBits > 0)
				into[offset + numberOfWholeBytes] = ReadByte(extraBits);
		}

		/// <summary>
		/// Reads a 16 bit signed integer written using Write(Int16)
		/// </summary>
		public Int16 ReadInt16()
		{
			NetException.Assert(MBitLength - MReadPosition >= 16, CReadOverflowError);
			uint retval = NetBitWriter.ReadUInt16(MData, 16, MReadPosition);
			MReadPosition += 16;
			return (short)retval;
		}

		/// <summary>
		/// Reads a 16 bit unsigned integer written using Write(UInt16)
		/// </summary>
		[CLSCompliant(false)]
		public UInt16 ReadUInt16()
		{
			NetException.Assert(MBitLength - MReadPosition >= 16, CReadOverflowError);
			uint retval = NetBitWriter.ReadUInt16(MData, 16, MReadPosition);
			MReadPosition += 16;
			return (ushort)retval;
		}

		/// <summary>
		/// Reads a 32 bit signed integer written using Write(Int32)
		/// </summary>
		public Int32 ReadInt32()
		{
			NetException.Assert(MBitLength - MReadPosition >= 32, CReadOverflowError);
			var retval = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			MReadPosition += 32;
			return (Int32)retval;
		}

		/// <summary>
		/// Reads a 32 bit signed integer written using Write(Int32)
		/// </summary>
		[CLSCompliant(false)]
		public bool ReadInt32(out Int32 result)
		{
			if (MBitLength - MReadPosition < 32)
			{
				result = 0;
				return false;
			}

			result = (Int32)NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			MReadPosition += 32;
			return true;
		}

		/// <summary>
		/// Reads a signed integer stored in 1 to 32 bits, written using Write(Int32, Int32)
		/// </summary>
		public Int32 ReadInt32(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 32, "ReadInt32(bits) can only read between 1 and 32 bits");
			NetException.Assert(MBitLength - MReadPosition >= numberOfBits, CReadOverflowError);

			var retval = NetBitWriter.ReadUInt32(MData, numberOfBits, MReadPosition);
			MReadPosition += numberOfBits;

			if (numberOfBits == 32)
				return (int)retval;

			var signBit = 1 << (numberOfBits - 1);
			if ((retval & signBit) == 0)
				return (int)retval; // positive

			// negative
			unchecked
			{
				var mask = ((uint)-1) >> (33 - numberOfBits);
				var tmp = (retval & mask) + 1;
				return -((int)tmp);
			}
		}

		/// <summary>
		/// Reads an 32 bit unsigned integer written using Write(UInt32)
		/// </summary>
		[CLSCompliant(false)]
		public UInt32 ReadUInt32()
		{
			NetException.Assert(MBitLength - MReadPosition >= 32, CReadOverflowError);
			var retval = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			MReadPosition += 32;
			return retval;
		}

		/// <summary>
		/// Reads an 32 bit unsigned integer written using Write(UInt32) and returns true for success
		/// </summary>
		[CLSCompliant(false)]
		public bool ReadUInt32(out UInt32 result)
		{
			if (MBitLength - MReadPosition < 32)
			{
				result = 0;
				return false;
			}
			result = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			MReadPosition += 32;
			return true;
		}

		/// <summary>
		/// Reads an unsigned integer stored in 1 to 32 bits, written using Write(UInt32, Int32)
		/// </summary>
		[CLSCompliant(false)]
		public UInt32 ReadUInt32(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 32, "ReadUInt32(bits) can only read between 1 and 32 bits");
			//NetException.Assert(m_bitLength - m_readBitPtr >= numberOfBits, "tried to read past buffer size");

			var retval = NetBitWriter.ReadUInt32(MData, numberOfBits, MReadPosition);
			MReadPosition += numberOfBits;
			return retval;
		}

		/// <summary>
		/// Reads a 64 bit unsigned integer written using Write(UInt64)
		/// </summary>
		[CLSCompliant(false)]
		public UInt64 ReadUInt64()
		{
			NetException.Assert(MBitLength - MReadPosition >= 64, CReadOverflowError);

			ulong low = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			MReadPosition += 32;
			ulong high = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);

			var retval = low + (high << 32);

			MReadPosition += 32;
			return retval;
		}

		/// <summary>
		/// Reads a 64 bit signed integer written using Write(Int64)
		/// </summary>
		public Int64 ReadInt64()
		{
			NetException.Assert(MBitLength - MReadPosition >= 64, CReadOverflowError);
			unchecked
			{
				var retval = ReadUInt64();
				var longRetval = (long)retval;
				return longRetval;
			}
		}

		/// <summary>
		/// Reads an unsigned integer stored in 1 to 64 bits, written using Write(UInt64, Int32)
		/// </summary>
		[CLSCompliant(false)]
		public UInt64 ReadUInt64(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 64, "ReadUInt64(bits) can only read between 1 and 64 bits");
			NetException.Assert(MBitLength - MReadPosition >= numberOfBits, CReadOverflowError);

			ulong retval;
			if (numberOfBits <= 32)
			{
				retval = NetBitWriter.ReadUInt32(MData, numberOfBits, MReadPosition);
			}
			else
			{
				retval = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
				retval |= NetBitWriter.ReadUInt32(MData, numberOfBits - 32, MReadPosition) << 32;
			}
			MReadPosition += numberOfBits;
			return retval;
		}

		/// <summary>
		/// Reads a signed integer stored in 1 to 64 bits, written using Write(Int64, Int32)
		/// </summary>
		public Int64 ReadInt64(int numberOfBits)
		{
			NetException.Assert(((numberOfBits > 0) && (numberOfBits <= 64)), "ReadInt64(bits) can only read between 1 and 64 bits");
			return (long)ReadUInt64(numberOfBits);
		}

		/// <summary>
		/// Reads a 32 bit floating point value written using Write(Single)
		/// </summary>
		public float ReadFloat()
		{
			return ReadSingle();
		}

		/// <summary>
		/// Reads a 32 bit floating point value written using Write(Single)
		/// </summary>
		public float ReadSingle()
		{
			NetException.Assert(MBitLength - MReadPosition >= 32, CReadOverflowError);

			if ((MReadPosition & 7) == 0) // read directly
			{
				var retval = BitConverter.ToSingle(MData, MReadPosition >> 3);
				MReadPosition += 32;
				return retval;
			}

			var bytes = ReadBytes(4);
			return BitConverter.ToSingle(bytes, 0);
		}

		/// <summary>
		/// Reads a 32 bit floating point value written using Write(Single)
		/// </summary>
		public bool ReadSingle(out float result)
		{
			if (MBitLength - MReadPosition < 32)
			{
				result = 0.0f;
				return false;
			}

			if ((MReadPosition & 7) == 0) // read directly
			{
				result = BitConverter.ToSingle(MData, MReadPosition >> 3);
				MReadPosition += 32;
				return true;
			}

			var bytes = ReadBytes(4);
			result = BitConverter.ToSingle(bytes, 0);
			return true;
		}

		/// <summary>
		/// Reads a 64 bit floating point value written using Write(Double)
		/// </summary>
		public double ReadDouble()
		{
			NetException.Assert(MBitLength - MReadPosition >= 64, CReadOverflowError);

			if ((MReadPosition & 7) == 0) // read directly
			{
				// read directly
				var retval = BitConverter.ToDouble(MData, MReadPosition >> 3);
				MReadPosition += 64;
				return retval;
			}

			var bytes = ReadBytes(8);
			return BitConverter.ToDouble(bytes, 0);
		}

		//
		// Variable bit count
		//

		/// <summary>
		/// Reads a variable sized UInt32 written using WriteVariableUInt32()
		/// </summary>
		[CLSCompliant(false)]
		public uint ReadVariableUInt32()
		{
			var num1 = 0;
			var num2 = 0;
			while (MBitLength - MReadPosition >= 8)
			{
				var num3 = ReadByte();
				num1 |= (num3 & 0x7f) << num2;
				num2 += 7;
				if ((num3 & 0x80) == 0)
					return (uint)num1;
			}

			// ouch; failed to find enough bytes; malformed variable length number?
			return (uint)num1;
		}

		/// <summary>
		/// Reads a variable sized UInt32 written using WriteVariableUInt32() and returns true for success
		/// </summary>
		[CLSCompliant(false)]
		public bool ReadVariableUInt32(out uint result)
		{
			var num1 = 0;
			var num2 = 0;
			while (MBitLength - MReadPosition >= 8)
			{
				byte num3;
				if (ReadByte(out num3) == false)
				{
					result = 0;
					return false;
				}
				num1 |= (num3 & 0x7f) << num2;
				num2 += 7;
				if ((num3 & 0x80) == 0)
				{
					result = (uint)num1;
					return true;
				}
			}
			result = (uint)num1;
			return false;
		}

		/// <summary>
		/// Reads a variable sized Int32 written using WriteVariableInt32()
		/// </summary>
		public int ReadVariableInt32()
		{
			var n = ReadVariableUInt32();
			return (int)(n >> 1) ^ -(int)(n & 1); // decode zigzag
		}

		/// <summary>
		/// Reads a variable sized Int64 written using WriteVariableInt64()
		/// </summary>
		public Int64 ReadVariableInt64()
		{
			var n = ReadVariableUInt64();
			return (Int64)(n >> 1) ^ -(long)(n & 1); // decode zigzag
		}

		/// <summary>
		/// Reads a variable sized UInt32 written using WriteVariableInt64()
		/// </summary>
		[CLSCompliant(false)]
		public UInt64 ReadVariableUInt64()
		{
			UInt64 num1 = 0;
			var num2 = 0;
			while (MBitLength - MReadPosition >= 8)
			{
				//if (num2 == 0x23)
				//	throw new FormatException("Bad 7-bit encoded integer");

				var num3 = ReadByte();
				num1 |= ((UInt64)num3 & 0x7f) << num2;
				num2 += 7;
				if ((num3 & 0x80) == 0)
					return num1;
			}

			// ouch; failed to find enough bytes; malformed variable length number?
			return num1;
		}

		/// <summary>
		/// Reads a 32 bit floating point value written using WriteSignedSingle()
		/// </summary>
		/// <param name="numberOfBits">The number of bits used when writing the value</param>
		/// <returns>A floating point value larger or equal to -1 and smaller or equal to 1</returns>
		public float ReadSignedSingle(int numberOfBits)
		{
			var encodedVal = ReadUInt32(numberOfBits);
			var maxVal = (1 << numberOfBits) - 1;
			return ((encodedVal + 1) / (float)(maxVal + 1) - 0.5f) * 2.0f;
		}

		/// <summary>
		/// Reads a 32 bit floating point value written using WriteUnitSingle()
		/// </summary>
		/// <param name="numberOfBits">The number of bits used when writing the value</param>
		/// <returns>A floating point value larger or equal to 0 and smaller or equal to 1</returns>
		public float ReadUnitSingle(int numberOfBits)
		{
			var encodedVal = ReadUInt32(numberOfBits);
			var maxVal = (1 << numberOfBits) - 1;
			return (encodedVal + 1) / (float)(maxVal + 1);
		}

		/// <summary>
		/// Reads a 32 bit floating point value written using WriteRangedSingle()
		/// </summary>
		/// <param name="min">The minimum value used when writing the value</param>
		/// <param name="max">The maximum value used when writing the value</param>
		/// <param name="numberOfBits">The number of bits used when writing the value</param>
		/// <returns>A floating point value larger or equal to MIN and smaller or equal to MAX</returns>
		public float ReadRangedSingle(float min, float max, int numberOfBits)
		{
			var range = max - min;
			var maxVal = (1 << numberOfBits) - 1;
			float encodedVal = ReadUInt32(numberOfBits);
			var unit = encodedVal / maxVal;
			return min + (unit * range);
		}

		/// <summary>
		/// Reads a 32 bit integer value written using WriteRangedInteger()
		/// </summary>
		/// <param name="min">The minimum value used when writing the value</param>
		/// <param name="max">The maximum value used when writing the value</param>
		/// <returns>A signed integer value larger or equal to MIN and smaller or equal to MAX</returns>
		public int ReadRangedInteger(int min, int max)
		{
			var range = (uint)(max - min);
			var numBits = NetUtility.BitsToHoldUInt(range);

			var rvalue = ReadUInt32(numBits);
			return (int)(min + rvalue);
		}

		/// <summary>
		/// Reads a string written using Write(string)
		/// </summary>
		public string ReadString()
		{
			var byteLen = (int)ReadVariableUInt32();

			if (byteLen <= 0)
				return String.Empty;

			if ((ulong)(MBitLength - MReadPosition) < ((ulong)byteLen * 8))
			{
				// not enough data
#if DEBUG
				
				throw new NetException(CReadOverflowError);
#else
				MReadPosition = MBitLength;
				return null; // unfortunate; but we need to protect against DDOS
#endif
            }

			if ((MReadPosition & 7) == 0)
			{
				// read directly
				var retval = Encoding.UTF8.GetString(MData, MReadPosition >> 3, byteLen);
				MReadPosition += (8 * byteLen);
				return retval;
			}

			var bytes = ReadBytes(byteLen);
			return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
		}

		/// <summary>
		/// Reads a string written using Write(string) and returns true for success
		/// </summary>
		public bool ReadString(out string result)
		{
			uint byteLen;
			if (ReadVariableUInt32(out byteLen) == false)
			{
				result = String.Empty;
				return false;
			}

			if (byteLen <= 0)
			{
				result = String.Empty;
				return true;
			}

			if (MBitLength - MReadPosition < (byteLen * 8))
			{
				result = String.Empty;
				return false;
			}

			if ((MReadPosition & 7) == 0)
			{
				// read directly
				result = Encoding.UTF8.GetString(MData, MReadPosition >> 3, (int)byteLen);
				MReadPosition += (8 * (int)byteLen);
				return true;
			}

			byte[] bytes;
			if (ReadBytes((int)byteLen, out bytes) == false)
			{
				result = String.Empty;
				return false;
			}

			result = Encoding.UTF8.GetString(bytes, 0, bytes.Length);
			return true;
		}

		/// <summary>
		/// Reads a value, in local time comparable to NetTime.Now, written using WriteTime() for the connection supplied
		/// </summary>
		public double ReadTime(NetConnection connection, bool highPrecision)
		{
			var remoteTime = (highPrecision ? ReadDouble() : ReadSingle());

			if (connection == null)
				throw new NetException("Cannot call ReadTime() on message without a connected sender (ie. unconnected messages)");

			// lets bypass NetConnection.GetLocalTime for speed
			return remoteTime - connection.MRemoteTimeOffset;
		}

		/// <summary>
		/// Reads a stored IPv4 endpoint description
		/// </summary>
		public IPEndPoint ReadIpEndPoint()
		{
			var len = ReadByte();
			var addressBytes = ReadBytes(len);
			int port = ReadUInt16();

			var address = new IPAddress(addressBytes);
			return new IPEndPoint(address, port);
		}

		/// <summary>
		/// Pads data with enough bits to reach a full byte. Decreases cpu usage for subsequent byte writes.
		/// </summary>
		public void SkipPadBits()
		{
			MReadPosition = ((MReadPosition + 7) >> 3) * 8;
		}

		/// <summary>
		/// Pads data with enough bits to reach a full byte. Decreases cpu usage for subsequent byte writes.
		/// </summary>
		public void ReadPadBits()
		{
			MReadPosition = ((MReadPosition + 7) >> 3) * 8;
		}

		/// <summary>
		/// Pads data with the specified number of bits.
		/// </summary>
		public void SkipPadBits(int numberOfBits)
		{
			MReadPosition += numberOfBits;
		}
	}
}
