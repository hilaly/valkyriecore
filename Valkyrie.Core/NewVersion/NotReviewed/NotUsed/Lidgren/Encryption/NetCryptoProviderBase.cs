﻿using System.IO;
using System.Security.Cryptography;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public abstract class NetCryptoProviderBase : NetEncryption
	{
		protected SymmetricAlgorithm MAlgorithm;

	    protected NetCryptoProviderBase(NetPeer peer, SymmetricAlgorithm algo)
			: base(peer)
		{
			MAlgorithm = algo;
			MAlgorithm.GenerateKey();
			MAlgorithm.GenerateIV();
		}

		public override void SetKey(byte[] data, int offset, int count)
		{
			var len = MAlgorithm.Key.Length;
			var key = new byte[len];
			for (var i = 0; i < len; i++)
				key[i] = data[offset + (i % count)];
			MAlgorithm.Key = key;

			len = MAlgorithm.IV.Length;
			key = new byte[len];
			for (var i = 0; i < len; i++)
				key[len - 1 - i] = data[offset + (i % count)];
			MAlgorithm.IV = key;
		}

		public override bool Encrypt(NetOutgoingMessage msg)
		{
			var unEncLenBits = msg.LengthBits;

			var ms = new MemoryStream();
			var cs = new CryptoStream(ms, MAlgorithm.CreateEncryptor(), CryptoStreamMode.Write);
			cs.Write(msg.MData, 0, msg.LengthBytes);
			cs.Close();

			// get results
			var arr = ms.ToArray();
			ms.Close();

			msg.EnsureBufferSize((arr.Length + 4) * 8);
			msg.LengthBits = 0; // reset write pointer
			msg.Write((uint)unEncLenBits);
			msg.Write(arr);
			msg.LengthBits = (arr.Length + 4) * 8;

			return true;
		}

		public override bool Decrypt(NetIncomingMessage msg)
		{
			var unEncLenBits = (int)msg.ReadUInt32();

			var ms = new MemoryStream(msg.MData, 4, msg.LengthBytes - 4);
			var cs = new CryptoStream(ms, MAlgorithm.CreateDecryptor(), CryptoStreamMode.Read);

			var result = MPeer.GetStorage(unEncLenBits);
			cs.Read(result, 0, NetUtility.BytesToHoldBits(unEncLenBits));
			cs.Close();

			// TODO: recycle existing msg

			msg.MData = result;
			msg.MBitLength = unEncLenBits;
			msg.MReadPosition = 0;

			return true;
		}
	}
}
