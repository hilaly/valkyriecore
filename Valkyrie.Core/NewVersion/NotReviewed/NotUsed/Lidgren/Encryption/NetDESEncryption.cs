using System.Security.Cryptography;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public class NetDesEncryption : NetCryptoProviderBase
	{
		public NetDesEncryption(NetPeer peer)
			: base(peer, new DESCryptoServiceProvider())
		{
		}

		public NetDesEncryption(NetPeer peer, string key)
			: base(peer, new DESCryptoServiceProvider())
		{
			SetKey(key);
		}

		public NetDesEncryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new DESCryptoServiceProvider())
		{
			SetKey(data, offset, count);
		}
	}
}
