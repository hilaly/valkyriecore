using System.Security.Cryptography;

// ReSharper disable once CheckNamespace
namespace Lidgren.Network
{
	public class NetTripleDesEncryption : NetCryptoProviderBase
	{
		public NetTripleDesEncryption(NetPeer peer)
			: base(peer, new TripleDESCryptoServiceProvider())
		{
		}

		public NetTripleDesEncryption(NetPeer peer, string key)
			: base(peer, new TripleDESCryptoServiceProvider())
		{
			SetKey(key);
		}

		public NetTripleDesEncryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new TripleDESCryptoServiceProvider())
		{
			SetKey(data, offset, count);
		}
	}
}
