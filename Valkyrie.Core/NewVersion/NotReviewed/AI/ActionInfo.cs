﻿using System;
using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI
{
    public class ActionInfo : IActionInfo
    {
        readonly Func<IEnumerable<object>, Action<bool>, bool> _func;

        void Finish(bool obj)
        {
            if (Finished != null)
            {
                Finished(obj);
                Finished = null;
            }
        }

        public ActionInfo(Func<IEnumerable<object>, Action<bool>, bool> func)
        {
            _func = func;
        }

        public event Action<bool> Finished;

        public bool Execute(IEnumerable<object> args)
        {
            return _func(args, Finish);
        }
    }
}