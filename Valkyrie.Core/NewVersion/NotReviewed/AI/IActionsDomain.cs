﻿using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI
{
    public interface IActionsDomain
    {
        IDictionary<string, HashSet<IActionInfo>> GetActions();
    }
}
