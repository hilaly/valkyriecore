using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI
{
    public static class ActionsDomainExtensions
    {
        public static IEnumerable<IActionInfo> GetAction(this IActionsDomain domain, string actionName)
        {
            var allActions = domain.GetActions();
            HashSet<IActionInfo> targetActions;
            if (allActions.TryGetValue(actionName, out targetActions))
                return targetActions;
            return null;
        }
    }
}