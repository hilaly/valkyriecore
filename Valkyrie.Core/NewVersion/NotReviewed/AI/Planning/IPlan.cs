﻿using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI.Planning
{
    public interface IPlan
    {
        void Add(IEnumerable<string> task);
    }
}