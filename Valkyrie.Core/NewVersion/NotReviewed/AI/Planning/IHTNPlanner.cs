namespace Valkyrie.NewVersion.NotReviewed.AI.Planning
{
    public interface IHTNPlanner : IPlanner
    {
        void SetTaskResolvers(IDecisionDomain domain);
    }
}