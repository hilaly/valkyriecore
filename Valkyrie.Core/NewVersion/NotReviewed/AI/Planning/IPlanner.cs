﻿using System.Collections.Generic;
using Valkyrie.NewVersion.NotReviewed.AI.Knowledge;

namespace Valkyrie.NewVersion.NotReviewed.AI.Planning
{
    public interface IPlanner
    {
        void SetAvailableActions(IActionsDomain domain);

        IPlan BuildPlan(IState state, List<List<string>> tasks, int verbose);
    }
}
