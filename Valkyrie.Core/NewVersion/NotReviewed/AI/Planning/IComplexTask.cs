﻿using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI.Planning
{
    public interface IComplexTask
    {
        List<List<string>> ApplyForward(object[] parameters);
    }
}
