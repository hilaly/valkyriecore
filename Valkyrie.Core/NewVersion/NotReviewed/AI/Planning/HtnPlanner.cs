﻿using System.Collections.Generic;
using System.Linq;
using Valkyrie.NewVersion.NotReviewed.AI.Knowledge;
using Valkyrie.Tools.Logs;

namespace Valkyrie.NewVersion.NotReviewed.AI.Planning
{
    class HtnPlanner : IHTNPlanner
    {
        int _searchDepth = 30;

        readonly ILogInstance _log;
        IActionsDomain _operatorsDomain;
        IDecisionDomain _methodsDomain;
        IDictionary<string, List<IPlanningActionInfo>> _operators;
        IDictionary<string, List<IComplexTask>> _methods;

        public int SearchDepth { set { _searchDepth = value; } }

        public HtnPlanner(ILogService logSystem)
        {
            _log = logSystem.GetLog("Valkyrie.AI.Planner");
        }

        public void SetAvailableActions(IActionsDomain domain)
        {
            _operatorsDomain = domain;
            _operators = new Dictionary<string, List<IPlanningActionInfo>>();
            foreach(var pair in _operatorsDomain.GetActions())
            {
                var collection = pair.Value.OfType<IPlanningActionInfo>().ToList();
                if (collection.Count > 0)
                    _operators.Add(pair.Key, collection);
            }
        }

        public void SetTaskResolvers(IDecisionDomain domain)
        {
            _methodsDomain = domain;
            _methods = new Dictionary<string, List<IComplexTask>>();
            foreach(var pair in _methodsDomain.GetTasks())
            {
                var collection = pair.Value.OfType<IComplexTask>().ToList();
                if (collection.Count > 0)
                    _methods.Add(pair.Key, collection);
            }
        }

        public IPlan BuildPlan(IState initialState, List<List<string>> tasks, int verbose)
        {
            if (verbose > 0)
            {
                _log.Info("CUHP, verbose = " + verbose);
                _log.Info("State = " + initialState.Name);
                _log.Info("Tasks = " + tasks);
            }
            var result = SeekPlan(initialState, new List<List<string>>(tasks), new HtnPlan(), 0, verbose);
            if (verbose > 0)
                _log.Info("Result = " + result);

            return result;
        }

        object[] CreateArgs(List<string> task)
        {
            var result = new object[task.Count];
            for (int i = 1; i < task.Count; ++i)
                result[i] = task[i];
            return result;
        }

        IPlan SeekPlan(IState state, List<List<string>> tasks, IPlan plan, int depth, int verbose)
        {
            if (_searchDepth > 0)
            {
                if (depth >= _searchDepth)
                {
                    return null;
                }
            }

            if (verbose > 1)
                _log.Info("Depth: " + depth + ", tasks: " + tasks);

            if (tasks.Count == 0)
            {
                if (verbose > 2)
                    _log.Info("Depth " + depth + " returns plan: " + plan);
                return plan;
            }

            List<string> task = tasks[0];
            var parameters = CreateArgs(task);

            List<IPlanningActionInfo> operators;
            if (_operators.TryGetValue(task[0], out operators))
            {
                if (verbose > 2)
                    _log.Info("Depth: " + depth + ", action: " + task);

                for (int index = 0; index < operators.Count; ++index)
                {
                    parameters[0] = new State(state);

                    var info = operators[index];
                    var newState = info.ApplyForward(parameters);

                    if (verbose > 2)
                        _log.Info("Depth: " + depth + ", new state: " + newState);

                    if (newState == null)
                        continue;

                    plan.Add(task);
                    var solution = SeekPlan(newState, tasks.GetRange(1, tasks.Count - 1), plan, depth + 1, verbose);
                    if (solution != null)
                        return solution;
                }
            }

            List<IComplexTask> methods;
            if (_methods.TryGetValue(task[0], out methods))
            {
                if (verbose > 2)
                    _log.Info("Depth: " + depth + ", method instance: " + task);

                for (int index = 0; index < methods.Count; ++index)
                {
                    var info = methods[index];
                    parameters[0] = new State(state);

                    List<List<string>> subtasks = info.ApplyForward(parameters);

                    if (verbose > 2)
                        _log.Info("Depth: " + depth + ", new tasks: " + subtasks);

                    if (subtasks == null)
                        continue;

                    List<List<string>> newTasks = new List<List<string>>(subtasks);
                    newTasks.AddRange(tasks.GetRange(1, tasks.Count - 1));

                    var solution = SeekPlan(state, newTasks, plan, depth + 1, verbose);
                    if (solution != null)
                        return solution;
                }
            }

            if (verbose > 2)
                _log.Info("Depth " + depth + " returns failure!");

            return null;
        }
    }
}
