﻿using Valkyrie.NewVersion.NotReviewed.AI.Knowledge;

namespace Valkyrie.NewVersion.NotReviewed.AI.Planning
{
    public interface IPlanningActionInfo : IActionInfo
    {
        IState ApplyForward(object[] parameters);
    }
}
