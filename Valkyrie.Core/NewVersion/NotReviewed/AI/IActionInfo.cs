﻿using System;
using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI
{
    public interface IActionInfo
    {
        event Action<bool> Finished;

        bool Execute(IEnumerable<object> args);
    }
}
