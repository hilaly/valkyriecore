﻿using System.Collections.Generic;
using Valkyrie.NewVersion.NotReviewed.AI.Planning;

namespace Valkyrie.NewVersion.NotReviewed.AI
{
    public interface IDecisionDomain
    {
        IDictionary<string, HashSet<IComplexTask>> GetTasks();
    }
}
