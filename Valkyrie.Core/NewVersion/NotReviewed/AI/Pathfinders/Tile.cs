using System.Numerics;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{

    public class Tile
    {
        public Tile(Vector3 position)
        {
            Position = position;
        }

        public Vector3 Position { get; private set; }
    }
    
}
