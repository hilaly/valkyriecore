using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Valkyrie.Math;
using Valkyrie.Math.Algorithms;
using Valkyrie.Math.Graphs;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{
    class NavigationGraph : GenericGraph<Tile, TileDistance>, INavigationMap
    {
	    readonly IRandom _random;

		public AStarPathfinder<Tile, TileDistance> PathPlanner { get; private set; }

		static float DistanceEuristic(Tile a0, Tile a1)
		{
			return (a0.Position - a1.Position).Length();
		}

		T Random<T>(IRandom rand, IEnumerable<T> collection)
		{
			var index = rand.Range(0, collection.Count());
			return collection.ElementAt(index);
		}

        public NavigationGraph(IIdProvider<long> idProvider, IRandom random) : base(idProvider)
        {
			_random = random;
			PathPlanner = new AStarPathfinder<Tile, TileDistance>(this, DistanceEuristic);
        }

		public Tile GetRandom(IRandom rand)
		{
			return Random(rand, Nodes);
		}

		public Tile GetTile(Vector3 position)
		{
		    Tile nearesTile = null;
		    var minDistance = float.MaxValue;
			foreach(var node in Nodes)
			{
				var p = node.Position;
                var distance = p - position;
                if (distance.LengthSquared() > minDistance)
                    continue;

			    minDistance = distance.LengthSquared();
			    nearesTile = node;

                if (System.Math.Abs(distance.X) <= 0.5f && System.Math.Abs(distance.Z) <= 0.5f)
					return node;
			}
			return nearesTile;
		}

		#region INavigationMap

		public Vector3 GetRandomPoint()
		{
			var tile = GetRandom(_random);
			return tile.Position + new Vector3(_random.Range(-.5f, .5f), 0, _random.Range(-.5f, .5f));
		}

		public INavigationPath BuildPath(Vector3 startPoint, Vector3 endPoint)
		{
			var path = PathPlanner.Compute(GetTile(startPoint), GetTile(endPoint));
			if(path.Nodes == null)
				return new PointPath(new []{startPoint}) { State = NavigationPathState.Empty };
			var result = new PointPath(new [] {startPoint}) { State = NavigationPathState.Builded };
            for (var i = 1; i < path.Nodes.Count - 1; ++i)
                result.AddPoint(path.Nodes[i].Position);
		    result.AddPoint(endPoint);
		    return result;
		}

		#endregion

		public void DrawDebug()
		{
            //var displ = Vector3.Up*0.1f;
            //foreach(var edge in Edges)
            //    Debug.DrawLine(edge.Head.Position + displ, edge.Tail.Position + displ, Color.grey);

            //foreach (var node in Nodes)
            //    DebugDrawer.DrawSphere(node.Position + displ, 0.3f, Color.black);
		}
    }
}
