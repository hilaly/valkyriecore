﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Math.Graphs;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{
	class OldAStarPathfinder<TNode, TEdge>
		where TNode : class
    {
		private readonly IGraph<TNode, TEdge> _graph;
		private readonly Func<TNode, TNode, float> _heuristic;

		public OldAStarPathfinder(IGraph<TNode, TEdge> graph, Func<TNode, TNode, float> heuristic)
        {
            _graph = graph;
            _heuristic = heuristic;
        }

		public Path<TNode> Compute(TNode startNode, TNode endNode)
        {
            //TODO: replace with profiler Profiler.BeginSample("AStarPathfinder.Compute");
			var result = new Path<TNode>();
            StartCompute(startNode, endNode, result);
            //Profiler.EndSample();
            return result;
        }

        struct AStarPathNode
        {
			public readonly TNode Node;
			public readonly TNode Parent;
            public readonly float G;
            public readonly float F;

			public AStarPathNode(TNode node, TNode parent, float g, float h)
            {
			    Node = node;
                Parent = parent;
                G = g;
			    F = G + h;
            }
        }

		private void StartCompute(TNode startNode, TNode endNode, Path<TNode> path)
        {
            var closeNodes = new List<AStarPathNode>();
            var openNodes = new List<AStarPathNode> { new AStarPathNode(startNode, null, 0, 0) };

            while (openNodes.Count > 0)
            {
                var current = openNodes[openNodes.Count - 1];
                openNodes.RemoveAt(openNodes.Count - 1);
                closeNodes.Add(current);

                var connections = _graph.GetEdges(current.Node);
                foreach (var edge in connections)
                {
                    var node = edge.Tail;
                    if (node == endNode)
                    {
                        Consruct(edge, closeNodes, path);
                        return;
                    }
                    if (closeNodes.Any(u => u.Node == node))
                        continue;
                    if (openNodes.Any(u => u.Node == node))
                        continue;
                    var pathNode = new AStarPathNode(node, edge.Head, current.G + edge.Length,
                        _heuristic(current.Node, endNode));
                    bool added = false;
                    for (var i = openNodes.Count - 1; i >= 0; --i)
                    {
                        var test = openNodes[i];
                        if (test.F < pathNode.F)
                            continue;
                        openNodes.Insert(i + 1, pathNode);
                        added = true;
                        break;
                    }
                    if(!added)
                        openNodes.Insert(0, pathNode);
                }
            }
        }

		private void Consruct(IEdge<TNode, TEdge> endNode, List<AStarPathNode> closeNodes, Path<TNode> path)
        {
			var pathNodes = new List<TNode> { endNode.Tail };
            var current = closeNodes.FirstOrDefault(u => u.Node == endNode.Head);
            pathNodes.Insert(0, current.Node);
            while (current.Parent != null)
            {
                current = closeNodes.FirstOrDefault(u => u.Node == current.Parent);
                pathNodes.Insert(0, current.Node);
            }
            path.Nodes = pathNodes;
        }
    }
}
