using System.Numerics;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{
    public interface INavigationMap
    {
        Vector3 GetRandomPoint();

        INavigationPath BuildPath(Vector3 startPoint, Vector3 endPoint);
    }
}