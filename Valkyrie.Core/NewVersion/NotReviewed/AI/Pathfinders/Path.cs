using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{
    public class Path<TNode>
    {
        public List<TNode> Nodes { get; set; }
    }
}