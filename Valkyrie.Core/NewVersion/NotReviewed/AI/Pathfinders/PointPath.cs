using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{
    public class PointPath : INavigationPath
    {
        readonly List<Vector3> _points;

        public PointPath(IEnumerable<Vector3> points)
        {
            _points = points.ToList();
            State = NavigationPathState.Empty;
        }
		

        public Vector3 NextPoint { get { return _points[0]; } }
        public void ReachPoint()
        {
            _points.RemoveAt(0);
        }

        public Vector3 GetPoint(int index)
        {
            return _points[index];
        }

        public NavigationPathState State { get; set; }

        public int Count { get { return _points.Count; } }

        public void AddPoint(Vector3 point)
        {
            _points.Add(point);
        }
    }
}