using System.Numerics;

namespace Valkyrie.NewVersion.NotReviewed.AI.Pathfinders
{
    public interface INavigationPath
    {
        NavigationPathState State { get; }
        int Count { get; }
        Vector3 GetPoint(int index);
        Vector3 NextPoint { get; }
        void ReachPoint();
    }
}