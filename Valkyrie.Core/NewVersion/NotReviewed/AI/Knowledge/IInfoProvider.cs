﻿namespace Valkyrie.NewVersion.NotReviewed.AI.Knowledge
{
    public interface IInfoProvider
    {
        object Get(string id);
        void Set(string id, object value);
    }
}
