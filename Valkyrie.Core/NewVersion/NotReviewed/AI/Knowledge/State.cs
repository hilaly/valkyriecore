﻿using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI.Knowledge
{
    public class State : IState
    {
        readonly Dictionary<string, List<string>> stateVariables = new Dictionary<string, List<string>>();
        readonly Dictionary<string, IDictionary<string, List<string>>> stateRelations = new Dictionary<string, IDictionary<string, List<string>>>();

        public string Name { get; private set; }

        public State(string name)
        {
            Name = name;
        }

        public State(IState state)
        {
            Name = state.Name;
            
            foreach (var pair in state.GetAllVariables())
            {
                stateVariables.Add(pair.Key, new List<string>(pair.Value));
            }

            foreach (var pair in state.GetAllRelations())
            {
                stateRelations.Add(pair.Key, new Dictionary<string, List<string>>());
                foreach (KeyValuePair<string, List<string>> otherPair in pair.Value)
                {
                    stateRelations[pair.Key].Add(otherPair.Key, new List<string>(otherPair.Value));
                }
            }
        }
        
        public void Add(string variable, string innerState)
        {
            if (stateVariables.ContainsKey(variable))
            {
                stateVariables[variable].Add(innerState);
            }
            else
            {
                stateVariables.Add(variable, new List<string>(new[] { innerState }));
            }
        }

        public void Add(string relation, string elementOne, string elementTwo)
        {
            if (stateRelations.ContainsKey(relation))
            {
                if (stateRelations[relation].ContainsKey(elementOne))
                {
                    stateRelations[relation][elementOne].Add(elementTwo);
                }
                else
                {
                    stateRelations[relation].Add(elementOne, new List<string>(new[] { elementTwo }));
                }
            }
            else
            {
                stateRelations.Add(relation, new Dictionary<string, List<string>>());
                stateRelations[relation].Add(elementOne, new List<string>(new[] { elementTwo }));
            }
        }

        public bool Check(string variable, string innerState)
        {
            if (stateVariables.ContainsKey(variable) && stateVariables[variable].Contains(innerState))
                return true;
            return false;
        }

        public bool CheckRelation(string relation, string elementOne, string elementTwo)
        {
            if (stateRelations.ContainsKey(relation) && stateRelations[relation].ContainsKey(elementOne)
                && stateRelations[relation][elementOne].Contains(elementTwo))
                return true;
            return false;
        }

        public bool Contains(string variable)
        {
            return stateVariables.ContainsKey(variable);
        }

        public bool ContainsRelation(string relation)
        {
            return stateRelations.ContainsKey(relation);
        }

        public bool ContainsRelation(string relation, string elementOne)
        {
            return stateRelations.ContainsKey(relation) && stateRelations[relation].ContainsKey(elementOne);
        }

        public bool ContainsRelationMatch(string relation, string elementTwo)
        {
            if (!stateRelations.ContainsKey(relation))
                return false;

            foreach (KeyValuePair<string, List<string>> pair in stateRelations[relation])
            {
                if (pair.Value.Contains(elementTwo))
                    return true;
            }

            return false;
        }

        public IDictionary<string, IDictionary<string, List<string>>> GetAllRelations()
        {
            return stateRelations;
        }

        public IDictionary<string, List<string>> GetAllVariables()
        {
            return stateVariables;
        }

        public IDictionary<string, List<string>> GetStateOfRelation(string relation)
        {
            return stateRelations[relation];
        }

        public List<string> GetStateOfRelation(string relation, string elementOne)
        {
            return stateRelations[relation][elementOne];
        }

        public List<string> GetVariableStates(string variable)
        {
            if (Contains(variable))
                return stateVariables[variable];
            return null;
        }

        public void Remove(string variable, string innerState)
        {
            if (stateVariables.ContainsKey(variable))
            {
                stateVariables[variable].Remove(innerState);
                if (stateVariables[variable].Count == 0)
                {
                    stateVariables.Remove(variable);
                }
            }
        }

        public void Remove(string relation, string elementOne, string elementTwo)
        {
            if (stateRelations.ContainsKey(relation) && stateRelations[relation].ContainsKey(elementOne))
            {
                stateRelations[relation][elementOne].Remove(elementTwo);
                if (stateRelations[relation][elementOne].Count == 0)
                {
                    stateRelations[relation].Remove(elementOne);
                    if (stateRelations[relation].Count == 0)
                    {
                        stateRelations.Remove(relation);
                    }
                }
            }
        }

        public List<string> UnifyRelation(string relation, string elementTwo)
        {
            List<string> unificationList = new List<string>();

            if (!stateRelations.ContainsKey(relation))
                return unificationList;

            foreach (KeyValuePair<string, List<string>> pair in stateRelations[relation])
            {
                if (pair.Value.Contains(elementTwo))
                    unificationList.Add(pair.Key);
            }

            return unificationList;
        }
    }
}
