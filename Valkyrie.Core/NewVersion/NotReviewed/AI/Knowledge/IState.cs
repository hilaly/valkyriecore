﻿using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI.Knowledge
{
    public interface IState
    {
        string Name { get; }

        #region Variables

        bool Contains(string variable);
        void Add(string variable, string innerState);
        bool Check(string variable, string innerState);
        void Remove(string variable, string innerState);

        List<string> GetVariableStates(string variable);
        IDictionary<string, List<string>> GetAllVariables();

        #endregion

        #region Relations

        void Add(string relation, string elementOne, string elementTwo);
        bool CheckRelation(string relation, string elementOne, string elementTwo);
        void Remove(string relation, string elementOne, string elementTwo);
        bool ContainsRelation(string relation);
        bool ContainsRelation(string relation, string elementOne);
        bool ContainsRelationMatch(string relation, string elementTwo);

        IDictionary<string, List<string>> GetStateOfRelation(string relation);
        List<string> GetStateOfRelation(string relation, string elementOne);
        List<string> UnifyRelation(string relation, string elementTwo);

        IDictionary<string, IDictionary<string, List<string>>> GetAllRelations();

        #endregion
    }
}
