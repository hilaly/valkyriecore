﻿namespace Valkyrie.NewVersion.NotReviewed.AI.Knowledge
{
    public interface IKnowledgeProvider
    {
        IState GetCurrentState();
    }
}
