using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation
{
    class MinimumErrorNeuralNetworkTeacher : INeuralNetworkTeacher
    {
        private readonly float minimumError;
        private readonly int _epochCount;
        private readonly INeuralNetwork _network;
        private readonly IExamplesProvider _examplesProvider;

        public MinimumErrorNeuralNetworkTeacher(INeuralNetwork network, IExamplesProvider examplesProvider, int epochCount, float minimumError)
        {
            _network = network;
            _examplesProvider = examplesProvider;
            _epochCount = epochCount;
            this.minimumError = minimumError;
        }

        public void Teach()
        {
            var error = 1.0;

            while (true)
            {
                var numEpochs = 0;

                while (error > minimumError && numEpochs < int.MaxValue)
                {
                    var errors = new List<float>();

                    for (var num = 0; num < _epochCount; ++num)
                    {
                        var set = _examplesProvider.GetExample().ToArray();
                        var target = _examplesProvider.GetResult(set).ToArray();

                        var result = _network.ForwardPropagate(set);
                        _network.BackPropagate(target);

                        errors.Add(CalculateError(target, result));
                    }
                    error = errors.Max(u => System.Math.Abs(u));
                    if(numEpochs % 1000 == 0)
                        Console.WriteLine("Epoch: {0}, error {1}", numEpochs, error);
                    numEpochs++;
                }

                var exam = new List<float>();

                for (var num = 0; num < _epochCount; ++num)
                {
                    var set = _examplesProvider.GetExample().ToArray();
                    var target = _examplesProvider.GetResult(set).ToArray();

                    var result = _network.ForwardPropagate(set);
                    _network.BackPropagate(target);

                    exam.Add(CalculateError(target, result));
                }

                error = System.Math.Abs(exam.Average());
                if(error < minimumError)
                    break;
            }
        }

        private float CalculateError(float[] target, float[] result)
        {
            return result.Select((t, i) => t - target[i]).Sum();
        }
    }
}