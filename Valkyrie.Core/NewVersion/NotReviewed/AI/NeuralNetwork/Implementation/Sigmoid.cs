namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation
{
    public static class Sigmoid
    {
        public static float Output(float x)
        {
            return (float) (1.0/(1.0 + System.Math.Exp(-x)));
            return x < -45.0f
                ? 0.0f
                : x > 45.0f
                    ? 1.0f
                    : 1.0f/(1.0f + (float) System.Math.Exp(-x));
        }

        public static float Derivative(float x)
        {
            var output = Output(x);
            return output*(1 - output);
        }
    }
}