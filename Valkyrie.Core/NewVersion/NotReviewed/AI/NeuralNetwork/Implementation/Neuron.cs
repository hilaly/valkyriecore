using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation
{
    public class Neuron
    {
        public Guid Id { get; set; }
        public List<Synapse> InputSynapses { get; set; }
        public List<Synapse> OutputSynapses { get; set; }
        public float Bias { get; set; }
        public float BiasDelta { get; set; }
        public float Gradient { get; set; }
        public float Value { get; set; }
        
        public Neuron(float bias)
        {
            Id = Guid.NewGuid();
            InputSynapses = new List<Synapse>();
            OutputSynapses = new List<Synapse>();
            Bias = bias;
        }

        public Neuron(IEnumerable<Neuron> inputNeurons, float bias) : this(bias)
        {
            foreach (var inputNeuron in inputNeurons)
            {
                var synapse = new Synapse(inputNeuron, this, 1f);
                inputNeuron.OutputSynapses.Add(synapse);
                InputSynapses.Add(synapse);
            }
        }


        #region -- Values & Weights --

        public virtual float CalculateValue()
        {
            return Value = Sigmoid.Output(InputSynapses.Sum(a => a.Weight * a.InputNeuron.Value) + Bias);
        }

        public float CalculateError(float target)
        {
            return target - Value;
        }

        public float CalculateGradient(float? target = null)
        {
            Gradient =  target == null
                ? OutputSynapses.Sum(a => a.OutputNeuron.Gradient*a.Weight)*Sigmoid.Derivative(Value)
                : CalculateError(target.Value)*Sigmoid.Derivative(Value);
            return Gradient;
        }

        public void UpdateWeights(float learnRate, float momentum)
        {
            var prevDelta = BiasDelta;
            BiasDelta = learnRate * Gradient;
            Bias += BiasDelta + momentum * prevDelta;

            foreach (var synapse in InputSynapses)
            {
                prevDelta = synapse.WeightDelta;
                synapse.WeightDelta = learnRate * Gradient * synapse.InputNeuron.Value;
                synapse.Weight += synapse.WeightDelta + momentum * prevDelta;
            }
        }

        #endregion

    }
}