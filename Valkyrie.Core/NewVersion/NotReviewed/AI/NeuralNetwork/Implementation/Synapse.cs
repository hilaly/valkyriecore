using System;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation
{
    public class Synapse
    {
        public Guid Id { get; set; }
        public Neuron InputNeuron { get; set; }
        public Neuron OutputNeuron { get; set; }
        public float Weight { get; set; }
        public float WeightDelta { get; set; }

        public Synapse() { }

        public Synapse(Neuron inputNeuron, Neuron outputNeuron, float weight)
        {
            Id = Guid.NewGuid();
            InputNeuron = inputNeuron;
            OutputNeuron = outputNeuron;
            Weight = weight;
        }
    }
}