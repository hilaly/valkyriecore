﻿using System.Linq;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation
{
    class EpochNeuralNetworkTeacher : INeuralNetworkTeacher
    {
        private readonly int _numEpoch;
        private readonly int _epochCount;
        private readonly INeuralNetwork _network;
        private readonly IExamplesProvider _examplesProvider;

        public EpochNeuralNetworkTeacher(INeuralNetwork network, IExamplesProvider examplesProvider, int epochCount, int numEpoch)
        {
            _network = network;
            _examplesProvider = examplesProvider;
            _epochCount = epochCount;
            _numEpoch = numEpoch;
        }

        public void Teach()
        {
            for (var i = 0; i < _numEpoch; ++i)
            {
                for(var num = 0; num < _epochCount; ++num)
                {
                    var set = _examplesProvider.GetExample().ToArray();
                    var target = _examplesProvider.GetResult(set).ToArray();

                    _network.ForwardPropagate(set);
                    _network.BackPropagate(target);
                }
            }
        }
    }
}