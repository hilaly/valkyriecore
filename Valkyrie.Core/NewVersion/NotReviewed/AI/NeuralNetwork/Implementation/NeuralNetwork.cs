using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation
{
    class NeuralNetwork : INeuralNetwork
    {
        readonly List<Neuron[]> _layers;
        readonly float _momentum;
        private readonly float _learnRate;

        public NeuralNetwork(List<Neuron[]> layers, float learnRate, float momentum)
        {
            _layers = layers;
            _learnRate = learnRate;
            _momentum = momentum;
        }

        public float[] ForwardPropagate(float[] set)
        {
            var inputLayer = _layers[0];
            for (var index = 0; index < inputLayer.Length; ++index)
                inputLayer[index].Value = set[index];

            for (var layerIndex = 1; layerIndex < _layers.Count; ++layerIndex)
                foreach (var neuron in _layers[layerIndex])
                    neuron.CalculateValue();

            var outputLayer = _layers[_layers.Count - 1];
            return outputLayer.Select(u => u.Value).ToArray();
        }

        public void BackPropagate(float[] target)
        {
            var outputLayer = _layers[_layers.Count - 1];
            for (var index = 0; index < outputLayer.Length; ++index)
                outputLayer[index].CalculateGradient(target[index]);

            for (var layerIndex = _layers.Count - 2; layerIndex > 0; --layerIndex)
                foreach (var neuron in _layers[layerIndex])
                    neuron.CalculateGradient();

            for(var layerIndex = 1; layerIndex < _layers.Count; ++layerIndex)
                foreach (var neuron in _layers[layerIndex])
                    neuron.UpdateWeights(_learnRate, _momentum);
        }
    }
}