﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork
{
    public class NeuralNetworkFactory
    {
        private readonly Random _random;

        private int _inputLayer = 1;
        private readonly List<int> _hiddenLayers = new List<int>();
        private int _outputLayer = 1;
        private float _learnRate = 0.4f;
        private float _momentum = 0.9f;

        private IEnumerable<int> GetAllLayersCount()
        {
            if(_inputLayer > 0)
                yield return _inputLayer;
            foreach (var layer in _hiddenLayers)
                yield return layer;
            if(_outputLayer > 0)
                yield return _outputLayer;
        }

        NeuralNetworkFactory(int seed)
        {
            _random = new Random(seed);
        }

        public static NeuralNetworkFactory Start(int seed = 1234)
        {
            return new NeuralNetworkFactory(seed);
        }

        public NeuralNetworkFactory SetInputsCount(int neuronsCount)
        {
            _inputLayer = neuronsCount;
            return this;
        }

        public NeuralNetworkFactory SetOutputsCount(int neuronsCount)
        {
            _outputLayer = neuronsCount;
            return this;
        }

        public NeuralNetworkFactory SetLearnRate(float rate)
        {
            _learnRate = rate;
            return this;
        }

        public NeuralNetworkFactory SetMomentum(float momentum)
        {
            _momentum = momentum;
            return this;
        }

        public NeuralNetworkFactory SetHiddenLayer(int neuronsCount)
        {
            _hiddenLayers.Add(neuronsCount);
            return this;
        }

        public INeuralNetwork Build()
        {
            var counts = GetAllLayersCount().ToList();
            List<Neuron[]> _allLayers = new List<Neuron[]>(counts.Count);
            foreach (var layerCount in counts)
            {
                var layer = new Neuron[layerCount];
                for (var index = 0; index < layerCount; ++index)
                    layer[index] = new Neuron(GetRandom());
                _allLayers.Add(layer);
            }
            for (var layerIndex = 1; layerIndex < _allLayers.Count; ++layerIndex)
            {
                var previousLayer = _allLayers[layerIndex - 1];
                var currentLayer = _allLayers[layerIndex];
                foreach (var outNeuron in currentLayer)
                {
                    foreach (var inNeuron in previousLayer)
                    {
                        var synapse = new Synapse(inNeuron, outNeuron, GetRandom());
                        inNeuron.OutputSynapses.Add(synapse);
                        outNeuron.InputSynapses.Add(synapse);
                    }
                }
            }
            return new Implementation.NeuralNetwork(_allLayers, _learnRate, _momentum);
        }

        float GetRandom()
        {
            return (float) (2.0*_random.NextDouble()) - 1f;
        }
    }
}