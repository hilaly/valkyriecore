using System.Collections.Generic;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork
{
    public interface IExamplesProvider
    {
        IEnumerable<float> GetExample();
        IEnumerable<float> GetResult(IEnumerable<float> example);
    }
}