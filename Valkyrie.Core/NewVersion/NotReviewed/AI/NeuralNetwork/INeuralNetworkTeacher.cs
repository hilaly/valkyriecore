﻿namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork
{
    public interface INeuralNetworkTeacher
    {
        void Teach();
    }
}