using Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork.Implementation;

namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork
{
    public static class NeuralNetworkExt
    {
        private static readonly int CountInEpoch = 1000;

        public static INeuralNetworkTeacher CreateTeacher(this INeuralNetwork network, IExamplesProvider examples, int trainingEpochCount)
        {
            return new EpochNeuralNetworkTeacher(network, examples, CountInEpoch, trainingEpochCount);
        }

        public static INeuralNetworkTeacher CreateTeacher(this INeuralNetwork network, IExamplesProvider examples,
            float minimumError)
        {
            return new MinimumErrorNeuralNetworkTeacher(network, examples, CountInEpoch, minimumError);
        }

        public static float[] ForwardPropagate(this INeuralNetwork network, params float[] args)
        {
            return network.ForwardPropagate(args);
        }
    }
}