﻿namespace Valkyrie.NewVersion.NotReviewed.AI.NeuralNetwork
{
    public interface INeuralNetwork
    {
        float[] ForwardPropagate(float[] set);
        void BackPropagate(float[] target);
    }
}