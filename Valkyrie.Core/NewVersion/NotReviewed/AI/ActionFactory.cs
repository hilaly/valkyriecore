using System;
using System.Linq;

namespace Valkyrie.NewVersion.NotReviewed.AI
{
    public static class ActionFactory
    {
        public static IActionInfo Create(Action func)
        {
            return new ActionInfo((args, finish) =>
            {
                if (args == null || !args.Any())
                {
                    func();
                    finish(true);
                    return true;
                }
                return false;
            });
        }
    }
}