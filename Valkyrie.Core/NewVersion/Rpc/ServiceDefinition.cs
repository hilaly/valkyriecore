using System;
using System.Collections.Generic;
using Valkyrie.Data.Serialization;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;

namespace Valkyrie.Rpc
{
    public class ServiceDefinition
    {
        readonly Dictionary<string, ServerMethodInfo> _methods = new Dictionary<string, ServerMethodInfo>();
        private readonly IContractSerializer _contractSerializer = new ContractSerializer();

        internal IEnumerable<KeyValuePair<string, ServerMethodInfo>> Build()
        {
            return _methods;
        }

        internal ServerMethodInfo this[string rpcName]
        {
            get
            {
                if (_methods.TryGetValue(rpcName, out var result))
                    return result;

                throw new RpcException(RpcErrorCode.Unimplemented);
            }
        }

        //UnaryCall
        public ServiceDefinition BindMethod<TRequest, TResponse>(string rpcName,
            Func<TRequest, ServerCallContext, IPromise<TResponse>> methodImpl)
        {
            _methods.Add(rpcName, new ServerUnaryCallInfo<TRequest, TResponse>(methodImpl, _contractSerializer));

            return this;
        }

        //ServerStreamingCall
        public ServiceDefinition BindMethod<TRequest, TResponse>(string rpcName,
            Action<TRequest, IObserver<TResponse>, ServerCallContext> methodImpl)
        {
            _methods.Add(rpcName, new ServerServerStreamingCallInfo<TRequest, TResponse>(methodImpl));

            return this;
        }

        //Client streaming
        public ServiceDefinition BindMethod<TRequest, TResponse>(string rpcName,
            Func<IObservable<TRequest>, ServerCallContext, IPromise<TResponse>> methodImpl)
        {
            _methods.Add(rpcName, new ServerClientStreamingCallInfo<TRequest, TResponse>(methodImpl));

            return this;
        }

        //duplex streaming
        public ServiceDefinition BindMethod<TRequest, TResponse>(string rpcName,
            Action<IObservable<TRequest>, IObserver<TResponse>, ServerCallContext>
                methodImpl)
        {
            _methods.Add(rpcName,
                new ServerDuplexStreamingCallInfo<TRequest, TResponse>(methodImpl, _contractSerializer));

            return this;
        }

        internal abstract class ServerMethodInfo
        {
            public abstract IServerRpcHandler CreateRequestHandler(ServerCallContext ctx);
        }

        internal interface IServerRpcHandler
        {
            void Receive(byte[] data, bool isFinished, string requestUid);
            void Error(RpcErrorCode error, string details, string requestUid);
        }

        internal interface IStreamServerRpcHandler : IServerRpcHandler
        {
        }

        class ServerServerStreamingCallInfo<TRequest, TResponse> : ServerMethodInfo
        {
            private readonly Action<TRequest, IObserver<TResponse>, ServerCallContext> _impl;

            public ServerServerStreamingCallInfo(Action<TRequest, IObserver<TResponse>, ServerCallContext> impl)
            {
                _impl = impl;
            }

            public override IServerRpcHandler CreateRequestHandler(ServerCallContext ctx)
            {
                return new ServerStreamingHandler(_impl, ctx);
            }

            class ServerStreamingHandler : IServerRpcHandler
            {
                private readonly Action<TRequest, IObserver<TResponse>, ServerCallContext> _impl;
                private readonly ServerCallContext _callContext;
                private readonly Subject<TResponse> _outStream;

                public ServerStreamingHandler(Action<TRequest, IObserver<TResponse>, ServerCallContext> impl,
                    ServerCallContext callContext)
                {
                    _impl = impl;
                    _callContext = callContext;

                    _outStream = new Subject<TResponse>();
                    _outStream.Subscribe(response =>
                    {
                        RpcUtils.SendRpcMessage(_callContext.Channel, new RpcMessage
                            {
                                IsFinished = false,
                                Data = _callContext.Serializer.Serialize(response, false),
                                Error = RpcErrorCode.Ok,
                                RequestUid = _callContext.RequestUid
                            },
                            RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                    }, exception =>
                    {
                        switch (exception)
                        {
                            case RpcException rpcException:
                                Error(rpcException.Code, rpcException.Details, _callContext.RequestUid);
                                break;
                            case NotImplementedException notImplementedException:
                                Error(RpcErrorCode.Unimplemented, null, _callContext.RequestUid);
                                break;
                            default:
                                Error(RpcErrorCode.Internal, null, _callContext.RequestUid);
                                break;
                        }
                    }, () =>
                    {
                        RpcUtils.SendRpcMessage(_callContext.Channel, new RpcMessage
                            {
                                IsFinished = true,
                                Data = null,
                                Error = RpcErrorCode.Ok,
                                RequestUid = _callContext.RequestUid
                            },
                            RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                    });
                }

                public void Receive(byte[] data, bool isFinished, string requestUid)
                {
                    _impl(_callContext.Serializer.Deserialize<TRequest>(data), _outStream, _callContext);
                }

                public void Error(RpcErrorCode error, string details, string requestUid)
                {
                    throw new NotImplementedException();
                }
            }
        }

        class ServerDuplexStreamingCallInfo<TRequest, TResponse> : ServerMethodInfo
        {
            private readonly Action<IObservable<TRequest>, IObserver<TResponse>, ServerCallContext> _impl;
            private readonly IContractSerializer _contractSerializer;

            public ServerDuplexStreamingCallInfo(
                Action<IObservable<TRequest>, IObserver<TResponse>, ServerCallContext> impl,
                IContractSerializer contractSerializer)
            {
                _impl = impl;
                _contractSerializer = contractSerializer;
            }

            public override IServerRpcHandler CreateRequestHandler(ServerCallContext ctx)
            {
                return new DuplexStreamingHandler(_impl, ctx);
            }

            class DuplexStreamingHandler : IStreamServerRpcHandler
            {
                private readonly Action<IObservable<TRequest>, IObserver<TResponse>, ServerCallContext> _impl;
                private readonly ServerCallContext _callContext;
                private readonly Subject<TRequest> _requestsStream;

                public DuplexStreamingHandler(
                    Action<IObservable<TRequest>, IObserver<TResponse>, ServerCallContext> impl,
                    ServerCallContext callContext)
                {
                    _impl = impl;
                    _callContext = callContext;

                    _requestsStream = new Subject<TRequest>();
                    var responsesStream = new Subject<TResponse>();
                    responsesStream.Subscribe(response =>
                    {
                        RpcUtils.SendRpcMessage(callContext.Channel, new RpcMessage
                            {
                                IsFinished = false,
                                Data = _callContext.Serializer.Serialize(response, false),
                                Error = RpcErrorCode.Ok,
                                RequestUid = _callContext.RequestUid
                            },
                            RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                    }, exception =>
                    {
                        switch (exception)
                        {
                            case RpcException rpcException:
                                Error(rpcException.Code, rpcException.Details, _callContext.RequestUid);
                                break;
                            case NotImplementedException notImplementedException:
                                Error(RpcErrorCode.Unimplemented, null, _callContext.RequestUid);
                                break;
                            default:
                                Error(RpcErrorCode.Internal, null, _callContext.RequestUid);
                                break;
                        }

                        _requestsStream.OnCompleted();
                    }, () =>
                    {
                        RpcUtils.SendRpcMessage(callContext.Channel, new RpcMessage
                            {
                                IsFinished = true,
                                Data = null,
                                Error = RpcErrorCode.Ok,
                                RequestUid = _callContext.RequestUid
                            },
                            RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                    });

                    _impl(_requestsStream, responsesStream, callContext);
                }

                public void Receive(byte[] data, bool isFinished, string requestUid)
                {
                    if (data != null)
                    {
                        _requestsStream.OnNext(_callContext.Serializer.Deserialize<TRequest>(data));
                        if (isFinished)
                            _requestsStream.OnCompleted();
                    }
                    else if (isFinished)
                        _requestsStream.OnCompleted();
                    else
                        throw new RpcException(RpcErrorCode.Transfer);
                }

                public void Error(RpcErrorCode error, string details, string requestUid)
                {
                    _requestsStream.OnError(new RpcException(error, details));
                }
            }
        }

        class ServerUnaryCallInfo<TRequest, TResponse> : ServerMethodInfo
        {
            private readonly Func<TRequest, ServerCallContext, IPromise<TResponse>> _impl;
            private readonly IContractSerializer _contractSerializer;

            public ServerUnaryCallInfo(Func<TRequest, ServerCallContext, IPromise<TResponse>> impl,
                IContractSerializer contractSerializer)
            {
                _impl = impl;
                _contractSerializer = contractSerializer;
            }

            public override IServerRpcHandler CreateRequestHandler(ServerCallContext ctx)
            {
                return new UnaryCallHandler(_impl, ctx);
            }

            class UnaryCallHandler : IServerRpcHandler
            {
                private readonly Func<TRequest, ServerCallContext, IPromise<TResponse>> _impl;
                private readonly ServerCallContext _callContext;

                public UnaryCallHandler(Func<TRequest, ServerCallContext, IPromise<TResponse>> impl,
                    ServerCallContext callContext)
                {
                    _impl = impl;
                    _callContext = callContext;
                }

                public void Receive(byte[] data, bool isFinished, string requestUid)
                {
                    _impl(_callContext.Serializer.Deserialize<TRequest>(data), _callContext).Done(response =>
                    {
                        RpcUtils.SendRpcMessage(_callContext.Channel, new RpcMessage
                            {
                                IsFinished = true,
                                Data = _callContext.Serializer.Serialize(response, false),
                                Error = RpcErrorCode.Ok,
                                RequestUid = requestUid
                            },
                            RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                    }, exception =>
                    {
                        switch (exception)
                        {
                            case RpcException rpcException:
                                Error(rpcException.Code, rpcException.Details, requestUid);
                                break;
                            case NotImplementedException notImplementedException:
                                Error(RpcErrorCode.Unimplemented, null, requestUid);
                                break;
                            default:
                                Error(RpcErrorCode.Internal, null, requestUid);
                                break;
                        }
                    });
                }

                public void Error(RpcErrorCode error, string details, string requestUid)
                {
                    var om = new RpcMessage
                    {
                        RequestUid = requestUid,
                        IsFinished = true,
                        Error = error,
                        Details = details
                    };
                    RpcUtils.SendRpcMessage(_callContext.Channel, om,
                        RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                }
            }
        }

        class ServerClientStreamingCallInfo<TRequest, TResponse> : ServerMethodInfo
        {
            private readonly Func<IObservable<TRequest>, ServerCallContext, IPromise<TResponse>> _impl;

            public ServerClientStreamingCallInfo(
                Func<IObservable<TRequest>, ServerCallContext, IPromise<TResponse>> impl)
            {
                _impl = impl;
            }

            public override IServerRpcHandler CreateRequestHandler(ServerCallContext ctx)
            {
                return new ClientStreamingHandler(_impl, ctx);
            }

            class ClientStreamingHandler : IStreamServerRpcHandler
            {
                private readonly IContractSerializer _contractSerializer;
                private readonly Subject<TRequest> _requestsStream;

                public ClientStreamingHandler(Func<IObservable<TRequest>, ServerCallContext, IPromise<TResponse>> impl,
                    ServerCallContext callContext)
                {
                    _contractSerializer = callContext.Serializer;
                    _requestsStream = new Subject<TRequest>();

                    var requestUid = callContext.RequestUid;
                    impl(_requestsStream, callContext).Done(response =>
                    {
                        RpcUtils.SendRpcMessage(callContext.Channel, new RpcMessage
                            {
                                IsFinished = true,
                                Data = _contractSerializer.Serialize(response, false),
                                Error = RpcErrorCode.Ok,
                                RequestUid = requestUid
                            },
                            RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                        _requestsStream.OnCompleted();
                    }, exception =>
                    {
                        switch (exception)
                        {
                            case RpcException rpcException:
                                Error(rpcException.Code, rpcException.Details, requestUid);
                                break;
                            case NotImplementedException notImplementedException:
                                Error(RpcErrorCode.Unimplemented, null, requestUid);
                                break;
                            default:
                                Error(RpcErrorCode.Internal, null, requestUid);
                                break;
                        }

                        _requestsStream.OnCompleted();
                    });
                }

                public void Receive(byte[] data, bool isFinished, string requestUid)
                {
                    if (data != null)
                    {
                        _requestsStream.OnNext(_contractSerializer.Deserialize<TRequest>(data));
                        if (isFinished)
                            _requestsStream.OnCompleted();
                    }
                    else if (isFinished)
                        _requestsStream.OnCompleted();
                    else
                        throw new RpcException(RpcErrorCode.Transfer);
                }

                public void Error(RpcErrorCode error, string details, string requestUid)
                {
                    _requestsStream.OnError(new RpcException(error, details));
                }
            }
        }
    }
}