using Valkyrie.Communication.Network;
using Valkyrie.Data.Serialization;
using Valkyrie.Network;

namespace Valkyrie.Rpc
{
    static class RpcUtils
    {
        internal enum NetworkDataType
        {
            Unknown = 0,
            Rpc = 1 << 0,
            Contract = 1 << 1,
            RawData = 1 << 2
        }

        private static IContractSerializer _serializer;
        internal static IContractSerializer Serializer => _serializer ??= new ContractSerializer();

        internal static void SendContract(IClient networkChannel, object message, NetDeliveryMethod delivery,
            int channel)
        {
            SendImpl(networkChannel, NetworkDataType.Contract, Serializer.Serialize(message, true), delivery, channel);
        }
        public static void SendRpcMessage(IClient networkChannel, RpcMessage message, NetDeliveryMethod delivery, int channel)
        {
            SendImpl(networkChannel, NetworkDataType.Rpc, Serializer.Serialize(message, false), delivery, channel);
        }
        public static void SendRpcMessage(IClient networkChannel, RpcMessage message, ClientCallOptions options)
        {
            SendRpcMessage(networkChannel, message, options.Delivery, options.RpcChannel);
        }

        static void SendImpl(IClient networkChannel, NetworkDataType type, byte[] rawData, NetDeliveryMethod delivery, int channel)
        {
            var bp = new SimplePacker();
            bp.Write((byte)type);
            bp.Write(rawData);
            networkChannel.Send(bp.Flush(), delivery, channel);
        }

        internal static NetworkDataType DecodeType(byte[] msg)
        {
            return (NetworkDataType) msg[0];
        }

        internal static RpcMessage ReadRpc(byte[] msgBytes)
        {
            var packer = new SimplePacker();
            packer.Load(msgBytes);
            packer.ReadByte();
            return Serializer.Deserialize<RpcMessage>(packer.ReadBytes());
        }
    }
}