using Valkyrie.Data.Serialization;
// ReSharper disable CheckNamespace
// ReSharper disable IdentifierTypo

namespace google.protobuf
{
    [Contract]
    public class Empty : IContract
    {
        public void Serialize(IBitsPacker packer)
        {
        }

        public void Deserialize(IBitsPacker packer)
        {
        }
    }
}