using System;
using Valkyrie.Data;

namespace Valkyrie.Rpc
{
    public enum RpcErrorCode
    {
        Ok = 0,
        Unavailable,
        Unimplemented,
        Internal,
        Transfer,
        FailedPrecondition
    }
    
    public class RpcException : Exception
    {
        public RpcErrorCode Code { get; }
        public string Details { get; }

        static string GetErrorDetails(RpcErrorCode code)
        {
            switch (code)
            {
                case RpcErrorCode.Ok:
                    return "Success";
                case RpcErrorCode.Unavailable:
                    return "The service is currently unavailable";
                case RpcErrorCode.Unimplemented:
                    return "Operation is not implemented or not supported/enabled";
                case RpcErrorCode.Internal:
                    return "Internal error";
                case RpcErrorCode.Transfer:
                    return "Network transfer error, possible lost of data";
                case RpcErrorCode.FailedPrecondition:
                    return
                        "Operation was rejected because the system is not in a state required for the operation's execution.";
                default:
                    throw new ArgumentOutOfRangeException(nameof(code), code, null);
            }
        }

        public RpcException(RpcErrorCode code, string description)
            : base($"Code={code}, Details={(description.NotNullOrEmpty() ? description : GetErrorDetails(code))}")
        {
            Code = code;
            Details = description.NotNullOrEmpty() ? description : GetErrorDetails(code);
        }
        public RpcException(RpcErrorCode code)
            : this(code, GetErrorDetails(code))
        {
        }
    }
}