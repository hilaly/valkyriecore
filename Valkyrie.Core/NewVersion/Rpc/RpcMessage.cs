using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;

namespace Valkyrie.Rpc
{
    [Contract, NetworkMessage(RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel)]
    class RpcMessage : IContract
    {
        internal const NetDeliveryMethod DeliveryMethod = NetDeliveryMethod.ReliableOrdered;
        internal const int DeliveryChannel = 10;
        
        [ContractMember(1)] public string RpcHash;
        [ContractMember(2)] public string RequestUid;
        
        [ContractMember(3), Ignore] public byte[] Data;
        [ContractMember(4)] public bool IsFinished;

        [ContractMember(5)] public RpcErrorCode Error = RpcErrorCode.Ok;
        [ContractMember(6)] public string Details;
        
        public void Serialize(IBitsPacker packer)
        {
            if(RpcHash.IsNullOrEmpty())
                packer.Write(false);
            else
            {
                packer.Write(true);
                packer.Write(RpcHash);
            }
            if(RequestUid.IsNullOrEmpty())
                packer.Write(false);
            else
            {
                packer.Write(true);
                packer.Write(RequestUid);
            }
            packer.Write((byte)Error);
            
            
            if (Error == RpcErrorCode.Ok)
            {
                if(Data == null)
                    packer.Write(false);
                else
                {
                    packer.Write(true);
                    packer.Write(Data);
                }
                packer.Write(IsFinished);
            }
            else
            {
                if(Details.IsNullOrEmpty())
                    packer.Write(false);
                else
                {
                    packer.Write(true);
                    packer.Write(Details);
                }
            }
        }

        public void Deserialize(IBitsPacker packer)
        {
            RpcHash = packer.ReadBool() ? packer.ReadString() : null;
            RequestUid = packer.ReadBool() ? packer.ReadString() : null;
            Error = (RpcErrorCode) packer.ReadByte();
            
            
            if (Error == RpcErrorCode.Ok)
            {
                Data = packer.ReadBool() ? packer.ReadBytes() : null;
                IsFinished = packer.ReadBool();
            }
            else
                Details = packer.ReadBool() ? packer.ReadString() : null;
        }
    }
}