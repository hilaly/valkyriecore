using Valkyrie.Communication.Network;

namespace Valkyrie.Rpc
{
    public sealed class ClientCallOptions
    {
        public NetDeliveryMethod Delivery { get; set; } = RpcMessage.DeliveryMethod;
        public int RpcChannel { get; set; } = RpcMessage.DeliveryChannel;
    }
}