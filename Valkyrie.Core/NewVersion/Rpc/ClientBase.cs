using System;
using System.Collections.Generic;
using System.Diagnostics;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;

namespace Valkyrie.Rpc
{
    internal interface IClientWaitingRequest : IDisposable
    {
        string Id { get; }

        void Receive(byte[] data, bool isFinished);
        void Error(RpcErrorCode error, string details);
    }

    public interface IClientWriteStream<in T>
    {
        void Write(T value);
        void Close();
        IClient Channel { get; }
    }

    #region SingleToSingle

    //SingleToSingle
    public interface IAsyncUnaryCall<TResponse> : IPromise<TResponse>
    {
        IPromise<TResponse> Response { get; }
    }

    class AsyncUnaryCall<TResponse> : Promise<TResponse>, IAsyncUnaryCall<TResponse>, IClientWaitingRequest
    {
        private readonly IContractSerializer _serializer;
        private readonly RpcMessage _outMessage;
        private readonly Action<IClientWaitingRequest> _disposeAction;
        private bool _isDisposed;

        public AsyncUnaryCall(IClient client, IContractSerializer serializer,
            ClientCallOptions callOptions, RpcMessage outMessage, object request,
            Action<IClientWaitingRequest> disposeAction)
        {
            _serializer = serializer;
            _outMessage = outMessage;
            _disposeAction = disposeAction;

            _outMessage.Data = _serializer.Serialize(request, false);
            _outMessage.IsFinished = true;
            RpcUtils.SendRpcMessage(client, _outMessage, callOptions);
        }

        #region IClientWaitnigRequest

        string IClientWaitingRequest.Id => _outMessage.RequestUid;

        public void Receive(byte[] data, bool isFinished)
        {
            Resolve(_serializer.Deserialize<TResponse>(data));
        }

        public void Error(RpcErrorCode error, string details)
        {
            Reject(new RpcException(error, details ?? string.Empty));
        }

        #endregion

        #region IAsyncUnaryCall

        public IPromise<TResponse> Response => this;

        #endregion

        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;

            //TODO: replace with RpcException
            if (CurState == PromiseState.Pending)
                Reject(new ObjectDisposedException($"{_outMessage.RpcHash}",
                    "Response was not received"));

            _disposeAction?.Invoke(this);
        }
    }

    #endregion

    class ClientWriteStream<T> : IClientWriteStream<T>
    {
        private readonly RpcMessage _outMessage;
        private readonly IContractSerializer _serializer;
        private readonly IClient _networkChannel;
        private readonly ClientCallOptions _callOptions;

        public IClient Channel => _networkChannel;

        public ClientWriteStream(RpcMessage outMessage, IContractSerializer serializer, IClient networkChannel, ClientCallOptions callOptions)
        {
            _outMessage = outMessage;
            _serializer = serializer;
            _networkChannel = networkChannel;
            _callOptions = callOptions;
        }

        public void Write(T request)
        {
            Debug.Assert(_outMessage.IsFinished == false, "Stream is closed");
            _outMessage.Data = _serializer.Serialize(request, false);
            RpcUtils.SendRpcMessage(_networkChannel, _outMessage, _callOptions);
        }

        public void Close()
        {
            _outMessage.Data = null;
            _outMessage.IsFinished = true;
            RpcUtils.SendRpcMessage(_networkChannel, _outMessage, _callOptions);
        }
    }

    #region ManyToSingle

    public interface IAsyncClientStreamingCall<in TRequest, TResponse> : IPromise<TResponse>
    {
        IPromise<TResponse> Response { get; }

        IClientWriteStream<TRequest> RequestsStream { get; }
    }

    class AsyncStreamingCall<TRequest, TResponse> : Promise<TResponse>, IAsyncClientStreamingCall<TRequest, TResponse>,
        IClientWaitingRequest
    {
        private readonly IContractSerializer _serializer;
        private readonly RpcMessage _outMessage;
        private readonly Action<IClientWaitingRequest> _disposeAction;
        private bool _isDisposed;

        public AsyncStreamingCall(IClient networkChannel, IContractSerializer serializer,
            ClientCallOptions callOptions, RpcMessage outMessage, Action<IClientWaitingRequest> disposeAction)
        {
            _serializer = serializer;
            _outMessage = outMessage;
            _disposeAction = disposeAction;

            RequestsStream = new ClientWriteStream<TRequest>(_outMessage, serializer, networkChannel, callOptions);
        }

        #region IAsyncStreamingCall

        public IPromise<TResponse> Response => this;
        public IClientWriteStream<TRequest> RequestsStream { get; }

        #endregion

        #region IClientWaitnigRequest

        string IClientWaitingRequest.Id => _outMessage.RequestUid;

        public void Receive(byte[] data, bool isFinished)
        {
            Resolve(_serializer.Deserialize<TResponse>(data));
        }

        public void Error(RpcErrorCode error, string details)
        {
            Reject(new RpcException(error, details ?? string.Empty));
        }

        #endregion

        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;

            //TODO: replace with RpcException
            if (CurState == PromiseState.Pending)
                Reject(new ObjectDisposedException($"{_outMessage.RpcHash}",
                    "Response was not received"));

            _disposeAction?.Invoke(this);
        }
    }

    #endregion

    #region SingleToMany

    public interface IAsyncServerStreamingCall<out TResponse>
    {
        IObservable<TResponse> ResponsesStream { get; }
    }

    class AsyncServerStreamingCall<TRequest, TResponse> : IAsyncServerStreamingCall<TResponse>, IClientWaitingRequest
    {
        private readonly IContractSerializer _serializer;
        private readonly RpcMessage _outMessage;
        private readonly Action<IClientWaitingRequest> _disposeAction;
        private readonly Subject<TResponse> _responseStream = new Subject<TResponse>();
        private bool _isDisposed;

        #region IAsyncServerStreamingCall

        public IObservable<TResponse> ResponsesStream => _responseStream;

        #endregion

        public AsyncServerStreamingCall(IClient networkChannel, IContractSerializer serializer,
            ClientCallOptions callOptions, RpcMessage outMessage, TRequest request,
            Action<IClientWaitingRequest> disposeAction)
        {
            _serializer = serializer;
            _outMessage = outMessage;
            _disposeAction = disposeAction;

            _outMessage.Data = _serializer.Serialize(request, false);
            _outMessage.IsFinished = true;
            RpcUtils.SendRpcMessage(networkChannel, _outMessage, callOptions);
        }

        #region IClientWaitnigRequest

        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;

            //TODO: replace with RpcException
            _responseStream.Dispose();
            _disposeAction?.Invoke(this);
        }

        public string Id => _outMessage.RequestUid;

        public void Receive(byte[] data, bool isFinished)
        {
            if (data != null)
                _responseStream.OnNext(_serializer.Deserialize<TResponse>(data));
            if (isFinished)
                _responseStream.OnCompleted();
        }

        public void Error(RpcErrorCode error, string details)
        {
            _responseStream.OnError(new RpcException(error, details));
        }

        #endregion
    }

    #endregion

    #region ManyToMany

    public interface IAsyncDuplexStreamingCall<in TRequest, out TResponse>
    {
        IObservable<TResponse> ResponsesStream { get; }
        IClientWriteStream<TRequest> RequestsStream { get; }
    }

    class AsyncDuplexStreamingCall<TRequest, TResponse> : IAsyncDuplexStreamingCall<TRequest, TResponse>,
        IClientWaitingRequest
    {
        private readonly IContractSerializer _serializer;
        private readonly RpcMessage _outMessage;
        private readonly Action<IClientWaitingRequest> _disposeAction;
        private readonly Subject<TResponse> _responseStream = new Subject<TResponse>();
        private bool _isDisposed;

        public IObservable<TResponse> ResponsesStream => _responseStream;
        public IClientWriteStream<TRequest> RequestsStream { get; }

        public AsyncDuplexStreamingCall(IClient networkChannel, IContractSerializer serializer,
            ClientCallOptions callOptions, RpcMessage outMessage, Action<IClientWaitingRequest> disposeAction)
        {
            _serializer = serializer;
            _outMessage = outMessage;
            _disposeAction = disposeAction;

            RequestsStream = new ClientWriteStream<TRequest>(_outMessage, serializer, networkChannel, callOptions);
        }

        #region IClientWaitingRequest
        public void Dispose()
        {
            if (_isDisposed)
                return;
            _isDisposed = true;

            //TODO: replace with RpcException
            _responseStream.Dispose();
            _disposeAction?.Invoke(this);
        }

        public string Id => _outMessage.RequestUid;
        
        public void Receive(byte[] data, bool isFinished)
        {
            if (data != null)
                _responseStream.OnNext(_serializer.Deserialize<TResponse>(data));
            if (isFinished)
                _responseStream.OnCompleted();
        }

        public void Error(RpcErrorCode error, string details)
        {
            _responseStream.OnError(new RpcException(error, details));
        }

        #endregion
    }

    #endregion

    public abstract class ClientBase : IDisposable
    {
        private readonly INetworkChannel _channel;
        private readonly IContractSerializer _contractSerializer = new ContractSerializer();

        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        private readonly Dictionary<string, IClientWaitingRequest> _requests =
            new Dictionary<string, IClientWaitingRequest>();

        private IClient _client;

        protected ClientBase(INetworkChannel channel)
        {
            _channel = channel;
            _disposable.Add(_channel.Listener.Subscribe<RpcMessage>(HandleRpcMessage));
        }

        protected ClientBase(IClient networkClient)
        {
            _client = networkClient;
            _disposable.Add(((LidgrenClient) _client).SubscribeIncoming().Subscribe(HandleByteMessage));
        }

        void HandleByteMessage(byte[] bytes)
        {
            var type = RpcUtils.DecodeType(bytes);
            if (type == RpcUtils.NetworkDataType.Rpc)
                HandleRpcMessage(RpcUtils.ReadRpc(bytes));
        }

        public INetworkChannel Channel => _channel;

        public void Dispose()
        {
            _disposable.Dispose();
        }

        #region OutMethods

        protected IAsyncUnaryCall<TResponse> SingleToSingle<TRequest, TResponse>(string rpcName, TRequest request,
            ClientCallOptions callOptions)
            where TRequest : class
        {
            return SaveRequest(new AsyncUnaryCall<TResponse>(
                _channel ?? _client, _contractSerializer, callOptions,
                CreateNew(rpcName), request, FinishRequest));
        }

        protected IAsyncClientStreamingCall<TRequest, TResponse> ManyToSingle<TRequest, TResponse>(string rpcName,
            ClientCallOptions callOptions)
        {
            return SaveRequest(new AsyncStreamingCall<TRequest, TResponse>(
                _channel ?? _client, _contractSerializer, callOptions,
                CreateNew(rpcName), FinishRequest));
        }

        protected IAsyncServerStreamingCall<TResponse> SingleToMany<TRequest, TResponse>(string rpcName,
            TRequest request,
            ClientCallOptions callOptions)
        {
            return SaveRequest(new AsyncServerStreamingCall<TRequest, TResponse>(
                _channel ?? _client, _contractSerializer, callOptions,
                CreateNew(rpcName), request, FinishRequest));
        }

        protected IAsyncDuplexStreamingCall<TRequest, TResponse> ManyToMany<TRequest, TResponse>(string rpcName,
            ClientCallOptions callOptions)
        {
            return SaveRequest(new AsyncDuplexStreamingCall<TRequest, TResponse>(
                _channel ?? _client, _contractSerializer, callOptions,
                CreateNew(rpcName), FinishRequest));
        }

        T SaveRequest<T>(T wait) where T : IClientWaitingRequest
        {
            lock (_requests)
                _requests.Add(((IClientWaitingRequest) wait).Id, wait);
            lock (_disposable)
                _disposable.Add(wait);
            return wait;
        }

        #endregion

        private void HandleRpcMessage(RpcMessage message)
        {
            /*
            if (message.ServiceUid != _serviceUid)
                return;

            if (!_rpcNames.Contains(message.RpcUid))
                return;
            */

            IClientWaitingRequest handler;
            lock (_requests)
                if (!_requests.TryGetValue(message.RequestUid, out handler))
                    return;

            if (message.Error == RpcErrorCode.Ok)
                handler.Receive(message.Data, message.IsFinished);
            else
            {
                handler.Error(message.Error, message.Details);
                message.IsFinished = true;
            }

            if (!message.IsFinished)
                return;

            lock (_requests)
                _requests.Remove(message.RequestUid);
            handler.Dispose();
        }

        void FinishRequest(IClientWaitingRequest waitingRequest)
        {
            lock (_requests)
                _requests.Remove(waitingRequest.Id);
            lock (_disposable)
                _disposable.Remove(waitingRequest);
        }

        RpcMessage CreateNew(string rpcName)
        {
            return new RpcMessage
            {
                RpcHash = rpcName,
                RequestUid = Guid.NewGuid().ToString().Replace("-", string.Empty)
            };
        }
    }
}