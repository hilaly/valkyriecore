using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Data.Config;
using Valkyrie.Data.Serialization;
using Valkyrie.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Rpc
{
    public class ServerPort
    {
        public ServerPort()
        {
        }

        public ServerPort(int port)
        {
            Port = port;
        }

        public string Address { get; set; }
        public int Port { get; set; }
        public int MaxConnections { get; set; } = int.MaxValue;
        public string AppId { get; set; } = "valkyrie_rpc_app";
        public string AuthKey { get; set; } = "valkyrie_rpc_default_key";

        public override string ToString()
        {
            return $"{(Address.NotNullOrEmpty() ? Address + ":" : "")}{Port}";
        }
    }

    public class Server : IDisposable
    {
        private readonly INetwork _network;
        private readonly List<ServerPort> _ports = new List<ServerPort>();
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        private readonly Dictionary<string, ServiceDefinition.ServerMethodInfo> _rpcs =
            new Dictionary<string, ServiceDefinition.ServerMethodInfo>();

        private readonly Dictionary<string, ServiceDefinition.IServerRpcHandler> _activeRequests =
            new Dictionary<string, ServiceDefinition.IServerRpcHandler>();

        private readonly IContractSerializer _contractSerializer = new ContractSerializer();
        private readonly List<ServiceDefinition> _servicesDefines = new List<ServiceDefinition>();
        private ILogInstance _log;

        public Func<ServerPort, INetworkChannel, string, ErrorCodes> AuthHandler = (port, channel, s) =>
            s == port.AuthKey ? ErrorCodes.Ok : ErrorCodes.AuthFailed;

        private readonly List<IServer> _listeners;

        public Server(INetwork network, ILogService logService)
        {
            _network = network;
            _log = logService.GetLog("Valkyrie.Rpc.Server");
            _listeners = new List<IServer>();
        }

        public Server(IDispatcher dispatcher, ILogService logService)
        {
            _log = logService.GetLog("Valkyrie.Rpc.Server");
            _listeners = new List<IServer>();
            dispatcher.EveryUpdate(UpdateListeners);
        }

        public IEnumerable<ServerPort> Ports
        {
            get => _ports;
            set
            {
                _ports.Clear();
                _ports.AddRange(value);
            }
        }

        public IEnumerable<ServiceDefinition> Services
        {
            get => _servicesDefines;
            set
            {
                _servicesDefines.Clear();
                _servicesDefines.AddRange(value);
                
                _rpcs.Clear();
                
                foreach (var definition in value)
                foreach (var pair in definition.Build())
                    _rpcs.Add(pair.Key, pair.Value);
            }
        }

        public IPromise Start()
        {
            return Promise.All(
                _ports.Select(portDef => _network.Listen(portDef.Port, portDef.MaxConnections, portDef.AppId,
                        (channel, s) => AuthHandler(portDef, channel, s))
                    .Then(SubscribeListener)));
        }
        

        private void UpdateListeners()
        {
            List<SerializedKeyValuePair<IClientView, byte[]>> msgs;
            lock (_listeners)
                msgs = _listeners.SelectMany(x => x.PopMessages()).ToList();
            foreach (var pair in msgs)
            {
                if (RpcUtils.DecodeType(pair.Value) == RpcUtils.NetworkDataType.Rpc)
                    EventHandler(RpcUtils.ReadRpc(pair.Value), pair.Key);
            }
        }

        public async Task StartAsync()
        {
            foreach (var portDef in _ports)
            {
                var serverListener = await Network.Utils.StartServerAsync(portDef, _log);
                lock(_listeners)
                    _listeners.Add(serverListener);
                _log.Info($"RPC server listen on {serverListener.Port}");
            }
        }

        private IPromise SubscribeListener(INetworkListener listener)
        {
            _disposable.Add(listener.Subscribe<RpcMessage>(EventHandler));
            return Promise.Resolved();
        }

        private void EventHandler(RpcMessage rpcMessage, IClient sourceChannel)
        {
            try
            {
                Handle(rpcMessage, sourceChannel);
            }
            catch (RpcException rpcException)
            {
                var om = new RpcMessage
                {
                    RequestUid = rpcMessage.RequestUid,
                    IsFinished = true,
                    Error = rpcException.Code,
                    Details = rpcException.Details
                };
                RpcUtils.SendRpcMessage(sourceChannel, om, RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                _log.Error($"Exception during handle rpc {rpcException}");
            }
            catch (NotImplementedException nie)
            {
                var om = new RpcMessage
                {
                    RequestUid = rpcMessage.RequestUid,
                    IsFinished = true,
                    Error = RpcErrorCode.Unimplemented,
                    Details = nie.ToString()
                };
                RpcUtils.SendRpcMessage(sourceChannel, om, RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                _log.Error($"Exception during handle rpc {nie}");
            }
            catch (Exception e)
            {
                var om = new RpcMessage
                {
                    RequestUid = rpcMessage.RequestUid,
                    IsFinished = true,
                    Error = RpcErrorCode.Internal,
                    Details = e.ToString()
                };
                RpcUtils.SendRpcMessage(sourceChannel, om, RpcMessage.DeliveryMethod, RpcMessage.DeliveryChannel);
                _log.Error($"Exception during handle rpc {e}");
            }
        }

        public void Dispose()
        {
            _disposable?.Dispose();

            _rpcs.Clear();
        }

        private void Handle(RpcMessage rpcMessage, IClient sourceChannel)
        {
            if (!_activeRequests.TryGetValue(rpcMessage.RequestUid, out var handler))
            {
                if (!_rpcs.TryGetValue(rpcMessage.RpcHash, out var method))
                    throw new RpcException(RpcErrorCode.Unavailable);
                handler = method.CreateRequestHandler(new ServerCallContext(rpcMessage.RequestUid, sourceChannel, _contractSerializer));
                if (handler is ServiceDefinition.IStreamServerRpcHandler)
                    _activeRequests.Add(rpcMessage.RequestUid, handler);
            }


            if (rpcMessage.Error == RpcErrorCode.Ok)
                handler.Receive(rpcMessage.Data, rpcMessage.IsFinished, rpcMessage.RequestUid);
            else
            {
                handler.Error(rpcMessage.Error, rpcMessage.Details, rpcMessage.RequestUid);
                rpcMessage.IsFinished = true;
            }

            if (rpcMessage.IsFinished && handler is ServiceDefinition.IStreamServerRpcHandler)
                _activeRequests.Remove(rpcMessage.RequestUid);
        }
    }

    public class ServerCallContext
    {
        public string RequestUid { get; }
        public IClient Channel { get; }
        public IContractSerializer Serializer { get; }

        public ServerCallContext(string requestUid, IClient channel, IContractSerializer serializer)
        {
            RequestUid = requestUid;
            Channel = channel;
            Serializer = serializer;
        }
    }
}