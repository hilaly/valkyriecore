using System.Collections.Generic;
using Valkyrie.Data.Config;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Iaps
{
    public enum Platform
    {
        Editor,
        GooglePlay,
        AppStore
    }

    public enum ProductType
    {
        Consumable,
        NonConsumable,
        Subscription,
    }

    public interface IPurchase
    {
        string Id { get; }
        ProductType ProductType { get; }
        
        bool IsActive { get; }

        float Cost { get; }
        string Currency { get; }
        string LocalizedCost { get; }
    }
    
    public sealed class PurchaseConfigData : BaseConfigData
    {
        public ProductType ProductType;
        public List<Platform> Platforms = new List<Platform>();

        public override bool Validate(ILogInstance logInstance)
        {
            if (Platforms.Count == 0)
                logInstance.Error($"{nameof(Platforms)} can not be empty");

            return base.Validate(logInstance)
                   && Platforms.Count > 0;
        }
    }
    
    public interface IPurchaseService
    {
        IPromise Init(string serverAddress, int port);
        
        IPromise<string> Buy(PurchaseConfigData iapConfig);

        IEnumerable<IPurchase> Purchases(bool includeInactive);
    }

    public class ValidatePurchaseRequest
    {
        public Platform Store;
        public string Payload;
    }
    public class ValidatePurchaseResponse
    {}
}