using System;
using System.Collections.Generic;
using System.Numerics;
using Valkyrie.Grammar;

namespace Valkyrie.Procedural.Fields
{
    class FieldsAstCompiler
    {
        public IFieldsProvider Compile(IAstNode node)
        {
            var result = new FieldsProvider();

            Build(result, node);
            
            return result;
        }

        float ReadFloat(List<IAstNode> nodes, int startIndex)
        {
            return nodes[startIndex].GetFloat();
        }

        string ReadString(List<IAstNode> nodes, int startIndex)
        {
            return nodes[startIndex].GetString();
        }

        Vector2 ReadVector2(List<IAstNode> nodes, int startIndex)
        {
            return new Vector2(nodes[startIndex].GetFloat(), nodes[startIndex + 2].GetFloat());
        }

        Vector3 ReadVector3(List<IAstNode> nodes, int startIndex)
        {
            return new Vector3(nodes[startIndex].GetFloat(), nodes[startIndex + 2].GetFloat(),
                nodes[startIndex + 4].GetFloat());
        }

        Func<object[], ISignedDistanceField> Build(FieldsProvider target, IAstNode node)
        {
            var nodes = (node as NonTerminalNode)?.Nodes;
            switch (node.Name)
            {
                case "DefinitionList":
                {
                    foreach (var t in nodes)
                        Build(target, t);
                    return null;
                }
                case "DEFINITION":
                {
                    var result = Build(target, nodes[3]);
                    target.Add(ReadString(nodes, 0), result);
                    return result;
                }
                case "SDF":
                {
                    return Build(target, nodes[0]);
                }
                case "REPLACE":
                {
                    var id = ReadString(nodes, 0);
                    return (args) => target.Construct(id, args);
                }
                case "UNION":
                case "SUBTRACTION":
                case "INTERSECT":
                {
                    var sdf0 = Build(target, nodes[2]);
                    var sdf1 = Build(target, nodes[4]);
                    var keyword = ReadString(nodes, 0); 
                    switch (keyword)
                    {
                        case "Union":
                            return (args) => Factory.Union(sdf0(args), sdf1(args));
                        case "Subtract":
                            return (args) => Factory.Subtraction(sdf0(args), sdf1(args));
                        case "Intersect":
                            return (args) => Factory.Intersection(sdf0(args), sdf1(args));
                        default:
                            throw new Exception($"Unknown Keywork {keyword}");
                    }
                }
                case "ELONGATE":
                case "ROUND":
                case "ONION":
                case "INVERT":
                case "TRANSLATE":
                case "ROTATE":
                case "SCALE":
                {
                    var sdf = Build(target, nodes[2]);
                    var keyword = ReadString(nodes, 0); 
                    switch (keyword)
                    {
                        case "Elongate":
                        {
                            var value = ReadVector3(nodes, 4);
                            return (args) => Factory.Elongate(sdf(args), value);
                        }
                        case "Translate":
                        {
                            var value = ReadVector3(nodes, 4);
                            return (args) => Factory.Translate(sdf(args), value);
                        }
                        case "Rotate":
                        {
                            var value = ReadVector3(nodes, 4);
                            return (args) => Factory.Rotate(sdf(args), value);
                        }
                        case "Scale":
                        {
                            var value = ReadVector3(nodes, 4);
                            return (args) => Factory.Scale(sdf(args), value);
                        }
                        case "Round":
                        {
                            var value = ReadFloat(nodes, 4);
                            return (args) => Factory.Round(sdf(args), value);
                        }
                        case "Onion":
                        {
                            var value = ReadFloat(nodes, 4);
                            return (args) => Factory.Onion(sdf(args), value);
                        }
                        case "Not":
                        case "Invert":
                            return (args) => Factory.Invert(sdf(args));
                        default:
                            throw new Exception($"Unknown Keywork {keyword}");
                    }
                }
                case "PRIMITIVE":
                {
                    var primitiveId = nodes[0].Name;
                    if (primitiveId == "IDENTIFIER")
                        primitiveId = nodes[0].GetString();
                    
                    switch (primitiveId)
                    {
                        case "Sphere":
                        {
                            var radius = ReadFloat(nodes, 2);
                            return (args) => Factory.Sphere(radius);
                        }
                        case "Torus":
                        {
                            var size = ReadVector2(nodes, 2);
                            return (args) => Factory.Torus(size);
                        }
                        case "Cone":
                        {
                            var size = ReadVector2(nodes, 2);
                            return (args) => Factory.Cone(size);
                        }
                        case "Box":
                        {
                            var size = ReadVector3(nodes, 2);
                            return (args) => Factory.Box(size);
                        }
                        case "Cylinder":
                        {
                            var size = ReadVector3(nodes, 2);
                            return (args) => Factory.Cylinder(size);
                        }
                        default:
                            throw new Exception($"Unknown primitive {primitiveId}");
                    }
                }
                default:
                    throw new Exception($"Unknown node {node.Name}");
            }
        }
    }
}