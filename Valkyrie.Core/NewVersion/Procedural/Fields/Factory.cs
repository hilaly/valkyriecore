using System;
using System.IO;
using System.Numerics;
using System.Text;
using Valkyrie.Data;
using Valkyrie.Grammar;
using Valkyrie.Math;
using Valkyrie.Math.Noise;

namespace Valkyrie.Procedural.Fields
{
    public static class Factory
    {
        #region classes
        
        class FuncSourceSignedDistanceField : ISignedDistanceField
        {
            public Vector3 Position { get; set; } = Vector3.Zero;
            public Quaternion Rotation { get; set; } = Quaternion.Identity;
            public Vector3 Scale { get; set; } = Vector3.One;

            private readonly Func<Vector3, float> _sampleMethod;

            public FuncSourceSignedDistanceField(Func<Vector3, float> sampleMethod)
            {
                _sampleMethod = sampleMethod;
            }

            public float Sample(Vector3 position)
            {
                var localPoint = ConvertPointToLocal(position, Position, Rotation, Scale);
                return -_sampleMethod(localPoint);
            }

        }

        #endregion
        
        #region SDF methods
        
        static float SdSphere(Vector3 p, float s)
        {
            return Length(p) - s;
        }
        static float SdBox(Vector3 p, Vector3 b)
        {
            var d = Abs(p) - b;
            return Length(Max(d, 0.0f)) +
                   Min(Max(d.X, Max(d.Y, d.Z)), 0.0f); // remove this line for an only partially signed sdf
        }
        static float SdTorus(Vector3 p, Vector2 t)
        {
            var q = new Vector2(Length(p.XZ())-t.X,p.Y);
            return Length(q) - t.Y;
        }
        static float SdCylinder( Vector3 p, Vector3 c )
        {
            return Length(p.XZ() - c.XY()) - c.Z;
        }
        static float SdCone( Vector3 p, Vector2 c )
        {
            // c must be normalized
            var q = Length(p.XY());
            return Dot(c, new Vector2(q, p.Z));
        }
        
        #region Operations

        static float SdOpRound(Vector3 p, ISignedDistanceField sdf, float rad)
        {
            return -sdf.Sample(p) - rad;
        }

        static float SdOpElongate(Vector3 p, ISignedDistanceField sdf, Vector3 h)
        {
            var q = Abs(p)-h;
            return Min(Max(q.X, Max(q.Y, q.Z)), 0.0f) - sdf.Sample(Max(q, 0.0f));
        }
        
        static float SdOpOnion( Vector3 p, ISignedDistanceField sdf, float thickness )
        {
            return Abs(sdf.Sample(p)) - thickness;
        }

        static float SdOpUnion(Vector3 p, ISignedDistanceField d1, ISignedDistanceField d2)
        {
            return Min(-d1.Sample(p), -d2.Sample(p));
        }

        static float SdOpSubtraction(Vector3 p, ISignedDistanceField d1, ISignedDistanceField d2)
        {
            return Max(d1.Sample(p), -d2.Sample(p));
        }

        static float SdOpIntersection(Vector3 p, ISignedDistanceField d1, ISignedDistanceField d2)
        {
            return Max(-d1.Sample(p), -d2.Sample(p));
        }

        static float OpSmoothUnion(float d1, float d2, float k)
        {
            float h = Clamp(0.5f + 0.5f * (d2 - d1) / k, 0.0f, 1.0f);
            return Mix(d2, d1, h) - k * h * (1.0f - h);
        }

        static float OpSmoothSubtraction(float d1, float d2, float k)
        {
            float h = Clamp(0.5f - 0.5f * (d2 + d1) / k, 0.0f, 1.0f);
            return Mix(d2, -d1, h) + k * h * (1.0f - h);
        }

        static float OpSmoothIntersection(float d1, float d2, float k)
        {
            float h = Clamp(0.5f - 0.5f * (d2 - d1) / k, 0.0f, 1.0f);
            return Mix(d2, d1, h) + k * h * (1.0f - h);
        }

        #endregion
        
        #endregion
        
        #region Primitives

        public static ISignedDistanceField Sphere(float radius)
        {
            return new FuncSourceSignedDistanceField((p) => SdSphere(p, radius));
        }

        public static ISignedDistanceField Box(Vector3 size)
        {
            var halfSize = size * 0.5f;
            return new FuncSourceSignedDistanceField((p) => SdBox(p, halfSize));
        }

        public static ISignedDistanceField Torus(Vector2 t)
        {
            return new FuncSourceSignedDistanceField((p) => SdTorus(p, t));
        }

        public static ISignedDistanceField Cylinder(Vector3 c)
        {
            return new FuncSourceSignedDistanceField((p) => SdCylinder(p, c));
        }

        public static ISignedDistanceField Cone(Vector2 c)
        {
            var nc = c.Normalized();
            return new FuncSourceSignedDistanceField((p) => SdCone(p, nc));
        }
        
        #endregion
        
        #region Operations

        #region Unary
        
        public static ISignedDistanceField Round(this ISignedDistanceField sdf, float radius)
        {
            return new FuncSourceSignedDistanceField((p) => SdOpRound(p, sdf, radius));
        }

        /// <summary>
        /// Split SDF to 8 parts and longate it
        /// </summary>
        /// <param name="sdf"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        public static ISignedDistanceField Elongate(this ISignedDistanceField sdf, Vector3 h)
        {
            return new FuncSourceSignedDistanceField((p) => SdOpElongate(p, sdf, h));
        }

        /// <summary>
        /// Double side SDF with thickness
        /// </summary>
        /// <param name="sdf">Source SDF</param>
        /// <param name="thickness">Thickness</param>
        /// <returns></returns>
        public static ISignedDistanceField Onion(this ISignedDistanceField sdf, float thickness)
        {
            return new FuncSourceSignedDistanceField((p) => SdOpOnion(p, sdf, thickness));
        }

        /// <summary>
        /// Invert SDF
        /// </summary>
        /// <param name="sdf"></param>
        /// <returns></returns>
        public static ISignedDistanceField Invert(this ISignedDistanceField sdf)
        {
            return new FuncSourceSignedDistanceField((p) => -sdf.Sample(p));
        }
        
        #endregion

        #region Binary
        
        public static ISignedDistanceField Union(this ISignedDistanceField sdf, ISignedDistanceField otherSdf)
        {
            return new FuncSourceSignedDistanceField((p) => SdOpUnion(p, sdf, otherSdf));
        }

        public static ISignedDistanceField Subtraction(this ISignedDistanceField sdf, ISignedDistanceField otherSdf)
        {
            return new FuncSourceSignedDistanceField((p) => SdOpSubtraction(p, sdf, otherSdf));
        }

        public static ISignedDistanceField Intersection(this ISignedDistanceField sdf, ISignedDistanceField otherSdf)
        {
            return new FuncSourceSignedDistanceField((p) => SdOpIntersection(p, sdf, otherSdf));
        }
        
        public static ISignedDistanceField SmoothUnion(this ISignedDistanceField sdf, ISignedDistanceField otherSdf, float k)
        {
            return new FuncSourceSignedDistanceField((p) => OpSmoothUnion(-sdf.Sample(p), -otherSdf.Sample(p), k));
        }

        public static ISignedDistanceField SmoothSubtraction(this ISignedDistanceField sdf, ISignedDistanceField otherSdf, float k)
        {
            return new FuncSourceSignedDistanceField((p) => OpSmoothSubtraction(-sdf.Sample(p), -otherSdf.Sample(p), k));
        }

        public static ISignedDistanceField SmoothIntersection(this ISignedDistanceField sdf, ISignedDistanceField otherSdf, float k)
        {
            return new FuncSourceSignedDistanceField((p) => OpSmoothIntersection(-sdf.Sample(p), -otherSdf.Sample(p), k));
        }
        
        #endregion
        
        #endregion

        #region Transforming
        
        public static ISignedDistanceField Translate(this ISignedDistanceField sdf, Vector3 vector)
        {
            sdf.Position += vector;
            return sdf;
        }

        public static ISignedDistanceField Rotate(this ISignedDistanceField sdf, Quaternion rotation)
        {
            sdf.Rotation *= rotation;
            return sdf;
        }

        public static ISignedDistanceField Rotate(this ISignedDistanceField sdf, Vector3 eulerAngles)
        {
            return Rotate(sdf, eulerAngles.CreateQuaternionFromEuler());
        }
        
        public static ISignedDistanceField Scale(this ISignedDistanceField sdf, Vector3 scale)
        {
            sdf.Scale = new Vector3(sdf.Scale.X * scale.X, sdf.Scale.Y * scale.Y, sdf.Scale.Z * scale.Z);
            return sdf;
        }
        
        #endregion

        #region Help methods
        
        private static float Length(Vector3 vector)
        {
            return vector.Length();
        }

        private static float Length(Vector2 vector)
        {
            return vector.Length();
        }

        private static Vector3 ConvertPointToLocal(Vector3 worldPoint, Vector3 position, Quaternion orientation, Vector3 scale)
        {
            var p = worldPoint - position;
            var unscaled = new Vector3(p.X / scale.X, p.Y / scale.Y, p.Z / scale.Z);
            return unscaled.Rotate(Quaternion.Inverse(orientation));
        }

        private static float Abs(float v)
        {
            return v < 0f ? -v : v;
        }

        private static float Max(float a, float b)
        {
            return a > b ? a : b;
        }

        private static float Min(float a, float b)
        {
            return a < b ? a : b;
        }

        private static float Dot(Vector2 v0, Vector2 v1)
        {
            return v0.X * v1.X + v0.Y * v1.Y;
        }

        private static float Dot(Vector3 v0, Vector3 v1)
        {
            return v0.X * v1.X + v0.Y * v1.Y + v0.Z * v1.Z;
        }

        private static float Clamp(float value, float min, float max)
        {
            return value.Clamp(min, max);
        }

        private static float Mix(float a, float b, float f)
        {
            return a + f * (b - a);
        }

        private static Vector3 Max(Vector3 v, float s)
        {
            return new Vector3(
                Max(v.X, s),
                Max(v.Y, s),
                Max(v.Z, s));
        }

        private static Vector3 Abs(Vector3 v)
        {
            return new Vector3(
                v.X < 0f ? -v.X : v.X,
                v.Y < 0f ? -v.Y : v.Y,
                v.Z < 0f ? -v.Z : v.Z);
        }
        
        #endregion

        public static ISignedDistanceField ToVoxelSource(this INoise noise)
        {
            return new FuncSourceSignedDistanceField((p) => noise.Sample3D(p.X, p.Y, p.Z));
        }

        public static Vector3 GetNormal(this ISignedDistanceField sdf, Vector3 point)
        {
            const float eps = 0.0001f; // or some other value
            var h = new Vector2(eps, 0);
            return new Vector3(
                sdf.Sample(point + new Vector3(h.X,h.Y,h.Y)) - sdf.Sample(point - new Vector3(h.X,h.Y,h.Y)),
                sdf.Sample(point + new Vector3(h.Y,h.X,h.Y)) - sdf.Sample(point - new Vector3(h.Y,h.X,h.Y)),
                sdf.Sample(point + new Vector3(h.Y,h.Y,h.X)) - sdf.Sample(point - new Vector3(h.Y,h.Y,h.X))
                ).Normalized();

            //p = {h,0,0}
        }

        #region Grammar

        private const string SdfGrammar = "DefinitionList -> Definition DefinitionList || Definition" + "\n"
            + "Definition -> IDENTIFIER = \\{ PRIMITIVE \\}" + "\n"
            + "PRIMITIVE -> Sphere \\( NUMBER \\) || Box \\( NUMBER , NUMBER , NUMBER \\) || Torus \\( NUMBER , NUMBER \\) || Cylinder \\( NUMBER , NUMBER , NUMBER \\) || Cone \\( NUMBER , NUMBER \\)" + "\n"
            + "IDENTIFIER -> [A-Za-z][A-Za-z0-9]*" + "\n"
            + "NUMBER -> -*[0-9]+ || -*[0-9]*\\.[0-9]+" + "\n";

        private static IAstConstructor _sdfParser;

        static Stream GetSdfGrammarStream()
        {
            return DataExtensions.GetEmbeddedResourceStream("ProceduralLGrammarDefinition.txt");
        }
        
        public static IAstConstructor GetSdfGrammarParser()
        {
            if (_sdfParser != null)
                return _sdfParser;
            using var stream = GetSdfGrammarStream();
            _sdfParser = Grammar.Grammar.Create(stream);
            return _sdfParser;
        }

        public static IFieldsProvider Compile(this IAstNode node)
        {
            return new FieldsAstCompiler().Compile(node);
        }

        public static IFieldsProvider Parse(Stream sdfScriptStream)
        {
            return GetSdfGrammarParser().Parse(sdfScriptStream).Compile();
        }

        public static IFieldsProvider Parse(string sdfScript)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(sdfScript)))
                return GetSdfGrammarParser().Parse(stream).Compile();
        }

        #endregion
    }
}