using System.Numerics;

namespace Valkyrie.Procedural.Fields
{
    public interface ISignedDistanceField : IVoxelSource
    {
        Vector3 Position { get; set; }
        Quaternion Rotation { get; set; }
        Vector3 Scale { get; set; }
    }
}