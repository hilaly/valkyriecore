using System;
using System.Collections.Generic;
using System.Text;

namespace Valkyrie.Procedural.Fields
{
    public interface IFieldsProvider
    {
        ISignedDistanceField Construct(string id, params object[] args);
    }

    class FieldsProvider : IFieldsProvider
    {
        private readonly Dictionary<string, Func<object[], ISignedDistanceField>> _ctors =
            new Dictionary<string, Func<object[], ISignedDistanceField>>();

        public void Add(string key, Func<object[], ISignedDistanceField> ctor)
        {
            _ctors.Add(key, ctor);
        }
        
        public ISignedDistanceField Construct(string id, params object[] args)
        {
            if (_ctors.TryGetValue(id, out var ctor))
                return ctor(args);
            
            throw new ArgumentException($"Distance field with id '{id}' is not registered");
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("SDF provider:");
            foreach (var ctor in _ctors)
                sb.Append("  ").AppendLine(ctor.Key);
            return sb.ToString();
        }
    }
}