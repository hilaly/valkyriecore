using System.Numerics;

namespace Valkyrie.Procedural.Fields
{
    public interface IVoxelSource
    {
        float Sample(Vector3 position);
    }
}