using Valkyrie.Math;
using Valkyrie.Procedural.Fields;

namespace Valkyrie.Procedural.Triangulation
{
    public static class Factory
    {
        public static ITriangulator TriangulateCubes(this IVoxelSource voxels, BoundingBox3 bounding, float surface)
        {
            return new Triangulator(bounding, voxels, new MarchingCubes(surface));
        }
        public static ITriangulator TriangulateTetrahedrons(this IVoxelSource voxels, BoundingBox3 bounding, float surface)
        {
            return new Triangulator(bounding, voxels, new MarchingTertrahedron(surface));
        }
    }
}