using System.Collections.Generic;
using System.Numerics;
using Valkyrie.Math;
using Valkyrie.Procedural.Fields;

namespace Valkyrie.Procedural.Triangulation
{
    class Triangulator : ITriangulator
    {
        private readonly List<int> _indices = new List<int>();
        private readonly List<Vector3> _verts = new List<Vector3>();
        private readonly IMarching _marching;

        public IList<Vector3> Vertices => _verts;
        public IList<int> Indices => _indices;
        
        public BoundingBox3 Bounding { get; set; }
        public int Width { get; set; } = 2;
        public int Height { get; set; } = 2;
        public int Length { get; set; } = 2;
        public float Surface
        {
            get => _marching.Surface;
            set => _marching.Surface = value;
        }
        public IVoxelSource Voxels { get; set; }

        public Triangulator(BoundingBox3 bounding, IVoxelSource voxels, IMarching marching)
        {
            Bounding = bounding;
            Voxels = voxels;
            _marching = marching;
        }

        public void Generate()
        {
            //The size of voxel array.
            int width = Width + 1;
            int height = Height + 1;
            int length = Length + 1;

            float[] voxels = new float[width * height * length];

            var min = Bounding.Min;
            var tempDispl = Bounding.Max - min;

            //Fill voxels with values. Im using perlin noise but any method to create voxels will work.
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int z = 0; z < length; z++)
                    {
                        float fx = x / (width - 1.0f);
                        float fy = y / (height - 1.0f);
                        float fz = z / (length - 1.0f);

                        int idx = x + y * width + z * width * height;

                        voxels[idx] = Voxels.Sample(new Vector3(
                            fx * tempDispl.X + min.X,
                            fy * tempDispl.Y + min.Y,
                            fz * tempDispl.Z + min.Z));
                    }
                }
            }

            _indices.Clear();
            _verts.Clear();

            //The mesh produced is not optimal. There is one vert for each index.
            //Would need to weld vertices for better quality mesh.
            _marching.Generate(voxels, width, height, length, _verts, _indices);

            var multiplier = new Vector3(tempDispl.X / (width - 1f), tempDispl.Y / (height - 1f),
                tempDispl.Z / (length - 1f));
            
            for (var i = 0; i < Vertices.Count; i++)
            {
                var temp = Vertices[i];
                temp.X *= multiplier.X;
                temp.Y *= multiplier.Y;
                temp.Z *= multiplier.Z;
                Vertices[i] = min + temp;
            }
        }
    }
}