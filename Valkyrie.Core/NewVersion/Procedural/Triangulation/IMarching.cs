using System.Collections.Generic;
using System.Numerics;

namespace Valkyrie.Procedural.Triangulation
{
    interface IMarching
    {
        float Surface { get; set; }
        
        void Generate(IList<float> voxels, int width, int height, int depth, IList<Vector3> verts, IList<int> indices);
    }
}