using System.Collections.Generic;
using System.Numerics;
using Valkyrie.Math;
using Valkyrie.Procedural.Fields;

namespace Valkyrie.Procedural.Triangulation
{
    public interface ITriangulator
    {
        BoundingBox3 Bounding { get; set; }
        
        int Width { get; set; }
        int Height { get; set; }
        int Length { get; set; }
	    
        float Surface { get; set; }
        
        IVoxelSource Voxels { get; set; }
        
        IList<Vector3> Vertices { get; }
        IList<int> Indices { get; }

        void Generate();
    }
}