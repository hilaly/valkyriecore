using System;
using System.Collections.Generic;
using System.IO;
using Valkyrie.Data;
using Valkyrie.Grammar;

namespace Valkyrie.Scripts
{
    
    class ScriptEngine : IScriptEngine
    {
        public List<ExportedMethod> ExportedFunctions { get; } = new List<ExportedMethod>();
        public Dictionary<string, IScriptType> ExportedTypes { get; } = new Dictionary<string, IScriptType>();
        Dictionary<Type, ExportedType> _typesMap = new Dictionary<Type, ExportedType>();

        public IAstConstructor ScriptParser { get; }
        public ScriptCompiler ScriptCompiler { get; }

        public ScriptEngine()
        {
            using var stream = DataExtensions.GetEmbeddedResourceStream("scriptGrammarDefinition.txt");
            ScriptParser = Grammar.Grammar.Create(stream);
            ScriptCompiler = new ScriptCompiler(this);
            
            this.ExportEmbeddedTypes();
            this.ExportEmbeddedOperators();

            foreach (var key in _typesMap.Keys)
                this.ExportTypeMethods(key);
        }

        public ScriptContextExecutor Executor => new ScriptContextExecutor(this);

        #region Export

        public void Export(ExportedType scriptType)
        {
            ExportedTypes.Add(scriptType.GetFullName(), scriptType);
            _typesMap.Add(scriptType.NativeType, scriptType);
            
            //Console.WriteLine($"Type '{scriptType.GetFullName()}' exported");
        }
        
        public void Export(ExportedMethod method)
        {
            ExportedFunctions.Add(method);
            //Console.WriteLine($"Method '{method.GetMethodSign()}' exported");
        }

        public string GetScriptTypeName(Type type)
        {
            return GetScriptType(type)?.GetFullName() ?? type.Name;
        }

        public ExportedType GetScriptType(Type type)
        {
            return _typesMap.TryGetValue(type, out var scriptType) ? scriptType : null;
        }

        #endregion

        public IScript Compile(Stream stream)
        {
            Console.WriteLine($"Script: {stream.ReadAllText()}");

            var ast = ScriptParser.Parse(stream);
            var program = ScriptCompiler.Compile(ast);
            return program;
        }

        public IScript Compile(IScript sourceScript, Stream stream)
        {
            Console.WriteLine($"Compiled: {sourceScript}\nScript: {stream.ReadAllText()}");

            var ast = ScriptParser.Parse(stream);
            var program = ScriptCompiler.Compile(ast, (ScriptProgram)sourceScript);
            return program;
        }
    }
}