namespace Valkyrie.Scripts
{
    class Register
    {
        private readonly object[] _regs = new object[DefinedConstants.Scripts.RegistersCount];
        public object this[int index]
        {
            get => _regs[index];
            set => _regs[index] = value;
        }

        public object[] GetArgs(int start, int count)
        {
            var res = new object[count];
            for (var i = 0; i < count; ++i)
                res[i] = _regs[start + i];
            return res;
        }
    }
}