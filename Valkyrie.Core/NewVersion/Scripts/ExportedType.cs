using System;
using System.Collections.Generic;

namespace Valkyrie.Scripts
{
    class ExportedType : IScriptType
    {
        private readonly ScriptEngine _engine;
        
        public string Name { get; }
        public string Namespace { get; }
        
        public Type NativeType { get; }

        public ExportedType(string name, string ns, Type nativeType, ScriptEngine engine)
        {
            _engine = engine;
            Name = name;
            Namespace = ns;
            NativeType = nativeType;
        }

        public List<IScriptMethod> Methods => throw new NotImplementedException();
    }
}