namespace Valkyrie.Scripts
{
    public interface IScriptMethod
    {
        string Name { get; }
        IScriptType ResultType { get; }
        IScriptType[] ArgsType { get; }
        
        object Invoke(object[] args, int argsCount);
    }
}