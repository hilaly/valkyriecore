#lexer

'(?<value>[^']*)' -> STRING
"(?<value>[^"]*)" -> STRING
(true|false) -> BOOL
[ \n\r\t]+ -> escape
-?([\d]+(\.[\d]+)?|\.[\d]+) -> NUMBER
[A-Za-z][A-Za-z0-9]* -> IDENTIFIER

(\*|\/) -> MUL_OP
(\+|\-) -> ADD_OP
(<=|>=|==|!=|<|>) -> COMP_OP
= -> ASSIGN_OP

[\{\}\(\),\.;]

#parser

<root> -> <method_define>+ | LIST_STATEMENT | <return_expr> | <expr>

LIST_STATEMENT -> STATEMENT LIST_STATEMENT | STATEMENT

STATEMENT -> <return_expr> ";" | <if_statement> | <expr> ";"

<if_statement> -> "if" "\(" <expr> "\)" STATEMENT "else" STATEMENT | "if" "\(" <expr> "\)" STATEMENT

<return_expr> -> "return" <expr> | "return"

<expr> -> <assign_expr>

<assign_expr> -> <var_expr> ASSIGN_OP <assign_expr> | <comp_expr>

<comp_expr> -> <add_expr> COMP_OP <comp_expr> | <add_expr>
<add_expr> -> <mul_expr> ADD_OP <add_expr> | <mul_expr>
<mul_expr> -> <single_expr> MUL_OP <mul_expr> | <single_expr>

<single_expr> -> <const_expr> | <method_expr> | <var_expr>

<var_expr> -> <identifier>
<identifier> -> IDENTIFIER "\." <identifier> | IDENTIFIER
<const_expr> -> STRING | NUMBER | BOOL

<method_expr> -> <identifier> "\(" <method_args_list> "\)" | <identifier> "\(" "\)"
<method_args_list> -> <expr> "\," <method_args_list> | <expr>

<method_define> -> <identifier> IDENTIFIER "\(" <method_arg_def_list> "\)" "\{" [LIST_STATEMENT] "\}"
<method_define> -> <identifier> IDENTIFIER "\(" "\)" "\{" [LIST_STATEMENT] "\}"
<method_arg_def_list> -> <method_arg_def> "\," <method_arg_def_list> | <method_arg_def>
<method_arg_def> -> <identifier> IDENTIFIER