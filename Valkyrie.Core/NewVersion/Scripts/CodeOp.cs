namespace Valkyrie.Scripts
{
    struct CodeOp
    {
        public ILOpCodes Code;
        public object[] Args;
    }
}