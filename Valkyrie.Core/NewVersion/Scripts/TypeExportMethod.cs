using System.Linq;
using System.Reflection;

namespace Valkyrie.Scripts
{
    class TypeExportMethod : ExportedMethod
    {
        private readonly MethodInfo _methodInfo;
        private readonly IScriptEngine _engine;

        public TypeExportMethod(MethodInfo methodInfo, string name, ScriptEngine engine)
            : base(engine.GetScriptType(methodInfo.ReturnType), methodInfo.GetParameters().Select(x => x.ParameterType).ToArray(), name, engine)
        {
            _methodInfo = methodInfo;
            _engine = engine;
        }

        public override object Invoke(object[] args, int argsCount)
        {
            if (_methodInfo.IsStatic)
                return _methodInfo.Invoke(null, args);
            else
                return _methodInfo.Invoke(args[0], args.ToList().GetRange(1, argsCount - 1).ToArray());
        }
    }
}