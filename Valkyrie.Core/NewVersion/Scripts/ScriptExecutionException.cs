using System;

namespace Valkyrie.Scripts
{
    public class ScriptExecutionException : Exception
    {
        public ScriptExecutionException(string s)
            : base(s)
        {
        }
    }
}