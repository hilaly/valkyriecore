using System.Collections.Generic;

namespace Valkyrie.Scripts
{
    public interface IScriptType
    {
        string Name { get; }
        string Namespace { get; }
        
        List<IScriptMethod> Methods { get; }
    }
}