using System;

namespace Valkyrie.Scripts
{
    public class ScriptCompileException : Exception
    {
        public ScriptCompileException(string s) : base(s)
        {
        }
    }
}