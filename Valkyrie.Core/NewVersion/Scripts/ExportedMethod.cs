using System;
using System.Linq;

namespace Valkyrie.Scripts
{
    abstract class ExportedMethod : IScriptMethod
    {
        public abstract object Invoke(object[] args, int argsCount);

        public string Name { get; }

        public IScriptType ResultType { get; }
        public IScriptType[] ArgsType { get; }

        protected ExportedMethod(IScriptType resultType, Type[] argsType, string name, ScriptEngine engine)
        {
            ResultType = resultType;
            ArgsType = argsType.Select(engine.GetScriptType).ToArray();
            Name = name;
        }

        public override string ToString()
        {
            return this.GetMethodSign();
        }
    }
}