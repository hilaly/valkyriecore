using System;
using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Scripts
{
    class ScriptContextExecutor
    {
        private readonly ScriptEngine _scriptEngine;

        public ScriptContextExecutor(ScriptEngine scriptEngine)
        {
            _scriptEngine = scriptEngine;
        }

        public ExecutionContext Execute(ExecutionContext context)
        {
            var op = context.byteCode[context.CurrentOperator];
            switch (op.Code)
            {
                case ILOpCodes.nop:
                {
                    break;
                }
                case ILOpCodes.err:
                {
                    var arg = op.Args != null ? string.Join(" ", op.Args) : string.Empty;
                    throw new ScriptExecutionException(arg);
                }
                case ILOpCodes.ldcst:
                {
                    context.Register[(int) op.Args[0]] = op.Args[1];
                    break;
                }
                case ILOpCodes.stloc:
                {
                    context.Locals[(string) op.Args[1]] = context.Register[(int) op.Args[0]];
                    break;
                }
                case ILOpCodes.ldloc:
                {
                    context.Register[(int) op.Args[0]] = context.Locals[(string) op.Args[1]];
                    break;
                }
                case ILOpCodes.calln:
                {
                    var argsCount = (int)op.Args[0];
                    var args = context.Register.GetArgs(1, argsCount);
                    var methodIndex = (int)op.Args[1];
                    if (methodIndex < 0)
                    {
                        var methodSign =
                            $"{(string)op.Args[2]}({string.Join(",", args.Select(x => _scriptEngine.GetScriptTypeName(x.GetType())))})";
                        op.Args[1] = methodIndex = context.Functions.FindIndex(x => x.GetMethodSign() == methodSign);
                        if(methodIndex < 0)
                            throw new ScriptExecutionException($"Couldn't find function with signature: {methodSign}");
                    }

                    var function = context.Functions[methodIndex];
                    var result = function.Invoke(args, argsCount);
                    context.Register[0] = result;
                    break;
                }
                case ILOpCodes.jmp:
                {
                    context.CurrentOperator = (int) op.Args[0];
                    return context;
                }
                case ILOpCodes.call:
                {
                    var callCode = context.CurrentOperator;
                    var retCode = callCode;
                    
                    var argsCount = (int)op.Args[0];
                    var methodIndex = (int)op.Args[1];
                    var methodName = (string) op.Args[2];
                    var methodSign = (string) op.Args[3];

                    context.CallStack.Push(new ExecutionContext.CallInfo()
                    {
                        MethodName = methodName,
                        MethodSign = methodSign,
                        RetCodeIndex = retCode,
                        Locals = context.Locals
                    });

                    context.Locals = new Dictionary<string, object>();
                    context.CurrentOperator = methodIndex;
                    return context;
                }
                case ILOpCodes.ret:
                {
                    if (context.CallStack.Count > 0)
                    {
                        var info = context.CallStack.Pop();
                        context.CurrentOperator = info.RetCodeIndex + 1;
                        context.Locals = info.Locals;
                    }
                    else
                        context.CurrentOperator = context.byteCode.Count;
                    return context;
                }
                case ILOpCodes.beq:
                {
                    var jmpIndex = context.Register[0] is bool b && b ? 0 : 1;
                    context.CurrentOperator = (int) op.Args[jmpIndex];
                    return context;
                }
                default:
                    throw new NotImplementedException($"Unimplemented OpCode={op.Code}");
            }
            
            context.CurrentOperator++;
            return context;
        }

        public object Run(ScriptProgram program)
        {
            var context = new ExecutionContext(program, _scriptEngine.ExportedFunctions);
            while (context.CurrentOperator < context.byteCode.Count)
            {
                Execute(context);
            }

            return context.Register[0];
        }

        public object Run(ScriptProgram program, ScriptProgram call)
        {
            var context = new ExecutionContext(program, _scriptEngine.ExportedFunctions);
            context.Load(call);
            
            while (context.CurrentOperator < context.byteCode.Count)
            {
                Execute(context);
            }

            return context.Register[0];
        }

        public object Run(ScriptProgram program, string method, object[] args)
        {
            var opIndex = program.byteCode.Count;
            var sign =
                $"{method}({string.Join(",", args.Select(a => this._scriptEngine.GetScriptTypeName(a.GetType())))})";
            var methodPair = program.methodsTable.First(x => x.Key.GetMethodSign() == sign);
            program.Call(args.Length, methodPair.Value, method, sign);


            var context = new ExecutionContext(program, _scriptEngine.ExportedFunctions)
            {
                CurrentOperator = opIndex
            };
            for (var i = 0; i < args.Length; ++i)
                context.Register[i + 1] = args[i];
            
            while (context.CurrentOperator < context.byteCode.Count)
            {
                Execute(context);
            }

            return context.Register[0];
        }
    }
}