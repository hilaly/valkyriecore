using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Valkyrie.Data;

namespace Valkyrie.Scripts
{
    public static class ScriptsUtils
    {
        internal static readonly Dictionary<string, string> OperatorMethodMap = new Dictionary<string, string>()
        {
            { "+", "Add"},
            { "-", "Rem"},
            { "*", "Mul"},
            { "/", "Del"},
            
            { "<", "Less"},
            { ">", "More"},
            { "<=", "LessOrEqual"},
            { ">=", "MoreOrEqual"},
            { "==", "Equal"},
            { "!=", "NotEqual"},
        }; 
        private static string GetLocalVarName(int index)
        {
            return $"__localVar{index}";
        }

        #region Compilation to byte code

        internal static IScriptType GetLocalType(this ScriptProgram program, string varName)
        {
            return program.LocalTypes.TryGetValue(varName, out var result)
                ? result
                : null;
        }

        internal static IScriptType GetLocalType(this ScriptProgram program, int localVarIndex)
        {
            return GetLocalType(program, GetLocalVarName(localVarIndex));
        }

        internal static int SaveRegisterToLocal(this ScriptProgram program, int registerNum, ref int localVarIndex, IScriptType varType)
        {
            SaveRegisterToLocal(program, registerNum, GetLocalVarName(localVarIndex), varType);
            return localVarIndex++;
        }
        internal static void SaveRegisterToLocal(this ScriptProgram program, int registerNum, string localVarName, IScriptType varType)
        {
            program.LocalTypes[localVarName] = varType;
            program.byteCode.Add(new CodeOp()
                {Code = ILOpCodes.stloc, Args = new object[] {registerNum, localVarName, varType.GetFullName()}});
        }

        internal static void LoadLocalToRegister(this ScriptProgram program, int registerNum, string localVarName)
        {
            program.byteCode.Add(new CodeOp()
                {Code = ILOpCodes.ldloc, Args = new object[] {registerNum, localVarName}});
        }

        internal static void LoadLocalToRegister(this ScriptProgram program, int registerNum, int localVarIndex)
        {
            LoadLocalToRegister(program, registerNum, GetLocalVarName(localVarIndex));
        }

        internal static void CallNative(this ScriptProgram program, int argsCount, int methodIndex, string methodName, string methodSign)
        {
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.calln, Args = new object[] {argsCount, methodIndex, methodName, methodSign}});
        }
        
        internal static void Call(this ScriptProgram program, int argsCount, int methodAddress, string methodName, string methodSign)
        {
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.call, Args = new object[] {argsCount, methodAddress, methodName, methodSign}});
        }

        internal static void LoadConst(this ScriptProgram program, int registerNum, object value)
        {
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.ldcst, Args = new object[] {registerNum, value}});
        }

        internal static void SetNop(this ScriptProgram program, params object[] args)
        {
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.nop, Args = args});
        }

        internal static void SetErr(this ScriptProgram program, params object[] args)
        {
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.err, Args = args});
        }

        internal static void SetReturn(this ScriptProgram program)
        {
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.ret});
        }

        internal static int SetJump(this ScriptProgram program, int opIndex, string comment)
        {
            var res = program.byteCode.Count;
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.jmp, Args = new object[] {opIndex, comment}});
            return res;
        }

        internal static int SetIf(this ScriptProgram program)
        {
            var res = program.byteCode.Count;
            program.byteCode.Add(new CodeOp() {Code = ILOpCodes.beq, Args = new object[] {res + 1, -1}});
            program.SetNop("START IF TRUE BRANCH");
            return res;
        }

        #endregion

        #region Methods export
        
        class FuncExportedMethod<TResult> : ExportedMethod
        {
            private readonly Func<TResult> _call;

            public FuncExportedMethod(Func<TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new Type[0], name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call();
            }
        }

        class FuncExportedMethod<T0,TResult> : ExportedMethod
        {
            private readonly Func<T0, TResult> _call;

            public FuncExportedMethod(Func<T0, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0]);
            }
        }

        class FuncExportedMethod<T0,T1,TResult> : ExportedMethod
        {
            private readonly Func<T0,T1, TResult> _call;

            public FuncExportedMethod(Func<T0,T1, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0),typeof(T1)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0], (T1) args[1]);
            }
        }

        class FuncExportedMethod<T0,T1,T2,TResult> : ExportedMethod
        {
            private readonly Func<T0,T1,T2, TResult> _call;

            public FuncExportedMethod(Func<T0,T1,T2, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0),typeof(T1),typeof(T2)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0], (T1) args[1], (T2) args[2]);
            }
        }

        class FuncExportedMethod<T0,T1,T2,T3,TResult> : ExportedMethod
        {
            private readonly Func<T0,T1,T2,T3, TResult> _call;

            public FuncExportedMethod(Func<T0,T1,T2,T3, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3]);
            }
        }

        class FuncExportedMethod<T0,T1,T2,T3,T4,TResult> : ExportedMethod
        {
            private readonly Func<T0,T1,T2,T3,T4, TResult> _call;

            public FuncExportedMethod(Func<T0,T1,T2,T3,T4, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3),typeof(T4)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3], (T4) args[4]);
            }
        }

        class FuncExportedMethod<T0,T1,T2,T3,T4,T5,TResult> : ExportedMethod
        {
            private readonly Func<T0,T1,T2,T3,T4,T5, TResult> _call;

            public FuncExportedMethod(Func<T0,T1,T2,T3,T4,T5, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3),typeof(T4),typeof(T5)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3], (T4) args[4], (T5) args[5]);
            }
        }

        class FuncExportedMethod<T0,T1,T2,T3,T4,T5,T6,TResult> : ExportedMethod
        {
            private readonly Func<T0,T1,T2,T3,T4,T5,T6, TResult> _call;

            public FuncExportedMethod(Func<T0,T1,T2,T3,T4,T5,T6, TResult> call, string name, ScriptEngine engine)
                : base(engine.GetScriptType(typeof(TResult)), new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3),typeof(T4),typeof(T5),typeof(T6)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                return _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3], (T4) args[4], (T5) args[5], (T6) args[6]);
            }
        }

        public static IScriptMethod Export<TResult>(this IScriptEngine engine, string name,
             Func<TResult> call)
        {
            var result = new FuncExportedMethod<TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, TResult>(this IScriptEngine engine, string name,
             Func<T0, TResult> call)
        {
            var result = new FuncExportedMethod<T0, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, TResult>(this IScriptEngine engine, string name,
             Func<T0, T1, TResult> call)
        {
            var result = new FuncExportedMethod<T0, T1, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, TResult>(this IScriptEngine engine, string name, Func<T0, T1, T2, TResult> call)
        {
            var result = new FuncExportedMethod<T0, T1, T2, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, TResult>(this IScriptEngine engine, string name,
             Func<T0, T1, T2, T3, TResult> call)
        {
            var result = new FuncExportedMethod<T0, T1, T2, T3, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, T4, TResult>(this IScriptEngine engine, string name,
             Func<T0, T1, T2, T3, T4, TResult> call)
        {
            var result = new FuncExportedMethod<T0, T1, T2, T3, T4, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, T4, T5, TResult>(this IScriptEngine engine, string name,
             Func<T0, T1, T2, T3, T4, T5, TResult> call)
        {
            var result = new FuncExportedMethod<T0, T1, T2, T3, T4, T5, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, T4, T5, T6, TResult>(this IScriptEngine engine, string name,
            Func<T0, T1, T2, T3, T4, T5, T6, TResult> call)
        {
            var result = new FuncExportedMethod<T0, T1, T2, T3, T4, T5, T6, TResult>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        
        class ActionExportedMethod : ExportedMethod
        {
            private readonly Action _call;

            public ActionExportedMethod(Action call, string name, ScriptEngine engine)
                : base(null, new Type[0], name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call();
                return null;
            }
        }
        
        class ActionExportedMethod<T0> : ExportedMethod
        {
            private readonly Action<T0> _call;

            public ActionExportedMethod(Action<T0> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0]);
                return null;
            }
        }
        
        class ActionExportedMethod<T0,T1> : ExportedMethod
        {
            private readonly Action<T0,T1> _call;

            public ActionExportedMethod(Action<T0,T1> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0),typeof(T1)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0], (T1) args[1]);
                return null;
            }
        }
        
        class ActionExportedMethod<T0,T1,T2> : ExportedMethod
        {
            private readonly Action<T0,T1,T2> _call;

            public ActionExportedMethod(Action<T0,T1,T2> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0),typeof(T1),typeof(T2)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0], (T1) args[1], (T2) args[2]);
                return null;
            }
        }
        
        class ActionExportedMethod<T0,T1,T2,T3> : ExportedMethod
        {
            private readonly Action<T0,T1,T2,T3> _call;

            public ActionExportedMethod(Action<T0,T1,T2,T3> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3]);
                return null;
            }
        }
        
        class ActionExportedMethod<T0,T1,T2,T3,T4> : ExportedMethod
        {
            private readonly Action<T0,T1,T2,T3,T4> _call;

            public ActionExportedMethod(Action<T0,T1,T2,T3,T4> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3),typeof(T4)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3], (T4) args[4]);
                return null;
            }
        }
        
        class ActionExportedMethod<T0,T1,T2,T3,T4,T5> : ExportedMethod
        {
            private readonly Action<T0,T1,T2,T3,T4,T5> _call;

            public ActionExportedMethod(Action<T0,T1,T2,T3,T4,T5> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3),typeof(T4),typeof(T5)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3], (T4) args[4], (T5) args[5]);
                return null;
            }
        }
        
        class ActionExportedMethod<T0,T1,T2,T3,T4,T5,T6> : ExportedMethod
        {
            private readonly Action<T0,T1,T2,T3,T4,T5,T6> _call;

            public ActionExportedMethod(Action<T0,T1,T2,T3,T4,T5,T6> call, string name, ScriptEngine engine)
                : base(null, new []{typeof(T0),typeof(T1),typeof(T2),typeof(T3),typeof(T4),typeof(T5),typeof(T6)}, name, engine)
            {
                _call = call;
            }

            public override object Invoke(object[] args, int argsCount)
            {
                _call((T0) args[0], (T1) args[1], (T2) args[2], (T3) args[3], (T4) args[4], (T5) args[5], (T6) args[6]);
                return null;
            }
        }
        
        public static IScriptMethod Export(this IScriptEngine engine, string name,
             Action call)
        {
            var result = new ActionExportedMethod(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0>(this IScriptEngine engine, string name,
            Action<T0> call)
        {
            var result = new ActionExportedMethod<T0>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1>(this IScriptEngine engine, string name,
            Action<T0, T1> call)
        {
            var result = new ActionExportedMethod<T0, T1>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2>(this IScriptEngine engine, string name, Action<T0, T1, T2> call)
        {
            var result = new ActionExportedMethod<T0, T1, T2>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3>(this IScriptEngine engine, string name,
            Action<T0, T1, T2, T3> call)
        {
            var result = new ActionExportedMethod<T0, T1, T2, T3>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, T4>(this IScriptEngine engine, string name,
            Action<T0, T1, T2, T3, T4> call)
        {
            var result = new ActionExportedMethod<T0, T1, T2, T3, T4>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, T4, T5>(this IScriptEngine engine, string name,
            Action<T0, T1, T2, T3, T4, T5> call)
        {
            var result = new ActionExportedMethod<T0, T1, T2, T3, T4, T5>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export<T0, T1, T2, T3, T4, T5, T6>(this IScriptEngine engine, string name,
            Action<T0, T1, T2, T3, T4, T5, T6> call)
        {
            var result = new ActionExportedMethod<T0, T1, T2, T3, T4, T5, T6>(call, name, (ScriptEngine)engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static IScriptMethod Export(this IScriptEngine engine, MethodInfo methodInfo, string name)
        {
            var result = new TypeExportMethod(methodInfo, name, (ScriptEngine) engine);
            ((ScriptEngine)engine).Export(result);
            return result;
        }

        public static string GetMethodSign(this IScriptMethod method)
        {
            return $"{method.Name}({string.Join(",", method.ArgsType.Select(x => x.GetFullName()))})";
        }
        
        #endregion

        #region Export types

        public static string GetFullName(this IScriptType type)
        {
            return type.Namespace.NotNullOrEmpty() ? $"{type.Namespace}.{type.Name}" : type.Name;
        }

        public static void Export(this IScriptEngine engine, Type type, string name = null, string nameSpace = null)
        {
            if (name.IsNullOrEmpty())
                name = type.Name;
            ((ScriptEngine)engine).Export(new ExportedType(name, nameSpace, type, (ScriptEngine) engine));
        }

        public static void ExportTypeWithMethods<T>(this IScriptEngine engine, string name = null,
            string nameSpace = null)
        {
            ExportTypeWithMethods(engine, typeof(T), name, nameSpace);
        }

        public static void ExportTypeWithMethods(this IScriptEngine engine, Type type, string name = null,
            string nameSpace = null)
        {
            Export(engine, type, name, nameSpace);
            ExportTypeMethods(engine, type);
        }

        public static void Export<T>(this IScriptEngine engine, string name = null, string nameSpace = null)
        {
            Export(engine, typeof(T), name, nameSpace);
        }

        internal static void ExportTypeMethods(this IScriptEngine e, Type type)
        {
            var engine = (ScriptEngine) e;
            var typeScriptType = engine.GetScriptType(type);
            var allMethods = type.GetMethods();
            foreach (var methodInfo in allMethods)
            {
                var resultType = engine.GetScriptType(methodInfo.ReturnType);
                var argsTypes = methodInfo.GetParameters().Select(x => engine.GetScriptType(x.ParameterType)).ToList();
                if (resultType == null || argsTypes.Any(x => x == null))
                    continue;
                engine.Export(methodInfo, $"{typeScriptType.GetFullName()}.{methodInfo.Name}");
            }
        }

        #endregion

        #region Extensions

        public static object Eval(this IScriptEngine engine, string code)
        {
            using var stream = code.ToStream();
            return engine.Eval(engine.Compile(stream));
        }

        public static object Eval(this IScriptEngine engine, IScript script)
        {
            var e = (ScriptEngine) engine;
            return e.Executor.Run((ScriptProgram) script);
        }

        static object Run(this IScriptEngine engine, IScript script, string code)
        {
            ScriptProgram merged;
            using( var stream = code.ToStream())
                merged = (ScriptProgram) engine.Compile((ScriptProgram) script, stream);
            
            return ((ScriptEngine) engine).Executor.Run(merged);
        }

        public static object Run(this IScriptEngine engine, IScript script, string method, params object[] args)
        {
            var merged = new ScriptProgram();
            merged.Load((ScriptProgram) script);
            return ((ScriptEngine) engine).Executor.Run(merged, method, args);
        }

        #endregion

        #region Scripts init

        internal static void ExportEmbeddedTypes(this IScriptEngine engine)
        {
            Export(engine, typeof(void), "void");
            engine.Export<string>("string");
            engine.Export<float>("float");
            engine.Export<bool>("bool");
            engine.Export<int>("int");
        }

        internal static void ExportEmbeddedOperators(this IScriptEngine engine)
        {
            engine.Export<string, string, string>("Add", (s, s1) => s + s1);
            
            engine.Export<string, string, bool>("Equal", (s, s1) => s == s1);
            engine.Export<string, string, bool>("NotEqual", (s, s1) => s != s1);

            engine.Export<float, float, float>("Add", (f, f1) => f + f1);
            engine.Export<float, float, float>("Rem", (f, f1) => f - f1);
            engine.Export<float, float, float>("Mul", (f, f1) => f * f1);
            engine.Export<float, float, float>("Del", (f, f1) => f / f1);
            
            engine.Export<float, float, bool>("Less", (f, f1) => f < f1);
            engine.Export<float, float, bool>("More", (f, f1) => f > f1);
            engine.Export<float, float, bool>("LessOrEqual", (f, f1) => f <= f1);
            engine.Export<float, float, bool>("MoreOrEqual", (f, f1) => f >= f1);
            
            engine.Export<float, float, bool>("Equal", (f, f1) => f == f1);
            engine.Export<float, float, bool>("NotEqual", (f, f1) => f != f1);
            
            engine.Export<bool, bool, bool>("Equal", (f, f1) => f == f1);
            engine.Export<bool, bool, bool>("NotEqual", (f, f1) => f != f1);
        }

        #endregion
    }
}