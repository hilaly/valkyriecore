namespace Valkyrie.Scripts
{
    public class Interpreter
    {
        private readonly ScriptEngine _engine;

        public Interpreter()
        {
            _engine = new ScriptEngine();
        }

        public object Eval(string code)
        {
            return _engine.Eval(code);
        }
    }
}