using System.Collections.Generic;

namespace Valkyrie.Scripts
{
    class ExecutionContext
    {
        public class CallInfo
        {
            public int RetCodeIndex;
            public string MethodSign;
            public string MethodName;
            public Dictionary<string, object> Locals;
        }
        public readonly List<ExportedMethod> Functions;
        private readonly ScriptProgram Program;
        public int CurrentOperator;
        public Stack<CallInfo> CallStack = new Stack<CallInfo>();
        public Register Register = new Register();
        public Dictionary<string, object> Locals = new Dictionary<string, object>();
        public readonly List<CodeOp> byteCode;

        public ExecutionContext(ScriptProgram program, List<ExportedMethod> functions)
        {
            this.Program = program;
            Functions = functions;
            CurrentOperator = 0;
            byteCode = new List<CodeOp>(Program.byteCode);
        }

        public void Load(ScriptProgram script)
        {
            byteCode.AddRange(script.byteCode);
        }
    }
}