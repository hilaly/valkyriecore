using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Valkyrie.Grammar;

namespace Valkyrie.Scripts
{
    class ScriptCompiler
    {
        private readonly ScriptEngine _engine;

        public ScriptCompiler(ScriptEngine engine)
        {
            _engine = engine;
        }

        public ScriptProgram Compile(IAstNode ast, ScriptProgram scriptProgram = null)
        {
            Console.WriteLine(ast);
            var result = new ScriptProgram();
            if(scriptProgram != null)
                result.Load(scriptProgram);
            int localVarIndex = 0;
            var r = Build(result, ast, ref localVarIndex);
            Console.WriteLine($"Result={r?.GetFullName()}");
            Console.WriteLine(result);
            return result;
        }
        
        private IScriptType Build(ScriptProgram program, IAstNode node, ref int localVarIndex)
        {
            IScriptType LoadPropertyValueToZeroRegister(string propName, IScriptType resultType, ref int localVarIndex)
            {
                var tempIndex = program.SaveRegisterToLocal(0, ref localVarIndex, resultType);
                program.LoadLocalToRegister(1, tempIndex);
                            
                            
                var localPropName = propName;
                var methodName = $"{resultType.GetFullName()}.get_{localPropName}";
                var sign = $"{methodName}()";
                var methodIndex = _engine.ExportedFunctions.FindIndex(x => x.GetMethodSign() == sign);
                if (methodIndex >= 0)
                {
                    program.CallNative(1, methodIndex, methodName, sign);
                    resultType = _engine.ExportedFunctions[methodIndex].ResultType;
                }
                else
                {
                    program.SetErr(node.Name, "Method Not Found", sign);
                    resultType = null;
                }

                return resultType;
            }
            
            
            var nodes = node.GetChilds();
            switch (node.Name)
            {
                case "<method_define>":
                {
                    var jmpIndex = program.SetJump(-1, string.Empty);
                    
                    var method = ExtractMethod(program, node, ref localVarIndex);
                    
                    program.byteCode[jmpIndex].Args[0] = program.byteCode.Count;
                    program.byteCode[jmpIndex].Args[1] =
                        $"SKIP '{method.ResultType.GetFullName()} {method.GetMethodSign()}'";
                    program.SetNop($"END '{method.ResultType.GetFullName()} {method.GetMethodSign()}'");

                    return null;
                }
                case "<method_list>":
                case "<generated-list-<method_define>>":
                {
                    foreach (var astNode in nodes)
                        Build(program, astNode, ref localVarIndex);
                    return null;
                }
                case "LIST_STATEMENT":
                {
                    var statements = node.FindAllInList("LIST_STATEMENT", "STATEMENT");
                    IScriptType res = null;
                    for (var i = 0; i < statements.Count; ++i)
                        res = Build(program, statements[i], ref localVarIndex);
                    return res;
                }
                case "<root>":
                case "STATEMENT":
                case "<expr>":
                case "<single_expr>":
                {
                    return Build(program, nodes[0], ref localVarIndex);
                }
                case "<const_expr>":
                {
                    switch (nodes[0].Name)
                    {
                        case "STRING":
                            program.LoadConst(0, nodes[0].GetString().Trim('\'', '"'));
                            return _engine.GetScriptType(typeof(string));
                        case "NUMBER":
                            program.LoadConst(0, nodes[0].GetFloat());
                            return _engine.GetScriptType(typeof(float));
                        case "BOOL":
                            program.LoadConst(0, nodes[0].GetBool());
                            return _engine.GetScriptType(typeof(bool));
                        default:
                            throw new ScriptCompileException($"Unknown const type: {nodes[0].Name}");
                    }
                }
                case "<comp_expr>":
                case "<add_expr>":
                case "<mul_expr>":
                {
                    var allChild = new List<IAstNode>();
                    var cn = node;
                    while (true)
                    {
                        var sn = cn;
                        foreach (var child in cn.GetChilds())
                        {
                            if (child.Name == node.Name)
                            {
                                cn = child;
                                break;
                            }
                            allChild.Add(child);
                        }
                        if(sn == cn)
                            break;
                    }

                    if (allChild.Count % 2 != 1) throw new Exception($"Not valid child in {node.Name}");

                    int indexLeft = -1;
                    IScriptType leftType = null;
                    for (var i = 0; i < allChild.Count; i+=2)
                    {
                        var rightType = Build(program, allChild[i], ref localVarIndex);
                        //do op
                        if (leftType != null)
                        {
                            if(!ScriptsUtils.OperatorMethodMap.TryGetValue(allChild[i-1].GetString(), out var methodName))
                                throw new ScriptCompileException($"Unimplemented operator '{allChild[i-1].GetString()}'");
                            
                            var sign = $"{methodName}({leftType.GetFullName()},{rightType.GetFullName()})";
                            var methodIndex = _engine.ExportedFunctions.FindIndex(x => x.GetMethodSign() == sign);
                            if (methodIndex < 0)
                            {
                                program.SetErr(node.Name, "Method Not Found", sign);
                                return null;
                            }

                            int indexRight = program.SaveRegisterToLocal(0, ref localVarIndex, rightType);
                            
                            program.LoadLocalToRegister(1, indexLeft);
                            program.LoadLocalToRegister(2, indexRight);
                            program.CallNative(2, methodIndex, methodName, sign);
                            leftType = _engine.ExportedFunctions[methodIndex].ResultType;
                        }
                        else
                            leftType = rightType;

                        if (i < allChild.Count - 1)
                            indexLeft = program.SaveRegisterToLocal(0, ref localVarIndex, leftType);
                    }

                    return leftType;
                }
                case "<method_expr>":
                {
                    int argsCount = 0;
                    var methodName = nodes[0].ConvertTreeToString("");
                    List<int> argsIndices = new List<int>();
                    if (!IsLocal(program, nodes[0]))
                    {
                        var fullName = methodName;
                        var split = fullName.Split('.');
                        var localVarName = split[0];
                        var resultType = program.GetLocalType(localVarName);
                        program.LoadLocalToRegister(0, localVarName);
                        for (var i = 1; i < split.Length - 1; ++i)
                            resultType = LoadPropertyValueToZeroRegister(split[i], resultType, ref localVarIndex);
                        argsIndices.Add(program.SaveRegisterToLocal(0, ref localVarIndex, resultType));
                        var localMethodName = split[split.Length - 1];
                        methodName = $"{resultType.GetFullName()}.{localMethodName}";
                        argsCount += 1;
                    }
                    var sb = new StringBuilder(methodName);
                    sb.Append("(");
                    if (nodes.Count > 3)
                    {
                        var exprList = CollectExpr(nodes[2], "<expr>", "<method_args_list>");
                        argsCount += exprList.Count;
                        List<IScriptType> argsTypes = new List<IScriptType>(exprList.Count);
                        foreach (var expr in exprList)
                        {
                            var argType = Build(program, expr, ref localVarIndex);
                            argsTypes.Add(argType);
                            argsIndices.Add(program.SaveRegisterToLocal(0, ref localVarIndex, argType));
                        }

                        sb.Append(string.Join(",", argsTypes.Select(x => x.GetFullName())));
                    }
                    sb.Append(")");

                    for (var i = 0; i < argsIndices.Count; ++i)
                        program.LoadLocalToRegister(i + 1, argsIndices[i]);

                    var sign = sb.ToString();
                    var methodIndex = _engine.ExportedFunctions.FindIndex(x => x.GetMethodSign() == sign);
                    if (methodIndex >= 0)
                    {
                        program.CallNative(argsCount, methodIndex, methodName, sign);
                        return _engine.ExportedFunctions[methodIndex].ResultType;
                    }

                    var method = program.methodsTable.FirstOrDefault(x => x.Key.GetMethodSign() == sign);
                    if (method.Key != null)
                    {
                        program.Call(argsCount, method.Value, methodName, sign);
                        return method.Key.ResultType;
                    }

                    program.SetErr(node.Name, "Method Not Found", sign);
                    return null;
                }
                case "<return_expr>":
                {
                    IScriptType resultType = null;
                    if (nodes.Count > 1)
                        resultType = Build(program, nodes[1], ref localVarIndex);
                    program.SetReturn();
                    return resultType;
                }
                case "<assign_expr>":
                {
                    if (nodes.Count == 1)
                        return Build(program, nodes[0], ref localVarIndex);
                    var rightType = Build(program, nodes[2], ref localVarIndex);
                    if (IsLocal(program, nodes[0]))
                    {
                        program.SaveRegisterToLocal(0, nodes[0].GetString(), rightType);
                        return rightType;
                    }
                    else
                    {
                        var valueIndex = program.SaveRegisterToLocal(0, ref localVarIndex, rightType);
                        
                        var fullName = nodes[0].ConvertTreeToString("");
                        var split = fullName.Split('.');
                        var localVarName = split[0];
                        var resultType = program.GetLocalType(localVarName);
                        program.LoadLocalToRegister(0, localVarName);
                        for (var i = 1; i < split.Length - 1; ++i)
                            resultType = LoadPropertyValueToZeroRegister(split[i], resultType, ref localVarIndex);

                        var tempIndex = program.SaveRegisterToLocal(0, ref localVarIndex, resultType);
                        program.LoadLocalToRegister(1, tempIndex);
                        program.LoadLocalToRegister(2, valueIndex);

                        var localPropName = split[split.Length - 1];
                        var methodName = $"{resultType.GetFullName()}.set_{localPropName}";
                        var sign = $"{methodName}({rightType.GetFullName()})";
                        var methodIndex = _engine.ExportedFunctions.FindIndex(x => x.GetMethodSign() == sign);
                        if (methodIndex >= 0)
                        {
                            program.CallNative(2, methodIndex, methodName, sign);
                            program.LoadLocalToRegister(0, valueIndex);
                            resultType = rightType;
                        }
                        else
                        {
                            program.SetErr(node.Name, "Method Not Found", sign);
                            resultType = null;
                        }

                        return resultType;
                    }
                }
                case "<var_expr>":
                {
                    if (IsLocal(program, node))
                    {
                        var localVarName = node.ConvertTreeToString(".");
                        program.LoadLocalToRegister(0, localVarName);
                        return program.GetLocalType(localVarName);
                    }
                    else
                    {
                        var fullName = node.ConvertTreeToString("");
                        var split = fullName.Split('.');
                        var localVarName = split[0];
                        var resultType = program.GetLocalType(localVarName);
                        program.LoadLocalToRegister(0, localVarName);
                        for (var i = 1; i < split.Length; ++i)
                            resultType = LoadPropertyValueToZeroRegister(split[i], resultType, ref localVarIndex);
                        return resultType;
                    }
                }
                case "<if_statement>":
                {
                    var exprNode = nodes[2];
                    var trueNode = nodes[4];
                    var falseNode = nodes.Count > 5 ? nodes[6] : null;
                    var exprType = Build(program, exprNode, ref localVarIndex);
                    if(exprType.Name != "bool")
                        program.SetErr($"IF expr must be bool type");
                    var opIndex = program.SetIf();
                    var trueType = Build(program, trueNode, ref localVarIndex);
                    program.SetNop("END IF TRUE BRANCH");
                    
                    if (falseNode != null)
                    {
                        var jmpIndex = program.SetJump(-1, "Jump to end of IF after TRUE branch");
                        var falseIndex = program.byteCode.Count;
                        program.byteCode[opIndex].Args[1] = falseIndex;
                        program.SetNop("START IF FALSE BRANCH");
                        var falseType = Build(program, falseNode, ref localVarIndex);
                        program.SetNop("END IF FALSE BRANCH");
                        program.byteCode[jmpIndex].Args[0] = program.byteCode.Count;
                    }
                    else
                    {
                        var falseIndex = program.byteCode.Count;
                        program.byteCode[opIndex].Args[1] = falseIndex;
                    }
                    program.SetNop("END IF");

                    return null;
                }
                default:
                {
                    program.SetErr(node.Name);
                    if(nodes != null)
                        foreach (var child in nodes) Build(program, child, ref localVarIndex);
                    return null;
                }
            }

#pragma warning disable 162
            throw new NotImplementedException($"Result type not defined, node={node.Name}");
#pragma warning restore 162
        }

        bool IsLocal(ScriptProgram program, IAstNode node)
        {
            var str = node.ConvertTreeToString("");
            return str.Split('.').Length == 1;
        }

        private List<IAstNode> CollectExpr(IAstNode n, string argName, string argsList)
        {
            var l = new List<IAstNode>();
            if(n.Name == argsList)
                foreach (var astNode in n.GetChilds())
                    l.AddRange(CollectExpr(astNode, argName, argsList));
            else if (n.Name == argName)
                l.Add(n);
            return l;
        }

        IScriptType FindType(string fullName)
        {
            return _engine.ExportedTypes[fullName];
        }

        private IScriptMethod ExtractMethod(ScriptProgram program, IAstNode node, ref int localVarIndex)
        {
            IScriptType GetScriptType(IAstNode n) => FindType(n.ConvertTreeToString("."));

            var nodes = node.GetChilds();
            
            var argsNodes = CollectExpr(nodes[3], "<method_arg_def>", "<method_arg_def_list>");

            var resultMethod = new ScriptCompiledMethod(
                nodes[1].GetString(),
                GetScriptType(nodes[0]),
                argsNodes.Select(x => GetScriptType(x.GetChilds()[0])).ToArray());
            program.methodsTable.Add(resultMethod, program.byteCode.Count);
            
            for (int i = 0; i < argsNodes.Count; i++)
            {
                var localVarName = argsNodes[i].GetChilds()[1].GetString();
                var localVarType = GetScriptType(argsNodes[i].GetChilds()[0]);
                program.SaveRegisterToLocal(i + 1, localVarName, localVarType);
            }

            var codeNode = nodes.Find(x => x.Name == "LIST_STATEMENT");
            if(codeNode != null)
                Build(program, codeNode, ref localVarIndex);
            
            //we need return for guard
            program.SetReturn();
            
            return resultMethod;
        }
    }

    class ScriptCompiledMethod : IScriptMethod
    {
        public string Name { get; }

        public IScriptType ResultType { get; }

        public IScriptType[] ArgsType { get; }

        public object Invoke(object[] args, int argsCount)
        {
            throw new NotImplementedException();
        }

        public ScriptCompiledMethod(string name, IScriptType resultType, IScriptType[] argsType)
        {
            Name = name;
            ResultType = resultType;
            ArgsType = argsType;
        }
    }
}