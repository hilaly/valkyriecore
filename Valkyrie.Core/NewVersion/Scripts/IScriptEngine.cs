using System.IO;

namespace Valkyrie.Scripts
{
    public interface IScriptEngine
    {
        IScript Compile(Stream stream);
        IScript Compile(IScript sourceScript, Stream stream);
    }
}