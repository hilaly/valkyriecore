namespace Valkyrie.Scripts
{
    enum ILOpCodes
    {
        nop = 0,	//This is nothing operation			Syntax: nop
        
        err,        //Throws error

        calln,		//This operation for calling exported function Syntax: calln argsCount index_in_table name sign
        ret,		//This is leave function operation	Syntax: ret
        call,		//This is call function operation	Syntax: call argsCount RetType [filename] FullTypeName::FunctionName(ArgsTypes)
        
        jmp,        //jump to operation                 Syntax: jmp index comment
        
        ldcst,      //Load constant to register         Syntax: ldcst %registernum% value

        ldloc,		//Load local var to register		Syntax: ldarg %registernum% %localvarnum%
        stloc,		//Stores register to local var		Syntax: starg %registernum% %localvarnum%

        newobj,		//Creates new object				Syntax: newobj func_signature

        beq,        //Branch to target
        
        OpCodesCount,
    };
}