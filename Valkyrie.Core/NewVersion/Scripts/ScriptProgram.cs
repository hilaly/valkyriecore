using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Valkyrie.Scripts
{
    public interface IScript
    {
        IEnumerable<IScriptType> Types { get; }
        IEnumerable<IScriptMethod> Methods { get; }
    }

    class ScriptProgram : IScript
    {
        public readonly List<CodeOp> byteCode = new List<CodeOp>();
        public readonly Dictionary<IScriptMethod, int> methodsTable = new Dictionary<IScriptMethod, int>();
        public Dictionary<string, IScriptType> LocalTypes { get; } = new Dictionary<string, IScriptType>();
        public IEnumerable<IScriptType> Types => Enumerable.Empty<IScriptType>();
        public IEnumerable<IScriptMethod> Methods => methodsTable.Keys;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Valkyrie Script:");

            sb.AppendLine("Methods table:");
            foreach (var pair in methodsTable)
                sb.AppendLine($"{pair.Value:x6}\t{pair.Key.ResultType.GetFullName()} {pair.Key.GetMethodSign()}");

            sb.AppendLine("IL code:");
            for (var index = 0; index < byteCode.Count; index++)
            {
                var codeOp = byteCode[index];
                sb.AppendLine(codeOp.Args != null
                    ? $"{index:x6}:\t{codeOp.Code}\t{string.Join(" ", codeOp.Args)}"
                    : $"{index:x6}:\t{codeOp.Code}");
            }

            return sb.ToString();
        }

        public void Load(ScriptProgram precompiled)
        {
            var sourceSize = byteCode.Count;
            byteCode.AddRange(precompiled.byteCode);
            foreach (var precompiledLocalType in precompiled.LocalTypes)
                LocalTypes.Add(precompiledLocalType.Key, precompiledLocalType.Value);
            foreach (var methods in precompiled.methodsTable)
                methodsTable.Add(methods.Key, methods.Value + sourceSize);
        }
    }
}