﻿using Valkyrie.Di;

namespace Valkyrie
{
    public static class Configurator
    {
        public static IContainer Start(string secretKey)
        {
            var result = new NewContainer();
            result.RegisterLibrary(Libraries.ToolsLibrary(secretKey));
            return result.Build();
        }
    }
}