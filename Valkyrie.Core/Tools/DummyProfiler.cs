using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Valkyrie.Tools
{
    class DummyProfiler : IProfiler
    {
        private readonly Dictionary<int, Stack<KeyValuePair<string,Stopwatch>>> _dictionary = new Dictionary<int, Stack<KeyValuePair<string, Stopwatch>>>();

        Stack<KeyValuePair<string,Stopwatch>> GetStack()
        {
            lock (_dictionary)
            {
                var key = Thread.CurrentThread.ManagedThreadId;
                if (!_dictionary.TryGetValue(key, out var stack))
                {
                    stack = new Stack<KeyValuePair<string,Stopwatch>>();
                    _dictionary.Add(key, stack);
                }

                return stack;
            }
        }
        
        public void BeginSample(string name)
        {
            var stack = GetStack();
            lock (stack)
                stack.Push(new KeyValuePair<string, Stopwatch>(name, Stopwatch.StartNew()));
        }

        public void EndSample()
        {
            var stack = GetStack();
            lock (stack)
            {
                var s = stack.Pop();
                var name = s.Key;
                var time = s.Value.ElapsedMilliseconds / 1000f;
            }
        }
    }
}