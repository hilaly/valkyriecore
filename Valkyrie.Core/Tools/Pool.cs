﻿using System;
using System.Collections.Generic;

namespace Valkyrie.Tools
{
    public class Pool<T>
    {
        private readonly Stack<T> _pool;
        private readonly Func<T> _createAction;
        private readonly Action<T> _initAction;
        private readonly Action<T> _destroyAction;

        public Pool(int defaultSize, Func<T> createAction, Action<T> initAction, Action<T> destroyAction)
        {
            _createAction = createAction;
            _initAction = initAction;
            _destroyAction = destroyAction;
            _pool = new Stack<T>(defaultSize);
        }

        public Pool(int defaultSize, Action<T> initAction, Action<T> destroyAction) : this(defaultSize,
            Activator.CreateInstance<T>, initAction, destroyAction)
        {
        }

        public T New()
        {
            var result = default(T);
            lock (_pool)
                if (_pool.Count > 0)
                    result = _pool.Pop();
            if (result == null)
                result = _createAction();
            _initAction?.Invoke(result);
            return result;
        }

        public void Store(T instance)
        {
            _destroyAction?.Invoke(instance);
            lock (_pool)
                _pool.Push(instance);
        }
    }
}
