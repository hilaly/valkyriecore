﻿using System.IO;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Tools
{
    /// <summary>
    /// Contains key-value config pairs
    /// Registered values:
    ///     log_path - log file path - single string
    ///     net_listen_ports - network ports for listen server - list of ports
    ///     net_max_connections - max count of network connections per port
    /// </summary>
    public interface IEnvironmentConfig
    {
        /// <summary>
        /// Check does environment config contains value with given key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool HasKey(string key);
        /// <summary>
        /// Return typed value for given key
        /// </summary>
        /// <typeparam name="T">result Type</typeparam>
        /// <param name="key">key to get value</param>
        /// <returns>throws exception if key is absent</returns>
        T Get<T>(string key);
        /// <summary>
        /// Return typed value for given key
        /// </summary>
        /// <typeparam name="T">result Type</typeparam>
        /// <param name="key">key to get value</param>
        /// <param name="defaultValue">default value if key is not present</param>
        /// <returns>throws exception if key is absent</returns>
        T Get<T>(string key, T defaultValue);
        /// <summary>
        /// Add new keyed value to config
        /// </summary>
        /// <typeparam name="T">Any of supported types</typeparam>
        /// <param name="key">Key for new value</param>
        /// <param name="value">Value for new avlue</param>
        void Set<T>(string key, T value);
        
        IPromise Deserialize(Stream stream);
        IPromise Serialize(Stream stream);
    }
}
