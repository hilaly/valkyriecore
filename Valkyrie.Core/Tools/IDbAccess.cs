namespace Valkyrie.Tools
{
    public interface IDbAccess
    {
        T Get<T>(string id);
        void Set<T>(string id, T value);
        void Delete<T>(string id);
    }

}