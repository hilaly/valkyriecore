﻿using System;

namespace Valkyrie.Tools.Logs
{
    internal class ConsoleLogger : LogWriter
    {
        private static readonly object GlobalLock = new object();

        private const string FormatWithStack = "{0}: {1}\n{2}";
        private const string FormatWithout = "{0}: {1}";

        protected override void Write(LogRecord record)
        {
            switch (record.Level)
            {
                case MessageLevel.Error:
                    var temp = string.Format(FormatWithStack, record.Channel, record.Message, record.StackTrace);
                    Log(ConsoleColor.Red,temp);
                    Console.Error.WriteLine(temp);
                    break;
                case MessageLevel.Warning:
                    Log(ConsoleColor.Yellow,
                        string.Format(FormatWithStack, record.Channel, record.Message, record.StackTrace));
                    break;
                case MessageLevel.Info:
                    Log(ConsoleColor.Gray,
                        string.Format(FormatWithout, record.Channel, record.Message, record.StackTrace));
                    break;
                case MessageLevel.Debug:
                    Log(ConsoleColor.DarkGray,
                        string.Format(FormatWithout, record.Channel, record.Message, record.StackTrace));
                    break;
                default:
                    throw new ArgumentOutOfRangeException("record.Level", record.Level, null);
            }
        }

        private void Log(ConsoleColor color, string message)
        {
            lock (GlobalLock)
            {
                var temp = Console.ForegroundColor;
                Console.ForegroundColor = color;
                Console.WriteLine(message);
                Console.ForegroundColor = temp;
            }
        }
    }
}