﻿namespace Valkyrie.Tools.Logs
{
    public interface ILogInstance
    {
        void Info(string message);
        void Debug(string message);
        void Warn(string message);
        void Error(string message);
    }
}