﻿namespace Valkyrie.Tools.Logs
{
    public static class LogExtension
    {
        public static ILogInstance GetLog(this ILogService service, string name)
        {
            return new LogInstance(name, service);
        }
    }
}