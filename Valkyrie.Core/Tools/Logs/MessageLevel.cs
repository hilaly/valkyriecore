﻿namespace Valkyrie.Tools.Logs
{
    public enum MessageLevel
    {
        Error = 0,
        Warning = 1,
        Info = 2,
        Debug = 3
    }
}