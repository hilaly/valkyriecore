﻿using System;

namespace Valkyrie.Tools.Logs
{
    internal class LogInstance : ILogInstance
    {
        private readonly IObserver<LogMessage> _message;

        public LogInstance(string name, ILogService service)
        {
            _message = service.GetChannel(name);
        }

        private void Log(MessageLevel level, string message)
        {
            _message.OnNext(new LogMessage {Level = level, Message = message});
        }
        
        public void Info(string message)
        {
            Log(MessageLevel.Info, message);
        }

        public void Debug(string message)
        {
            Log(MessageLevel.Debug, message);
        }

        public void Warn(string message)
        {
            Log(MessageLevel.Warning, message);
        }

        public void Error(string message)
        {
            Log(MessageLevel.Error, message);
        }
    }
}