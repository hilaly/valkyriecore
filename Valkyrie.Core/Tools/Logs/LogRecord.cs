﻿using System;
using System.Diagnostics;

namespace Valkyrie.Tools.Logs
{
    public struct LogRecord
    {
        public string Channel;
        public string Message;
        public MessageLevel Level;
        public StackTrace StackTrace;
        public DateTime Time;
    }
}