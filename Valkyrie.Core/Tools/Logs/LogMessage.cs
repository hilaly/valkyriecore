﻿namespace Valkyrie.Tools.Logs
{
    public struct LogMessage
    {
        public string Message;
        public MessageLevel Level;
    }
}