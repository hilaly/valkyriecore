﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Valkyrie.Threading.Async;

namespace Valkyrie.Tools.Logs
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class LogService : ILogService, IDisposable
    {
        private class LogChannel : IObserver<LogMessage>, IObservable<LogRecord>
        {
            private readonly Subject<LogRecord> _subject;
            private readonly string _name;

            public LogChannel(string name, Subject<LogRecord> subject)
            {
                _name = name;
                _subject = subject;
            }

            public void OnNext(LogMessage item)
            {
                _subject.OnNext(new LogRecord
                {
                    Channel = _name,
                    Level = item.Level,
                    Message = item.Message,
                    StackTrace = new StackTrace(true),
                    Time = DateTime.UtcNow
                });
            }

            public void OnError(Exception error)
            {
                _subject.OnNext(new LogRecord
                {
                    Channel = _name,
                    Level = MessageLevel.Error,
                    Message = error.Message,
                    StackTrace = new StackTrace(true),
                    Time = DateTime.UtcNow
                });
            }

            public void OnCompleted()
            {
                _subject.OnCompleted();
            }

            public IDisposable Subscribe(IObserver<LogRecord> observable)
            {
                return _subject.Subscribe(observable);
            }
        }

        private readonly Subject<LogRecord> _outChannel = new Subject<LogRecord>();
        private readonly Dictionary<string, LogChannel> _inputChannels = new Dictionary<string, LogChannel>();
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        public LogService(/*IEnvironmentConfig cfg*/)
        {
            _disposable.Add(_outChannel.Subscribe(new ConsoleLogger()));
//            var filename = cfg.Get(DefinedConstants.EnvironmentKeys.LogOutputFilePath, string.Empty);
//            if (filename.NotNullOrEmpty())
//                _disposable.Add(_outChannel.Subscribe(new HtmlFileLogger(filename)));
        }

        public IObserver<LogMessage> GetChannel(string name)
        {
            if (!_inputChannels.TryGetValue(name, out var channel))
            {
                channel = new LogChannel(name, _outChannel);
                _inputChannels.Add(name, channel);
            }

            return channel;
        }

        public IObservable<LogRecord> Observe()
        {
            return _outChannel;
        }

        public void Dispose()
        {
            _disposable.Dispose();
        }
    }
}