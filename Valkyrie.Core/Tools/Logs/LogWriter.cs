﻿using System;

namespace Valkyrie.Tools.Logs
{
    internal abstract class LogWriter : IObserver<LogRecord>
    {
        public void OnNext(LogRecord item)
        {
            Write(item);
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        protected abstract void Write(LogRecord record);
    }
}