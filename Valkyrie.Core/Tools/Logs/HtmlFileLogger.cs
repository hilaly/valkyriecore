﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Valkyrie.Tools.Logs
{
    internal class HtmlFileLogger : LogWriter
    {
        private readonly string _directoryName;
        private int _logMessageCounter;
        private StreamWriter _writeStream;
        private StreamWriter _writeButtonsStream;
        private StreamWriter _writeCssStream;

        private readonly HashSet<string> _buttonsList = new HashSet<string>();

        public HtmlFileLogger(string logDirectoryName)
        {
            var dt = DateTime.UtcNow;
            var locDirectoryName = (dt.ToShortDateString() + dt.ToLongTimeString() + dt.Ticks).Replace('/', '_')
                .Replace(':', '_');
            _directoryName = Path.Combine(logDirectoryName, locDirectoryName);

            if (!string.IsNullOrEmpty(_directoryName) && !Directory.Exists(_directoryName))
                Directory.CreateDirectory(_directoryName);

            lock (_buttonsList)
            {
                CreateScriptFile(_directoryName);
                Thread.Sleep(1);
                CreateCssFile(_directoryName);
                Thread.Sleep(1);
                CreateButtonsFile(_directoryName);
                Thread.Sleep(1);
                CreateLogFile(_directoryName);
                Thread.Sleep(1);
            }
        }

        #region Base init

        private void CreateLogFile(string directoryName)
        {
            var path = Path.Combine(directoryName, "log.html");
            var file = File.Create(path);
            _writeStream = new StreamWriter(file) {AutoFlush = false};

            _writeStream.WriteLine("<script language=\"javascript\" src=\"log.js\"></script>");
            _writeStream.WriteLine("<link rel=\"stylesheet\" type=\"text/css\" href=\"log.css\" />");
            _writeStream.WriteLine("<div class=\"Header\">");
            _writeStream.WriteLine();

            _writeStream.WriteLine("<object type=\"text/html\" data=\"buttons.html\"></object>");

            _writeStream.WriteLine();
            _writeStream.WriteLine("<br />");
            _writeStream.WriteLine("</div>");
        }

        private void CreateButtonsFile(string directoryName)
        {
            var path = Path.Combine(directoryName, "buttons.html");
            var file = File.Create(path);
            _writeButtonsStream = new StreamWriter(file) {AutoFlush = false};
        }

        private void WriteButton(TextWriter writeStream, string buttonName, string messageClassName)
        {
            writeStream.WriteLine(
                "<input type=\"button\" value=\"{0}\" class=\"{1} Button\" onclick=\"hide_class('{1}')\"/>", buttonName,
                messageClassName);
        }

        private void CreateCssFile(string directoryName)
        {
            if (File.Exists(Path.Combine(directoryName, "log.css")))
                return;

            _writeCssStream = new StreamWriter(File.Create(Path.Combine(directoryName, "log.css")));
            _writeCssStream.WriteLine("pre {");
            _writeCssStream.WriteLine("display:none;");
            _writeCssStream.WriteLine("}");
            _writeCssStream.WriteLine();
            _writeCssStream.WriteLine("p {");
            _writeCssStream.WriteLine("font-family:\"Calibri\";");
            _writeCssStream.WriteLine("margin:0;");
            _writeCssStream.WriteLine("}");
            _writeCssStream.WriteLine();
            _writeCssStream.WriteLine(".system {");
            _writeCssStream.WriteLine("background-color:#1E1E1E;");
            _writeCssStream.WriteLine("color:#DCDCDC;");
            _writeCssStream.WriteLine("display:block;");
            _writeCssStream.WriteLine("}");
            _writeCssStream.WriteLine();
            _writeCssStream.WriteLine(".button {");
            _writeCssStream.WriteLine("display:inline;");
            _writeCssStream.WriteLine("}");
            _writeCssStream.WriteLine();
        }

        private void CreateScriptFile(string directoryName)
        {
            if (File.Exists(Path.Combine(directoryName, "log.js")))
                return;

            using (var writer = new StreamWriter(File.Create(Path.Combine(directoryName, "log.js"))))
            {
                writer.WriteLine("/* Functions for toggling visibility in the log. */");
                writer.WriteLine("/* Gets all the elements in the node of the given  class and tag. */");
                writer.WriteLine("function getElementsByClass(searchClass,node,tag) {");
                writer.WriteLine("var classElements = new Array();");
                writer.WriteLine("if ( node == null )");
                writer.WriteLine("{");
                writer.WriteLine("node = document;");
                writer.WriteLine("}");
                writer.WriteLine("if ( tag == null )");
                writer.WriteLine("{");
                writer.WriteLine("tag = '*';");
                writer.WriteLine("}");
                writer.WriteLine("var elements = node.getElementsByTagName(tag);");
                writer.WriteLine("var elementsSize = elements.length;");
                writer.WriteLine("var pattern = new RegExp(\"(^|\\\\s)\" + searchClass + \"(\\\\s|$)\");");
                writer.WriteLine("for (i = 0, j = 0; i < elementsSize; i++)");
                writer.WriteLine("{");
                writer.WriteLine("if ( pattern.test(elements[i].className) ) ");
                writer.WriteLine("{");
                writer.WriteLine("classElements[j] = elements[i];");
                writer.WriteLine("j++;");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.WriteLine("return classElements;");
                writer.WriteLine("}");
                writer.WriteLine("/* For toggling visibility of stacks in the HTML log. */");
                writer.WriteLine("function hide(id) ");
                writer.WriteLine("{");
                writer.WriteLine("var element_style = document.getElementById(id).style;");
                writer.WriteLine("var status = element_style.display;");
                writer.WriteLine("if (status != 'block') ");
                writer.WriteLine("{");
                writer.WriteLine("element_style.display = 'block';");
                writer.WriteLine("}");
                writer.WriteLine("else //status == visible");
                writer.WriteLine("{");
                writer.WriteLine("element_style.display = 'none';");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.WriteLine("/* For toggling visibility of a class in the HTML log. */");
                writer.WriteLine("function hide_class(className)");
                writer.WriteLine("{");
                writer.WriteLine("var elements = getElementsByClass(className);");
                writer.WriteLine("var pattern = new RegExp(\"(^|\\\\s)Button(\\\\s|$)\");");
                writer.WriteLine("for(i = 0; i < elements.length; i++)");
                writer.WriteLine("{		");
                writer.WriteLine("if(!pattern.test(elements[i].className))");
                writer.WriteLine("{");
                writer.WriteLine("if(elements[i].style.display != 'none')");
                writer.WriteLine("{");
                writer.WriteLine("elements[i].style.display = 'none'");
                writer.WriteLine("}");
                writer.WriteLine("else");
                writer.WriteLine("{");
                writer.WriteLine("elements[i].style.display = 'block'");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.WriteLine("}");
                writer.WriteLine("}");
            }
        }

        private void RewriteLog()
        {
            //Free last stream
            _writeStream.Flush();
            _writeStream.Dispose();
            _writeStream = null;

            //Read old data
            using (var tempStream = new MemoryStream())
            {
                using (var writer = new StreamWriter(tempStream))
                {
                    using (var reader = new StreamReader(File.OpenRead(Path.Combine(_directoryName, "log.html"))))
                        writer.Write(reader);

                    writer.Flush();
                    tempStream.Position = 0;

                    CreateLogFile(_directoryName);

                    using (var reader = new StreamReader(tempStream))
                        _writeStream.Write(reader);
                }
            }

            _writeStream.Flush();
        }

        #endregion

        #region Implementation of LogWriter

        protected override void Write(LogRecord record)
        {
            lock (_buttonsList)
            {
                var messageClassName = record.Channel.Replace('.', '_');
                if (!_buttonsList.Contains(record.Channel))
                {
                    _buttonsList.Add(record.Channel);
                    WriteButton(_writeButtonsStream, record.Channel, messageClassName);
                    _writeButtonsStream.Flush();

                    _writeCssStream.WriteLine(".{0} {1}", messageClassName, '{');
                    _writeCssStream.WriteLine("}");
                    _writeCssStream.Flush();
                }

                var index = _logMessageCounter++;

                var text = new[]
                {
                    $"<p class=\"{messageClassName}\"><span class=\"time\">{record.Time}</span><a onclick=\"hide('trace{index}')\">STACK</a> {record.Message}</p>",
                    $"<pre id=\"trace{index}\">{record.StackTrace}</pre>"
                };

                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < text.Length; ++i)
                    _writeStream.Write(text[i]);

                _writeStream.Flush();
            }
        }

        #endregion
    }
}