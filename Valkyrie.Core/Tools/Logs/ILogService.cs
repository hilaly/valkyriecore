﻿using System;

namespace Valkyrie.Tools.Logs
{
    public interface ILogService
    {
        IObserver<LogMessage> GetChannel(string name);
        IObservable<LogRecord> Observe();
    }
}