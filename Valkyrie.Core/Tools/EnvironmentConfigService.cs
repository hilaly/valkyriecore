using System;
using System.Collections.Generic;
using System.IO;
using Valkyrie.Data;
using Valkyrie.Data.Config;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Tools
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class EnvironmentConfigService : IEnvironmentConfig
    {
        private const char Separator = '=';

        private readonly Dictionary<string, string> _dictionary = new Dictionary<string, string>();
        private readonly IStringInterning _strings;

        public EnvironmentConfigService(IStringInterning strings)
        {
            _strings = strings;
        }

        public bool HasKey(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        public T Get<T>(string key)
        {
            if (_dictionary.TryGetValue(key, out var value))
            {
                try
                {
                    return (T) DataSerializer.DeserializeValue(typeof(T), value, _strings);
                }
                catch (Exception e)
                {
                    return value.ToObject<T>();
                }
            }
            throw new KeyNotFoundException($"Environment config doesn't contains '{key}' key");
        }

        public T Get<T>(string key, T defaultValue)
        {
            return _dictionary.TryGetValue(key, out var value)
                ? (T) DataSerializer.DeserializeValue(typeof(T), value, _strings)
                : defaultValue;
        }

        public void Set<T>(string key, T value)
        {
            if (DataSerializer.GetStringValue(value, out var v))
                _dictionary[key] = v;
            else
                throw new Exception($"{typeof(T).FullName} is not supported type for environment config");
        }
        
        #region IEnvironmentConfigSerializer

        public IPromise Deserialize(Stream stream)
        {
            try
            {
                using (var reader = new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        if(line.IsNullOrEmpty())
                            continue;
                        // ReSharper disable once PossibleNullReferenceException
                        var index = line.IndexOf(Separator);
                        if(index < 0)
                            _dictionary[line.Trim()] = string.Empty;
                        else
                            _dictionary[line.Substring(0, index).Trim()] = line.Substring(index + 1).Trim();
                    }
                }
                
                return Promise.Resolved();
            }
            catch (Exception e)
            {
                return Promise.Rejected(e);
            }
        }

        public IPromise Serialize(Stream stream)
        {
            try
            {
                using (var writer = new StreamWriter(stream))
                {
                    foreach (var pair in _dictionary)
                        if (pair.Value.IsNullOrEmpty())
                            writer.WriteLine(pair.Key);
                        else
                            writer.WriteLine("{0}={1}", pair.Key, pair.Value);

                    writer.Flush();
                }

                return Promise.Resolved();
            }
            catch (Exception e)
            {
                return Promise.Rejected(e);
            }
        }

        #endregion
    }
}