﻿namespace Valkyrie.Tools
{
    public interface IProfiler
    {
        void BeginSample(string name);
        void EndSample();
    }
}