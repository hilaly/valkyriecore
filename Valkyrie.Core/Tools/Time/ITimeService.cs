﻿using System;

namespace Valkyrie.Tools.Time
{
    public interface ITimeService
    {
        ITime Default { get; }
        TimeSpan TimeSinceStartup { get; }
        
        ITime GetTime(string name);
    }
}
