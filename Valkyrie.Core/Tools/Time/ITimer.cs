using System;

namespace Valkyrie.Tools.Time
{
    public interface ITimer : IObservable<float>, IDisposable
    {
        float TimeLeft { get; }
    }
}