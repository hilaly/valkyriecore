﻿using System;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Tools.Time
{
    class TimeAndDispatcherTimer : ITimer
    {
        private readonly ITime _time;
        private readonly float _startTime;
        private readonly float _length;
        private CompositeDisposable _subscription = new CompositeDisposable();
        private readonly Subject<float> _subject = new Subject<float>();

        public TimeAndDispatcherTimer(ITime time, float length, IDispatcher dispatcher)
        {
            _time = time;
            _startTime = _time.Time;
            _length = length;

            _subscription.Add(_subject);
            _subscription.Add(dispatcher.EveryUpdate(() =>
            {
                var timeLeft = TimeLeft;
                _subject.OnNext(timeLeft);
                if (timeLeft > 0)
                    return;
                
                _subject.OnCompleted();
                Dispose();
            }));
        }

        public float TimeLeft
        {
            get { return _length - (_time.Time - _startTime); }
        }

        public void Dispose()
        {
            if (_subscription != null)
            {
                _subscription.Dispose();
                _subscription = null;
            }
        }

        public IDisposable Subscribe(IObserver<float> observable)
        {
            return _subject.Subscribe(observable);
        }
    }
}