﻿namespace Valkyrie.Tools.Time
{
    public interface ITime
    {
        float DeltaTime { get; }
        float UnscaledDeltaTime { get; }

        float Time { get; }
        float UnscaledTime { get; }

        float Scale { get; set; }

        ITimer StartTimer(float seconds);
    }
}