﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Tools.Time
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class TimeService : ITimeService
    {
        private readonly Stopwatch _startStopwatch;
        private long _ticks;
        private readonly Dictionary<string, SimpleTime> _times = new Dictionary<string, SimpleTime>();
        private readonly IDispatcher _dispatcher;

        public TimeService(IDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
            _ticks = 0;
            _startStopwatch = Stopwatch.StartNew();

            Default = GetTime("Default");
        }

        #region Implementation of ITimeSystem

        public ITime Default { get; private set; }
        public TimeSpan TimeSinceStartup { get { return _startStopwatch.Elapsed; } }
        public ITime GetTime(string name)
        {
            SimpleTime result;
            lock (_times)
            {
                if (!_times.TryGetValue(name, out result))
                {
                    result = new SimpleTime(TimeSinceStartup.Seconds, _dispatcher);
                    _times.Add(name, result);
                }
            }
            return result;
        }

        #endregion

        #region Update

        public void Update()
        {
            var currentCount = _startStopwatch.ElapsedTicks;
            var timeElapsedInTicks = currentCount - _ticks;
            _ticks = currentCount;

            var deltaTime = (float)((double)timeElapsedInTicks / Stopwatch.Frequency);

            Update(deltaTime);
        }

        public void Update(float deltaTime)
        {
            UpdateTimes(deltaTime);
        }

        private void UpdateTimes(float deltaTime)
        {
            lock (_times)
                foreach (var pair in _times)
                    pair.Value.Update(deltaTime);
        }

        #endregion
    }
}