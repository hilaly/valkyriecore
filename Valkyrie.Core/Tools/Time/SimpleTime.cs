﻿using Valkyrie.Threading.Scheduler;

namespace Valkyrie.Tools.Time
{
    class SimpleTime : ITime
    {
        private readonly IDispatcher _dispatcher;
        
        public SimpleTime(float time, IDispatcher dispatcher)
        {
            Time = time;
            _dispatcher = dispatcher;
            UnscaledTime = time;
            Scale = 1f;
        }

        #region Implementation of ITime

        public float Scale { get; set; }

        public float DeltaTime { get; private set; }
        public float UnscaledDeltaTime { get; private set; }
        public float Time { get; private set; }
        public float UnscaledTime { get; private set; }

        #endregion

        public void Update(float deltaTime)
        {
            UnscaledDeltaTime = deltaTime;
            UnscaledTime += UnscaledDeltaTime;

            DeltaTime = deltaTime*Scale;
            Time += DeltaTime;
        }

        public ITimer StartTimer(float time)
        {
            return new TimeAndDispatcherTimer(this, time, _dispatcher);
        }
    }
}