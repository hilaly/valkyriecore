using System;
using System.Threading;
using System.Threading.Tasks;
using Valkyrie.Communication.Network;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Communication.Network.LidgrenImpl;
using Valkyrie.Data;
using Valkyrie.Rpc;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network
{
    public static class Utils
    {
        public static IClient CreateClient(ServerPort connectionDefine, ILogInstance logInstance)
        {
            return new LidgrenClient(connectionDefine.Address, connectionDefine.Port, connectionDefine.AppId,
                SynchronizationContext.Current, logInstance ?? new DummyLogInstance());
        }

        public static Task<IClient> CreateClientAsync(ServerPort connectionDefine, ILogInstance logInstance)
        {
            var completionSource = new TaskCompletionSource<IClient>();
            var client = (LidgrenClient) CreateClient(connectionDefine, logInstance);
            Action<NetConnectionStatus> call = null;
            call = (status) =>
            {
                if (status == NetConnectionStatus.Connected)
                {
                    client.OnStatesChanged -= call;
                    completionSource.SetResult(client);
                }
            };
            client.OnStatesChanged += call;
            return completionSource.Task;
        }

        public static IServer StartServer(ServerPort connectionDefine, ILogInstance logInstance)
        {
            return new LidgrenServer(connectionDefine.Port, connectionDefine.AppId, connectionDefine.MaxConnections,
                SynchronizationContext.Current, logInstance ?? new DummyLogInstance());
        }

        public static Task<IServer> StartServerAsync(ServerPort connectionDefine, ILogInstance logInstance)
        {
            var source = new TaskCompletionSource<IServer>();
            var server = (LidgrenServer)StartServer(connectionDefine, logInstance);
            //TODO: handle server state
            source.SetResult(server);
            return source.Task;
        }

        internal static byte[] ReadMessage(NetIncomingMessage im)
        {
            var isCompressed = im.ReadByte();
            var msgBytes = isCompressed != 0
                ? im.ReadBytes(im.LengthBytes - 1).UnzipToByteArray(DataExtensions.LidgrenNetworkZipEntryName)
                : im.ReadBytes(im.LengthBytes - 1);
            return msgBytes;
        }

        internal static void HandleMessage(NetIncomingMessage im, ILogInstance logger, 
            Action<NetConnectionStatus, string, NetConnection> statusHandler,
            Action<NetIncomingMessage> dataHandler)
        {
            // handle incoming message
            switch (im.MessageType)
            {
                case NetIncomingMessageType.VerboseDebugMessage:
                    logger.Debug(im.ReadString());
                    break;
                case NetIncomingMessageType.DebugMessage:
                    logger.Info(im.ReadString());
                    break;
                case NetIncomingMessageType.WarningMessage:
                    logger.Warn(im.ReadString());
                    break;
                case NetIncomingMessageType.ErrorMessage:
                    logger.Error(im.ReadString());
                    break;

                case NetIncomingMessageType.StatusChanged:
                    var status = (NetConnectionStatus)im.ReadByte();
                    var reason = im.ReadString();
                    statusHandler(status, reason, im.SenderConnection);
                    break;
                case NetIncomingMessageType.Data:
                    dataHandler(im);
                    break;
                default:
                    logger.Warn("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes");
                    break;
            }
        }

        internal static NetChannelStatus Convert(NetConnectionStatus status)
        {
            switch (status)
            {
                case NetConnectionStatus.InitiatedConnect:
                case NetConnectionStatus.ReceivedInitiation:
                case NetConnectionStatus.RespondedAwaitingApproval:
                case NetConnectionStatus.RespondedConnect:
                    return NetChannelStatus.Connecting;
                case NetConnectionStatus.Connected:
                    return NetChannelStatus.Connected;
                case NetConnectionStatus.Disconnecting:
                    return NetChannelStatus.Disconnecting;
                case NetConnectionStatus.None:
                case NetConnectionStatus.Disconnected:
                default:
                    return NetChannelStatus.Disconnected;
            }
        }

        internal static void SendImpl(NetConnection connection, NetPeer netClient, int compression
            , byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            var outMsg = compression == 0
                ? buffer
                : buffer.ZipToByteArray(DataExtensions.LidgrenNetworkZipEntryName, compression);
            
            var im = connection.Peer.CreateMessage(outMsg.Length+1);
            im.Write((byte) (compression != 0 ? 1 : 0));
            im.Write(outMsg);
            connection.SendMessage(im, LidgrenNetwork.Convert(deliveryMethod), dataChannel);
            netClient.FlushSendQueue();
        }
    }
}