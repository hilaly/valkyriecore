using System;
using System.Collections.Generic;
using System.Net;
using Valkyrie.Communication.Network;
using Valkyrie.Data.Config;

namespace Valkyrie.Network
{
    public interface IClient : IDisposable
    {
        /// <summary>
        /// Ping in seconds
        /// </summary>
        float Ping { get; }
        /// <summary>
        /// EndPoint of remote host
        /// </summary>
        IPEndPoint EndPoint { get; }
        /// <summary>
        /// Set compression level for traffic (1-9), 0 is no compression
        /// </summary>
        int TrafficCompression { get; set; }
        /// <summary>
        /// Sends data to remote host
        /// </summary>
        /// <param name="buffer">data to transition</param>
        /// <param name="deliveryMethod">delivery method for use for this data</param>
        /// <param name="dataChannel">channel in which this data will be sended</param>
        void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel);
        NetChannelStatus Status { get; }
        byte[] PopMessage();
    }

    public interface IClientView : IClient
    {
        /// <summary>
        /// Unique client identifier
        /// </summary>
        int Uid { get; }
        /// <summary>
        /// User defined data, associated with this client
        /// </summary>
        object Data { get; set; }
    }

    public interface IServer : IDisposable
    {
        /// <summary>
        /// Port on which this server listen
        /// </summary>
        int Port { get; }
        /// <summary>
        /// Set default compression level for traffic (1-9), 0 is no compression
        /// </summary>
        int TrafficCompression { get; set; }
        /// <summary>
        /// Pop all received messages
        /// </summary>
        /// <returns>list of key_value pairs client-message</returns>
        List<SerializedKeyValuePair<IClientView, byte[]>> PopMessages();
    }
}