using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Valkyrie.Communication.Network;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Data.Config;
using Valkyrie.Math;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network
{
    class LidgrenServer : IServer
    {
        private readonly ILogInstance _logger;
        private int _trafficCompression;
        private readonly NetServer _netServer;

        #region IDisposable

        public void Dispose()
        {
            if(_netServer != null)
                _netServer.Shutdown(nameof(Dispose));
        }

        #endregion

        #region IServer

        public int Port { get; }

        public int TrafficCompression
        {
            get => _trafficCompression;
            set
            {
                if (value < 0 || value > 9)
                    throw new ArgumentOutOfRangeException(nameof(TrafficCompression), value,
                        $"0 - no compression, 1-9 is valid compression range");
                _trafficCompression = value;
            }
        }

        public List<SerializedKeyValuePair<IClientView, byte[]>> PopMessages()
        {
            lock (_incomingQueue)
            {
                var res = new List<SerializedKeyValuePair<IClientView, byte[]>>(_incomingQueue);
                _incomingQueue.Clear();
                return res;
            }
        }

        #endregion

        public LidgrenServer(int port, string appId, int maxConnections,
            SynchronizationContext ctx, ILogInstance logger)
        {
            _logger = logger;
            _incomingHandler = AddToIncomingQueue;
            Port = port;

            NetPeerConfiguration config = new NetPeerConfiguration(appId)
            {
                MaximumConnections = maxConnections,
                Port = port,
                NetworkThreadName = $"ServerNetwork_{port}"
            };
            _netServer = new NetServer(config);
            _netServer.RegisterReceivedCallback(GotMessage, ctx);
            _netServer.Start();
        }

        private void AddToIncomingQueue(IClientView clientView, byte[] bytes)
        {
            lock (_incomingQueue)
                _incomingQueue.Add(new SerializedKeyValuePair<IClientView, byte[]>() {Key = clientView, Value = bytes});
        }

        void GotMessage(object netPeer)
        {
            var peer = (NetPeer) netPeer;
            NetIncomingMessage im;
            while ((im = peer.ReadMessage()) != null)
            {
                Utils.HandleMessage(im, _logger, (status, reason, connection) =>
                    {
                        //TODO: handle disconnecting
                        _logger.Debug(status + ": " + reason);
                        UpdateConnectionsList();
                    },
                    ReceiveData);
                peer.Recycle(im);
            }
        }

        private void ReceiveData(NetIncomingMessage im)
        {
            var msgBytes = Utils.ReadMessage(im);
            var senderConnection = im.SenderConnection;
            var view = GetOrCreate(senderConnection);

            _incomingHandler(view, msgBytes);
        }

        #region Working with clients

        class LidgrenClientView : IClientView
        {
            private int _trafficCompression;
            private readonly LidgrenServer _owner;
            public int Uid { get; }
            public NetConnection Connection { get; }
            public float Ping => Connection.AverageRoundtripTime;
            public IPEndPoint EndPoint => Connection.RemoteEndPoint;
            public NetChannelStatus Status => Utils.Convert(Connection.Status);
            
            public byte[] PopMessage()
            {
                throw new NotImplementedException();
            }

            public object Data
            {
                get => Connection.Tag;
                set => Connection.Tag = value;
            }
            public int TrafficCompression
            {
                get => _trafficCompression;
                set
                {
                    if (value < 0 || value > 9)
                        throw new ArgumentOutOfRangeException(nameof(TrafficCompression), value,
                            $"0 - no compression, 1-9 is valid compression range");
                    _trafficCompression = value;
                }
            }

            public LidgrenClientView(int id, NetConnection connection, LidgrenServer owner)
            {
                Uid = id;
                Connection = connection;
                _owner = owner;
            }

            public void Dispose()
            {
                Connection.Disconnect(nameof(Dispose));
            }

            public void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
            {
                _owner.SendMessage(this, buffer, deliveryMethod, dataChannel);
            }
        }

        private void SendMessage(LidgrenClientView view, byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            var compression = view.TrafficCompression != 0 ? view.TrafficCompression : TrafficCompression;
            Utils.SendImpl(view.Connection, _netServer, compression,
                buffer, deliveryMethod, dataChannel);
        }

        private readonly IntIdProvider _idProvider = new IntIdProvider();
        private readonly Dictionary<NetConnection, LidgrenClientView> _connectionToClientsMap = new Dictionary<NetConnection, LidgrenClientView>();
        private readonly Dictionary<int, LidgrenClientView> _idToClientsMap = new Dictionary<int, LidgrenClientView>();
        private Action<IClientView, byte[]> _incomingHandler;

        private readonly List<SerializedKeyValuePair<IClientView, byte[]>> _incomingQueue =
            new List<SerializedKeyValuePair<IClientView, byte[]>>();

        private void UpdateConnectionsList()
        {
            var exist = _netServer.Connections;
            foreach (var connection in exist)
            {
                var view = GetOrCreate(connection);
            }

            List<NetConnection> toRemove = new List<NetConnection>();
            lock (_idProvider)
            {
                foreach (var view in _connectionToClientsMap)
                {
                    if(exist.Contains(view.Key))
                        continue;
                    toRemove.Add(view.Key);
                }
            }

            foreach (var connection in toRemove)
                Destroy(connection);
        }

        private void Destroy(NetConnection connection)
        {
            lock (_idProvider)
            {
                if(!_connectionToClientsMap.TryGetValue(connection, out var res))
                    return;
                _idToClientsMap.Remove(res.Uid);
                _connectionToClientsMap.Remove(connection);
                
                res.Dispose();
            }
        }

        LidgrenClientView GetOrCreate(NetConnection connection)
        {
            lock (_idProvider)
            {
                if (_connectionToClientsMap.TryGetValue(connection, out var result))
                    return result;
                
                result = new LidgrenClientView(_idProvider.Generate(), connection, this);
                _connectionToClientsMap.Add(connection, result);
                _idToClientsMap.Add(result.Uid, result);
                return result;
            }
        }
        
        #endregion
    }
}