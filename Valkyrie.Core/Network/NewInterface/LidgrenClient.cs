using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Valkyrie.Communication.Network;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network
{
    class LidgrenClient : IClient
    {
        private readonly ILogInstance _logger;
        private int _trafficCompression;
        private NetClient _netClient;
        private NetConnection _connection;
        private Action<byte[]> _incomingHandler;
        private Subject<byte[]> _incomingObservable;

        public override string ToString()
        {
            return $"Client(Lidgren)[{EndPoint}]";
        }

        #region IClient

        public IObservable<byte[]> SubscribeIncoming()
        {
            lock (_netClient)
            {
                if (_incomingObservable == null)
                {
                    _logger.Debug($"Create new PUSH incoming handler for connection {this}");
                    _incomingObservable = new Subject<byte[]>();
                    _incomingHandler = IncomingEvent;
                }

                return _incomingObservable;
            }
        }

        private void IncomingEvent(byte[] bytes)
        {
            _logger.Debug($"Handle new network packet from {this}");
            lock (_netClient)
                _incomingObservable.OnNext(bytes);
        }

        public float Ping => _connection?.AverageRoundtripTime ?? -1f;

        public IPEndPoint EndPoint => _connection?.RemoteEndPoint;
        public NetChannelStatus Status => Utils.Convert(_connection.Status);
        public event Action<NetConnectionStatus> OnStatesChanged;

        public int TrafficCompression
        {
            get => _trafficCompression;
            set
            {
                if (value < 0 || value > 9)
                    throw new ArgumentOutOfRangeException(nameof(TrafficCompression), value,
                        $"0 - no compression, 1-9 is valid compression range");
                _trafficCompression = value;
            }
        }

        class SendDto
        {
            public byte[] Buffer;
            public NetDeliveryMethod Method;
            public int Channel;
        }

        readonly List<SendDto> _sendQueue = new List<SendDto>();

        private void OnStatus(NetConnectionStatus status)
        {
            _logger.Debug($"Status of {this} updated to {status}");
            
            if (status == NetConnectionStatus.Connected)
                lock (_sendQueue)
                {
                    foreach (var dto in _sendQueue)
                    {
                        _logger.Debug($"Sending waiting outgoing packet for {this}");
                        Utils.SendImpl(_connection, _netClient, TrafficCompression, dto.Buffer, dto.Method, dto.Channel);
                    }
                    _sendQueue.Clear();
                }
        }

        public void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            if (Status != NetChannelStatus.Connected)
            {
                lock (_sendQueue)
                    if (Status != NetChannelStatus.Connected)
                    {
                        _logger.Debug($"Save outgoing packet in {this} for future sending");
                        _sendQueue.Add(new SendDto() {Buffer = buffer, Channel = dataChannel, Method = deliveryMethod});
                        return;
                    }
            }

            _logger.Debug($"Send out packet size={buffer.Length} to {this} immediate");
            Utils.SendImpl(_connection, _netClient, TrafficCompression,
                buffer, deliveryMethod, dataChannel);
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            _logger.Debug($"Disposing {this}");
            if (_netClient != null)
            {
                _netClient.Shutdown(nameof(Dispose));
                _netClient = null;
            }
        }

        #endregion

        public LidgrenClient(string address, int port, string appId, SynchronizationContext syncCtx,
            ILogInstance logger)
        {
            _logger = logger;
            _incomingHandler = AddToIncomingQueue;
            OnStatesChanged += OnStatus;

            _netClient = new NetClient(new NetPeerConfiguration(appId)
            {
                AutoFlushSendQueue = false,
                NetworkThreadName = $"NetworkThread_{address}:{port}"
            });
            _netClient.RegisterReceivedCallback(GotMessage, syncCtx);
            _logger.Info($"Client {this} created");

            _netClient.Start();
            _connection = _netClient.Connect(address, port);
        }

        #region Messages handling

        private readonly List<byte[]> _incomingQueue = new List<byte[]>();

        void GotMessage(object netPeer)
        {
            var peer = (NetPeer) netPeer;
            NetIncomingMessage im;
            while ((im = peer.ReadMessage()) != null)
            {
                _logger.Debug($"Receive message from {this}");
                Utils.HandleMessage(im, _logger, (status, reason, connection) =>
                    {
                        _logger.Debug($"Status changed {status}: {reason}");
                        OnStatesChanged?.Invoke(status);
                        //TODO: handle disconnecting
                    },
                    ReceiveData);
                peer.Recycle(im);
            }
        }

        private void ReceiveData(NetIncomingMessage im)
        {
            var msgBytes = Utils.ReadMessage(im);
            _logger.Debug($"Receive packet from {this}, size={msgBytes.Length}");
            _incomingHandler(msgBytes);
        }

        private void AddToIncomingQueue(byte[] bytes)
        {
            _logger.Debug($"Push packet to incoming queue for POLL handling");
            lock (_incomingQueue)
                _incomingQueue.Add(bytes);
        }

        public byte[] PopMessage()
        {
            lock (_incomingQueue)
            {
                if (_incomingQueue.Count > 0)
                {
                    var res = _incomingQueue[0];
                    _incomingQueue.RemoveAt(0);
                    return res;
                }

                return null;
            }
        }

        #endregion
    }
}