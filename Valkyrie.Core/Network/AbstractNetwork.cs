﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Authentication;
using System.Threading;
using Valkyrie.Communication.Network.LidgrenImpl;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network
{
    
    abstract class AbstractNetwork : INetwork, IDisposable
    {
        private readonly Dictionary<IPEndPoint, INetworkChannel> _networkChannels =
            new Dictionary<IPEndPoint, INetworkChannel>();

        protected readonly CompositeDisposable Disposable = new CompositeDisposable();

        protected ILogInstance Log { get; }

        protected AbstractNetwork(ILogService logService, IDispatcher dispatcher)
        {
            Channel.Singleton = this;
            
            if (SynchronizationContext.Current == null)
                SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());

            Log = logService.GetLog("Valkyrie.Network");

            Disposable.Add(dispatcher.EveryPostUpdate(Iteration));
        }

        #region INetwork

        #endregion

        #region Derived classes callbacks

        protected void HandleChannelCreation(INetworkChannel channel)
        {
            lock (_networkChannels)
                _networkChannels.Add(channel.EndPoint, channel);

            Log.Info($"Open connection with {channel.EndPoint.Address}:{channel.EndPoint.Port}");
        }

        protected void HandleChannelDestroying(INetworkChannel channel)
        {
            lock (_networkChannels)
                _networkChannels.Remove(channel.EndPoint);

            Log.Info($"Close connection with {channel.EndPoint.Address}:{channel.EndPoint.Port}");
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Disposable.Dispose();
        }

        #endregion

        #region derived classes override

        protected abstract void Iteration();
        protected abstract IPromise<INetworkListener> Bind(int port, int maxConnections, string appId);
        protected abstract IPromise<INetworkChannel> CreateChannel(IPEndPoint endPoint, string appId);

        #endregion

        #region INetworkStarter

        [Contract, NetworkMessage(NetDeliveryMethod.ReliableUnordered, 0)]
        class AuthRequest
        {
            [ContractMember(0)] public string Secret;
            [ContractMember(1)] public string Uid;
        }

        [Contract, NetworkMessage(NetDeliveryMethod.ReliableUnordered, 0)]
        class AuthResponse
        {
            [ContractMember(0)] public int Error;
            [ContractMember(1)] public string Uid;
        }

        public int CompressLevel
        {
            get => Channel.ZipCompressLevel;
            set => Channel.ZipCompressLevel = value;
        }

        public IPromise<INetworkListener> Listen(int port, int maxConnections, string appId,
            Func<INetworkChannel, string, ErrorCodes> authRequestHandler)
        {
            Log.Info($"Start listening on port {port}");

            return Bind(port, maxConnections, appId)
                .Then((listener) => {
                    Log.Info($"Listen network on port {port}");
                    return listener; 
                })
                .Then((listener) =>
                {
                    listener.Subscribe<AuthRequest>((request, channel) =>
                    {
                        ((LidgrenBaseChannel) channel).AuthToken = request.Secret;
                        
                        var response = new AuthResponse
                        {
                            Uid = request.Uid,
                            Error = (int)authRequestHandler(channel, request.Secret)
                        };

                        channel.Send(response);

                        if (response.Error != (int)ErrorCodes.Ok)
                            channel.Dispose();
                    });
                    
                    return listener;
                });
        }


        private Dictionary<IPEndPoint, IPromise<INetworkChannel>> _pending = new Dictionary<IPEndPoint, IPromise<INetworkChannel>>();

        public IPromise<INetworkChannel> Connect(string ipAddress, int port, string appId, string authkey)
        {
            var ipEndPoint = new IPEndPoint(IPAddress.Parse(ipAddress), port);
            
            if (_pending.TryGetValue(ipEndPoint, out var promise))
                return promise;

            INetworkChannel channel;

            lock (_networkChannels)
                if (!_networkChannels.TryGetValue(ipEndPoint, out channel))
                {
                    var uid = Guid.NewGuid().ToString();
                    promise = CreateChannel(ipEndPoint, appId).Then(c => channel = c).Then((c) =>
                    {
                        return c.Send<AuthRequest, AuthResponse>(new AuthRequest {Secret = authkey, Uid = uid},
                            response => response.Uid == uid);
                    }).Then(response =>
                    {
                        if (response.Error == (int)ErrorCodes.Ok)
                            return Promise<INetworkChannel>.Resolved(channel);
                        else
                        {
                            channel.Dispose();
                            return Promise<INetworkChannel>.Rejected(new AuthenticationException(response.Error.ToString()));
                        }
                    })
                        .Finally(() => _pending.Remove(ipEndPoint));
                    _pending.Add(ipEndPoint, promise);
                    return promise;
                }

            return Promise<INetworkChannel>.Resolved(channel);
        }

        #endregion
    }
}