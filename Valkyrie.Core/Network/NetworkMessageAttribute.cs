﻿using System;

namespace Valkyrie.Communication.Network
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class NetworkMessageAttribute : Attribute
    {
        public NetworkMessageAttribute(NetDeliveryMethod delivery, int channel)
        {
            Delivery = delivery;
            Channel = channel;
        }

        public int Channel { get; }
        public NetDeliveryMethod Delivery { get; }
    }
}