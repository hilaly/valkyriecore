using System;
using Valkyrie.Ai.FSM;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;

namespace Valkyrie.Communication.Network
{
    public enum ChannelState
    {
        Idle = 0,
        Connecting = 1 << 0,
        Ready = 1 << 1,
        Shutdown = 1 << 2
    }

    public class Channel
    {
        private readonly Fsm _fsm;
        private Promise _connectPromise;

        public Channel(ServerPort portDefine)
        {
            _fsm = new Fsm();

            _fsm.In(ChannelState.Idle)
                .On<CommandConnect>((tr, evt) => tr.Switch(ChannelState.Connecting));

            _fsm.In(ChannelState.Connecting)
                .OnEnter(() =>
                {
                    _connectPromise = new Promise();
                    var sp = portDefine;
                    Singleton.Connect(sp.Address, sp.Port, sp.AppId, sp.AuthKey)
                        .Done(nc => { _fsm.Switch(ChannelState.Ready, nc); },
                            exception => { _fsm.Switch(ChannelState.Shutdown, exception); });
                });

            _fsm.In(ChannelState.Shutdown)
                .OnEnter(o =>
                {
                    _connectPromise.Reject((Exception) o);
                    _connectPromise = null;
                })
                .On<CommandConnect>((tr, evt) => tr.Switch(ChannelState.Connecting));

            INetworkChannel channel = null;
            _fsm.In(ChannelState.Ready)
                .OnEnter(o =>
                {
                    channel = (INetworkChannel) o;
                    _connectPromise.Resolve();
                })
                .OnExit(() => channel = null)
                .On<CommandSend>(send => { channel.Send(send.buffer, send.method, send.dataChannel); });

            _fsm.Start(ChannelState.Idle);
        }

        public static INetwork Singleton { get; set; }

        #region Api

        public ChannelState State => (ChannelState) _fsm.State;

        internal static int ZipCompressLevel { get; set; } = 3;

        public IPromise ConnectAsync()
        {
            _fsm.Raise(CommandConnect.Instance);
            return _connectPromise;
        }

        public void Write(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            _fsm.Raise(new CommandSend() {buffer = buffer, method = deliveryMethod, dataChannel = dataChannel});
        }

        #endregion

        class CommandConnect
        {
            public static CommandConnect Instance = new CommandConnect();
        }

        class CommandSend
        {
            public byte[] buffer;
            public NetDeliveryMethod method;
            public int dataChannel;
        }
    }
}