using System;
using System.Net;
using System.Net.Sockets;
using LiteNetLib;
using Valkyrie.Communication.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network.LiteNetLibImpl
{
    internal class LiteClient : INetworkChannel, INetEventListener
    {
        private readonly ILogInstance _log;
        private readonly MessageListener _listener;
        
        public NetManager Manager { get; }
        public NetPeer Peer { get; set; }

        public LiteClient(ILogInstance log)
        {
            _log = log;
            _listener = new MessageListener(log);
            Manager = new NetManager(this) { SimulationMaxLatency = 1500, ChannelsCount = LiteNetService.MaxChannelsCount };
            if(!Manager.Start())
                throw new Exception("Can not start network client");
        }

        private Promise _connectPromise;
        
        public IPromise Connect(string address, int port, string token)
        {
            _connectPromise = new Promise();
            Peer = Manager.Connect(address, port, token);
            return _connectPromise;
        }

        public void Update()
        {
            Manager.PollEvents();
        }

        #region INetwork channel

        public void Dispose()
        {
            Manager.Stop(true);
        }

        public float Ping => Peer?.Ping * 1000f ?? float.MaxValue;

        public IPEndPoint EndPoint => Peer?.EndPoint;

        public int TrafficCompression
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public INetworkListener Listener => _listener;

        public IObservable<NetChannelStatus> ObserveStatus()
        {
            throw new NotImplementedException();
        }

        public void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            //_log.Debug($"Client sends {buffer.Length}");
            Peer.Send(buffer, (byte) dataChannel, LiteNetService.Delivery(deliveryMethod));
        }

        public NetChannelStatus Status => throw new NotImplementedException();
        public byte[] PopMessage()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region INetEventListener

        public void OnPeerConnected(NetPeer peer)
        {
            _connectPromise?.Resolve();
            _connectPromise = null;
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            _log.Info($"OnPeerDisconnected: {disconnectInfo}");
        }

        public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
        {
            _log.Error($"OnNetworkError: {socketError}");
        }

        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            _listener.HandleIncomingMessage(LiteNetService.Decode(reader), this);
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
        {
            _log.Warn($"OnNetworkReceiveUnconnected: {messageType}");
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            //_log.Info($"OnNetworkLatencyUpdate: {latency}");
        }

        public void OnConnectionRequest(ConnectionRequest request)
        {
            //_log.Info($"OnConnectionRequest: {request}");
        }

        #endregion
    }
}