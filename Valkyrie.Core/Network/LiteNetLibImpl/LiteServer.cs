using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using LiteNetLib;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Iaps;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network.LiteNetLibImpl
{
    internal class LiteServer : MessageListener, INetEventListener, IDisposable
    {
        private readonly ILogInstance _log;
        private Func<INetworkChannel, string, ErrorCodes> _handler;

        public NetManager Manager { get; private set; }

        public LiteServer(ILogInstance log) : base(log)
        {
            _log = log;
            Manager = new NetManager(this) { ChannelsCount = LiteNetService.MaxChannelsCount };
        }

        public void Update()
        {
            Manager.PollEvents();
        }

        public IPromise Listen(int port, Func<INetworkChannel,string,ErrorCodes> handler)
        {
            _handler = handler;
            if (!Manager.Start(port))
                return Promise.Rejected(new Exception("Can not start server"));
            return Promise.Resolved();
        }

        #region INetEventListener

        public void OnPeerConnected(NetPeer peer)
        {
            _log.Info($"OnPeerConnected: {GetClient(peer)}");
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            _log.Info($"OnPeerDisconnected: {GetClient(peer)}");
        }

        public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
        {
            _log.Error($"OnNetworkError: {socketError}");
        }

        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            var cl = GetClient(peer);
            var msg = LiteNetService.Decode(reader);
            //_log.Warn($"OnNetworkReceive: {msg}");
            HandleIncomingMessage(msg, cl);
        }

        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
        {
            _log.Warn($"OnNetworkReceiveUnconnected: {remoteEndPoint}");
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            //_log.Info($"OnNetworkLatencyUpdate: {GetClient(peer)} {latency}");
        }

        public void OnConnectionRequest(ConnectionRequest request)
        {
            _log.Info($"OnConnectionRequest: {request}");
            var res = _handler.Invoke(GetClient(request.RemoteEndPoint), request.Data.GetString());
            if (res == ErrorCodes.Ok)
                request.Accept();
            else
                request.Reject();
        }

        readonly Dictionary<NetPeer, LiteServerClient> _clients = new Dictionary<NetPeer, LiteServerClient>();
        
        LiteServerClient GetClient(IPEndPoint point)
        {
            return _clients.FirstOrDefault(x => x.Key.EndPoint == point).Value;
        }
        LiteServerClient GetClient(NetPeer point)
        {
            if (!_clients.TryGetValue(point, out var res))
            {
                res = new LiteServerClient(point, _log);
                _clients.Add(point, res);
            }

            return res;
        }

        #endregion

        class LiteServerClient : INetworkChannel
        {
            private readonly MessageListener _listener;
            public NetPeer Peer { get; }

            public LiteServerClient(NetPeer peer, ILogInstance log)
            {
                Peer = peer;
                _listener = new MessageListener(log);
            }

            public void Dispose()
            {
                Peer.Disconnect();
            }

            public float Ping => Peer.Ping * 1000f;

            public IPEndPoint EndPoint => Peer.EndPoint;

            public int TrafficCompression
            {
                get => throw new NotImplementedException();
                set => throw new NotImplementedException();
            }

            public INetworkListener Listener => _listener;

            public IObservable<NetChannelStatus> ObserveStatus()
            {
                throw new NotImplementedException();
            }

            public void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
            {
                Peer.Send(buffer, (byte) dataChannel, LiteNetService.Delivery(deliveryMethod));
            }

            public NetChannelStatus Status => throw new NotImplementedException();
            public byte[] PopMessage()
            {
                throw new NotImplementedException();
            }

            public override string ToString()
            {
                return $"Peer status={Peer.ConnectionState} endPoint={Peer.EndPoint}";
            }
        }

        public void Dispose()
        {
            Manager.Stop(true);
        }
    }
}