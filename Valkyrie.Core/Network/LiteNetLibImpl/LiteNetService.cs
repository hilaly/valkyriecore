using System;
using System.Collections.Generic;
using LiteNetLib;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network.LiteNetLibImpl
{
    public class LiteNetService : INetwork, IDisposable
    {
        public int CompressLevel
        {
            get => Channel.ZipCompressLevel;
            set => Channel.ZipCompressLevel = value;
        }
        
        private ILogInstance _log;
        private IDisposable _subscription;
        
        Dictionary<string, LiteClient> _clients = new Dictionary<string, LiteClient>();
        private bool _updateClients;
        List<LiteClient> _clientsQueue = new List<LiteClient>();
        
        Dictionary<int, LiteServer> _servers = new Dictionary<int, LiteServer>();
        private bool _updateServers;
        List<LiteServer> _serversQueue = new List<LiteServer>();

        public LiteNetService(IDispatcher dispatcher, ILogService logService)
        {
            Channel.Singleton = this;

            _log = logService.GetLog("Network");
            _subscription = dispatcher.EveryUpdate(Update);
        }

        private void Update()
        {
            lock (_clients)
                if (_updateClients)
                {
                    _updateClients = false;
                    _clientsQueue.Clear();
                    _clientsQueue.AddRange(_clients.Values);
                }
            
            lock(_servers)
                if (_updateServers)
                {
                    _updateServers = false;
                    _serversQueue.Clear();
                    _serversQueue.AddRange(_servers.Values);
                }

            foreach (var liteClient in _clientsQueue)
                liteClient.Update();
            foreach (var liteServer in _serversQueue)
                liteServer.Update();
        }

        public IPromise<INetworkListener> Listen(int port, int maxIncomingConnections, string appId, Func<INetworkChannel, string, ErrorCodes> authRequestHandler)
        {
            lock (_servers)
            {
                if (!_servers.TryGetValue(port, out var server))
                {
                    server = new LiteServer(_log);
                    
                    _servers.Add(port, server);
                    _updateServers = true;
                    return server
                        .Listen(port, authRequestHandler)
                        .Then(() => Promise<INetworkListener>.Resolved(server));
                }
                
                return Promise<INetworkListener>.Resolved(server);
            }
        }

        public IPromise<INetworkChannel> Connect(string ipAddress, int port, string appId, string authToken)
        {
            var fullAddress = $"{ipAddress}:{port}";

            lock (_clients)
            {
                if (!_clients.TryGetValue(fullAddress, out var client))
                {
                    client = new LiteClient(_log);
                    
                    _clients.Add(fullAddress, client);
                    _updateClients = true;
                    return client.Connect(ipAddress, port, authToken)
                        .Then(() => Promise<INetworkChannel>.Resolved(client));
                }

                return Promise<INetworkChannel>.Resolved(client);
            }
        }

        public static DeliveryMethod Delivery(NetDeliveryMethod deliveryMethod)
        {
            switch(deliveryMethod)
            {
                case NetDeliveryMethod.Unknown:
                case NetDeliveryMethod.Unreliable:
                    return DeliveryMethod.Unreliable;
                case NetDeliveryMethod.UnreliableSequenced:
                    return DeliveryMethod.Sequenced;
                case NetDeliveryMethod.ReliableUnordered:
                    return DeliveryMethod.ReliableUnordered;
                case NetDeliveryMethod.ReliableSequenced:
                    return DeliveryMethod.ReliableSequenced;
                case NetDeliveryMethod.ReliableOrdered:
                    return DeliveryMethod.ReliableOrdered;
                default:
                    throw new ArgumentOutOfRangeException(nameof(deliveryMethod), deliveryMethod, null);
            }
        }

        public static object Decode(NetPacketReader reader)
        {
            var temp = new byte[reader.AvailableBytes];
            Array.Copy(reader.RawData, reader.UserDataOffset, temp, 0, reader.AvailableBytes);
            return RpcUtils.Serializer.Deserialize(temp);
        }

        public void Dispose()
        {
            _subscription?.Dispose();
        }
        
        internal const int MaxChannelsCount = 16;
    }
}