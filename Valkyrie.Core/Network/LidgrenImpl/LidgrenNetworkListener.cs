using System;
using System.Collections.Generic;
using Valkyrie.Data;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network.LidgrenImpl
{
    class LidgrenNetworkListener : INetworkListener
    {
        private ILogInstance _log;

        public LidgrenNetworkListener(ILogInstance log)
        {
            _log = log;
        }

        #region INetworkListener
        
        #region Helper classes

        interface IMessageHandler
        {
            void Handle(object message, INetworkChannel channel);
        }

        class TypedMessageHandler<T> : IMessageHandler
        {
            public TypedMessageHandler(ISubject<NetworkMessageInstance<T>> subject)
            {
                Subject = subject;
            }

            public ISubject<NetworkMessageInstance<T>> Subject { get; }

            public void Handle(object message, INetworkChannel channel)
            {
                Subject.OnNext(new NetworkMessageInstance<T>((T) message, channel));
            }
        }

        #endregion

        private readonly Dictionary<Type, IMessageHandler> _inChannels = new Dictionary<Type, IMessageHandler>();

        public IObservable<NetworkMessageInstance<T>> In<T>()
        {
            return GetChannel<T>();
        }

        ISubject<NetworkMessageInstance<T>> GetChannel<T>()
        {
            lock (_inChannels)
            {
                if (_inChannels.TryGetValue(typeof(T), out var temp))
                    return ((TypedMessageHandler<T>) temp).Subject;

                var result = new Subject<NetworkMessageInstance<T>> {CompleteOnError = false};
                _inChannels.Add(typeof(T), new TypedMessageHandler<T>(result));
                return result;
            }
        }

        public void HandleIncomingMessage(object message, INetworkChannel networkChannel)
        {
            IMessageHandler messageHandler;
            lock (_inChannels)
            {
                if (!_inChannels.TryGetValue(message.GetType(), out messageHandler))
                    _log.Warn($"Unhandled message {message}: {message.ToJson()}");
            }

            if (messageHandler == null)
                return;
            
            try
            {
                messageHandler.Handle(message, networkChannel);
            }
            catch (Exception e)
            {
                _log.Error($"Exception during handle {message}[{message.ToJson()}]: {e}");
            }
        }

        
        #endregion
    }
}