using System;
using System.Collections.Generic;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network.LidgrenImpl
{
    class LidgrenIncomingMessagesQueue
    {
        public struct MessageStore
        {
            public NetIncomingMessage Message;
            public LidgrenBaseChannel Connection;
        }

        private readonly List<MessageStore> _messagesStore = new List<MessageStore>();
        private readonly ILogInstance _logger;
        private readonly Action<NetConnection> _closeConnectionIfAny;
        private readonly Func<NetConnection, INetworkListener, LidgrenBaseChannel> _establishConnectionIfNeed;

        public LidgrenIncomingMessagesQueue(ILogInstance log, Func<NetConnection, INetworkListener, LidgrenBaseChannel> establishConnectionIfNeed, Action<NetConnection> closeConnectionIfAny)
        {
            _logger = log;
            _establishConnectionIfNeed = establishConnectionIfNeed;
            _closeConnectionIfAny = closeConnectionIfAny;
        }

        public INetworkListener Listener { private get; set; }

        public MessageStore[] PopQueue()
        {
            MessageStore[] messagesToProcess;
            lock (_messagesStore)
            {
                messagesToProcess = _messagesStore.ToArray();
                _messagesStore.Clear();
            }

            return messagesToProcess;
        }

        public void ReceiveIncomingMessage(object netPeer)
        {
            var peer = (NetPeer) netPeer;

            NetIncomingMessage im;
            while ((im = peer.ReadMessage()) != null)
            {
                switch (im.MessageType)
                {
                    case NetIncomingMessageType.VerboseDebugMessage:
                        _logger.Info(im.ReadString());
                        break;
                    case NetIncomingMessageType.DebugMessage:
                        _logger.Debug(im.ReadString());
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        _logger.Warn(im.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        _logger.Error(im.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        var status = (NetConnectionStatus) im.ReadByte();
                        var reason = im.ReadString();
                        _logger.Debug(status + ": " + reason);

                        //TODO: handle disconnecting
                        switch (status)
                        {
                            case NetConnectionStatus.None:
                                break;
                            case NetConnectionStatus.InitiatedConnect:
                                break;
                            case NetConnectionStatus.ReceivedInitiation:
                                break;
                            case NetConnectionStatus.RespondedAwaitingApproval:
                                break;
                            case NetConnectionStatus.RespondedConnect:
                                break;
                            case NetConnectionStatus.Connected:
                                _establishConnectionIfNeed(im.SenderConnection, Listener);
                                break;
                            case NetConnectionStatus.Disconnecting:
                                break;
                            case NetConnectionStatus.Disconnected:
                                _closeConnectionIfAny(im.SenderConnection);
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        break;
                    case NetIncomingMessageType.Data:
                        OnIncomingData(im, _establishConnectionIfNeed(im.SenderConnection, Listener));
                        break;
                    default:
                        _logger.Warn("Unhandled type: " + im.MessageType + " " + im.LengthBytes + " bytes");
                        break;
                }
            }
        }

        private void OnIncomingData(NetIncomingMessage im, LidgrenBaseChannel connection)
        {
            lock (_messagesStore)
                _messagesStore.Add(new MessageStore {Connection = connection, Message = im});
        }
    }
}