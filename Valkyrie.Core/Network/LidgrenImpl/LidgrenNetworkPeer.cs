using System;
using System.Threading;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.Rpc;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network.LidgrenImpl
{
    class LidgrenNetworkPeer : LidgrenNetworkListener, IDisposable, INetworkListener
    {
        private readonly LidgrenIncomingMessagesQueue _messagesQueue;
        private NetPeerStatus _lastStatus;
        private IContractSerializer _contractSerializer;
        public NetPeer Peer { get; private set; }

        public event Action<LidgrenNetworkPeer> OnStatusChanged;

        public LidgrenNetworkPeer(NetPeerConfiguration cfg, SynchronizationContext ctx,
            LidgrenIncomingMessagesQueue messagesQueue, ILogInstance log, IContractSerializer contractSerializer)
            : base(log)
        {
            _messagesQueue = messagesQueue;
            _messagesQueue.Listener = this;
            _contractSerializer = contractSerializer;
            if (cfg.Port > 0)
                Peer = new NetServer(cfg);
            else
                Peer = new NetClient(cfg);

            _lastStatus = Peer.Status;

            Peer.RegisterReceivedCallback(messagesQueue.ReceiveIncomingMessage, ctx);
            Peer.Start();
        }

        public void Dispose()
        {
            if (Peer == null) return;
            Peer.Shutdown("buy");
            Peer = null;
        }

        public void CheckStatus()
        {
            var ns = Peer.Status;
            if (ns == _lastStatus)
                return;
            _lastStatus = ns;
            var h = OnStatusChanged;
            h?.Invoke(this);
        }

        public void Iteration()
        {
            Peer.FlushSendQueue();

            CheckStatus();
        }

        public void HandleIncoming()
        {
            foreach (var s in _messagesQueue.PopQueue())
            {
                var store = s;
                var connection = store.Connection;
                var rawBytes = store.Message.ReadBytes(store.Message.LengthBytes);
                var msgBytes = rawBytes.UnzipToByteArray(DataExtensions.LidgrenNetworkZipEntryName);

                var packer = new SimplePacker();
                packer.Load(msgBytes);
                packer.ReadByte();
                switch ((RpcUtils.NetworkDataType) msgBytes[0])
                {
                    case RpcUtils.NetworkDataType.Rpc:
                        HandleIncomingMessage(_contractSerializer.Deserialize<RpcMessage>(packer.ReadBytes()),
                            connection);
                        break;
                    case RpcUtils.NetworkDataType.Contract:
                        HandleIncomingMessage(_contractSerializer.Deserialize(packer.ReadBytes()), connection);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}