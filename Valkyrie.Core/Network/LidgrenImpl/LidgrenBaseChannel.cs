﻿using System;
using System.Collections.Generic;
using System.Net;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;
using Valkyrie.Network;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network.LidgrenImpl
{
    class ChannelListener : INetworkListener, IDisposable
    {
        private readonly CompositeDisposable _disposable = new CompositeDisposable();

        private readonly INetworkListener _source;
        private readonly INetworkChannel _channel;

        public ChannelListener(INetworkListener source, INetworkChannel channel)
        {
            _source = source;
            _channel = channel;
        }

        public IObservable<NetworkMessageInstance<T>> In<T>()
        {
            return _source.In<T>().Where(u => u.Channel == _channel);
        }

        public void Dispose()
        {
            _disposable.Dispose();
        }
    }

    class LidgrenBaseChannel : INetworkChannel
    {
        private readonly IContractSerializer _contractSerializer;
        private readonly IDispatcher _dispatcher;
        private readonly Dictionary<Type, object> _channels = new Dictionary<Type, object>();
        private readonly Subject<NetChannelStatus> _status = new Subject<NetChannelStatus>();
        private bool _statusChanged;
        private IDisposable _subscription;
        private NetConnectionStatus _netStatus;
        private ILogInstance _log;

        public event Action OnStatusChanged;

        public NetConnection LidgrenConnection { get; }
        public INetworkListener Listener { get; }

        public IObservable<NetChannelStatus> ObserveStatus()
        {
            return _status;
        }

        public NetConnectionStatus Status => _netStatus;
        public byte[] PopMessage()
        {
            throw new NotImplementedException();
        }

        NetChannelStatus IClient.Status => Valkyrie.Network.Utils.Convert(Status);

        public IPEndPoint EndPoint => LidgrenConnection.RemoteEndPoint;

        public int TrafficCompression
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public string AuthToken { get; set; }
        public float Ping => LidgrenConnection.AverageRoundtripTime;

        public LidgrenBaseChannel(NetConnection innerConnection, ILogInstance log,
            IContractSerializer contractSerializer, INetworkListener listener, IDispatcher dispatcher)
        {
            _log = log;
            _contractSerializer = contractSerializer;
            _dispatcher = dispatcher;
            LidgrenConnection = innerConnection;
            Listener = new ChannelListener(listener, this);

            _netStatus = LidgrenConnection.Status;
            _subscription = dispatcher.EveryEndIteration(UpdateStatus);
        }

        internal void UpdateStatus()
        {
            if (_netStatus != LidgrenConnection.Status)
                _dispatcher.ExecuteOnMainThread(OnInnerStatusChanged);
        }

        private void OnInnerStatusChanged()
        {
            _netStatus = LidgrenConnection.Status;
            OnStatusChanged?.Invoke();
            _statusChanged = true;
            CheckStatus();
        }

        public void Dispose()
        {
            ((ChannelListener) Listener).Dispose();
            _subscription.Dispose();
            LidgrenConnection.Disconnect("disposed");
            _status.Dispose();
        }

        public void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            var zippedBuffer = buffer.ZipToByteArray(DataExtensions.LidgrenNetworkZipEntryName, Channel.ZipCompressLevel);
            var im = LidgrenConnection.Peer.CreateMessage(zippedBuffer.Length);
            im.Write(zippedBuffer);
            LidgrenConnection.SendMessage(im, LidgrenNetwork.Convert(deliveryMethod), dataChannel);

            //_log.Warn($"[send]: size={zippedBuffer.Length} original={buffer.Length}");
        }

        public override string ToString()
        {
            return LidgrenConnection.ToString();
        }

        public void CheckStatus()
        {
            if (!_statusChanged)
                return;
            _statusChanged = false;
            switch (Status)
            {
                case NetConnectionStatus.InitiatedConnect:
                case NetConnectionStatus.ReceivedInitiation:
                case NetConnectionStatus.RespondedAwaitingApproval:
                case NetConnectionStatus.RespondedConnect:
                    _status.OnNext(NetChannelStatus.Connecting);
                    break;
                case NetConnectionStatus.Connected:
                    _status.OnNext(NetChannelStatus.Connected);
                    break;
                case NetConnectionStatus.Disconnecting:
                    _status.OnNext(NetChannelStatus.Disconnecting);
                    break;
                case NetConnectionStatus.None:
                case NetConnectionStatus.Disconnected:
                    _status.OnNext(NetChannelStatus.Disconnected);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}