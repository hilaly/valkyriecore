﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Valkyrie.Communication.Network.Lidgren;
using Valkyrie.Data.Serialization;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network.LidgrenImpl
{
    internal sealed class LidgrenNetwork : AbstractNetwork
    {
        private readonly List<LidgrenNetworkPeer> _peers = new List<LidgrenNetworkPeer>();
        private readonly Dictionary<NetConnection, LidgrenBaseChannel> _lidgrenConnections = new Dictionary<NetConnection, LidgrenBaseChannel>();
        private readonly IContractSerializer _contractSerializer;
        private IDispatcher _dispatcher;
        private Scheduler _scheduler;

        public LidgrenNetwork(IDispatcher dispatcher, ILogService logService, IContractSerializer contractSerializer,
            MetaInfo appInfo, Scheduler scheduler)
            : base(logService, dispatcher)
        {
            _dispatcher = dispatcher;
            _contractSerializer = contractSerializer;
            _scheduler = scheduler;
            _scheduler.Run(UpdateStatuses, TimeSpan.FromSeconds(1f), TimeSpan.FromSeconds(1f / 30));
        }

        private IPromise<LidgrenNetworkPeer> BindPeer(int port, int maxConnections, string appId)
        {
            var configuration = new NetPeerConfiguration(appId)
            {
                AutoFlushSendQueue = true,
                Port = port,
                MaximumConnections = maxConnections
            };

            try
            {
                var peer = new LidgrenNetworkPeer(configuration, SynchronizationContext.Current, new LidgrenIncomingMessagesQueue(Log, EstablishConnectionIfNeed, CloseConnectionIfAny),
                    Log, _contractSerializer);
                lock (_peers)
                {
                    _peers.Add(peer);
                }

                Disposable.Add(peer);
            
                var result = new Promise<LidgrenNetworkPeer>();

                void OnPeerOnOnStatusChanged(LidgrenNetworkPeer networkPeer)
                {
                    switch (networkPeer.Peer.Status)
                    {
                        case NetPeerStatus.Starting:
                            break;
                        case NetPeerStatus.Running:
                            result.Resolve(peer);
                            peer.OnStatusChanged -= OnPeerOnOnStatusChanged;
                            break;
                        case NetPeerStatus.NotRunning:
                        case NetPeerStatus.ShutdownRequested:
                            result.Reject(new Exception($"Failed to bind to port {port}"));
                            peer.OnStatusChanged -= OnPeerOnOnStatusChanged;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                peer.OnStatusChanged += OnPeerOnOnStatusChanged;
                return result;
            }
            catch (Exception e)
            {
                Log.Error($"Failed to bing network to {port}, {e}");
                return Promise<LidgrenNetworkPeer>.Rejected(e);
            }
        }

        protected override IPromise<INetworkListener> Bind(int port, int maxConnections, string appId)
        {
            lock (_peers)
            {
                var createdPeer = _peers.Find(u => u.Peer.Port == port);
                if (createdPeer != null)
                    return Promise<INetworkListener>.Resolved(createdPeer);
            }

            return BindPeer(port, maxConnections, appId).Then(peer => (INetworkListener) peer);
        }

        protected override IPromise<INetworkChannel> CreateChannel(IPEndPoint endPoint, string appId)
        {
            return BindPeer(0, 1, appId).Then(networkPeer =>
            {
                var peer = networkPeer.Peer;
                var c = peer.Connect(endPoint, peer.CreateMessage(appId));
                var connection = EstablishConnectionIfNeed(c, networkPeer);

                var result = new Promise<INetworkChannel>();

                void OnConnectionOnStatusChanged()
                {
                    Log.Debug($"{connection}, {connection.Status}");
                    switch(connection.Status)
                    {
                        case NetConnectionStatus.None:
                            break;
                        case NetConnectionStatus.InitiatedConnect:
                            break;
                        case NetConnectionStatus.ReceivedInitiation:
                            break;
                        case NetConnectionStatus.RespondedAwaitingApproval:
                            break;
                        case NetConnectionStatus.RespondedConnect:
                            break;
                        case NetConnectionStatus.Connected:
                            connection.OnStatusChanged -= OnConnectionOnStatusChanged;
                            result.Resolve(connection);
                            break;
                        case NetConnectionStatus.Disconnecting:
                            break;
                        case NetConnectionStatus.Disconnected:
                            connection.OnStatusChanged -= OnConnectionOnStatusChanged;
                            result.Reject(new Exception("Connection failed"));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                connection.OnStatusChanged += OnConnectionOnStatusChanged;
                return (IPromise<INetworkChannel>)result;
            });
        }

        protected override void Iteration()
        {
//            foreach (var s in _incomingMessagesQueue.PopQueue())
//            {
//                var store = s;
//                var connection = store.Connection;
//                var msgBytes = store.Message.ReadBytes(store.Message.LengthBytes);
//                var msgData = _contractSerializer.Deserialize(msgBytes);
//
//                HandleIncomingMessage(msgData, connection);
//            }

            lock (_peers)
            {
                var ppers = _peers.ToArray();
                foreach (var peer in ppers)
                    peer.HandleIncoming();
                foreach (var peer in ppers)
                    peer.Peer?.FlushSendQueue();

                foreach (var t in _peers.ToArray())
                {
                    if (t.Peer == null)
                        _peers.Remove(t);
                    else
                        t.CheckStatus();
                }
                
//                foreach (var pair in _lidgrenConnections.ToArray())
//                    pair.Value.CheckStatus();
            }
        }

        #region Handling messages
        
        private void UpdateStatuses()
        {
            lock (_lidgrenConnections)
            {
                foreach (var connection in _lidgrenConnections)
                {
                    connection.Value.UpdateStatus();
                }
            }
        }

        private LidgrenBaseChannel EstablishConnectionIfNeed(NetConnection lidgrenConnection, INetworkListener listener)
        {
            LidgrenBaseChannel bc;
            
            lock (_lidgrenConnections)
            {
                if (_lidgrenConnections.TryGetValue(lidgrenConnection, out bc))
                    return bc;

                bc = new LidgrenBaseChannel(lidgrenConnection, Log, _contractSerializer, listener, _dispatcher);
                _lidgrenConnections.Add(bc.LidgrenConnection, bc);
            }
            
            HandleChannelCreation(bc);

            return bc;
        }

        private void CloseConnectionIfAny(NetConnection lidgrenConnection)
        { 
            LidgrenBaseChannel baseChannel;

            lock (_lidgrenConnections)
            {
                if (_lidgrenConnections.TryGetValue(lidgrenConnection, out baseChannel))
                    _lidgrenConnections.Remove(baseChannel.LidgrenConnection);
            }

            if(baseChannel != null)
                HandleChannelDestroying(baseChannel);
        }

        #endregion

        internal static NetDeliveryMethod Convert(NetDeliveryMethod m)
        {
            switch (m)
            {
                case NetDeliveryMethod.Unknown:
                    return NetDeliveryMethod.Unknown;
                case NetDeliveryMethod.Unreliable:
                    return NetDeliveryMethod.Unreliable;
                case NetDeliveryMethod.UnreliableSequenced:
                    return NetDeliveryMethod.UnreliableSequenced;
                case NetDeliveryMethod.ReliableUnordered:
                    return NetDeliveryMethod.ReliableUnordered;
                case NetDeliveryMethod.ReliableSequenced:
                    return NetDeliveryMethod.ReliableSequenced;
                case NetDeliveryMethod.ReliableOrdered:
                    return NetDeliveryMethod.ReliableOrdered;
                default:
                    throw new ArgumentOutOfRangeException(nameof(m), m, null);
            }
        }
    }
}