﻿using System;
using System.Net;
using Valkyrie.Network;

namespace Valkyrie.Communication.Network
{
    public interface INetworkChannel : IClient
    {
        INetworkListener Listener { get; }
        IObservable<NetChannelStatus> ObserveStatus();
    }
}