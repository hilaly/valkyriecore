using System;
using System.Collections.Generic;
using System.Net;
using MiniUDP;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network.MiniUdpImpl
{
    class MiniUdpServerListener : MessageListener, IDisposable, IMiniUdpListener
    {
        private readonly ILogInstance _logInstance;
        private readonly Func<INetworkChannel, string, ErrorCodes> _connectionHandler;
        public Connector Connector { get; }

        public MiniUdpServerListener(Connector connector, ILogInstance logInstance,
            Func<INetworkChannel, string, ErrorCodes> connectionHandler)
            : base(logInstance)
        {
            _logInstance = logInstance;
            _connectionHandler = connectionHandler;
            Connector = connector;
            Connector.MiniUdpListener = this;
        }

        public void Host(int port)
        {
            Connector.Host(port);
        }

        public void Dispose()
        {
            Connector.Stop();
        }

        #region MiniUdpListener
        
        Dictionary<NetPeer, MiniUdpPeerChannel> _channels = new Dictionary<NetPeer, MiniUdpPeerChannel>();

        MiniUdpPeerChannel GetChannel(NetPeer peer)
        {
            if (!_channels.TryGetValue(peer, out var channel))
            {
                _channels.Add(peer, channel = new MiniUdpPeerChannel(_logInstance) {Peer = peer});
                
                _logInstance.Info($"Open connection {peer.EndPoint.Address}:{peer.EndPoint.Port}");
            }
            return channel;
        }

        public void OnPeerConnected(NetPeer peer, string token)
        {
            var channel = GetChannel(peer);
            var result = this._connectionHandler(channel, token);
            switch (result)
            {
                case ErrorCodes.Ok:
                    return;
                default:
                    peer.Close((byte) result);
                    return;
            }
        }

        public void OnPeerClosed(NetPeer peer)
        {
            _logInstance.Info($"Close connection {peer.EndPoint.Address}:{peer.EndPoint.Port}");
            _channels.Remove(peer);
        }

        public void OnDataReceived(NetPeer peer, byte[] data, int length)
        {
            var channel = GetChannel(peer);
            this.HandleIncomingMessage(MiniUdpService.Decode(data, length), channel);
            //channel.OnDataReceived(channel.Peer, data, length);
        }

        #endregion
    }

    class MiniUdpPeerChannel : INetworkChannel
    {
        private readonly MessageListener _listener;
        public NetPeer Peer { get; set; }

        public float Ping => Peer?.Traffic.Ping ?? float.MaxValue;

        public IPEndPoint EndPoint => Peer?.EndPoint;

        public int TrafficCompression
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        public INetworkListener Listener => _listener;

        public MiniUdpPeerChannel(ILogInstance logInstance)
        {
            _listener = new MessageListener(logInstance);
        }

        public virtual void Dispose()
        {
        }

        public IObservable<NetChannelStatus> ObserveStatus()
        {
            throw new NotImplementedException();
        }

        public void Send(byte[] buffer, NetDeliveryMethod deliveryMethod, int dataChannel)
        {
            switch (deliveryMethod)
            {
                case NetDeliveryMethod.Unknown:
                case NetDeliveryMethod.Unreliable:
                case NetDeliveryMethod.UnreliableSequenced:
                    Peer.SendPayload(buffer, (ushort) buffer.Length);
                    break;
                case NetDeliveryMethod.ReliableUnordered:
                case NetDeliveryMethod.ReliableSequenced:
                case NetDeliveryMethod.ReliableOrdered:
                    Peer.QueueNotification(buffer, (ushort) buffer.Length);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(deliveryMethod), deliveryMethod, null);
            }
        }

        public NetChannelStatus Status => throw new NotImplementedException();
        public byte[] PopMessage()
        {
            throw new NotImplementedException();
        }

        public void OnDataReceived(NetPeer peer, byte[] data, int length)
        {
            _listener.HandleIncomingMessage(MiniUdpService.Decode(data, length), this);
        }
    }
}