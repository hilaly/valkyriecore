using System;
using System.Collections.Generic;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network.MiniUdpImpl
{
    class MiniUdpService : INetwork, IDisposable
    {
        public int CompressLevel
        {
            get => Channel.ZipCompressLevel;
            set => Channel.ZipCompressLevel = value;
        }
        
        readonly Dictionary<int, MiniUdpServerListener> _servers = new Dictionary<int, MiniUdpServerListener>();
        private readonly List<MiniUdpServerListener> _updateQueueServers = new List<MiniUdpServerListener>();
        private bool _updateServers;

        readonly Dictionary<string, MiniUdpClient> _clients = new Dictionary<string, MiniUdpClient>();
        private readonly List<MiniUdpClient> _clientsQueue = new List<MiniUdpClient>();
        private bool _updateClients;

        private readonly IDisposable _updateSubscription;
        private readonly ILogInstance _log;

        public MiniUdpService(IDispatcher dispatcher, ILogService log)
        {
            Channel.Singleton = this;
            
            _log = log.GetLog("Network");
            _updateSubscription = dispatcher.EveryUpdate(Update);
        }

        public IPromise<INetworkListener> Listen(int port, int maxIncomingConnections, string appId,
            Func<INetworkChannel, string, ErrorCodes> authRequestHandler)
        {
            lock (_servers)
            {
                if (!_servers.TryGetValue(port, out var listener))
                {
                    listener = new MiniUdpServerListener(new Connector(appId, true), _log, authRequestHandler);
                    _servers.Add(port, listener);
                    listener.Host(port);
                    _updateServers = true;
                }

                return Promise<INetworkListener>.Resolved(listener);
            }
        }

        public IPromise<INetworkChannel> Connect(string ipAddress, int port, string appId, string authToken)
        {
            var fullAddress = $"{ipAddress}:{port}";
            lock (_clients)
            {
                if (!_clients.TryGetValue(fullAddress, out var client))
                {
                    client = new MiniUdpClient(new Connector(appId, false), _log);
                    _clients.Add(fullAddress, client);
                    _updateClients = true;
                    return client
                        .Connect(fullAddress, authToken)
                        .Then(() => Promise<INetworkChannel>.Resolved(client));
                }

                return Promise<INetworkChannel>.Resolved(client);
            }
        }

        void Update()
        {
            lock (_updateQueueServers)
            {
                if (_updateServers)
                {
                    _updateServers = false;
                    _updateQueueServers.Clear();
                    _updateQueueServers.AddRange(_servers.Values);
                }
            }

            lock (_clientsQueue)
            {
                if (_updateClients)
                {
                    _updateClients = false;
                    _clientsQueue.Clear();
                    _clientsQueue.AddRange(_clients.Values);
                }
            }

            foreach (var server in _updateQueueServers)
                server.Connector.Update();
            foreach (var server in _clientsQueue)
                server.Connector.Update();
        }

        public void Dispose()
        {
            _updateSubscription?.Dispose();
        }

        internal static object Decode(byte[] buffer, int length)
        {
            return RpcUtils.Serializer.Deserialize(buffer);
        }
    }
}