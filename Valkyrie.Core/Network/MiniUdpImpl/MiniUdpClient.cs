using System;
using System.Net;
using MiniUDP;
using Valkyrie.Communication.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Network.MiniUdpImpl
{
    class MiniUdpClient : MiniUdpPeerChannel, IMiniUdpListener
    {
        private readonly ILogInstance _logInstance;
        public Connector Connector { get; }
        public MiniUdpClient(Connector connector, ILogInstance logInstance)
            : base(logInstance)
        {
            _logInstance = logInstance;
            Connector = connector;
            Connector.MiniUdpListener = this;
        }

        public override void Dispose()
        {
            Connector?.Stop();
        }

        private Promise _connectPromise;
        
        public IPromise Connect(string address, string token)
        {
            _connectPromise = new Promise();
            Peer = Connector.Connect(address, token);
            return _connectPromise;
        }

        #region MiniUdpListener

        public void OnPeerConnected(NetPeer peer, string token)
        {
            _logInstance.Info($"Open connection {peer.EndPoint.Address}:{peer.EndPoint.Port}");
            _connectPromise?.Resolve();
            _connectPromise = null;
        }

        public void OnPeerClosed(NetPeer peer)
        {
            _logInstance.Info($"Close connection {peer.EndPoint.Address}:{peer.EndPoint.Port}");
        }


        #endregion
    }
}