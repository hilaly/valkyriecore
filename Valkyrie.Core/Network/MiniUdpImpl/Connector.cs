using System;
using System.Net.Sockets;
using System.Text;
using MiniUDP;

namespace Valkyrie.Network.MiniUdpImpl
{
    interface IMiniUdpListener
    {
        void OnPeerConnected(NetPeer peer, string token);
        void OnPeerClosed(NetPeer peer);

        void OnDataReceived(NetPeer peer, byte[] data, int length);
    }
    
    class Connector
    {
        private readonly bool _allowConnections;
        private readonly NetCore _connection;
        
        public IMiniUdpListener MiniUdpListener { get; set; }

        public Connector(string version, bool allowConnections)
        {
            _allowConnections = allowConnections;
            this._connection = new NetCore(version, allowConnections);
            this._connection.PeerConnected += Connection_PeerConnected;
            this._connection.PeerClosed += Connection_PeerClosed;
        }

        public void Update()
        {
            this._connection.PollEvents();
        }

        private void Connection_PeerConnected(NetPeer peer, string token)
        {
            Console.WriteLine(peer.EndPoint + " peer connected: " + token);

            MiniUdpListener?.OnPeerConnected(peer, token);

            if (_allowConnections)
            {
                peer.PayloadReceived += Peer_PayloadReceived;
                peer.NotificationReceived += Peer_NotificationReceived;
            }
        }

        private void Connection_PeerClosed(NetPeer peer, NetCloseReason reason, byte userKickReason, SocketError error)
        {
            Console.WriteLine("Peer closed due to reason: " + reason);

            MiniUdpListener?.OnPeerClosed(peer);
        }

        private void Peer_PayloadReceived(NetPeer peer, byte[] data, int dataLength)
        {
            Console.WriteLine(peer.EndPoint + " got payload: \"" + Encoding.UTF8.GetString(data, 0, dataLength) + "\"");
            
            MiniUdpListener?.OnDataReceived(peer, data, dataLength);
        }

        private void Peer_NotificationReceived(NetPeer peer, byte[] data, int dataLength)
        {
            Console.WriteLine(peer.EndPoint + " got notification: \"" + Encoding.UTF8.GetString(data, 0, dataLength) +
                              "\"");
            Console.WriteLine(
                peer.Traffic.Ping + "ms " +
                (peer.Traffic.LocalLoss * 100.0f) + "% " +
                (peer.Traffic.RemoteLoss * 100.0f) + "% " +
                (peer.Traffic.LocalDrop * 100.0f) + "% " +
                (peer.Traffic.RemoteDrop * 100.0f) + "%");
            
            MiniUdpListener?.OnDataReceived(peer, data, dataLength);
        }

        public void Host(int port)
        {
            this._connection.Host(port);
        }

        public NetPeer Connect(string address, string token = "")
        {
            NetPeer host =
                this._connection.Connect(NetUtil.StringToEndPoint(address), token);

            host.PayloadReceived += Peer_PayloadReceived;
            host.NotificationReceived += Peer_NotificationReceived;

            return host;
        }

        public void Stop()
        {
            this._connection.Stop();
        }
    }
}