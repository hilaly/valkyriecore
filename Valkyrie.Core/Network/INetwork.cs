﻿using System;
using System.Collections.Generic;
using Valkyrie.Data;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Communication.Network
{
    public interface INetwork
    {
        /// <summary>
        /// 0-9, 9 is max
        /// </summary>
        public int CompressLevel { get; set; }
        //TODO: log level

        IPromise<INetworkListener> Listen(int port, int maxIncomingConnections, string appId,
            Func<INetworkChannel, string, ErrorCodes> authRequestHandler);
        IPromise<INetworkChannel> Connect(string ipAddress, int port, string appId, string authToken);
    }

    public interface INetworkListener
    {
        IObservable<NetworkMessageInstance<T>> In<T>();
    }

    class MessageListener : INetworkListener
    {
        interface IMessageHandler
        {
            void Handle(object message, INetworkChannel channel);
        }

        class TypedMessageHandler<T> : IMessageHandler
        {
            public TypedMessageHandler(ISubject<NetworkMessageInstance<T>> subject)
            {
                Subject = subject;
            }

            public ISubject<NetworkMessageInstance<T>> Subject { get; }

            public void Handle(object message, INetworkChannel channel)
            {
                Subject.OnNext(new NetworkMessageInstance<T>((T) message, channel));
            }
        }

        private readonly Dictionary<Type, IMessageHandler> _inChannels = new Dictionary<Type, IMessageHandler>();
        private readonly ILogInstance _log;

        public MessageListener(ILogInstance log)
        {
            _log = log;
        }

        public IObservable<NetworkMessageInstance<T>> In<T>()
        {
            return GetChannel<T>();
        }

        ISubject<NetworkMessageInstance<T>> GetChannel<T>()
        {
            lock (_inChannels)
            {
                if (_inChannels.TryGetValue(typeof(T), out var temp))
                    return ((TypedMessageHandler<T>) temp).Subject;

                var result = new Subject<NetworkMessageInstance<T>> {CompleteOnError = false};
                _inChannels.Add(typeof(T), new TypedMessageHandler<T>(result));
                return result;
            }
        }

        public void HandleIncomingMessage(object message, INetworkChannel networkChannel)
        {
            IMessageHandler messageHandler;
            lock (_inChannels)
            {
                if (!_inChannels.TryGetValue(message.GetType(), out messageHandler))
                    _log.Warn($"Unhandled message {message}: {message.ToJson()}");
            }

            if (messageHandler == null)
                return;
            
            try
            {
                messageHandler.Handle(message, networkChannel);
            }
            catch (Exception e)
            {
                _log.Error($"Exception during handle {message}[{message.ToJson()}]: {e}");
            }
        }
    }
}