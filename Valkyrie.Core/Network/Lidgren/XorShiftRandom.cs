﻿namespace Valkyrie.Communication.Network.Lidgren
{
    /// <summary>
    /// Xor Shift based random
    /// </summary>
    public sealed class XorShiftRandom : NetRandom
    {
        /// <summary>
        /// Get global instance of XorShiftRandom
        /// </summary>
        public new static readonly XorShiftRandom Instance = new XorShiftRandom();

        private const uint CX = 123456789;
        private const uint CY = 362436069;
        private const uint CZ = 521288629;
        private const uint CW = 88675123;

        private uint _mX, _mY, _mZ, _mW;

        /// <summary>
        /// Constructor with randomized seed
        /// </summary>
        public XorShiftRandom()
        {
            Initialize(NetRandomSeed.GetUInt64());
        }

        /// <summary>
        /// Constructor with provided 64 bit seed
        /// </summary>
        
        public XorShiftRandom(ulong seed)
        {
            Initialize(seed);
        }

        /// <summary>
        /// (Re)initialize this instance with provided 32 bit seed
        /// </summary>
        
        public override void Initialize(uint seed)
        {
            _mX = seed;
            _mY = CY;
            _mZ = CZ;
            _mW = CW;
        }

        /// <summary>
        /// (Re)initialize this instance with provided 64 bit seed
        /// </summary>
        
        public void Initialize(ulong seed)
        {
            _mX = (uint)seed;
            _mY = CY;
            _mZ = (uint)(seed << 32);
            _mW = CW;
        }

        /// <summary>
        /// Generates a random value from UInt32.MinValue to UInt32.MaxValue, inclusively
        /// </summary>
        
        public override uint NextUInt32()
        {
            var t = _mX ^ (_mX << 11);
            _mX = _mY; _mY = _mZ; _mZ = _mW;
            return _mW = _mW ^ (_mW >> 19) ^ t ^ (t >> 8);
        }
    }
}