﻿using System;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Class for generating random seeds
	/// </summary>
	public static class NetRandomSeed
	{
		private static int _mSeedIncrement = -1640531527;

		/// <summary>
		/// Generates a 32 bit random seed
		/// </summary>
		
		public static uint GetUInt32()
		{
			var seed = GetUInt64();
			var low = (uint)seed;
			var high = (uint)(seed >> 32);
			return low ^ high;
		}

		/// <summary>
		/// Generates a 64 bit random seed
		/// </summary>
		
		public static ulong GetUInt64()
		{
#if !__ANDROID__ && !IOS && !UNITY_WEBPLAYER && !UNITY_ANDROID && !UNITY_IPHONE
			var seed = (ulong)System.Diagnostics.Stopwatch.GetTimestamp();
			seed ^= (ulong)Environment.WorkingSet;
			var s2 = (ulong)Interlocked.Increment(ref _mSeedIncrement);
			s2 |= (ulong)Guid.NewGuid().GetHashCode() << 32;
			seed ^= s2;
#else
			ulong seed = (ulong)Environment.TickCount;
			seed |= (((ulong)(new object().GetHashCode())) << 32);
			ulong s2 = (ulong)Guid.NewGuid().GetHashCode();
            s2 |= (((ulong)Interlocked.Increment(ref _mSeedIncrement)) << 32);
			seed ^= s2;
#endif
			return seed;
		}
	}
}
