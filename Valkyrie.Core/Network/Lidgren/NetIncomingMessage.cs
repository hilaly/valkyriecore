﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

using System.Diagnostics;
using System.Net;
using Valkyrie.Communication.Network.Lidgren.Encryption;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Incoming message either sent from a remote peer or generated within the library
	/// </summary>
	[DebuggerDisplay("Type={MessageType} LengthBits={LengthBits}")]
	public sealed class NetIncomingMessage : NetBuffer
	{
		internal NetIncomingMessageType MIncomingMessageType;
		internal IPEndPoint MSenderEndPoint;
		internal NetConnection MSenderConnection;
		internal int MSequenceNumber;
		internal NetMessageType MReceivedMessageType;
		internal bool MIsFragment;
		internal double MReceiveTime;

		/// <summary>
		/// Gets the type of this incoming message
		/// </summary>
		public NetIncomingMessageType MessageType { get { return MIncomingMessageType; } }

		/// <summary>
		/// Gets the delivery method this message was sent with (if user data)
		/// </summary>
		public NetDeliveryMethod DeliveryMethod { get { return NetUtility.GetDeliveryMethod(MReceivedMessageType); } }

		/// <summary>
		/// Gets the sequence channel this message was sent with (if user data)
		/// </summary>
		public int SequenceChannel { get { return (int)MReceivedMessageType - (int)NetUtility.GetDeliveryMethod(MReceivedMessageType); } }

		/// <summary>
		/// IPEndPoint of sender, if any
		/// </summary>
		public IPEndPoint SenderEndPoint { get { return MSenderEndPoint; } }

		/// <summary>
		/// NetConnection of sender, if any
		/// </summary>
		public NetConnection SenderConnection { get { return MSenderConnection; } }

		/// <summary>
		/// What local time the message was received from the network
		/// </summary>
		public double ReceiveTime { get { return MReceiveTime; } }

		internal NetIncomingMessage()
		{
		}

		internal NetIncomingMessage(NetIncomingMessageType tp)
		{
			MIncomingMessageType = tp;
		}

		internal void Reset()
		{
			MIncomingMessageType = NetIncomingMessageType.Error;
			MReadPosition = 0;
			MReceivedMessageType = NetMessageType.LibraryError;
			MSenderConnection = null;
			MBitLength = 0;
			MIsFragment = false;
		}

		/// <summary>
		/// Decrypt a message
		/// </summary>
		/// <param name="encryption">The encryption algorithm used to encrypt the message</param>
		/// <returns>true on success</returns>
		public bool Decrypt(NetEncryption encryption)
		{
			return encryption.Decrypt(this);
		}

		/// <summary>
		/// Reads a value, in local time comparable to NetTime.Now, written using WriteTime()
		/// Must have a connected sender
		/// </summary>
		public double ReadTime(bool highPrecision)
		{
			return ReadTime(MSenderConnection, highPrecision);
		}

		/// <summary>
		/// Returns a string that represents this object
		/// </summary>
		public override string ToString()
		{
			return "[NetIncomingMessage #" + MSequenceNumber + " " + LengthBytes + " bytes]";
		}
	}
}
