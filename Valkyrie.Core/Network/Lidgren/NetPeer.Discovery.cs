﻿using System;
using System.Net;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	public partial class NetPeer
	{
		/// <summary>
		/// Emit a discovery signal to all hosts on your subnet
		/// </summary>
		public void DiscoverLocalPeers(int serverPort)
		{
			var um = CreateMessage(0);
			um.MMessageType = NetMessageType.Discovery;
			Interlocked.Increment(ref um.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(new IPEndPoint(IPAddress.Broadcast, serverPort), um));
		}

		/// <summary>
		/// Emit a discovery signal to a single known host
		/// </summary>
		public bool DiscoverKnownPeer(string host, int serverPort)
		{
			var address = NetUtility.Resolve(host);
			if (address == null)
				return false;
			DiscoverKnownPeer(new IPEndPoint(address, serverPort));
			return true;
		}

		/// <summary>
		/// Emit a discovery signal to a single known host
		/// </summary>
		public void DiscoverKnownPeer(IPEndPoint endPoint)
		{
			var om = CreateMessage(0);
			om.MMessageType = NetMessageType.Discovery;
			om.MRecyclingCount = 1;
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(endPoint, om));
		}

		/// <summary>
		/// Send a discovery response message
		/// </summary>
		public void SendDiscoveryResponse(NetOutgoingMessage msg, IPEndPoint recipient)
		{
			if (recipient == null)
				throw new ArgumentNullException("recipient");

			if (msg == null)
				msg = CreateMessage(0);
			else if (msg.MIsSent)
				throw new NetException("Message has already been sent!");

			if (msg.LengthBytes >= MConfiguration.MaximumTransmissionUnit)
				throw new NetException("Cannot send discovery message larger than MTU (currently " + MConfiguration.MaximumTransmissionUnit + " bytes)");

			msg.MMessageType = NetMessageType.DiscoveryResponse;
			Interlocked.Increment(ref msg.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(recipient, msg));
		}
	}
}
