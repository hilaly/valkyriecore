﻿namespace Valkyrie.Communication.Network.Lidgren
{
    internal enum MessageResendReason
    {
        Delay,
        HoleInSequence
    }
}