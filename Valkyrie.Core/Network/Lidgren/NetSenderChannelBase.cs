﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	internal abstract class NetSenderChannelBase
	{
		// access this directly to queue things in this channel
		protected NetQueue<NetOutgoingMessage> MQueuedSends;

		internal abstract int WindowSize { get; }

		internal abstract int GetAllowedSends();

		internal int QueuedSendsCount { get { return MQueuedSends.Count; } }

		public int GetFreeWindowSlots()
		{
			return GetAllowedSends() - MQueuedSends.Count;
		}

		internal abstract NetSendResult Enqueue(NetOutgoingMessage message);
		internal abstract void SendQueuedMessages(double now);
		internal abstract void Reset();
		internal abstract void ReceiveAcknowledge(double now, int sequenceNumber);
	}
}
