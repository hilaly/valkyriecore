﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	internal sealed class NetUnreliableSequencedReceiver : NetReceiverChannelBase
	{
		private int _mLastReceivedSequenceNumber = -1;

		public NetUnreliableSequencedReceiver(NetConnection connection)
			: base(connection)
		{
		}

		internal override void ReceiveMessage(NetIncomingMessage msg)
		{
			var nr = msg.MSequenceNumber;

			// ack no matter what
			MConnection.QueueAck(msg.MReceivedMessageType, nr);

			var relate = NetUtility.RelativeSequenceNumber(nr, _mLastReceivedSequenceNumber + 1);
			if (relate < 0)
				return; // drop if late

			_mLastReceivedSequenceNumber = nr;
			MPeer.ReleaseMessage(msg);
		}
	}
}
