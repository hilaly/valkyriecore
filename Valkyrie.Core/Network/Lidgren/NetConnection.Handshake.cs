﻿using System;
using System.Linq;
using System.Net;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	public partial class NetConnection
	{
		internal bool MConnectRequested;
		internal bool MDisconnectRequested;
		internal bool MDisconnectReqSendBye;
		internal string MDisconnectMessage;
		internal bool MConnectionInitiator;
		internal NetIncomingMessage MRemoteHailMessage;
		internal double MLastHandshakeSendTime;
		internal int MHandshakeAttempts;

		/// <summary>
		/// The message that the remote part specified via Connect() or Approve() - can be null.
		/// </summary>
		public NetIncomingMessage RemoteHailMessage { get { return MRemoteHailMessage; } }

		// heartbeat called when connection still is in m_handshakes of NetPeer
		internal void UnconnectedHeartbeat(double now)
		{
			MPeer.VerifyNetworkThread();

			if (MDisconnectRequested)
				ExecuteDisconnect(MDisconnectMessage, true);

			if (MConnectRequested)
			{
				switch (MStatus)
				{
					case NetConnectionStatus.Connected:
					case NetConnectionStatus.RespondedConnect:
						// reconnect
						ExecuteDisconnect("Reconnecting", true);
						break;

					case NetConnectionStatus.InitiatedConnect:
						// send another connect attempt
						SendConnect(now);
						break;

					case NetConnectionStatus.Disconnected:
						MPeer.ThrowOrLog("This connection is Disconnected; spent. A new one should have been created");
						break;

					case NetConnectionStatus.Disconnecting:
						// let disconnect finish first
						break;

					case NetConnectionStatus.None:
					default:
						SendConnect(now);
						break;
				}
				return;
			}

			if (now - MLastHandshakeSendTime > MPeerConfiguration.MResendHandshakeInterval)
			{
				if (MHandshakeAttempts >= MPeerConfiguration.MMaximumHandshakeAttempts)
				{
					// failed to connect
					ExecuteDisconnect("Failed to establish connection - no response from remote host", true);
					return;
				}

				// resend handshake
				switch (MStatus)
				{
					case NetConnectionStatus.InitiatedConnect:
						SendConnect(now);
						break;
					case NetConnectionStatus.RespondedConnect:
						SendConnectResponse(now, true);
						break;
					case NetConnectionStatus.RespondedAwaitingApproval:
						// awaiting approval
						MLastHandshakeSendTime = now; // postpone handshake resend
						break;
					case NetConnectionStatus.None:
					case NetConnectionStatus.ReceivedInitiation:
					default:
						MPeer.LogWarning("Time to resend handshake, but status is " + MStatus);
						break;
				}
			}
		}

		internal void ExecuteDisconnect(string reason, bool sendByeMessage)
		{
			MPeer.VerifyNetworkThread();

			// clear send queues
			foreach (var channel in MSendChannels.Where(channel => channel != null))
			{
			    channel.Reset();
			}

			if (sendByeMessage)
				SendDisconnect(reason, true);

			if (MStatus == NetConnectionStatus.ReceivedInitiation)
			{
				// nothing much has happened yet; no need to send disconnected status message
				MStatus = NetConnectionStatus.Disconnected;
			}
			else
			{
				SetStatus(NetConnectionStatus.Disconnected, reason);
			}

			// in case we're still in handshake
			lock (MPeer.MHandshakes)
				MPeer.MHandshakes.Remove(MRemoteEndPoint);

			MDisconnectRequested = false;
			MConnectRequested = false;
			MHandshakeAttempts = 0;
		}

		internal void SendConnect(double now)
		{
			MPeer.VerifyNetworkThread();

			var preAllocate = 13 + MPeerConfiguration.AppIdentifier.Length;
			preAllocate += MLocalHailMessage == null ? 0 : MLocalHailMessage.LengthBytes;

			var om = MPeer.CreateMessage(preAllocate);
			om.MMessageType = NetMessageType.Connect;
			om.Write(MPeerConfiguration.AppIdentifier);
			om.Write(MPeer.MUniqueIdentifier);
			om.Write((float)now);

			WriteLocalHail(om);
			
			MPeer.SendLibrary(om, MRemoteEndPoint);

			MConnectRequested = false;
			MLastHandshakeSendTime = now;
			MHandshakeAttempts++;

			if (MHandshakeAttempts > 1)
				MPeer.LogDebug("Resending Connect...");
			SetStatus(NetConnectionStatus.InitiatedConnect, "Locally requested connect");
		}

		internal void SendConnectResponse(double now, bool onLibraryThread)
		{
			if (onLibraryThread)
				MPeer.VerifyNetworkThread();

			var om = MPeer.CreateMessage(MPeerConfiguration.AppIdentifier.Length + 13 + (MLocalHailMessage == null ? 0 : MLocalHailMessage.LengthBytes));
			om.MMessageType = NetMessageType.ConnectResponse;
			om.Write(MPeerConfiguration.AppIdentifier);
			om.Write(MPeer.MUniqueIdentifier);
			om.Write((float)now);
			Interlocked.Increment(ref om.MRecyclingCount);
			WriteLocalHail(om);

			if (onLibraryThread)
				MPeer.SendLibrary(om, MRemoteEndPoint);
			else
				MPeer.MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(MRemoteEndPoint, om));

			MLastHandshakeSendTime = now;
			MHandshakeAttempts++;

			if (MHandshakeAttempts > 1)
				MPeer.LogDebug("Resending ConnectResponse...");

			SetStatus(NetConnectionStatus.RespondedConnect, "Remotely requested connect");
		}

		internal void SendDisconnect(string reason, bool onLibraryThread)
		{
			if (onLibraryThread)
				MPeer.VerifyNetworkThread();

			var om = MPeer.CreateMessage(reason);
			om.MMessageType = NetMessageType.Disconnect;
			Interlocked.Increment(ref om.MRecyclingCount);
			if (onLibraryThread)
				MPeer.SendLibrary(om, MRemoteEndPoint);
			else
				MPeer.MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(MRemoteEndPoint, om));
		}

		private void WriteLocalHail(NetOutgoingMessage om)
		{
			var hi = MLocalHailMessage?.Data;
			if (hi != null && hi.Length >= MLocalHailMessage.LengthBytes)
			{
				if (om.LengthBytes + MLocalHailMessage.LengthBytes > MPeerConfiguration.MMaximumTransmissionUnit - 10)
					MPeer.ThrowOrLog("Hail message too large; can maximally be " + (MPeerConfiguration.MMaximumTransmissionUnit - 10 - om.LengthBytes));
				om.Write(MLocalHailMessage.Data, 0, MLocalHailMessage.LengthBytes);
			}
		}

		internal void SendConnectionEstablished()
		{
			var om = MPeer.CreateMessage(4);
			om.MMessageType = NetMessageType.ConnectionEstablished;
			om.Write((float)NetTime.Now);
			MPeer.SendLibrary(om, MRemoteEndPoint);

			MHandshakeAttempts = 0;

			InitializePing();
			if (MStatus != NetConnectionStatus.Connected)
				SetStatus(NetConnectionStatus.Connected, "Connected to " + NetUtility.ToHexString(MRemoteUniqueIdentifier));
		}

		/// <summary>
		/// Approves this connection; sending a connection response to the remote host
		/// </summary>
		public void Approve()
		{
			if (MStatus != NetConnectionStatus.RespondedAwaitingApproval)
			{
				MPeer.LogWarning("Approve() called in wrong status; expected RespondedAwaitingApproval; got " + MStatus);
				return;
			}

			MLocalHailMessage = null;
			MHandshakeAttempts = 0;
			SendConnectResponse(NetTime.Now, false);
		}

		/// <summary>
		/// Approves this connection; sending a connection response to the remote host
		/// </summary>
		/// <param name="localHail">The local hail message that will be set as RemoteHailMessage on the remote host</param>
		public void Approve(NetOutgoingMessage localHail)
		{
			if (MStatus != NetConnectionStatus.RespondedAwaitingApproval)
			{
				MPeer.LogWarning("Approve() called in wrong status; expected RespondedAwaitingApproval; got " + MStatus);
				return;
			}

			MLocalHailMessage = localHail;
			MHandshakeAttempts = 0;
			SendConnectResponse(NetTime.Now, false);
		}

		/// <summary>
		/// Denies this connection; disconnecting it
		/// </summary>
		public void Deny()
		{
			Deny(string.Empty);
		}

		/// <summary>
		/// Denies this connection; disconnecting it
		/// </summary>
		/// <param name="reason">The stated reason for the disconnect, readable as a string in the StatusChanged message on the remote host</param>
		public void Deny(string reason)
		{
			// send disconnect; remove from handshakes
			SendDisconnect(reason, false);

			// remove from handshakes
			lock (MPeer.MHandshakes)
				MPeer.MHandshakes.Remove(MRemoteEndPoint);
		}

		internal void ReceivedHandshake(double now, NetMessageType tp, int ptr, int payloadLength)
		{
			MPeer.VerifyNetworkThread();

		    switch (tp)
			{
				case NetMessageType.Connect:
					if (MStatus == NetConnectionStatus.ReceivedInitiation)
					{
						// Whee! Server full has already been checked
					    byte[] hail;
					    var ok = ValidateHandshakeData(ptr, payloadLength, out hail);
						if (ok)
						{
							if (hail != null)
							{
								MRemoteHailMessage = MPeer.CreateIncomingMessage(NetIncomingMessageType.Data, hail);
								MRemoteHailMessage.LengthBits = hail.Length * 8;
							}
							else
							{
								MRemoteHailMessage = null; 
							}

							if (MPeerConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.ConnectionApproval))
							{
								// ok, let's not add connection just yet
								var appMsg = MPeer.CreateIncomingMessage(NetIncomingMessageType.ConnectionApproval, MRemoteHailMessage == null ? 0 : MRemoteHailMessage.LengthBytes);
								appMsg.MReceiveTime = now;
								appMsg.MSenderConnection = this;
								appMsg.MSenderEndPoint = MRemoteEndPoint;
								if (MRemoteHailMessage != null)
									appMsg.Write(MRemoteHailMessage.MData, 0, MRemoteHailMessage.LengthBytes);
								SetStatus(NetConnectionStatus.RespondedAwaitingApproval, "Awaiting approval");
								MPeer.ReleaseMessage(appMsg);
								return;
							}

							SendConnectResponse((float)now, true);
						}
						return;
					}
					if (MStatus == NetConnectionStatus.RespondedAwaitingApproval)
					{
						MPeer.LogWarning("Ignoring multiple Connect() most likely due to a delayed Approval");
						return;
					}
					if (MStatus == NetConnectionStatus.RespondedConnect)
					{
						// our ConnectResponse must have been lost
						SendConnectResponse((float)now, true);
						return;
					}
					MPeer.LogDebug("Unhandled Connect: " + tp + ", status is " + MStatus + " length: " + payloadLength);
					break;
				case NetMessageType.ConnectResponse:
					HandleConnectResponse(now, tp, ptr, payloadLength);
					break;

				case NetMessageType.ConnectionEstablished:
					switch (MStatus)
					{
						case NetConnectionStatus.Connected:
							// ok...
							break;
						case NetConnectionStatus.Disconnected:
						case NetConnectionStatus.Disconnecting:
						case NetConnectionStatus.None:
							// too bad, almost made it
							break;
						case NetConnectionStatus.ReceivedInitiation:
							// uh, a little premature... ignore
							break;
						case NetConnectionStatus.InitiatedConnect:
							// weird, should have been RespondedConnect...
							break;
						case NetConnectionStatus.RespondedConnect:
							// awesome
				
							var msg = MPeer.SetupReadHelperMessage(ptr, payloadLength);
							InitializeRemoteTimeOffset(msg.ReadSingle());

							MPeer.AcceptConnection(this);
							InitializePing();
							SetStatus(NetConnectionStatus.Connected, "Connected to " + NetUtility.ToHexString(MRemoteUniqueIdentifier));
							return;
					}
					break;

				case NetMessageType.Disconnect:
					// ouch
					var reason = "Ouch";
					try
					{
						var inc = MPeer.SetupReadHelperMessage(ptr, payloadLength);
						reason = inc.ReadString();
					}
					catch
					{
					}
					ExecuteDisconnect(reason, false);
					break;

				case NetMessageType.Discovery:
					MPeer.HandleIncomingDiscoveryRequest(now, MRemoteEndPoint, ptr, payloadLength);
					return;

				case NetMessageType.DiscoveryResponse:
					MPeer.HandleIncomingDiscoveryResponse(now, MRemoteEndPoint, ptr, payloadLength);
					return;

				case NetMessageType.Ping:
					// silently ignore
					return;

				default:
					MPeer.LogDebug("Unhandled type during handshake: " + tp + " length: " + payloadLength);
					break;
			}
		}

		private void HandleConnectResponse(double now, NetMessageType tp, int ptr, int payloadLength)
		{
		    switch (MStatus)
			{
				case NetConnectionStatus.InitiatedConnect:
					// awesome
			        byte[] hail;
			        var ok = ValidateHandshakeData(ptr, payloadLength, out hail);
					if (ok)
					{
						if (hail != null)
						{
							MRemoteHailMessage = MPeer.CreateIncomingMessage(NetIncomingMessageType.Data, hail);
							MRemoteHailMessage.LengthBits = hail.Length * 8;
						}
						else
						{
							MRemoteHailMessage = null;
						}

						MPeer.AcceptConnection(this);
						SendConnectionEstablished();
					}
					break;
				case NetConnectionStatus.RespondedConnect:
					// hello, wtf?
					break;
				case NetConnectionStatus.Disconnecting:
				case NetConnectionStatus.Disconnected:
				case NetConnectionStatus.ReceivedInitiation:
				case NetConnectionStatus.None:
					// wtf? anyway, bye!
					break;
				case NetConnectionStatus.Connected:
					// my ConnectionEstablished must have been lost, send another one
					SendConnectionEstablished();
					return;
			}
		}

		private bool ValidateHandshakeData(int ptr, int payloadLength, out byte[] hail)
		{
			hail = null;

			// create temporary incoming message
			var msg = MPeer.SetupReadHelperMessage(ptr, payloadLength);
			try
			{
				var remoteAppIdentifier = msg.ReadString();
				var remoteUniqueIdentifier = msg.ReadInt64();
				InitializeRemoteTimeOffset(msg.ReadSingle());

				var remainingBytes = payloadLength - (msg.PositionInBytes - ptr);
				if (remainingBytes > 0)
					hail = msg.ReadBytes(remainingBytes);

				if (remoteAppIdentifier != MPeer.MConfiguration.AppIdentifier)
				{
					ExecuteDisconnect("Wrong application identifier!", true);
					return false;
				}

				MRemoteUniqueIdentifier = remoteUniqueIdentifier;
			}
			catch(Exception ex)
			{
				// whatever; we failed
				ExecuteDisconnect("Handshake data validation failed", true);
				MPeer.LogWarning("ReadRemoteHandshakeData failed: " + ex.Message);
				return false;
			}
			return true;
		}
		
		/// <summary>
		/// Disconnect from the remote peer
		/// </summary>
		/// <param name="byeMessage">the message to send with the disconnect message</param>
		public void Disconnect(string byeMessage)
		{
			// user or library thread
			if (MStatus == NetConnectionStatus.None || MStatus == NetConnectionStatus.Disconnected)
				return;

			MPeer.LogVerbose("Disconnect requested for " + this);
			MDisconnectMessage = byeMessage;

			if (MStatus != NetConnectionStatus.Disconnected && MStatus != NetConnectionStatus.None)
				SetStatus(NetConnectionStatus.Disconnecting, byeMessage);

			MHandshakeAttempts = 0;
			MDisconnectRequested = true;
			MDisconnectReqSendBye = true;
		}
	}
}
