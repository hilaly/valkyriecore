﻿using System;
using System.Security.Cryptography;

namespace Valkyrie.Communication.Network.Lidgren
{
    /// <summary>
    /// RNGCryptoServiceProvider based random; very slow but cryptographically safe
    /// </summary>
    public class CryptoRandom : NetRandom
    {
        /// <summary>
        /// Global instance of CryptoRandom
        /// </summary>
        public new static readonly CryptoRandom Instance = new CryptoRandom();

        private readonly RandomNumberGenerator _mRnd = new RNGCryptoServiceProvider();

        /// <summary>
        /// Seed in CryptoRandom does not create deterministic sequences
        /// </summary>
        
        public override void Initialize(uint seed)
        {
            var tmp = new byte[seed % 16];
            _mRnd.GetBytes(tmp); // just prime it
        }

        /// <summary>
        /// Generates a random value from UInt32.MinValue to UInt32.MaxValue, inclusively
        /// </summary>
        
        public override uint NextUInt32()
        {
            var bytes = new byte[4];
            _mRnd.GetBytes(bytes);
            return bytes[0] | ((uint)bytes[1] << 8) | ((uint)bytes[2] << 16) | ((uint)bytes[3] << 24);
        }

        /// <summary>
        /// Fill the specified buffer with random values
        /// </summary>
        public override void NextBytes(byte[] buffer)
        {
            _mRnd.GetBytes(buffer);
        }

        /// <summary>
        /// Fills all bytes from offset to offset + length in buffer with random values
        /// </summary>
        public override void NextBytes(byte[] buffer, int offset, int length)
        {
            var bytes = new byte[length];
            _mRnd.GetBytes(bytes);
            Array.Copy(bytes, 0, buffer, offset, length);
        }
    }
}