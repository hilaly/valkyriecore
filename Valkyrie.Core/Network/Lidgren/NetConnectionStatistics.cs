﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

// Uncomment the line below to get statistics in RELEASE builds
//#define USE_RELEASE_STATISTICS

using System.Diagnostics;
using System.Linq;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Statistics for a NetConnection instance
	/// </summary>
	public sealed class NetConnectionStatistics
	{
		private readonly NetConnection _mConnection;

		internal long MSentPackets;
		internal long MReceivedPackets;

		internal long MSentMessages;
		internal long MReceivedMessages;
		internal long MReceivedFragments;

		internal long MSentBytes;
		internal long MReceivedBytes;

		internal long MResentMessagesDueToDelay;
		internal long MResentMessagesDueToHole;

		internal NetConnectionStatistics(NetConnection conn)
		{
			_mConnection = conn;
			Reset();
		}

		internal void Reset()
		{
			MSentPackets = 0;
			MReceivedPackets = 0;
			MSentMessages = 0;
			MReceivedMessages = 0;
			MReceivedFragments = 0;
			MSentBytes = 0;
			MReceivedBytes = 0;
			MResentMessagesDueToDelay = 0;
			MResentMessagesDueToHole = 0;
		}

		/// <summary>
		/// Gets the number of sent packets for this connection
		/// </summary>
		public long SentPackets { get { return MSentPackets; } }

		/// <summary>
		/// Gets the number of received packets for this connection
		/// </summary>
		public long ReceivedPackets { get { return MReceivedPackets; } }

		/// <summary>
		/// Gets the number of sent bytes for this connection
		/// </summary>
		public long SentBytes { get { return MSentBytes; } }

		/// <summary>
		/// Gets the number of received bytes for this connection
		/// </summary>
		public long ReceivedBytes { get { return MReceivedBytes; } }

		/// <summary>
		/// Gets the number of resent reliable messages for this connection
		/// </summary>
		public long ResentMessages { get { return MResentMessagesDueToHole + MResentMessagesDueToDelay; } }

		// public double LastSendRespondedTo { get { return m_connection.m_lastSendRespondedTo; } }

#if USE_RELEASE_STATISTICS
		internal void PacketSent(int numBytes, int numMessages)
		{
			NetException.Assert(numBytes > 0 && numMessages > 0);
			m_sentPackets++;
			m_sentBytes += numBytes;
			m_sentMessages += numMessages;
		}
#else
		[Conditional("DEBUG")]
		internal void PacketSent(int numBytes, int numMessages)
		{
			NetException.Assert(numBytes > 0 && numMessages > 0);
			MSentPackets++;
			MSentBytes += numBytes;
			MSentMessages += numMessages;
		}
#endif

#if USE_RELEASE_STATISTICS
		internal void PacketReceived(int numBytes, int numMessages)
		{
			NetException.Assert(numBytes > 0 && numMessages > 0);
			m_receivedPackets++;
			m_receivedBytes += numBytes;
			m_receivedMessages += numMessages;
		}
#else
		[Conditional("DEBUG")]
		internal void PacketReceived(int numBytes, int numMessages, int numFragments)
		{
			NetException.Assert(numBytes > 0 && numMessages > 0);
			MReceivedPackets++;
			MReceivedBytes += numBytes;
			MReceivedMessages += numMessages;
			MReceivedFragments += numFragments;
		}
#endif

#if USE_RELEASE_STATISTICS
		internal void MessageResent(MessageResendReason reason)
		{
			if (reason == MessageResendReason.Delay)
				m_resentMessagesDueToDelay++;
			else
				m_resentMessagesDueToHole++;
		}
#else
		[Conditional("DEBUG")]
		internal void MessageResent(MessageResendReason reason)
		{
			if (reason == MessageResendReason.Delay)
				MResentMessagesDueToDelay++;
			else
				MResentMessagesDueToHole++;
		}
#endif

		/// <summary>
		/// Returns a string that represents this object
		/// </summary>
		public override string ToString()
		{
			var bdr = new StringBuilder();
			//bdr.AppendLine("Average roundtrip time: " + NetTime.ToReadable(m_connection.m_averageRoundtripTime));
			bdr.AppendLine("Current MTU: " + _mConnection.MCurrentMtu);
			bdr.AppendLine("Sent " + MSentBytes + " bytes in " + MSentMessages + " messages in " + MSentPackets + " packets");
			bdr.AppendLine("Received " + MReceivedBytes + " bytes in " + MReceivedMessages + " messages (of which " + MReceivedFragments + " fragments) in " + MReceivedPackets + " packets");

			if (MResentMessagesDueToDelay > 0)
				bdr.AppendLine("Resent messages (delay): " + MResentMessagesDueToDelay);
			if (MResentMessagesDueToDelay > 0)
				bdr.AppendLine("Resent messages (holes): " + MResentMessagesDueToHole);

			var numUnsent = 0;
			var numStored = 0;
			foreach (var sendChan in _mConnection.MSendChannels)
			{
				if (sendChan == null)
					continue;
				numUnsent += sendChan.QueuedSendsCount;

				var relSendChan = sendChan as NetReliableSenderChannel;
				if (relSendChan != null)
				{
					for (var i = 0; i < relSendChan.MStoredMessages.Length; i++)
						if (relSendChan.MStoredMessages[i].Message != null)
							numStored++;
				}
			}

			var numWithheld = 0;
			foreach (var recChan in _mConnection.MReceiveChannels)
			{
				var relRecChan = recChan as NetReliableOrderedReceiver;
				if (relRecChan != null)
				{
				    numWithheld += relRecChan.MWithheldMessages.Count(t => t != null);
				}
			}

			bdr.AppendLine("Unsent messages: " + numUnsent);
			bdr.AppendLine("Stored messages: " + numStored);
			bdr.AppendLine("Withheld messages: " + numWithheld);

			return bdr.ToString();
		}
	}
}