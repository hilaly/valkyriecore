﻿using System.Net;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	public partial class NetPeer
	{
		/// <summary>
		/// Send NetIntroduction to hostExternal and clientExternal; introducing client to host
		/// </summary>
		public void Introduce(
			IPEndPoint hostInternal,
			IPEndPoint hostExternal,
			IPEndPoint clientInternal,
			IPEndPoint clientExternal,
			string token)
		{
			// send message to client
			var um = CreateMessage(10 + token.Length + 1);
			um.MMessageType = NetMessageType.NatIntroduction;
			um.Write((byte)0);
			um.Write(hostInternal);
			um.Write(hostExternal);
			um.Write(token);
			Interlocked.Increment(ref um.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(clientExternal, um));

			// send message to host
			um = CreateMessage(10 + token.Length + 1);
			um.MMessageType = NetMessageType.NatIntroduction;
			um.Write((byte)1);
			um.Write(clientInternal);
			um.Write(clientExternal);
			um.Write(token);
			Interlocked.Increment(ref um.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(hostExternal, um));
		}

		/// <summary>
		/// Called when host/client receives a NatIntroduction message from a master server
		/// </summary>
		internal void HandleNatIntroduction(int ptr)
		{
			VerifyNetworkThread();

			// read intro
			var tmp = SetupReadHelperMessage(ptr, 1000); // never mind length

			var hostByte = tmp.ReadByte();
			var remoteInternal = tmp.ReadIpEndPoint();
			var remoteExternal = tmp.ReadIpEndPoint();
			var token = tmp.ReadString();
			var isHost = hostByte != 0;

			LogDebug("NAT introduction received; we are designated " + (isHost ? "host" : "client"));

		    if (!isHost && MConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.NatIntroductionSuccess) == false)
				return; // no need to punch - we're not listening for nat intros!

			// send internal punch
			var punch = CreateMessage(1);
			punch.MMessageType = NetMessageType.NatPunchMessage;
			punch.Write(hostByte);
			punch.Write(token);
			Interlocked.Increment(ref punch.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(remoteInternal, punch));
			LogDebug("NAT punch sent to " + remoteInternal);

			// send external punch
			punch = CreateMessage(1);
			punch.MMessageType = NetMessageType.NatPunchMessage;
			punch.Write(hostByte);
			punch.Write(token);
			Interlocked.Increment(ref punch.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(remoteExternal, punch));
			LogDebug("NAT punch sent to " + remoteExternal);

		}

		/// <summary>
		/// Called when receiving a NatPunchMessage from a remote endpoint
		/// </summary>
		private void HandleNatPunch(int ptr, IPEndPoint senderEndPoint)
		{
			var tmp = SetupReadHelperMessage(ptr, 1000); // never mind length

			var fromHostByte = tmp.ReadByte();
			if (fromHostByte == 0)
			{
				// it's from client
				LogDebug("NAT punch received from " + senderEndPoint + " we're host, so we ignore this");
				return; // don't alert hosts about nat punch successes; only clients
			}
			var token = tmp.ReadString();

			LogDebug("NAT punch received from " + senderEndPoint + " we're client, so we've succeeded - token is " + token);

			//
			// Release punch success to client; enabling him to Connect() to msg.SenderIPEndPoint if token is ok
			//
			var punchSuccess = CreateIncomingMessage(NetIncomingMessageType.NatIntroductionSuccess, 10);
			punchSuccess.MSenderEndPoint = senderEndPoint;
			punchSuccess.Write(token);
			ReleaseMessage(punchSuccess);

			// send a return punch just for good measure
			var punch = CreateMessage(1);
			punch.MMessageType = NetMessageType.NatPunchMessage;
			punch.Write((byte)0);
			punch.Write(token);
			Interlocked.Increment(ref punch.MRecyclingCount);
			MUnsentUnconnectedMessages.Enqueue(new NetTuple<IPEndPoint, NetOutgoingMessage>(senderEndPoint, punch));
		}
	}
}
