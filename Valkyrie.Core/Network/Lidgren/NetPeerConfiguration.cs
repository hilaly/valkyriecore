﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System.Net;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Partly immutable after NetPeer has been initialized
	/// </summary>
	public sealed class NetPeerConfiguration
	{
		// Maximum transmission unit
		// Ethernet can take 1500 bytes of payload, so lets stay below that.
		// The aim is for a max full packet to be 1440 bytes (30 x 48 bytes, lower than 1468)
		// -20 bytes IP header
		//  -8 bytes UDP header
		//  -4 bytes to be on the safe side and align to 8-byte boundary
		// Total 1408 bytes
		// Note that lidgren headers (5 bytes) are not included here; since it's part of the "mtu payload"
		
		/// <summary>
		/// Default MTU value in bytes
		/// </summary>
		public const int KDefaultMtu = 1408;
		
		private const string CIsLockedMessage = "You may not modify the NetPeerConfiguration after it has been used to initialize a NetPeer";

		private bool _mIsLocked;
		private readonly string _mAppIdentifier;
		private string _mNetworkThreadName;
		private IPAddress _mLocalAddress;
		private IPAddress _mBroadcastAddress;
		internal bool MAcceptIncomingConnections;
		internal int MMaximumConnections;
		internal int MDefaultOutgoingMessageCapacity;
		internal float MPingInterval;
		internal bool MUseMessageRecycling;
		internal int MRecycledCacheMaxCount;
		internal float MConnectionTimeout;
		internal bool MEnableUpnP;
		internal bool MAutoFlushSendQueue;

	    internal NetIncomingMessageType MDisabledTypes;
		internal int MPort;
		internal int MReceiveBufferSize;
		internal int MSendBufferSize;
		internal float MResendHandshakeInterval;
		internal int MMaximumHandshakeAttempts;

		// bad network simulation
		internal float MLoss;
		internal float MDuplicates;
		internal float MMinimumOneWayLatency;
		internal float MRandomOneWayLatency;

		// MTU
		internal int MMaximumTransmissionUnit;
		internal bool MAutoExpandMtu;
		internal float MExpandMtuFrequency;
		internal int MExpandMtuFailAttempts;

		/// <summary>
		/// NetPeerConfiguration constructor
		/// </summary>
		public NetPeerConfiguration(string appIdentifier)
		{
			if (string.IsNullOrEmpty(appIdentifier))
				throw new NetException("App identifier must be at least one character long");
			_mAppIdentifier = appIdentifier;

			//
			// default values
			//
			MDisabledTypes = NetIncomingMessageType.ConnectionApproval | NetIncomingMessageType.UnconnectedData | NetIncomingMessageType.VerboseDebugMessage | NetIncomingMessageType.ConnectionLatencyUpdated | NetIncomingMessageType.NatIntroductionSuccess;
			_mNetworkThreadName = "Lidgren network thread";
			_mLocalAddress = IPAddress.Any;
			_mBroadcastAddress = IPAddress.Broadcast;
			var ip = NetUtility.GetBroadcastAddress();
			if (ip != null)
			{
				_mBroadcastAddress = ip;
			}
			MPort = 0;
			MReceiveBufferSize = 131071;
			MSendBufferSize = 131071;
			MAcceptIncomingConnections = false;
			MMaximumConnections = 32;
			MDefaultOutgoingMessageCapacity = 16;
			MPingInterval = 4.0f;
			MConnectionTimeout = 25.0f;
			MUseMessageRecycling = true;
			MRecycledCacheMaxCount = 64;
			MResendHandshakeInterval = 3.0f;
			MMaximumHandshakeAttempts = 5;
			MAutoFlushSendQueue = true;

			MMaximumTransmissionUnit = KDefaultMtu;
			MAutoExpandMtu = false;
			MExpandMtuFrequency = 2.0f;
			MExpandMtuFailAttempts = 5;
			UnreliableSizeBehaviour = NetUnreliableSizeBehaviour.IgnoreMtu;

			MLoss = 0.0f;
			MMinimumOneWayLatency = 0.0f;
			MRandomOneWayLatency = 0.0f;
			MDuplicates = 0.0f;

			_mIsLocked = false;
		}

		internal void Lock()
		{
			_mIsLocked = true;
		}

		/// <summary>
		/// Gets the identifier of this application; the library can only connect to matching app identifier peers
		/// </summary>
		public string AppIdentifier
		{
			get { return _mAppIdentifier; }
		}

		/// <summary>
		/// Enables receiving of the specified type of message
		/// </summary>
		public void EnableMessageType(NetIncomingMessageType type)
		{
			MDisabledTypes &= ~type;
		}

		/// <summary>
		/// Disables receiving of the specified type of message
		/// </summary>
		public void DisableMessageType(NetIncomingMessageType type)
		{
			MDisabledTypes |= type;
		}

		/// <summary>
		/// Enables or disables receiving of the specified type of message
		/// </summary>
		public void SetMessageTypeEnabled(NetIncomingMessageType type, bool enabled)
		{
			if (enabled)
				MDisabledTypes &= ~type;
			else
				MDisabledTypes |= type;
		}

		/// <summary>
		/// Gets if receiving of the specified type of message is enabled
		/// </summary>
		public bool IsMessageTypeEnabled(NetIncomingMessageType type)
		{
			return !((MDisabledTypes & type) == type);
		}

		/// <summary>
		/// Gets or sets the behaviour of unreliable sends above MTU
		/// </summary>
		public NetUnreliableSizeBehaviour UnreliableSizeBehaviour { get; set; }

	    /// <summary>
		/// Gets or sets the name of the library network thread. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public string NetworkThreadName
		{
			get { return _mNetworkThreadName; }
			set
			{
				if (_mIsLocked)
					throw new NetException("NetworkThreadName may not be set after the NetPeer which uses the configuration has been started");
				_mNetworkThreadName = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum amount of connections this peer can hold. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public int MaximumConnections
		{
			get { return MMaximumConnections; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MMaximumConnections = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum amount of bytes to send in a single packet, excluding ip, udp and lidgren headers. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public int MaximumTransmissionUnit
		{
			get { return MMaximumTransmissionUnit; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				if (value < 1 || value >= (ushort.MaxValue + 1) / 8)
					throw new NetException("MaximumTransmissionUnit must be between 1 and " + ((ushort.MaxValue + 1) / 8 - 1) + " bytes");
				MMaximumTransmissionUnit = value;
			}
		}

		/// <summary>
		/// Gets or sets the default capacity in bytes when NetPeer.CreateMessage() is called without argument
		/// </summary>
		public int DefaultOutgoingMessageCapacity
		{
			get { return MDefaultOutgoingMessageCapacity; }
			set { MDefaultOutgoingMessageCapacity = value; }
		}

		/// <summary>
		/// Gets or sets the time between latency calculating pings
		/// </summary>
		public float PingInterval
		{
			get { return MPingInterval; }
			set { MPingInterval = value; }
		}

		/// <summary>
		/// Gets or sets if the library should recycling messages to avoid excessive garbage collection. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public bool UseMessageRecycling
		{
			get { return MUseMessageRecycling; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MUseMessageRecycling = value;
			}
		}

		/// <summary>
		/// Gets or sets the maximum number of incoming/outgoing messages to keep in the recycle cache.
		/// </summary>
		public int RecycledCacheMaxCount
		{
			get { return MRecycledCacheMaxCount; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MRecycledCacheMaxCount = value;
			}
		}

		/// <summary>
		/// Gets or sets the number of seconds timeout will be postponed on a successful ping/pong
		/// </summary>
		public float ConnectionTimeout
		{
			get { return MConnectionTimeout; }
			set
			{
				if (value < MPingInterval)
					throw new NetException("Connection timeout cannot be lower than ping interval!");
				MConnectionTimeout = value;
			}
		}

		/// <summary>
		/// Enables UPnP support; enabling port forwarding and getting external ip
		/// </summary>
		public bool EnableUpnP
		{
			get { return MEnableUpnP; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MEnableUpnP = value;
			}
		}

		/// <summary>
		/// Enables or disables automatic flushing of the send queue. If disabled, you must manully call NetPeer.FlushSendQueue() to flush sent messages to network.
		/// </summary>
		public bool AutoFlushSendQueue
		{
			get { return MAutoFlushSendQueue; }
			set { MAutoFlushSendQueue = value; }
		}

		/// <summary>
		/// Gets or sets the local ip address to bind to. Defaults to IPAddress.Any. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public IPAddress LocalAddress
		{
			get { return _mLocalAddress; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				_mLocalAddress = value;
			}
		}

		/// <summary>
		/// Gets or sets the local broadcast address to use when broadcasting
		/// </summary>
		public IPAddress BroadcastAddress
		{
			get { return _mBroadcastAddress; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				_mBroadcastAddress = value;
			}
		}

		/// <summary>
		/// Gets or sets the local port to bind to. Defaults to 0. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public int Port
		{
			get { return MPort; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MPort = value;
			}
		}

		/// <summary>
		/// Gets or sets the size in bytes of the receiving buffer. Defaults to 131071 bytes. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public int ReceiveBufferSize
		{
			get { return MReceiveBufferSize; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MReceiveBufferSize = value;
			}
		}

		/// <summary>
		/// Gets or sets the size in bytes of the sending buffer. Defaults to 131071 bytes. Cannot be changed once NetPeer is initialized.
		/// </summary>
		public int SendBufferSize
		{
			get { return MSendBufferSize; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MSendBufferSize = value;
			}
		}

		/// <summary>
		/// Gets or sets if the NetPeer should accept incoming connections. This is automatically set to true in NetServer and false in NetClient.
		/// </summary>
		public bool AcceptIncomingConnections
		{
			get { return MAcceptIncomingConnections; }
			set { MAcceptIncomingConnections = value; }
		}

		/// <summary>
		/// Gets or sets the number of seconds between handshake attempts
		/// </summary>
		public float ResendHandshakeInterval
		{
			get { return MResendHandshakeInterval; }
			set { MResendHandshakeInterval = value; }
		}

		/// <summary>
		/// Gets or sets the maximum number of handshake attempts before failing to connect
		/// </summary>
		public int MaximumHandshakeAttempts
		{
			get { return MMaximumHandshakeAttempts; }
			set
			{
				if (value < 1)
					throw new NetException("MaximumHandshakeAttempts must be at least 1");
				MMaximumHandshakeAttempts = value;
			}
		}

		/// <summary>
		/// Gets or sets if the NetPeer should send large messages to try to expand the maximum transmission unit size
		/// </summary>
		public bool AutoExpandMtu
		{
			get { return MAutoExpandMtu; }
			set
			{
				if (_mIsLocked)
					throw new NetException(CIsLockedMessage);
				MAutoExpandMtu = value;
			}
		}

		/// <summary>
		/// Gets or sets how often to send large messages to expand MTU if AutoExpandMTU is enabled
		/// </summary>
		public float ExpandMtuFrequency
		{
			get { return MExpandMtuFrequency; }
			set { MExpandMtuFrequency = value; }
		}

		/// <summary>
		/// Gets or sets the number of failed expand mtu attempts to perform before setting final MTU
		/// </summary>
		public int ExpandMtuFailAttempts
		{
			get { return MExpandMtuFailAttempts; }
			set { MExpandMtuFailAttempts = value; }
		}

#if DEBUG
		/// <summary>
		/// Gets or sets the simulated amount of sent packets lost from 0.0f to 1.0f
		/// </summary>
		public float SimulatedLoss
		{
			get { return MLoss; }
			set { MLoss = value; }
		}

		/// <summary>
		/// Gets or sets the minimum simulated amount of one way latency for sent packets in seconds
		/// </summary>
		public float SimulatedMinimumLatency
		{
			get { return MMinimumOneWayLatency; }
			set { MMinimumOneWayLatency = value; }
		}

		/// <summary>
		/// Gets or sets the simulated added random amount of one way latency for sent packets in seconds
		/// </summary>
		public float SimulatedRandomLatency
		{
			get { return MRandomOneWayLatency; }
			set { MRandomOneWayLatency = value; }
		}

		/// <summary>
		/// Gets the average simulated one way latency in seconds
		/// </summary>
		public float SimulatedAverageLatency
		{
			get { return MMinimumOneWayLatency + MRandomOneWayLatency * 0.5f; }
		}

		/// <summary>
		/// Gets or sets the simulated amount of duplicated packets from 0.0f to 1.0f
		/// </summary>
		public float SimulatedDuplicatesChance
		{
			get { return MDuplicates; }
			set { MDuplicates = value; }
		}
#endif

		/// <summary>
		/// Creates a memberwise shallow clone of this configuration
		/// </summary>
		public NetPeerConfiguration Clone()
		{
			var retval = MemberwiseClone() as NetPeerConfiguration;
			retval._mIsLocked = false;
			return retval;
		}
	}
}
