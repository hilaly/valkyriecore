﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

//
// Comment for Linux Mono users: reports of library thread hangs on EnterReadLock() suggests switching to plain lock() works better
//

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Thread safe (blocking) expanding queue with TryDequeue() and EnqueueFirst()
	/// </summary>
	[DebuggerDisplay("Count={Count} Capacity={Capacity}")]
	public sealed class NetQueue<T>
	{
		// Example:
		// m_capacity = 8
		// m_size = 6
		// m_head = 4
		//
		// [0] item
		// [1] item (tail = ((head + size - 1) % capacity)
		// [2] 
		// [3] 
		// [4] item (head)
		// [5] item
		// [6] item 
		// [7] item
		//
		private T[] _mItems;
		private readonly ReaderWriterLockSlim _mLock = new ReaderWriterLockSlim();
		private int _mSize;
		private int _mHead;

		/// <summary>
		/// Gets the number of items in the queue
		/// </summary>
		public int Count { get { return _mSize; } }

		/// <summary>
		/// Gets the current capacity for the queue
		/// </summary>
		public int Capacity { get { return _mItems.Length; } }

		/// <summary>
		/// NetQueue constructor
		/// </summary>
		public NetQueue(int initialCapacity)
		{
			_mItems = new T[initialCapacity];
		}

		/// <summary>
		/// Adds an item last/tail of the queue
		/// </summary>
		public void Enqueue(T item)
		{
			_mLock.EnterWriteLock();
			try
			{
				if (_mSize == _mItems.Length)
					SetCapacity(_mItems.Length + 8);

				var slot = (_mHead + _mSize) % _mItems.Length;
				_mItems[slot] = item;
				_mSize++;
			}
			finally
			{
				_mLock.ExitWriteLock();
			}
		}

		/// <summary>
		/// Adds an item last/tail of the queue
		/// </summary>
		public void Enqueue(IEnumerable<T> items)
		{
			_mLock.EnterWriteLock();
			try
			{
				foreach (var item in items)
				{
					if (_mSize == _mItems.Length)
						SetCapacity(_mItems.Length + 8); // @TODO move this out of loop

					var slot = (_mHead + _mSize) % _mItems.Length;
					_mItems[slot] = item;
					_mSize++;
				}
			}
			finally
			{
				_mLock.ExitWriteLock();
			}
		}

		/// <summary>
		/// Places an item first, at the head of the queue
		/// </summary>
		public void EnqueueFirst(T item)
		{
			_mLock.EnterWriteLock();
			try
			{
				if (_mSize >= _mItems.Length)
					SetCapacity(_mItems.Length + 8);

				_mHead--;
				if (_mHead < 0)
					_mHead = _mItems.Length - 1;
				_mItems[_mHead] = item;
				_mSize++;
			}
			finally
			{
				_mLock.ExitWriteLock();
			}
		}

		// must be called from within a write locked m_lock!
		private void SetCapacity(int newCapacity)
		{
			if (_mSize == 0)
			{
				if (_mSize == 0)
				{
					_mItems = new T[newCapacity];
					_mHead = 0;
					return;
				}
			}

			var newItems = new T[newCapacity];

			if (_mHead + _mSize - 1 < _mItems.Length)
			{
				Array.Copy(_mItems, _mHead, newItems, 0, _mSize);
			}
			else
			{
				Array.Copy(_mItems, _mHead, newItems, 0, _mItems.Length - _mHead);
				Array.Copy(_mItems, 0, newItems, _mItems.Length - _mHead, _mSize - (_mItems.Length - _mHead));
			}

			_mItems = newItems;
			_mHead = 0;

		}

		/// <summary>
		/// Gets an item from the head of the queue, or returns default(T) if empty
		/// </summary>
		public bool TryDequeue(out T item)
		{
			if (_mSize == 0)
			{
				item = default(T);
				return false;
			}

			_mLock.EnterWriteLock();
			try
			{
				if (_mSize == 0)
				{
					item = default(T);
					return false;
				}

				item = _mItems[_mHead];
				_mItems[_mHead] = default(T);

				_mHead = (_mHead + 1) % _mItems.Length;
				_mSize--;

				return true;
			}
			catch
			{
#if DEBUG
				throw;
#else
				item = default(T);
				return false;
#endif
			}
			finally
			{
				_mLock.ExitWriteLock();
			}
		}

		/// <summary>
		/// Gets all items from the head of the queue, or returns number of items popped
		/// </summary>
		public int TryDrain(IList<T> addTo)
		{
			if (_mSize == 0)
				return 0;

			_mLock.EnterWriteLock();
			try
			{
				var added = _mSize;
				while (_mSize > 0)
				{
					var item = _mItems[_mHead];
					addTo.Add(item);

					_mItems[_mHead] = default(T);
					_mHead = (_mHead + 1) % _mItems.Length;
					_mSize--;
				}
				return added;
			}
			finally
			{
				_mLock.ExitWriteLock();
			}
		}

		/// <summary>
		/// Returns default(T) if queue is empty
		/// </summary>
		public T TryPeek(int offset)
		{
			if (_mSize == 0)
				return default(T);

			_mLock.EnterReadLock();
			try
			{
				if (_mSize == 0)
					return default(T);
				return _mItems[(_mHead + offset) % _mItems.Length];
			}
			finally
			{
				_mLock.ExitReadLock();
			}
		}

		/// <summary>
		/// Determines whether an item is in the queue
		/// </summary>
		public bool Contains(T item)
		{
			_mLock.EnterReadLock();
			try
			{
				var ptr = _mHead;
				for (var i = 0; i < _mSize; i++)
				{
					if (_mItems[ptr] == null)
					{
						if (item == null)
							return true;
					}
					else
					{
						if (_mItems[ptr].Equals(item))
							return true;
					}
					ptr = (ptr + 1) % _mItems.Length;
				}
				return false;
			}
			finally
			{
				_mLock.ExitReadLock();
			}
		}

		/// <summary>
		/// Copies the queue items to a new array
		/// </summary>
		public T[] ToArray()
		{
			_mLock.EnterReadLock();
			try
			{
				var retval = new T[_mSize];
				var ptr = _mHead;
				for (var i = 0; i < _mSize; i++)
				{
					retval[i] = _mItems[ptr++];
					if (ptr >= _mItems.Length)
						ptr = 0;
				}
				return retval;
			}
			finally
			{
				_mLock.ExitReadLock();
			}
		}

		/// <summary>
		/// Removes all objects from the queue
		/// </summary>
		public void Clear()
		{
			_mLock.EnterWriteLock();
			try
			{
				for (var i = 0; i < _mItems.Length; i++)
					_mItems[i] = default(T);
				_mHead = 0;
				_mSize = 0;
			}
			finally
			{
				_mLock.ExitWriteLock();
			}
		}
	}
}
