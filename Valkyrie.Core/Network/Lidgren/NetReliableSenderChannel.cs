﻿using System.Threading;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Sender part of Selective repeat ARQ for a particular NetChannel
	/// </summary>
	internal sealed class NetReliableSenderChannel : NetSenderChannelBase
	{
		private readonly NetConnection _mConnection;
		private int _mWindowStart;
		private readonly int _mWindowSize;
		private int _mSendStart;

		private readonly NetBitVector _mReceivedAcks;
		internal NetStoredReliableMessage[] MStoredMessages;

		internal double MResendDelay;

		internal override int WindowSize { get { return _mWindowSize; } }

		internal NetReliableSenderChannel(NetConnection connection, int windowSize)
		{
			_mConnection = connection;
			_mWindowSize = windowSize;
			_mWindowStart = 0;
			_mSendStart = 0;
			_mReceivedAcks = new NetBitVector(NetConstants.NumSequenceNumbers);
			MStoredMessages = new NetStoredReliableMessage[_mWindowSize];
			MQueuedSends = new NetQueue<NetOutgoingMessage>(8);
			MResendDelay = _mConnection.GetResendDelay();
		}

		internal override int GetAllowedSends()
		{
			var retval = _mWindowSize - (_mSendStart + NetConstants.NumSequenceNumbers - _mWindowStart) % NetConstants.NumSequenceNumbers;
			NetException.Assert(retval >= 0 && retval <= _mWindowSize);
			return retval;
		}

		internal override void Reset()
		{
			_mReceivedAcks.Clear();
			for (var i = 0; i < MStoredMessages.Length; i++)
				MStoredMessages[i].Reset();
			MQueuedSends.Clear();
			_mWindowStart = 0;
			_mSendStart = 0;
		}

		internal override NetSendResult Enqueue(NetOutgoingMessage message)
		{
			MQueuedSends.Enqueue(message);
			_mConnection.MPeer.MNeedFlushSendQueue = true; // a race condition to this variable will simply result in a single superflous call to FlushSendQueue()
			if (MQueuedSends.Count <= GetAllowedSends())
				return NetSendResult.Sent;
			return NetSendResult.Queued;
		}

		// call this regularely
		internal override void SendQueuedMessages(double now)
		{
			//
			// resends
			//
			for (var i = 0; i < MStoredMessages.Length; i++)
			{
				var storedMsg = MStoredMessages[i];
				var om = storedMsg.Message;
				if (om == null)
					continue;

				var t = storedMsg.LastSent;
				if (t > 0 && now - t > MResendDelay)
				{
					// deduce sequence number
					/*
					int startSlot = m_windowStart % m_windowSize;
					int seqNr = m_windowStart;
					while (startSlot != i)
					{
						startSlot--;
						if (startSlot < 0)
							startSlot = m_windowSize - 1;
						seqNr--;
					}
					*/

					//m_connection.m_peer.LogVerbose("Resending due to delay #" + m_storedMessages[i].SequenceNumber + " " + om.ToString());
					_mConnection.MStatistics.MessageResent(MessageResendReason.Delay);

					Interlocked.Increment(ref om.MRecyclingCount); // increment this since it's being decremented in QueueSendMessage
					_mConnection.QueueSendMessage(om, storedMsg.SequenceNumber);

					MStoredMessages[i].LastSent = now;
					MStoredMessages[i].NumSent++;
				}
			}

			var num = GetAllowedSends();
			if (num < 1)
				return;

			// queued sends
			while (num > 0 && MQueuedSends.Count > 0)
			{
				NetOutgoingMessage om;
				if (MQueuedSends.TryDequeue(out om))
					ExecuteSend(now, om);
				num--;
				NetException.Assert(num == GetAllowedSends());
			}
		}

		private void ExecuteSend(double now, NetOutgoingMessage message)
		{
			var seqNr = _mSendStart;
			_mSendStart = (_mSendStart + 1) % NetConstants.NumSequenceNumbers;

			// must increment recycle count here, since it's decremented in QueueSendMessage and we want to keep it for the future in case or resends
			// we will decrement once more in DestoreMessage for final recycling
			Interlocked.Increment(ref message.MRecyclingCount);

			_mConnection.QueueSendMessage(message, seqNr);

			var storeIndex = seqNr % _mWindowSize;
			NetException.Assert(MStoredMessages[storeIndex].Message == null);

			MStoredMessages[storeIndex].NumSent++;
			MStoredMessages[storeIndex].Message = message;
			MStoredMessages[storeIndex].LastSent = now;
			MStoredMessages[storeIndex].SequenceNumber = seqNr;
		}

		private void DestoreMessage(int storeIndex)
		{
			var storedMessage = MStoredMessages[storeIndex].Message;

			// on each destore; reduce recyclingcount so that when all instances are destored, the outgoing message can be recycled
			Interlocked.Decrement(ref storedMessage.MRecyclingCount);
#if DEBUG
			if (storedMessage == null)
				throw new NetException("m_storedMessages[" + storeIndex + "].Message is null; sent " + MStoredMessages[storeIndex].NumSent + " times, last time " + (NetTime.Now - MStoredMessages[storeIndex].LastSent) + " seconds ago");
#else
			if (storedMessage != null)
			{
#endif
			if (storedMessage.MRecyclingCount <= 0)
				_mConnection.MPeer.Recycle(storedMessage);

#if !DEBUG
			}
#endif
			MStoredMessages[storeIndex] = new NetStoredReliableMessage();
		}

		// remoteWindowStart is remote expected sequence number; everything below this has arrived properly
		// seqNr is the actual nr received
		internal override void ReceiveAcknowledge(double now, int seqNr)
		{
			// late (dupe), on time or early ack?
			var relate = NetUtility.RelativeSequenceNumber(seqNr, _mWindowStart);

			if (relate < 0)
			{
				//m_connection.m_peer.LogDebug("Received late/dupe ack for #" + seqNr);
				return; // late/duplicate ack
			}

			if (relate == 0)
			{
				//m_connection.m_peer.LogDebug("Received right-on-time ack for #" + seqNr);

				// ack arrived right on time
				NetException.Assert(seqNr == _mWindowStart);

				_mReceivedAcks[_mWindowStart] = false;
				DestoreMessage(_mWindowStart % _mWindowSize);
				_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;

				// advance window if we already have early acks
				while (_mReceivedAcks.Get(_mWindowStart))
				{
					//m_connection.m_peer.LogDebug("Using early ack for #" + m_windowStart + "...");
					_mReceivedAcks[_mWindowStart] = false;
					DestoreMessage(_mWindowStart % _mWindowSize);

					NetException.Assert(MStoredMessages[_mWindowStart % _mWindowSize].Message == null); // should already be destored
					_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;
					//m_connection.m_peer.LogDebug("Advancing window to #" + m_windowStart);
				}

				return;
			}

			//
			// early ack... (if it has been sent!)
			//
			// If it has been sent either the m_windowStart message was lost
			// ... or the ack for that message was lost
			//

			//m_connection.m_peer.LogDebug("Received early ack for #" + seqNr);

			var sendRelate = NetUtility.RelativeSequenceNumber(seqNr, _mSendStart);
			if (sendRelate <= 0)
			{
				// yes, we've sent this message - it's an early (but valid) ack
				if (_mReceivedAcks[seqNr])
				{
					// we've already destored/been acked for this message
				}
				else
				{
					_mReceivedAcks[seqNr] = true;
				}
			}
			else if (sendRelate > 0)
			{
				// uh... we haven't sent this message yet? Weird, dupe or error...
				NetException.Assert(false, "Got ack for message not yet sent?");
				return;
			}

			// Ok, lets resend all missing acks
			var rnr = seqNr;
			do
			{
				rnr--;
				if (rnr < 0)
					rnr = NetConstants.NumSequenceNumbers - 1;

				if (_mReceivedAcks[rnr])
				{
					// m_connection.m_peer.LogDebug("Not resending #" + rnr + " (since we got ack)");
				}
				else
				{
					var slot = rnr % _mWindowSize;
					NetException.Assert(MStoredMessages[slot].Message != null);
					if (MStoredMessages[slot].NumSent == 1)
					{
						// just sent once; resend immediately since we found gap in ack sequence
						var rmsg = MStoredMessages[slot].Message;
						//m_connection.m_peer.LogVerbose("Resending #" + rnr + " (" + rmsg + ")");

						if (now - MStoredMessages[slot].LastSent < MResendDelay * 0.35)
						{
							// already resent recently
						}
						else
						{
							MStoredMessages[slot].LastSent = now;
							MStoredMessages[slot].NumSent++;
							_mConnection.MStatistics.MessageResent(MessageResendReason.HoleInSequence);
							Interlocked.Increment(ref rmsg.MRecyclingCount); // increment this since it's being decremented in QueueSendMessage
							_mConnection.QueueSendMessage(rmsg, rnr);
						}
					}
				}

			} while (rnr != _mWindowStart);
		}
	}
}
