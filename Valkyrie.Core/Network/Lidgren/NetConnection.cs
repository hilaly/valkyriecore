﻿using System;
using System.Diagnostics;
using System.Net;
using System.Threading;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Represents a connection to a remote peer
	/// </summary>
	[DebuggerDisplay("RemoteUniqueIdentifier={RemoteUniqueIdentifier} RemoteEndPoint={" + "remoteEndPoint}")]
	public partial class NetConnection
	{
		private const int MInfrequentEventsSkipFrames = 8; // number of heartbeats to skip checking for infrequent events (ping, timeout etc)
		private const int MMessageCoalesceFrames = 3; // number of heartbeats to wait for more incoming messages before sending packet

		public event Action OnStatusChanged;

		internal NetPeer MPeer;
		internal NetPeerConfiguration MPeerConfiguration;
		internal NetConnectionStatus MStatus;
		private NetConnectionStatus _mVisibleStatus;

		internal NetConnectionStatus MVisibleStatus
		{
			get => _mVisibleStatus;
			set
			{
				if(_mVisibleStatus == value)
					return;
				_mVisibleStatus = value;
				OnStatusChanged?.Invoke();
			}
		}

		internal IPEndPoint MRemoteEndPoint;
		internal NetSenderChannelBase[] MSendChannels;
		internal NetReceiverChannelBase[] MReceiveChannels;
		internal NetOutgoingMessage MLocalHailMessage;
		internal long MRemoteUniqueIdentifier;
		internal NetQueue<NetTuple<NetMessageType, int>> MQueuedOutgoingAcks;
		internal NetQueue<NetTuple<NetMessageType, int>> MQueuedIncomingAcks;
		private int _mSendBufferWritePtr;
		private int _mSendBufferNumMessages;
	    internal NetConnectionStatistics MStatistics;

		/// <summary>
		/// Gets or sets the application defined object containing data about the connection
		/// </summary>
		public object Tag { get; set; }

	    /// <summary>
		/// Gets the peer which holds this connection
		/// </summary>
		public NetPeer Peer { get { return MPeer; } }

		/// <summary>
		/// Gets the current status of the connection (synced to the last status message read)
		/// </summary>
		public NetConnectionStatus Status { get { return MVisibleStatus; } }

		/// <summary>
		/// Gets various statistics for this connection
		/// </summary>
		public NetConnectionStatistics Statistics { get { return MStatistics; } }

		/// <summary>
		/// Gets the remote endpoint for the connection
		/// </summary>
		public IPEndPoint RemoteEndPoint { get { return MRemoteEndPoint; } }

		/// <summary>
		/// Gets the unique identifier of the remote NetPeer for this connection
		/// </summary>
		public long RemoteUniqueIdentifier { get { return MRemoteUniqueIdentifier; } }

		/// <summary>
		/// Gets the local hail message that was sent as part of the handshake
		/// </summary>
		public NetOutgoingMessage LocalHailMessage { get { return MLocalHailMessage; } }

		// gets the time before automatically resending an unacked message
		internal double GetResendDelay()
		{
			var avgRtt = _mAverageRoundtripTime;
			if (avgRtt <= 0)
				avgRtt = 0.1; // "default" resend is based on 100 ms roundtrip time
			return 0.025 + avgRtt * 2.1; // 25 ms + double rtt
		}

		internal NetConnection(NetPeer peer, IPEndPoint remoteEndPoint)
		{
			MPeer = peer;
			MPeerConfiguration = MPeer.Configuration;
			MStatus = NetConnectionStatus.None;
			MVisibleStatus = NetConnectionStatus.None;
			MRemoteEndPoint = remoteEndPoint;
			MSendChannels = new NetSenderChannelBase[NetConstants.NumTotalChannels];
			MReceiveChannels = new NetReceiverChannelBase[NetConstants.NumTotalChannels];
			MQueuedOutgoingAcks = new NetQueue<NetTuple<NetMessageType, int>>(4);
			MQueuedIncomingAcks = new NetQueue<NetTuple<NetMessageType, int>>(4);
			MStatistics = new NetConnectionStatistics(this);
			_mAverageRoundtripTime = -1.0f;
			MCurrentMtu = MPeerConfiguration.MaximumTransmissionUnit;
		}

		/// <summary>
		/// Change the internal endpoint to this new one. Used when, during handshake, a switch in port is detected (due to NAT)
		/// </summary>
		internal void MutateEndPoint(IPEndPoint endPoint)
		{
			MRemoteEndPoint = endPoint;
		}

		internal void SetStatus(NetConnectionStatus status, string reason)
		{
			// user or library thread

			if (status == MStatus)
				return;

			MStatus = status;
			if (reason == null)
				reason = string.Empty;

			if (MStatus == NetConnectionStatus.Connected)
			{
				_mTimeoutDeadline = NetTime.Now + MPeerConfiguration.MConnectionTimeout;
				MPeer.LogVerbose("Timeout deadline initialized to  " + _mTimeoutDeadline);
			}

			if (MPeerConfiguration.IsMessageTypeEnabled(NetIncomingMessageType.StatusChanged))
			{
				var info = MPeer.CreateIncomingMessage(NetIncomingMessageType.StatusChanged, 4 + reason.Length + (reason.Length > 126 ? 2 : 1));
				info.MSenderConnection = this;
				info.MSenderEndPoint = MRemoteEndPoint;
				info.Write((byte)MStatus);
				info.Write(reason);
				MPeer.ReleaseMessage(info);
			}
			else
			{
				// app dont want those messages, update visible status immediately
				MVisibleStatus = MStatus;
			}
		}

		internal void Heartbeat(double now, uint frameCounter)
		{
			MPeer.VerifyNetworkThread();

			NetException.Assert(MStatus != NetConnectionStatus.InitiatedConnect && MStatus != NetConnectionStatus.RespondedConnect);

			if (frameCounter % MInfrequentEventsSkipFrames == 0)
			{
				if (now > _mTimeoutDeadline)
				{
					//
					// connection timed out
					//
					MPeer.LogVerbose("Connection timed out at " + now + " deadline was " + _mTimeoutDeadline);
					ExecuteDisconnect("Connection timed out", true);
					return;
				}

				// send ping?
				if (MStatus == NetConnectionStatus.Connected)
				{
					if (now > _mSentPingTime + MPeer.MConfiguration.MPingInterval)
						SendPing();

					// handle expand mtu
					MtuExpansionHeartbeat(now);
				}

				if (MDisconnectRequested)
				{
					ExecuteDisconnect(MDisconnectMessage, MDisconnectReqSendBye);
					return;
				}
			}

			bool connectionReset; // TODO: handle connection reset

			//
			// Note: at this point m_sendBufferWritePtr and m_sendBufferNumMessages may be non-null; resends may already be queued up
			//

			var sendBuffer = MPeer.MSendBuffer;
			var mtu = MCurrentMtu;

			if (frameCounter % MMessageCoalesceFrames == 0) // coalesce a few frames
			{
				//
				// send ack messages
				//
				while (MQueuedOutgoingAcks.Count > 0)
				{
					var acks = (mtu - (_mSendBufferWritePtr + 5)) / 3; // 3 bytes per actual ack
					if (acks > MQueuedOutgoingAcks.Count)
						acks = MQueuedOutgoingAcks.Count;

					NetException.Assert(acks > 0);

					_mSendBufferNumMessages++;

					// write acks header
					sendBuffer[_mSendBufferWritePtr++] = (byte)NetMessageType.Acknowledge;
					sendBuffer[_mSendBufferWritePtr++] = 0; // no sequence number
					sendBuffer[_mSendBufferWritePtr++] = 0; // no sequence number
					var len = acks * 3 * 8; // bits
					sendBuffer[_mSendBufferWritePtr++] = (byte)len;
					sendBuffer[_mSendBufferWritePtr++] = (byte)(len >> 8);

					// write acks
					for (var i = 0; i < acks; i++)
					{
						NetTuple<NetMessageType, int> tuple;
						MQueuedOutgoingAcks.TryDequeue(out tuple);

						//m_peer.LogVerbose("Sending ack for " + tuple.Item1 + "#" + tuple.Item2);

						sendBuffer[_mSendBufferWritePtr++] = (byte)tuple.Item1;
						sendBuffer[_mSendBufferWritePtr++] = (byte)tuple.Item2;
						sendBuffer[_mSendBufferWritePtr++] = (byte)(tuple.Item2 >> 8);
					}

					if (MQueuedOutgoingAcks.Count > 0)
					{
						// send packet and go for another round of acks
						NetException.Assert(_mSendBufferWritePtr > 0 && _mSendBufferNumMessages > 0);
						MPeer.SendPacket(_mSendBufferWritePtr, MRemoteEndPoint, _mSendBufferNumMessages, out connectionReset);
						MStatistics.PacketSent(_mSendBufferWritePtr, 1);
						_mSendBufferWritePtr = 0;
						_mSendBufferNumMessages = 0;
					}
				}

				//
				// Parse incoming acks (may trigger resends)
				//
				NetTuple<NetMessageType, int> incAck;
				while (MQueuedIncomingAcks.TryDequeue(out incAck))
				{
					//m_peer.LogVerbose("Received ack for " + acktp + "#" + seqNr);
					var chan = MSendChannels[(int)incAck.Item1 - 1];

					// If we haven't sent a message on this channel there is no reason to ack it

					chan?.ReceiveAcknowledge(now, incAck.Item2);
				}
			}

			//
			// send queued messages
			//
			if (MPeer.MExecuteFlushSendQueue)
			{
				for (var i = MSendChannels.Length - 1; i >= 0; i--)    // Reverse order so reliable messages are sent first
				{
					var channel = MSendChannels[i];
					NetException.Assert(_mSendBufferWritePtr < 1 || _mSendBufferNumMessages > 0);
					if (channel != null)
					{
						channel.SendQueuedMessages(now);
						if (channel.QueuedSendsCount > 0)
							MPeer.MNeedFlushSendQueue = true; // failed to send all queued sends; likely a full window - need to try again
					}
					NetException.Assert(_mSendBufferWritePtr < 1 || _mSendBufferNumMessages > 0);
				}
			}

			//
			// Put on wire data has been written to send buffer but not yet sent
			//
			if (_mSendBufferWritePtr > 0)
			{
				MPeer.VerifyNetworkThread();
				NetException.Assert(_mSendBufferWritePtr > 0 && _mSendBufferNumMessages > 0);
				MPeer.SendPacket(_mSendBufferWritePtr, MRemoteEndPoint, _mSendBufferNumMessages, out connectionReset);
				MStatistics.PacketSent(_mSendBufferWritePtr, _mSendBufferNumMessages);
				_mSendBufferWritePtr = 0;
				_mSendBufferNumMessages = 0;
			}
		}
		
		// Queue an item for immediate sending on the wire
		// This method is called from the ISenderChannels
		internal void QueueSendMessage(NetOutgoingMessage om, int seqNr)
		{
			MPeer.VerifyNetworkThread();

			var sz = om.GetEncodedSize();
			//if (sz > m_currentMTU)
			//	m_peer.LogWarning("Message larger than MTU! Fragmentation must have failed!");

			bool connReset; // TODO: handle connection reset

			// can fit this message together with previously written to buffer?
			if (_mSendBufferWritePtr + sz > MCurrentMtu)
			{
				if (_mSendBufferWritePtr > 0 && _mSendBufferNumMessages > 0)
				{
					// previous message in buffer; send these first
					MPeer.SendPacket(_mSendBufferWritePtr, MRemoteEndPoint, _mSendBufferNumMessages, out connReset);
					MStatistics.PacketSent(_mSendBufferWritePtr, _mSendBufferNumMessages);
					_mSendBufferWritePtr = 0;
					_mSendBufferNumMessages = 0;
				}
			}

			// encode it into buffer regardless if it (now) fits within MTU or not
			_mSendBufferWritePtr = om.Encode(MPeer.MSendBuffer, _mSendBufferWritePtr, seqNr);
			_mSendBufferNumMessages++;

			if (_mSendBufferWritePtr > MCurrentMtu)
			{
				// send immediately; we're already over MTU
				MPeer.SendPacket(_mSendBufferWritePtr, MRemoteEndPoint, _mSendBufferNumMessages, out connReset);
				MStatistics.PacketSent(_mSendBufferWritePtr, _mSendBufferNumMessages);
				_mSendBufferWritePtr = 0;
				_mSendBufferNumMessages = 0;
			}

			Interlocked.Decrement(ref om.MRecyclingCount);
		}

		/// <summary>
		/// Send a message to this remote connection
		/// </summary>
		/// <param name="msg">The message to send</param>
		/// <param name="method">How to deliver the message</param>
		/// <param name="sequenceChannel">Sequence channel within the delivery method</param>
		public NetSendResult SendMessage(NetOutgoingMessage msg, NetDeliveryMethod method, int sequenceChannel)
		{
			return MPeer.SendMessage(msg, this, method, sequenceChannel);
		}

		// called by SendMessage() and NetPeer.SendMessage; ie. may be user thread
		internal NetSendResult EnqueueMessage(NetOutgoingMessage msg, NetDeliveryMethod method, int sequenceChannel)
		{
			if (MStatus != NetConnectionStatus.Connected)
				return NetSendResult.FailedNotConnected;

			var tp = (NetMessageType)((int)method + sequenceChannel);
			msg.MMessageType = tp;

			// TODO: do we need to make this more thread safe?
			var channelSlot = (int)method - 1 + sequenceChannel;
			var chan = MSendChannels[channelSlot] ?? CreateSenderChannel(tp);

		    if (method != NetDeliveryMethod.Unreliable && method != NetDeliveryMethod.UnreliableSequenced && msg.GetEncodedSize() > MCurrentMtu)
				MPeer.ThrowOrLog("Reliable message too large! Fragmentation failure?");

			var retval = chan.Enqueue(msg);
			//if (retval == NetSendResult.Sent && m_peerConfiguration.m_autoFlushSendQueue == false)
			//	retval = NetSendResult.Queued; // queued since we're not autoflushing
			return retval;
		}

		// may be on user thread
		private NetSenderChannelBase CreateSenderChannel(NetMessageType tp)
		{
			NetSenderChannelBase chan;
			lock (MSendChannels)
			{
				var method = NetUtility.GetDeliveryMethod(tp);
				var sequenceChannel = (int)tp - (int)method;

				var channelSlot = (int)method - 1 + sequenceChannel;
				if (MSendChannels[channelSlot] != null)
				{
					// we were pre-empted by another call to this method
					chan = MSendChannels[channelSlot];
				}
				else
				{
					switch (method)
					{
						case NetDeliveryMethod.Unreliable:
						case NetDeliveryMethod.UnreliableSequenced:
							chan = new NetUnreliableSenderChannel(this, NetUtility.GetWindowSize(method));
							break;
						case NetDeliveryMethod.ReliableOrdered:
							chan = new NetReliableSenderChannel(this, NetUtility.GetWindowSize(method));
							break;
						case NetDeliveryMethod.ReliableSequenced:
						case NetDeliveryMethod.ReliableUnordered:
						default:
							chan = new NetReliableSenderChannel(this, NetUtility.GetWindowSize(method));
							break;
					}
					MSendChannels[channelSlot] = chan;
				}
			}

			return chan;
		}

		// received a library message while Connected
		internal void ReceivedLibraryMessage(NetMessageType tp, int ptr, int payloadLength)
		{
			MPeer.VerifyNetworkThread();

			var now = NetTime.Now;

			switch (tp)
			{
				case NetMessageType.Connect:
					MPeer.LogDebug("Received handshake message (" + tp + ") despite connection being in place");
					break;

				case NetMessageType.ConnectResponse:
					// handshake message must have been lost
					HandleConnectResponse(now, tp, ptr, payloadLength);
					break;

				case NetMessageType.ConnectionEstablished:
					// do nothing, all's well
					break;

				case NetMessageType.LibraryError:
					MPeer.ThrowOrLog("LibraryError received by ReceivedLibraryMessage; this usually indicates a malformed message");
					break;

				case NetMessageType.Disconnect:
					var msg = MPeer.SetupReadHelperMessage(ptr, payloadLength);

					MDisconnectRequested = true;
					MDisconnectMessage = msg.ReadString();
					MDisconnectReqSendBye = false;
					//ExecuteDisconnect(msg.ReadString(), false);
					break;
				case NetMessageType.Acknowledge:
					for (var i = 0; i < payloadLength; i+=3)
					{
						var acktp = (NetMessageType)MPeer.MReceiveBuffer[ptr++]; // netmessagetype
						int seqNr = MPeer.MReceiveBuffer[ptr++];
						seqNr |= MPeer.MReceiveBuffer[ptr++] << 8;

						// need to enqueue this and handle it in the netconnection heartbeat; so be able to send resends together with normal sends
						MQueuedIncomingAcks.Enqueue(new NetTuple<NetMessageType, int>(acktp, seqNr));
					}
					break;
				case NetMessageType.Ping:
					int pingNr = MPeer.MReceiveBuffer[ptr++];
					SendPong(pingNr);
					break;
				case NetMessageType.Pong:
					var pmsg = MPeer.SetupReadHelperMessage(ptr, payloadLength);
					int pongNr = pmsg.ReadByte();
					var remoteSendTime = pmsg.ReadSingle();
					ReceivedPong(now, pongNr, remoteSendTime);
					break;
				case NetMessageType.ExpandMtuRequest:
					SendMtuSuccess(payloadLength);
					break;
				case NetMessageType.ExpandMtuSuccess:
					if (MPeer.Configuration.AutoExpandMtu == false)
					{
						MPeer.LogDebug("Received ExpandMTURequest altho AutoExpandMTU is turned off!");
						break;
					}
					var emsg = MPeer.SetupReadHelperMessage(ptr, payloadLength);
					var size = emsg.ReadInt32();
					HandleExpandMtuSuccess(now, size);
					break;
				case NetMessageType.NatIntroduction:
					// Unusual situation where server is actually already known, but got a nat introduction - oh well, lets handle it as usual
					MPeer.HandleNatIntroduction(ptr);
					break;
				default:
					MPeer.LogWarning("Connection received unhandled library message: " + tp);
					break;
			}
		}

		internal void ReceivedMessage(NetIncomingMessage msg)
		{
			MPeer.VerifyNetworkThread();

			var tp = msg.MReceivedMessageType;

			var channelSlot = (int)tp - 1;
			var chan = MReceiveChannels[channelSlot];
			if (chan == null)
				chan = CreateReceiverChannel(tp);

			chan.ReceiveMessage(msg);
		}

		private NetReceiverChannelBase CreateReceiverChannel(NetMessageType tp)
		{
			MPeer.VerifyNetworkThread();

			// create receiver channel
			NetReceiverChannelBase chan;
			var method = NetUtility.GetDeliveryMethod(tp);
			switch (method)
			{
				case NetDeliveryMethod.Unreliable:
					chan = new NetUnreliableUnorderedReceiver(this);
					break;
				case NetDeliveryMethod.ReliableOrdered:
					chan = new NetReliableOrderedReceiver(this, NetConstants.ReliableOrderedWindowSize);
					break;
				case NetDeliveryMethod.UnreliableSequenced:
					chan = new NetUnreliableSequencedReceiver(this);
					break;
				case NetDeliveryMethod.ReliableUnordered:
					chan = new NetReliableUnorderedReceiver(this, NetConstants.ReliableOrderedWindowSize);
					break;
				case NetDeliveryMethod.ReliableSequenced:
					chan = new NetReliableSequencedReceiver(this, NetConstants.ReliableSequencedWindowSize);
					break;
				default:
					throw new NetException("Unhandled NetDeliveryMethod!");
			}

			var channelSlot = (int)tp - 1;
			NetException.Assert(MReceiveChannels[channelSlot] == null);
			MReceiveChannels[channelSlot] = chan;

			return chan;
		}

		internal void QueueAck(NetMessageType tp, int sequenceNumber)
		{
			MQueuedOutgoingAcks.Enqueue(new NetTuple<NetMessageType, int>(tp, sequenceNumber));
		}

		/// <summary>
		/// Zero windowSize indicates that the channel is not yet instantiated (used)
		/// Negative freeWindowSlots means this amount of messages are currently queued but delayed due to closed window
		/// </summary>
		public void GetSendQueueInfo(NetDeliveryMethod method, int sequenceChannel, out int windowSize, out int freeWindowSlots)
		{
			var channelSlot = (int)method - 1 + sequenceChannel;
			var chan = MSendChannels[channelSlot];
			if (chan == null)
			{
				windowSize = NetUtility.GetWindowSize(method);
				freeWindowSlots = windowSize;
				return;
			}

			windowSize = chan.WindowSize;
			freeWindowSlots = chan.GetFreeWindowSlots();
		}

		public bool CanSendImmediately(NetDeliveryMethod method, int sequenceChannel)
		{
			var channelSlot = (int)method - 1 + sequenceChannel;
			var chan = MSendChannels[channelSlot];
			if (chan == null)
				return true;
			return chan.GetFreeWindowSlots() > 0;
		}

		internal void Shutdown(string reason)
		{
			ExecuteDisconnect(reason, true);
		}

		/// <summary>
		/// Returns a string that represents this object
		/// </summary>
		public override string ToString()
		{
			return "[NetConnection to " + MRemoteEndPoint + "]";
		}
	}
}
