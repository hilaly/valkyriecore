﻿namespace Valkyrie.Communication.Network.Lidgren
{
    internal class ReceivedFragmentGroup
    {
        //public float LastReceived;
        public byte[] Data;
        public NetBitVector ReceivedChunks;
    }
}