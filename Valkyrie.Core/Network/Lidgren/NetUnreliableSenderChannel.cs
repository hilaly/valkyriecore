﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Sender part of Selective repeat ARQ for a particular NetChannel
	/// </summary>
	internal sealed class NetUnreliableSenderChannel : NetSenderChannelBase
	{
		private readonly NetConnection _mConnection;
		private int _mWindowStart;
		private readonly int _mWindowSize;
		private int _mSendStart;

		private readonly NetBitVector _mReceivedAcks;

		internal override int WindowSize { get { return _mWindowSize; } }

		internal NetUnreliableSenderChannel(NetConnection connection, int windowSize)
		{
			_mConnection = connection;
			_mWindowSize = windowSize;
			_mWindowStart = 0;
			_mSendStart = 0;
			_mReceivedAcks = new NetBitVector(NetConstants.NumSequenceNumbers);
			MQueuedSends = new NetQueue<NetOutgoingMessage>(8);
		}

		internal override int GetAllowedSends()
		{
			var retval = _mWindowSize - (_mSendStart + NetConstants.NumSequenceNumbers - _mWindowStart) % _mWindowSize;
			NetException.Assert(retval >= 0 && retval <= _mWindowSize);
			return retval;
		}

		internal override void Reset()
		{
			_mReceivedAcks.Clear();
			MQueuedSends.Clear();
			_mWindowStart = 0;
			_mSendStart = 0;
		}

		internal override NetSendResult Enqueue(NetOutgoingMessage message)
		{
			var queueLen = MQueuedSends.Count + 1;
			var left = GetAllowedSends();
			if (queueLen > left || message.LengthBytes > _mConnection.MCurrentMtu && _mConnection.MPeerConfiguration.UnreliableSizeBehaviour == NetUnreliableSizeBehaviour.DropAboveMtu)
			{
				// drop message
				return NetSendResult.Dropped;
			}

			MQueuedSends.Enqueue(message);
			_mConnection.MPeer.MNeedFlushSendQueue = true; // a race condition to this variable will simply result in a single superflous call to FlushSendQueue()
			return NetSendResult.Sent;
		}

		// call this regularely
		internal override void SendQueuedMessages(double now)
		{
			var num = GetAllowedSends();
			if (num < 1)
				return;

			// queued sends
			while (num > 0 && MQueuedSends.Count > 0)
			{
				NetOutgoingMessage om;
				if (MQueuedSends.TryDequeue(out om))
					ExecuteSend(om);
				num--;
			}
		}

		private void ExecuteSend(NetOutgoingMessage message)
		{
			_mConnection.MPeer.VerifyNetworkThread();

			var seqNr = _mSendStart;
			_mSendStart = (_mSendStart + 1) % NetConstants.NumSequenceNumbers;

			_mConnection.QueueSendMessage(message, seqNr);

			if (message.MRecyclingCount <= 0)
				_mConnection.MPeer.Recycle(message);
		}
		
		// remoteWindowStart is remote expected sequence number; everything below this has arrived properly
		// seqNr is the actual nr received
		internal override void ReceiveAcknowledge(double now, int seqNr)
		{
			// late (dupe), on time or early ack?
			var relate = NetUtility.RelativeSequenceNumber(seqNr, _mWindowStart);

			if (relate < 0)
			{
				//m_connection.m_peer.LogDebug("Received late/dupe ack for #" + seqNr);
				return; // late/duplicate ack
			}

			if (relate == 0)
			{
				//m_connection.m_peer.LogDebug("Received right-on-time ack for #" + seqNr);

				// ack arrived right on time
				NetException.Assert(seqNr == _mWindowStart);

				_mReceivedAcks[_mWindowStart] = false;
				_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;

				return;
			}

			// Advance window to this position
			_mReceivedAcks[seqNr] = true;

			while (_mWindowStart != seqNr)
			{
				_mReceivedAcks[_mWindowStart] = false;
				_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;
			}
		}
	}
}
