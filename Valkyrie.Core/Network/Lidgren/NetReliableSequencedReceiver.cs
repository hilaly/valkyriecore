﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	internal sealed class NetReliableSequencedReceiver : NetReceiverChannelBase
	{
		private int _mWindowStart;
		private readonly int _mWindowSize;

		public NetReliableSequencedReceiver(NetConnection connection, int windowSize)
			: base(connection)
		{
			_mWindowSize = windowSize;
		}

		private void AdvanceWindow()
		{
			_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;
		}

		internal override void ReceiveMessage(NetIncomingMessage message)
		{
			var nr = message.MSequenceNumber;

			var relate = NetUtility.RelativeSequenceNumber(nr, _mWindowStart);

			// ack no matter what
			MConnection.QueueAck(message.MReceivedMessageType, nr);

			if (relate == 0)
			{
				// Log("Received message #" + message.SequenceNumber + " right on time");

				//
				// excellent, right on time
				//

				AdvanceWindow();
				MPeer.ReleaseMessage(message);
				return;
			}

			if (relate < 0)
			{
				MPeer.LogVerbose("Received message #" + message.MSequenceNumber + " DROPPING LATE or DUPE");
				return;
			}

			// relate > 0 = early message
			if (relate > _mWindowSize)
			{
				// too early message!
				MPeer.LogDebug("Received " + message + " TOO EARLY! Expected " + _mWindowStart);
				return;
			}

			// ok
			_mWindowStart = (_mWindowStart + relate) % NetConstants.NumSequenceNumbers;
			MPeer.ReleaseMessage(message);
		}
	}
}
