﻿namespace Valkyrie.Communication.Network.Lidgren
{
    /// <summary>
    /// Behaviour of unreliable sends above MTU
    /// </summary>
    public enum NetUnreliableSizeBehaviour
    {
        /// <summary>
        /// Sending an unreliable message will ignore MTU and send everything in a single packet; this is the new default
        /// </summary>
        IgnoreMtu = 0,

        /// <summary>
        /// Old behaviour; use normal fragmentation for unreliable messages - if a fragment is dropped, memory for received fragments are never reclaimed!
        /// </summary>
        NormalFragmentation = 1,

        /// <summary>
        /// Alternate behaviour; just drops unreliable messages above MTU
        /// </summary>
        DropAboveMtu = 2
    }
}