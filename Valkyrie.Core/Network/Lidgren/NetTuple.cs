﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	// replace with BCL 4.0 Tuple<> when appropriate
	internal struct NetTuple<TA, TB>
	{
		public TA Item1;
		public TB Item2;

		public NetTuple(TA item1, TB item2)
		{
			Item1 = item1;
			Item2 = item2;
		}
	}
}
