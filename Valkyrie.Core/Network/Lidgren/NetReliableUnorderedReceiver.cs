﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	internal sealed class NetReliableUnorderedReceiver : NetReceiverChannelBase
	{
		private int _mWindowStart;
		private readonly int _mWindowSize;
		private readonly NetBitVector _mEarlyReceived;

		public NetReliableUnorderedReceiver(NetConnection connection, int windowSize)
			: base(connection)
		{
			_mWindowSize = windowSize;
			_mEarlyReceived = new NetBitVector(windowSize);
		}

		private void AdvanceWindow()
		{
			_mEarlyReceived.Set(_mWindowStart % _mWindowSize, false);
			_mWindowStart = (_mWindowStart + 1) % NetConstants.NumSequenceNumbers;
		}

		internal override void ReceiveMessage(NetIncomingMessage message)
		{
			var relate = NetUtility.RelativeSequenceNumber(message.MSequenceNumber, _mWindowStart);

			// ack no matter what
			MConnection.QueueAck(message.MReceivedMessageType, message.MSequenceNumber);

			if (relate == 0)
			{
				// Log("Received message #" + message.SequenceNumber + " right on time");

				//
				// excellent, right on time
				//
				//m_peer.LogVerbose("Received RIGHT-ON-TIME " + message);

				AdvanceWindow();
				MPeer.ReleaseMessage(message);

				// release withheld messages
				var nextSeqNr = (message.MSequenceNumber + 1) % NetConstants.NumSequenceNumbers;

				while (_mEarlyReceived[nextSeqNr % _mWindowSize])
				{
					//message = m_withheldMessages[nextSeqNr % m_windowSize];
					//NetException.Assert(message != null);

					// remove it from withheld messages
					//m_withheldMessages[nextSeqNr % m_windowSize] = null;

					//m_peer.LogVerbose("Releasing withheld message #" + message);

					//m_peer.ReleaseMessage(message);

					AdvanceWindow();
					nextSeqNr++;
				}

				return;
			}

			if (relate < 0)
			{
				// duplicate
				MPeer.LogVerbose("Received message #" + message.MSequenceNumber + " DROPPING DUPLICATE");
				return;
			}

			// relate > 0 = early message
			if (relate > _mWindowSize)
			{
				// too early message!
				MPeer.LogDebug("Received " + message + " TOO EARLY! Expected " + _mWindowStart);
				return;
			}

			_mEarlyReceived.Set(message.MSequenceNumber % _mWindowSize, true);
			//m_peer.LogVerbose("Received " + message + " WITHHOLDING, waiting for " + m_windowStart);
			//m_withheldMessages[message.m_sequenceNumber % m_windowSize] = message;

			MPeer.ReleaseMessage(message);
		}
	}
}
