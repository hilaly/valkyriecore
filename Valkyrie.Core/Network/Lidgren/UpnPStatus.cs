﻿namespace Valkyrie.Communication.Network.Lidgren
{
    /// <summary>
    /// Status of the UPnP capabilities
    /// </summary>
    public enum UpnPStatus
    {
        /// <summary>
        /// Still discovering UPnP capabilities
        /// </summary>
        Discovering,

        /// <summary>
        /// UPnP is not available
        /// </summary>
        NotAvailable,

        /// <summary>
        /// UPnP is available and ready to use
        /// </summary>
        Available
    }
}