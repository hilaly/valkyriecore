﻿

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// Multiply With Carry random
	/// </summary>
	public class MwcRandom : NetRandom
	{
		/// <summary>
		/// Get global instance of MWCRandom
		/// </summary>
		public new static readonly MwcRandom Instance = new MwcRandom();

		private uint _mW, _mZ;

		/// <summary>
		/// Constructor with randomized seed
		/// </summary>
		public MwcRandom()
		{
			Initialize(NetRandomSeed.GetUInt64());
		}

		/// <summary>
		/// (Re)initialize this instance with provided 32 bit seed
		/// </summary>
		
		public override void Initialize(uint seed)
		{
			_mW = seed;
			_mZ = seed * 16777619;
		}

		/// <summary>
		/// (Re)initialize this instance with provided 64 bit seed
		/// </summary>
		
		public void Initialize(ulong seed)
		{
			_mW = (uint)seed;
			_mZ = (uint)(seed >> 32);
		}

		/// <summary>
		/// Generates a random value from UInt32.MinValue to UInt32.MaxValue, inclusively
		/// </summary>
		
		public override uint NextUInt32()
		{
			_mZ = 36969 * (_mZ & 65535) + (_mZ >> 16);
			_mW = 18000 * (_mW & 65535) + (_mW >> 16);
			return (_mZ << 16) + _mW;
		}
	}
}
