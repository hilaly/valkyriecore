﻿/* Copyright (c) 2010 Michael Lidgren

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom
the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

using System;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	public partial class NetBuffer
	{
		/// <summary>
		/// Gets the internal data buffer
		/// </summary>
		public byte[] PeekDataBuffer() { return MData; }

		//
		// 1 bit
		//
		/// <summary>
		/// Reads a 1-bit Boolean without advancing the read pointer
		/// </summary>
		public bool PeekBoolean()
		{
			NetException.Assert(MBitLength - MReadPosition >= 1, CReadOverflowError);
			var retval = NetBitWriter.ReadByte(MData, 1, MReadPosition);
			return retval > 0 ? true : false;
		}

		//
		// 8 bit 
		//
		/// <summary>
		/// Reads a Byte without advancing the read pointer
		/// </summary>
		public byte PeekByte()
		{
			NetException.Assert(MBitLength - MReadPosition >= 8, CReadOverflowError);
			var retval = NetBitWriter.ReadByte(MData, 8, MReadPosition);
			return retval;
		}

		/// <summary>
		/// Reads an SByte without advancing the read pointer
		/// </summary>
		
		public sbyte PeekSByte()
		{
			NetException.Assert(MBitLength - MReadPosition >= 8, CReadOverflowError);
			var retval = NetBitWriter.ReadByte(MData, 8, MReadPosition);
			return (sbyte)retval;
		}

		/// <summary>
		/// Reads the specified number of bits into a Byte without advancing the read pointer
		/// </summary>
		public byte PeekByte(int numberOfBits)
		{
			var retval = NetBitWriter.ReadByte(MData, numberOfBits, MReadPosition);
			return retval;
		}

		/// <summary>
		/// Reads the specified number of bytes without advancing the read pointer
		/// </summary>
		public byte[] PeekBytes(int numberOfBytes)
		{
			NetException.Assert(MBitLength - MReadPosition >= numberOfBytes * 8, CReadOverflowError);

			var retval = new byte[numberOfBytes];
			NetBitWriter.ReadBytes(MData, numberOfBytes, MReadPosition, retval, 0);
			return retval;
		}

		/// <summary>
		/// Reads the specified number of bytes without advancing the read pointer
		/// </summary>
		public void PeekBytes(byte[] into, int offset, int numberOfBytes)
		{
			NetException.Assert(MBitLength - MReadPosition >= numberOfBytes * 8, CReadOverflowError);
			NetException.Assert(offset + numberOfBytes <= into.Length);

			NetBitWriter.ReadBytes(MData, numberOfBytes, MReadPosition, into, offset);
		}

		//
		// 16 bit
		//
		/// <summary>
		/// Reads an Int16 without advancing the read pointer
		/// </summary>
		public Int16 PeekInt16()
		{
			NetException.Assert(MBitLength - MReadPosition >= 16, CReadOverflowError);
			uint retval = NetBitWriter.ReadUInt16(MData, 16, MReadPosition);
			return (short)retval;
		}

		/// <summary>
		/// Reads a UInt16 without advancing the read pointer
		/// </summary>
		
		public UInt16 PeekUInt16()
		{
			NetException.Assert(MBitLength - MReadPosition >= 16, CReadOverflowError);
			uint retval = NetBitWriter.ReadUInt16(MData, 16, MReadPosition);
			return (ushort)retval;
		}

		//
		// 32 bit
		//
		/// <summary>
		/// Reads an Int32 without advancing the read pointer
		/// </summary>
		public Int32 PeekInt32()
		{
			NetException.Assert(MBitLength - MReadPosition >= 32, CReadOverflowError);
			var retval = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			return (Int32)retval;
		}

		/// <summary>
		/// Reads the specified number of bits into an Int32 without advancing the read pointer
		/// </summary>
		public Int32 PeekInt32(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 32, "ReadInt() can only read between 1 and 32 bits");
			NetException.Assert(MBitLength - MReadPosition >= numberOfBits, CReadOverflowError);

			var retval = NetBitWriter.ReadUInt32(MData, numberOfBits, MReadPosition);

			if (numberOfBits == 32)
				return (int)retval;

			var signBit = 1 << (numberOfBits - 1);
			if ((retval & signBit) == 0)
				return (int)retval; // positive

			// negative
			unchecked
			{
				var mask = (uint)-1 >> (33 - numberOfBits);
				var tmp = (retval & mask) + 1;
				return -(int)tmp;
			}
		}

		/// <summary>
		/// Reads a UInt32 without advancing the read pointer
		/// </summary>
		
		public UInt32 PeekUInt32()
		{
			NetException.Assert(MBitLength - MReadPosition >= 32, CReadOverflowError);
			var retval = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			return retval;
		}

		/// <summary>
		/// Reads the specified number of bits into a UInt32 without advancing the read pointer
		/// </summary>
		
		public UInt32 PeekUInt32(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 32, "ReadUInt() can only read between 1 and 32 bits");
			//NetException.Assert(m_bitLength - m_readBitPtr >= numberOfBits, "tried to read past buffer size");

			var retval = NetBitWriter.ReadUInt32(MData, numberOfBits, MReadPosition);
			return retval;
		}

		//
		// 64 bit
		//
		/// <summary>
		/// Reads a UInt64 without advancing the read pointer
		/// </summary>
		
		public UInt64 PeekUInt64()
		{
			NetException.Assert(MBitLength - MReadPosition >= 64, CReadOverflowError);

			ulong low = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
			ulong high = NetBitWriter.ReadUInt32(MData, 32, MReadPosition + 32);

			var retval = low + (high << 32);

			return retval;
		}

		/// <summary>
		/// Reads an Int64 without advancing the read pointer
		/// </summary>
		public Int64 PeekInt64()
		{
			NetException.Assert(MBitLength - MReadPosition >= 64, CReadOverflowError);
			unchecked
			{
				var retval = PeekUInt64();
				var longRetval = (long)retval;
				return longRetval;
			}
		}

		/// <summary>
		/// Reads the specified number of bits into an UInt64 without advancing the read pointer
		/// </summary>
		
		public UInt64 PeekUInt64(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits <= 64, "ReadUInt() can only read between 1 and 64 bits");
			NetException.Assert(MBitLength - MReadPosition >= numberOfBits, CReadOverflowError);

			ulong retval;
			if (numberOfBits <= 32)
			{
				retval = NetBitWriter.ReadUInt32(MData, numberOfBits, MReadPosition);
			}
			else
			{
				retval = NetBitWriter.ReadUInt32(MData, 32, MReadPosition);
				retval |= NetBitWriter.ReadUInt32(MData, numberOfBits - 32, MReadPosition) << 32;
			}
			return retval;
		}

		/// <summary>
		/// Reads the specified number of bits into an Int64 without advancing the read pointer
		/// </summary>
		public Int64 PeekInt64(int numberOfBits)
		{
			NetException.Assert(numberOfBits > 0 && numberOfBits < 65, "ReadInt64(bits) can only read between 1 and 64 bits");
			return (long)PeekUInt64(numberOfBits);
		}

		//
		// Floating point
		//
		/// <summary>
		/// Reads a 32-bit Single without advancing the read pointer
		/// </summary>
		public float PeekFloat()
		{
			return PeekSingle();
		}

		/// <summary>
		/// Reads a 32-bit Single without advancing the read pointer
		/// </summary>
		public float PeekSingle()
		{
			NetException.Assert(MBitLength - MReadPosition >= 32, CReadOverflowError);

			if ((MReadPosition & 7) == 0) // read directly
			{
				var retval = BitConverter.ToSingle(MData, MReadPosition >> 3);
				return retval;
			}

			var bytes = PeekBytes(4);
			return BitConverter.ToSingle(bytes, 0);
		}

		/// <summary>
		/// Reads a 64-bit Double without advancing the read pointer
		/// </summary>
		public double PeekDouble()
		{
			NetException.Assert(MBitLength - MReadPosition >= 64, CReadOverflowError);

			if ((MReadPosition & 7) == 0) // read directly
			{
				// read directly
				var retval = BitConverter.ToDouble(MData, MReadPosition >> 3);
				return retval;
			}

			var bytes = PeekBytes(8);
			return BitConverter.ToDouble(bytes, 0);
		}

		/// <summary>
		/// Reads a string without advancing the read pointer
		/// </summary>
		public string PeekString()
		{
			var wasReadPosition = MReadPosition;
			var retval = ReadString();
			MReadPosition = wasReadPosition;
			return retval;
		}
	}
}

