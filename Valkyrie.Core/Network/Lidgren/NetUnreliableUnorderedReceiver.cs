﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	internal sealed class NetUnreliableUnorderedReceiver : NetReceiverChannelBase
	{
		public NetUnreliableUnorderedReceiver(NetConnection connection)
			: base(connection)
		{
		}

		internal override void ReceiveMessage(NetIncomingMessage msg)
		{
			// ack no matter what
			MConnection.QueueAck(msg.MReceivedMessageType, msg.MSequenceNumber);

			MPeer.ReleaseMessage(msg);
		}
	}
}
