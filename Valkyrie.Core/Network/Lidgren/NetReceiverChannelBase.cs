﻿// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	internal abstract class NetReceiverChannelBase
	{
		internal NetPeer MPeer;
		internal NetConnection MConnection;

	    protected NetReceiverChannelBase(NetConnection connection)
		{
			MConnection = connection;
			MPeer = connection.MPeer;
		}

		internal abstract void ReceiveMessage(NetIncomingMessage msg);
	}
}
