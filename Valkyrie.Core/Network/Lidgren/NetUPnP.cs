﻿using System;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren
{
	/// <summary>
	/// UPnP support class
	/// </summary>
	public class NetUpnP
	{
		private const int CDiscoveryTimeOutMillis = 1000;

		private string _mServiceUrl;
		private string _mServiceName = "";
		private readonly NetPeer _mPeer;
		private readonly ManualResetEvent _mDiscoveryComplete = new ManualResetEvent(false);

		internal double MDiscoveryResponseDeadline;

		private UpnPStatus _mStatus;

		/// <summary>
		/// Status of the UPnP capabilities of this NetPeer
		/// </summary>
		public UpnPStatus Status { get { return _mStatus; } }

		/// <summary>
		/// NetUPnP constructor
		/// </summary>
		public NetUpnP(NetPeer peer)
		{
			_mPeer = peer;
			MDiscoveryResponseDeadline = double.MinValue;
		}

		internal void Discover(NetPeer peer)
		{
			var str =
"M-SEARCH * HTTP/1.1\r\n" +
"HOST: 239.255.255.250:1900\r\n" +
"ST:upnp:rootdevice\r\n" +
"MAN:\"ssdp:discover\"\r\n" +
"MX:3\r\n\r\n";

			_mStatus = UpnPStatus.Discovering;

			var arr = Encoding.UTF8.GetBytes(str);

			_mPeer.LogDebug("Attempting UPnP discovery");
			peer.Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
			peer.RawSend(arr, 0, arr.Length, new IPEndPoint(IPAddress.Broadcast, 1900));
			peer.Socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, false);

			// allow some extra time for router to respond
			// System.Threading.Thread.Sleep(50);

			MDiscoveryResponseDeadline = NetTime.Now + 6.0; // arbitrarily chosen number, router gets 6 seconds to respond
			_mStatus = UpnPStatus.Discovering;
		}

		internal void ExtractServiceUrl(string resp)
		{
#if !DEBUG
			try
			{
#endif
			var desc = new XmlDocument();
			desc.Load(WebRequest.Create(resp).GetResponse().GetResponseStream());
			var nsMgr = new XmlNamespaceManager(desc.NameTable);
			nsMgr.AddNamespace("tns", "urn:schemas-upnp-org:device-1-0");
			var typen = desc.SelectSingleNode("//tns:device/tns:deviceType/text()", nsMgr);
			if (!typen.Value.Contains("InternetGatewayDevice"))
				return;

			_mServiceName = "WANIPConnection";
			var node = desc.SelectSingleNode("//tns:service[tns:serviceType=\"urn:schemas-upnp-org:service:" + _mServiceName + ":1\"]/tns:controlURL/text()", nsMgr);
			if (node == null)
			{
				//try another service name
				_mServiceName = "WANPPPConnection";
				node = desc.SelectSingleNode("//tns:service[tns:serviceType=\"urn:schemas-upnp-org:service:" + _mServiceName + ":1\"]/tns:controlURL/text()", nsMgr);
				if (node == null)
					return;
			}

			_mServiceUrl = CombineUrls(resp, node.Value);
			_mPeer.LogDebug("UPnP service ready");
			_mStatus = UpnPStatus.Available;
			_mDiscoveryComplete.Set();
#if !DEBUG
			}
			catch
			{
				_mPeer.LogVerbose("Exception ignored trying to parse UPnP XML response");
				return;
			}
#endif
        }

		private static string CombineUrls(string gatewayUrl, string subUrl)
		{
			// Is Control URL an absolute URL?
			if (subUrl.Contains("http:") || subUrl.Contains("."))
				return subUrl;

			gatewayUrl = gatewayUrl.Replace("http://", "");  // strip any protocol
			var n = gatewayUrl.IndexOf("/", StringComparison.Ordinal);
			if (n != -1)
				gatewayUrl = gatewayUrl.Substring(0, n);  // Use first portion of URL
			return "http://" + gatewayUrl + subUrl;
		}

		private bool CheckAvailability()
		{
			switch (_mStatus)
			{
				case UpnPStatus.NotAvailable:
					return false;
				case UpnPStatus.Available:
					return true;
				case UpnPStatus.Discovering:
					if (_mDiscoveryComplete.WaitOne(CDiscoveryTimeOutMillis))
						return true;
					if (NetTime.Now > MDiscoveryResponseDeadline)
						_mStatus = UpnPStatus.NotAvailable;
					return false;
			}
			return false;
		}

		/// <summary>
		/// Add a forwarding rule to the router using UPnP
		/// </summary>
		public bool ForwardPort(int port, string description)
		{
			if (!CheckAvailability())
				return false;

			IPAddress mask;
			var client = NetUtility.GetMyAddress(out mask);
			if (client == null)
				return false;

			try
			{
				SoapRequest(_mServiceUrl,
					"<u:AddPortMapping xmlns:u=\"urn:schemas-upnp-org:service:" + _mServiceName + ":1\">" +
					"<NewRemoteHost></NewRemoteHost>" +
					"<NewExternalPort>" + port + "</NewExternalPort>" +
					"<NewProtocol>" + ProtocolType.Udp.ToString().ToUpper(CultureInfo.InvariantCulture) + "</NewProtocol>" +
					"<NewInternalPort>" + port + "</NewInternalPort>" +
					"<NewInternalClient>" + client + "</NewInternalClient>" +
					"<NewEnabled>1</NewEnabled>" +
					"<NewPortMappingDescription>" + description + "</NewPortMappingDescription>" +
					"<NewLeaseDuration>0</NewLeaseDuration>" +
					"</u:AddPortMapping>",
					"AddPortMapping");

				_mPeer.LogDebug("Sent UPnP port forward request");
				Thread.Sleep(50);
			}
			catch (Exception ex)
			{
				_mPeer.LogWarning("UPnP port forward failed: " + ex.Message);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Delete a forwarding rule from the router using UPnP
		/// </summary>
		public bool DeleteForwardingRule(int port)
		{
			if (!CheckAvailability())
				return false;

			try
			{
				SoapRequest(_mServiceUrl,
				"<u:DeletePortMapping xmlns:u=\"urn:schemas-upnp-org:service:" + _mServiceName + ":1\">" +
				"<NewRemoteHost>" +
				"</NewRemoteHost>" +
				"<NewExternalPort>" + port + "</NewExternalPort>" +
				"<NewProtocol>" + ProtocolType.Udp.ToString().ToUpper(CultureInfo.InvariantCulture) + "</NewProtocol>" +
				"</u:DeletePortMapping>", "DeletePortMapping");
				return true;
			}
			catch (Exception ex)
			{
				_mPeer.LogWarning("UPnP delete forwarding rule failed: " + ex.Message);
				return false;
			}
		}

		/// <summary>
		/// Retrieve the extern ip using UPnP
		/// </summary>
		public IPAddress GetExternalIp()
		{
			if (!CheckAvailability())
				return null;
			try
			{
				var xdoc = SoapRequest(_mServiceUrl, "<u:GetExternalIPAddress xmlns:u=\"urn:schemas-upnp-org:service:" + _mServiceName + ":1\">" +
				"</u:GetExternalIPAddress>", "GetExternalIPAddress");
				var nsMgr = new XmlNamespaceManager(xdoc.NameTable);
				nsMgr.AddNamespace("tns", "urn:schemas-upnp-org:device-1-0");
				var ip = xdoc.SelectSingleNode("//NewExternalIPAddress/text()", nsMgr).Value;
				return IPAddress.Parse(ip);
			}
			catch (Exception ex)
			{
				_mPeer.LogWarning("Failed to get external IP: " + ex.Message);
				return null;
			}
		}

		private XmlDocument SoapRequest(string url, string soap, string function)
		{
			var req = "<?xml version=\"1.0\"?>" +
			"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" s:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">" +
			"<s:Body>" +
			soap +
			"</s:Body>" +
			"</s:Envelope>";
			var r = WebRequest.Create(url);
			r.Method = "POST";
			var b = Encoding.UTF8.GetBytes(req);
			r.Headers.Add("SOAPACTION", "\"urn:schemas-upnp-org:service:" + _mServiceName + ":1#" + function + "\""); 
			r.ContentType = "text/xml; charset=\"utf-8\"";
			r.ContentLength = b.Length;
			r.GetRequestStream().Write(b, 0, b.Length);
			var resp = new XmlDocument();
			var wres = r.GetResponse();
			var ress = wres.GetResponseStream();
			resp.Load(ress);
			return resp;
		}
	}
}