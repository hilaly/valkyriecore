﻿using System;

namespace Valkyrie.Communication.Network.Lidgren
{
    /// <summary>
    /// Mersenne Twister based random
    /// </summary>
    public sealed class MersenneTwisterRandom : NetRandom
    {
        /// <summary>
        /// Get global instance of MersenneTwisterRandom
        /// </summary>
        public new static readonly MersenneTwisterRandom Instance = new MersenneTwisterRandom();

        private const int N = 624;
        private const int M = 397;
        private const uint MatrixA = 0x9908b0dfU;
        private const uint UpperMask = 0x80000000U;
        private const uint LowerMask = 0x7fffffffU;
        private const uint Temper1 = 0x9d2c5680U;
        private const uint Temper2 = 0xefc60000U;
        private const int Temper3 = 11;
        private const int Temper4 = 7;
        private const int Temper5 = 15;
        private const int Temper6 = 18;

        private UInt32[] _mt;
        private int _mti;
        private UInt32[] _mag01;

        private const double CRealUnitInt = 1.0 / (int.MaxValue + 1.0);

        /// <summary>
        /// Constructor with randomized seed
        /// </summary>
        public MersenneTwisterRandom()
        {
            Initialize(NetRandomSeed.GetUInt32());
        }

        /// <summary>
        /// Constructor with provided 32 bit seed
        /// </summary>
        
        public MersenneTwisterRandom(uint seed)
        {
            Initialize(seed);
        }

        /// <summary>
        /// (Re)initialize this instance with provided 32 bit seed
        /// </summary>
        
        public override void Initialize(uint seed)
        {
            _mt = new UInt32[N];
            _mti = N + 1;
            _mag01 = new[] { 0x0U, MatrixA };
            _mt[0] = seed;
            for (var i = 1; i < N; i++)
                _mt[i] = (UInt32)(1812433253 * (_mt[i - 1] ^ (_mt[i - 1] >> 30)) + i);
        }

        /// <summary>
        /// Generates a random value from UInt32.MinValue to UInt32.MaxValue, inclusively
        /// </summary>
        
        public override uint NextUInt32()
        {
            if (_mti >= N)
            {
                GenRandAll();
                _mti = 0;
            }
            var y = _mt[_mti++];
            y ^= y >> Temper3;
            y ^= (y << Temper4) & Temper1;
            y ^= (y << Temper5) & Temper2;
            y ^= y >> Temper6;
            return y;
        }

        private void GenRandAll()
        {
            var kk = 1;
            UInt32 p;
            var y = _mt[0] & UpperMask;
            do
            {
                p = _mt[kk];
                _mt[kk - 1] = _mt[kk + (M - 1)] ^ ((y | (p & LowerMask)) >> 1) ^ _mag01[p & 1];
                y = p & UpperMask;
            } while (++kk < N - M + 1);
            do
            {
                p = _mt[kk];
                _mt[kk - 1] = _mt[kk + (M - N - 1)] ^ ((y | (p & LowerMask)) >> 1) ^ _mag01[p & 1];
                y = p & UpperMask;
            } while (++kk < N);
            p = _mt[0];
            _mt[N - 1] = _mt[M - 1] ^ ((y | (p & LowerMask)) >> 1) ^ _mag01[p & 1];
        }
    }
}