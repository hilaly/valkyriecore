using System.Security.Cryptography;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren.Encryption
{
	public class NetRc2Encryption : NetCryptoProviderBase
	{
		public NetRc2Encryption(NetPeer peer)
			: base(peer, new RC2CryptoServiceProvider())
		{
		}

		public NetRc2Encryption(NetPeer peer, string key)
			: base(peer, new RC2CryptoServiceProvider())
		{
			SetKey(key);
		}

		public NetRc2Encryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new RC2CryptoServiceProvider())
		{
			SetKey(data, offset, count);
		}
	}
}
