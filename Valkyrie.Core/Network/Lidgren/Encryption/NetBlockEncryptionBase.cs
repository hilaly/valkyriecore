﻿using System;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren.Encryption
{
	/// <summary>
	/// Base for a non-threadsafe encryption class
	/// </summary>
	public abstract class NetBlockEncryptionBase : NetEncryption
	{
		// temporary space for one block to avoid reallocating every time
		private readonly byte[] _mTmp;

		/// <summary>
		/// Block size in bytes for this cipher
		/// </summary>
		public abstract int BlockSize { get; }

		/// <summary>
		/// NetBlockEncryptionBase constructor
		/// </summary>
		protected NetBlockEncryptionBase(NetPeer peer)
			: base(peer)
		{
			_mTmp = new byte[BlockSize];
		}

		/// <summary>
		/// Encrypt am outgoing message with this algorithm; no writing can be done to the message after encryption, or message will be corrupted
		/// </summary>
		public override bool Encrypt(NetOutgoingMessage msg)
		{
			var payloadBitLength = msg.LengthBits;
			var numBytes = msg.LengthBytes;
			var blockSize = BlockSize;
			var numBlocks = (int)System.Math.Ceiling(numBytes / (double)blockSize);
			var dstSize = numBlocks * blockSize;

			msg.EnsureBufferSize(dstSize * 8 + 4 * 8); // add 4 bytes for payload length at end
			msg.LengthBits = dstSize * 8; // length will automatically adjust +4 bytes when payload length is written

			for(var i=0;i<numBlocks;i++)
			{
				EncryptBlock(msg.MData, i * blockSize, _mTmp);
				Buffer.BlockCopy(_mTmp, 0, msg.MData, i * blockSize, _mTmp.Length);
			}

			// add true payload length last
			msg.Write((UInt32)payloadBitLength);

			return true;
		}

		/// <summary>
		/// Decrypt an incoming message encrypted with corresponding Encrypt
		/// </summary>
		/// <param name="msg">message to decrypt</param>
		/// <returns>true if successful; false if failed</returns>
		public override bool Decrypt(NetIncomingMessage msg)
		{
			var numEncryptedBytes = msg.LengthBytes - 4; // last 4 bytes is true bit length
			var blockSize = BlockSize;
			var numBlocks = numEncryptedBytes / blockSize;
			if (numBlocks * blockSize != numEncryptedBytes)
				return false;

			for (var i = 0; i < numBlocks; i++)
			{
				DecryptBlock(msg.MData, i * blockSize, _mTmp);
				Buffer.BlockCopy(_mTmp, 0, msg.MData, i * blockSize, _mTmp.Length);
			}

			// read 32 bits of true payload length
			var realSize = NetBitWriter.ReadUInt32(msg.MData, 32, numEncryptedBytes * 8);
			msg.MBitLength = (int)realSize;
			return true;
		}

		/// <summary>
		/// Encrypt a block of bytes
		/// </summary>
		protected abstract void EncryptBlock(byte[] source, int sourceOffset, byte[] destination);

		/// <summary>
		/// Decrypt a block of bytes
		/// </summary>
		protected abstract void DecryptBlock(byte[] source, int sourceOffset, byte[] destination);
	}
}
