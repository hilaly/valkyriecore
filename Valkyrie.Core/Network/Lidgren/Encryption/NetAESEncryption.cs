using System.Security.Cryptography;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren.Encryption
{
	public class NetAesEncryption : NetCryptoProviderBase
	{
		public NetAesEncryption(NetPeer peer)
			: base(peer, new RijndaelManaged())
		{
		}

		public NetAesEncryption(NetPeer peer, string key)
			: base(peer, new RijndaelManaged())
		{
			SetKey(key);
		}

		public NetAesEncryption(NetPeer peer, byte[] data, int offset, int count)
			: base(peer, new RijndaelManaged())
		{
			SetKey(data, offset, count);
		}
	}
}
