﻿using System;
using System.Text;

// ReSharper disable once CheckNamespace
namespace Valkyrie.Communication.Network.Lidgren.Encryption
{
	/// <summary>
	/// Example class; not very good encryption
	/// </summary>
	public class NetXorEncryption : NetEncryption
	{
		private byte[] _mKey;

		/// <summary>
		/// NetXorEncryption constructor
		/// </summary>
		public NetXorEncryption(NetPeer peer, byte[] key)
			: base(peer)
		{
			_mKey = key;
		}

		public override void SetKey(byte[] data, int offset, int count)
		{
			_mKey = new byte[count];
			Array.Copy(data, offset, _mKey, 0, count);
		}

		/// <summary>
		/// NetXorEncryption constructor
		/// </summary>
		public NetXorEncryption(NetPeer peer, string key)
			: base(peer)
		{
			_mKey = Encoding.UTF8.GetBytes(key);
		}

		/// <summary>
		/// Encrypt an outgoing message
		/// </summary>
		public override bool Encrypt(NetOutgoingMessage msg)
		{
			var numBytes = msg.LengthBytes;
			for (var i = 0; i < numBytes; i++)
			{
				var offset = i % _mKey.Length;
				msg.MData[i] = (byte)(msg.MData[i] ^ _mKey[offset]);
			}
			return true;
		}

		/// <summary>
		/// Decrypt an incoming message
		/// </summary>
		public override bool Decrypt(NetIncomingMessage msg)
		{
			var numBytes = msg.LengthBytes;
			for (var i = 0; i < numBytes; i++)
			{
				var offset = i % _mKey.Length;
				msg.MData[i] = (byte)(msg.MData[i] ^ _mKey[offset]);
			}
			return true;
		}
	}
}
