﻿namespace Valkyrie.Communication.Network
{
    public class NetworkMessageInstance<T>
    {
        public NetworkMessageInstance(T message, INetworkChannel channel)
        {
            Message = message;
            Channel = channel;
        }

        public T Message { get; }
        public INetworkChannel Channel { get; }
    }
}