﻿namespace Valkyrie
{
    public static class DefinedConstants
    {
        public const int DefaultMaxIncomingConnections = 32;

        public static class NetworkChannels
        {
            public const int PersistentData = 9;
            
            public const int WorldState = 10;
            public const int WorldSync = 11;
        }

        public static class SyncBuffers
        {
            public const int StoreSize = 30;
            public const int SendSize = 10;
        }

        public static class Scripts
        {
            public const int RegistersCount = 8;
        }
    }
}