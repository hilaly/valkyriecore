﻿namespace Valkyrie.Data
{
    public enum ErrorCodes : int
    {
        Ok = 0,
        PreconditionFailed = -1,
        InvalidArguments = -2,
        NotEnoughResource = -3,
        Unknown = -4,
        LoginRequired = -5,
        InternalError = -6,
        AuthFailed = 1
    }
}