﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Valkyrie.Ai.Planning;
using Valkyrie.Communication.Network;
using Valkyrie.Data.Bind;
using Valkyrie.Data.Config;
using Valkyrie.Data.Json.LitJson;
using Valkyrie.Data.Serialization;
using Valkyrie.Di;
using Valkyrie.Math;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;
using Valkyrie.Threading.Scheduler;
using Valkyrie.Tools.Logs;
using Random = System.Random;

namespace Valkyrie.Data
{
    public static class DataExtensions
    {
        static DataExtensions()
        {
            JsonMapper.RegisterExporter<Vector2>((v, writer) =>
            {
                writer.WriteObjectStart();
                writer.WritePropertyName("X");
                writer.Write(v.X);
                writer.WritePropertyName("Y");
                writer.Write(v.Y);
                writer.WriteObjectEnd();
            });
            JsonMapper.RegisterExporter<Vector3>((v, writer) =>
            {
                writer.WriteObjectStart();
                writer.WritePropertyName("X");
                writer.Write(v.X);
                writer.WritePropertyName("Y");
                writer.Write(v.Y);
                writer.WritePropertyName("Z");
                writer.Write(v.Z);
                writer.WriteObjectEnd();
            });
            JsonMapper.RegisterExporter<Quaternion>((v, writer) =>
            {
                writer.WriteObjectStart();
                writer.WritePropertyName("X");
                writer.Write(v.X);
                writer.WritePropertyName("Y");
                writer.Write(v.Y);
                writer.WritePropertyName("Z");
                writer.Write(v.Z);
                writer.WritePropertyName("W");
                writer.Write(v.W);
                writer.WriteObjectEnd();
            });

            // JsonMapper.RegisterImporter<JsonData, Vector2>(reader =>
            // {
            //     return new Vector2((float) ((IJsonWrapper)reader["x"]).GetDouble(), (float) ((IJsonWrapper)reader["Y"]).GetDouble());
            // });
        }

        #region Naming

        public static string ConvertToCamelCasePropertyName(this string original)
        {
            var sb = new StringBuilder();

            var parts = original.Split(new[] {"_"}, StringSplitOptions.RemoveEmptyEntries);
            foreach (var part in parts)
            {
                sb.Append(part.Substring(0, 1).ToUpperInvariant());
                if (part.Length > 1)
                    sb.Append(part.Substring(1));
            }

            return sb.ToString();
        }

        public static string ConvertToUnityPropertyName(this string original)
        {
            var sb = new StringBuilder();

            var parts = original.Split(new[] {"_"}, StringSplitOptions.RemoveEmptyEntries);
            for (var index = 0; index < parts.Length; index++)
            {
                var part = parts[index];
                sb.Append(index > 0
                    ? part.Substring(0, 1).ToUpperInvariant()
                    : part.Substring(0, 1).ToLowerInvariant());
                if (part.Length > 1)
                    sb.Append(part.Substring(1));
            }

            return sb.ToString();
        }

        public static string ConvertToCamelCaseFieldName(this string original)
        {
            var sb = new StringBuilder("_");

            var parts = original.Split(new[] {"_"}, StringSplitOptions.RemoveEmptyEntries);
            for (var index = 0; index < parts.Length; index++)
            {
                var part = parts[index];
                sb.Append(index > 0
                    ? part.Substring(0, 1).ToUpperInvariant()
                    : part.Substring(0, 1).ToLowerInvariant());
                if (part.Length > 1)
                    sb.Append(part.Substring(1));
            }

            return sb.ToString();
        }

        #endregion

        #region Strings

        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        public static bool NotNullOrEmpty(this string text)
        {
            return !string.IsNullOrEmpty(text);
        }

        public static string Join<T>(this IEnumerable<T> source, string separator)
        {
            return string.Join(separator, source.Select(u => u.ToString()).ToArray());
        }

        public static string Join(this string[] source, string separator)
        {
            return string.Join(separator, source);
        }

        public static Stream ToStream(this string source)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(source));
        }

        public static string ReadAllText(this Stream stream)
        {
            return new StreamReader(stream).ReadToEnd();
        }

        private static HashAlgorithm _hashAlgorithm;

        private static string ToHashString(byte[] data)
        {
            // Create a new StringBuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder(data.Length * 2);

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (var i = 0; i < data.Length; i++)
                sBuilder.Append(BitConverter.ToString(data, i, 1));

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        static HashAlgorithm HashAlgorithm => _hashAlgorithm ?? (_hashAlgorithm = SHA1.Create());

        private static string GetHash(HashAlgorithm hashAlgorithm, string input)
        {
            // Convert the input string
            // to a byte array and compute the hash.
            return GetHash(hashAlgorithm, Encoding.UTF8.GetBytes(input));
        }

        private static string GetHash(HashAlgorithm hashAlgorithm, byte[] input)
        {
            //compute the hash.
            return ToHashString(hashAlgorithm.ComputeHash(input));
        }

        public static string ComputeHash(this string source)
        {
            return source.Length < 20
                ? source
                : GetHash(HashAlgorithm, source);
        }

        public static string ComputeHash(this byte[] source)
        {
            return GetHash(HashAlgorithm, source);
        }

        #endregion

        #region Type

        public static List<Type> GetAllSubTypes(this Type aBaseClass, Func<Type, bool> where)
        {
            var result = new List<Type>
            {
                aBaseClass
            };
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    var assemblyTypes = assembly.GetTypes();
                    var selectedTypes = assemblyTypes
                        .Where(typ => typ.IsSubclassOf(aBaseClass) || aBaseClass.IsAssignableFrom(typ)).ToArray();
                    result.AddRange(selectedTypes);
                }
                catch
                {
                    //Do nothing if we got to assembly that probably not from this project
                }
            }

            return where != null ? result.Where(where).ToList() : result;
        }

        public static T GetCustomAttribute<T>(this ICustomAttributeProvider type, bool inherit) where T : Attribute
        {
            var attrs = type.GetCustomAttributes(typeof(T), inherit);
            if (attrs.Length > 0)
                return (T) attrs[0];
            return default(T);
        }

        #endregion

        #region Network

        #region NewNetwork

        // public static void Send<TEvent>(this INetworkChannel channel, TEvent message)
        // {
        //     Debug.Assert(typeof(TEvent).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
        //         $"{typeof(TEvent)} is not network message");
        //
        //     var netAttribute = typeof(TEvent).GetCustomAttribute<NetworkMessageAttribute>(true);
        //     RpcUtils.SendContractMessage(channel, message, netAttribute.Delivery, netAttribute.Channel);
        // }

        #endregion

        public static IPromise<INetworkChannel> Connect(this INetwork network, ServerPort portDefine)
        {
            return network.Connect(portDefine.Address, portDefine.Port, portDefine.AppId, portDefine.AuthKey);
        }

        public static void Send<TEvent>(this INetworkChannel channel, TEvent message)
        {
            Debug.Assert(typeof(TEvent).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TEvent)} is not network message");

            var type = typeof(TEvent);
            var netAttribute = type.GetCustomAttribute<NetworkMessageAttribute>(true);
            var contractAttribute = type.GetCustomAttribute<ContractAttribute>(false);
            if (netAttribute == null)
                throw new InvalidDataException($"{type.FullName} is not NetworkMessage");
            if (contractAttribute == null)
                throw new InvalidDataException($"{type.FullName} is not Contract");

            RpcUtils.SendContract(channel, message, netAttribute.Delivery, netAttribute.Channel);
        }

        public static IPromise<TResponse> Send<TRequest, TResponse>(this INetworkChannel channel, TRequest request,
            Func<TResponse, bool> response)
        {
            var result = new Promise<TResponse>();

            Debug.Assert(typeof(TResponse).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TResponse)} is not network message");
            IDisposable subscription = null;
            subscription = channel.Subscribe<TResponse>().Subscribe(instance =>
            {
                if (response(instance.Message))
                {
                    subscription.Dispose();
                    result.Resolve(instance.Message);
                }
            });
            channel.Send(request);

            return result;
        }

        public static IPromise<TResponse> Send<TRequest, TResponse>(this INetworkChannel channel, TRequest request)
        {
            return channel.Send<TRequest, TResponse>(request, response => true);
        }

        public static IDisposable Subscribe<TEvent>(this INetworkListener network, Action<TEvent> eventHandler)
        {
            Debug.Assert(typeof(TEvent).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TEvent)} is not network message");
            return network.In<TEvent>().Subscribe(instance => eventHandler(instance.Message));
        }

        public static IDisposable Subscribe<TEvent>(this INetworkListener network,
            Action<TEvent, INetworkChannel> eventHandler)
        {
            Debug.Assert(typeof(TEvent).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TEvent)} is not network message");
            return network.In<TEvent>().Subscribe(instance => eventHandler(instance.Message, instance.Channel));
        }

        public static IDisposable Subscribe<TRequest, TResponse>(this INetworkListener network,
            Action<TRequest, Action<TResponse>> requestHandler)
        {
            Debug.Assert(typeof(TRequest).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TRequest)} is not network message");
            Debug.Assert(typeof(TResponse).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TResponse)} is not network message");

            return network.In<TRequest>().Subscribe(instance =>
                requestHandler(instance.Message, response => instance.Channel.Send(response)));
        }

        public static IDisposable Subscribe<TRequest, TResponse>(this INetworkListener network,
            Func<TRequest, TResponse> requestHandler)
        {
            Debug.Assert(typeof(TRequest).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TRequest)} is not network message");
            Debug.Assert(typeof(TResponse).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TResponse)} is not network message");

            return network.In<TRequest>().Subscribe(instance =>
                instance.Channel.Send(requestHandler(instance.Message)));
        }

        public static IDisposable Subscribe<TEvent>(this INetworkChannel channel, Action<TEvent> eventHandler)
        {
            Debug.Assert(typeof(TEvent).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TEvent)} is not network message");
            return channel.Listener.In<TEvent>().Subscribe(instance => eventHandler(instance.Message));
        }

        public static IObservable<NetworkMessageInstance<TEvent>> Subscribe<TEvent>(this INetworkChannel channel)
        {
            Debug.Assert(typeof(TEvent).GetCustomAttributes(typeof(NetworkMessageAttribute), true).Length > 0,
                $"{typeof(TEvent)} is not network message");
            return channel.Listener.In<TEvent>();
        }

        public static IDisposable SubscribeDisconnect(this INetworkChannel channel,
            Action<INetworkChannel> handleDisconnect)
        {
            return channel.ObserveStatus().Subscribe(channelStatus =>
            {
                if (channelStatus != NetChannelStatus.Connected)
                    handleDisconnect(channel);
            });
        }

        public static IDisposable SubscribeDisconnect(this INetworkChannel channel, Action handleDisconnect)
        {
            return channel.ObserveStatus().Subscribe(channelStatus =>
            {
                if (channelStatus != NetChannelStatus.Connected)
                    handleDisconnect();
            });
        }

        #endregion

        #region Object

        /// <summary>
        /// Return target
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static T CopyFields<T>(this object source, T target)
        {
            return (T) CopyFields(source, (object) target);
        }

        /// <summary>
        /// Returns target
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        static object CopyFields(this object source, object target)
        {
            var sourceType = source.GetType();
            var targetType = target.GetType();

            var sourceFields = DiUtils.GetFields(sourceType,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var targetFields = DiUtils.GetFields(targetType,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

            foreach (var sourceField in sourceFields)
            {
                foreach (var targetField in targetFields)
                {
                    if (targetField.Name != sourceField.Name)
                        continue;

                    targetField.SetValue(target, sourceField.GetValue(source));
                    break;
                }
            }

            return target;
        }

        public static object MakeCopy(this object source)
        {
            var type = source.GetType();
            var result = Activator.CreateInstance(type);
            foreach (var field in DiUtils.GetFields(type,
                BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                field.SetValue(result, field.GetValue(source));
            return result;
        }

        public static T MakeCopy<T>(this T source)
        {
            return (T) MakeCopy((object) source);
        }

        public static bool SetBinding(this object target, string propertyName, Binding binding)
        {
            binding.Target = target;
            binding.TargetPath = propertyName;
            return binding.Update();
        }

        public static T DeserializeValkyrieObject<T>(this string dataString)
        {
            return (T) DataSerializer.DeserializeValue(typeof(T), dataString, new StringInterning());
        }

        public static string SerializeValkyrieObject(object o)
        {
            if (DataSerializer.GetStringValue(o, out var result)) return result;
            throw new Exception($"{o.GetType().FullName} is not supported type for valkyrie object");
        }

        #endregion

        #region Json

        public static string ToJson<T>(this T o)
        {
            return JsonMapper.ToJson(o);
        }

        public static T ToObject<T>(this string json)
        {
            var result = JsonMapper.ToObject<T>(json);
            return result;
        }

        public static JsonData ToJsonData<T>(this T o)
        {
            return o.ToJson().ToJsonData();
        }

        public static object ToObject(this string json, Type targetType)
        {
            return JsonMapper.ToObject(json, targetType);
        }

        public static bool IsValidJson<T>(this string json)
        {
            try
            {
                return ToObject<T>(json) != null;
            }
            catch (JsonException)
            {
                return false;
            }
        }

        public static JsonData ToJsonData(this string json)
        {
            return JsonMapper.ToObject(json);
        }

        #endregion

        #region Xml

        public static string ToXml<T>(this T o)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var targetStream = new MemoryStream())
            {
                serializer.Serialize(targetStream, o);
                targetStream.Flush();
                targetStream.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(targetStream))
                    return reader.ReadToEnd();
            }
        }

        public static T FromXml<T>(this string xml)
        {
            return (T) new XmlSerializer(typeof(T)).Deserialize(new XmlTextReader(xml));
        }

        #endregion

        #region Math

        public static readonly float Deg2Rad = (float) (System.Math.PI / 180);
        public static readonly float Rad2Deg = (float) (180 / System.Math.PI);

        public static int Clamp(this int value, int min, int max)
        {
            if (value < min)
                return min;
            if (value > max)
                return max;
            return value;
        }

        public static float Clamp(this float value, float min, float max)
        {
            if (value < min)
                return min;
            if (value > max)
                return max;
            return value;
        }

        public static float Clamp01(this float value)
        {
            if (value < 0)
                return 0;
            if (value > 1)
                return 1;
            return value;
        }

        public static float RadToDeg(this float value)
        {
            return value * Rad2Deg;
        }

        public static float DegToRad(this float value)
        {
            return value * Deg2Rad;
        }

        public static string ToBigNumberString(this int value)
        {
            return ((long) value).ToBigNumberString();
        }

        public static float Pow(this float value, int pow)
        {
            float temp = 1;
            for (var i = 0; i < pow; ++i)
                temp *= value;
            return temp;
        }

        public static int Pow(this int value, int pow)
        {
            var temp = 1;
            for (var i = 0; i < pow; ++i)
                temp *= value;
            return temp;
        }

        public static long Pow(this long value, int pow)
        {
            long temp = 1;
            for (var i = 0; i < pow; ++i)
                temp *= value;
            return temp;
        }

        public static string ToBigNumberString(this long value)
        {
            var formats = new[]
            {
                "{0:F1}K",
                "{0:F1}M",
                "{0:F1}B",
                "{0:F1}T",
                "{0:F1}q",
                "{0:F1}Q",
                //"{0:F1}s",
                //"{0:F1}S",
                "A lot"
            };

            if (value < 1000)
                return value.ToString();

            for (var i = formats.Length - 1; i >= 0; --i)
            {
                var expr = 1000L.Pow(i + 1);
                if (value >= expr)
                    return string.Format(formats[i], (double) value / expr);
            }

            return "A lot";
        }

        #endregion

        #region Enumerable

        public static bool Chance(this float chance, Random random)
        {
            return random.NextDouble() < chance;
        }

        public static int GetWeighted(this System.Random random, IEnumerable<float> weights)
        {
            var arr = weights.ToArray();
            var weight = random.NextDouble() * arr.Sum();
            for (var i = 0; i < arr.Length; ++i)
            {
                if (weight <= arr[i])
                    return i;
                weight -= arr[i];
            }

            return arr.Length - 1;
        }

        public static T GetWeighted<T>(this IEnumerable<T> source, Func<T, float> weightProvider)
        {
            return source.ToArray()[_random.GetWeighted(source.Select(weightProvider))];
        }

        public static float GetRange(this Random random, float min, float max)
        {
            return (float) (random.NextDouble() * (max - min) + min);
        }

        public static T[] Shuffle<T>(this T[] array, IRandom randomGenerator)
        {
            var n = array.Length;
            while (n > 1)
            {
                var k = randomGenerator.Range(0, n--);
                var temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }

            return array;
        }

        public static T[] Shuffle<T>(this T[] array, int n, IRandom randomGenerator)
        {
            while (n > 1)
            {
                var k = randomGenerator.Range(0, n--);
                var temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }

            return array;
        }

        public static T Random<T>(this IEnumerable<T> collection)
        {
            return collection.ToArray().Random();
        }

        public static T Random<T>(this T[] collection)
        {
            return collection.Random(out var index);
        }

        public static T Random<T>(this List<T> collection)
        {
            return collection.Random(out var index);
        }

        private static Random _random = new Random(DateTime.UtcNow.GetHashCode());

        public static T Random<T>(this T[] collection, out int index)
        {
            if (collection.Length == 0)
            {
                index = -1;
                return default(T);
            }

            index = _random.Next(collection.Length);
            return collection[index];
        }

        public static T Random<T>(this List<T> collection, out int index)
        {
            if (collection.Count == 0)
            {
                index = -1;
                return default(T);
            }

            index = _random.Next(collection.Count);
            return collection[index];
        }

        #endregion

        #region Files

        public static string GetFileHash(string filename)
        {
            if (!File.Exists(filename))
                return null;

            using (var stream = File.OpenRead(filename))
                return ToHashString(HashAlgorithm.ComputeHash(stream));
        }

        public static IDisposable WatchFile(this string path, IContainer scope, Action<string> callback)
        {
            return path.WatchFileChanges((p) =>
                scope.Resolve<Scheduler>().Run(() => callback(p), (int) (_random.NextDouble() * 1000 + 500)));
        }

        public static IDisposable WatchFileChanges(this string path, Action<string> callback)
        {
            if (!Path.IsPathRooted(path))
                path = Path.Combine(Directory.GetCurrentDirectory(), path);

            var f = new FileSystemWatcher
            {
                Path = Path.GetDirectoryName(path),
                Filter = Path.GetFileName(path),
                EnableRaisingEvents = true
            };

            f.Changed += (sender, args) => callback(path);

            if (File.Exists(path))
                callback(path);

            return f;
        }

        #endregion

        #region Embedded

        internal static Stream GetEmbeddedResourceStream(string resourceName)
        {
            var assembly = typeof(DataExtensions).Assembly;
            var resourceFullname = assembly.GetManifestResourceNames().Single(str => str.EndsWith(resourceName));
            return assembly.GetManifestResourceStream(resourceFullname);
        }

        #endregion

        #region AI

        #region Planning

        public static string ToStringDesc(this IPlan plan)
        {
            var sb = new StringBuilder();
            sb.AppendLine(plan.GetType().Name);
            foreach (var planStep in plan.Steps)
                sb.AppendLine(planStep.ToString());
            sb.AppendFormat("; cost = {0} (unit cost)", plan.Cost);
            return sb.ToString();
        }

        #endregion

        #endregion

        #region Time

        public static readonly DateTime UnixEpoch
            = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime ToDateTime(this long date)
        {
            return UnixEpoch.AddSeconds(date);
        }

        public static long ToLongTime(this DateTime date)
        {
            return (long) (date - UnixEpoch).TotalSeconds;
        }

        #endregion

        #region Config

        [Serializable]
        public class VisualAssetConfigData : BaseConfigData
        {
            public string RootPrefab;
            public List<string> Materials;

            public List<string> ChildPrefabs;

            public override bool Validate(ILogInstance logInstance)
            {
                if (RootPrefab.IsNullOrEmpty())
                    logInstance.Error($"{nameof(RootPrefab)} can not be empty");
                return base.Validate(logInstance)
                       && RootPrefab.NotNullOrEmpty();
            }
        }

        class VariableConfigData : BaseConfigData
        {
#pragma warning disable 649
            public string Value;
#pragma warning restore 649
        }

        public static T GetDesignValue<T>(this IConfiguration configuration, string id, T defaultValue)
        {
            var dc = configuration.Get<VariableConfigData>(id);
            if (dc != null && dc.Value.NotNullOrEmpty())
                return dc.Value.DeserializeValkyrieObject<T>();
            return defaultValue;
        }

        public static string GetHash(this IEditableConfiguration serializer, out byte[] data)
        {
            using (var stream = new MemoryStream())
            {
                serializer.Save(stream, ConfigType.Xml);
                stream.Seek(0, SeekOrigin.Begin);
                return ToHashString(HashAlgorithm.ComputeHash(data = stream.ToArray()));
            }
        }

        #endregion

        #region Serialization

        public static byte[] ToByteArray(this IContract o)
        {
            var packer = new SimplePacker();
            o.Serialize(packer);
            return packer.Flush();
        }

        public static T ToObject<T>(this byte[] buffer) where T : IContract, new()
        {
            var packer = new SimplePacker(buffer);
            var result = new T();
            result.Deserialize(packer);
            return result;
        }

        public static void Write(this IBitsPacker bitsPacker, Quaternion vector)
        {
            bitsPacker.Write(vector.X);
            bitsPacker.Write(vector.Y);
            bitsPacker.Write(vector.Z);
            bitsPacker.Write(vector.W);
        }

        public static void Write(this IBitsPacker bitsPacker, Vector3 vector)
        {
            bitsPacker.Write(vector.X);
            bitsPacker.Write(vector.Y);
            bitsPacker.Write(vector.Z);
        }

        public static void WriteAsShort(this IBitsPacker bitsPacker, Vector3 vector)
        {
            bitsPacker.Write((short) (vector.X * 100f));
            bitsPacker.Write((short) (vector.Y * 100f));
            bitsPacker.Write((short) (vector.Z * 100f));
        }

        public static void Write(this IBitsPacker bitsPacker, Vector2 vector)
        {
            bitsPacker.Write(vector.X);
            bitsPacker.Write(vector.Y);
        }

        public static void WriteAsByte(this IBitsPacker bitsPacker, Vector2 vector)
        {
            bitsPacker.Write((byte) ((vector.X + 1f) / 2f * 255));
            bitsPacker.Write((byte) ((vector.Y + 1f) / 2f * 255));
        }

        public static Quaternion ReadQuaternion(this IBitsPacker packer)
        {
            var x = packer.ReadFloat();
            var y = packer.ReadFloat();
            var z = packer.ReadFloat();
            var w = packer.ReadFloat();

            return new Quaternion(x, y, z, w);
        }

        public static void WriteAsShort(this IBitsPacker packer, float value)
        {
            packer.Write((short) (value * 100f));
        }

        public static float ReadFloatAsShort(this IBitsPacker packer)
        {
            return packer.ReadShort() * 0.01f;
        }

        public static Vector3 ReadVector3(this IBitsPacker packer)
        {
            var x = packer.ReadFloat();
            var y = packer.ReadFloat();
            var z = packer.ReadFloat();

            return new Vector3(x, y, z);
        }

        public static Vector3 ReadVector3AsShort(this IBitsPacker packer)
        {
            var x = packer.ReadShort() * 0.01f;
            var y = packer.ReadShort() * 0.01f;
            var z = packer.ReadShort() * 0.01f;

            return new Vector3(x, y, z);
        }

        public static Vector2 ReadVector2(this IBitsPacker packer)
        {
            var x = packer.ReadFloat();
            var y = packer.ReadFloat();

            return new Vector2(x, y);
        }

        public static Vector2 ReadVector2AsByte(this IBitsPacker packer)
        {
            var x = packer.ReadByte() / 255f * 2f - 1f;
            var y = packer.ReadByte() / 255f * 2f - 1f;

            return new Vector2(x, y);
        }

        public static string WriteAsString(this float value)
        {
            return value.ToString(NumberFormatInfo.InvariantInfo);
        }

        public static float ReadFloat(this string value)
        {
            return float.Parse(value, NumberStyles.Float);
        }
        
        #endregion

        #region zip

        public static MemoryStream ZipToMemoryStream(this MemoryStream memStreamIn, string zipEntryName,
            int compressionLevel)
        {
            var outputMemStream = new MemoryStream();
            using (var zipStream = new ZipOutputStream(outputMemStream))
            {
                // 0-9, 9 being the highest level of compression
                zipStream.SetLevel(compressionLevel);

                var newEntry = new ZipEntry(zipEntryName) {DateTime = DateTime.Now};

                zipStream.PutNextEntry(newEntry);

                StreamUtils.Copy(memStreamIn, zipStream, new byte[4096]);
                zipStream.CloseEntry();

                // Stop ZipStream.Dispose() from also Closing the underlying stream.
                zipStream.IsStreamOwner = false;
            }

            outputMemStream.Position = 0;
            return outputMemStream;
        }

        public static MemoryStream ZipToMemoryStream(this byte[] byteIn, string zipEntryName, int compressionLevel)
        {
            var outputMemStream = new MemoryStream();
            using (var zipStream = new ZipOutputStream(outputMemStream))
            {
                // 0-9, 9 being the highest level of compression
                zipStream.SetLevel(compressionLevel);

                var newEntry = new ZipEntry(zipEntryName) {DateTime = DateTime.Now};

                zipStream.PutNextEntry(newEntry);

                zipStream.Write(byteIn, 0, byteIn.Length);
                zipStream.CloseEntry();

                // Stop ZipStream.Dispose() from also Closing the underlying stream.
                zipStream.IsStreamOwner = false;
            }

            outputMemStream.Position = 0;
            return outputMemStream;
        }

        public static byte[] ZipToByteArray(this MemoryStream memStreamIn, string zipEntryName,
            int compressionLevel)
        {
            var outputMemStream = new MemoryStream();
            using (var zipStream = new ZipOutputStream(outputMemStream))
            {
                // 0-9, 9 being the highest level of compression
                zipStream.SetLevel(compressionLevel);

                var newEntry = new ZipEntry(zipEntryName) {DateTime = DateTime.Now};

                zipStream.PutNextEntry(newEntry);

                StreamUtils.Copy(memStreamIn, zipStream, new byte[4096]);
                zipStream.CloseEntry();

                // Stop ZipStream.Dispose() from also Closing the underlying stream.
                zipStream.IsStreamOwner = false;
            }

            // Alternative outputs:
            // ToArray is the cleaner and easiest to use correctly with the penalty of duplicating allocated memory.
            var byteArrayOut = outputMemStream.ToArray();

            return byteArrayOut;

            // GetBuffer returns a raw buffer raw and so you need to account for the true length yourself.
            //byte[] byteArrayOut = outputMemStream.GetBuffer();
            //long len = outputMemStream.Length;
        }

        public static byte[] ZipToByteArray(this byte[] byteIn, string zipEntryName, int compressionLevel)
        {
            var outputMemStream = new MemoryStream();
            using (var zipStream = new ZipOutputStream(outputMemStream))
            {
                // 0-9, 9 being the highest level of compression
                zipStream.SetLevel(compressionLevel);

                var newEntry = new ZipEntry(zipEntryName) {DateTime = DateTime.Now};

                zipStream.PutNextEntry(newEntry);

                zipStream.Write(byteIn, 0, byteIn.Length);
                zipStream.CloseEntry();

                // Stop ZipStream.Dispose() from also Closing the underlying stream.
                zipStream.IsStreamOwner = false;
            }

            // Alternative outputs:
            // ToArray is the cleaner and easiest to use correctly with the penalty of duplicating allocated memory.
            var byteArrayOut = outputMemStream.ToArray();

            return byteArrayOut;

            // GetBuffer returns a raw buffer raw and so you need to account for the true length yourself.
            //byte[] byteArrayOut = outputMemStream.GetBuffer();
            //long len = outputMemStream.Length;
        }

        public static MemoryStream UnzipToMemoryStream(this Stream zipStream, string zipEntryName)
        {
            using var zipInputStream = new ZipInputStream(zipStream);

            while (zipInputStream.GetNextEntry() is ZipEntry zipEntry)
            {
                var entryFileName = zipEntry.Name;
                if (entryFileName != zipEntryName)
                    continue;

                // 4K is optimum
                var buffer = new byte[4096];

                var outStream = new MemoryStream();
                StreamUtils.Copy(zipInputStream, outStream, buffer);
                outStream.Position = 0;
                return outStream;
            }

            return null;
        }

        public static byte[] UnzipToByteArray(this Stream zipStream, string zipEntryName)
        {
            using var zipInputStream = new ZipInputStream(zipStream);

            while (zipInputStream.GetNextEntry() is ZipEntry zipEntry)
            {
                var entryFileName = zipEntry.Name;
                if (entryFileName != zipEntryName)
                    continue;

                // 4K is optimum
                var buffer = new byte[4096];

                var outStream = new MemoryStream();
                StreamUtils.Copy(zipInputStream, outStream, buffer);
                outStream.Position = 0;
                return outStream.ToArray();
            }

            return null;
        }

        public static MemoryStream UnzipToMemoryStream(this byte[] zipArray, string zipEntryName)
        {
            using var zipInputStream = new ZipInputStream(new MemoryStream(zipArray));

            while (zipInputStream.GetNextEntry() is ZipEntry zipEntry)
            {
                var entryFileName = zipEntry.Name;
                if (entryFileName != zipEntryName)
                    continue;

                // 4K is optimum
                var buffer = new byte[4096];

                var outStream = new MemoryStream();
                StreamUtils.Copy(zipInputStream, outStream, buffer);
                outStream.Position = 0;
                return outStream;
            }

            return null;
        }

        public static byte[] UnzipToByteArray(this byte[] zipArray, string zipEntryName)
        {
            using var zipInputStream = new ZipInputStream(new MemoryStream(zipArray));

            while (zipInputStream.GetNextEntry() is ZipEntry zipEntry)
            {
                var entryFileName = zipEntry.Name;
                if (entryFileName != zipEntryName)
                    continue;

                // 4K is optimum
                var buffer = new byte[4096];

                var outStream = new MemoryStream();
                StreamUtils.Copy(zipInputStream, outStream, buffer);
                outStream.Position = 0;
                return outStream.ToArray();
            }

            return null;
        }

        internal const string LidgrenNetworkZipEntryName = "lnzn";

        #endregion
    }
}