using System.IO;

namespace Valkyrie.Data.Config
{
    public interface IEditableConfiguration : IConfiguration
    {
        T Add<T>(T data) where T : BaseConfigData;
        void Remove<T>(T data) where T : BaseConfigData;

        void Clear();
        void Validate();
        
        void Load(Stream stream, ConfigType type);
        void Save(Stream stream, ConfigType type);

        void Fetch();
    }
}