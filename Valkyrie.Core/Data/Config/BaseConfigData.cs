﻿using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Data.Config
{
    /// <summary>
    /// Select nodes 'root/data'
    /// </summary>
    public abstract class BaseConfigData
    {
        public string Id;

        internal static List<Type> GetAllTypes()
        {
            return typeof(BaseConfigData).GetAllSubTypes(typeInfo => !typeInfo.IsAbstract);
        }

        internal string GetRefClass(List<Type> allTypes)
        {
            var name = GetType().Name;
            if (allTypes.Count(u => u.Name == name) == 1)
                return name;
            
            var fullName = GetType().FullName;
            return allTypes.Count(u => u.FullName == fullName) == 1
                ? fullName
                : GetType().AssemblyQualifiedName;
        }

        #region Overrides of Object

        public override string ToString()
        {
            return
                $"{GetRefClass(GetAllTypes())} {this.ToJson()}";
        }

        #endregion

        /// <summary>
        /// Validate datas, true if application can work, false to throw exception
        /// </summary>
        /// <param name="logInstance"></param>
        /// <returns>false is can not work</returns>
        public virtual bool Validate(ILogInstance logInstance)
        {
            if (!Id.IsNullOrEmpty())
                return true;
            
            logInstance.Error($"{nameof(Id)} can not be null or empty");
            return false;
        }

        public virtual void PostParse(ILogInstance log, List<BaseConfigData> datas)
        {
            
        }
    }
}
