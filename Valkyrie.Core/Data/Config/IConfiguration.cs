using System;
using System.Collections.Generic;

namespace Valkyrie.Data.Config
{
    public interface IConfiguration
    {
        T Get<T>(string id) where T : BaseConfigData;
        BaseConfigData Get(Type type, string id);
        IEnumerable<T> Get<T>() where T : BaseConfigData;

        IEditableConfiguration StartEdit();
    }
}