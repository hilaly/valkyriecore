﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;

namespace Valkyrie.Data.Config
{
    class XmlConfigurationDataSerializer : IConfigurationDataSerializer
    {
        #region Implementation of IConfigurationDataSerializer
        
        public void Serialize<T>(Stream stream, IEnumerable<T> datas) where T : BaseConfigData
        {
            var doc = new XmlDocument();

            var rootNode = doc.CreateElement("root");
            doc.AppendChild(rootNode);

            var allTypes = BaseConfigData.GetAllTypes();
            var sorted = new Dictionary<string, List<BaseConfigData>>();
            foreach (var data in datas)
            {
                var key = data.GetRefClass(allTypes);
                if (!sorted.ContainsKey(key))
                    sorted.Add(key, new List<BaseConfigData> {data});
                else
                    sorted[key].Add(data);
            }

            foreach (var pair in sorted)
            {
                var typeName = pair.Key;

                var typeNode = doc.CreateElement(typeName);

                foreach (var data in pair.Value)
                    SerializeConfigData(doc, data, typeNode, false);

                rootNode.AppendChild(typeNode);
            }

//            foreach (var data in datas)
//                SerializeConfigData(doc, data, rootNode, true);

            doc.Save(stream);
        }

        private void SerializeConfigData(XmlDocument doc, BaseConfigData data, XmlElement parentNode, bool saveType)
        {
            var node = doc.CreateElement("data");

            if (saveType)
                Serialize(doc, node, DataSerializer.TypeArgumentName, data.GetRefClass(BaseConfigData.GetAllTypes()));

            var fields =
                data.GetType()
                    .GetFields()
                    .Where(u => u.GetCustomAttributes(typeof(IgnoreAttribute), true).Length == 0);
            var properties =
                data.GetType()
                    .GetProperties()
                    .Where(u => u.GetCustomAttributes(typeof(IgnoreAttribute), true).Length == 0);

            foreach (var fieldInfo in fields)
                Serialize(doc, node, fieldInfo.Name, fieldInfo.GetValue(data));
            foreach (var propertyInfo in properties)
                Serialize(doc, node, propertyInfo.Name, propertyInfo.GetValue(data, null));

            parentNode.AppendChild(node);
        }

        void Serialize(XmlDocument doc, XmlNode node, string name, object value)
        {
            if (value == null || !DataSerializer.GetStringValue(value, out var result))
                return;

            var attribute = doc.CreateAttribute(name);
            attribute.Value = result;
            Debug.Assert(node.Attributes != null, "node.Attributes != null");
            node.Attributes.Append(attribute);
        }
        
        public IEnumerable<DataSerializer.DataEntry> Deserialize(Stream stream)
        {
            var datas = new List<DataSerializer.DataEntry>();

            var doc = new XmlDocument();
            doc.Load(stream);

            var root = doc.SelectSingleNode("root");
            if (root == null)
                return datas;

            foreach (XmlNode node in root.ChildNodes)
            {
                var typeName = node.Name;
                if (typeName != "data")
                    foreach (XmlElement e in node.SelectNodes("data"))
                    {
                        datas.Add(new DataSerializer.DataEntry
                        {
                            TypeName = typeName,
                            Members =
                                e.Attributes.OfType<XmlAttribute>()
                                    .Select(u => new KeyValuePair<string, string>(u.Name, u.Value))
                        });
                    }
                else
                {
                    typeName = ((XmlElement)node).GetAttribute(DataSerializer.TypeArgumentName);
                    datas.Add(new DataSerializer.DataEntry
                    {
                        TypeName = typeName,
                        Members =
                            node.Attributes.OfType<XmlAttribute>().Where(u => u.Name != DataSerializer.TypeArgumentName)
                                .Select(u => new KeyValuePair<string, string>(u.Name, u.Value))
                    });
                }
            }

            return datas;
        }
        
        #endregion
    }
}