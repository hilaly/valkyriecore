namespace Valkyrie.Data.Config
{
    public enum ConfigType
    {
        Xml,
        Json
    };
}