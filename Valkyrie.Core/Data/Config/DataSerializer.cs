using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Data.Config
{
    static class DataSerializer
    {
        internal const string TypeArgumentName = "RefClass";
        
        private static readonly Regex BoolRegex = new Regex("[Tt]rue");
        private static readonly string OverridingKey = "Base";
        private static readonly string IdKey = "Id";
        private static readonly string NullValue = "null";

        public static bool GetStringValue(object value, out string result)
        {
            if (value == null)
            {
                result = NullValue;
                return true;
            }

            var type = value.GetType();
            if (type == typeof(string))
                result = (string) value;
            else if (type == typeof(bool))
                result = (bool) value ? "true" : "false";
            else if (type == typeof(int))
                result = ((int) value).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(long))
                result = ((long) value).ToString(CultureInfo.InvariantCulture);
            else if (type == typeof(float))
                result = ((float) value).WriteAsString();
            else if (type == typeof(Type))
                result = ((Type) value).AssemblyQualifiedName;
            else if (type.IsEnum)
                result = value.ToString();
            else if (type.IsSubclassOf(typeof(BaseConfigData)))
                result = ((BaseConfigData) value).Id;
            else if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                var sb = new StringBuilder();
                foreach (var o in (IEnumerable) value)
                {
                    if (GetStringValue(o, out var v))
                        sb.Append($"{v}; ");
                    else
                    {
                        result = null;
                        return false;
                    }
                }

                if (sb.Length < 2)
                {
                    result = null;
                    return false;
                }

                result = sb.ToString(0, sb.Length - 2);
            }
            else if (type.IsValueType || type == typeof(Vector2) || type == typeof(Vector3))
            {
                result = value.ToJson();
            }
            else
            {
                result = null;
                return false;
            }

            return true;
        }

        public static object DeserializeValue(Type type, string value, IStringInterning stringInterning)
        {
            if (type == typeof(string))
                return stringInterning.Intern(value);
            if (type == typeof(long))
                return long.Parse(value);
            if (type == typeof(int))
                return int.Parse(value);
            if (type == typeof(float))
                return value.ReadFloat();
            if (type == typeof(bool))
                return BoolRegex.IsMatch(value);
            if (type.IsEnum)
                return Enum.Parse(type, value);
            if (type.IsValueType || type == typeof(Vector2) || type == typeof(Vector3))
                return value.ToObject(type);
            
            throw new SerializationException($"{type.Name} isn't supported type for ref class members");
        }

        public static object DeserializeValue(Type type, string value, IConfiguration cfg,
            IStringInterning stringInterning)
        {
            if (typeof(BaseConfigData).IsAssignableFrom(type) || type.IsSubclassOf(typeof(BaseConfigData)))
                return cfg.Get(type, value);

            if (type == typeof(string))
                return stringInterning.Intern(value);

            if (typeof(IEnumerable).IsAssignableFrom(type))
            {
                var values = value == NullValue
                    ? new string[0]
                    : value.Split(new[] {"; ", ";"}, StringSplitOptions.RemoveEmptyEntries);
                var t = type.GetGenericArguments()[0];
                var os = values.Select(s => DeserializeValue(t, s, cfg, stringInterning)).ToList();
                var result = Activator.CreateInstance(type);
                var add = type.GetMethod("Add");
                Debug.Assert(add != null, "add != null");
                foreach (var o in os)
                    add.Invoke(result, new[] {o});
                return result;
            }

            if (type == typeof(int))
                return int.Parse(value);
            if (type == typeof(long))
                return long.Parse(value);
            if (type == typeof(float))
                return float.Parse(value, CultureInfo.InvariantCulture);
            if (type == typeof(bool))
                return BoolRegex.IsMatch(value);
            if (type.IsEnum)
                return Enum.Parse(type, value);
            if (type.IsValueType || type == typeof(Vector2) || type == typeof(Vector3))
                return value.ToObject(type);
            throw new SerializationException($"{type.Name} isn't supported type for ref class members");
        }

        static bool CanSetNow(Type type)
        {
            return type == typeof(string)
                   || type == typeof(int)
                   || type == typeof(long)
                   || type == typeof(float)
                   || type == typeof(bool)
                   || type == typeof(Vector2)
                   || type == typeof(Vector3)
                   || type.IsEnum
                   || type.IsValueType;
        }

        static bool NeedSetReference(Type type)
        {
            var result = typeof(BaseConfigData).IsAssignableFrom(type) || type.IsSubclassOf(typeof(BaseConfigData));
            var isSubclass = typeof(IEnumerable).IsAssignableFrom(type);
            return result || isSubclass;
        }

        static void DeserializeImmediate(object entity, string memberName, string value,
            IStringInterning stringInterning, ILogInstance log, IConfiguration cfg)
        {
            var field = entity.GetType().GetField(memberName);
            if (field != null)
                field.SetValue(entity, DeserializeValue(field.FieldType, value, cfg, stringInterning));
            else
            {
                var property = entity.GetType().GetProperty(memberName);
                if (property != null)
                    property.SetValue(entity, DeserializeValue(property.PropertyType, value, cfg, stringInterning),
                        null);
                else
                    log.Warn($"Can not find field or property with name '{memberName}' at type '{entity.GetType().Name}'");
            }
        }

        static void DeserializeEntry(object entity, string memberName, string value,
            List<Action<IConfiguration>> listToCallAfterPreload, IStringInterning stringInterning, ILogInstance log)
        {
            var field = entity.GetType().GetField(memberName);
            if (field != null)
            {
                if (field.GetCustomAttributes(typeof(IgnoreAttribute), true).Length == 0)
                {
                    if (field.IsInitOnly)
                    {
                        log.Warn($"Can not deserialize field '{field.Name}' for type '{entity.GetType().Name}', this field IsInitOnly");
                    }
                    else
                    {
                        if (CanSetNow(field.FieldType))
                            field.SetValue(entity, DeserializeValue(field.FieldType, value, stringInterning));
                        else if (NeedSetReference(field.FieldType))
                        {
                            listToCallAfterPreload.Add(
                                cfg => field.SetValue(entity,
                                    DeserializeValue(field.FieldType, value, cfg, stringInterning)));
                        }
                        else
                        {
                            log.Warn($"Can not deserialize field '{field.Name}' for type '{entity.GetType().Name}', this value type ({field.FieldType.Name}) isn't supported");
                        }
                    }
                }
            }
            else
            {
                var property = entity.GetType().GetProperty(memberName);
                if (property != null)
                {
                    if (property.GetCustomAttributes(typeof(IgnoreAttribute), true).Length == 0)
                    {
                        if (property.CanWrite)
                        {
                            if (CanSetNow(property.PropertyType))
                            {
                                property.SetValue(entity,
                                    DeserializeValue(property.PropertyType, value, stringInterning),
                                    null);
                            }
                            else if (NeedSetReference(property.PropertyType))
                            {
                                listToCallAfterPreload.Add(
                                    cfg => property.SetValue(entity,
                                        DeserializeValue(property.PropertyType, value, cfg, stringInterning),
                                        null));
                            }
                            else
                            {
                                log.Warn($"Can not deserialize property '{property.Name}' for type '{entity.GetType().Name}', this value type ({property.PropertyType.Name}) isn't supported");
                            }
                        }
                        else
                        {
                            log.Warn($"Can not deserialize property '{property.Name}' for type '{entity.GetType().Name}', this property is readonly");
                        }
                    }
                }
                else
                {
                    log.Warn($"Can not find field or property with name '{memberName}' at type '{entity.GetType().Name}'");
                }
            }
        }

        internal class DataEntry
        {
            public string TypeName;
            public IEnumerable<KeyValuePair<string, string>> Members;
        }

        public static object Deserialize(List<Type> allavailableTypes, DataEntry data,
            List<Action<IConfiguration>> listToCallAfterPreload, IStringInterning stringInterning, ILogInstance log)
        {
            var type = allavailableTypes.FirstOrDefault(u => u.Name == data.TypeName);
            if (type == null)
            {
                type = allavailableTypes.FirstOrDefault(u => u.FullName == data.TypeName);
                if (type == null)
                {
                    type = allavailableTypes.FirstOrDefault(u =>
                        data.TypeName == $"{u.Namespace}.{u.Name}");
                    if (type == null)
                    {
                        log.Warn($"Can not create data instance for refClass '{data.TypeName}'");
                        return null;
                    }
                }
            }

            var result = Activator.CreateInstance(type);
            var baseMember = data.Members.FirstOrDefault(u => u.Key == OverridingKey);
            if (baseMember.Key == OverridingKey)
            {
                var fieldInfo = type.GetField(IdKey);
                if (!fieldInfo.IsInitOnly)
                {
                    fieldInfo.SetValue(result, data.Members.First(m => m.Key == IdKey).Value);
                    listToCallAfterPreload.Add(cfg =>
                    {
                        result = cfg.Get(typeof(BaseConfigData), baseMember.Value).CopyFields(result);
                        foreach (var member in data.Members)
                            DeserializeImmediate(result, member.Key, member.Value, stringInterning, log, cfg);
                    });
                }
            }
            else
                foreach (var argument in data.Members)
                    DeserializeEntry(result, argument.Key, argument.Value, listToCallAfterPreload, stringInterning,
                        log);

            return result;
        }
    }
}