using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Valkyrie.Di;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Data.Config
{
    // ReSharper disable ClassNeverInstantiated.Global
    class Configuration : IEditableConfiguration
        // ReSharper restore ClassNeverInstantiated.Global
    {
        private Dictionary<string, BaseConfigData> _allData;
        private readonly IStringInterning _stringInterning;
        private readonly ILogInstance _log;
        private readonly Configuration _parent;

        [Inject]
        public Configuration(IStringInterning stringInterning, ILogService logService)
        {
            _log = logService.GetLog("Valkyrie.Config");
            _stringInterning = stringInterning;
            _allData = new Dictionary<string, BaseConfigData>();
        }

        private Configuration(IStringInterning stringInterning, ILogInstance logInstance, Configuration configuration)
        {
            _log = logInstance;
            _stringInterning = stringInterning;
            _parent = configuration;
            _allData = new Dictionary<string, BaseConfigData>();

            using var ms = new MemoryStream();
            _parent.Save(ms, ConfigType.Xml);
            ms.Seek(0, SeekOrigin.Begin);
            Load(ms, ConfigType.Xml);
        }

        static IConfigurationDataSerializer GetSerializer(ConfigType type)
        {
            switch (type)
            {
                case ConfigType.Xml:
                    return new XmlConfigurationDataSerializer();
                case ConfigType.Json:
                    throw new NotImplementedException();
/*
                    return new JsonConfigurationDataSerializer();
*/
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        void Process()
        {
            var logger = new DataLogger(_log);
            var datas = Get<BaseConfigData>().ToList();
            foreach (var data in datas)
            {
                try
                {
                    data.PostParse(logger, datas);
                }
                catch (Exception e)
                {
                    logger.Error($"{data.Id} is not valid: {e}");
                }
            }
        }

        public void Validate()
        {
            var logger = new DataLogger(_log);
            var valid = true;
            foreach (var data in Get<BaseConfigData>())
            {
                logger.Data = data;
                var current = data.Validate(logger);
                if(!current)
                    logger.Warn($"{data.Id} is not valid");
                valid = valid && current;
            }

            if (!valid)
                throw new Exception("ConfigData is not valid");
        }

        public void Fetch()
        {
            if (_parent != null)
                lock (_allData)
                {
                    Process();
                    Validate();
                    
                    lock (_parent._allData)
                    {
                        _parent._allData = _allData;
                    }
                }
        }

        #region Implementation of IConfiguration

        public BaseConfigData Get(Type type, string id)
        {
            lock (_allData)
                return _allData.TryGetValue(id, out var result) ? result : null;
        }

        public T Get<T>(string id) where T : BaseConfigData
        {
            return (T) Get(typeof(T), id);
        }

        public IEnumerable<T> Get<T>() where T : BaseConfigData
        {
            lock (_allData)
                return _allData.Values.OfType<T>();
        }
        
        public IEditableConfiguration StartEdit()
        {
            return new Configuration(_stringInterning, _log, this);
        }

        #endregion
        
        #region IEditableConfiguration
        
        public T Add<T>(T data) where T : BaseConfigData
        {
            lock (_allData)
                _allData.Add(data.Id, data);
            return data;
        }

        public void Remove<T>(T data) where T : BaseConfigData
        {
            lock (_allData)
                if (!_allData.Remove(data.Id))
                    return;

            lock(_allData)
                foreach (var value in _allData.Values)
                {
                    foreach (var fieldInfo in value.GetType()
                        .GetFields(BindingFlags.Instance | BindingFlags.GetField | BindingFlags.SetField)
                        .Where(u => u.GetValue(value) == value))
                        fieldInfo.SetValue(value, null);
                    foreach (var propertyInfo in value.GetType()
                        .GetProperties(BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.SetProperty)
                        .Where(u => u.GetValue(value, null) == data))
                        propertyInfo.SetValue(value, null, null);
                }
        }

        public void Clear()
        {
            lock (_allData) _allData.Clear();
        }

        #endregion

        #region IConfigurationLoader

        public void Load(Stream stream, ConfigType type)
        {
            var serializer = GetSerializer(type);
            var callAfterPreload = new List<Action<IConfiguration>>();

            var allTypes = typeof(BaseConfigData).GetAllSubTypes(typeInfo => !typeInfo.IsAbstract);
            var deserializedData = serializer.Deserialize(stream)
                .Select(entry => DataSerializer.Deserialize(allTypes, entry, callAfterPreload, _stringInterning, _log))
                .OfType<BaseConfigData>();

            lock (_allData)
            {
                foreach (var data in deserializedData)
                    _allData.Add(data.Id, data);
            }

            foreach (var action in callAfterPreload)
                action(this);

            Process();
            Validate();
        }

        public void Save(Stream stream, ConfigType type)
        {
            Validate();

            lock (_allData)
            {
                GetSerializer(type).Serialize(stream, _allData.Values);
            }
        }

        #endregion
        
        class DataLogger : ILogInstance
        {
            private readonly ILogInstance _logInstanceImplementation;
            public BaseConfigData Data { private get; set; }

            public DataLogger(ILogInstance logInstanceImplementation)
            {
                _logInstanceImplementation = logInstanceImplementation;
            }

            public void Info(string message)
            {
                _logInstanceImplementation.Info($"{Data}, {message}");
            }

            public void Debug(string message)
            {
                _logInstanceImplementation.Debug($"{Data}, {message}");
            }

            public void Warn(string message)
            {
                _logInstanceImplementation.Warn($"{Data}, {message}");
            }

            public void Error(string message)
            {
                _logInstanceImplementation.Error($"{Data}, {message}");
            }
        }
    }
}