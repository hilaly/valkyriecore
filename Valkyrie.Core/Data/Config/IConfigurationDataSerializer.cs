﻿using System.Collections.Generic;
using System.IO;

namespace Valkyrie.Data.Config
{
    interface IConfigurationDataSerializer
    {
        void Serialize<T>(Stream stream, IEnumerable<T> datas) where T : BaseConfigData;
        IEnumerable<DataSerializer.DataEntry> Deserialize(Stream stream);
    }
}