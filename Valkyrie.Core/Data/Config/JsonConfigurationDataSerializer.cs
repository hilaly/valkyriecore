﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Valkyrie.Data.Config
{
    class JsonConfigurationDataSerializer : IConfigurationDataSerializer
    {
        #region Implementation of IConfigurationDataSerializer

        public void Serialize<T>(Stream stream, IEnumerable<T> datas) where T : BaseConfigData
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DataSerializer.DataEntry> Deserialize(Stream stream)
        {
            string jsonText;
            using (var reader = new StreamReader(stream))
                jsonText = reader.ReadToEnd();

            throw new NotImplementedException();
        }

        #endregion
    }
}