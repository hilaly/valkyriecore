using System;

namespace Valkyrie.Data.Config
{
    [Serializable]
    public class SerializedKeyValuePair<TKey, TValue>
    {
        public TKey Key;
        public TValue Value;
    }
}