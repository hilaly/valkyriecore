namespace Valkyrie.Data.Json.LitJson
{
    delegate void ExporterFunc    (object obj, JsonWriter writer);
}