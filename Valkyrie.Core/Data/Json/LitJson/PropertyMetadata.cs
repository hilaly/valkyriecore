using System;
using System.Reflection;

namespace Valkyrie.Data.Json.LitJson
{
    struct PropertyMetadata
    {
        public MemberInfo Info;
        public bool       IsField;
        public Type       Type;
    }
}