namespace Valkyrie.Data.Json.LitJson
{
    class FsmContext
    {
        public bool  Return;
        public int   NextState;
        public Lexer L;
        public int   StateStack;
    }
}