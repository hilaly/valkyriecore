namespace Valkyrie.Data.Json.LitJson
{
    class WriterContext
    {
        public int  Count;
        public bool InArray;
        public bool InObject;
        public bool ExpectingValue;
        public int  Padding;
    }
}