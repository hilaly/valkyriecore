namespace Valkyrie.Data.Json.LitJson
{
    enum Condition
    {
        InArray,
        InObject,
        NotAProperty,
        Property,
        Value
    }
}