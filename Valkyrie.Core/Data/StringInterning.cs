using System.Collections.Generic;

namespace Valkyrie.Data
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal class StringInterning : IStringInterning
    {
        private readonly Dictionary<string, string> _internedStrings = new Dictionary<string, string>();

        public StringInterning()
        {
            Intern(string.Empty);
        }

        public string Intern(string value)
        {
            var result = value;
            if (result != null)
                lock (_internedStrings)
                    if (!_internedStrings.TryGetValue(value, out result))
                    {
                        result = value;
                        _internedStrings.Add(result, result);
                    }
            return result;
        }
    }
}