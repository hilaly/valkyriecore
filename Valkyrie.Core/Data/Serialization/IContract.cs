namespace Valkyrie.Data.Serialization
{
    public interface IContract
    {
        void Serialize(IBitsPacker packer);
        void Deserialize(IBitsPacker packer);
    }
}