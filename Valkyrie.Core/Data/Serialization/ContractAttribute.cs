using System;

namespace Valkyrie.Data.Serialization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false, AllowMultiple = false)]
    public class ContractAttribute : Attribute
    {
        /// <summary>
        /// Unique identifier of this contract, can be used when class renamed
        /// </summary>
        public string Tag { get; set; }
        /// <summary>
        /// Reserved tags
        /// </summary>
        public byte[] Reserved { get; private set; }

        public ContractAttribute(params byte[] reserved)
        {
            Reserved = reserved;
        }
    }
}