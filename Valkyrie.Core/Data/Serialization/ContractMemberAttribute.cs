using System;

namespace Valkyrie.Data.Serialization
{
    public class ByteFloatConverter
    {
        private readonly float _minValue;
        private readonly float _maxValue;

        private readonly float _diffValue;
        private const float ShortRange = byte.MaxValue - byte.MinValue;

        public ByteFloatConverter( float minValue, float maxValue )
        {
            _minValue = minValue;
            _maxValue = maxValue;
            _diffValue = maxValue - _minValue;
        }

        public float Unpack( byte value )
        {
            return _minValue + _diffValue * ( (value - byte.MinValue) / ShortRange );
        }

        public byte Pack( float value )
        {
            if ( value <= _minValue )
                return byte.MinValue;

            if ( value >= _maxValue )
                return byte.MaxValue;
            
            var result = byte.MinValue;
            result += (byte)( ShortRange * ( ( value - _minValue ) / _diffValue ) );

            return result;
        }
    }
    public class ShortFloatConverter
    {
        private readonly float _minValue;
        private readonly float _maxValue;

        private readonly float _diffValue;
        private const float ShortRange = short.MaxValue - short.MinValue;

        public ShortFloatConverter( float minValue, float maxValue )
        {
            _minValue = minValue;
            _maxValue = maxValue;
            _diffValue = maxValue - _minValue;
        }

        public float Unpack( short value )
        {
            return _minValue + _diffValue * ( (value - short.MinValue) / ShortRange );
        }

        public short Pack( float value )
        {
            if ( value <= _minValue )
                return short.MinValue;

            if ( value >= _maxValue )
                return short.MaxValue;
            
            var result = short.MinValue;
            result += (short)( ShortRange * ( ( value - _minValue ) / _diffValue ) );

            return result;
        }
    }
    public enum CompessedType
    {
        None = 0,
        Byte,
        Short,
    }
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class CompressedFloatAttribute : Attribute
    {
        public float MinValue { get; set; } = float.MinValue;
        public float MaxValue { get; set; } = float.MaxValue;
        
        public CompessedType Compression { get; }

        public CompressedFloatAttribute(CompessedType compression)
        {
            Compression = compression;
        }
    }
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class ContractMemberAttribute : Attribute
    {
        /// <summary>
        /// Tag to serialize/deserialize must be unique in one contract
        /// </summary>
        public byte Tag { get; }

        public ContractMemberAttribute(byte tag)
        {
            Tag = tag;
        }
    }
}