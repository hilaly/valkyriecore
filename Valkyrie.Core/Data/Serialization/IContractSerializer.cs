using System;

namespace Valkyrie.Data.Serialization
{
    public interface IContractSerializer
    {
        Type GetDataContractType(byte[] b);
        byte[] Serialize(object o, bool writeType);
        object Deserialize(byte[] b);
        object Deserialize(byte[] b, object o);
        T Deserialize<T>(byte[] b);
        object Deserialize(byte[] b, Type type, object o);

        T Deserialize<T>(IBitsPacker packer);
        object Deserialize(IBitsPacker packer);
    }
}