using System;
using System.Text;
using Valkyrie.Di;

namespace Valkyrie.Data.Serialization
{
    [Contract]
    public class SimplePacker : IBitsPacker, IContract
    {
        private byte[] _buffer;
        private int _position;

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(_buffer);
            packer.Write(_position);
        }

        public void Deserialize(IBitsPacker packer)
        {
            _buffer = packer.ReadBytes();
            _position = packer.ReadInt();
        }

        [Inject]
        public SimplePacker()
        {
            _buffer = new byte[64];
        }
        public SimplePacker(int size)
        {
            _buffer = new byte[size];
        }
        public SimplePacker(byte[] buffer)
        {
            _buffer = buffer;
        }

        #region IbitPacker

        public int Size => _buffer.Length;
        public int Position => _position;

        public void Reset()
        {
            _position = 0;
        }

        public void Resize(int size)
        {
            if (_buffer.Length >= size)
                return;

            var temp = new byte[size];
            Buffer.BlockCopy(_buffer, 0, temp, 0, _buffer.Length);
            _buffer = temp;
        }

        public void Load(byte[] buffer)
        {
            if (buffer.Length > _buffer.Length)
                Resize(buffer.Length);
            Buffer.BlockCopy(buffer, 0, _buffer, 0, buffer.Length);
            _position = 0;
        }

        public byte[] Flush()
        {
            var result = new byte[_position];
            Array.Copy(_buffer, result, _position);
            return result;
        }

        void EnsureSize(int size)
        {
            if (size > Size)
            {
                var ns = Size * 2;
                if (size > ns)
                    ns = size;
                Resize(ns * 2);
            }
        }

        #endregion
        
        #region Read/Write

        public void Write(byte value)
        {
            EnsureSize(_position + 1);
            _buffer[_position] = value;
            _position += 1;
        }

        public void Write(short value)
        {
            var temp = BitConverter.GetBytes(value);
            EnsureSize(_position + temp.Length);
            Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
            _position += temp.Length;
        }

        public void Write(ushort value)
        {
            var temp = BitConverter.GetBytes(value);
            EnsureSize(_position + temp.Length);
            Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
            _position += temp.Length;
        }

        public void Write(int value)
        {
            var temp = BitConverter.GetBytes(value);
            EnsureSize(_position + temp.Length);
            Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
            _position += temp.Length;
        }

        public void Write(long value)
        {
            var temp = BitConverter.GetBytes(value);
            EnsureSize(_position + temp.Length);
            Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
            _position += temp.Length;
        }

        public void Write(string value)
        {
            if(value == null)
                Write((int)-1);
            else
            {
                var temp = Encoding.UTF8.GetBytes(value);
                Write(temp.Length);
                EnsureSize(_position + temp.Length);
                Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
                _position += temp.Length;
            }
        }

        public void Write(bool value)
        {
            EnsureSize(_position + 1);
            _buffer[_position] = (byte) (value ? 1 : 0);
            _position += 1;
        }

        public void Write(float value)
        {
            var temp = BitConverter.GetBytes(value);
            EnsureSize(_position + temp.Length);
            Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
            _position += temp.Length;
        }

        public void Write(byte[] temp)
        {
            if(temp == null)
                Write((int)-1);
            else
            {
                Write(temp.Length);
                EnsureSize(_position + temp.Length);
                Buffer.BlockCopy(temp, 0, _buffer, _position, temp.Length);
                _position += temp.Length;
            }
        }

        public byte ReadByte()
        {
            var result = _buffer[_position];
            _position += 1;
            return result;
        }

        public short ReadShort()
        {
            var result = BitConverter.ToInt16(_buffer, _position);
            _position += sizeof(short);
            return result;
        }

        public ushort ReadUShort()
        {
            var result = BitConverter.ToUInt16(_buffer, _position);
            _position += sizeof(ushort);
            return result;
        }

        public int ReadInt()
        {
            var result = BitConverter.ToInt32(_buffer, _position);
            _position += sizeof(int);
            return result;
        }

        public long ReadLong()
        {
            var result = BitConverter.ToInt64(_buffer, _position);
            _position += sizeof(long);
            return result;
        }

        public string ReadString()
        {
            var size = ReadInt();
            if (size < 0)
                return null;
            else
            {
                var result = Encoding.UTF8.GetString(_buffer, _position, size);
                _position += size;
                return result;
            }
        }

        public bool ReadBool()
        {
            return ReadByte() != 0;
        }

        public float ReadFloat()
        {
            var result = BitConverter.ToSingle(_buffer, _position);
            _position += sizeof(float);
            return result;
        }

        public byte[] ReadBytes()
        {
            var count = ReadInt();
            return count < 0 ? null : ReadBytes(count);
        }

        public byte[] ReadBytes(int size)
        {
            var result = new byte[size];
            Buffer.BlockCopy(_buffer, _position, result, 0, size);
            _position += size;
            return result;
        }

        #endregion
    }
}