namespace Valkyrie.Data.Serialization
{
    public interface IContractProcessor
    {
        void PrepareSerialization();
        void PostDeserialization();
    }
}