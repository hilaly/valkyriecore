using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using Debug = System.Diagnostics.Debug;

namespace Valkyrie.Data.Serialization
{
    public class ContractSerializer : IContractSerializer
    {
        readonly IBitsPacker _bitPacker = new SimplePacker(64);

        private static readonly Dictionary<string, ITypeSerializer> DataContractSerializers =
            new Dictionary<string, ITypeSerializer>();
        
        private static readonly Dictionary<Type, string> _tagsMap = new Dictionary<Type, string>();

        static string GetTag(Type type)
        {
            string tag;
            lock (_tagsMap)
                if (_tagsMap.TryGetValue(type, out tag))
                    return tag;

            var contract = type.GetCustomAttribute<ContractAttribute>(false);
            if (contract == null)
                throw new Exception($"Type {type.Name} is not data contract");
            tag = contract.Tag ?? type.Name;
            lock(_tagsMap)
                _tagsMap.Add(type, tag);
            return tag;
        }

        public Type GetDataContractType(byte[] b)
        {
            lock (_bitPacker)
            {
                _bitPacker.Load(b);
                var tag = _bitPacker.ReadString();
                if (tag == "null")
                    return null;

                var serializer = GetSerializer(tag);
                return serializer.Type;
            }
        }

        public byte[] Serialize(object o, bool writeType)
        {
            if (o == null)
                lock (_bitPacker)
                {
                    _bitPacker.Reset();
                    _bitPacker.Write("null");
                    return _bitPacker.Flush();
                }
            else
            {
                var tag = GetTag(o.GetType());
                var serializer = GetSerializer(tag);

                lock (_bitPacker)
                {
                    _bitPacker.Reset();
                    if(writeType)
                        _bitPacker.Write(tag);
                    serializer.Serialize(o, _bitPacker);
                    return _bitPacker.Flush();
                }
            }
        }

        static void Serialize(IBitsPacker writer, object o)
        {
            if (o == null)
                writer.Write("null");
            else
            {
                var tag = GetTag(o.GetType());
                var serializer = GetSerializer(tag);

                writer.Write(tag);
                serializer.Serialize(o, writer);
            }
        }

        private static object Deserialize(IBitsPacker reader)
        {
            var tag = reader.ReadString();
            if (tag == "null")
                return null;

            var serializer = GetSerializer(tag);
            return serializer.Deserialize(reader);
        }

        static object Deserialize(IBitsPacker reader, object result)
        {
            var tag = reader.ReadString();
            if (tag == "null")
                return null;

            var serializer = GetSerializer(tag);
            return serializer.Deserialize(reader, result);
        }

        public object Deserialize(byte[] b)
        {
            lock (_bitPacker)
            {
                _bitPacker.Load(b);
                return Deserialize(_bitPacker);
            }
        }

        object IContractSerializer.Deserialize(IBitsPacker packer)
        {
            return Deserialize(packer);
        }

        public T Deserialize<T>(IBitsPacker packer)
        {
            var tag = GetTag(typeof(T));
            var serializer = GetSerializer(tag);
            var result = Activator.CreateInstance(typeof(T));
            return (T) serializer.Deserialize(packer, result);
        }

        public object Deserialize(byte[] b, object o)
        {
            lock (_bitPacker)
            {
                _bitPacker.Load(b);
                return Deserialize(_bitPacker, o);
            }
        }

        public T Deserialize<T>(byte[] b)
        {
            return (T) Deserialize(b, typeof(T), null);
            var tag = GetTag(typeof(T));
            var serializer = GetSerializer(tag);
            var result = Activator.CreateInstance<T>();
            lock (_bitPacker)
            {
                _bitPacker.Load(b);
                return (T) serializer.Deserialize(_bitPacker, result);
            }
        }

        public object Deserialize(byte[] b, Type type, object o)
        {
            var tag = GetTag(type);
            var serializer = GetSerializer(tag);
            var result = o ?? Activator.CreateInstance(type);
            lock (_bitPacker)
            {
                _bitPacker.Load(b);
                return serializer.Deserialize(_bitPacker, result);
            }
        }

        static ITypeSerializer GetSerializer(string tag)
        {
            if (!DataContractSerializers.TryGetValue(tag, out var result))
                result = CreateTypeSerializer(tag);
            result.Build();
            return result;
        }

        private static ITypeSerializer CreateTypeSerializer(string tag)
        {
            var types = typeof(object).GetAllSubTypes(u =>
                !u.IsAbstract && u.GetCustomAttribute<ContractAttribute>(false) != null);

            ITypeSerializer result = null;
            foreach (var type in types)
            {
                var typeTag = GetTag(type);
                if (!DataContractSerializers.ContainsKey(typeTag))
                    DataContractSerializers.Add(typeTag, new TypeSerializer(type));

                if (typeTag == tag)
                    result = DataContractSerializers[typeTag];
            }

            foreach (var pair in DataContractSerializers)
                pair.Value.Build();

            return result;
        }

        class TypeSerializer : ITypeSerializer
        {
            private bool _isBuild;

            private Func<IBitsPacker, object> _deserializeAction;
            private Func<IBitsPacker, object, object> _writeAction;
            private Action<IBitsPacker, object> _serializeAction;

            private const float Epsilon = 0.000001f;

            public TypeSerializer(Type type)
            {
                Type = type;
            }

            object ITypeSerializer.Deserialize(IBitsPacker reader)
            {
                return _deserializeAction(reader);
            }

            object ITypeSerializer.Deserialize(IBitsPacker reader, object o)
            {
                return _writeAction(reader, o);
            }

            public void Serialize(object o, IBitsPacker writer)
            {
                _serializeAction(writer, o);
            }

            class MemberSerializationData
            {
                public byte Tag;

                public Func<object, bool> IsExist;
                public Action<IBitsPacker, object> FullWrite;
                public Action<IBitsPacker, object> FullRead;
                public Action<object> WriteDefault;
            }

            MemberSerializationData CreateMemberSerializationData(byte tempTag, Func<object, object> getMethod,
                Action<object, object> setMethod, Type memberType, MemberInfo memberInfo)
            {
                var nm = new MemberSerializationData
                {
                    Tag = tempTag
                };

                if (memberType == typeof(byte))
                {
                    nm.IsExist = o => 0 != (byte) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (byte) getMethod(o);
                        if (value == 0) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadByte());
                    nm.WriteDefault = o => setMethod(o, (byte) 0);
                }
                else if (memberType == typeof(short))
                {
                    nm.IsExist = o => 0 != (short) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (short) getMethod(o);
                        if (value == 0) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadShort());
                    nm.WriteDefault = o => setMethod(o, (short) 0);
                }
                else if (memberType == typeof(ushort))
                {
                    nm.IsExist = o => 0 != (ushort) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (ushort) getMethod(o);
                        if (value == 0) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadUShort());
                    nm.WriteDefault = o => setMethod(o, (ushort) 0);
                }
                else if (memberType == typeof(bool))
                {
                    nm.IsExist = o => (bool) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        if (!(bool) getMethod(o))
                            return;
                        writer.Write(tempTag);
                        writer.Write(true);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadBool());
                    nm.WriteDefault = o => setMethod(o, false);
                }
                else if (memberType == typeof(int))
                {
                    nm.IsExist = o => 0 != (int) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (int) getMethod(o);
                        if (value == 0) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadInt());
                    nm.WriteDefault = o => setMethod(o, 0);
                }
                else if (memberType == typeof(long))
                {
                    nm.IsExist = o => 0 != (long) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (long) getMethod(o);
                        if (value == 0) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadLong());
                    nm.WriteDefault = o => setMethod(o, (long) 0);
                }
                else if (memberType == typeof(Vector2))
                {
                    nm.IsExist = o => Vector2.Zero != (Vector2) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (Vector2) getMethod(o);
                        if (value == Vector2.Zero) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadVector2());
                    nm.WriteDefault = o => setMethod(o, Vector2.Zero);
                }
                else if (memberType == typeof(Vector3))
                {
                    nm.IsExist = o => Vector3.Zero != (Vector3) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (Vector3) getMethod(o);
                        if (value == Vector3.Zero) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadVector3());
                    nm.WriteDefault = o => setMethod(o, Vector3.Zero);
                }
                else if (memberType == typeof(Quaternion))
                {
                    nm.IsExist = o => Quaternion.Identity != (Quaternion) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (Quaternion) getMethod(o);
                        if (value == Quaternion.Identity) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadQuaternion());
                    nm.WriteDefault = o => setMethod(o, Quaternion.Identity);
                }
                else if (memberType == typeof(float))
                {
                    var compressedAttribute = memberInfo.GetCustomAttribute<CompressedFloatAttribute>(false);
                    nm.IsExist = o => System.Math.Abs((float) getMethod(o)) > Epsilon;
                    if (compressedAttribute == null || compressedAttribute.Compression == CompessedType.None)
                    {
                        nm.FullWrite = (writer, o) =>
                        {
                            var value = (float) getMethod(o);
                            if (System.Math.Abs(value) < Epsilon) return;
                            writer.Write(tempTag);
                            writer.Write(value);
                        };
                        nm.FullRead = (reader, o) => setMethod(o, reader.ReadFloat());
                    }
                    else
                    {
                        switch (compressedAttribute.Compression)
                        {
                            case CompessedType.Byte:
                            {
                                var converter = new ByteFloatConverter(compressedAttribute.MinValue,
                                    compressedAttribute.MaxValue);
                                nm.FullWrite = (writer, o) =>
                                {
                                    var value = (float) getMethod(o);
                                    if (System.Math.Abs(value) < Epsilon) return;
                                    writer.Write(tempTag);
                                    writer.Write(converter.Pack(value));
                                };
                                nm.FullRead = (reader, o) => setMethod(o, converter.Unpack(reader.ReadByte()));
                                break;
                            }
                            case CompessedType.Short:
                            {
                                var converter = new ShortFloatConverter(compressedAttribute.MinValue,
                                    compressedAttribute.MaxValue);
                                nm.FullWrite = (writer, o) =>
                                {
                                    var value = (float) getMethod(o);
                                    if (System.Math.Abs(value) < Epsilon) return;
                                    writer.Write(tempTag);
                                    writer.Write(converter.Pack(value));
                                };
                                nm.FullRead = (reader, o) => setMethod(o, converter.Unpack(reader.ReadShort()));
                                break;
                            }
                            default:
                                throw new Exception(
                                    $"Compression method for {compressedAttribute.Compression} is not implemented");
                        }
                    }

                    nm.WriteDefault = o => setMethod(o, 0f);
                }
                else if (memberType == typeof(string))
                {
                    nm.IsExist = o => null != (string) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (string) getMethod(o);
                        if (value == null) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadString());
                    nm.WriteDefault = o => setMethod(o, null);
                }
                else if (memberType.IsEnum)
                {
                    nm.IsExist = o => 0 != (byte) getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (byte) getMethod(o);
                        if (value == 0) return;
                        writer.Write(tempTag);
                        writer.Write(value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, reader.ReadByte());
                    nm.WriteDefault = o => setMethod(o, (byte) 0);
                }
                else if (memberType.GetCustomAttribute<ContractAttribute>(false) != null)
                {
                    nm.IsExist = o => null != getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = getMethod(o);
                        if (value == null) return;
                        writer.Write(tempTag);
                        ContractSerializer.Serialize(writer, value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, ContractSerializer.Deserialize(reader));
                    nm.WriteDefault = o => setMethod(o, null);
                }
                else if (memberType.IsArray)
                {
                    var innerType = memberType.GetElementType();
                    Debug.Assert(innerType != null, "innerType != null");

                    GetDirectMethods(innerType, out var serialize, out var deserialize, memberInfo);

                    nm.IsExist = o => null != getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (Array) getMethod(o);
                        if (value == null) return;
                        writer.Write(tempTag);
                        writer.Write(value.Length);
                        for (var i = 0; i < value.Length; ++i)
                            serialize(writer, value.GetValue(i));
                    };
                    nm.FullRead = (reader, o) =>
                    {
                        var temp = reader.ReadInt();
                        var value = Array.CreateInstance(innerType, temp);
                        for (var i = 0; i < temp; ++i)
                            value.SetValue(deserialize(reader), i);
                        setMethod(o, value);
                    };
                    nm.WriteDefault = o => setMethod(o, null);
                }
                else if (memberType.IsGenericType && memberType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    GetDirectMethods(memberType.GetGenericArguments()[0], out var serialize, out var deserialize, memberInfo);

                    nm.IsExist = o => null != getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (IList) getMethod(o);
                        if (value == null) return;
                        writer.Write(tempTag);
                        writer.Write(value.Count);
                        foreach (var t in value)
                            serialize(writer, t);
                    };
                    nm.FullRead = (reader, o) =>
                    {
                        var value = (IList) Activator.CreateInstance(memberType);
                        var temp = reader.ReadInt();
                        for (var i = 0; i < temp; ++i)
                            value.Add(deserialize(reader));
                        setMethod(o, value);
                    };
                    nm.WriteDefault = o => setMethod(o, null);
                }
                else if (memberType.IsGenericType && memberType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                {
                    GetDirectMethods(memberType.GetGenericArguments()[0], out var serializeKey, out var deserializeKey, memberInfo);
                    GetDirectMethods(memberType.GetGenericArguments()[1], out var serializeValue, out var deserializeValue, memberInfo);

                    nm.IsExist = o => null != getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = (IDictionary) getMethod(o);
                        if (value == null) return;
                        writer.Write(tempTag);
                        writer.Write(value.Count);
                        foreach (DictionaryEntry entry in value)
                        {
                            serializeKey(writer, entry.Key);
                            serializeValue(writer, entry.Value);
                        }
                    };
                    nm.FullRead = (reader, o) =>
                    {
                        var value = (IDictionary) Activator.CreateInstance(memberType);
                        var temp = reader.ReadInt();
                        for (var i = 0; i < temp; ++i)
                        {
                            var key = deserializeKey(reader);
                            var v = deserializeValue(reader);
                            value.Add(key, v);
                        }

                        setMethod(o, value);
                    };
                    nm.WriteDefault = o => setMethod(o, null);
                }
                else if (memberType == typeof(object))
                {
                    nm.IsExist = o => null != getMethod(o);
                    nm.FullWrite = (writer, o) =>
                    {
                        var value = getMethod(o);
                        if (value == null) return;
                        writer.Write(tempTag);
                        ContractSerializer.Serialize(writer, value);
                    };
                    nm.FullRead = (reader, o) => setMethod(o, ContractSerializer.Deserialize(reader));
                    nm.WriteDefault = o => setMethod(o, null);
                }
                else
                {
                    nm.IsExist = o => null != getMethod(o);
                    nm.FullWrite = (writer, o) => throw new Exception($"Unsupported DataMember {memberType.Name}");
                    nm.FullRead = (reader, o) => throw new Exception($"Unsupported DataMember {memberType.Name}");
                    nm.WriteDefault = o => setMethod(o, null);
                }

                return nm;
            }

            public void Build()
            {
                if (_isBuild)
                    return;

                var dataContract = Type.GetCustomAttribute<ContractAttribute>(false);
                var unusedTag = dataContract.Tag ?? Type.Name;
                var fields = Type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .Where(u => u.GetCustomAttribute<ContractMemberAttribute>(false) != null);
                var properties = Type
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                    .Where(u => u.GetCustomAttribute<ContractMemberAttribute>(false) != null);

                var members = new List<MemberSerializationData>();

                #region Members

                foreach (var fieldInfo in fields)
                {
                    var info = fieldInfo;
                    var tempTag = info.GetCustomAttribute<ContractMemberAttribute>(false).Tag;

                    var nm = CreateMemberSerializationData(tempTag, info.GetValue, info.SetValue, info.FieldType, info);

                    members.Add(nm);
                }

                foreach (var propertyInfo in properties)
                {
                    var info = propertyInfo;
                    var tempTag = info.GetCustomAttribute<ContractMemberAttribute>(false).Tag;
                    var nm = CreateMemberSerializationData(tempTag, (o) => info.GetValue(o, null),
                        (o, value) => info.SetValue(o, value, null), info.PropertyType, info);
                    members.Add(nm);
                }

                #endregion

                members = members.OrderBy(u => u.Tag).ToList();
                var length = System.Math.Max(members.Count > 0 ? members.Max(u => u.Tag) : 0,
                                 dataContract.Reserved.Length > 0 ? dataContract.Reserved.Max() : 0) + 1;
                var serMembers = new MemberSerializationData[length];
                foreach (var memberSerializationData in members)
                    serMembers[memberSerializationData.Tag] = memberSerializationData;

                var additionalActions = typeof(IContractProcessor).IsAssignableFrom(Type);
                var isSelfSerialize = typeof(IContract).IsAssignableFrom(Type);

                _serializeAction = (writer, o) =>
                {
                    if (additionalActions)
                        ((IContractProcessor) o).PrepareSerialization();
                    
                    if(isSelfSerialize)
                        ((IContract)o).Serialize(writer);
                    else
                    {
                        byte count = 0;
                        for (var i = 0; i < members.Count; ++i)
                            if (members[i].IsExist(o))
                                ++count;

                        writer.Write(count);
                        for (var i = 0; i < members.Count; ++i)
                            members[i].FullWrite(writer, o);
                    }
                };
                _writeAction = (reader, o) =>
                {
                    if(isSelfSerialize)
                        ((IContract)o).Deserialize(reader);
                    else
                    {
                        byte nextToWrite = 0;
                        var count = reader.ReadByte();
                        for (var i = 0; i < count; ++i)
                        {
                            var index = reader.ReadByte();
                            while (nextToWrite < index)
                                serMembers[nextToWrite++]?.WriteDefault(o);
                            serMembers[nextToWrite++].FullRead(reader, o);
                        }

                        while (nextToWrite < serMembers.Length)
                            serMembers[nextToWrite++]?.WriteDefault(o);
                    }
                    
                    if (additionalActions)
                        ((IContractProcessor) o).PostDeserialization();

                    return o;
                };
                _deserializeAction = reader => _writeAction(reader, Activator.CreateInstance(Type));

                _isBuild = true;
            }

            public Type Type { get; }

            private static void GetDirectMethods(Type innerType,
                out Action<IBitsPacker, object> serialize,
                out Func<IBitsPacker, object> deserialize,
                MemberInfo memberInfo)
            {
                if (innerType == typeof(byte))
                {
                    serialize = (writer, o) => writer.Write((byte) o);
                    deserialize = reader => reader.ReadByte();
                }
                else if (innerType == typeof(byte[]))
                {
                    serialize = (writer, o) => writer.Write((byte[]) o);
                    deserialize = reader => reader.ReadBytes();
                }
                else if (innerType == typeof(float))
                {
                    var compressionAttribute = memberInfo?.GetCustomAttribute<CompressedFloatAttribute>(false);
                    if (compressionAttribute == null || compressionAttribute.Compression == CompessedType.None)
                    {
                        serialize = (writer, o) => writer.Write((float) o);
                        deserialize = reader => reader.ReadFloat();
                    }
                    else
                    {
                        switch (compressionAttribute.Compression)
                        {
                            case CompessedType.Byte:
                            {
                                var converter = new ByteFloatConverter(compressionAttribute.MinValue,
                                    compressionAttribute.MaxValue);
                                serialize = (writer, o) => writer.Write(converter.Pack((float) o));
                                deserialize = reader => converter.Unpack(reader.ReadByte());
                                break;
                            }
                            case CompessedType.Short:
                            {
                                var converter = new ShortFloatConverter(compressionAttribute.MinValue,
                                    compressionAttribute.MaxValue);
                                serialize = (writer, o) => writer.Write(converter.Pack((float) o));
                                deserialize = reader => converter.Unpack(reader.ReadShort());
                                break;
                            }
                            default:
                                throw new Exception(
                                    $"Compression method for {compressionAttribute.Compression} is not implemented");
                        }
                    }
                }
                else if (innerType == typeof(int))
                {
                    serialize = (writer, o) => writer.Write((int) o);
                    deserialize = reader => reader.ReadInt();
                }
                else if (innerType == typeof(string))
                {
                    serialize = (writer, o) => writer.Write((string) o);
                    deserialize = reader => reader.ReadString();
                }
                else if (innerType == typeof(bool))
                {
                    serialize = (writer, o) => writer.Write((bool) o);
                    deserialize = reader => reader.ReadBool();
                }
                else if (innerType == typeof(ushort))
                {
                    serialize = (writer, o) => writer.Write((ushort) o);
                    deserialize = reader => reader.ReadUShort();
                }
                else if (innerType.GetCustomAttribute<ContractAttribute>(false) != null)
                {
                    serialize = ContractSerializer.Serialize;
                    deserialize = ContractSerializer.Deserialize;
                }
                else
                {
                    throw new Exception($"{innerType.Name} is Not supported collection member type");
                }
            }
        }
    }

    static class ContractUtils
    {
        public static int GetContractSize(string text)
        {
            return sizeof(int) + Encoding.UTF8.GetBytes(text).Length;
        }
    }
}