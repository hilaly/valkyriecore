using System;

namespace Valkyrie.Data.Serialization
{
    interface ITypeSerializer
    {
        object Deserialize(IBitsPacker reader); 
        object Deserialize(IBitsPacker reader, object o); 
        void Serialize(object o, IBitsPacker writer);
        
        void Build();
        Type Type { get; }
    }
}