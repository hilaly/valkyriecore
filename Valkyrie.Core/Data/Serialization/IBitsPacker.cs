namespace Valkyrie.Data.Serialization
{
    public interface IBitsPacker
    {
        int Size { get; }
        int Position { get; }

        void Reset();
        void Resize(int size);
        void Load(byte[] buffer);
        byte[] Flush();

        void Write(byte value);
        void Write(short value);
        void Write(ushort value);
        void Write(int value);
        void Write(long value);
        void Write(string value);
        void Write(bool value);
        void Write(float value);
        void Write(byte[] value);

        byte ReadByte();
        short ReadShort();
        ushort ReadUShort();
        int ReadInt();
        long ReadLong();
        string ReadString();
        bool ReadBool();
        float ReadFloat();
        byte[] ReadBytes();
        byte[] ReadBytes(int count);
    }
}