﻿namespace Valkyrie.Data
{
    public interface IStringInterning
    {
        string Intern(string value);
    }
}