﻿using System.Collections.Generic;
using System.IO;
 using Valkyrie.NewVersion.Rsg;
 
 namespace Valkyrie.Data.Localization
 {
     public interface ILocalizationSerializer
     {
         IPromise Load(Stream stream);
     }

     public interface ILocalizationEditor
     {
         IList<string> Keys { get; }
     }
 }