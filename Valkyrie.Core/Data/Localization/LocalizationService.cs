﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Data.Localization
{

        public class LocalizationSource
        {
            public string Language;
            public Dictionary<string, string> Keys;
        }
        
    class LocalizationService : ILocalization, ILocalizationSerializer, ILocalizationEditor
    {
        private readonly IStringInterning _strings;
        private readonly Dictionary<string, Dictionary<string, string>> _sources =
            new Dictionary<string, Dictionary<string, string>>();
        private readonly Dictionary<string, string> _emptyLocale = new Dictionary<string, string>();
        private Dictionary<string, string> _currentLocale;
        private string _language;

        public bool IsDebug { get; set; }

        public LocalizationService(IStringInterning strings)
        {
            _strings = strings;
            Language = "en";
        }
        
        #region ILocalization

        public string Language
        {
            get => _language;
            set
            {
                _language = value;
                _sources.TryGetValue(_language, out _currentLocale);
                if (_currentLocale == null)
                    _currentLocale = _emptyLocale;
            }
        }

        public string GetString(string stringId)
        {
            return _currentLocale.TryGetValue(stringId, out var result)
                ? result
                : IsDebug ? $"key_not_found_{Language}_{stringId}" : stringId;
        }

        public string GetString(string stringId, params object[] args)
        {
            var formattedArgs = new object[args.Length];
            for (var i = 0; i < args.Length; ++i)
                formattedArgs[i] = args[i] is string s ? GetString(s) : args[i];
            return string.Format(GetString(stringId), formattedArgs);
        }

        public string GetFormattedString(string stringId, IEnumerable<object> args)
        {
            var a = args.ToArray();
            var formattedArgs = new object[a.Length];
            for (var i = 0; i < a.Length; ++i)
                formattedArgs[i] = a[i] is string s ? GetString(s) : a[i];
            return string.Format(GetString(stringId), formattedArgs);
        }

        #endregion
        
        #region ILocalizationSerilizer

        public IPromise Load(Stream stream)
        {
            var source = stream.ReadAllText();
            var loaded = source.ToObject<LocalizationSource>();
            _sources[loaded.Language] = loaded.Keys;
            Language = _language;
            return Promise.Resolved();
        }

        #endregion

        public IList<string> Keys
        {
            get
            {
                var l = new HashSet<string>(_sources.SelectMany(u => u.Value.Keys)).ToList();
                l.Sort();
                return l;
            }
        }
    }
}
