﻿using System.Collections.Generic;

namespace Valkyrie.Data.Localization
{
    public interface ILocalization
    {
        string Language { get; set; }
        bool IsDebug { get; set; }

        string GetString(string stringId);
        string GetString(string stringId, params object[] args);
        string GetFormattedString(string stringId, IEnumerable<object> args);
    }
}
