﻿namespace Valkyrie.Data.Localization
{
    class LocalizationEntry
    {
        public string Id { get; private set; }
        public string Value { get; private set; }
        public string Source { get; set; }

        public LocalizationEntry(string id, string value, IStringInterning stringInterning)
        {
            Id = stringInterning.Intern(id);
            Value = stringInterning.Intern(value);
        }
    }
}