using System;

namespace Valkyrie.Data.Bind
{
    public class ToStringAdapter : IBindingAdapter
    {
        public bool IsAwailableSourceType(Type type)
        {
            return true;
        }

        public Type GetResultType()
        {
            return typeof(string);
        }

        public object Convert(object source)
        {
            if (source == null)
                return string.Empty;
            return source.ToString();
        }
    }
}