using System;

namespace Valkyrie.Data.Bind
{
    public interface IBindingAdapter
    {
        bool IsAwailableSourceType(Type type);
        Type GetResultType();
        
        object Convert(object source);
    }
}