using System;

namespace Valkyrie.Data.Bind
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property | AttributeTargets.Method | AttributeTargets.Interface, Inherited = false)]
    public class BindAttribute : Attribute
    {}
}