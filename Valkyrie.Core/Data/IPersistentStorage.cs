﻿using System;

namespace Valkyrie.Data
{
    public interface IPersistentStorage
    {
        T Get<T>(string key);
        object Get(string key, Type type);
        void Set(string key, object value);
        bool Has(string key);
    }
}