using System;
using System.Collections.Generic;
using Valkyrie.Data;

namespace Valkyrie.Math
{
    class StringIdProvider : IIdProvider<string>
    {
        private readonly IStringInterning _stringInterning;
        private readonly Stack<string> _freeIds = new Stack<string>();

        public StringIdProvider(IStringInterning stringInterning)
        {
            _stringInterning = stringInterning;
        }

        public void Prepare(int count)
        {
            for(var i = 0; i < count; ++i)
                Release(New());
        }

        string New()
        {
            return _stringInterning.Intern(Guid.NewGuid().ToString());
        }

        public string Generate()
        {
            lock(_freeIds)
                return _freeIds.Count > 0 ? _freeIds.Pop() : New();
        }

        public void Release(string id)
        {
            lock (_freeIds)
                _freeIds.Push(id);
        }
    }
}