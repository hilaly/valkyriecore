using System.Collections.Generic;
using System.Text;

namespace Valkyrie.Math
{
    public class BigNumber
    {
        private bool _sign;
        private readonly List<byte> _numbers;

        #region Ctors

        public BigNumber(int value)
        {
            _sign = value < 0;

            _numbers = new List<byte>();

            if (value < 0)
                value = -value;

            while (value > 0 || _numbers.Count == 0)
            {
                var mod = value % 10;
                _numbers.Add((byte) mod);
                value /= 10;
            }

            Simplify();
        }

        public BigNumber()
        {
            _numbers = new List<byte>() {0};
            Simplify();
        }

        public BigNumber(List<byte> numbers)
        {
            _numbers = numbers;
            Simplify();
        }

        public BigNumber(BigNumber numbers)
        {
            _sign = numbers._sign;
            _numbers = new List<byte>(numbers._numbers);
            Simplify();
        }

        void Simplify()
        {
            while (_numbers.Count > 1 && _numbers[_numbers.Count - 1] == 0)
                _numbers.RemoveAt(_numbers.Count - 1);

            if (_numbers.Count == 1 && _numbers[0] == 0)
                _sign = false;
        }

        #endregion

        private byte this[int index] => _numbers.Count > index ? _numbers[index] : (byte) 0;

        #region Math

        static int CompareMod(BigNumber s0, BigNumber s1)
        {
            if (s0._numbers.Count > s1._numbers.Count)
                return 1;
            if (s0._numbers.Count < s1._numbers.Count)
                return -1;

            for (var i = s0._numbers.Count - 1; i >= 0; --i)
                if (s0._numbers[i] > s1._numbers[i])
                    return 1;
                else if (s0._numbers[i] < s1._numbers[i])
                    return -1;
            return 0;
        }

        public static bool operator >(BigNumber s0, BigNumber s1)
        {
            if (s0._sign == s1._sign)
            {
                var mod = CompareMod(s0, s1);
                if (mod == 0)
                    return false;
                if (mod > 0)
                    return !s0._sign;
                return s0._sign;
            }

            return s0._sign == false;
        }

        public static bool operator <(BigNumber s0, BigNumber s1)
        {
            if (s0._sign == s1._sign)
            {
                var mod = CompareMod(s1, s0);
                if (mod == 0)
                    return false;
                if (mod > 0)
                    return !s1._sign;
                return s1._sign;
            }

            return s1._sign == false;
        }

        public static bool operator ==(BigNumber s0, BigNumber s1)
        {
            if (s0._sign != s1._sign || s0._numbers.Count != s1._numbers.Count)
                return false;

            for (var i = 0; i < s0._numbers.Count; ++i)
                if (s0._numbers[i] != s1._numbers[i])
                    return false;

            return true;
        }

        public static bool operator !=(BigNumber s0, BigNumber s1)
        {
            return !(s0 == s1);
        }

        public static BigNumber operator -(BigNumber s0, BigNumber s1)
        {
            if (s0._sign != s1._sign)
                return s0 + -s1;

            BigNumber more = new BigNumber(s0);
            BigNumber less = new BigNumber(s1);

            var inverse = false;
            if (CompareMod(s0, s1) != 1)
            {
                inverse = true;
                less = new BigNumber(s0);
                more = new BigNumber(s1);
            }

            var result = new List<byte>(more._numbers.Count);
            foreach (var unused in more._numbers)
                result.Add(0);

            for (var i = more._numbers.Count - 1; i >= 0; --i)
            {
                var f = more[i];
                var s = less[i];

                if (s > f)
                {
                    f += 10;
                    for (var k = i + 1; k < result.Count; ++k)
                    {
                        if (result[k] == 0)
                            result[k] = 9;
                        else
                        {
                            result[k] -= 1;
                            break;
                        }
                    }
                }

                result[i] = (byte) (f - s);
            }

            return new BigNumber(result) {_sign = inverse ? !s0._sign : s0._sign};
        }

        public static BigNumber operator +(BigNumber s0, BigNumber s1)
        {
            if (s0._sign != s1._sign)
                return s0 - -s1;

            var digitsCount = System.Math.Max(s0._numbers.Count, s1._numbers.Count) + 1;
            var result = new List<byte>(digitsCount);

            var i = 0;
            byte c = 0;

            while (i < digitsCount)
            {
                var temp = (byte) ((byte) s0[i] + (byte) s1[i] + (byte) c);
                if (temp > 9)
                {
                    c = 1;
                    temp -= 10;
                }
                else
                    c = 0;

                result.Add(temp);
                ++i;
            }

            return new BigNumber(result) {_sign = s0._sign};
        }

        public static BigNumber operator -(BigNumber s)
        {
            return new BigNumber(new List<byte>(s._numbers))
            {
                _sign = !s._sign
            };
        }

        public static BigNumber operator <<(BigNumber s, int k)
        {
            var temp = new List<byte>(s._numbers);
            for (var i = 0; i < k; ++i)
                temp.Insert(0, 0);
            return new BigNumber(temp) {_sign = s._sign};
        }

        #endregion

        #region Constants

        public static readonly BigNumber Zero = new BigNumber();
        public static readonly BigNumber One = new BigNumber(1);
        public static readonly BigNumber Thousand = One << 3 * 1;
        public static readonly BigNumber Million = One << 3 * 2;
        public static readonly BigNumber Billion = One << 3 * 3;
        public static readonly BigNumber Trillion = One << 3 * 4;
        public static readonly BigNumber Quadrillion = One << 3 * 5;
        public static readonly BigNumber Quintillion = One << 3 * 6;
        public static readonly BigNumber Sextillion = One << 3 * 7;
        public static readonly BigNumber Septillion = One << 3 * 8;
        public static readonly BigNumber Octillion = One << 3 * 9;
        public static readonly BigNumber Nonillion = One << 3 * 10;
        public static readonly BigNumber Decillion = One << 3 * 11;
        public static readonly BigNumber Undecillion = One << 3 * 12;
        public static readonly BigNumber Duodecillion = One << 3 * 13;
        public static readonly BigNumber Tredecillion = One << 3 * 14;
        public static readonly BigNumber Quattuordecillion = One << 3 * 15;
        public static readonly BigNumber Quindecillion = One << 3 * 16;
        public static readonly BigNumber Sexdecillion = One << 3 * 17;
        public static readonly BigNumber Septendecillion = One << 3 * 18;
        public static readonly BigNumber Octodecillion = One << 3 * 19;
        public static readonly BigNumber Novemdecillion = One << 3 * 20;
        public static readonly BigNumber Vigintillion = One << 3 * 21;

        #endregion

        /// <summary>
        /*
         * 1K	=	1,000	=	One Thousand
         * 1M	=	1,000K	=	One Million
         * 1B	=	1,000M	=	One Billion
         * 1T	=	1,000B	=	One Trillion
         * 1q	=	1,000T	=	One Quadrillion
         * 1Q	=	1,000q	=	One Quintillion
         * 1s	=	1,000Q	=	One Sextillion
         * 1S	=	1,000s	=	One Septillion
         * 1O	=	1,000S	=	One Octillion
         * 1N	=	1,000O	=	One Nonillion
         * 1d	=	1,000N	=	One Decillion
         * 1U	=	1,000d	=	One Undecillion
         * 1D	=	1,000U	=	One Duodecillion
         * 1!	=	1,000D	=	One Tredecillion
         * 1@	=	1,000!	=	One Quattuordecillion
         * 1#	=	1,000@	=	One Quindecillion
         * 1$	=	1,000#	=	One Sexdecillion
         * 1%	=	1,000$	=	One Septendecillion
         * 1^	=	1,000%	=	One Octodecillion
         * 1&	=	1,000^	=	One Novemdecillion
         * 1*	=	1,000&	=	One Vigintillion
         * A lot	>	1,000*	<	A lot
         */
        /// </summary>
        /// <returns></returns>
        public string ToShortString()
        {
            
            return "A lot";
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (_sign && (_numbers.Count > 1 || _numbers[0] != 0))
                sb.Append("-");
            for (var i = _numbers.Count - 1; i >= 0; --i)
                sb.Append(_numbers[i].ToString());
            return sb.ToString();
        }

        public static BigNumber Parse(string value)
        {
            value = value.Trim();
            var sign = value[0] == '-';
            var number = sign ? value.Substring(1) : value;
            var result = new List<byte>(number.Length);
            for (var i = number.Length - 1; i >= 0; --i)
                result.Add(byte.Parse(number[i].ToString()));
            return new BigNumber(result) {_sign = sign};
        }
    }
}