﻿using System.Numerics;

namespace Valkyrie.Math.Noise
{
    /// <summary>
    /// Abstract class for generating noise.
    /// </summary>
	public abstract class Noise : INoise
	{

        /// <summary>
        /// The frequency of the fractal.
        /// </summary>
        public float Frequency { get; set; }

        /// <summary>
        /// The amplitude of the fractal.
        /// </summary>
        public float Amplitude { get; set; }

        /// <summary>
        /// The offset applied to each dimension.
        /// </summary>
        public Vector3 Offset { get; set; }

        /// <summary>
        /// Create a noise object.
        /// </summary>
		public Noise()
		{
            
		}

        /// <summary>
        /// Sample the noise in 1 dimension.
        /// </summary>
		public abstract float Sample1D(float x);

        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
		public abstract float Sample2D(float x, float y);

        /// <summary>
        /// Sample the noise in 3 dimensions.
        /// </summary>
		public abstract float Sample3D(float x, float y, float z);

        /// <summary>
        /// Update the seed.
        /// </summary>
        public abstract void UpdateSeed(int seed);
		
	}

    public class WorleyNoise : Noise
    {

        private static readonly float[] OFFSET_F = new float[] { -0.5f, 0.5f, 1.5f };

        private const float K = 1.0f / 7.0f;

        private const float Ko = 3.0f / 7.0f;

        public float Jitter { get; set; }

        public VoronoiDistance Distance { get; set; }

        public VoronoiCombination Combination { get; set; }

        private PermutationTable Perm { get; set; }

        public WorleyNoise(int seed, float frequency, float jitter, float amplitude = 1.0f)
        {

            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;
            Jitter = jitter;
            Distance = VoronoiDistance.EUCLIDIAN;
            Combination = VoronoiCombination.D1_D0;

            Perm = new PermutationTable(1024, 255, seed);
        }

        /// <summary>
        /// Update the seed.
        /// </summary>
        public override void UpdateSeed(int seed)
        {
            Perm.Build(seed);
        }

        /// <summary>
        /// Sample the noise in 1 dimension.
        /// </summary>
        public override float Sample1D(float x)
        {
            x = (x + Offset.X) * Frequency;

            int Pi0 = (int)System.Math.Floor(x);
            float Pf0 = Frac(x);

            Vector3 pX = new Vector3();
            pX.X = Perm[Pi0 - 1];
            pX.Y = Perm[Pi0];
            pX.Z = Perm[Pi0 + 1];

            float d0, d1, d2;
            float F0 = float.PositiveInfinity;
            float F1 = float.PositiveInfinity;
            float F2 = float.PositiveInfinity;

            int px, py, pz;
            float oxx, oxy, oxz;

            px = Perm[(int)pX.X];
            py = Perm[(int)pX.Y];
            pz = Perm[(int)pX.Z];

            oxx = Frac(px * K) - Ko;
            oxy = Frac(py * K) - Ko;
            oxz = Frac(pz * K) - Ko;

            d0 = Distance1(Pf0, OFFSET_F[0] + Jitter * oxx);
            d1 = Distance1(Pf0, OFFSET_F[1] + Jitter * oxy);
            d2 = Distance1(Pf0, OFFSET_F[2] + Jitter * oxz);

            if (d0 < F0) { F2 = F1; F1 = F0; F0 = d0; }
            else if (d0 < F1) { F2 = F1; F1 = d0; }
            else if (d0 < F2) { F2 = d0; }

            if (d1 < F0) { F2 = F1; F1 = F0; F0 = d1; }
            else if (d1 < F1) { F2 = F1; F1 = d1; }
            else if (d1 < F2) { F2 = d1; }

            if (d2 < F0) { F2 = F1; F1 = F0; F0 = d2; }
            else if (d2 < F1) { F2 = F1; F1 = d2; }
            else if (d2 < F2) { F2 = d2; }

            return Combine(F0, F1, F2) * Amplitude;
        }

        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
        public override float Sample2D(float x, float y)
        {

            x = (x + Offset.X) * Frequency;
            y = (y + Offset.Y) * Frequency;

            int Pi0 = (int)System.Math.Floor(x);
            int Pi1 = (int)System.Math.Floor(y);

            float Pf0 = Frac(x);
            float Pf1 = Frac(y);

            var pX = new float[3];
            pX[0] = Perm[Pi0 - 1];
            pX[1] = Perm[Pi0];
            pX[2] = Perm[Pi0 + 1];

            float d0, d1, d2;
            float F0 = float.PositiveInfinity;
            float F1 = float.PositiveInfinity;
            float F2 = float.PositiveInfinity;

            int px, py, pz;
            float oxx, oxy, oxz;
            float oyx, oyy, oyz;

            for (int i = 0; i < 3; i++)
            {
                px = Perm[(int)pX[i], Pi1 - 1];
                py = Perm[(int)pX[i], Pi1];
                pz = Perm[(int)pX[i], Pi1 + 1];

                oxx = Frac(px * K) - Ko;
                oxy = Frac(py * K) - Ko;
                oxz = Frac(pz * K) - Ko;

                oyx = Mod((float) System.Math.Floor(px * K), 7.0f) * K - Ko;
                oyy = Mod((float) System.Math.Floor(py * K), 7.0f) * K - Ko;
                oyz = Mod((float) System.Math.Floor(pz * K), 7.0f) * K - Ko;

                d0 = Distance2(Pf0, Pf1, OFFSET_F[i] + Jitter * oxx, -0.5f + Jitter * oyx);
                d1 = Distance2(Pf0, Pf1, OFFSET_F[i] + Jitter * oxy, 0.5f + Jitter * oyy);
                d2 = Distance2(Pf0, Pf1, OFFSET_F[i] + Jitter * oxz, 1.5f + Jitter * oyz);

                if (d0 < F0) { F2 = F1; F1 = F0; F0 = d0; }
                else if (d0 < F1) { F2 = F1; F1 = d0; }
                else if (d0 < F2) { F2 = d0; }

                if (d1 < F0) { F2 = F1; F1 = F0; F0 = d1; }
                else if (d1 < F1) { F2 = F1; F1 = d1; }
                else if (d1 < F2) { F2 = d1; }

                if (d2 < F0) { F2 = F1; F1 = F0; F0 = d2; }
                else if (d2 < F1) { F2 = F1; F1 = d2; }
                else if (d2 < F2) { F2 = d2; }

            }

            return Combine(F0, F1, F2) * Amplitude;
        }

        /// <summary>
        /// Sample the noise in 3 dimensions.
        /// </summary>
        public override float Sample3D(float x, float y, float z)
        {

            x = (x + Offset.X) * Frequency;
            y = (y + Offset.Y) * Frequency;
            z = (z + Offset.Z) * Frequency;

            int Pi0 = (int)System.Math.Floor(x);
            int Pi1 = (int)System.Math.Floor(y);
            int Pi2 = (int)System.Math.Floor(z);

            float Pf0 = Frac(x);
            float Pf1 = Frac(y);
            float Pf2 = Frac(z);

            var pX = new float[3];
            pX[0] = Perm[Pi0 - 1];
            pX[1] = Perm[Pi0];
            pX[2] = Perm[Pi0 + 1];

            var pY = new float[3];
            pY[0] = Perm[Pi1 - 1];
            pY[1] = Perm[Pi1];
            pY[2] = Perm[Pi1 + 1];

            float d0, d1, d2;
            float F0 = 1e6f;
            float F1 = 1e6f;
            float F2 = 1e6f;

            int px, py, pz;
            float oxx, oxy, oxz;
            float oyx, oyy, oyz;
            float ozx, ozy, ozz;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {

                    px = Perm[(int)pX[i], (int)pY[j], Pi2 - 1]; 
                    py = Perm[(int)pX[i], (int)pY[j], Pi2]; 
                    pz = Perm[(int)pX[i], (int)pY[j], Pi2 + 1];

                    oxx = Frac(px * K) - Ko;
                    oxy = Frac(py * K) - Ko;
                    oxz = Frac(pz * K) - Ko;

                    oyx = Mod((float) System.Math.Floor(px * K), 7.0f) * K - Ko;
                    oyy = Mod((float) System.Math.Floor(py * K), 7.0f) * K - Ko;
                    oyz = Mod((float) System.Math.Floor(pz * K), 7.0f) * K - Ko;

                    px = Perm[px];
                    py = Perm[py];
                    pz = Perm[pz];

                    ozx = Frac(px * K) - Ko;
                    ozy = Frac(py * K) - Ko;
                    ozz = Frac(pz * K) - Ko;

                    d0 = Distance3(Pf0, Pf1, Pf2, OFFSET_F[i] + Jitter * oxx, OFFSET_F[j] + Jitter * oyx, -0.5f + Jitter * ozx);
                    d1 = Distance3(Pf0, Pf1, Pf2, OFFSET_F[i] + Jitter * oxy, OFFSET_F[j] + Jitter * oyy, 0.5f + Jitter * ozy);
                    d2 = Distance3(Pf0, Pf1, Pf2, OFFSET_F[i] + Jitter * oxz, OFFSET_F[j] + Jitter * oyz, 1.5f + Jitter * ozz);

                    if (d0 < F0) { F2 = F1; F1 = F0; F0 = d0; }
                    else if (d0 < F1) { F2 = F1; F1 = d0; }
                    else if (d0 < F2) { F2 = d0; }

                    if (d1 < F0) { F2 = F1; F1 = F0; F0 = d1; }
                    else if (d1 < F1) { F2 = F1; F1 = d1; }
                    else if (d1 < F2) { F2 = d1; }

                    if (d2 < F0) { F2 = F1; F1 = F0; F0 = d2; }
                    else if (d2 < F1) { F2 = F1; F1 = d2; }
                    else if (d2 < F2) { F2 = d2; }
                }
            }

            return Combine(F0, F1, F2) * Amplitude;
        }

        private float Mod(float x, float y)
        {
            return  x - y * (float)System.Math.Floor(x / y);
        }

        private float Frac(float v)
        {
            return v - (float)System.Math.Floor(v);
        }

        private float Distance1(float p1x, float p2x)
        {
            switch (Distance)
            {
                case VoronoiDistance.EUCLIDIAN:
                    return (p1x - p2x) * (p1x - p2x);

                case VoronoiDistance.MANHATTAN:
                    return System.Math.Abs(p1x - p2x);

                case VoronoiDistance.CHEBYSHEV:
                    return System.Math.Abs(p1x - p2x);
            }

            return 0;
        }

        private float Distance2(float p1x, float p1y, float p2x, float p2y)
        {
            switch (Distance)
            {
                case VoronoiDistance.EUCLIDIAN:
                    return (p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y);

                case VoronoiDistance.MANHATTAN:
                    return System.Math.Abs(p1x - p2x) + System.Math.Abs(p1y - p2y);

                case VoronoiDistance.CHEBYSHEV:
                    return System.Math.Max(System.Math.Abs(p1x - p2x), System.Math.Abs(p1y - p2y));
            }

            return 0;
        }

        private float Distance3(float p1x, float p1y, float p1z, float p2x, float p2y, float p2z)
        {
            switch (Distance)
            {
                case VoronoiDistance.EUCLIDIAN:
                    return (p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y) + (p1z - p2z) * (p1z - p2z);

                case VoronoiDistance.MANHATTAN:
                    return System.Math.Abs(p1x - p2x) + System.Math.Abs(p1y - p2y) + System.Math.Abs(p1z - p2z);

                case VoronoiDistance.CHEBYSHEV:
                    return System.Math.Max(System.Math.Max(System.Math.Abs(p1x - p2x), System.Math.Abs(p1y - p2y)), System.Math.Abs(p1z - p2z));
            }

            return 0;
        }

        private float Combine(float f0, float f1, float f2)
        {
            switch (Combination)
            {
                case VoronoiCombination.D0:
                    return f0;

                case VoronoiCombination.D1_D0:
                    return f1 - f0;

                case VoronoiCombination.D2_D0:
                    return f2 - f0;
            }

            return 0;
        }
    }
    
    public enum VoronoiDistance { EUCLIDIAN, MANHATTAN, CHEBYSHEV };

    public enum VoronoiCombination { D0, D1_D0, D2_D0 };

    public class VoronoiNoise : Noise
    {

        public VoronoiDistance Distance { get; set; }

        public VoronoiCombination Combination { get; set; }

        private PermutationTable Perm { get; set; }

        public VoronoiNoise(int seed, float frequency, float amplitude = 1.0f)
        {

            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;

            Distance = VoronoiDistance.EUCLIDIAN;
            Combination = VoronoiCombination.D1_D0;

            Perm = new PermutationTable(1024, int.MaxValue, seed);

        }

        /// <summary>
        /// Update the seed.
        /// </summary>
        public override void UpdateSeed(int seed)
        {
            Perm.Build(seed);
        }

        /// <summary>
        /// Sample the noise in 1 dimension.
        /// </summary>
        public override float Sample1D(float x)
        {
            //The 0.75 is to make the scale simliar to the other noise algorithms
            x = (x + Offset.X) * Frequency * 0.75f;

            int lastRandom, numberFeaturePoints;
            float randomDiffX;
            float featurePointX;
            int cubeX;

            Vector3 distanceArray = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

            //1. Determine which cube the evaluation point is in
            int evalCubeX = (int)System.Math.Floor(x);

            for (int i = -1; i < 2; ++i)
            {
                cubeX = evalCubeX + i;

                //2. Generate a reproducible random number generator for the cube
                lastRandom = Perm[cubeX];

                //3. Determine how many feature points are in the cube
                numberFeaturePoints = ProbLookup(lastRandom * Perm.Inverse);

                //4. Randomly place the feature points in the cube
                for (int l = 0; l < numberFeaturePoints; ++l)
                {
                    lastRandom = Perm[lastRandom];
                    randomDiffX = lastRandom * Perm.Inverse;

                    lastRandom = Perm[lastRandom];

                    featurePointX = randomDiffX + cubeX;

                    //5. Find the feature point closest to the evaluation point. 
                    //This is done by inserting the distances to the feature points into a sorted list
                    distanceArray = Insert(distanceArray, Distance1(x, featurePointX));
                }

                //6. Check the neighboring cubes to ensure their are no closer evaluation points.
                // This is done by repeating steps 1 through 5 above for each neighboring cube
            }

            return Combine(distanceArray) * Amplitude;
        }

        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
        public override float Sample2D(float x, float y)
        {
            //The 0.75 is to make the scale simliar to the other noise algorithms
            x = (x + Offset.X) * Frequency * 0.75f;
            y = (y + Offset.Y) * Frequency * 0.75f;

            int lastRandom, numberFeaturePoints;
            float randomDiffX, randomDiffY;
            float featurePointX, featurePointY;
            int cubeX, cubeY;

            Vector3 distanceArray = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

            //1. Determine which cube the evaluation point is in
            int evalCubeX = (int)System.Math.Floor(x);
            int evalCubeY = (int)System.Math.Floor(y);

            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    cubeX = evalCubeX + i;
                    cubeY = evalCubeY + j;

                    //2. Generate a reproducible random number generator for the cube
                    lastRandom = Perm[cubeX, cubeY];

                    //3. Determine how many feature points are in the cube
                    numberFeaturePoints = ProbLookup(lastRandom * Perm.Inverse);

                    //4. Randomly place the feature points in the cube
                    for (int l = 0; l < numberFeaturePoints; ++l)
                    {
                        lastRandom = Perm[lastRandom];
                        randomDiffX = lastRandom * Perm.Inverse;

                        lastRandom = Perm[lastRandom];
                        randomDiffY = lastRandom * Perm.Inverse;

                        featurePointX = randomDiffX + cubeX;
                        featurePointY = randomDiffY + cubeY;

                        //5. Find the feature point closest to the evaluation point. 
                        //This is done by inserting the distances to the feature points into a sorted list
                        distanceArray = Insert(distanceArray, Distance2(x, y, featurePointX, featurePointY));
                    }

                    //6. Check the neighboring cubes to ensure their are no closer evaluation points.
                    // This is done by repeating steps 1 through 5 above for each neighboring cube
                }
            }

            return Combine(distanceArray) * Amplitude;
        }

        /// <summary>
        /// Sample the noise in 3 dimensions.
        /// </summary>
        public override float Sample3D(float x, float y, float z)
        {
            //The 0.75 is to make the scale simliar to the other noise algorithms
            x = (x + Offset.X) * Frequency * 0.75f;
            y = (y + Offset.Y) * Frequency * 0.75f;
            z = (z + Offset.Z) * Frequency * 0.75f;

            int lastRandom, numberFeaturePoints;
            float randomDiffX, randomDiffY, randomDiffZ;
            float featurePointX, featurePointY, featurePointZ;
            int cubeX, cubeY, cubeZ;

            Vector3 distanceArray = new Vector3(float.PositiveInfinity, float.PositiveInfinity, float.PositiveInfinity);

            //1. Determine which cube the evaluation point is in
            int evalCubeX = (int)System.Math.Floor(x);
            int evalCubeY = (int)System.Math.Floor(y);
            int evalCubeZ = (int)System.Math.Floor(z);

            for (int i = -1; i < 2; ++i)
            {
                for (int j = -1; j < 2; ++j)
                {
                    for (int k = -1; k < 2; ++k)
                    {
                        cubeX = evalCubeX + i;
                        cubeY = evalCubeY + j;
                        cubeZ = evalCubeZ + k;

                        //2. Generate a reproducible random number generator for the cube
                        lastRandom = Perm[cubeX, cubeY, cubeZ];

                        //3. Determine how many feature points are in the cube
                        numberFeaturePoints = ProbLookup(lastRandom * Perm.Inverse);

                        //4. Randomly place the feature points in the cube
                        for (int l = 0; l < numberFeaturePoints; ++l)
                        {
                            lastRandom = Perm[lastRandom];
                            randomDiffX = lastRandom * Perm.Inverse;

                            lastRandom = Perm[lastRandom];
                            randomDiffY = lastRandom * Perm.Inverse;

                            lastRandom = Perm[lastRandom];
                            randomDiffZ = lastRandom * Perm.Inverse;

                            featurePointX = randomDiffX + cubeX;
                            featurePointY = randomDiffY + cubeY;
                            featurePointZ = randomDiffZ + cubeZ;

                            //5. Find the feature point closest to the evaluation point. 
                            //This is done by inserting the distances to the feature points into a sorted list
                            distanceArray = Insert(distanceArray, Distance3(x, y, z, featurePointX, featurePointY, featurePointZ));
                        }

                        //6. Check the neighboring cubes to ensure their are no closer evaluation points.
                        // This is done by repeating steps 1 through 5 above for each neighboring cube
                    }
                }
            }

            return Combine(distanceArray) * Amplitude;
        }

        private float Distance1(float p1x, float p2x)
        {
            switch (Distance)
            {
                case VoronoiDistance.EUCLIDIAN:
                    return (p1x - p2x) * (p1x - p2x);

                case VoronoiDistance.MANHATTAN:
                    return System.Math.Abs(p1x - p2x);

                case VoronoiDistance.CHEBYSHEV:
                    return System.Math.Abs(p1x - p2x);
            }

            return 0;
        }

        private float Distance2(float p1x, float p1y, float p2x, float p2y)
        {
            switch(Distance)
            {
                case VoronoiDistance.EUCLIDIAN:
                    return (p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y);

                case VoronoiDistance.MANHATTAN:
                    return System.Math.Abs(p1x - p2x) + System.Math.Abs(p1y - p2y);

                case VoronoiDistance.CHEBYSHEV:
                    return System.Math.Max(System.Math.Abs(p1x - p2x), System.Math.Abs(p1y - p2y));
            }

            return 0;
        }

        private float Distance3(float p1x, float p1y, float p1z, float p2x, float p2y, float p2z)
        {
            switch (Distance)
            {
                case VoronoiDistance.EUCLIDIAN:
                    return (p1x - p2x) * (p1x - p2x) + (p1y - p2y) * (p1y - p2y) + (p1z - p2z) * (p1z - p2z);

                case VoronoiDistance.MANHATTAN:
                    return System.Math.Abs(p1x - p2x) + System.Math.Abs(p1y - p2y) + System.Math.Abs(p1z - p2z);

                case VoronoiDistance.CHEBYSHEV:
                    return System.Math.Max(System.Math.Max(System.Math.Abs(p1x - p2x), System.Math.Abs(p1y - p2y)), System.Math.Abs(p1z - p2z));
            }

            return 0;
        }

        private float Combine(Vector3 arr)
        {
            switch(Combination)
            {
                case VoronoiCombination.D0:
                    return arr.X;

                case VoronoiCombination.D1_D0:
                    return arr.Y - arr.X;

                case VoronoiCombination.D2_D0:
                    return arr.Z - arr.X;
            }

            return 0;
        }

        /// <summary>
        /// Given a uniformly distributed random number this function returns the number of feature points in a given cube.
        /// </summary>
        /// <param name="value">a uniformly distributed random number</param>
        /// <returns>The number of feature points in a cube.</returns>
        int ProbLookup(float value)
        {
            //Poisson Distribution
            if (value < 0.0915781944272058) return 1;
            if (value < 0.238103305510735) return 2;
            if (value < 0.433470120288774) return 3;
            if (value < 0.628836935299644) return 4;
            if (value < 0.785130387122075) return 5;
            if (value < 0.889326021747972) return 6;
            if (value < 0.948866384324819) return 7;
            if (value < 0.978636565613243) return 8;

            return 9;
        }

        /// <summary>
        /// Inserts value into array using insertion sort. If the value is greater than the largest value in the array
        /// it will not be added to the array.
        /// </summary>
        /// <param name="arr">The array to insert the value into.</param>
        /// <param name="value">The value to insert into the array.</param>
        Vector3 Insert(Vector3 v, float value)
        {
            var arr = new[] {v.X, v.Y, v.Z};
            for (var i = 2; i >= 0; i--)
            {
                if (value > arr[i])
                    return v;
                var temp = arr[i];
                arr[i] = value;
                if (i + 1 < 3) arr[i + 1] = temp;
            }

            v.X = arr[0];
            v.Y = arr[1];
            v.Z = arr[2];
            return v;
        }

    }



    /// <summary>
    /// Simple noise implementation by interpolating random values.
    /// Works same as Perlin noise but uses the values instead of gradients.
    /// Perlin noise uses gradients as it makes better noise but this still
   ///  looks good and might be a little faster.
    /// </summary>
	public class ValueNoise : Noise
	{

        private PermutationTable Perm { get; set; }

        public ValueNoise(int seed, float frequency, float amplitude = 1.0f) 
        {

            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;

            Perm = new PermutationTable(1024, 255, seed);

        }

        /// <summary>
        /// Update the seed.
        /// </summary>
        public override void UpdateSeed(int seed)
        {
            Perm.Build(seed);
        }

        /// <summary>
        /// Sample the noise in 1 dimension.
        /// </summary>
        public override float Sample1D(float x)
        {
            x = (x + Offset.X) * Frequency;

            int ix0;
            float fx0;
            float s, n0, n1;

            ix0 = (int)System.Math.Floor(x);     // Integer part of x
            fx0 = x - ix0;                // Fractional part of x

            s = FADE(fx0);

            n0 = Perm[ix0];
            n1 = Perm[ix0 + 1];

            // rescale from 0 to 255 to -1 to 1.
            float n = LERP(s, n0, n1) * Perm.Inverse;
            n = n * 2.0f - 1.0f;

            return n * Amplitude;
        }

        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
        public override float Sample2D(float x, float y)
        {
            x = (x + Offset.X) * Frequency;
            y = (y + Offset.Y) * Frequency;

            int ix0, iy0;
            float fx0, fy0, s, t, nx0, nx1, n0, n1;

            ix0 = (int)System.Math.Floor(x);   // Integer part of x
            iy0 = (int)System.Math.Floor(y);   // Integer part of y

            fx0 = x - ix0;              // Fractional part of x
            fy0 = y - iy0;        		// Fractional part of y

            t = FADE(fy0);
            s = FADE(fx0);

            nx0 = Perm[ix0, iy0];
            nx1 = Perm[ix0, iy0 + 1];

            n0 = LERP(t, nx0, nx1);

            nx0 = Perm[ix0 + 1, iy0];
            nx1 = Perm[ix0 + 1, iy0 + 1];

            n1 = LERP(t, nx0, nx1);

            // rescale from 0 to 255 to -1 to 1.
            float n = LERP(s, n0, n1) * Perm.Inverse;
            n = n * 2.0f - 1.0f;

            return n * Amplitude;
        }

        /// <summary>
        /// Sample the noise in 3 dimensions.
        /// </summary>
        public override float Sample3D(float x, float y, float z)
        {

            x = (x + Offset.X) * Frequency;
            y = (y + Offset.Y) * Frequency;
            z = (z + Offset.Z) * Frequency;

            int ix0, iy0, iz0;
            float fx0, fy0, fz0;
            float s, t, r;
            float nxy0, nxy1, nx0, nx1, n0, n1;

            ix0 = (int)System.Math.Floor(x);   // Integer part of x
            iy0 = (int)System.Math.Floor(y);   // Integer part of y
            iz0 = (int)System.Math.Floor(z);   // Integer part of z
            fx0 = x - ix0;              // Fractional part of x
            fy0 = y - iy0;              // Fractional part of y
            fz0 = z - iz0;              // Fractional part of z

            r = FADE(fz0);
            t = FADE(fy0);
            s = FADE(fx0);

            nxy0 = Perm[ix0, iy0, iz0];
            nxy1 = Perm[ix0, iy0, iz0 + 1];
            nx0 = LERP(r, nxy0, nxy1);

            nxy0 = Perm[ix0, iy0 + 1, iz0];
            nxy1 = Perm[ix0, iy0 + 1, iz0 + 1];
            nx1 = LERP(r, nxy0, nxy1);

            n0 = LERP(t, nx0, nx1);

            nxy0 = Perm[ix0 + 1, iy0, iz0];
            nxy1 = Perm[ix0 + 1, iy0, iz0 + 1];
            nx0 = LERP(r, nxy0, nxy1);

            nxy0 = Perm[ix0 + 1, iy0 + 1, iz0];
            nxy1 = Perm[ix0 + 1, iy0 + 1, iz0 + 1];
            nx1 = LERP(r, nxy0, nxy1);

            n1 = LERP(t, nx0, nx1);

            // rescale from 0 to 255 to -1 to 1.
            float n = LERP(s, n0, n1) * Perm.Inverse;
            n = n * 2.0f - 1.0f;

            return n * Amplitude;
        }

        private float FADE(float t) { return t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f); }

        private float LERP(float t, float a, float b) { return a + t * (b - a); }

	}

    /// <summary>
    /// Implementation of the Perlin simplex noise, an improved Perlin noise algorithm.
    /// Based loosely on SimplexNoise1234 by Stefan Gustavson 
    /// <http://staffwww.itn.liu.se/~stegu/aqsis/aqsis-newnoise/>
    /// </summary>
	public class SimplexNoise : Noise
    {

        private PermutationTable Perm { get; set; }

        /// <summary>
        /// Create a simplex noise object.
        /// </summary>
        public SimplexNoise(int seed, float frequency, float amplitude = 1.0f)
        {

            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;

            Perm = new PermutationTable(1024, 255, seed);
        }

        /// <summary>
        /// Update the seed.
        /// </summary>
        public override void UpdateSeed(int seed)
        {
            Perm.Build(seed);
        }

       	/// <summary>
       	/// Sample the noise in 1 dimension.
       	/// </summary>
		public override float Sample1D(float x)
        {
            //The 0.5 is to make the scale simliar to the other noise algorithms
            x = (x + Offset.X) * Frequency * 0.5f;

            int i0 = (int)System.Math.Floor(x);
            int i1 = i0 + 1;
            float x0 = x - i0;
            float x1 = x0 - 1.0f;

            float n0, n1;

            float t0 = 1.0f - x0*x0;
            t0 *= t0;
			n0 = t0 * t0 * Grad(Perm[i0], x0);

            float t1 = 1.0f - x1*x1;
            t1 *= t1;
			n1 = t1 * t1 * Grad(Perm[i1], x1);

            // The maximum value of this noise is 8*(3/4)^4 = 2.53125
            // A factor of 0.395 scales to fit exactly within [-1,1]
            return 0.395f * (n0 + n1) * Amplitude;
        }

		/// <summary>
		/// Sample the noise in 2 dimensions.
		/// </summary>
		public override float Sample2D(float x, float y)
        {
            //The 0.5 is to make the scale simliar to the other noise algorithms
            x = (x + Offset.X) * Frequency * 0.5f;
            y = (y + Offset.Y) * Frequency * 0.5f;

            const float F2 = 0.366025403f; // F2 = 0.5*(sqrt(3.0)-1.0)
            const float G2 = 0.211324865f; // G2 = (3.0-Math.sqrt(3.0))/6.0

            float n0, n1, n2; // Noise contributions from the three corners

            // Skew the input space to determine which simplex cell we're in
            float s = (x+y)*F2; // Hairy factor for 2D
            float xs = x + s;
            float ys = y + s;
            int i = (int)System.Math.Floor(xs);
            int j = (int)System.Math.Floor(ys);

            float t = (i+j)*G2;
            float X0 = i-t; // Unskew the cell origin back to (x,y) space
            float Y0 = j-t;
            float x0 = x-X0; // The x,y distances from the cell origin
            float y0 = y-Y0;

            // For the 2D case, the simplex shape is an equilateral triangle.
            // Determine which simplex we are in.
            int i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
            if(x0>y0) {i1=1; j1=0;} // lower triangle, XY order: (0,0)->(1,0)->(1,1)
            else {i1=0; j1=1;}      // upper triangle, YX order: (0,0)->(0,1)->(1,1)

            // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
            // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
            // c = (3-sqrt(3))/6

            float x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
            float y1 = y0 - j1 + G2;
            float x2 = x0 - 1.0f + 2.0f * G2; // Offsets for last corner in (x,y) unskewed coords
            float y2 = y0 - 1.0f + 2.0f * G2;

            // Calculate the contribution from the three corners
            float t0 = 0.5f - x0*x0-y0*y0;
            if(t0 < 0.0) n0 = 0.0f;
            else {
                t0 *= t0;
				n0 = t0 * t0 * Grad(Perm[i, j], x0, y0); 
            }

            float t1 = 0.5f - x1*x1-y1*y1;
            if(t1 < 0.0) n1 = 0.0f;
            else {
                t1 *= t1;
				n1 = t1 * t1 * Grad(Perm[i+i1, j+j1], x1, y1);
            }

            float t2 = 0.5f - x2*x2-y2*y2;
            if(t2 < 0.0) n2 = 0.0f;
            else {
                t2 *= t2;
				n2 = t2 * t2 * Grad(Perm[i+1, j+1], x2, y2);
            }

            // Add contributions from each corner to get the final noise value.
            // The result is scaled to return values in the interval [-1,1].
            return 40.0f * (n0 + n1 + n2) * Amplitude; 
        }

		/// <summary>
		/// Sample the noise in 3 dimensions.
		/// </summary>
        public override float Sample3D(float x, float y, float z)
        {
            //The 0.5 is to make the scale simliar to the other noise algorithms
            x = (x + Offset.X) * Frequency * 0.5f;
            y = (y + Offset.Y) * Frequency * 0.5f;
            z = (z + Offset.Z) * Frequency * 0.5f;

            // Simple skewing factors for the 3D case
            const float F3 = 0.333333333f;
            const float G3 = 0.166666667f;

            float n0, n1, n2, n3; // Noise contributions from the four corners

            // Skew the input space to determine which simplex cell we're in
            float s = (x+y+z)*F3; // Very nice and simple skew factor for 3D
            float xs = x+s;
            float ys = y+s;
            float zs = z+s;
            int i = (int)System.Math.Floor(xs);
            int j = (int)System.Math.Floor(ys);
            int k = (int)System.Math.Floor(zs);

            float t = (i+j+k)*G3; 
            float X0 = i-t; // Unskew the cell origin back to (x,y,z) space
            float Y0 = j-t;
            float Z0 = k-t;
            float x0 = x-X0; // The x,y,z distances from the cell origin
            float y0 = y-Y0;
            float z0 = z-Z0;

            // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
            // Determine which simplex we are in.
            int i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
            int i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords

            /* This code would benefit from a backport from the GLSL version! */
            if(x0>=y0) {
                if(y0>=z0)
                { i1=1; j1=0; k1=0; i2=1; j2=1; k2=0; } // X Y Z order
                else if(x0>=z0) { i1=1; j1=0; k1=0; i2=1; j2=0; k2=1; } // X Z Y order
                else { i1=0; j1=0; k1=1; i2=1; j2=0; k2=1; } // Z X Y order
                }
            else { // x0<y0
                if(y0<z0) { i1=0; j1=0; k1=1; i2=0; j2=1; k2=1; } // Z Y X order
                else if(x0<z0) { i1=0; j1=1; k1=0; i2=0; j2=1; k2=1; } // Y Z X order
                else { i1=0; j1=1; k1=0; i2=1; j2=1; k2=0; } // Y X Z order
            }

            // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
            // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
            // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
            // c = 1/6.

            float x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
            float y1 = y0 - j1 + G3;
            float z1 = z0 - k1 + G3;
            float x2 = x0 - i2 + 2.0f*G3; // Offsets for third corner in (x,y,z) coords
            float y2 = y0 - j2 + 2.0f*G3;
            float z2 = z0 - k2 + 2.0f*G3;
            float x3 = x0 - 1.0f + 3.0f*G3; // Offsets for last corner in (x,y,z) coords
            float y3 = y0 - 1.0f + 3.0f*G3;
            float z3 = z0 - 1.0f + 3.0f*G3;

            // Calculate the contribution from the four corners
            float t0 = 0.6f - x0*x0 - y0*y0 - z0*z0;
            if(t0 < 0.0) n0 = 0.0f;
            else {
                t0 *= t0;
				n0 = t0 * t0 * Grad(Perm[i, j, k], x0, y0, z0);
            }

            float t1 = 0.6f - x1*x1 - y1*y1 - z1*z1;
            if(t1 < 0.0) n1 = 0.0f;
            else {
                t1 *= t1;
				n1 = t1 * t1 * Grad(Perm[i+i1, j+j1, k+k1], x1, y1, z1);
            }

            float t2 = 0.6f - x2*x2 - y2*y2 - z2*z2;
            if(t2 < 0.0) n2 = 0.0f;
            else {
                t2 *= t2;
				n2 = t2 * t2 * Grad(Perm[i+i2, j+j2, k+k2], x2, y2, z2);
            }

            float t3 = 0.6f - x3*x3 - y3*y3 - z3*z3;
            if(t3<0.0) n3 = 0.0f;
            else {
                t3 *= t3;
				n3 = t3 * t3 * Grad(Perm[i+1, j+1, k+1], x3, y3, z3);
            }

            // Add contributions from each corner to get the final noise value.
            // The result is scaled to stay just inside [-1,1]
            return 32.0f * (n0 + n1 + n2 + n3) * Amplitude;
        }

        private float Grad(int hash, float x)
        {
            int h = hash & 15;
            float grad = 1.0f + (h & 7);   // Gradient value 1.0, 2.0, ..., 8.0
            if ((h & 8) != 0) grad = -grad;// Set a random sign for the gradient
            return grad * x;           // Multiply the gradient with the distance
        }

        private float Grad(int hash, float x, float y)
        {
            int h = hash & 7;           // Convert low 3 bits of hash code
            float u = h < 4 ? x : y;     // into 8 simple gradient directions,
            float v = h < 4 ? y : x;     // and compute the dot product with (x,y).
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0f * v : 2.0f * v);
        }

        private float Grad(int hash, float x, float y, float z)
        {
            int h = hash & 15;      // Convert low 4 bits of hash code into 12 simple
            float u = h < 8 ? x : y; // gradient directions, and compute dot product.
            float v = h < 4 ? y : h == 12 || h == 14 ? x : z; // Fix repeats at h = 12 to 15
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
        }

        private float Grad(int hash, float x, float y, float z, float t)
        {
            int h = hash & 31;          // Convert low 5 bits of hash code into 32 simple
            float u = h < 24 ? x : y;    // gradient directions, and compute dot product.
            float v = h < 16 ? y : z;
            float w = h < 8 ? z : t;
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v) + ((h & 4) != 0 ? -w : w);
        }

    }
    internal class PermutationTable
    {

        public int Size { get; private set; }

        public int Seed { get; private set; }

        public int Max { get; private set; }

        public float Inverse { get; private set; }

        private readonly int Wrap;

        private int[] Table;

        internal PermutationTable(int size, int max, int seed)
        {
            Size = size;
            Wrap = Size - 1;
            Max = System.Math.Max(1, max);
            Inverse = 1.0f / Max;
            Build(seed);
        }

        internal void Build(int seed)
        {
            if (Seed == seed && Table != null) return;

            Seed = seed;
            Table = new int[Size];

            System.Random rnd = new System.Random(Seed);

            for(int i = 0; i < Size; i++)
            {
                Table[i] = rnd.Next();
            }
        }

        internal int this[int i]
        {
            get
            {
                return Table[i & Wrap] & Max;
            }
        }

        internal int this[int i, int j]
        {
            get
            {
                return Table[(j + Table[i & Wrap]) & Wrap] & Max;
            }
        }

        internal int this[int i, int j, int k]
        {
            get
            {
                return Table[(k + Table[(j + Table[i & Wrap]) & Wrap]) & Wrap] & Max;
            }
        }

    }

	public class PerlinNoise : Noise
	{

        private PermutationTable Perm { get; set; }

        public PerlinNoise(int seed, float frequency, float amplitude = 1.0f)
		{

            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;

            Perm = new PermutationTable(1024, 255, seed);
		}

        public override void UpdateSeed(int seed)
        {
            Perm.Build(seed);
        }
		
        /// <summary>
        /// Sample the noise in 1 dimension.
        /// </summary>
		public override float Sample1D( float x )
		{
            x = (x + Offset.X) * Frequency;

		    int ix0;
		    float fx0, fx1;
		    float s, n0, n1;
		
		    ix0 = (int)System.Math.Floor(x); 	// Integer part of x
		    fx0 = x - ix0;              // Fractional part of x
		    fx1 = fx0 - 1.0f;
			
		    s = FADE(fx0);
		
		    n0 = Grad(Perm[ix0], fx0);
		    n1 = Grad(Perm[ix0 + 1], fx1);

            return 0.25f * LERP(s, n0, n1) * Amplitude;
		}
		
        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
		public override float Sample2D( float x, float y )
		{
            x = (x + Offset.X) * Frequency;
            y = (y + Offset.Y) * Frequency;

		    int ix0, iy0;
		    float fx0, fy0, fx1, fy1, s, t, nx0, nx1, n0, n1;
		
			ix0 = (int)System.Math.Floor(x); 		// Integer part of x
			iy0 = (int)System.Math.Floor(y); 		// Integer part of y

		    fx0 = x - ix0;        		// Fractional part of x
		    fy0 = y - iy0;        		// Fractional part of y
		    fx1 = fx0 - 1.0f;
		    fy1 = fy0 - 1.0f;
		    
		    t = FADE( fy0 );
		    s = FADE( fx0 );

			nx0 = Grad(Perm[ix0, iy0], fx0, fy0);
            nx1 = Grad(Perm[ix0, iy0 + 1], fx0, fy1);

		    n0 = LERP( t, nx0, nx1 );

		    nx0 = Grad(Perm[ix0 + 1, iy0], fx1, fy0);
		    nx1 = Grad(Perm[ix0 + 1, iy0 + 1], fx1, fy1);

		    n1 = LERP(t, nx0, nx1);

            return 0.66666f * LERP(s, n0, n1) * Amplitude;
		}
		
        /// <summary>
        /// Sample the noise in 3 dimensions.
        /// </summary>
		public override float Sample3D( float x, float y, float z )
		{
            x = (x + Offset.X) * Frequency;
            y = (y + Offset.Y) * Frequency;
            z = (z + Offset.Z) * Frequency;

            int ix0, iy0, iz0;
		    float fx0, fy0, fz0, fx1, fy1, fz1;
		    float s, t, r;
		    float nxy0, nxy1, nx0, nx1, n0, n1;
		
			ix0 = (int)System.Math.Floor(x);   		// Integer part of x
			iy0 = (int)System.Math.Floor(y);   		// Integer part of y
			iz0 = (int)System.Math.Floor(z);   		// Integer part of z
		    fx0 = x - ix0;        		        // Fractional part of x
		    fy0 = y - iy0;        		        // Fractional part of y
		    fz0 = z - iz0;        		        // Fractional part of z
		    fx1 = fx0 - 1.0f;
		    fy1 = fy0 - 1.0f;
		    fz1 = fz0 - 1.0f;
		    
		    r = FADE( fz0 );
		    t = FADE( fy0 );
		    s = FADE( fx0 );
		
			nxy0 = Grad(Perm[ix0, iy0, iz0], fx0, fy0, fz0);
		    nxy1 = Grad(Perm[ix0, iy0, iz0 + 1], fx0, fy0, fz1);
		    nx0 = LERP( r, nxy0, nxy1 );
		
		    nxy0 = Grad(Perm[ix0, iy0 + 1, iz0], fx0, fy1, fz0);
		    nxy1 = Grad(Perm[ix0, iy0 + 1, iz0 + 1], fx0, fy1, fz1);
		    nx1 = LERP( r, nxy0, nxy1 );
		
		    n0 = LERP( t, nx0, nx1 );
		
		    nxy0 = Grad(Perm[ix0 + 1, iy0, iz0], fx1, fy0, fz0);
		    nxy1 = Grad(Perm[ix0 + 1, iy0, iz0 + 1], fx1, fy0, fz1);
		    nx0 = LERP( r, nxy0, nxy1 );
		
		    nxy0 = Grad(Perm[ix0 + 1, iy0 + 1, iz0], fx1, fy1, fz0);
		   	nxy1 = Grad(Perm[ix0 + 1, iy0 + 1, iz0 + 1], fx1, fy1, fz1);
		    nx1 = LERP( r, nxy0, nxy1 );
		
		    n1 = LERP( t, nx0, nx1 );

            return 1.1111f * LERP(s, n0, n1) * Amplitude;
		}

        private float FADE(float t) { return t * t * t * (t * (t * 6.0f - 15.0f) + 10.0f); }

        private float LERP(float t, float a, float b) { return a + t * (b - a); }

        private float Grad(int hash, float x)
        {
            int h = hash & 15;
            float grad = 1.0f + (h & 7);    // Gradient value 1.0, 2.0, ..., 8.0
            if ((h & 8) != 0) grad = -grad; // Set a random sign for the gradient
            return grad * x;              // Multiply the gradient with the distance
        }

        private float Grad(int hash, float x, float y)
        {
            int h = hash & 7;           // Convert low 3 bits of hash code
            float u = h < 4 ? x : y;  // into 8 simple gradient directions,
            float v = h < 4 ? y : x;  // and compute the dot product with (x,y).
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -2.0f * v : 2.0f * v);
        }

        private float Grad(int hash, float x, float y, float z)
        {
            int h = hash & 15;     // Convert low 4 bits of hash code into 12 simple
            float u = h < 8 ? x : y; // gradient directions, and compute dot product.
            float v = h < 4 ? y : h == 12 || h == 14 ? x : z; // Fix repeats at h = 12 to 15
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v);
        }

        private float Grad(int hash, float x, float y, float z, float t)
        {
            int h = hash & 31;          // Convert low 5 bits of hash code into 32 simple
            float u = h < 24 ? x : y; // gradient directions, and compute dot product.
            float v = h < 16 ? y : z;
            float w = h < 8 ? z : t;
            return ((h & 1) != 0 ? -u : u) + ((h & 2) != 0 ? -v : v) + ((h & 4) != 0 ? -w : w);
        }


    }

    /// <summary>
    /// A concrete class for generating fractal noise.
    /// </summary>
	public class FractalNoise : INoise
    {

        /// <summary>
        /// The number of octaves in the fractal.
        /// </summary>
        public int Octaves { get; set; }

        /// <summary>
        /// The frequency of the fractal.
        /// </summary>
        public float Frequency { get; set; }

        /// <summary>
        /// The amplitude of the fractal.
        /// </summary>
        public float Amplitude { get; set; }

        /// <summary>
        /// The offset applied to each dimension.
        /// </summary>
        public Vector3 Offset { get; set; }

        /// <summary>
        /// The rate at which the amplitude changes.
        /// </summary>
        public float Lacunarity { get; set; }

        /// <summary>
        /// The rate at which the frequency changes.
        /// </summary>
        public float Gain { get; set; }

        /// <summary>
        /// The noises to sample from to generate the fractal.
        /// </summary>
        public INoise[] Noises { get; set; }

        /// <summary>
        /// The amplitudes for each octave.
        /// </summary>
        public float[] Amplitudes { get; set; }

        /// <summary>
        /// The frequencies for each octave.
        /// </summary>
        public float[] Frequencies { get; set; }
		
        public FractalNoise(INoise noise, int octaves, float frequency, float amplitude = 1.0f)
        {

            Octaves = octaves;
            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;
            Lacunarity = 2.0f;
            Gain = 0.5f;

            UpdateTable(new INoise[] { noise });
        }

        public FractalNoise(INoise[] noises, int octaves, float frequency, float amplitude = 1.0f)
        {

            Octaves = octaves;
            Frequency = frequency;
            Amplitude = amplitude;
            Offset = Vector3.Zero;
            Lacunarity = 2.0f;
            Gain = 0.5f;

            UpdateTable(noises);
        }

        /// <summary>
        /// Calculates the amplitudes and frequencies tables for each octave
        /// based on the fractal settings. The tables are used so individual 
        /// octaves can be sampled. Must be called when object is first created
        /// and when ever the settings are changed.
        /// </summary>
        public virtual void UpdateTable()
        {
            UpdateTable(Noises);
        }

        protected virtual void UpdateTable(INoise[] noises)
		{
			Amplitudes = new float[Octaves];
			Frequencies = new float[Octaves];
            Noises = new INoise[Octaves];

            int numNoises = noises.Length;
			
			float amp = 0.5f;
			float frq = Frequency;
			for(int i = 0; i < Octaves; i++)
			{
                Noises[i] = noises[System.Math.Min(i, numNoises - 1)];
				Frequencies[i] = frq;
				Amplitudes[i] = amp;
				amp *= Gain;
				frq *= Lacunarity;
			}

		}
		
        /// <summary>
        /// Returns the noise value from a octave in a 1D fractal.
        /// </summary>
        /// <param name="i">The octave to sample.</param>
        /// <param name="x">A value on the x axis.</param>
        /// <returns>A noise value between -Amp and Amp.</returns>
		public virtual float Octave1D(int i, float x)
		{
            if (i >= Octaves) return 0.0f;
            if (Noises[i] == null) return 0.0f;

			x = x + Offset.X;

			float frq = Frequencies[i];
			return Noises[i].Sample1D(x * frq) * Amplitudes[i] * Amplitude;
		}
		
        /// <summary>
        /// Returns the noise value from a octave in a 2D fractal.
        /// </summary>
        /// <param name="i">The octave to sample.</param>
        /// <param name="x">A value on the x axis.</param>
        /// <param name="y">A value on the y axis.</param>
        /// <returns>A noise value between -Amp and Amp.</returns>
		public virtual float Octave2D(int i, float x, float y)
		{
            if (i >= Octaves) return 0.0f;
            if (Noises[i] == null) return 0.0f;

			x = x + Offset.X;
			y = y + Offset.Y;

			float frq = Frequencies[i];
            return Noises[i].Sample2D(x * frq, y * frq) * Amplitudes[i] * Amplitude;
		}
		
        /// <summary>
        /// Returns the noise value from a octave in a 3D fractal.
        /// </summary>
        /// <param name="i">The octave to sample.</param>
        /// <param name="x">A value on the x axis.</param>
        /// <param name="y">A value on the y axis.</param>
        /// <param name="z">A value on the z axis.</param>
        /// <returns>A noise value between -Amp and Amp.</returns>
		public virtual float Octave3D(int i, float x, float y, float z)
		{
            if (i >= Octaves) return 0.0f;
            if (Noises[i] == null) return 0.0f;

			x = x + Offset.X;
			y = y + Offset.Y;
			z = z + Offset.Z;

			float frq = Frequencies[i];
            return Noises[i].Sample3D(x * frq, y * frq, z * frq) * Amplitudes[i] * Amplitude;
		}

        /// <summary>
        /// Samples a 1D fractal.
        /// </summary>
        /// <param name="x">A value on the x axis.</param>
        /// <returns>A noise value between -Amp and Amp.</returns>
        public virtual float Sample1D(float x)
        {
			x = x + Offset.X;

	        float sum = 0, frq;
			for(int i = 0; i < Octaves; i++) 
	        {	
				frq = Frequencies[i];

                if (Noises[i] != null)
                    sum += Noises[i].Sample1D(x * frq) * Amplitudes[i];
	        }
			return sum * Amplitude;
        }

        /// <summary>
        /// Samples a 2D fractal.
        /// </summary>
        /// <param name="x">A value on the x axis.</param>
        /// <param name="y">A value on the y axis.</param>
        /// <returns>A noise value between -Amp and Amp.</returns>
        public virtual float Sample2D(float x, float y)
        {
			x = x + Offset.X;
			y = y + Offset.Y;

	        float sum = 0, frq;
	        for(int i = 0; i < Octaves; i++) 
	        {
				frq = Frequencies[i];

                if (Noises[i] != null)
                    sum += Noises[i].Sample2D(x * frq, y * frq) * Amplitudes[i];
			}
			return sum * Amplitude;
        }

        /// <summary>
        /// Samples a 3D fractal.
        /// </summary>
        /// <param name="x">A value on the x axis.</param>
        /// <param name="y">A value on the y axis.</param>
        /// <param name="z">A value on the z axis.</param>
        /// <returns>A noise value between -Amp and Amp.</returns>
        public virtual float Sample3D(float x, float y, float z)
        {
			x = x + Offset.X;
			y = y + Offset.Y;
			z = z + Offset.Z;

	        float sum = 0, frq;
			for(int i = 0; i < Octaves; i++) 
	        {
				frq = Frequencies[i];

                if (Noises[i] != null)
                    sum += Noises[i].Sample3D(x * frq, y * frq, z * frq) * Amplitudes[i];
	        }
			return sum * Amplitude;
        }

        public void UpdateSeed(int seed)
        {
            throw new System.NotImplementedException();
        }
    }

    /// <summary>
    /// Interface for generating noise.
    /// </summary>
    public interface INoise 
    {

        /// <summary>
        /// The frequency of the fractal.
        /// </summary>
        float Frequency { get; set; }

        /// <summary>
        /// The amplitude of the fractal.
        /// </summary>
        float Amplitude { get; set; }

        /// <summary>
        /// The offset applied to each dimension.
        /// </summary>
        Vector3 Offset { get; set; }

        /// <summary>
        /// Sample the noise in 1 dimension.
        /// </summary>
        float Sample1D(float x);

        /// <summary>
        /// Sample the noise in 2 dimensions.
        /// </summary>
        float Sample2D(float x, float y);

        /// <summary>
        /// Sample the noise in 3 dimensions.
        /// </summary>
        float Sample3D(float x, float y, float z);

        /// <summary>
        /// Update the seed.
        /// </summary>
        void UpdateSeed(int seed);

    }

}












