using System.Diagnostics;
using System.Numerics;

namespace Valkyrie.Math
{
    public class BoundingBox3
    {
        public Vector3 Min;
        public Vector3 Max;

        public static BoundingBox3 CreateFromPoints(params Vector3[] points)
        {
            Debug.Assert(points != null);
            Debug.Assert(points.Length > 0);

            var min = new Vector3(points[0].X, points[0].Y, points[0].Z);
            var max = new Vector3(points[0].X, points[0].Y, points[0].Z);
            for (var index = 1; index < points.Length; index++)
            {
                var tmp = points[index];
                min = Vector3.Min(min, tmp);
                max = Vector3.Max(max, tmp);
            }

            return new BoundingBox3 { Max = max, Min = min };
        }

        public BoundingBox3 IntersectWith(BoundingBox3 area)
        {
            return new BoundingBox3 { Min = Vector3.Max(Min, area.Min), Max = Vector3.Min(Max, area.Max) };
        }

        public bool IsValid()
        {
            return (Max.X < Min.X || Max.Y < Min.Y || Max.Z < Min.Z) == false;
        }

        public static bool IsTheSame(BoundingBox3 first, BoundingBox3 second)
        {
            return IsTheSame(first.Min, second.Min) &&
                   IsTheSame(first.Max, second.Max);
        }

        public bool Contains(Vector3 coordinate)
        {
            return coordinate.IsBetween(Min, Max);
        }

        static bool IsTheSame(Vector3 v0, Vector3 v1)
        {
            return v0 == v1;
        }
    }
}