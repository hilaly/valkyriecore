﻿using System.Collections.Generic;
using System.Linq;

namespace Valkyrie.Math.Graphs
{
    public static class GraphsExtension
    {
        public static bool IsLoop<TNode, TEdge>(this IEdge<TNode, TEdge> edge)
        {
            object head = edge.Head;
            object tail = edge.Tail;
            return head == tail;
        }

        public static IEnumerable<IEdge<TNode, TEdge>> GetEdges<TNode, TEdge>(this IGraph<TNode, TEdge> graph,
            TNode node)
        {
            return graph.GetArrows().Where(u => (object)u.Head == (object)node);
        }
    }
}