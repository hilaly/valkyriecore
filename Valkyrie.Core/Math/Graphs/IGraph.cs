﻿using System.Collections.Generic;

namespace Valkyrie.Math.Graphs
{
    public interface IGraph<TNode, TEdge>
    {
        #region Nodes

        IEnumerable<TNode> Nodes { get; }
        int AddNode(TNode node);
        TNode GetNode(int id);

        #endregion

        #region Edges

        IEnumerable<IEdge<TNode, TEdge>> Edges { get; }
        IEdge<TNode, TEdge> GetEdge(int id);
        int Connect(TNode first, TNode second, TEdge edge, float length);

        #endregion

        #region Methods

        IEnumerable<IEdge<TNode, TEdge>> GetArrows();

        #endregion
    }
}
