﻿namespace Valkyrie.Math.Graphs
{
    class GenericEdge<TNode, TEdge> : IEdge<TNode, TEdge>
    {
        public TEdge Instance { get; private set; }

        public TNode Head { get; private set; }

        public TNode Tail { get; private set; }

        public float Length { get; private set; }

        public GenericEdge(TEdge instance, TNode head, TNode tail, float length)
        {
            Instance = instance;
            Head = head;
            Tail = tail;
            Length = length;
        }
    }
}