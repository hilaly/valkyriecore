﻿using System.Collections.Generic;

namespace Valkyrie.Math.Graphs
{
    public class GenericGraph<TNode, TEdge> : IGraph<TNode, TEdge>
    {
        #region private fields

        private readonly IIdProvider<long> _idProvider;

        readonly Dictionary<int, TNode> _nodes = new Dictionary<int, TNode>();
        readonly Dictionary<int, IEdge<TNode, TEdge>> _edges = new Dictionary<int, IEdge<TNode, TEdge>>();

        #endregion

        public GenericGraph(IIdProvider<long> idProvider)
        {
            _idProvider = idProvider;
        }

        #region Public properties

        public IEnumerable<TNode> Nodes
        {
            get { return _nodes.Values; }
        }

        public IEnumerable<IEdge<TNode, TEdge>> Edges
        {
            get { return _edges.Values; }
        }

        #endregion

        #region public methods

        public int AddNode(TNode node)
        {
            var newId = (int)_idProvider.Generate();
            _nodes.Add(newId, node);
            return newId;
        }

        public TNode GetNode(int id)
        {
            TNode result;
            return _nodes.TryGetValue(id, out result) ? result : default(TNode);
        }

        public IEdge<TNode, TEdge> GetEdge(int id)
        {
            IEdge<TNode, TEdge> result;
            return _edges.TryGetValue(id, out result) ? result : null;
        }

        public int Connect(TNode first, TNode second, TEdge edge, float length)
        {
            var newId = (int)_idProvider.Generate();
            IEdge<TNode, TEdge> newEdge = new GenericEdge<TNode, TEdge>(edge, first, second, length);
            _edges.Add(newId, newEdge);
            return newId;
        }

        public IEnumerable<IEdge<TNode, TEdge>> GetArrows()
        {
            return _edges.Values;
        }

        #endregion
    }
}