﻿namespace Valkyrie.Math.Graphs
{
    public interface IEdge<out TNode, out TEdge>
    {
        TEdge Instance { get; }

        TNode Head { get; }
        TNode Tail { get; }

        float Length { get; }
    }
}