namespace Valkyrie.Math
{
    public interface IRandom
    {
        float Range(float min, float max);
        float Next();
        int Range(int min, int max);
        float NormalDistribution();
    }
}