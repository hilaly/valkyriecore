﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Valkyrie.Math.Splines
{
    public class Bezier
    {
        #region Compute static functions

        static float Factorial(int n)
        {
            return n <= 1 ? 1f : n * Factorial(n - 1);
        }

        #endregion

        #region ComputeFunctions

        float BezierBasis(int index, int totalCount, float t)
        {
            return _factorials[totalCount]/(_factorials[index]*_factorials[totalCount - index])*
                   (float) (System.Math.Pow(t, index)*System.Math.Pow(1 - t, totalCount - index));
        }

        public Vector3 GetPoint(float t)
        {
            Vector3 result = Vector3.Zero;
            for (var index = 0; index < _points.Length; ++index)
            {
                var weight = BezierBasis(index, _points.Length - 1, t);
                result += _points[index] * weight;
            }
            return result;
        }

        public Vector3 GetTangent(float t)
        {
            Vector3 result = Vector3.Zero;
            for (var index = 0; index < _points.Length - 1; ++index)
            {
                var weight = BezierBasis(index, _points.Length - 2, t);
                result += (_points[index + 1] - _points[index]) * _points.Length * weight;
            }
            return result.Normalized();
        }

        #endregion

        #region private fields

        readonly Vector3[] _points;
        readonly float[] _factorials;

        #endregion

        public Bezier(IEnumerable<Vector3> points)
        {
            _points = points.ToArray();
            _factorials = new float[_points.Length];
            for (int i = 0; i < _points.Length; ++i)
            {
                _factorials[i] = Factorial(i);
            }

            //TODO: compute actual length
            ActualLength = SplineExtension.ComputeLength(GetPoint);
        }

        public float ActualLength { get; private set; }

        public void UpdateSpline(Vector3[] points)
        {
            for (int i = 0; i < _points.Length; ++i)
            {
                _points[i] = points[i];
            }

            //TODO: compute actual length
            ActualLength = SplineExtension.ComputeLength(GetPoint);
        }

#if UNITY_EDITOR

        public void DebugDraw(Color color)
        {
            Vector3 previous = GetPoint(0);
            for (float i = 0.01f; i <= 1f; i += 0.01f)
            {
                var current = GetPoint(i);
                Debug.DrawLine(previous, current, color);
                previous = current;
            }

            for (int i = 1; i < _points.Length; ++i)
            {
                Debug.DrawLine(_points[i - 1], _points[i], Color.red);
            }
        }

#endif
    }
}
