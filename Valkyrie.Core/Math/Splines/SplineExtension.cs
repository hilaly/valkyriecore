using System;
using System.Numerics;

namespace Valkyrie.Math.Splines
{
    public static class SplineExtension
    {
        #region Bezier

        public static float GetNearestPoint(this Bezier spline, Vector3 point, out Vector3 nearestPoint, out float lengthFromBegin)
        {
            var totalLength = 0f;
            lengthFromBegin = 0f;
            nearestPoint = point;
            //TODO: implement more fast way
            float minDistance = float.MaxValue;
            Vector3 lastPoint = spline.GetPoint(0);
            int count = (int)(spline.ActualLength * 10f);
            for (int i = 0; i <= count; ++i)
            {
                var currentPoint = spline.GetPoint((float)i / count);
                var distance = (currentPoint - point).LengthSquared();
                totalLength += (currentPoint - lastPoint).Length();
                lastPoint = currentPoint;

                if (distance < minDistance)
                {
                    lengthFromBegin = totalLength;
                    minDistance = distance;
                    nearestPoint = currentPoint;
                }
            }
            return (float)System.Math.Sqrt(minDistance);
        }

        #endregion
        
        public static float ComputeLength(Func<float, Vector3> function)
        {
            float result = 0f;
            var previous = function(0);
            for (float i = 0.01f; i <= 1f; i += 0.01f)
            {
                var current = function(i);
                result += (current - previous).Length();
                previous = current;
            }
            return result;
        }
    }
}