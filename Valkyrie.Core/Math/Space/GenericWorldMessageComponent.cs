﻿using System.Collections.Generic;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space
{
    public abstract class GenericWorldMessageComponent<T> : IEcsComponent
    {
        public readonly List<T> Messages;

        public GenericWorldMessageComponent(T message)
        {
            Messages = new List<T> {message};
        }
    }
}