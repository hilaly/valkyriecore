﻿using System;
using System.Numerics;


namespace Valkyrie.Math.Space.Management
{
    public interface IWorldBuilder
    {
        IWorldBuilder SetBounding(BoundingBox2 bounding);
        IWorldBuilder SetBounding(Vector2 point0, Vector2 point1, params Vector2[] points);
        
        IWorldBuilder SetRegionSize(Vector2 size);

        IWorldBuilder SetOnline(float tickPerSecond, Func<IWorld, string, IWorldObject> incomingRequestsHandler);

        IWorld Build();
        
        ushort AddObject(Vector2 position, object additionalData);
        bool RemoveObject(ushort uid);
    }
}