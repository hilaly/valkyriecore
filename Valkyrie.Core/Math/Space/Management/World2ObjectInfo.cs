﻿using System.Numerics;

namespace Valkyrie.Math.Space.Management
{
    class World2ObjectInfo
    {
        public ushort Uid;
        
        public Vector2 Position;

        public string AdditionalDataType;
        public string AdditionalData;
    }
}