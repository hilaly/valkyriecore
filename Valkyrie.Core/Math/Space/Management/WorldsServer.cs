using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Communication.Network;
using Valkyrie.Data;
using Valkyrie.Di;
using Valkyrie.Ecs;
using Valkyrie.Math.Space.Sync;
using Valkyrie.Math.Space.Sync.Components;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Math.Space.Management
{
    class ClientWorldCreator : IWorldCreator, IInnerWorldCreator
    {
        private World2 _worldImpl;

        #region IWorldCreator

        public IWorldBuilder CreateWorld(string worldUid, IContainer scope)
        {
            if (_worldImpl != null)
                throw new Exception("World already created");

            return new WorldBuilder(scope, worldUid, this);
        }

        public IPromise<IWorld> ConnectOnlineWorld(INetworkChannel networkChannel, string worldUid, string optionalArgs,
            IContainer scope)
        {
            if (_worldImpl != null)
                throw new Exception("World already created");

            var result = new Promise<IWorld>();

            var client = new WorldSyncService.Client(networkChannel);
            client.ConnectOnlineWorld(new ConnectWorldRequest
                {
                    worldUid = worldUid
                })
                .Done(response =>
                {
                    if (_worldImpl != null)
                        throw new Exception("World already created");

                    _worldImpl = CreateWorld(scope, response.worldData, true);
                    var componentsMap = response.components.OrderBy(u => u.index).Select(u =>
                        typeof(object).GetAllSubTypes(_ => true).Find(t => t.FullName == u.type)).ToArray();
                    ((OnlineWorld2) _worldImpl).SyncWith(client, response.token, optionalArgs, componentsMap);

                    //                _worldImpl.ReplicaFrom(networkChannel, response.worldHash);
                    //                _worldImpl.SimulationTick = ComputeTick(response.tick, networkChannel,
                    //                    response.worldData.SimulationTime);
                    //                _worldImpl.ServerTick = response.tick;

                    result.Resolve(_worldImpl);
                }, exception =>
                {
                    scope.Resolve<ILogService>().GetLog("Valkyrie.ClientWorldCreator")
                        .Error($"Connecting to world {worldUid}: {exception}");
                    result.Reject(exception);
                });

            return result;
        }

        #endregion

        #region IInnerWorldCreator

        public World2 CreateWorld(IContainer scope, World2Data data)
        {
            return CreateWorld(scope, data, false);
        }

        World2 CreateWorld(IContainer scope, World2Data data, bool online)
        {
            scope.Resolve<ILogService>().GetLog("Valkyrie.ClientWorldCreator").Info($"Creating world {data.WorldName}");
            var ctx = scope.Resolve<IEcsContext>();
            _worldImpl = online ? scope.Resolve<OnlineWorld2>(data, ctx) : scope.Resolve<World2>(data, ctx);
            ctx.Start();
            return _worldImpl;
        }

        #endregion
    }

    class WorldSyncServerImpl : WorldSyncService.ServerBase
    {
        private readonly IWorldsManager _worldsManager;
        private ILogInstance _log;

        public WorldSyncServerImpl(IWorldsManager worldsManager, ILogService logService)
        {
            _worldsManager = worldsManager;
            _log = logService.GetLog("Valkyrie.WorldSyncServerImpl");
        }

        public override IPromise<ConnectWorldResponse> ConnectOnlineWorld(ConnectWorldRequest request,
            ServerCallContext context)
        {
            var world = (World2) _worldsManager.GetWorld(request.worldUid);
            if (world == null)
                return Promise<ConnectWorldResponse>.Rejected(new RpcException(RpcErrorCode.FailedPrecondition,
                    $"World {request.worldUid} not founded"));

            if (world.WorldData.ConnectionHandler == null)
                return Promise<ConnectWorldResponse>.Rejected(new RpcException(RpcErrorCode.FailedPrecondition,
                    $"Connection to world {request.worldUid} is rejected"));

            return Promise<ConnectWorldResponse>.Resolved(new ConnectWorldResponse
            {
                token = world.WorldData.WorldName,
                worldData = world.WorldData,
                components = EcsExtensions.GetComponentsMapping()
                    .Select(u => new IndexMap() {index = (byte) u.Value, type = u.Key.FullName}).ToList()
            });
        }

        public override void SyncOnlineWorld(IObservable<WorldSyncClientDto> requestsStream,
            IObserver<WorldSyncServerDto> responsesStream, ServerCallContext context)
        {
            IWorldObject clientObject = null;
            requestsStream.Subscribe(clientDto =>
            {
                if (clientDto.syncCommand != null)
                    clientDto.syncCommand = clientDto.syncCommand;
                if (clientDto.init != null)
                    clientDto.init = clientDto.init;

                switch (clientDto.commandCase)
                {
                    case WorldSyncClientDto.commandCaseEnum.None:
                        break;
                    case WorldSyncClientDto.commandCaseEnum.init:
                    {
                        var worldName = clientDto.init.token;
                        var args = clientDto.init.args;
                        var world = (World2) _worldsManager.GetWorld(worldName);
                        if (world == null)
                        {
                            responsesStream.OnError(new RpcException(RpcErrorCode.FailedPrecondition,
                                $"World {worldName} not founded"));
                            return;
                        }

                        if (world.WorldData.ConnectionHandler == null)
                        {
                            responsesStream.OnError(new RpcException(RpcErrorCode.FailedPrecondition,
                                $"Connection to world {worldName} is rejected"));
                            return;
                        }

                        clientObject = world.WorldData.IncomingConnectionHandler(world, args);
                        if (clientObject != null)
                        {
                            clientObject.SetAvatar(responsesStream, world.SimulationTick).SetOnline();
                            _log.Info($"Avatar created on {clientObject.GetUid()} at {world.SimulationTick}, {clientObject.EcsEntity.Get<SyncObserverComponent>().LastConfirmedTick}");
                            clientObject.EcsEntity.Add(new DisconnectionComponent(responsesStream));
                        }
                        else
                        {
                            responsesStream.OnError(new RpcException(RpcErrorCode.FailedPrecondition,
                                $"Connection to world {worldName} is rejected"));
                        }

                        return;
                    }
                    case WorldSyncClientDto.commandCaseEnum.syncCommand:
                    {
                        var observer = clientObject?.EcsEntity.Get<SyncObserverComponent>();
                        if (observer != null)
                        {
                            observer.Handle(clientDto.syncCommand);
                        }
                    }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }, exception => { }, () => { });
        }
    }

    public interface IWorldsManager
    {
        IWorldBuilder CreateWorld(string worldUid, IContainer scope);

        IWorld GetWorld(string uid);

        IPromise Start(params ServerPort[] ports);
        IEnumerable<ServiceDefinition> Services { get; }
    }

    public class WorldsServer : IWorldsManager, IInnerWorldCreator, IDisposable
    {
        private readonly Server _server;
        private readonly CompositeDisposable _compositeDisposable;
        private readonly Dictionary<string, World2> _worlds = new Dictionary<string, World2>();

        public WorldsServer(INetwork network, ILogService logService)
        {
            _server = new Server(network, logService)
            {
                Services = new[] {WorldSyncService.BindService(new WorldSyncServerImpl(this, logService))}
            };

            _compositeDisposable = new CompositeDisposable {_server};
        }

        #region IWorldsManager

        public IWorldBuilder CreateWorld(string worldUid, IContainer scope)
        {
            return new WorldBuilder(scope, worldUid, this);
        }

        public IWorld GetWorld(string uid)
        {
            lock (_worlds)
                return _worlds.TryGetValue(uid, out var world2) ? world2 : null;
        }

        public IEnumerable<ServiceDefinition> Services => _server.Services;

        public IPromise Start(params ServerPort[] ports)
        {
            _server.Ports = ports;
            return _server.Start();
        }

        #endregion

        #region IInnerWorldCreator

        public World2 CreateWorld(IContainer scope, World2Data data)
        {
            scope.Resolve<ILogService>().GetLog("Valkyrie.WorldsServer").Info($"Creating world {data.WorldName}");
            var ctx = scope.Resolve<IEcsContext>();
            var result = scope.Resolve<World2>(data, ctx);
            lock (_worlds)
                _worlds.Add(data.WorldName, result);
            _compositeDisposable.Add(result.ObserveDispose().Chain(() =>
            {
                lock (_worlds) _worlds.Remove(data.WorldName);
            }));
            ctx.Start();
            return result;
        }

        #endregion

        public void Dispose()
        {
            _compositeDisposable.Dispose();
        }
    }
}