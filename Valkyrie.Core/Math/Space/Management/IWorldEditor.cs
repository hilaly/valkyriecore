﻿using System;
using System.IO;
using System.Numerics;
using Valkyrie.Communication.Network;

namespace Valkyrie.Math.Space.Management
{
    internal interface IWorldEditor
    {
        World2Data CreateWorld(string worldName, BoundingBox2 bounding);
        World2Data SetRegionSize(World2Data data, Vector2 size);
        string AddObject(World2Data data, Vector2 position, object additionalData);
        bool RemoveObject(World2Data data, string uid);

        void Serialize(World2Data data, Stream stream);
        World2Data Deserialize(Stream stream);

        IObservable<IWorld> ConnectWorld(INetworkChannel networkChannel, string worldName);
    }
}