﻿using Valkyrie.Di;

namespace Valkyrie.Math.Space.Management
{
    interface IInnerWorldCreator
    {
        World2 CreateWorld(IContainer scope, World2Data data);
    }
}