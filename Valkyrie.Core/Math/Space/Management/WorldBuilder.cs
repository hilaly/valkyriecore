﻿using System;
using System.Numerics;
using Valkyrie.Data;
using Valkyrie.Di;

namespace Valkyrie.Math.Space.Management
{
    class WorldBuilder : IWorldBuilder
    {
        private readonly IContainer _scope;
        private readonly IInnerWorldCreator _creator;
        private readonly World2Data _data;
        private readonly IIdProvider<ushort> _idGenerator;

        public WorldBuilder(IContainer scope, string worldUid, IInnerWorldCreator creator)
        {
            _scope = scope;
            _creator = creator;
            _data = new World2Data {WorldName = worldUid};
            _idGenerator = new UShortIdProvider();
        }

        public IWorldBuilder SetBounding(BoundingBox2 bounding)
        {
            _data.Bounding = bounding;
            return this;
        }

        public IWorldBuilder SetBounding(Vector2 point0, Vector2 point1, params Vector2[] points)
        {
            if (points.Length <= 0)
                return SetBounding(BoundingBox2.CreateFromPoints(point0, point1));
            
            var temp = new Vector2[points.Length + 2];
            for (var i = 0; i < points.Length; ++i)
                temp[i] = points[i];
            temp[points.Length] = point0;
            temp[points.Length + 1] = point1;
            return SetBounding(BoundingBox2.CreateFromPoints(temp));

        }

        public IWorldBuilder SetRegionSize(Vector2 size)
        {
            _data.RegionSize = size;
            return this;
        }

        public IWorldBuilder SetOnline(float tickPerSecond,
            Func<IWorld, string, IWorldObject> incomingRequestsHandler)
        {
            _data.SimulationTime = 1f / tickPerSecond;
            _data.ConnectionHandler = incomingRequestsHandler;
            return this;
        }

        public IWorld Build()
        {
            return _creator.CreateWorld(_scope, _data);
        }
        
        public ushort AddObject(Vector2 position, object additionalData)
        {
            var result = new World2ObjectInfo
            {
                Uid = _idGenerator.Generate(),
                Position = position,
                AdditionalDataType = additionalData?.GetType().FullName,
                AdditionalData = additionalData?.ToJson()
            };
            _data.AddObject(result);
            return result.Uid;
        }

        public bool RemoveObject(ushort uid)
        {
            return _data.RemoveObject(uid);
        }

    }
}