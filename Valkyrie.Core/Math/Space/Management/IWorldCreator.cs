﻿using Valkyrie.Communication.Network;
using Valkyrie.Di;
using Valkyrie.NewVersion.Rsg;

namespace Valkyrie.Math.Space.Management
{
    public interface IWorldCreator
    {
        IWorldBuilder CreateWorld(string worldUid, IContainer scope);
        IPromise<IWorld> ConnectOnlineWorld(INetworkChannel networkChannel, string worldUid, string optionalArgs, IContainer scope);
    }
}