﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;

namespace Valkyrie.Math.Space.Management
{
    [Contract]
    public class World2Data : IContract
    {
        private readonly List<World2ObjectInfo> _worldObjects = new List<World2ObjectInfo>();

        [ContractMember(0)] internal BoundingBox2 Bounding;
        [ContractMember(1)] internal Vector2 RegionSize;
        [ContractMember(2)] internal string WorldName;
        [ContractMember(3)] internal float SimulationTime;
        
        internal Func<IWorld, string, IWorldObject> ConnectionHandler;
        public bool IsOnline => ConnectionHandler != null;

        internal IWorldObject IncomingConnectionHandler(World2 world, string args)
        {
            return ConnectionHandler?.Invoke(world, args);
        }

        internal IEnumerable<World2ObjectInfo> GetObjects(BoundingBox2 box)
        {
            return _worldObjects.Where(o => box.Contains(o.Position));
        }

        internal void AddObject(World2ObjectInfo o)
        {
            _worldObjects.Add(o);
        }

        internal bool RemoveObject(ushort uid)
        {
            return _worldObjects.RemoveAll(u => u.Uid == uid) > 0;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(Bounding != null); Bounding?.Serialize(packer);
            packer.Write(RegionSize);
            packer.Write(WorldName);
            packer.Write(SimulationTime);
        }

        public void Deserialize(IBitsPacker packer)
        {
            if (packer.ReadBool())
                (Bounding ?? (Bounding = new BoundingBox2())).Deserialize(packer);
            else
                Bounding = null;

            RegionSize = packer.ReadVector2();
            WorldName = packer.ReadString();
            SimulationTime = packer.ReadFloat();
        }
    }
}