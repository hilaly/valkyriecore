﻿using System;
using System.Numerics;

namespace Valkyrie.Math.Space
{
    public interface IObserver : IDisposable
    {
        void SetViewDistance(Vector2 enter, Vector2 exit);

        IObservable<IWorldObject> ObserveObjectFind();
        IObservable<IWorldObject> ObserveObjectLost();

        IObservable<WorldMessageInstance> ObserveEvents();
    }
}