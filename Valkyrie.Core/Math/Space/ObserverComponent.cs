﻿using System.Collections.Generic;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space
{
    class ObserverFindObjectComponent : IEcsComponent
    {
        public List<World2ObjectComponent> Objects = new List<World2ObjectComponent>();
    }
    class ObserverLostObjectComponent : IEcsComponent
    {
        public List<World2ObjectComponent> Objects = new List<World2ObjectComponent>();
    }
    
    class ObserverComponent : IEcsComponent
    {
        internal readonly List<Observer2> Owners = new List<Observer2>();
        internal readonly HashSet<World2ObjectComponent> FoundedObjects = new HashSet<World2ObjectComponent>();
        
        readonly Dictionary<World2ObjectComponent, int> _counters = new Dictionary<World2ObjectComponent, int>();

        internal bool Add(World2ObjectComponent o)
        {
            if (!_counters.TryGetValue(o, out var count))
            {
                _counters.Add(o, 1);
                FoundedObjects.Add(o);
                return true;
            }

            _counters[o] = count + 1;
            return false;
        }

        internal bool Remove(World2ObjectComponent o)
        {
            var count = _counters[o];
            if (count == 1)
            {
                _counters.Remove(o);
                FoundedObjects.Remove(o);
                return true;
            }

            _counters[o] = count - 1;
            return false;
        }
    }
}