﻿namespace Valkyrie.Math.Space
{
    public class AllMessagesComponent : GenericWorldMessageComponent<WorldMessageInstance>
    {
        public AllMessagesComponent(WorldMessageInstance message) : base(message)
        {
        }
    }
}