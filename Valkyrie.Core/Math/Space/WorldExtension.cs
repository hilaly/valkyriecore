﻿using System;
using Valkyrie.Ecs.Entities;
using Valkyrie.Math.Space.Sync;
using Valkyrie.Math.Space.Sync.Components;

namespace Valkyrie.Math.Space
{
    public static class WorldExtension
    {
        public static ushort GetUid(this IWorldObject worldObject)
        {
            return worldObject.EcsEntity.Id;
        }

        public static bool IsControlled(this IEcsEntity e)
        {
            return e.Get<IsControlledComponent>() != null;
        }

        public static IWorldObject SetOnline(this IWorldObject o)
        {
            var entity = o.EcsEntity;
            if (entity.Get<SyncDataStoreComponent>() == null)
                entity.Add(new SyncDataStoreComponent());
            return o;
        }

        public static IObserver<WorldSyncServerDto> GetControllerChannel(this IWorldObject o)
        {
            return o.EcsEntity.Get<SyncObserverComponent>()?.OutStream;
        }

        public static IWorldObject SetAvatar(this IWorldObject o, IObserver<WorldSyncServerDto> channel,
            int worldSimulationTick)
        {
            var entity = o.EcsEntity;
            entity.Add(new SyncObserverComponent(channel, worldSimulationTick - 1));
            return o;
        }

        public static IWorldObject DestroyAvatar(this IWorldObject o)
        {
            var entity = o.EcsEntity;
            entity.Remove<SyncObserverComponent>();
            //TODO: send lost control message
            //entity.Remove<SyncDataHandleComponent>();
            return o;
        }

        /// <summary>
        /// Very slow method, only for test purpose
        /// </summary>
        /// <param name="world"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IWorldObject FindObject(this IWorld world, ushort id)
        {
            var w2 = (World2) world;
            return w2.FindObject(id);
        }

        public static IWorld GetWorld(this IEcsEntity e)
        {
            return e.Get<WorldComponent>()?.World;
        }

        public static IWorldObject GetWorldObject(this IEcsEntity e)
        {
            return e.Get<World2ObjectComponent>();
        }
    }
}