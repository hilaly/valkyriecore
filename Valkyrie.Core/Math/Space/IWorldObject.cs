﻿using System.Numerics;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space
{
    public interface IWorldObject
    {
        IEcsEntity EcsEntity { get; }
        
        Vector2 Position { get; set; }

        void Destroy();
    }
}