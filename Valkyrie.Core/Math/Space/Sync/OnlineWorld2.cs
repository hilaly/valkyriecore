using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Communication.Network;
using Valkyrie.Data.Serialization;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Engines;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Groups;
using Valkyrie.Math.Space.Management;
using Valkyrie.Network;
using Valkyrie.Rpc;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Math.Space.Sync
{
    #region Entities
    
    [Flags]
    public enum EntityType : byte
    {
        Other = 0,
        Server = 1 << 0,
        Controller = 1 << 1,
    }
    
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class SyncAttribute : Attribute
    {
        /// <summary>
        /// Dont use this field
        /// </summary>
        public EntityType SyncFrom { get; }

        /// <summary>
        /// Dont use this field
        /// </summary>
        public EntityType DataPrefer { get; set; } = EntityType.Server;
        /// <summary>
        /// Dont use this field now
        /// </summary>
        public byte SyncPeriod { get; set; } = 1;
        public bool DontDestroyOnNetwork { get; set; }
        /// <summary>
        /// Only for controllers
        /// </summary>
        public bool AutocreateOnController { get; set; }

        public SyncAttribute(EntityType whoSends)
        {
            SyncFrom = whoSends;
        }
    }
    
    public class IsControlledComponent : IEcsComponent
    {
    }

    /// <summary>
    /// Holds type of entity in current world
    /// </summary>
    class EntityTypeComponent : IEcsComponent
    {
        public EntityType Type;
    }

    class SyncDataStoreComponent : IEcsComponent
    {
        private readonly List<KeyValuePair<int, EntityState>> _states = new List<KeyValuePair<int, EntityState>>();

        public int HistoryCount { get; set; } = 10;

        public EntityState AllocateState(int tick)
        {
            var minIndex = -1;
            var minValue = int.MaxValue;
            
            for (var index = 0; index < _states.Count; index++)
            {
                var pair = _states[index];
                if (pair.Key == tick)
                {
                    pair.Value.components.Clear();
                    return pair.Value;
                }
                if (minValue > pair.Key)
                {
                    minValue = pair.Key;
                    minIndex = index;
                }
            }

            if (_states.Count < HistoryCount)
            {
                var result = new KeyValuePair<int, EntityState>(tick, new EntityState());
                _states.Add(result);
                return result.Value;
            }

            _states[minIndex] = new KeyValuePair<int, EntityState>(tick, _states[minIndex].Value);
            _states[minIndex].Value.components.Clear();
            return _states[minIndex].Value;
        }

        public EntityState GetStoredState(int tick)
        {
            foreach (var pair in _states)
            {
                if (pair.Key == tick)
                    return pair.Value;
            }

            return null;
        }
    }
    
    class DisconnectionComponent : IEcsComponent, IDisposable
    {
        private readonly IObserver<WorldSyncServerDto> _channel;

        public readonly CompositeDisposable Disposable = new CompositeDisposable();

        public DisconnectionComponent(IObserver<WorldSyncServerDto> channel)
        {
            _channel = channel;
        }

        public void Dispose()
        {
            _channel.OnCompleted();
            Disposable.Dispose();
        }
    }
    #endregion
    
    #region Engines
    
    abstract class WorldStateSyncObject : IEcsExecuteEngine
    {
        public EnginesRoot Root { get; set; }

        protected readonly ILogInstance _log;
        private float _timeSinceSync;

        public WorldStateSyncObject(ILogService log)
        {
            _log = log.GetLog("Valkyrie.WorldStateSync");
        }

        public int TicksInPacket = 7;
        public int PacketsPerSecond = 10;

        public int SimulationsPerSecond => Root.IterationsPerSecond;
        public int StateHistory => SimulationsPerSecond;

        protected int SimulationTick
        {
            get => Root.Tick;
            set => Root.Tick = value;
        }

        protected int LocalTick
        {
            get => Root.LocalTick;
            set => Root.LocalTick = value;
        }
        protected int ServerTick
        {
            get => Root.ServerTick;
            set => Root.ServerTick = value;
        }

        public string LogTime() => $"Sim={SimulationTick} Local={LocalTick} Server={ServerTick}";

        protected void LogInfo(string msg)
        {
            _log.Info($"{LogTime()} {msg}");
        }
        
        public void Execute(Iteration iterationInfo)
        {
            _timeSinceSync += iterationInfo.DeltaTime;
            var syncTime = 1f / PacketsPerSecond;
            if(syncTime > _timeSinceSync)
                return;

            _timeSinceSync = 0;

            SyncImpl(iterationInfo);
        }

        protected abstract void SyncImpl(Iteration iterationInfo);
    }

    class CollectSyncDataClientEngine : WorldSyncExecuteQueryEngine
    {
        private readonly IContractSerializer _contractSerializer;

        public CollectSyncDataClientEngine(IEcsEntitiesDatabase entitiesDatabase, IContractSerializer contractSerializer, ILogService ls) : base(entitiesDatabase, ls)
        {
            _contractSerializer = contractSerializer;
        }
        
        public OnlineWorld2 World { get; set; }

        protected override GroupBuilder GetBuilder()
        {
            return GroupBuilder.AllOf<IsControlledComponent>();
        }

        protected override void Execute(Iteration tickInfo, IList<IEcsEntity> entities)
        {
            foreach (var e in entities)
            {
                var tick = tickInfo.LocalTick;
                
                //var msg = ($"CLIENT {e.Id}: Tick {tick} SimTick {tickInfo.Tick}");
                
                var data = e.GetOrCreate<SyncDataStoreComponent>().AllocateState(tick);
                data.id = e.Id;

                for (var i = 0; i < EcsExtensions.ControllerSendsComponents.Count; i++)
                {
                    var componentIndex = EcsExtensions.ControllerSendsComponents[i];
                    var source = ((EcsEntity) e).Components[componentIndex];
                    if (source != null)
                    {
                        data.components.Add(new ComponentInfo
                        {
                            index = (byte) World.ComponentsMap.IndexOf(source.GetType()),
                            buffer = _contractSerializer.Serialize(source, false)
                        });

                        //msg += $" SAVE {source} {componentIndex}";
                    }
                    //else
                    //msg += $" SKIP {componentIndex}";
                }
                
                //Log.Debug(msg);
            }
        }
    }

    class SendSyncDataClientEngine : WorldStateSyncObject
    {
        private readonly IGroupHolder _controlledGroup;
        private readonly ApplySyncDataClientEngine _applyEngine;
        private int _lastReceivedTick;
        private int _lastConfirmedTick;
        
        public IClientWriteStream<WorldSyncClientDto> OutStream { get; set; }
        
        public SendSyncDataClientEngine(ILogService log, IGroupHolder controlledGroup, ApplySyncDataClientEngine applyEngine) : base(log)
        {
            _controlledGroup = controlledGroup;
            _applyEngine = applyEngine;
            _controlledGroup.Setup(GroupBuilder.AllOf<IsControlledComponent, SyncDataStoreComponent>());
        }

        protected override void SyncImpl(Iteration iterationInfo)
        {
            if(OutStream == null)
                return;
            
            var lastTick = iterationInfo.LocalTick;
            var firstTick = lastTick - TicksInPacket + 1;
            if (_lastConfirmedTick >= firstTick)
                firstTick = _lastConfirmedTick + 1;

            var totalTicksToSend = lastTick - firstTick + 1;
            if(totalTicksToSend <= 0)
                return;
            
            //LogInfo($"Will sync ticks {firstTick}-{lastTick} at {DateTime.UtcNow.Second}:{DateTime.UtcNow.Millisecond}");

            var msg = new WorldSyncClientDto.ClientSyncDto {lastReceivedTick = _lastReceivedTick};

            for (var tick = firstTick; tick <= lastTick; ++tick)
            {
                var snapshot = new WorldSyncClientDto.ClientSyncDto.ClientSnapshot { tick = tick };

                foreach (var e in _controlledGroup.GetEntities())
                {
                    var state = e.Get<SyncDataStoreComponent>().GetStoredState(tick);
                    if(state != null)
                        snapshot.states.Add(state);
                }

                //TODO: think, does we need to send empty snapshots
                msg.snapshots.Add(snapshot);
            }

            OutStream.Write(new WorldSyncClientDto {syncCommand = msg});
        }

        public void HandleServerState(WorldSyncServerDto.ServerSyncDto state)
        {
            _lastConfirmedTick = state.lastReceivedTick;
            foreach (var snapshot in state.snapshots)
            {
                if (snapshot.tick <= _lastReceivedTick)
                    continue;
                
                _lastReceivedTick = snapshot.tick;
                _applyEngine.PushSnapshot(snapshot);
            }

            //LogInfo($"Confirmed={_lastConfirmedTick}, Received={_lastReceivedTick}");
            UpdateLocalTick();
        }

        private void UpdateLocalTick()
        {
            var ticksPerPacket = SimulationsPerSecond / PacketsPerSecond;
            var ticksToAdd = (int)System.Math.Ceiling(Channel.Ping * 2 * SimulationsPerSecond) +
                             2 * ticksPerPacket + 5;
            var newLocalTick = ServerTick + ticksToAdd;
            if (LocalTick != newLocalTick)
            {
                //LogInfo($"Update LocalTick to {newLocalTick}");
                LocalTick = newLocalTick;
            }

            var newSimulationTick = _lastReceivedTick - ticksPerPacket;
            if (SimulationTick != newSimulationTick)
            {
                //LogInfo($"Update SimTick to {newSimulationTick}");
                SimulationTick = newSimulationTick;
            }
        }

        public IClient Channel => OutStream.Channel;
    }
    
    class ApplySyncDataClientEngine : IEcsExecuteEngine
    {
        private readonly IEcsEntitiesDatabase _entitiesDatabase;
        private readonly IContractSerializer _contractSerializer;
        readonly List<int> _appliedIndices = new List<int>();
        readonly List<int> _toRemoveIndicesNonControlled;
        readonly List<int> _toRemoveIndicesControlled;

        private readonly List<WorldSyncServerDto.ServerSyncDto.ServerSnapshot> _snapshots =
            new List<WorldSyncServerDto.ServerSyncDto.ServerSnapshot>();

        private int _lastAppliedTick = -1;
        private readonly ILogInstance _log;

        public ApplySyncDataClientEngine(IEcsEntitiesDatabase entitiesDatabase, IContractSerializer contractSerializer,
            ILogService ls)
        {
            _entitiesDatabase = entitiesDatabase;
            _contractSerializer = contractSerializer;
            _log = ls.GetLog("Valkyrie.ClientApplyEngine");

            EcsExtensions.GetComponentsCount();
            _toRemoveIndicesNonControlled = new List<int>(EcsExtensions.ServerSendsComponents.Where(u =>
                !EcsExtensions.DontDestroyNetworkComponents.Contains(u)));
            _toRemoveIndicesControlled =
                new List<int>(
                    _toRemoveIndicesNonControlled.Where(u => !EcsExtensions.ControllerSendsComponents.Contains(u)));
        }

        public OnlineWorld2 World { get; set; }

        public void Execute(Iteration iterationInfo)
        {
            var firstTick = _lastAppliedTick + 1;
            var endTick = iterationInfo.SimulationTick + 1;
            for (var i = firstTick; i < endTick; ++i)
            {
                var snapshot = GetSnapshot(i);
                if (snapshot != null)
                {
                    //_log.Info($"Client apply {i} tick");
                    RemoveLostEntities(snapshot);
                    CreateFoundedEntities(snapshot);
                    ApplyEntitiesStates(snapshot);
                    _lastAppliedTick = i;
                }
            }
        }

        private void RemoveLostEntities(WorldSyncServerDto.ServerSyncDto.ServerSnapshot snapshot)
        {
            if(snapshot.lost == null)
                return;

            foreach (var lostObjectId in snapshot.lost)
                _entitiesDatabase.GetEntity(lostObjectId)?.Get<World2ObjectComponent>()?.Destroy();
        }

        private void CreateFoundedEntities(WorldSyncServerDto.ServerSyncDto.ServerSnapshot snapshot)
        {
            if(snapshot.founded == null)
                return;
            
            foreach (var foundedData in snapshot.founded)
            {
                _log.Debug($"Create {foundedData.id} Controlled={foundedData.isControlled}");
                var no = World.FindObject(foundedData.id) ?? World.CreateObject(foundedData.id, foundedData.position);
                if (foundedData.isControlled)
                {
                    no.EcsEntity.Add(new IsControlledComponent());
                    //this._log.Info($"Controll added {no.GetUid()}");
                }
                else
                {
                    no.EcsEntity.Remove<IsControlledComponent>();
                    //this._log.Info($"Controll removed {no.GetUid()}");
                };
            }
        }

        private void ApplyEntitiesStates(WorldSyncServerDto.ServerSyncDto.ServerSnapshot snapshot)
        {
            if(snapshot.states == null)
                return;
            
            //_log.Debug($"Receive sync of {worldSyncEventInstance.Message.Tick} at {SimulationTick}");
            foreach (var syncEntityData in snapshot.states)
            {
                var e = (EcsEntity)_entitiesDatabase.GetEntity(syncEntityData.id);
                if (e == null)
                {
                    //_log.Info($"Skip apply {syncEntityData.id}");
                    continue;
                }
                
                var isControlled = e.Get<IsControlledComponent>() != null;
                
                foreach (var componentData in syncEntityData.components)
                {
                    var componentType = World.ComponentsMap[componentData.index];
                    var index = EcsExtensions.GetComponentIndex(componentType);
                    if (!isControlled || !EcsExtensions.ControllerSendsComponents.Contains(index))
                    {
                        if (e.Components[index] != null)
                        {
                            //msg += $" UPDATE {index} ({componentType.Name})";
                            _contractSerializer.Deserialize(componentData.buffer, componentType, e.Components[index]);
                        }
                        else
                        {
                            //msg += $" ADD {index} ({componentType.Name})";
                            e.Add((IEcsComponent) _contractSerializer.Deserialize(componentData.buffer, componentType, null), index);
                        }
                    }
                    //else
                    //msg += $" SKIP {index}";

                    _appliedIndices.Add(index);
                }

                var toRemoveList = isControlled ? _toRemoveIndicesControlled : _toRemoveIndicesNonControlled;
                foreach (var ownerSendsComponent in toRemoveList)
                {
                    if(_appliedIndices.Contains(ownerSendsComponent))
                        continue;
                    //msg += $" REM {ownerSendsComponent}";
                    e.Remove(ownerSendsComponent);
                }
                
                //Log.Debug(msg);
                _appliedIndices.Clear();
            }
        }
        
        WorldSyncServerDto.ServerSyncDto.ServerSnapshot GetSnapshot(int tick)
        {
            return _snapshots.Find(u => u.tick == tick);
        }
        
        public void PushSnapshot(WorldSyncServerDto.ServerSyncDto.ServerSnapshot snapshot)
        {
            _snapshots.Add(snapshot);
            _snapshots.Sort((o0, o1) => o0.tick - o1.tick);

            while (_snapshots.Count >= DefinedConstants.SyncBuffers.StoreSize)
                _snapshots.RemoveAt(0);
        }
    }

    #endregion
    
    class OnlineWorld2 : World2
    {
        private readonly SendSyncDataClientEngine _stateSyncObject;
        
        public List<Type> ComponentsMap { get; private set; }
        
        public OnlineWorld2(World2Data worldData, IEcsContext ecxCtx, ILogService logService,
            EnginesRoot enginesRoot)
            : base(worldData, ecxCtx, logService)
        {
            this._stateSyncObject = (SendSyncDataClientEngine) enginesRoot.Engines.First(u => u is SendSyncDataClientEngine);
            _stateSyncObject.Root = enginesRoot;

            ((ApplySyncDataClientEngine) enginesRoot.Engines.First(u => u is ApplySyncDataClientEngine)).World = this;
            ((CollectSyncDataClientEngine) enginesRoot.Engines.First(u => u is CollectSyncDataClientEngine)).World = this;
        }

        public void SyncWith(WorldSyncService.Client syncClient, string secret, string args, Type[] componentsMap)
        {
            ComponentsMap = componentsMap.ToList();
            
            _log.Info($"Sync with server world");
            
            var handler = syncClient.SyncOnlineWorld(new ClientCallOptions
                {Delivery = NetDeliveryMethod.Unreliable, RpcChannel = 0});
            
            _disposable.Add(handler.ResponsesStream.Subscribe(OnWorldSyncEvent, exception =>
            {
                _log.Error($"Online world {this.WorldData.WorldName}: {exception}");
                Dispose();
                throw exception;
            }, Dispose));
            
            _stateSyncObject.OutStream = handler.RequestsStream;
            
            handler.RequestsStream.Write(new WorldSyncClientDto
            { init = new WorldSyncClientDto.InitDto
            {
                token = secret,
                args = args
            }});
        }

        public override void Dispose()
        {
            _log.Info($"Online world disposed");
            base.Dispose();
        }

        private void OnWorldSyncEvent(WorldSyncServerDto msg)
        {
            if (msg.state != null)
                _stateSyncObject.HandleServerState(msg.state);
        }
    }
}