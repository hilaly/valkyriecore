// <auto-generated>
//  Generated by Valkyrie.ProtoParser
// <auto-generated>

using System;
using System.Collections.Generic;
using Valkyrie.Data.Serialization;
using Valkyrie.Communication.Network;
using Valkyrie.NewVersion.Rsg;
using Valkyrie.Threading.Async;
using Valkyrie.Rpc;
using static Valkyrie.Data.DataExtensions;

namespace Valkyrie.Math.Space.Sync
{
    public static class WorldSyncService
    {
        public sealed class Client : Valkyrie.Rpc.ClientBase
        {
            public Client(INetworkChannel channel)
                : base(channel)
            {
            }

            public IAsyncUnaryCall<ConnectWorldResponse> ConnectOnlineWorld(ConnectWorldRequest request)
            {
                return ConnectOnlineWorld(request, new ClientCallOptions());
            }

            public IAsyncUnaryCall<ConnectWorldResponse> ConnectOnlineWorld(ConnectWorldRequest request,
                ClientCallOptions callOptions)
            {
                return base.SingleToSingle<ConnectWorldRequest, ConnectWorldResponse>(
                    "E46C0228D4D5D2CA85261311F6F2D5F528E8F3D9", request, callOptions);
            }

            public IAsyncDuplexStreamingCall<WorldSyncClientDto, WorldSyncServerDto> SyncOnlineWorld()
            {
                return SyncOnlineWorld(new ClientCallOptions());
            }

            public IAsyncDuplexStreamingCall<WorldSyncClientDto, WorldSyncServerDto> SyncOnlineWorld(
                ClientCallOptions callOptions)
            {
                return base.ManyToMany<WorldSyncClientDto, WorldSyncServerDto>(
                    "55CD381205FAB4746924B2A8B41BB50CD87D9C44", callOptions);
            }
        }

        public class ServerBase
        {
            public virtual IPromise<ConnectWorldResponse> ConnectOnlineWorld(ConnectWorldRequest request,
                ServerCallContext context)
            {
                throw new NotImplementedException();
            }

            public virtual void SyncOnlineWorld(IObservable<WorldSyncClientDto> requestsStream,
                IObserver<WorldSyncServerDto> responsesStream, ServerCallContext context)
            {
                throw new NotImplementedException();
            }
        }

        public static ServiceDefinition BindService(ServerBase serviceBase)
        {
            return new ServiceDefinition()
                    .BindMethod("E46C0228D4D5D2CA85261311F6F2D5F528E8F3D9",
                        (Func<ConnectWorldRequest, ServerCallContext, IPromise<ConnectWorldResponse>>) serviceBase
                            .ConnectOnlineWorld)
                    .BindMethod("55CD381205FAB4746924B2A8B41BB50CD87D9C44",
                        (Action<IObservable<WorldSyncClientDto>,
                            IObserver<WorldSyncServerDto>, ServerCallContext>) serviceBase
                            .SyncOnlineWorld)
                ;
        }
    }

    [Contract]
    public sealed class IndexMap : IContract
    {
        [ContractMember(1)] byte _index;

        public byte index
        {
            get => _index;
            set => _index = value;
        }

        [ContractMember(2)] string _type;

        public string type
        {
            get => _type;
            set => _type = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(index);
            packer.Write(type);
        }

        public void Deserialize(IBitsPacker packer)
        {
            index = packer.ReadByte();
            type = packer.ReadString();
        }
    }

    [Contract]
    public sealed class ConnectWorldRequest : IContract
    {
        [ContractMember(1)] string _worldUid;

        public string worldUid
        {
            get => _worldUid;
            set => _worldUid = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(worldUid);
        }

        public void Deserialize(IBitsPacker packer)
        {
            worldUid = packer.ReadString();
        }
    }

    [Contract]
    public sealed class ConnectWorldResponse : IContract
    {
        [ContractMember(1)] string _token;

        public string token
        {
            get => _token;
            set => _token = value;
        }

        [ContractMember(2)] List<IndexMap> _components = new List<IndexMap>();

        public List<IndexMap> components
        {
            get => _components;
            set => _components = value;
        }

        [ContractMember(3)] Valkyrie.Math.Space.Management.World2Data _worldData;

        public Valkyrie.Math.Space.Management.World2Data worldData
        {
            get => _worldData;
            set => _worldData = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(token);
            packer.Write((short) (components?.Count ?? -1));
            if (components != null)
                for (var i = 0; i < components.Count; ++i)
                {
                    packer.Write(components[i] != null);
                    components[i]?.Serialize(packer);
                }

            packer.Write(worldData != null);
            worldData?.Serialize(packer); //Possible can cause compilation error
        }

        public void Deserialize(IBitsPacker packer)
        {
            token = packer.ReadString();
            var componentsCount = packer.ReadShort();
            if (componentsCount < 0)
                components = null;
            else if (components == null)
                components = new List<IndexMap>();
            for (short i = 0; i < componentsCount; ++i)
            {
                if (packer.ReadBool())
                {
                    var temp = new IndexMap();
                    temp.Deserialize(packer);
                    components.Add(temp);
                }
                else components.Add(null);
            }

            {
                if (packer.ReadBool())
                    (worldData ?? (worldData = new Valkyrie.Math.Space.Management.World2Data())).Deserialize(packer);
                else worldData = null;
            } //Possible error
        }
    }

    [Contract]
    public sealed class ComponentInfo : IContract
    {
        [ContractMember(1)] byte _index;

        public byte index
        {
            get => _index;
            set => _index = value;
        }

        [ContractMember(2)] byte[] _buffer;

        public byte[] buffer
        {
            get => _buffer;
            set => _buffer = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(index);
            packer.Write(buffer);
        }

        public void Deserialize(IBitsPacker packer)
        {
            index = packer.ReadByte();
            buffer = packer.ReadBytes();
        }
    }

    [Contract]
    public sealed class EntityState : IContract
    {
        [ContractMember(1)] ushort _id;

        public ushort id
        {
            get => _id;
            set => _id = value;
        }

        [ContractMember(2)] List<ComponentInfo> _components = new List<ComponentInfo>();

        public List<ComponentInfo> components
        {
            get => _components;
            set => _components = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(id);
            packer.Write((short) (components?.Count ?? -1));
            if (components != null)
                for (var i = 0; i < components.Count; ++i)
                {
                    packer.Write(components[i] != null);
                    components[i]?.Serialize(packer);
                }
        }

        public void Deserialize(IBitsPacker packer)
        {
            id = packer.ReadUShort();
            var componentsCount = packer.ReadShort();
            if (componentsCount < 0)
                components = null;
            else if (components == null)
                components = new List<ComponentInfo>();
            for (short i = 0; i < componentsCount; ++i)
            {
                if (packer.ReadBool())
                {
                    var temp = new ComponentInfo();
                    temp.Deserialize(packer);
                    components.Add(temp);
                }
                else components.Add(null);
            }
        }
    }

    [Contract]
    public sealed class NewEntityState : IContract
    {
        [ContractMember(1)] ushort _id;

        public ushort id
        {
            get => _id;
            set => _id = value;
        }

        [ContractMember(2)] System.Numerics.Vector2 _position;

        public System.Numerics.Vector2 position
        {
            get => _position;
            set => _position = value;
        }

        [ContractMember(3)] bool _isControlled;

        public bool isControlled
        {
            get => _isControlled;
            set => _isControlled = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(id);
            packer.Write(position);
            packer.Write(isControlled);
        }

        public void Deserialize(IBitsPacker packer)
        {
            id = packer.ReadUShort();
            position = packer.ReadVector2();
            isControlled = packer.ReadBool();
        }
    }

    [Contract]
    public sealed class WorldSyncClientDto : IContract
    {
        [Contract]
        public sealed class InitDto : IContract
        {
            [ContractMember(1)] string _token;

            public string token
            {
                get => _token;
                set => _token = value;
            }

            [ContractMember(2)] string _args;

            public string args
            {
                get => _args;
                set => _args = value;
            }

            public void Serialize(IBitsPacker packer)
            {
                packer.Write(token);
                packer.Write(args);
            }

            public void Deserialize(IBitsPacker packer)
            {
                token = packer.ReadString();
                args = packer.ReadString();
            }
        }

        [Contract]
        public sealed class ClientSyncDto : IContract
        {
            [Contract]
            public sealed class ClientSnapshot : IContract
            {
                [ContractMember(1)] int _tick;

                public int tick
                {
                    get => _tick;
                    set => _tick = value;
                }

                [ContractMember(2)] List<EntityState> _states = new List<EntityState>();

                public List<EntityState> states
                {
                    get => _states;
                    set => _states = value;
                }

                public void Serialize(IBitsPacker packer)
                {
                    packer.Write(tick);
                    packer.Write((short) (states?.Count ?? -1));
                    if (states != null)
                        for (var i = 0; i < states.Count; ++i)
                        {
                            packer.Write(states[i] != null);
                            states[i]?.Serialize(packer);
                        }
                }

                public void Deserialize(IBitsPacker packer)
                {
                    tick = packer.ReadInt();
                    var statesCount = packer.ReadShort();
                    if (statesCount < 0)
                        states = null;
                    else if (states == null)
                        states = new List<EntityState>();
                    for (short i = 0; i < statesCount; ++i)
                    {
                        if (packer.ReadBool())
                        {
                            var temp = new EntityState();
                            temp.Deserialize(packer);
                            states.Add(temp);
                        }
                        else states.Add(null);
                    }
                }
            }

            [ContractMember(1)] int _lastReceivedTick;

            public int lastReceivedTick
            {
                get => _lastReceivedTick;
                set => _lastReceivedTick = value;
            }

            [ContractMember(2)] List<ClientSnapshot> _snapshots = new List<ClientSnapshot>();

            public List<ClientSnapshot> snapshots
            {
                get => _snapshots;
                set => _snapshots = value;
            }

            public void Serialize(IBitsPacker packer)
            {
                packer.Write(lastReceivedTick);
                packer.Write((short) (snapshots?.Count ?? -1));
                if (snapshots != null)
                    for (var i = 0; i < snapshots.Count; ++i)
                    {
                        packer.Write(snapshots[i] != null);
                        snapshots[i]?.Serialize(packer);
                    }
            }

            public void Deserialize(IBitsPacker packer)
            {
                lastReceivedTick = packer.ReadInt();
                var snapshotsCount = packer.ReadShort();
                if (snapshotsCount < 0)
                    snapshots = null;
                else if (snapshots == null)
                    snapshots = new List<ClientSnapshot>();
                for (short i = 0; i < snapshotsCount; ++i)
                {
                    if (packer.ReadBool())
                    {
                        var temp = new ClientSnapshot();
                        temp.Deserialize(packer);
                        snapshots.Add(temp);
                    }
                    else snapshots.Add(null);
                }
            }
        }

        public enum commandCaseEnum
        {
            None = 0,
            init = 1,
            syncCommand = 2,
        }

        public commandCaseEnum commandCase { get; private set; } = commandCaseEnum.None;
        [ContractMember(1)] InitDto _init;

        public InitDto init
        {
            get => _init;
            set
            {
                _init = value;
                commandCase = commandCaseEnum.init;
            }
        }

        [ContractMember(2)] ClientSyncDto _syncCommand;

        public ClientSyncDto syncCommand
        {
            get => _syncCommand;
            set
            {
                _syncCommand = value;
                commandCase = commandCaseEnum.syncCommand;
            }
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write((byte) commandCase);
            switch (commandCase)
            {
                case commandCaseEnum.init:
                {
                    packer.Write(init != null);
                    init?.Serialize(packer);
                    break;
                }
                case commandCaseEnum.syncCommand:
                {
                    packer.Write(syncCommand != null);
                    syncCommand?.Serialize(packer);
                    break;
                }
            }
        }

        public void Deserialize(IBitsPacker packer)
        {
            switch ((commandCaseEnum) packer.ReadByte())
            {
                case commandCaseEnum.init:
                {
                    {
                        if (packer.ReadBool()) (init ?? (init = new InitDto())).Deserialize(packer);
                        else init = null;
                    }
                    break;
                }
                case commandCaseEnum.syncCommand:
                {
                    {
                        if (packer.ReadBool()) (syncCommand ?? (syncCommand = new ClientSyncDto())).Deserialize(packer);
                        else syncCommand = null;
                    }
                    break;
                }
            }
        }
    }

    [Contract]
    public sealed class WorldSyncServerDto : IContract
    {
        [Contract]
        public sealed class ServerSyncDto : IContract
        {
            [Contract]
            public sealed class ServerSnapshot : IContract
            {
                [ContractMember(1)] int _tick;

                public int tick
                {
                    get => _tick;
                    set => _tick = value;
                }

                [ContractMember(2)] List<EntityState> _states = new List<EntityState>();

                public List<EntityState> states
                {
                    get => _states;
                    set => _states = value;
                }

                [ContractMember(3)] List<NewEntityState> _founded = new List<NewEntityState>();

                public List<NewEntityState> founded
                {
                    get => _founded;
                    set => _founded = value;
                }

                [ContractMember(4)] List<ushort> _lost = new List<ushort>();

                public List<ushort> lost
                {
                    get => _lost;
                    set => _lost = value;
                }

                public void Serialize(IBitsPacker packer)
                {
                    packer.Write(tick);
                    packer.Write((short) (states?.Count ?? -1));
                    if (states != null)
                        for (var i = 0; i < states.Count; ++i)
                        {
                            packer.Write(states[i] != null);
                            states[i]?.Serialize(packer);
                        }

                    packer.Write((short) (founded?.Count ?? -1));
                    if (founded != null)
                        for (var i = 0; i < founded.Count; ++i)
                        {
                            packer.Write(founded[i] != null);
                            founded[i]?.Serialize(packer);
                        }

                    packer.Write((short) (lost?.Count ?? -1));
                    if (lost != null)
                        for (var i = 0; i < lost.Count; ++i)
                            packer.Write(lost[i]);
                }

                public void Deserialize(IBitsPacker packer)
                {
                    tick = packer.ReadInt();
                    var statesCount = packer.ReadShort();
                    if (statesCount < 0)
                        states = null;
                    else if (states == null)
                        states = new List<EntityState>();
                    for (short i = 0; i < statesCount; ++i)
                    {
                        if (packer.ReadBool())
                        {
                            var temp = new EntityState();
                            temp.Deserialize(packer);
                            states.Add(temp);
                        }
                        else states.Add(null);
                    }

                    var foundedCount = packer.ReadShort();
                    if (foundedCount < 0)
                        founded = null;
                    else if (founded == null)
                        founded = new List<NewEntityState>();
                    for (short i = 0; i < foundedCount; ++i)
                    {
                        if (packer.ReadBool())
                        {
                            var temp = new NewEntityState();
                            temp.Deserialize(packer);
                            founded.Add(temp);
                        }
                        else founded.Add(null);
                    }

                    var lostCount = packer.ReadShort();
                    if (lostCount < 0)
                        lost = null;
                    else if (lost == null)
                        lost = new List<ushort>();
                    for (short i = 0; i < lostCount; ++i)
                        lost.Add(packer.ReadUShort());
                }
            }

            [ContractMember(1)] int _lastReceivedTick;

            public int lastReceivedTick
            {
                get => _lastReceivedTick;
                set => _lastReceivedTick = value;
            }

            [ContractMember(2)] List<ServerSnapshot> _snapshots = new List<ServerSnapshot>();

            public List<ServerSnapshot> snapshots
            {
                get => _snapshots;
                set => _snapshots = value;
            }

            public void Serialize(IBitsPacker packer)
            {
                packer.Write(lastReceivedTick);
                packer.Write((short) (snapshots?.Count ?? -1));
                if (snapshots != null)
                    for (var i = 0; i < snapshots.Count; ++i)
                    {
                        packer.Write(snapshots[i] != null);
                        snapshots[i]?.Serialize(packer);
                    }
            }

            public void Deserialize(IBitsPacker packer)
            {
                lastReceivedTick = packer.ReadInt();
                var snapshotsCount = packer.ReadShort();
                if (snapshotsCount < 0)
                    snapshots = null;
                else if (snapshots == null)
                    snapshots = new List<ServerSnapshot>();
                for (short i = 0; i < snapshotsCount; ++i)
                {
                    if (packer.ReadBool())
                    {
                        var temp = new ServerSnapshot();
                        temp.Deserialize(packer);
                        snapshots.Add(temp);
                    }
                    else snapshots.Add(null);
                }
            }
        }

        [ContractMember(1)] ServerSyncDto _state;

        public ServerSyncDto state
        {
            get => _state;
            set => _state = value;
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(state != null);
            state?.Serialize(packer);
        }

        public void Deserialize(IBitsPacker packer)
        {
            {
                if (packer.ReadBool()) (state ?? (state = new ServerSyncDto())).Deserialize(packer);
                else state = null;
            }
        }
    }
}