using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Valkyrie.Communication.Network;
using Valkyrie.Data.Serialization;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Engines;
using Valkyrie.Ecs.Entities;
using Valkyrie.Ecs.Groups;
using Valkyrie.Math.Space.Sync.Components;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Math.Space.Sync
{
    [NetworkMessage(NetDeliveryMethod.ReliableOrdered, DefinedConstants.NetworkChannels.WorldState)]
    [Contract]
    class WorldSyncEvent
    {
        [Contract]
        public class FoundEntityData
        {
            [ContractMember(0)] public ushort Id;
            [ContractMember(1)] public Vector2 Position;
            [ContractMember(3)] public bool Controlled;

            public static FoundEntityData FromEntity(IEcsEntity e, bool isControlled)
            {
                return new FoundEntityData
                {
                    Id = e.Id,
                    Controlled = isControlled,
                    Position = e.Get<World2ObjectComponent>().Position
                };
            }
        }
        [Contract]
        public class SyncEntityData
        {
            [ContractMember(0)] public string Id;
            [ContractMember(1)] public List<byte[]> Data;
        }

        [ContractMember(0)] public int Tick;
        [ContractMember(1)] public float Time;
        [ContractMember(2)] public int WorldUidHash;
        [ContractMember(3)] public List<FoundEntityData> Founded;
        [ContractMember(4)] public List<string> Lost;
        [ContractMember(5)] public List<SyncEntityData> Sync;

        public override string ToString()
        {
            return $"Tick:{Tick} World:{WorldUidHash} Entities:{string.Join(",", Sync.Select(u => u.Id).ToArray())}";
        }
    }

    class SyncDataHandleComponent : IEcsComponent
    {
        readonly List<KeyValuePair<int, List<byte[]>>> _listData = new List<KeyValuePair<int, List<byte[]>>>();
        static readonly List<byte[]> _default = new List<byte[]>();

        public void PushData(int tick, List<byte[]> data)
        {
            var entry = new KeyValuePair<int, List<byte[]>>(tick, data);
            for(var i = 0; i < _listData.Count; ++i)
                if (_listData[i].Key == tick)
                {
                    _listData[i] = entry;
                    return;
                }

            _listData.Add(entry);
        }

        public List<byte[]> GetStoredData(int tick)
        {
            for (var i = _listData.Count - 1; i >= 0; --i)
            {
                if (_listData[i].Key <= tick)
                    return _listData[i].Value;
            }

            return _default;
        }
    }

    static class SyncExtension
    {
        public static EntityType GetEntityType(this IEcsEntity entity)
        {
            return entity.Get<EntityTypeComponent>()?.Type ?? EntityType.Other;
        }

        public static IEcsEntity SetEntityType(this IEcsEntity entity, EntityType newType)
        {
            entity.GetOrCreate<EntityTypeComponent>().Type = newType;
            return entity;
        }
        
        public static IEnumerable<IEcsComponent> CollectSyncComponents(this IEcsEntity entity, List<int> componentsIds)
        {
            var e = (EcsEntity) entity;
            for (var i = 0; i < componentsIds.Count; ++i)
            {
                var componentIndex = componentsIds[i];
                if (e.Components[componentIndex] != null)
                    yield return e.Components[componentIndex];
            }
        }

        public static void CollectSyncData(this IEcsEntity e, IContractSerializer serializer, int tick)
        {
            switch (e.GetEntityType())
            {
                case EntityType.Other:
                    break;
                case EntityType.Server:
                {
                    /*WorldSyncEvent.SyncEntityData data = e.GetOrCreate<SyncDataStoreComponent>().AllocateNewData(tick);
                    data.Id = e.Id;
                    data.Data.AddRange(e.CollectSyncComponents(EcsExtensions.OwnerSendsComponents)
                        .Select(o => serializer.Serialize(o, true)));*/
                    return;
                }
                case EntityType.Controller:
                {
                    /*WorldSyncEvent.SyncEntityData data = e.GetOrCreate<SyncDataStoreComponent>().AllocateNewData(tick);
                    data.Id = e.Id;
                    data.Data.AddRange(e.CollectSyncComponents(EcsExtensions.ControllerSendsComponents)
                        .Select(o => serializer.Serialize(o, true)));*/
                    return;
                }
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    abstract class WorldSyncExecuteQueryEngine : ExecuteQueryEngine
    {
        protected ILogInstance Log { get; }
        protected WorldSyncExecuteQueryEngine(IEcsEntitiesDatabase entitiesDatabase, ILogService logService) : base(entitiesDatabase)
        {
            Log = logService.GetLog(GetType().Name);
        }
    }
    
    class ObserverLostFindCleanupEngine : QueryEngine, IEcsCleanupEngine
    {
        public ObserverLostFindCleanupEngine(IEcsEntitiesDatabase entitiesDatabase) : base(entitiesDatabase)
        {
        }

        protected override GroupBuilder GetBuilder()
        {
            return GroupBuilder.Empty().AnyOf<ObserverFindObjectComponent, ObserverLostObjectComponent>();
        }
        
        public void Cleanup()
        {
            foreach (var e in GetEntities())
            {
                e.Remove<ObserverFindObjectComponent>();
                e.Remove<ObserverLostObjectComponent>();
            }
        }
    }

    class CollectFoundLostDataServerEngine : ExecuteQueryEngine
    {
        private readonly ILogInstance _log;

        public CollectFoundLostDataServerEngine(IEcsEntitiesDatabase entitiesDatabase, ILogService log) : base(entitiesDatabase)
        {
            _log = log.GetLog("CollectFoundLostDataServerEngine");
        }

        protected override GroupBuilder GetBuilder()
        {
            return GroupBuilder.AllOf<SyncObserverComponent>();
        }

        protected override void Execute(Iteration tickInfo, IList<IEcsEntity> entities)
        {
            var tick = tickInfo.SimulationTick;
            
            foreach (var e in entities)
            {
                var syncObserver = e.Get<SyncObserverComponent>();
                if (syncObserver.JustCreated)
                {
                    syncObserver.JustCreated = false;
                    syncObserver.AddFounded(tick, new NewEntityState()
                    {
                        id = e.Id,
                        isControlled = true,
                        position = e.Get<World2ObjectComponent>().Position
                    });
                }

                var foundedObjects = e.Get<ObserverFindObjectComponent>();
                if (foundedObjects != null)
                {
                    foreach (var o in foundedObjects.Objects)
                        if (o.EcsEntity.Get<SyncDataStoreComponent>() != null)
                        {
                            //_log.Info($"Server: tick {tick}, found object {o.EcsEntity.Id}");
                            syncObserver.AddFounded(tick, new NewEntityState()
                            {
                                id = o.EcsEntity.Id,
                                isControlled = false,
                                position = e.Get<World2ObjectComponent>().Position
                            });
                        }}
                
                var lostObjects = e.Get<ObserverLostObjectComponent>();
                if (lostObjects != null)
                    foreach (var o in lostObjects.Objects)
                        if (o.EcsEntity.Get<SyncDataStoreComponent>() != null)
                            syncObserver.AddLost(tick, o.EcsEntity.Id);
            }
        }
    }

    
    class CollectSyncDataServerEngine : WorldSyncExecuteQueryEngine
    {
        private readonly IContractSerializer _contractSerializer;

        public CollectSyncDataServerEngine(IEcsEntitiesDatabase entitiesDatabase,
            IContractSerializer contractSerializer, ILogService ls) : base(entitiesDatabase, ls)
        {
            _contractSerializer = contractSerializer;
        }

        protected override GroupBuilder GetBuilder()
        {
            return GroupBuilder.AllOf<SyncDataStoreComponent, EntityTypeComponent>();
        }

        protected override void Execute(Iteration tickInfo, IList<IEcsEntity> entities)
        {
            var tick = tickInfo.SimulationTick;
            foreach (var e in entities)
            {
                //var msg = $"SERVER {e.Id}: Tick {tick}";

                List<int> components;
                switch (e.GetEntityType())
                {
                    case EntityType.Other:
                        ////Log.Debug(msg + " DONT SYNC");
                        continue;
                    default:
                        components = EcsExtensions.ServerSendsComponents;
                        break;
                }
                
                var data = e.GetOrCreate<SyncDataStoreComponent>().AllocateState(tick);
                data.id = e.Id;
                for (var i = 0; i < components.Count; ++i)
                {
                    var componentIndex = components[i];
                    var source = ((EcsEntity) e).Components[componentIndex];
                    if (source != null)
                    {
                        data.components.Add(new ComponentInfo { index = (byte) componentIndex, buffer = _contractSerializer.Serialize(source, false)});
                        //msg += $" SAVE {source} {componentIndex}";
                    }
                    //else
                        //msg += $" SKIP {componentIndex}";
                }
                
                ////Log.Debug(msg);
            }
        }
    }

    class SendSyncDataServerEngine : WorldStateSyncObject
    {
        private readonly IGroupHolder _controllersGroup;
        
        public SendSyncDataServerEngine(ILogService log, IGroupHolder controllersGroup) : base(log)
        {
            _controllersGroup = controllersGroup;
            _controllersGroup.Setup(GroupBuilder.AllOf<SyncObserverComponent, SyncDataStoreComponent>());
        }
        
        Dictionary<IObserver<WorldSyncServerDto>, WorldSyncServerDto> SendData = new Dictionary<IObserver<WorldSyncServerDto>, WorldSyncServerDto>();

        protected override void SyncImpl(Iteration iterationInfo)
        {
            SendData.Clear();
            
            var lastTick = iterationInfo.SimulationTick;
            foreach (var entity in _controllersGroup.GetEntities())
            {
                var observer = entity.Get<SyncObserverComponent>();
                if(observer.OutStream == null)
                    continue;

                var firstTick = lastTick - TicksInPacket + 1;
                if (observer.LastConfirmedTick >= firstTick)
                    firstTick = observer.LastConfirmedTick + 1;
                
                var totalTicksToSend = lastTick - firstTick + 1;
                if(totalTicksToSend <= 0)
                    continue;
                
                if (!SendData.TryGetValue(observer.OutStream, out var msg))
                {
                    msg = new WorldSyncServerDto
                    {
                        state = new WorldSyncServerDto.ServerSyncDto {lastReceivedTick = observer.LastReceivedTick}
                    };
                    SendData.Add(observer.OutStream, msg);
                }

                for (var tick = firstTick; tick <= lastTick; ++tick)
                {
                    //_log.Info($"Send to {entity.Id} tick={tick}, confirmed={observer.LastConfirmedTick}");
                    var snapshot = CreateSnapshot(entity, tick);
                    Merge(msg.state.snapshots, snapshot);
                }

                //observer.OutStream.OnNext(msg);
            }

            foreach (var pair in SendData)
                pair.Key.OnNext(pair.Value);
            
            SendData.Clear();
        }

        private void Merge(List<WorldSyncServerDto.ServerSyncDto.ServerSnapshot> snapshots, WorldSyncServerDto.ServerSyncDto.ServerSnapshot snapshot)
        {
            var exist = snapshots.Find(u => u.tick == snapshot.tick);
            if (exist == null)
            {
                snapshots.Add(snapshot);
            }
            else
            {
                //_log.Info($"Merge snapshots for tick {exist.tick}");
                if(snapshot.founded != null)
                {
                    if(exist.founded != null)
                        foreach (var founded in snapshot.founded)
                        {
                            var existObject = exist.founded.Find(u => u.id == founded.id);
                            if (existObject != null)
                                existObject.isControlled = existObject.isControlled || founded.isControlled;
                            else
                                exist.founded.Add(founded);
                        }
                    else
                        exist.founded = snapshot.founded;
                }

                if(snapshot.lost != null)
                {
                    if(exist.lost != null)
                        foreach (var id in snapshot.lost)
                        {
                            if (exist.lost.Find(u => u == id) == null)
                                exist.lost.Add(id);
                        }
                    else
                        exist.lost = snapshot.lost;
                }

                foreach (var state in snapshot.states)
                {
                    var existState = exist.states.Find(u => u.id == state.id);
                    if(existState == null)
                        exist.states.Add(state);
                }
            }
        }

        private WorldSyncServerDto.ServerSyncDto.ServerSnapshot CreateSnapshot(IEcsEntity e, int tick)
        {
            var syncObserver = e.Get<SyncObserverComponent>();
            var snapshot = new WorldSyncServerDto.ServerSyncDto.ServerSnapshot()
            {
                tick = tick,
                founded = syncObserver.GetFoundedData(tick),
                lost = syncObserver.GetLostData(tick),
                states = new List<EntityState>() {e.Get<SyncDataStoreComponent>().GetStoredState(tick)}
            };

            var observer = e.Get<ObserverComponent>();
            if (observer != null)
            {
                foreach (var o in observer.FoundedObjects)
                {
                    var dataStore = o.EcsEntity.Get<SyncDataStoreComponent>();
                    if (dataStore != null)
                    {
                        var data = dataStore.GetStoredState(tick);
                        if(data != null)
                            snapshot.states.Add(data);
                    }
                }
            }

            return snapshot;
        }
    }
    
    class ApplySyncDataServerEngine : ExecuteQueryEngine
    {
        private readonly IEcsEntitiesDatabase _entitiesDatabase;
        private readonly List<int> _appliedIndices = new List<int>();
        private readonly List<int> _toRemoveIndices;
        private IContractSerializer _contractSerializer;

        public ApplySyncDataServerEngine(IEcsEntitiesDatabase entitiesDatabase, IContractSerializer contractSerializer) : base(entitiesDatabase)
        {
            _entitiesDatabase = entitiesDatabase;
            _contractSerializer = contractSerializer;

            EcsExtensions.GetComponentsCount();
            _toRemoveIndices =
                new List<int>(EcsExtensions.ControllerSendsComponents.Where(u =>
                    !EcsExtensions.DontDestroyNetworkComponents.Contains(u)));
        }

        protected override GroupBuilder GetBuilder()
        {
            return GroupBuilder.AllOf<SyncObserverComponent, EntityTypeComponent>();
        }

        protected override void Execute(Iteration tickInfo, IList<IEcsEntity> entities)
        {
            var tick = tickInfo.SimulationTick;

            foreach (var e in entities)
            {
                var syncObserver = e.Get<SyncObserverComponent>();
                var snapshot = syncObserver.GetSnapshot(tick);
                if(snapshot == null)
                    continue;

                foreach (var state in snapshot.states)
                {
                    var entity = _entitiesDatabase.GetEntity(state.id);

                    ApplyState((EcsEntity) entity, state.components, EcsExtensions.GetIndexMappingForComponents());
                }
            }
        }

        private void ApplyState(EcsEntity e, List<ComponentInfo> data, List<Type> componentsMap)
        {
            foreach (var componentData in data)
            {
                var componentType = componentsMap[componentData.index];
                    
                var index = EcsExtensions.GetComponentIndex(componentType);
                if (EcsExtensions.ControllerSendsComponents.Contains(index))
                {
                    if (e.Components[index] != null)
                    {
                        //msg += ($" UPDATE {index} ({componentType.Name})");
                        _contractSerializer.Deserialize(componentData.buffer, componentType, e.Components[index]);
                    }
                    else
                    {
                        //msg += ($" ADD {index} ({componentType.Name})");
                        e.Add(
                            (IEcsComponent) _contractSerializer.Deserialize(componentData.buffer, componentType, null),
                            index);
                    }
                    _appliedIndices.Add(index);
                }
            }

            foreach (var ownerSendsComponent in _toRemoveIndices)
            {
                if(_appliedIndices.Contains(ownerSendsComponent))
                    continue;
                //msg += ($" REM {ownerSendsComponent}");
                e.Remove(ownerSendsComponent);
            }
                
            //Log.Debug(msg);
            _appliedIndices.Clear();
        }
    }
    

}