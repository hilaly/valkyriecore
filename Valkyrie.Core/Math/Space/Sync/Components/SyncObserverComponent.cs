using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space.Sync.Components
{
    class Sampler<T>
    {
        private readonly int _size;
        private readonly List<KeyValuePair<int, List<T>>> _data;

        public Sampler(int size)
        {
            _size = size;
            _data = new List<KeyValuePair<int, List<T>>>();
        }

        public List<T> CreateSample(int key)
        {
            for (var i = _data.Count - 1; i >= 0; --i)
                if (_data[i].Key == key)
                    return _data[i].Value;
            
            if(_data.Count >= _size)
                _data.RemoveAt(0);
            _data.Add(new KeyValuePair<int, List<T>>(key, new List<T>()));
            return _data[_data.Count - 1].Value;
        }

        public void AddSample(int key, T sample)
        {
            CreateSample(key).Add(sample);
        }

        public List<T> GetSample(int key)
        {
            return _data.FirstOrDefault(u => u.Key == key).Value;
        }
    }
    
    class SyncObserverComponent : IEcsComponent
    {
        public bool JustCreated = true;
        public IObserver<WorldSyncServerDto> OutStream;
        public int LastConfirmedTick;
        public int LastReceivedTick;

        public SyncObserverComponent(IObserver<WorldSyncServerDto> channel, int worldSimulationTick)
        {
            OutStream = channel;
            LastConfirmedTick = worldSimulationTick;
        }

        #region Lost/Found local

        public const int SAMPLERSIZE = DefinedConstants.SyncBuffers.StoreSize;
        
        private readonly Sampler<NewEntityState> _foundedSampler =
            new Sampler<NewEntityState>(SAMPLERSIZE);
        private readonly Sampler<ushort> _lostSampler = new Sampler<ushort>(SAMPLERSIZE);
        private readonly List<WorldSyncClientDto.ClientSyncDto.ClientSnapshot> _snapshots = new List<WorldSyncClientDto.ClientSyncDto.ClientSnapshot>();

        public WorldSyncClientDto.ClientSyncDto.ClientSnapshot GetSnapshot(int tick)
        {
            return _snapshots.Find(u => u.tick == tick);
        }

        public void AddFounded(int tick, NewEntityState data)
        {
            _foundedSampler.AddSample(tick, data);
        }

        public List<NewEntityState> GetFoundedData(int tick)
        {
            return _foundedSampler.GetSample(tick);
        }

        public void AddLost(int tick, ushort eid)
        {
            _lostSampler.AddSample(tick, eid);
        }

        public List<ushort> GetLostData(int tick)
        {
            return _lostSampler.GetSample(tick);
        }

        #endregion

        public void Handle(WorldSyncClientDto.ClientSyncDto syncCommand)
        {
            if(syncCommand.lastReceivedTick > LastConfirmedTick)
                LastConfirmedTick = syncCommand.lastReceivedTick;
            
            foreach (var snapshot in syncCommand.snapshots)
            {
                if(snapshot.tick <= LastReceivedTick)
                    continue;
                LastReceivedTick = snapshot.tick;
                _snapshots.Add(snapshot);
            }

            _snapshots.Sort((o0, o1) => o0.tick - o1.tick);
            while (_snapshots.Count > DefinedConstants.SyncBuffers.StoreSize)
                _snapshots.RemoveAt(0);
        }
    }
}