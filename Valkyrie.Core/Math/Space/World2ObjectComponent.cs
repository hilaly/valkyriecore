﻿using System;
using System.Numerics;
using Valkyrie.Data.Serialization;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Entities;
using Valkyrie.Math.Space.Sync;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space
{
    [Contract]
    [Sync(EntityType.Server, SyncPeriod = 1, DontDestroyOnNetwork = true)]
    class World2ObjectComponent : IEcsComponent, IDisposable, IWorldObject
    {
        private World2 World => (World2) EcsEntity.Get<WorldComponent>().World;
        
        internal readonly ReactiveProperty<Vector2> _position;
        
        internal readonly Subject<World2ObjectComponent> DisposeChannel = new Subject<World2ObjectComponent>();
        
        private Region2 _region;

        public IEcsEntity EcsEntity { get; }
        
        [ContractMember(0)]
        public Vector2 Position
        {
            get => _position;
            set => _position.Value = value;
        }

        public World2ObjectComponent(Vector2 initialPosition, IEcsEntity ecsEntity)
        {
            EcsEntity = ecsEntity;
            _position = new ReactiveProperty<Vector2>(initialPosition);
            _position.Subscribe(UpdateInterestManagement);

            UpdateInterestManagement(initialPosition);
        }

        public void Dispose()
        {
            DisposeChannel.OnNext(this);
            DisposeChannel.OnCompleted();
            DisposeChannel.Dispose();
            
            _position.Dispose();
            World.Destroy(this);
        }
        
        private void UpdateInterestManagement(Vector2 currentPosition)
        {
            var region = World.GetRegion(currentPosition);
            if (_region != region)
            {
                _region = region;
                _region.ObjectSpawnChannel.OnNext(this);
            }
        }

        public void Destroy()
        {
            EcsEntity.Add(new EcsExtensions.NeedDestroyComponent());
        }
    }
}