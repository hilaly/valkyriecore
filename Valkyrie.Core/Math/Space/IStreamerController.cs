﻿using System;
using System.Numerics;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space
{
    public interface IStreamerController : IDisposable
    {
        ReactiveProperty<Vector2> Position { get; }
    }
}