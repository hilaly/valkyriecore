﻿namespace Valkyrie.Math.Space
{
    public class WorldMessageInstance
    {
        public readonly IWorldObject Sender;
        public readonly object Message;

        public WorldMessageInstance(IWorldObject sender, object message)
        {
            Sender = sender;
            Message = message;
        }
    }
}