﻿using System.Collections.Generic;
using System.Numerics;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space
{
    class AllowAllSpawnHandler : IWorldSpawnHandler
    {
        public bool IsSpawnAllowed(ushort uid, Vector2 position, object additionalData,
            List<IEcsComponent> additionalComponents)
        {
            return true;
        }

        public void HandleSpawn(IWorldObject o, object additionalData)
        {
        }
    }
}