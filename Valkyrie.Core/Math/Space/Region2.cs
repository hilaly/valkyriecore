using System.Numerics;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space
{
    class Region2
    {
        public ISubject<World2ObjectComponent> ObjectSpawnChannel = new Subject<World2ObjectComponent>();
        public Region2(Vector2 coordinate, BoundingBox2 bounding)
        {
            Coordinate = coordinate;
            Bounding = bounding;
        }

        public Vector2 Coordinate { get; private set; }
        public BoundingBox2 Bounding { get; private set; }
    }
}