﻿using System;
using System.Numerics;

namespace Valkyrie.Math.Space
{
    public interface IWorld : IDisposable
    {
        int SimulationTick { get; }
        BoundingBox2 Bounding { get; }

        IStreamerController CreateStreamController(Vector2 streamEnter, Vector2 streamExit, Vector2 position);

        IWorldObject CreateObject(ushort id, Vector2 position);
        IObserver CreateObserver(IWorldObject owner);
    }
}
