﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space
{
    class StreamerController : IStreamerController
    {
        private readonly IDisposable _disposable;
        private BoundingBox2 _regionInnerFocus;
        private BoundingBox2 _regionOuterFocus;
        private readonly World2 _world;
        
        private readonly Dictionary<Region2, IDisposable> _regionsSubscription = new Dictionary<Region2, IDisposable>();
        
        public ReactiveProperty<Vector2> Position { get; }
        
        public Vector2 StreamDistanceEnter { get; }
        public Vector2 StreamDistanceExit { get; }

        public StreamerController(Vector2 streamDistanceEnter, Vector2 streamDistanceExit, Vector2 initialPosition, IDisposable disposable, World2 world)
        {
            StreamDistanceEnter = streamDistanceEnter;
            StreamDistanceExit = streamDistanceExit;
            _disposable = disposable;
            _world = world;
            
            _regionInnerFocus =
                _regionOuterFocus = new BoundingBox2 {Max = _world.Bounding.Min, Min = _world.Bounding.Max};

            Position = new ReactiveProperty<Vector2>(initialPosition);
            Position.Subscribe(p => UpdateInterestManagement());
            
            UpdateInterestManagement();
        }

        public void Dispose()
        {
            _disposable.Dispose();
            Position.Dispose();

            UnsubscribeRegions(_regionsSubscription.Keys.ToList());
        }

        void UpdateInterestManagement()
        {
            var outerFocus =
                BoundingBox2.CreateFromPoints(Position - StreamDistanceExit, Position + StreamDistanceExit)
                    .IntersectWith(_world.Bounding);
            var innerFocus =
                BoundingBox2.CreateFromPoints(Position - StreamDistanceEnter, Position + StreamDistanceEnter)
                    .IntersectWith(_world.Bounding);

            innerFocus = _world.GetRegionAlignedBoundingBox(innerFocus);
            if (!innerFocus.Equals(_regionInnerFocus))
            {
                var regions = _regionInnerFocus.IsValid()
                    ? _world.GetRegionsExcept(innerFocus, _regionInnerFocus)
                    : _world.GetRegions(innerFocus);
                
                SubscribeRegions(regions);
                
                _regionInnerFocus = innerFocus;
            }

            outerFocus = _world.GetRegionAlignedBoundingBox(outerFocus);
            if (!outerFocus.Equals(_regionOuterFocus))
            {
                var regions = _regionOuterFocus.IsValid()
                    ? _world.GetRegionsExcept(_regionOuterFocus, outerFocus)
                    : _regionsSubscription.Keys.Where(r => !outerFocus.IsIntersect(r.Bounding));

                UnsubscribeRegions(regions);

                _regionOuterFocus = outerFocus;
            }
        }
        
        void SubscribeRegions(IEnumerable<Region2> regions)
        {
            foreach (var region in regions)
            {
                if(_regionsSubscription.ContainsKey(region))
                    continue;

                _regionsSubscription.Add(region, _world.Streamer.Load(region));
            }
        }

        void UnsubscribeRegions(IEnumerable<Region2> regions)
        {
            foreach (var region in regions)
            {
                if (!_regionsSubscription.TryGetValue(region, out var subscription))
                    continue;
                
                subscription.Dispose();
                _regionsSubscription.Remove(region);
            }
        }

    }
}