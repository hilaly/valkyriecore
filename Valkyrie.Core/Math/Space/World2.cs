﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Valkyrie.Ecs;
using Valkyrie.Ecs.Entities;
using Valkyrie.Math.Space.Management;
using Valkyrie.Math.Space.Sync;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Math.Space
{
    public class World2 :  IWorld
    {
        internal readonly IWorldSpawnHandler SpawnHandler;
        internal readonly World2Data WorldData;
        internal readonly WorldStreamer Streamer;
        private readonly List<Region2> _regions = new List<Region2>();

        readonly HashSet<World2ObjectComponent> _objects = new HashSet<World2ObjectComponent>();
        private readonly IEcsEntitiesDatabase _ecsDb;

        protected readonly CompositeDisposable _disposable = new CompositeDisposable();
        protected readonly IEcsContext _ecsCtx;

        private readonly Subject _disposeSubject = new Subject();
        protected readonly ILogInstance _log;

        public int SimulationTick => _ecsCtx.Engines.Tick;

        public int Hash { get; private set; }

        public IObservable ObserveDispose()
        {
            return _disposeSubject;
        }

        public World2(World2Data worldData, IEcsContext ecxCtx, ILogService logService)
        {
            _log = logService.GetLog($"Valkyrie.World.{worldData.WorldName}");
            WorldData = worldData;
            Hash = WorldData.WorldName.GetHashCode();
            _ecsCtx = ecxCtx;
            _ecsDb = _ecsCtx.EcsEntitiesDatabase;
            _ecsCtx.Engines.UseFixedTimestamp = WorldData.SimulationTime;
            
            SpawnHandler = new AllowAllSpawnHandler();

            var worldSize = WorldData.Bounding.Size;
            var regionsCount = new IntVec2(
                (int) System.Math.Ceiling(worldSize.X / worldData.RegionSize.X),
                (int) System.Math.Ceiling(worldSize.Y / worldData.RegionSize.Y));

            var regionSize = new Vector2(worldSize.X/regionsCount.x, worldSize.Y/regionsCount.y);
            for (var y = 0; y < regionsCount.y; ++y)
            {
                for (var x = 0; x < regionsCount.x; ++x)
                {
                    var min = new Vector2(regionSize.X*x, regionSize.Y*y) + WorldData.Bounding.Min;
                    var max = min + regionSize;

                    var regionBounding = new BoundingBox2 {Min = min, Max = max};
                    _regions.Add(new Region2(regionBounding.Center, regionBounding));
                }
            }
            
            Streamer = new WorldStreamer(this);
        }

        public virtual void Dispose()
        {
            foreach (var o in _objects)
                o.Destroy();

            _disposable.Dispose();
            
            _disposeSubject.OnNext();
            _disposeSubject.OnCompleted();
        }

        #region IWorld
        
        public BoundingBox2 Bounding => WorldData.Bounding;
        
        public IStreamerController CreateStreamController(Vector2 streamEnter, Vector2 streamExit, Vector2 position)
        {
            return new StreamerController(streamEnter, streamExit, position, Disposable.Empty, this);
        }

        public IWorldObject CreateObject(ushort uid, Vector2 position)
        {
            var ecs = _ecsDb.CreateEntity(uid, Enumerable.Empty<IEcsComponent>());
            ecs.Add(new WorldComponent(this));
            var w2O = new World2ObjectComponent(position, ecs);
            ecs.Add(w2O);
            if (WorldData.IsOnline)
                w2O.SetOnline().EcsEntity.SetEntityType(EntityType.Server);
            
            lock (_objects)
                _objects.Add(w2O);
            return w2O;
        }

        public IObserver CreateObserver(IWorldObject observer)
        {
            return new Observer2(this, (World2ObjectComponent) observer);
        }
        
        #endregion

        #region wORLDiMPLEMENTATION

        internal void Destroy(World2ObjectComponent o)
        {
            lock (_objects)
                _objects.Remove(o);
        }

        internal void Destroy(IObserver o)
        {
            //TODO: implement if need
        }

        internal BoundingBox2 GetRegionAlignedBoundingBox(BoundingBox2 focus)
        {
            if (focus.IsValid())
            {
                foreach (var region in GetRegions(focus))
                    focus = focus.UnionWith(region.Bounding);

                return focus;
            }
            return focus;
        }

        internal Region2 GetRegion(Vector2 position)
        {
            return _regions.FirstOrDefault(u => u.Bounding.Contains(position));
        }

        internal IEnumerable<Region2> GetRegions(BoundingBox2 focus)
        {
            return _regions.Where(u => focus.IsIntersect(u.Bounding)).ToList();
        }

        internal IEnumerable<Region2> GetRegionsExcept(BoundingBox2 focus, BoundingBox2 exclude)
        {
            return GetRegions(focus).Where(u => !exclude.IsIntersect(u.Bounding)).ToList();
        }

        internal IWorldObject FindObject(ushort id)
        {
            return _ecsDb.GetEntity(id)?.Get<World2ObjectComponent>();
        }

        internal IEnumerable<World2ObjectComponent> GetObjects(BoundingBox2 bounding)
        {
            lock (_objects)
                return _objects.Where(u => bounding.Contains(u.Position));
        }

        #endregion
    }
}