﻿using System.Collections.Generic;
using System.Numerics;
using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space
{
    public interface IWorldSpawnHandler
    {
        bool IsSpawnAllowed(ushort uid, Vector2 position, object additionalData, List<IEcsComponent> additionalComponents);
        void HandleSpawn(IWorldObject o, object additionalData);
    }
}