﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using Valkyrie.Ecs;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space
{
    class Observer2 : IObserver
    {
        private readonly World2 _world;
        private readonly World2ObjectComponent _owner;

        private readonly Subject<IWorldObject> _findSubject = new Subject<IWorldObject>();
        private readonly Subject<IWorldObject> _lostSubject = new Subject<IWorldObject>();
        private readonly Subject<WorldMessageInstance> _eventFinded = new Subject<WorldMessageInstance>();
        private CompositeDisposable _ownerSubscription = new CompositeDisposable();

        private BoundingBox2 _regionInnerFocus;
        private BoundingBox2 _regionOuterFocus;
        
        private Vector2 _viewDistanceEnter;
        private Vector2 _viewDistanceExit;

        private readonly ObserverComponent _observerComponent;

        private readonly Dictionary<Region2, IDisposable> _regionsSubscription = new Dictionary<Region2, IDisposable>();
        readonly Dictionary<World2ObjectComponent, IDisposable> _autoManagedSubscriptions = new Dictionary<World2ObjectComponent, IDisposable>();
        
        public Observer2(World2 world, World2ObjectComponent owner)
        {
            _world = world;

            _regionInnerFocus =
                _regionOuterFocus = new BoundingBox2 {Max = _world.Bounding.Min, Min = _world.Bounding.Max};
            
            _viewDistanceEnter = _viewDistanceExit = Vector2.Zero;
            
            Debug.Assert(owner != null, "Invalid attach");
            _owner = owner;
            
            _observerComponent = _owner.EcsEntity.GetOrCreate<ObserverComponent>();
            _observerComponent.Owners.Add(this);

            _ownerSubscription.Add(owner._position.Subscribe(p => AttachedObjectPosition(_owner)));
            _ownerSubscription.Add(owner.DisposeChannel.Subscribe(AttachedObjectDisposed));

            UpdateInterestManagement(_owner.Position);
        }
        
        #region IObserver

        public void SetViewDistance(Vector2 enter, Vector2 exit)
        {
            _viewDistanceEnter = enter;
            _viewDistanceExit = exit;

            UpdateInterestManagement(_owner.Position);
        }

        public IObservable<IWorldObject> ObserveObjectFind()
        {
            return _findSubject;
        }

        public IObservable<IWorldObject> ObserveObjectLost()
        {
            return _lostSubject;
        }

        public IObservable<WorldMessageInstance> ObserveEvents()
        {
            return _eventFinded;
        }

        #endregion

        public void Dispose()
        {
            _eventFinded.OnCompleted();
            _findSubject.OnCompleted();
            _lostSubject.OnCompleted();
            
            _eventFinded.Dispose();
            _findSubject.Dispose();
            _lostSubject.Dispose();
            
            _ownerSubscription.Dispose();
            _ownerSubscription = null;

            foreach (var value in _regionsSubscription.Values)
                value.Dispose();
            foreach (var pair in _autoManagedSubscriptions)
            {
                _observerComponent.Remove(pair.Key);
                pair.Value.Dispose();
            }
            
            _regionsSubscription.Clear();
            _autoManagedSubscriptions.Clear();

            _world.Destroy(this);

            _observerComponent.Owners.Remove(this);
            if (_observerComponent.Owners.Count == 0)
                _owner.EcsEntity.Remove<ObserverComponent>();
        }

        #region Handlers

        #region Attached

        void AttachedObjectPosition(World2ObjectComponent o)
        {
            UpdateInterestManagement(o.Position);
        }

        void AttachedObjectDisposed(World2ObjectComponent arg1)
        {
            Dispose();
        }

        #endregion

        #region Objects

        private void ObjectSubscriptionDispose()
        {
        }

        private void ObjectDisposed(World2ObjectComponent worldObject)
        {
            AutoUnsubscribeObject(worldObject);
        }

        private void ObjectEvent(WorldMessageInstance gm)
        {
            var e = _owner.EcsEntity;
            var component = e.Get<AllMessagesComponent>();
            if (component != null)
                component.Messages.Add(gm);
            else
                e.Add(new AllMessagesComponent(gm));

            _eventFinded.OnNext(gm);
        }

        private void ObjectPositionChange(World2ObjectComponent worldObject)
        {
            if(_regionOuterFocus.Contains(worldObject.Position))
                return;
            AutoUnsubscribeObject(worldObject);
        }

        #endregion

        #endregion

        #region Subscription management

        private void UpdateInterestManagement(Vector2 newPosition)
        {
            var outerFocus =
                BoundingBox2.CreateFromPoints(newPosition - _viewDistanceExit, newPosition + _viewDistanceExit)
                    .IntersectWith(_world.Bounding);
            var innerFocus =
                BoundingBox2.CreateFromPoints(newPosition - _viewDistanceEnter, newPosition + _viewDistanceEnter)
                    .IntersectWith(_world.Bounding);

            innerFocus = _world.GetRegionAlignedBoundingBox(innerFocus);
            if (!innerFocus.Equals(_regionInnerFocus))
            {
                var regions = _regionInnerFocus.IsValid()
                    ? _world.GetRegionsExcept(innerFocus, _regionInnerFocus)
                    : _world.GetRegions(innerFocus);
                SubscribeRegions(regions);
                _regionInnerFocus = innerFocus;
            }

            outerFocus = _world.GetRegionAlignedBoundingBox(outerFocus);
            if (!outerFocus.Equals(_regionOuterFocus))
            {
                var regions = _regionOuterFocus.IsValid()
                    ? _world.GetRegionsExcept(_regionOuterFocus, outerFocus)
                    : _regionsSubscription.Keys.Where(r => !outerFocus.IsIntersect(r.Bounding));

                UnsubscribeRegions(regions);

                _regionOuterFocus = outerFocus;
            }
        }

        void SubscribeRegions(IEnumerable<Region2> regions)
        {
            foreach (var region in regions)
            {
                if(_regionsSubscription.ContainsKey(region))
                    continue;

                var subscription = region.ObjectSpawnChannel.Subscribe(OnObjectSpawned, () => { });
                _regionsSubscription.Add(region, subscription);

                foreach (var o in _world.GetObjects(region.Bounding))
                    AutoSubscribeObject(o);
            }
        }

        private void OnObjectSpawned(World2ObjectComponent worldObject)
        {
            AutoSubscribeObject(worldObject);
        }

        void UnsubscribeRegions(IEnumerable<Region2> regions)
        {
            foreach (var region in regions)
            {
                if (!_regionsSubscription.TryGetValue(region, out var subscription))
                    continue;

                subscription.Dispose();
                _regionsSubscription.Remove(region);

                foreach (var o in _world.GetObjects(region.Bounding))
                    AutoUnsubscribeObject(o);
            }
        }

        private void AutoSubscribeObject(World2ObjectComponent worldObject)
        {
            if(worldObject == _owner)
                return;

            if(_autoManagedSubscriptions.ContainsKey(worldObject))
                return;

            var subscription =
                new CompositeDisposable(
                    worldObject._position.Subscribe(p => ObjectPositionChange(worldObject)),
                    worldObject.DisposeChannel.Subscribe(ObjectDisposed, ObjectSubscriptionDispose));

            _autoManagedSubscriptions.Add(worldObject, subscription);

            if (_observerComponent.Add(worldObject))
                _owner.EcsEntity.GetOrCreate<ObserverFindObjectComponent>().Objects.Add(worldObject);
            _findSubject.OnNext(worldObject);
        }

        private void AutoUnsubscribeObject(World2ObjectComponent worldObject)
        {
            if (!_autoManagedSubscriptions.TryGetValue(worldObject, out var subscription))
                return;
            
            subscription.Dispose();
            _autoManagedSubscriptions.Remove(worldObject);

            if (_observerComponent.Remove(worldObject))
                _owner.EcsEntity.GetOrCreate<ObserverLostObjectComponent>().Objects.Add(worldObject);
            _lostSubject.OnNext(worldObject);
        }

        #endregion
    }
}