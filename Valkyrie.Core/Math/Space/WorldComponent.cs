﻿using Valkyrie.Ecs.Entities;

namespace Valkyrie.Math.Space
{
    public sealed class WorldComponent : IEcsComponent
    {
        public readonly IWorld World;

        internal WorldComponent(IWorld world)
        {
            World = world;
        }
    }
}