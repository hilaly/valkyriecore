﻿using System;
using System.Collections.Generic;
using Valkyrie.Data;
using Valkyrie.Ecs.Entities;
using Valkyrie.Threading.Async;

namespace Valkyrie.Math.Space
{
    sealed class WorldStreamer
    {
        private readonly World2 _world;
        
        private static readonly Dictionary<Region2, int> Counters = new Dictionary<Region2, int>();
        private static readonly Dictionary<Region2, List<IWorldObject>> SpawnedObjects = new Dictionary<Region2, List<IWorldObject>>();

        public WorldStreamer(World2 world)
        {
            _world = world;
        }

        public IDisposable Load(Region2 region)
        {
            lock (Counters)
            {
                if (!Counters.ContainsKey(region))
                {
                    Counters.Add(region, 1);
                    LoadImpl(region);
                }   
            }
            return Disposable.Create(() =>
            {
                lock (Counters)
                {
                    var value = Counters[region] - 1;
                    if (value > 0)
                        Counters[region] = value;
                    else
                    {
                        Counters.Remove(region);
                        UnloadImpl(region);
                    }   
                }
            });
        }

        private void UnloadImpl(Region2 region)
        {
            var os = SpawnedObjects[region];
            SpawnedObjects.Remove(region);
            foreach (var o in os)
                o.Destroy();
        }

        private void LoadImpl(Region2 region)
        {
            var handler = _world.SpawnHandler;
            var allObjects = _world.WorldData.GetObjects(region.Bounding);
            var resultList = new List<IWorldObject>();
            foreach (var info in allObjects)
            {
                var components = new List<IEcsComponent>();
                var ao = info.AdditionalDataType.IsNullOrEmpty()
                    ? null
                    : info.AdditionalData.ToObject(Type.GetType(info.AdditionalDataType));
                if(!handler.IsSpawnAllowed(info.Uid, info.Position, ao, components))
                    continue;
                var o = _world.CreateObject(info.Uid, info.Position);
                foreach (var component in components)
                    ((EcsEntity)o.EcsEntity).Add(component);
                resultList.Add(o);
                handler.HandleSpawn(o, ao);
            }
            SpawnedObjects.Add(region, resultList);
        }
    }
}