using System.Numerics;
using Valkyrie.Data;

namespace Valkyrie.Math
{
    public static class MathExtensions
    {
        public static Vector2 XZ(this Vector3 v)
        {
            return new Vector2(v.X, v.Z);
        }
        public static Vector2 XY(this Vector3 v)
        {
            return new Vector2(v.X, v.Y);
        }
        public static Vector2 YZ(this Vector3 v)
        {
            return new Vector2(v.Y, v.Z);
        }

        public static Vector2 Normalized(this Vector2 v)
        {
            var l = v.Length();
            return v / l;
        }

        public static Vector3 Normalized(this Vector3 v)
        {
            var l = v.Length();
            return v / l;
        }

        public static Quaternion CreateQuaternionFromEuler(this Vector3 v)
        {
            return Quaternion.CreateFromYawPitchRoll(v.Y, v.X, v.Z);
        }

        public static Vector3 Rotate(this Vector3 v, Quaternion q)
        {
            return Vector3.Transform(v, q);
        }

        public static bool IsInRange(this float v, float min, float max)
        {
            return v >= min && v <= max;
        }

        public static bool IsBetween(this Vector3 v, Vector3 min, Vector3 max)
        {
            return v.X.IsInRange(min.X, max.X)
                   && v.Y.IsInRange(min.Y, max.Y)
                   && v.Z.IsInRange(min.Z, max.Z);
        }

        public static float[] GetArray(this Vector3 v)
        {
            return new[] {v.X, v.Y, v.Z};
        }

        public static Vector3 Load(this Vector3 v, float[] arr)
        {
            v.X = arr[0];
            v.Y = arr[1];
            v.Z = arr[2];
            return v;
        }
        
        public static float Angle(Vector2 from, Vector2 to)
        {
            float num = (float) System.Math.Sqrt(from.LengthSquared() * (double) to.LengthSquared());
            return (double) num < 1.00000000362749E-15
                ? 0.0f
                : (float) System.Math.Acos((Vector2.Dot(from, to) / num).Clamp(-1f, 1f)) * 57.29578f;
        }
        
        public static float Angle(Vector3 from, Vector3 to)
        {
            float num = (float) System.Math.Sqrt(from.LengthSquared() * (double) to.LengthSquared());
            return (double) num < 1.00000000362749E-15
                ? 0.0f
                : (float) System.Math.Acos((Vector3.Dot(from, to) / num).Clamp(-1f, 1f)) * 57.29578f;
        }

        public static float Sign(this float f) => f >= 0.0f ? 1f : -1f;
        
        public static float SignedAngle(Vector2 from, Vector2 to)
        {
            return NormalizeSigned(Angle(from, to) * Sign(from.X * to.Y - @from.Y * to.X));
        }

        public static Vector2 Rotate(this Vector2 vector2, float angle)
        {
            return Vector2.Transform(vector2, Matrix3x2.CreateRotation(angle.DegToRad()));
        }

        public static float Abs(this float value)
        {
            return value < 0f ? -value : value;
        }

        public static float NormalizeAngle(this float angle)
        {
            while (angle >= 360f)
                angle -= 360f;
            while (angle < 0f)
                angle += 360f;
            return angle;
        }

        public static float NormalizeSigned(this float angle)
        {
            while (angle >= 180f)
                angle -= 360f;
            while (angle < -180f)
                angle += 360f;
            return angle;
        }
        public static float Repeat(float t, float length)
        {
            return ((float) (t - System.Math
                .Floor(t / length) * length)).Clamp(0.0f, length);
        }
        public static float LerpAngle(float a, float b, float t)
        {
            float num = Repeat(b - a, 360f);
            if ((double) num > 180.0)
                num -= 360f;
            return a + num * t.Clamp01();
        }
    }
}