﻿using System;
using Valkyrie.Data;

namespace Valkyrie.Math
{
    public class Random : IRandom
    {
        public int Seed { get; private set; }
        public int MaxValue { get { return (int) _max; } }
        public int MinValue { get { return 0; } }

        private long _currentSeed;

        private readonly long _gen1;
        private readonly long _gen2;
        private readonly long _max;

        public Random(int seed, long gen = 48271, long max = 2147483647)
        {
            Seed = seed;
            _currentSeed = seed;

            _gen1 = gen;
            _gen2 = _gen1*2;
            _max = max;
        }

        private int Value()
        {
            _currentSeed = (_gen1 * _currentSeed + _gen2) % _max;
            return (int)_currentSeed;
        }


        #region Implementation of IRandom

        public float Range(float min, float max)
        {
            return min + Next()*(max - min);
        }

        public float Next()
        {
            return (float) Value()/(MaxValue - 1);
        }

        public int Range(int min, int max)
        {
            var temp = Range((float) min, max);
            return (int)temp.Clamp(min, max - 1);
        }

        public float NormalDistribution()
        {
            var sum = 0f;
            for (var i = 0; i < 6; ++i)
                sum += Next();
            return sum/6f;
        }

        #endregion

        #region static

        private static readonly Random StaticRandom = new Random(DateTime.UtcNow.GetHashCode());

        public static float SRange(float min, float max)
        {
            lock (StaticRandom)
                return StaticRandom.Range(min, max);
        }

        public static int SRange(int min, int max)
        {
            lock (StaticRandom)
                return StaticRandom.Range(min, max);
        }

        #endregion
    }
}
