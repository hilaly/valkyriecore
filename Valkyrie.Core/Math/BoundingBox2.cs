using System.Diagnostics;
using System.Numerics;
using Valkyrie.Data;
using Valkyrie.Data.Serialization;

namespace Valkyrie.Math
{
    [Contract]
    public class BoundingBox2 : IContract
    {
        [ContractMember(0)] public Vector2 Min;
        [ContractMember(1)] public Vector2 Max;
        [Ignore] public Vector2 Center => (Max + Min)*0.5f;
        [Ignore] public Vector2 Size => Max - Min;

        public static BoundingBox2 CreateFromPoints(params Vector2[] points)
        {
            Debug.Assert(points != null);
            Debug.Assert(points.Length > 0);

            var min = new Vector2(points[0].X, points[0].Y);
            var max = new Vector2(points[0].X, points[0].Y);
            for (var index = 1; index < points.Length; index++)
            {
                var tmp = points[index];
                min = Vector2.Min(min, tmp);
                max = Vector2.Max(max, tmp);
            }

            return new BoundingBox2 { Max = max, Min = min };
        }

        public BoundingBox2 IntersectWith(BoundingBox2 area)
        {
            return new BoundingBox2 { Min = Vector2.Max(Min, area.Min), Max = Vector2.Min(Max, area.Max) };
        }

        public BoundingBox2 UnionWith(BoundingBox2 area)
        {
            return new BoundingBox2 {Min = Vector2.Min(Min, area.Min), Max = Vector2.Max(Max, area.Max)};
        }

        public bool IsIntersect(BoundingBox2 area)
        {
            var intersection = IntersectWith(area);
            if (!intersection.IsValid())
                return false;
            var size = intersection.Size;
            return size.X > 0f && size.Y > 0;
        }

        public bool IsValid()
        {
            return (Max.X < Min.X || Max.Y < Min.Y) == false;
        }

        public static bool IsTheSame(BoundingBox3 first, BoundingBox3 second)
        {
            return first.Min == second.Min &&
                   first.Max == second.Max;
        }

        public bool Contains(Vector2 coordinate)
        {
            return coordinate.X.IsInRange(Min.X, Max.X) && coordinate.Y.IsInRange(Min.Y, Max.Y);
        }

        public override bool Equals(object obj)
        {
            return obj is BoundingBox2 && Equals((BoundingBox2) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Min.GetHashCode() * 397) ^ Max.GetHashCode();
            }
        }

        public bool Equals(BoundingBox2 other)
        {
            return Min.Equals(other.Min) && Max.Equals(other.Max);
        }

        public override string ToString()
        {
            return $"(Min={Min},Max={Max})";
        }

        public void Serialize(IBitsPacker packer)
        {
            packer.Write(Min);
            packer.Write(Max);
        }

        public void Deserialize(IBitsPacker packer)
        {
            Min = packer.ReadVector2();
            Max = packer.ReadVector2();
        }
    }
}