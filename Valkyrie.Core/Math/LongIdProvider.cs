using System.Collections.Generic;
// ReSharper disable ClassNeverInstantiated.Global

namespace Valkyrie.Math
{
    internal class LongIdProvider : IIdProvider<long>
    {
        private long _default = 1;
        private readonly Stack<long> _freeIds = new Stack<long>();

        private long New()
        {
            return _default++;
        }

        #region Implementation of IIdProvider<T>

        public void Prepare(int count)
        {
            for(int i = 0; i < count; ++i)
                Release(New());
        }

        public long Generate()
        {
            lock (_freeIds)
                return _freeIds.Count > 0 ? _freeIds.Pop() : New();
        }

        public void Release(long id)
        {
            lock (_freeIds)
                _freeIds.Push(id);
        }

        #endregion
    }

    internal class IntIdProvider : IIdProvider<int>
    {
        private int _default = 1;
        private readonly Stack<int> _freeIds = new Stack<int>();

        private int New()
        {
            return _default++;
        }

        #region Implementation of IIdProvider<T>

        public void Prepare(int count)
        {
            for(int i = 0; i < count; ++i)
                Release(New());
        }

        public int Generate()
        {
            lock (_freeIds)
                return _freeIds.Count > 0 ? _freeIds.Pop() : New();
        }

        public void Release(int id)
        {
            lock (_freeIds)
                _freeIds.Push(id);
        }

        #endregion
    }

    internal class ShortIdProvider : IIdProvider<short>
    {
        private short _default = 1;
        private readonly Stack<short> _freeIds = new Stack<short>();

        private short New()
        {
            return _default++;
        }

        #region Implementation of IIdProvider<T>

        public void Prepare(int count)
        {
            for(int i = 0; i < count; ++i)
                Release(New());
        }

        public short Generate()
        {
            lock (_freeIds)
                return _freeIds.Count > 0 ? _freeIds.Pop() : New();
        }

        public void Release(short id)
        {
            lock (_freeIds)
                _freeIds.Push(id);
        }

        #endregion
    }

    internal class UShortIdProvider : IIdProvider<ushort>
    {
        private ushort _default = 1;
        private readonly Stack<ushort> _freeIds = new Stack<ushort>();

        private ushort New()
        {
            return _default++;
        }

        #region Implementation of IIdProvider<T>

        public void Prepare(int count)
        {
            for(int i = 0; i < count; ++i)
                Release(New());
        }

        public ushort Generate()
        {
            lock (_freeIds)
                return _freeIds.Count > 0 ? _freeIds.Pop() : New();
        }

        public void Release(ushort id)
        {
            lock (_freeIds)
                _freeIds.Push(id);
        }

        #endregion
    }

    internal class ByteIdProvider : IIdProvider<byte>
    {
        private byte _default = 1;
        private readonly Stack<byte> _freeIds = new Stack<byte>();

        private byte New()
        {
            return _default++;
        }

        #region Implementation of IIdProvider<T>

        public void Prepare(int count)
        {
            for(int i = 0; i < count; ++i)
                Release(New());
        }

        public byte Generate()
        {
            lock (_freeIds)
                return _freeIds.Count > 0 ? _freeIds.Pop() : New();
        }

        public void Release(byte id)
        {
            lock (_freeIds)
                _freeIds.Push(id);
        }

        #endregion
    }
}