﻿using System.Numerics;

namespace Valkyrie.Math
{
    public class RandomNormalDistribution : IRandom
    {
        float? _normalDistributionValue;
        private readonly IRandom _random;

        public RandomNormalDistribution(IRandom random)
        {
            _random = random;
        }

        /// <summary>
        /// Математическое ожидание
        /// </summary>
        public float Mean { get; set; }
        /// <summary>
        /// Стандартное отклонение
        /// </summary>
        public float Deviation { get; set; }

        public float Range(float min, float max)
        {
            return _random.Range(min, max);
        }

        public float Next()
        {
            return _random.Next();
        }

        public int Range(int min, int max)
        {
            return _random.Range(min, max);
        }

        public float NormalDistribution()
        {
            float result;
            if (_normalDistributionValue.HasValue)
            {
                result = _normalDistributionValue.Value;
                _normalDistributionValue = null;
            }
            else
            {
                float u0, u1;
                do
                {
                    u0 = Next();
                    u1 = Next();
                }
                while (u0 < float.Epsilon);

                var s = System.Math.Sqrt(-2f * System.Math.Log(u0));
                var v = System.Math.PI * 2f * u1;
                result = (float)(s * System.Math.Cos(v));
                _normalDistributionValue = (float)(s * System.Math.Sin(v));
            }
            return result*Deviation + Mean;
        }

        public static float GetNormalDistribution(float mean, float deviation)
        {
            float u0, u1;
            do
            {
                u0 = Random.SRange(0f, 1f);
                u1 = Random.SRange(0f, 1f);
            }
            while (u0 < float.Epsilon);

            var s = System.Math.Sqrt(-2f * System.Math.Log(u0));
            var v = System.Math.PI * 2f * u1;
            var result = s * System.Math.Cos(v);
            return (float)(result * deviation + mean);
        }

        public static Vector2 GetPlaneNormalDistribution(float mean, float deviation)
        {
            var u1 = Random.SRange(0f, 1f);
            float u0;
            do
            {
                u0 = Random.SRange(0f, 1f);
            }
            while (u0 < float.Epsilon);

            var s = System.Math.Sqrt(-2f * System.Math.Log(u0)) * deviation;
            var v = System.Math.PI * 2f * u1;
            return new Vector2((float) (s*System.Math.Cos(v) + mean), (float) (s*System.Math.Sin(v) + mean));
        }
    }
}