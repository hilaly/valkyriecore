namespace Valkyrie.Math
{
    public struct IntVec2
    {
        public int x;
        public int y;

        public IntVec2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}