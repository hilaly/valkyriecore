﻿namespace Valkyrie.Math
{
    public interface IIdProvider<T>
    {
        void Prepare(int count);
        
        T Generate();
        void Release(T id);
    }
}