using System;
using System.Collections.Generic;
using System.Linq;
using Valkyrie.Math.Graphs;
using Valkyrie.NewVersion.NotReviewed.AI.Pathfinders;

namespace Valkyrie.Math.Algorithms
{
    public class AStarPathfinder<TNode, TEdge>
		where TNode : class
    {
		private readonly IGraph<TNode, TEdge> _graph;
		private readonly Func<TNode, TNode, float> _heuristic;

		public AStarPathfinder(IGraph<TNode, TEdge> graph, Func<TNode, TNode, float> heuristic)
        {
            _graph = graph;
            _heuristic = heuristic;
        }

		public Path<TNode> Compute(TNode startNode, TNode endNode)
        {
            //TODO: replace with profiler Profiler.BeginSample("AStarPathfinder.Compute");
			var result = new Path<TNode>();
            StartCompute(startNode, endNode, result);
            //Profiler.EndSample();
            return result;
        }

		private void StartCompute(TNode startNode, TNode endNode, Path<TNode> path)
        {
            var nodes = AlgoExtensions.FindPathAStar(startNode, endNode, node => _graph.GetEdges(node),
                edge => edge.Head, edge => edge.Tail, edge => edge.Length, _heuristic);

            path.Nodes = nodes.Select(u => u.Tail).ToList();
        }
    }

    public static class AlgoExtensions
    {
        class AStarPathNode<TState, TStep>
            where TState : class
            where TStep : class
        {
            public TState Node { get; }
            public TState Parent { get; }
            public float G { get; }
            public float F { get; }
            public TStep Step { get; }

            public AStarPathNode(TState node, TState parent, float g, float heuristic, TStep step)
            {
                Node = node;
                Parent = parent;
                G = g;
                F = heuristic + G;
                Step = step;
            }
        }

        public static List<TStep> FindPathAStar<TState, TStep>(TState startState, TState endState,
            Func<TState, IEnumerable<TStep>> getEdges,
            Func<TStep, TState> getSource, Func<TStep, TState> getTarget, Func<TStep, float> getLength, Func<TState, TState, float> getHeuristicLength)
            where TState : class where TStep : class
        {
            var closeNodes = new List<AStarPathNode<TState, TStep>>();
            var openNodes = new List<AStarPathNode<TState, TStep>>
                {new AStarPathNode<TState, TStep>(startState, null, 0, 0, null)};

            List<TStep> Consruct(TStep lastAction, TState state, List<AStarPathNode<TState, TStep>> nodes)
            {
                var pathActions = new List<TStep> {lastAction};
                while (state != null)
                {
                    var state1 = state;
                    
                    var step = nodes.Find(u => u.Node == state1);
                    if (step != null)
                    {
                        if (step.Parent != null)
                            pathActions.Insert(0, step.Step);
                        state = step.Parent;
                    }
                    else
                        state = null;
                }

                return pathActions;
            }
            
            while (openNodes.Count > 0)
            {
                var current = openNodes[openNodes.Count - 1];
                openNodes.RemoveAt(openNodes.Count - 1);
                closeNodes.Add(current);

                var connections = getEdges(current.Node);
                foreach (var edge in connections)
                {
                    var node = getTarget(edge);
                    if (node == endState)
                        return Consruct(edge, getSource(edge), closeNodes);

                    if (closeNodes.Any(u => u.Node == node) || openNodes.Any(u => u.Node == node))
                        continue;
                    var pathNode = new AStarPathNode<TState, TStep>(node, getSource(edge), current.G + getLength(edge),
                        getHeuristicLength(current.Node, endState), edge);
                    var added = false;
                    for (var i = openNodes.Count - 1; i >= 0; --i)
                    {
                        var test = openNodes[i];
                        if (test.F < pathNode.F)
                            continue;
                        openNodes.Insert(i + 1, pathNode);
                        added = true;
                        break;
                    }

                    if (!added)
                        openNodes.Insert(0, pathNode);
                }
            }

            throw new Exception($"Can not find path form {startState} to {endState}");
        }
    }
}