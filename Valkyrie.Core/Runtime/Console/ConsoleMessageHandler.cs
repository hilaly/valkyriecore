using System.Collections.Generic;
using Valkyrie.Threading.Async;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Runtime.Console
{
    class ConsoleMessageHandler
    {
        private readonly List<LogRecord> _messages;

        public ConsoleMessageHandler(ILogService logSystem)
        {
            HistoryCount = 1000;
            _messages = new List<LogRecord>(HistoryCount);

            logSystem.Observe().Subscribe(AddMessage);
        }

        public int HistoryCount { get; set; }
        public IEnumerable<LogRecord> Messages
        {
            get
            {
                lock (_messages)
                    return _messages;
            }
        }

        public void AddMessage(LogRecord record)
        {
            lock (_messages)
            {
                if (_messages.Count >= HistoryCount)
                    _messages.RemoveAt(0);

                _messages.Add(record);
            }
        }
    }
}