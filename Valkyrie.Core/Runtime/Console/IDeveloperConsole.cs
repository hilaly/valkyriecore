﻿using System;
using System.Collections.Generic;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Runtime.Console
{
    public interface IDeveloperConsole
    {
        int HistoryCount { get; set; }
        IEnumerable<LogRecord> Messages { get; }
        void AddMessage(string message);

        void RegisterCommand(Type type);
        void RegisterCommand(object command);
        void Execute(string command);
    }
}
