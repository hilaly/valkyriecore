using System;

namespace Valkyrie.Runtime.Console
{
    interface IConsoleCommand
    {
        string Name { get; }
        string Info { get; }
        string Usage { get; }

        Type[] Args { get; }

        object Execute(object[] args);
    }
}