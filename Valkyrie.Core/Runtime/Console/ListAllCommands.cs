using System.Linq;
using System.Text;

namespace Valkyrie.Runtime.Console
{
    [DeveloperConsoleCommand(Alias = "help")]
    class ListAllCommands
    {
        private readonly CommandsEngine _commandsEngine;

        public ListAllCommands(CommandsEngine commandsEngine)
        {
            _commandsEngine = commandsEngine;
        }

        [DeveloperConsoleCommand(Usage = "type 'help'", Description = "prints all registered commands")]
        string Invoke()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Registered commads:");
            foreach (var command in _commandsEngine.Commands)
                sb.AppendLine(command.Name);
            return sb.ToString();
        }

        [DeveloperConsoleCommand(Usage = "type 'help [command]'", Description = "prints command's info")]
        string Invoke(string commandName)
        {
            var sb = new StringBuilder();
            sb.AppendLine(commandName);
            foreach (var command in _commandsEngine.Commands.Where(u => u.Name == commandName))
            {
                var info = command.Info;
                if(!string.IsNullOrEmpty(info))
                    sb.AppendLine(info);
                var usage = command.Usage;
                if (!string.IsNullOrEmpty(usage))
                    sb.AppendFormat("Usage: {0}", usage).AppendLine();
            }
            return sb.ToString();
        }
    }
}