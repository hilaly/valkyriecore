﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Valkyrie.Data;
using Valkyrie.Tools.Logs;
using IContainer = Valkyrie.Di.IContainer;

namespace Valkyrie.Runtime.Console
{
    class DeveloperConsole : IDeveloperConsole
    {
        private readonly IContainer _container;

        #region Messages

        private readonly ConsoleMessageHandler _messages;
        public int HistoryCount
        {
            get => _messages.HistoryCount;
            set => _messages.HistoryCount = value;
        }
        public IEnumerable<LogRecord> Messages => _messages.Messages;

        #endregion

        #region Commands

        private readonly CommandsEngine _commands;
        private readonly ILogInstance _log;

        public void AddMessage(string message)
        {
            _messages.AddMessage(new LogRecord
            {
                Message = message,
                Level = MessageLevel.Info,
                Channel = "Valkyrie.Console",
                StackTrace = new StackTrace(),
                Time = DateTime.UtcNow
            });
        }

        public void RegisterCommand(Type type)
        {
            var commandAttribute = (DeveloperConsoleCommandAttribute)type.GetCustomAttributes(typeof(DeveloperConsoleCommandAttribute), true).FirstOrDefault();
            foreach (var methodInfo in type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public))
            {
                var methodAttributes = methodInfo.GetCustomAttributes(typeof(DeveloperConsoleCommandAttribute), true);
                if (!methodAttributes.Any())
                    continue;

                foreach (var attribute in methodAttributes)
                    RegisterCommand(methodInfo, null, commandAttribute, (DeveloperConsoleCommandAttribute) attribute);
                if (!methodAttributes.Any())
                    RegisterCommand(methodInfo, null, commandAttribute, commandAttribute);
            }
        }

        public void RegisterCommand(object command)
        {
            var type = command.GetType();
            var commandAttribute = (DeveloperConsoleCommandAttribute)type.GetCustomAttributes(typeof(DeveloperConsoleCommandAttribute), true).FirstOrDefault();
            foreach (var methodInfo in type.GetMethods(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
            {
                var methodAttributes = methodInfo.GetCustomAttributes(typeof(DeveloperConsoleCommandAttribute), true);
                if (!methodAttributes.Any())
                    continue;

                foreach (var attribute in methodAttributes)
                    RegisterCommand(methodInfo, command, commandAttribute, (DeveloperConsoleCommandAttribute)attribute);
                if (!methodAttributes.Any())
                    RegisterCommand(methodInfo, command, commandAttribute, commandAttribute);
            }
        }

        void RegisterCommand(MethodInfo methodInfo, object command, DeveloperConsoleCommandAttribute attribute, DeveloperConsoleCommandAttribute overrideAttribute)
        {
            var commandInstance = new InstanceMethodCommand(_container, methodInfo, command)
            {
                Name = overrideAttribute.Alias ?? attribute.Alias,
                Usage = overrideAttribute.Usage ?? attribute.Usage,
                Info = overrideAttribute.Description ?? attribute.Description
            };
            if (string.IsNullOrEmpty(commandInstance.Name))
                throw new ArgumentNullException("Command hasn't alias");
            _commands.Add(commandInstance);
            _log.Info($"Register {commandInstance}");
        }

        public void Execute(string command)
        {
            _commands.Execute(command);
        }

        #endregion

        public DeveloperConsole(ILogService logSystem, IStringInterning stringsInterning, IContainer container)
        {
            _log = logSystem.GetLog("Valkyrie.Console");
            _container = container;
            _messages = new ConsoleMessageHandler(logSystem);
            _commands = new CommandsEngine(logSystem.GetLog("Valkyrie.Console.Commands"), stringsInterning, this);

            RegisterCommand(new ListAllCommands(_commands));
        }


    }
}