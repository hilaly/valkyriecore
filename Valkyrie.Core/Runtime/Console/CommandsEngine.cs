using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Valkyrie.Data;
using Valkyrie.Data.Config;
using Valkyrie.Tools.Logs;

namespace Valkyrie.Runtime.Console
{
    class CommandsEngine
    {
        private readonly IStringInterning _stringsInterning;
        private readonly ILogInstance _log;
        private readonly IDeveloperConsole _console;
        readonly List<IConsoleCommand> _commands = new List<IConsoleCommand>();

        public IEnumerable<IConsoleCommand> Commands => _commands;

        public CommandsEngine(ILogInstance log, IStringInterning stringsInterning, IDeveloperConsole console)
        {
            _log = log;
            _stringsInterning = stringsInterning;
            _console = console;
        }

        public void Execute(string strCommand)
        {
            var splitCommand = strCommand.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            var args = new string[splitCommand.Length - 1];
            for (var i = 0; i < args.Length; ++i)
                args[i] = splitCommand[i + 1];

            Execute(splitCommand[0], args);
        }

        private void Execute(string commandName, string[] args)
        {
            commandName = _stringsInterning.Intern(commandName);
            var findedCommands = new List<IConsoleCommand>();
            lock (_commands)
            {
                foreach (var consoleCommand in _commands)
                {
                    if (consoleCommand.Name != commandName)
                        continue;

                    findedCommands.Add(consoleCommand);

                    var commandArgs = consoleCommand.Args;
                    if (commandArgs.Length != args.Length)
                        continue;
                    try
                    {
                        var argsArray = new object[commandArgs.Length];
                        for (var i = 0; i < args.Length; ++i)
                            argsArray[i] = DataSerializer.DeserializeValue(commandArgs[i], args[i], _stringsInterning);
                        var result = consoleCommand.Execute(argsArray);
                        _console.AddMessage(result.ToString());
                        _log.Debug($"{commandName}({string.Join(",", args)})=>{result}");
                        return;
                    }
                    //DataSerializer exception
                    catch (SerializationException)
                    {
                        continue;
                    }
                    catch (Exception e)
                    {
                        _log.Error($"Exception during execute '{commandName}({string.Join(",", args)})': {e.Message}");
                        return;
                    }
                }
            }
            if (findedCommands.Count == 0)
                _log.Warn($"Couldn't execute '{commandName}({string.Join(",", args)})': '{commandName}' isn't registered command");
            else
            {
                var sb = new StringBuilder();
                sb.AppendLine($"Invalid command call '{commandName}({string.Join(",", args)})': possible variations:");
                sb.AppendLine();
                foreach (var command in findedCommands)
                    sb.AppendLine(GetCommandCall(command));
                _log.Warn(sb.ToString());
            }
        }

        public void Add(IConsoleCommand command)
        {
            lock(_commands)
                _commands.Add(command);
        }

        string GetCommandCall(IConsoleCommand command)
        {
            return $"{command.Name}({string.Join(",", command.Args.Select(type => type.Name).ToArray())})";
        }
    }
}