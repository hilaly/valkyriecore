using System;
using System.Linq;
using System.Reflection;
using Valkyrie.Di;
using IContainer = Valkyrie.Di.IContainer;

namespace Valkyrie.Runtime.Console
{
    // ReSharper disable once ClassNeverInstantiated.Global
    class InstanceMethodCommand : IConsoleCommand
    {
        private readonly IContainer _container;
        private readonly MethodInfo _method;
        private readonly object _instance;

        public InstanceMethodCommand(IContainer container, MethodInfo method, object instance)
        {
            _container = container;
            _method = method;
            _instance = instance;
            Args = _method.GetParameters().Select(parameter => parameter.ParameterType).ToArray();
        }

        public string Name { get; set; }
        public string Info { get; set; }
        public string Usage { get; set; }
        public Type[] Args { get; }

        public object Execute(object[] args)
        {
            return _container.Invoke(_method, _instance, args);
        }

        public override string ToString()
        {
            return $"{Name}({string.Join(",", Array.ConvertAll(Args, input => input.Name))})";
        }
    }
}