using System;

namespace Valkyrie.Runtime.Console
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Struct)]
    public class DeveloperConsoleCommandAttribute : Attribute
    {
        public string Alias { get; set; }
        public string Usage { get; set; }
        public string Description { get; set; }
    }
}