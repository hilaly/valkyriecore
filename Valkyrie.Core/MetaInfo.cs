﻿using System.Threading;

namespace Valkyrie
{
    class MetaInfo
    {
        public MetaInfo(string secretKey)
        {
            SecretKey = secretKey;
            BackgroundThreadsCount = 4;
        }

        public string SecretKey { get; }
        public int BackgroundThreadsCount { get; set; }
        public LockRecursionPolicy LockPolicy { get; set; }
    }
}